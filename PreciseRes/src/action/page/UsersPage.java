package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import PreciseRes.Interfaces;

public class UsersPage extends AbstractPage {
	public UsersPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}

	/**
	 * click New Button
	 */
	public void clickNewButton() {
		waitForControl(driver, Interfaces.UsersPage.NEW_USER_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('New').click();");
		sleep();
	}

	/**
	 * input Applicant Info
	 */
	public void inputApplicantInfo(String accountNumber, String applicantName) {
		waitForControl(driver, Interfaces.ApplicantsPage.NEW_APPLICANT_ACCOUNT_NUMBER_TEXTBOX, timeWait);
		type(driver, Interfaces.ApplicantsPage.NEW_APPLICANT_ACCOUNT_NUMBER_TEXTBOX, accountNumber);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.NEW_APPLICANT_NAME_TEXTBOX, applicantName);
		sleep(1);
	}

	/**
	 * click Next Button
	 */
	public void clickNextButton() {
		waitForControl(driver, Interfaces.ApplicantsPage.NEXT_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		sleep();
	}

	/**
	 * check Applicant Detail Page Saved
	 */
	public boolean isApplicantDetailPageSaved() {
		waitForControl(driver, Interfaces.ApplicantsPage.APPLICANT_DETAIL_SAVED_MESSAGE, timeWait);
		return isControlDisplayed(driver, Interfaces.ApplicantsPage.APPLICANT_DETAIL_SAVED_MESSAGE);
	}

	/**
	 * search Applicant By Name
	 */
	public void searchUserLegalName(String applicantName) {
		waitForControl(driver, Interfaces.UsersPage.USER_LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.UsersPage.USER_LEGAL_NAME_TEXTBOX, applicantName);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(2);
	}

	/**
	 * search Name
	 */
	public void searchName(String name) {
		waitForControl(driver, Interfaces.UsersPage.SEARCH_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.UsersPage.SEARCH_NAME_TEXTBOX, name);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(2);
	}

	/**
	 * search Entity Type
	 * 
	 * @param applicantName
	 * @param entityType
	 */
	public void searchEntityType(String applicantName, String entityType) {
		waitForControl(driver, Interfaces.UsersPage.SEARCH_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.UsersPage.SEARCH_NAME_TEXTBOX, applicantName);
		sleep(1);
		selectItemCombobox(driver, Interfaces.UsersPage.SEARCH_ENTITY_DROPDOWN_LIST, entityType);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(2);
	}

	/**
	 * search User Login
	 * 
	 * @param applicantName
	 * @param userLogin
	 */
	public void searchUserLogin(String applicantName, String userLogin) {
		waitForControl(driver, Interfaces.UsersPage.USER_LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.UsersPage.USER_LEGAL_NAME_TEXTBOX, applicantName);
		sleep(1);
		type(driver, Interfaces.UsersPage.SEARCH_USER_LOGIN_TEXTBOX, userLogin);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(2);
	}

	/**
	 * search Email
	 * 
	 * @param applicantName
	 * @param email
	 */
	public void searchEmail(String applicantName, String email) {
		waitForControl(driver, Interfaces.UsersPage.USER_LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.UsersPage.USER_LEGAL_NAME_TEXTBOX, applicantName);
		sleep(1);
		type(driver, Interfaces.UsersPage.SEARCH_MAIL_TEXTBOX, email);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(2);
	}

	/**
	 * search City
	 * 
	 * @param applicantName
	 * @param city
	 */
	public void searchCity(String applicantName, String city) {
		waitForControl(driver, Interfaces.UsersPage.USER_LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.UsersPage.USER_LEGAL_NAME_TEXTBOX, applicantName);
		sleep(1);
		type(driver, Interfaces.UsersPage.SEARCH_CITY_TEXTBOX, city);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(2);
	}

	/**
	 * search State
	 * 
	 * @param applicantName
	 * @param state
	 */
	public void searchState(String applicantName, String state) {
		waitForControl(driver, Interfaces.UsersPage.USER_LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.UsersPage.USER_LEGAL_NAME_TEXTBOX, applicantName);
		sleep(1);
		selectItemCombobox(driver, Interfaces.UsersPage.SEARCH_STATE_DROPDOWN_LIST, state);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(2);
	}

	/**
	 * search Loan
	 * 
	 * @param applicantName
	 * @param loan
	 */
	public void searchLoan(String applicantName, String loan) {
		waitForControl(driver, Interfaces.UsersPage.USER_LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.UsersPage.USER_LEGAL_NAME_TEXTBOX, applicantName);
		sleep(1);
		selectItemCombobox(driver, Interfaces.UsersPage.SEARCH_LOAN_DROPDOWN_LIST, loan);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(2);
	}

	/**
	 * open Applicants Detail Page
	 */
	public void openApplicantsDetailPage(String applicantName) {
		waitForControl(driver, Interfaces.ApplicantsPage.DYNAMIC_APPLICANT_ITEM, applicantName, timeWait);
		click(driver, Interfaces.ApplicantsPage.DYNAMIC_APPLICANT_ITEM, applicantName);
		sleep();
	}

	/**
	 * input City and State info
	 */
	public void inputCityAndStateInfo(String city, String state) {
		waitForControl(driver, Interfaces.UsersPage.CITY_TEXTBOX, timeWait);
		type(driver, Interfaces.UsersPage.CITY_TEXTBOX, city);
		sleep(1);
		selectItemCombobox(driver, Interfaces.UsersPage.STATE_DROPDOWN_LIST, state);
		sleep(1);
	}

	/**
	 * click New Contact Detail
	 */
	public void clickNewContactDetail() {
		waitForControl(driver, Interfaces.ApplicantsPage.NEW_CONTACT_DETAIL_BUTTON, timeWait);
		click(driver, Interfaces.ApplicantsPage.NEW_CONTACT_DETAIL_BUTTON);
		sleep();
	}

	/**
	 * input Applicant Contact Info
	 */
	public void inputApplicantContactInfo(String firstName, String lastName, String emailContact) {
		waitForControl(driver, Interfaces.ApplicantsPage.BORROWER_FIRST_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.ApplicantsPage.BORROWER_FIRST_NAME_TEXTBOX, firstName);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.BORROWER_LAST_NAME_TEXTBOX, lastName);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.BORROWER_EMAIL_CONTACT_TEXTBOX, emailContact);
		sleep(1);
	}

	/**
	 * click Save Applicant Contact
	 */
	public void clickSaveApplicantContact() {
		waitForControl(driver, Interfaces.ApplicantsPage.SAVE_APPLICANT_CONTACT_BUTTON, timeWait);
		click(driver, Interfaces.ApplicantsPage.SAVE_APPLICANT_CONTACT_BUTTON);
		sleep();
	}

	/**
	 * check Message Email Sent Display
	 */
	public boolean isMessageEmailSentDisplay(String email) {
		waitForControl(driver, Interfaces.ApplicantsPage.DYNAMIC_EMAIL_SENT_MESSAGE, email, timeWait);
		return isControlDisplayed(driver, Interfaces.ApplicantsPage.DYNAMIC_EMAIL_SENT_MESSAGE, email);
	}

	/**
	 * switch to applicant contact frame
	 */
	public WebDriver switchToApplicantContactFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.ApplicantsPage.APPLICANT_CONTACT_FRAME, timeWait);
		sleep();
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.ApplicantsPage.APPLICANT_CONTACT_FRAME)));
		return driver;
	}

	/**
	 * check Applicant Name Detail Correctly
	 */
	public boolean isApplicantNameDetailCorrectly(String applicantName) {
		waitForControl(driver, Interfaces.ApplicantsPage.APPLICANT_DETAIL_NAME_TEXTBOX, timeWait);
		String realName = getAttributeValue(driver, Interfaces.ApplicantsPage.APPLICANT_DETAIL_NAME_TEXTBOX, "value");
		return realName.contains(applicantName);
	}

	/**
	 * click Save Applicant Detail
	 */
	public void clickSaveApplicantDetail() {
		waitForControl(driver, Interfaces.ApplicantsPage.SAVE_APPLICANT_DETAIL_BUTTON, timeWait);
		click(driver, Interfaces.ApplicantsPage.SAVE_APPLICANT_DETAIL_BUTTON);
		sleep();
	}
	
	/**
	 * All value display in User search table
	 * 
	 * @param userLegalName
	 * @param entityType
	 * @param numberOfLoans
	 * @param userLogin
	 * @param email
	 * @param city
	 * @param state
	 * @return
	 */
	public boolean isAllValueDisplayOnSearchUsersTable(String userLegalName, String entityType, String numberOfLoans, String userLogin, String email, String city, String state) {
		waitForControl(driver, Interfaces.UsersPage.DYNAMIC_ALL_VALUE_DISPLAY_IN_SEARCH_USERS_TABLE, userLegalName, entityType, numberOfLoans, userLogin, email, city, state,
				timeWait);
		return isControlDisplayed(driver, Interfaces.UsersPage.DYNAMIC_ALL_VALUE_DISPLAY_IN_SEARCH_USERS_TABLE, userLegalName, entityType, numberOfLoans, userLogin, email, city,
				state);
	}

	/**
	 * Name and Entity display in User search table
	 * @param userLegalName
	 * @param entityType
	 * @return
	 */
	public boolean isNameAndEntityDisplayOnSearchUsersTable(String userLegalName, String entityType) {
		waitForControl(driver, Interfaces.UsersPage.DYNAMIC_NAME_AND_ENTITY_DISPLAY_IN_SEARCH_USERS_TABLE, userLegalName, entityType, timeWait);
		return isControlDisplayed(driver, Interfaces.UsersPage.DYNAMIC_NAME_AND_ENTITY_DISPLAY_IN_SEARCH_USERS_TABLE, userLegalName, entityType);
	}
	
	/**
	 * click Send this information to email
	 */
	public void clickSendInformationToEmail() {
		waitForControl(driver, Interfaces.UsersPage.SEND_INFORMATION_TO_EMAIL, timeWait);
		click(driver, Interfaces.UsersPage.SEND_INFORMATION_TO_EMAIL);
		sleep();
	}
	
	/**
	 * Get user information b2r/caf
	 */
	public String getUserInformationB2RCAF() {
		waitForControl(driver, Interfaces.UsersPage.FIRST_BORROWER, timeWait);
		return getText(driver, Interfaces.UsersPage.FIRST_BORROWER).trim().toLowerCase();
	}
	
	/**
	 * Get user information store
	 */
	public String getRecordInformationStore() {
		waitForControl(driver, Interfaces.UsersPage.FIRST_RECORD, timeWait);
		return getText(driver, Interfaces.UsersPage.FIRST_RECORD).trim().toLowerCase();
	}
	
	/**
	 * Get user login
	 */
	public String getUserLogin(){
		waitForControl(driver, Interfaces.UsersPage.USER_LOGIN, timeWait);
		return getText(driver, Interfaces.UsersPage.USER_LOGIN).trim();
	}
	
	/**
	 * click On 'Save' button
	 */
	public void clickOnSaveButton() {
		waitForControl(driver, Interfaces.UsersPage.SAVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		sleep(1);
	}
	
	/**
	 * click On 'Save' button
	 */
	public void clickOnCreateNewLoginButton() {
		waitForControl(driver, Interfaces.UsersPage.CREATE_NEW_LOGIN_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('NewLogin').click();");
		sleep(1);
	}
	
	/**
	 * check Load data message title display
	 * 
	 * @param N/A
	 */
	public boolean isBasicDetailMessageTitleDisplay(String messageContent) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_BASIC_DETAIL_MESSAGE_TITLE, messageContent, timeWait);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_BASIC_DETAIL_MESSAGE_TITLE, messageContent);
	}
	
	/**
	 * Create new user login
	 */
	public void createNewUser (String lastName, String entityType, String email) {
		searchName(lastName);
		waitForControl(driver, Interfaces.UsersPage.NO_RESULT_ALERT, 5);
		if(!isControlDisplayed(driver, Interfaces.UsersPage.NO_RESULT_ALERT)){
			if(isControlDisplayed(driver, Interfaces.UsersPage.DYNAMIC_LOGIN_NAME_LABEL, lastName))
			return;
			click(driver, Interfaces.UsersPage.OPEN_USERS_DETAILS_LINK);
			waitForControl(driver, Interfaces.UsersPage.LOGIN_LAST_NAME_TEXTFIELD, timeWait);
			type(driver, Interfaces.UsersPage.LOGIN_LAST_NAME_TEXTFIELD, lastName);
			type(driver, Interfaces.UsersPage.LOGIN_EMAIL_TEXTFIELD, email);
			clickOnCreateNewLoginButton();
			isBasicDetailMessageTitleDisplay("Record saved");
			executeJavaScript(driver, "document.getElementById('Search').click();");
			sleep(2);
			return;
		}
		clickNewButton();
		waitForControl(driver, Interfaces.UsersPage.LAST_NAME_TEXTFIELD, timeWait);
		type(driver, Interfaces.UsersPage.LAST_NAME_TEXTFIELD, lastName);
		type(driver, Interfaces.UsersPage.EMAIL_TEXTFIELD, email);
		selectItemCombobox(driver, Interfaces.UsersPage.ENTITY_TYPE_DROPDOWN, entityType);
		clickOnSaveButton();
		isBasicDetailMessageTitleDisplay("Record saved");
		type(driver, Interfaces.UsersPage.LOGIN_LAST_NAME_TEXTFIELD, lastName);
		type(driver, Interfaces.UsersPage.LOGIN_EMAIL_TEXTFIELD, email);
		clickOnCreateNewLoginButton();
		isBasicDetailMessageTitleDisplay("Record saved");
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(2);
	}
	
	/**
	 * Open Users tab
	 */
	public TransactionPage openTransactionPage() {
		waitForControl(driver, Interfaces.UsersPage.TRANSACTIONS_PAGE_LINK, timeWait);
		click(driver, Interfaces.UsersPage.TRANSACTIONS_PAGE_LINK);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
		sleep();
		return PageFactory.getTransactionPage(driver, ipClient);
	}
	
	/**
	 * Create new user login
	 */
	public void createNewUser (String lastName, String entityType, String email, String companyName) {
		clickNewButton();
		waitForControl(driver, Interfaces.UsersPage.LAST_NAME_TEXTFIELD, timeWait);
		type(driver, Interfaces.UsersPage.LAST_NAME_TEXTFIELD, lastName);
		type(driver, Interfaces.UsersPage.EMAIL_TEXTFIELD, email);
		type(driver, Interfaces.UsersPage.COMPANY_NAME_TEXTBOX, companyName);
		selectItemCombobox(driver, Interfaces.UsersPage.ENTITY_TYPE_DROPDOWN, entityType);
		clickOnSaveButton();
		isBasicDetailMessageTitleDisplay("Record saved");
		type(driver, Interfaces.UsersPage.LOGIN_LAST_NAME_TEXTFIELD, lastName);
		type(driver, Interfaces.UsersPage.LOGIN_EMAIL_TEXTFIELD, email);
		clickOnCreateNewLoginButton();
		isBasicDetailMessageTitleDisplay("Record saved");
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(2);
	}
	
	/**
	 * All value display in User search table
	 * 
	 * @param userName
	 * @param entityType
	 * @param companyName
	 * @param email
	 * @param loginName
	 * @return
	 */
	public boolean isAllValueDisplayOnSearchUsersEntityTable(String userName, String entityType, String companyName, String email, String loginName) {
		waitForControl(driver, Interfaces.UsersPage.DYNAMIC_ALL_VALUE_DISPLAY_IN_SEARCH_USERS_ENTITY_TABLE, userName, entityType, companyName, email, loginName, timeWait);
		return isControlDisplayed(driver, Interfaces.UsersPage.DYNAMIC_ALL_VALUE_DISPLAY_IN_SEARCH_USERS_ENTITY_TABLE, userName, entityType, companyName, email, loginName);
	}
	
	/**
	 * search Company Name
	 */
	public void searchCompanyName(String company) {
		waitForControl(driver, Interfaces.UsersPage.SEARCH_COMPANY_TEXTBOX, timeWait);
		type(driver, Interfaces.UsersPage.SEARCH_COMPANY_TEXTBOX, company);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(2);
	}
	
	/**
	 * search by email
	 */
	public void searchEmail(String company) {
		waitForControl(driver, Interfaces.UsersPage.SEARCH_MAIL_TEXTBOX, timeWait);
		type(driver, Interfaces.UsersPage.SEARCH_MAIL_TEXTBOX, company);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(2);
	}
	
	/**
	 * search Login name
	 */
	public void searchLogin(String company) {
		waitForControl(driver, Interfaces.UsersPage.SEARCH_USER_LOGIN_TEXTBOX, timeWait);
		type(driver, Interfaces.UsersPage.SEARCH_USER_LOGIN_TEXTBOX, company);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(2);
	}
	
	/**
	 * click on User Detail icon of existing login to open user Details page
	 */
	public void openUserDetailsOfExistingLogin() {
		waitForControl(driver, Interfaces.ApplicantsPage.EXISTING_LOGIN_ICON,
				timeWait);
		click(driver, Interfaces.ApplicantsPage.EXISTING_LOGIN_ICON);
	}
	
	/**
	 * is Transactions Child Table Displayed Correctly
	 */
	
	public Boolean isTransactionsChildTableDisplayedCorrectly(String leeseName, String company, String clientID, String dateCreated, String active) {
		waitForControl(driver, Interfaces.UsersPage.TRANSACTION_CHILD_TABLE_LEESE_NAME, timeWait);
		if (!getText(driver, Interfaces.UsersPage.TRANSACTION_CHILD_TABLE_LEESE_NAME).contains(leeseName)){
			System.out.println("Failed at: "+leeseName);
			return false;
		}
		if (!getText(driver, Interfaces.UsersPage.TRANSACTION_CHILD_TABLE_COMPANY_NAME).contains(company)){
			System.out.println("Failed at: "+company);
			return false;
		}
		if (!getText(driver, Interfaces.UsersPage.TRANSACTION_CHILD_TABLE_CLIENT_ID).contains(clientID)){
			System.out.println("Failed at: "+clientID);
			return false;
		}
		if (!getText(driver, Interfaces.UsersPage.TRANSACTION_CHILD_TABLE_DATE_CREATED).contains(dateCreated)){
			System.out.println("Failed at: "+dateCreated);
			return false;
		}
		if (!getText(driver, Interfaces.UsersPage.TRANSACTION_CHILD_TABLE_ACTIVE).contains(active)){
			System.out.println("Failed at: "+active);
			return false;
		}
		return true;
	}
	
	public TransactionPage clickOnTransactionLink() {
		waitForControl(driver, Interfaces.UsersPage.TRANSACTION_CHILD_TABLE_LEESE_NAME, timeWait);
		click(driver, Interfaces.UsersPage.TRANSACTION_CHILD_TABLE_LEESE_NAME);
		return PageFactory.getTransactionPage(driver, ipClient);
	}
	
	private WebDriver driver;
	private String ipClient;
}