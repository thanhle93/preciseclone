package page;

import java.util.Set;
import java.util.Stack;

import org.openqa.selenium.WebDriver;
import common.Common;
import PreciseRes.Interfaces;

public class FNFRehabsPage extends AbstractPage{
	public FNFRehabsPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}
	
	/**
	 * click New Properties Button
	 */
	public void clickNewDocumentButton() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.NEW_PROPERTIES_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('New').click();");
		sleep(1);
	}
	
	/**
	 * input Lender, Applicant, Address, City, State, Zip Code
	 */
	public void inputValidInformationProperties(String lender,	String applicant, String address, String city,
			String state, String zipCode) {
		waitForControl(driver, Interfaces.FNFPropertiesPage.LENDER_COMBOBOX, lender, timeWait);
		if (!lender.equals("")) {
			selectItemCombobox(driver, Interfaces.FNFPropertiesPage.LENDER_COMBOBOX, lender);
		}
		waitForControl(driver, Interfaces.FNFPropertiesPage.APPLICANT_COMBOBOX, applicant, timeWait);
		if (!applicant.equals("")) {
			selectItemCombobox(driver, Interfaces.FNFPropertiesPage.APPLICANT_COMBOBOX, applicant);
		}
		sleep(1);
		type(driver, Interfaces.FNFPropertiesPage.ADDRESS_TEXTBOX, address);
		sleep(1);
		type(driver, Interfaces.FNFPropertiesPage.CITY_TEXTBOX, city);
		sleep(1);
		selectItemCombobox(driver, Interfaces.FNFPropertiesPage.STATE_COMBOBOX, state);
		sleep(1);
		type(driver, Interfaces.FNFPropertiesPage.ZIP_TEXTBOX, zipCode);
		sleep(1);
	}
	
	/**
	 * click On 'Save' button
	 */
	public void clickOnSaveButton() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.SAVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		sleep(1);
	}
	
	/**
	 * check Record Saved Message Displays
	 * 
	 * @return
	 */
	public boolean isRecordSavedMessageDisplays() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.RECORD_SAVED_MESSAGE, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFPropertiesPage.RECORD_SAVED_MESSAGE);
	}
	
	/**
	 * check 'Lender, Applicant, Address, City, State, Zip Code' Saved [Applicant]
	 */
	public boolean isApplicantPropertiesInformationSaved(String lender, String applicant, String address, String city, String state, String zipCode) {
		waitForControl(driver, Interfaces.FNFPropertiesPage.LENDER_COMBOBOX, timeWait);
		String realLender = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.LENDER_COMBOBOX, "value");
		String realApplicant = getText(driver, Interfaces.FNFPropertiesPage.APPLICANT_COMBOBOX);
		String realAddress = getAttributeValue(driver, Interfaces.FNFPropertiesPage.ADDRESS_TEXTBOX, "value");
		String realCity = getAttributeValue(driver, Interfaces.FNFPropertiesPage.CITY_TEXTBOX, "value");
		String realState = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.STATE_COMBOBOX, "value");
		String realZipCode = getAttributeValue(driver, Interfaces.FNFPropertiesPage.ZIP_TEXTBOX, "value");
		return realLender.contains(lender) && realApplicant.contains(applicant) && realAddress.contains(address) 
		&& realCity.contains(city) && realState.contains(state) && realZipCode.contains(zipCode);
	}

	/**
	 * check 'Lender, Applicant, Address, City, State, Zip Code' Saved [Lender]
	 */
	public boolean isLenderPropertiesInformationSaved(String lender, 
			String applicant, String address, String city, String state, String zipCode) {
		waitForControl(driver, Interfaces.FNFPropertiesPage.LENDER_COMBOBOX, timeWait);
		String realLender = getText(driver, Interfaces.FNFPropertiesPage.LENDER_COMBOBOX);
		String realApplicant = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.APPLICANT_COMBOBOX, "value");
		String realAddress = getAttributeValue(driver, Interfaces.FNFPropertiesPage.ADDRESS_TEXTBOX, "value");
		String realCity = getAttributeValue(driver, Interfaces.FNFPropertiesPage.CITY_TEXTBOX, "value");
		String realState = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.STATE_COMBOBOX, "value");
		String realZipCode = getAttributeValue(driver, Interfaces.FNFPropertiesPage.ZIP_TEXTBOX, "value");
		return realLender.contains(lender) && realApplicant.contains(applicant) && realAddress.contains(address) 
		&& realCity.contains(city) && realState.contains(state) && realZipCode.contains(zipCode);
	}
	
	/**
	 * check Detail Message Display With Text
	 */
	public boolean isSavedNameMessageDisplayWithText(String messageContent) {
		waitForControl(driver, Interfaces.FNFPropertiesPage.SAVE_NAME_MESSAGE_DISPLAY_WITH_TEXT,
				messageContent, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFPropertiesPage.SAVE_NAME_MESSAGE_DISPLAY_WITH_TEXT,
				messageContent);
	}

	/**
	 * click On 'Back To List' button
	 */
	public void clickOnBackToListButton() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.BACK_TO_LIST_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('ListProperties').click();");
		sleep(1);
	}
	
	/**
	 * search Property address
	 */
	public void searchAddress(String address) {
		waitForControl(driver,
				Interfaces.FNFPropertiesPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.FNFPropertiesPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX,
				address);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep();
	}
	
	/**
	 * open Property Detail
	 */
	public FNFRehabsPage openPropertyDetail(String address) {
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_ADDRESS_PROPERTY,	address, timeWait);
		click(driver, Interfaces.FNFPropertiesPage.DYNAMIC_ADDRESS_PROPERTY, address);
		sleep();
		return PageFactory.getRehabsPageFNF(driver, ipClient);
	}

	/**
	 * upload Document File
	 */
	public void uploadDocumentFile(String fileName) {
		waitForControl(driver, Interfaces.PropertiesPage.CHOOSE_FILE_BUTTON, timeWait);
		click(driver, Interfaces.PropertiesPage.CHOOSE_FILE_BUTTON);
		if(driver.toString().toLowerCase().contains("internetexplorerdriver"))
		{
			keyPressing("space");
		}
		sleep(1);
		Common.getCommon().openFileForUpload(driver, fileName);
		sleep();
	}
	
	/**
	 * click Save Button
	 */
	public void clickSaveButton() {
		waitForControl(driver, Interfaces.PropertiesPage.SAVE_BUTTON, timeWait);
//		driver.findElement(By.id("Save")).sendKeys(Keys.ENTER);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		sleep(1);
	}
	
	
	/**
	 * search Active
	 */
	public void searchActive() {
		waitForControl(driver,Interfaces.DocumentsPage.ACTIVE_RADIO_BUTTON, timeWait);
		click(driver, Interfaces.DocumentsPage.ACTIVE_RADIO_BUTTON);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep();
	}
	
	/**
	 * get Document ID In [General Loan Document - Corporate Entity Documents] Tab
	 */
	public String getDocumentID() {
		waitForControl(driver, Interfaces.DocumentsPage.GET_DOCUMENT_ID, timeWait);
		String documentID = getText(driver, Interfaces.DocumentsPage.GET_DOCUMENT_ID);
		return documentID;
	}
	
	public void searchDocumentID(String documentID) {
		waitForControl(driver, Interfaces.DocumentsPage.DOCUMENT_ID_TEXTBOX,	documentID, timeWait);
		type(driver, Interfaces.DocumentsPage.DOCUMENT_ID_TEXTBOX, documentID);
		sleep(1);
		click(driver, Interfaces.DocumentsPage.ACTIVE_RADIO_YES_BUTTON);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(1);
	}
	
	/**
	 * check Document Loaded To Document Type
	 */
	public boolean isDocumentLoadedToDocumentType(String docType, String fileName) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL,
				docType, fileName, timeWait);
		return isControlDisplayed(driver, Interfaces.PropertiesPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL,
				docType, fileName);
	}
	
	/**
	 * check Private Checkbox Checked
	 */
	public boolean isPrivateCheckboxChecked() {
		waitForControl(driver, Interfaces.PropertiesPage.PRIVATE_CHECKBOX, timeWait);
		return isCheckboxChecked(driver, Interfaces.PropertiesPage.PRIVATE_CHECKBOX);
	}
	
	/**
	 * check Document Type Display On Property Detail
	 */
	public boolean isDocumentTypeDisplayOnPropertyDetail(String docType) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_DOC_TYPE_ON_PROPERTY_DETAIL, docType, 5);
		return isControlDisplayed(driver, Interfaces.PropertiesPage.DYNAMIC_DOC_TYPE_ON_PROPERTY_DETAIL, docType);
	}
	
	/**
	 * check Uploaded Document Opened
	 */
	public boolean isUploadedDocumentOpened(String fileName) {
		sleep();
		String currentHandle = driver.getWindowHandle();
		openWindowHandles.push(driver.getWindowHandle());
		Set<String> newHandles = driver.getWindowHandles();
		newHandles.removeAll(openWindowHandles);
		String handle = newHandles.iterator().next();
		driver.switchTo().window(handle);
		String url = getCurrentUrl(driver);
		boolean exists = url.contains(fileName);
		driver.close();
		driver.switchTo().window(currentHandle);
		return exists;
	}
	
	/**
	 * open Document Of Document Type On Property Detail
	 */
	public void openDocumentOfDocumentTypeOnPropertyDetail(String documentType, String documentName) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL,
				documentType, documentName, timeWait);
		click(driver, Interfaces.PropertiesPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL,
				documentType, documentName);
		sleep(3);
	}
	
	/**
	 * update Property information
	 */
	public void updatePropertyInfor(String newCity, String newState, String newZip, String newPropertyType) {
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTY_CITY_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.PROPERTY_CITY_TEXTBOX, newCity);
		sleep(1);
		selectItemCombobox(driver, Interfaces.PropertiesPage.PROPERTY_STATE_COMBOBOX, newState);
		sleep(1);
		type(driver, Interfaces.PropertiesPage.PROPERTY_ZIP_TEXTBOX, newZip);
		sleep(1);
		selectItemCombobox(driver, Interfaces.PropertiesPage.PROPERTY_TYPE_COMBOBOX, newPropertyType);
		sleep(1);
		clickSaveButton();
	}
	
	/**
	 * check 'Document Type' On Properties Detail Loaded
	 */
	public boolean isDocumentTypeOnPropertiesDetailLoaded(String documentType) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_DOCTYPE_ON_PROPERTY_DETAIL,
				documentType, 5);
		return isControlDisplayed(driver, Interfaces.PropertiesPage.DYNAMIC_DOCTYPE_ON_PROPERTY_DETAIL,
				documentType);
	}
	
	/**
	 * upload Dynamic Document File by Place Holder
	 */
	public void uploadDynamicDocumentFileByPlaceHolder(String documentType, String fileName) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_CHOOSE_FILE_BUTTON_PLACE_HOLDER, documentType, timeWait);
		click(driver, Interfaces.PropertiesPage.DYNAMIC_CHOOSE_FILE_BUTTON_PLACE_HOLDER, documentType);
		if(driver.toString().toLowerCase().contains("internetexplorerdriver"))
		{
			keyPressing("space");
		}
		sleep(1);
		Common.getCommon().openFileForUpload(driver, fileName);
		sleep();
	}
	
	/**
	 * open Property Documents Tab
	 */
	public void openPropertyDocumentsTab() {
		waitForControl(driver, Interfaces.LoansPage.PROPERTY_DOCUMENTS_TAB,
				timeWait);
		click(driver, Interfaces.LoansPage.PROPERTY_DOCUMENTS_TAB);
		sleep();
	}
	
	private WebDriver driver;
	private String ipClient; 
	private final Stack<String> openWindowHandles = new Stack<String>();
}