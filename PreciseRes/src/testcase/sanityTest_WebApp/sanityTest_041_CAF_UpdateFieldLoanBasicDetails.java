package sanityTest_WebApp;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;

import common.AbstractTest;
import common.Constant;

public class sanityTest_041_CAF_UpdateFieldLoanBasicDetails extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		applicantName = "CAF Demo Applicant";
		loanStatus = "Term Sheet Issued";
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan"+accountNumber;
		state = "AA - Federal Services";
		companyType = "Non Profit Corporation";
		validCurrency = "$2,000,000.00";
		validDate = "12/31/2000";
		validPhoneNumber = "12345678901";
		validZipCode = "12345";
		validText = "Common value !@#";
		validEmail = "abc@gmail.com";
		validWholeNumber = "123";
		validNumber = "12.31";
		loanTerm = "5 year";
		
		newAccountNumber = getUniqueNumber()+200;
		newLoanName = "UdiTeam-Loan"+newAccountNumber;
		newLoanStatus = "Data Tape Received";
		newState = "AE - Federal Services";
		newCompanyType = "Limited Partnership";
		newValidCurrency = "$3,000,000.00";
		newValidDate = "11/11/2000";
		newValidPhoneNumber = "12345678902";
		newValidZipCode = "12346";
		newValidText = "Common new value !@#";
		newValidEmail = "newemail@gmail.com";
		newValidWholeNumber = "111";
		newValidNumber = "11.11";
		newLoanTerm = "10 year";
	}

	@Test(groups = { "regression" }, description = "Update field Loan Basic details 01 - Update field")
	public void UpdateFieldLoanBasicDetails_01_UpdateField() {
		
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Precondition 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus);
		
		//input all valid date
		
		log.info("ValidataDatatypes_02 - Step 01. Input Loan Amount Requested");
		loansPage.inputLoanAmountRequested(validCurrency);
		
		log.info("ValidataDatatypes_03 - Step 02: Input the invalid data to 'Term Sheet Issued Date ' field");
		loansPage.inputTermSheetIssuedDate(validDate);
		
		log.info("ValidataDatatypes_03 - Step 03: Input the invalid data to 'Term Sheet Signed Date ");
		loansPage.inputTermSheetSignedDate(validDate);
		
		log.info("ValidataDatatypes_03 - Step 04: Input the invalid data to 'Anticipated IC Approval ");
		loansPage.inputAnticipatedICApprovalDate(validDate);
		
		log.info("ValidataDatatypes_03 - Step 05: Input the invalid data to 'Date Established ");
		loansPage.inputDateEstablished(validDate);
		
		log.info("ValidataDatatypes_03 - Step 06: Input the invalid data to 'Expected Close Date ");
		loansPage.inputExpectedCloseDate(validDate);
		
		log.info("ValidataDatatypes_03 - Step 07: Input the invalid data to 'Kick Off Date ");
		loansPage.inputKickOffDate(validDate);
		
		log.info("ValidataDatatypes_04 - Step 08: Input the invalid data to 'Phone' field");
		loansPage.inputPhoneTextbox(validPhoneNumber);
		
		log.info("ValidataDatatypes_04 - Step 09: Input the invalid data to 'Zip' field");
		loansPage.inputZipcode(validZipCode);
		
		log.info("ValidataDatatypes_04 - Step 10: Input the invalid data to 'Email' field");
		loansPage.inputEmailTextbox(validEmail);
		
		log.info("ValidataDatatypes_05 - Step 11: Input the invalid data to 'Floor' field");
		loansPage.inputFloor(validNumber);
		
		log.info("ValidataDatatypes_05 - Step 12: Input the invalid data to 'Amortization Term' field");
		loansPage.inputAmortizationTerm(validWholeNumber);
		
		log.info("ValidataDatatypes_05 - Step 13: Input the invalid data to 'Loan Term' field");
		loansPage.selectLoanTerm(loanTerm);
		
		log.info("ValidataDatatypes_05 - Step 14: Input the invalid data to 'IO Term' field");
		loansPage.inputIOTerm(validWholeNumber);
		
		log.info("ValidataDatatypes_05 - Step 15: Input the invalid data to 'LTV' field");
		loansPage.inputLTV(validNumber);
		
		log.info("ValidataDatatypes_05 - Step 16: Input the invalid data to 'Loan ID' field");
		loansPage.inputLoanID(validText+accountNumber);
		
		log.info("ValidataDatatypes_05 - Step 17: Input the invalid data to 'Company website' field");
		loansPage.inputCompanyWebsite(validText);
		
		log.info("ValidataDatatypes_05 - Step 18: Input the invalid data to 'County' field");
		loansPage.inputCounty(validText);
		
		log.info("ValidataDatatypes_05 - Step 19: Input the invalid data to 'Address' field");
		loansPage.inputAddress(validText);
		
		log.info("ValidataDatatypes_05 - Step 20: Input the invalid data to 'City' field");
		loansPage.inputCity(validText);
		
		log.info("ValidataDatatypes_05 - Step 21: Input the invalid data to 'Cancellation Reason' field");
		loansPage.inputCancellationReason(validText);
		
		log.info("ValidataDatatypes_05 - Step 22: Input the valid data to 'Foreign National' field");
		loansPage.selectForeignNational("Yes");
		
		log.info("ValidataDatatypes_05 - Step 23: Input the valid data to 'Loan Purpose' field");
		loansPage.selectLoanPurpose("Refi");
		
		log.info("ValidataDatatypes_05 - Step 24: Input the valid data to 'Refinance Type' field");
		loansPage.selectRefinanceType("Refi Debt");
		
		log.info("ValidataDatatypes_05 - Step 25: Input the valid data to 'Property Management' field");
		loansPage.selectPropertyManagement("Self-Managed");
		
		log.info("ValidataDatatypes_05 - Step 26: Input the valid data to 'Company Type' field");
		loansPage.selectRecourse("Yes");
		
		log.info("ValidataDatatypes_05 - Step 27: Input the valid data to 'Bridge Borrower' field");
		loansPage.selectBridgeBorrower("Yes");
		
		log.info("ValidataDatatypes_05 - Step 28: Input the valid data to 'Company Type' field");
		loansPage.selectCompanyType(companyType);
		
		log.info("ValidataDatatypes_05 - Step 29: Input the valid data to 'State' field");
		loansPage.selectState(state);
		
		log.info("ValidataDatatypes_05 - Step 30: Input the valid data to 'Owner' field");
		loansPage.selectOwner("Dennis Spivey");
		
		log.info("ValidataDatatypes_05 - Step 31: Input the valid data to 'Source' field");
		loansPage.selectSource("Bridge Borrower");
		
		log.info("ValidataDatatypes_05 - Step 32: Click 'Save' button");
		loansPage.clickSaveButton();
		
		//Check data saved
		
		log.info("VP: Verify that Loan Amount Requested is saved successfully");
		verifyTrue(loansPage.isLoanAmountRequestedDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that 'Term Sheet Issued Date ' field is saved successfully");
		verifyTrue(loansPage.isTermSheetIssuedDateDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that'Term Sheet Signed Date is saved successfully ");
		verifyTrue(loansPage.isTermSheetSignedDateDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that 'Anticipated IC Approval is saved successfully");
		verifyTrue(loansPage.isAnticipatedICApprovalDateDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that 'Date Established is saved successfully ");
		verifyTrue(loansPage.isDateEstablishedDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that 'Expected Close Date is saved successfully ");
		verifyTrue(loansPage.isExpectedCloseDateDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that 'Kick Off Date is saved successfully ");
		verifyTrue(loansPage.isKickOffDateDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that 'Phone' field is saved successfully");
		verifyTrue(loansPage.isPhoneTextboxDisplayedCorrectly(validPhoneNumber));
		
		log.info("VP: Verify that 'Zip' field is saved successfully");
		verifyTrue(loansPage.isZipcodeDisplayedCorrectly(validZipCode));
		
		log.info("VP: Verify that 'Email' field is saved successfully");
		verifyTrue(loansPage.isEmailTextboxDisplayedCorrectly(validEmail));
		
		log.info("VP: Verify that 'Floor' field is saved successfully");
		verifyTrue(loansPage.isFloorDisplayedCorrectly(validNumber));
		
		log.info("VP: Verify that 'Amortization Term' field is saved successfully");
		verifyTrue(loansPage.isAmortizationTermDisplayedCorrectly(validWholeNumber));
		
		log.info("VP: Verify that 'Loan Term' field is saved successfully");
		verifyTrue(loansPage.isLoanTermDisplayedCorrectly(loanTerm));
		
		log.info("VP: Verify that 'IO Term' field is saved successfully");
		verifyTrue(loansPage.isIOTermDisplayedCorrectly(validWholeNumber));
		
		log.info("VP: Verify that 'LTV' field is saved successfully");
		verifyTrue(loansPage.isLTVDisplayedCorrectly(validNumber));
		
		log.info("VP: Verify that 'Loan ID' field is saved successfully");
		verifyTrue(loansPage.isLoanIDDisplayed(validText+accountNumber));
		
		log.info("VP: Verify that 'Company website' field is saved successfully");
		verifyTrue(loansPage.isCompanyWebsiteDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'County' field is saved successfully");
		verifyTrue(loansPage.isCountyDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Address' field is saved successfully");
		verifyTrue(loansPage.isAddressDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'City ' field is saved successfully");
		verifyTrue(loansPage.isCityDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Cancellation Reason' field is saved successfully");
		verifyTrue(loansPage.isCancellationReasonDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Foreign National' field is saved successfully");
		verifyTrue(loansPage.isForeignNationalDisplayedCorrectly("Yes"));
		
		log.info("VP: Verify that 'Loan Purpose' field is saved successfully");
		verifyTrue(loansPage.isLoanPurposeDisplayedCorrectly("Refi"));
		
		log.info("VP: Verify that 'Refinance Type' field is saved successfully");
		verifyTrue(loansPage.isRefinanceTypeDisplayedCorrectly("Refi Debt"));
		
		log.info("VP: Verify that 'Property Management' field is saved successfully");
		verifyTrue(loansPage.isPropertyManagementDisplayedCorrectly("Self-Managed"));
		
		log.info("VP: Verify that 'Recourse' field is saved successfully");
		verifyTrue(loansPage.isRecourseDisplayedCorrectly("Yes"));
		
		log.info("VP: Verify that 'Bridge Borrower' field is saved successfully");
		verifyTrue(loansPage.isBridgeBorrowerDisplayedCorrectly("Yes"));
		
		log.info("VP: Verify that 'Company Type' field is saved successfully");
		verifyTrue(loansPage.isCompanyTypeDisplayedCorrectly(companyType));
		
		log.info("VP: Verify that 'State' field is saved successfully");
		verifyTrue(loansPage.isStateDisplayedCorrectly(state));
		
		log.info("VP: Verify that 'Owner' field is saved successfully");
		verifyTrue(loansPage.isOwnerDisplayedCorrectly("Dennis Spivey"));
		
		log.info("VP: Verify that 'Source' field is saved successfully");
		verifyTrue(loansPage.isSourceDisplayedCorrectly("Bridge Borrower"));
		
		//input new valid data
		
		log.info("ValidataDatatypes_05 - Step 01: Input the valid data to 'Loan Name' field");
		loansPage.inputLoanName(newLoanName);
		
		log.info("ValidataDatatypes_05 - Step 02: Input the valid data to 'Loan status' field");
		loansPage.selectLoanStatusBasicDetail(newLoanStatus);
		
		log.info("newValidataDatatypes_02 - Step 03. Input Loan Amount Requested");
		loansPage.inputLoanAmountRequested(newValidCurrency);
		
		log.info("newValidataDatatypes_03 - Step 04: Input the new valid data to 'Term Sheet Issued Date ' field");
		loansPage.inputTermSheetIssuedDate(newValidDate);
		
		log.info("newValidataDatatypes_03 - Step 05: Input the new valid data to 'Term Sheet Signed Date ");
		loansPage.inputTermSheetSignedDate(newValidDate);
		
		log.info("newValidataDatatypes_03 - Step 06: Input the new valid data to 'Anticipated IC Approval ");
		loansPage.inputAnticipatedICApprovalDate(newValidDate);
		
		log.info("newValidataDatatypes_03 - Step 07: Input the new valid data to 'Date Established ");
		loansPage.inputDateEstablished(newValidDate);
		
		log.info("newValidataDatatypes_03 - Step 08: Input the new valid data to 'Expected Close Date ");
		loansPage.inputExpectedCloseDate(newValidDate);
		
		log.info("newValidataDatatypes_03 - Step 09: Input the new valid data to 'Kick Off Date ");
		loansPage.inputKickOffDate(newValidDate);
		
		log.info("newValidataDatatypes_04 - Step 10: Input the new valid data to 'Phone' field");
		loansPage.inputPhoneTextbox(newValidPhoneNumber);
		
		log.info("newValidataDatatypes_04 - Step 11: Input the new valid data to 'Zip' field");
		loansPage.inputZipcode(newValidZipCode);
		
		log.info("newValidataDatatypes_04 - Step 12: Input the new valid data to 'Email' field");
		loansPage.inputEmailTextbox(newValidEmail);
		
		log.info("newValidataDatatypes_05 - Step 13: Input the new valid data to 'Floor' field");
		loansPage.inputFloor(newValidNumber);
		
		log.info("newValidataDatatypes_05 - Step 14: Input the new valid data to 'Amortization Term' field");
		loansPage.inputAmortizationTerm(newValidWholeNumber);
		
		log.info("newValidataDatatypes_05 - Step 15: Input the new valid data to 'Loan Term' field");
		loansPage.selectLoanTerm(newLoanTerm);
		
		log.info("newValidataDatatypes_05 - Step 16: Input the new valid data to 'IO Term' field");
		loansPage.inputIOTerm(newValidWholeNumber);
		
		log.info("newValidataDatatypes_05 - Step 17: Input the new valid data to 'LTV' field");
		loansPage.inputLTV(newValidNumber);
		
		log.info("newValidataDatatypes_05 - Step 18: Input the new valid data to 'Loan ID' field");
		loansPage.inputLoanID(newValidText+accountNumber);
		
		log.info("newValidataDatatypes_05 - Step 19: Input the new valid data to 'Company website' field");
		loansPage.inputCompanyWebsite(newValidText);
		
		log.info("newValidataDatatypes_05 - Step 20: Input the new valid data to 'County' field");
		loansPage.inputCounty(newValidText);
		
		log.info("newValidataDatatypes_05 - Step 21: Input the new valid data to 'Address' field");
		loansPage.inputAddress(newValidText);
		
		log.info("newValidataDatatypes_05 - Step 22: Input the new valid data to 'City' field");
		loansPage.inputCity(newValidText);
		
		log.info("newValidataDatatypes_05 - Step 23: Input the new valid data to 'Cancellation Reason' field");
		loansPage.inputCancellationReason(newValidText);
		
		log.info("newValidataDatatypes_05 - Step 24: Input the new Valid data to 'Foreign National' field");
		loansPage.selectForeignNational("No");
		
		log.info("newValidataDatatypes_05 - Step 25: Input the new Valid data to 'Loan Purpose' field");
		loansPage.selectLoanPurpose("Acquisition");
		
		log.info("newValidataDatatypes_05 - Step 26: Input the new Valid data to 'Refinance Type' field");
		loansPage.selectRefinanceType("Refi Equity");
		
		log.info("newValidataDatatypes_05 - Step 27: Input the new Valid data to 'Property Management' field");
		loansPage.selectPropertyManagement("Third Party");
		
		log.info("newValidataDatatypes_05 - Step 28: Input the newValid data to 'Company Type' field");
		loansPage.selectRecourse("No");
		
		log.info("newValidataDatatypes_05 - Step 29: Input the new Valid data to 'Bridge Borrower' field");
		loansPage.selectBridgeBorrower("No");
		
		log.info("newValidataDatatypes_05 - Step 30: Input the new Valid data to 'Company Type' field");
		loansPage.selectCompanyType(newCompanyType);
		
		log.info("newValidataDatatypes_05 - Step 31: Input the new Valid data to 'State' field");
		loansPage.selectState(newState);
		
		log.info("newValidataDatatypes_05 - Step 32: Input the new Valid data to 'Owner' field");
		loansPage.selectOwner("Dwell");
		
		log.info("newValidataDatatypes_05 - Step 33: Input the new Valid data to 'Source' field");
		loansPage.selectSource("Broker");
		
		log.info("newValidataDatatypes_05 - Step 34: Click 'Save' button");
		loansPage.clickSaveButton();
		
		//Check new data saved
		
		log.info("VP: Verify that 'Loan Name' is saved successfully");
		verifyTrue(loansPage.isLoanNameDisplayed(newLoanName));
		
		log.info("VP: Verify that 'Loan Status' is saved successfully");
		verifyTrue(loansPage.isLoanStatusDisplayedCorrectly(newLoanStatus));
		
		log.info("VP: Verify that Loan Amount Requested is saved successfully");
		verifyTrue(loansPage.isLoanAmountRequestedDisplayedCorrectly(newValidCurrency));
		
		log.info("VP: Verify that 'Term Sheet Issued Date ' field is saved successfully");
		verifyTrue(loansPage.isTermSheetIssuedDateDisplayedCorrectly(newValidDate));
		
		log.info("VP: Verify that'Term Sheet Signed Date is saved successfully ");
		verifyTrue(loansPage.isTermSheetSignedDateDisplayedCorrectly(newValidDate));
		
		log.info("VP: Verify that 'Anticipated IC Approval is saved successfully");
		verifyTrue(loansPage.isAnticipatedICApprovalDateDisplayedCorrectly(newValidDate));
		
		log.info("VP: Verify that 'Date Established is saved successfully ");
		verifyTrue(loansPage.isDateEstablishedDisplayedCorrectly(newValidDate));
		
		log.info("VP: Verify that 'Expected Close Date is saved successfully ");
		verifyTrue(loansPage.isExpectedCloseDateDisplayedCorrectly(newValidDate));
		
		log.info("VP: Verify that 'Kick Off Date is saved successfully ");
		verifyTrue(loansPage.isKickOffDateDisplayedCorrectly(newValidDate));
		
		log.info("VP: Verify that 'Phone' field is saved successfully");
		verifyTrue(loansPage.isPhoneTextboxDisplayedCorrectly(newValidPhoneNumber));
		
		log.info("VP: Verify that 'Zip' field is saved successfully");
		verifyTrue(loansPage.isZipcodeDisplayedCorrectly(newValidZipCode));
		
		log.info("VP: Verify that 'Email' field is saved successfully");
		verifyTrue(loansPage.isEmailTextboxDisplayedCorrectly(newValidEmail));
		
		log.info("VP: Verify that 'Floor' field is saved successfully");
		verifyTrue(loansPage.isFloorDisplayedCorrectly(newValidNumber));
		
		log.info("VP: Verify that 'Amortization Term' field is saved successfully");
		verifyTrue(loansPage.isAmortizationTermDisplayedCorrectly(newValidWholeNumber));
		
		log.info("VP: Verify that 'Loan Term' field is saved successfully");
		verifyTrue(loansPage.isLoanTermDisplayedCorrectly(newLoanTerm));
		
		log.info("VP: Verify that 'IO Term' field is saved successfully");
		verifyTrue(loansPage.isIOTermDisplayedCorrectly(newValidWholeNumber));
		
		log.info("VP: Verify that 'LTV' field is saved successfully");
		verifyTrue(loansPage.isLTVDisplayedCorrectly(newValidNumber));
		
		log.info("VP: Verify that 'Loan ID' field is saved successfully");
		verifyTrue(loansPage.isLoanIDDisplayed(newValidText+accountNumber));
		
		log.info("VP: Verify that 'Company website' field is saved successfully");
		verifyTrue(loansPage.isCompanyWebsiteDisplayedCorrectly(newValidText));
		
		log.info("VP: Verify that 'County' field is saved successfully");
		verifyTrue(loansPage.isCountyDisplayedCorrectly(newValidText));
		
		log.info("VP: Verify that 'Address' field is saved successfully");
		verifyTrue(loansPage.isAddressDisplayedCorrectly(newValidText));
		
		log.info("VP: Verify that 'City ' field is saved successfully");
		verifyTrue(loansPage.isCityDisplayedCorrectly(newValidText));
		
		log.info("VP: Verify that 'Cancellation Reason' field is saved successfully");
		verifyTrue(loansPage.isCancellationReasonDisplayedCorrectly(newValidText));
		
		log.info("VP: Verify that 'Foreign National' field is saved successfully");
		verifyTrue(loansPage.isForeignNationalDisplayedCorrectly("No"));
		
		log.info("VP: Verify that 'Loan Purpose' field is saved successfully");
		verifyTrue(loansPage.isLoanPurposeDisplayedCorrectly("Acquisition"));
		
		log.info("VP: Verify that 'Refinance Type' field is saved successfully");
		verifyTrue(loansPage.isRefinanceTypeDisplayedCorrectly("Refi Equity"));
		
		log.info("VP: Verify that 'Property Management' field is saved successfully");
		verifyTrue(loansPage.isPropertyManagementDisplayedCorrectly("Third Party"));
		
		log.info("VP: Verify that 'Company Type' field is saved successfully");
		verifyTrue(loansPage.isRecourseDisplayedCorrectly("No"));
		
		log.info("VP: Verify that 'Bridge Borrower' field is saved successfully");
		verifyTrue(loansPage.isBridgeBorrowerDisplayedCorrectly("No"));
		
		log.info("VP: Verify that 'Company Type' field is saved successfully");
		verifyTrue(loansPage.isCompanyTypeDisplayedCorrectly(newCompanyType));
		
		log.info("VP: Verify that 'State' field is saved successfully");
		verifyTrue(loansPage.isStateDisplayedCorrectly(newState));
		
		log.info("VP: Verify that 'Owner' field is saved successfully");
		verifyTrue(loansPage.isOwnerDisplayedCorrectly("Dwell"));
		
		log.info("VP: Verify that 'Source' field is saved successfully");
		verifyTrue(loansPage.isSourceDisplayedCorrectly("Broker"));
		
	}
	
	@Test(groups = { "regression" }, description = "Update field Loan Basic details 02 - Check history box")
	public void UpdateFieldLoanBasicDetails_02_CheckHistoryBox() {
		//Check all data saved correclty
		
		log.info("VP: Verify that 'Loan Name' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("LOAN_NAME_TEXTBOX_BASIC_DETAIL_HISTORY", loanName, newLoanName));
		
		log.info("VP: Verify that 'Loan ID' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("LOAN_APPLICATION_ID_HISTORY", validText+accountNumber, newValidText+accountNumber));
		
		log.info("VP: Verify that 'Loan Status' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("LOAN_STATUS_COMBOBOX_BASIC_DETAIL_HISTORY", loanStatus, newLoanStatus));
		
		log.info("VP: Verify that 'Term Sheet Issued Date' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("TERM_SHEET_ISSUED_DATE_TEXTBOX_HISTORY", validDate, newValidDate));
		
		log.info("VP: Verify that 'Term Sheet Signed Date' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("TERM_SHEET_SIGNED_DATE_TEXTBOX_HISTORY", validDate, newValidDate));
		
		log.info("VP: Verify that 'Company Website' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("COMPANY_WEBSITE_TEXTBOX_HISTORY", validText, newValidText));
		
		log.info("VP: Verify that 'Foreign National' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("FOREIGN_NATIONAL_DROPDOWN_HISTORY", "Yes", "No"));
		
		log.info("VP: Verify that 'Country' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("COUNTRY_TEXTBOX_HISTORY", validText, newValidText));
		
		log.info("VP: Verify that 'Loan Amount Requested' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("LOAN_AMOUNT_REQUESTED_TEXTBOX_HISTORY", validCurrency, newValidCurrency));
		
		log.info("VP: Verify that 'Amortization Term' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("AMORTIZATION_TERM_TEXTBOX_HISTORY", validWholeNumber, newValidWholeNumber));
		
		log.info("VP: Verify that 'Loan Term' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("LOAN_TERM_TEXTBOX_HISTORY", loanTerm.replace(" year", ""), newLoanTerm.replace(" year", "")));
		
		log.info("VP: Verify that 'IO Term' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("IO_TERM_TEXTBOX_HISTORY", validWholeNumber, newValidWholeNumber));
		
		log.info("VP: Verify that 'Loan Purpose' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("LOAN_PURPOSE_DROPDOWN_HISTORY", "Refi", "Acquisition"));
		
		log.info("VP: Verify that 'Refinance Type' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("REFINANCE_TYPE_DROPDOWN_HISTORY", "Refi Debt", "Refi Equity"));
		
		log.info("VP: Verify that 'Property Management ' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("PROPERTY_MANAGAMENT_DROPDOWN_HISTORY", "Self-Managed", "Third Party"));
		
		log.info("VP: Verify that 'Bridge Borrower' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("BRIDGE_BORROWER_DROPDOWN_HISTORY", "Yes", "No"));
		
		log.info("VP: Verify that 'Anticipated IC Approval' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("ANTICIPATED_IC_APPROVAL_DATE_TEXTBOX_HISTORY", validDate, newValidDate));
		
		log.info("VP: Verify that 'Company Type' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("COMPANY_TYPE_DROPDOWN_HISTORY", companyType, newCompanyType));
		
		log.info("VP: Verify that 'Date Established' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("DATE_ESTABLISHED_TEXTBOX_HISTORY", validDate, newValidDate));
		
		log.info("VP: Verify that 'Phone Textbox' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("PHONE_TEXTBOX_HISTORY", validPhoneNumber, newValidPhoneNumber));
		
		log.info("VP: Verify that 'Email' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("EMAIL_TEXTBOX_HISTORY", validEmail, newValidEmail));
		
		log.info("VP: Verify that 'Address' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("ADDRESS_TEXTBOX_HISTORY", validText, newValidText));
		
		log.info("VP: Verify that 'City' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("CITY_TEXTBOX_HISTORY", validText, newValidText));
		
		log.info("VP: Verify that 'State' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("STATE_DROPDOWN_HISTORY", "AA", "AE"));
		
		log.info("VP: Verify that 'Zipcode' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("ZIPCODE_TEXTBOX_HISTORY", validZipCode, newValidZipCode));
		
		log.info("VP: Verify that 'Source' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("SOURCE_DROPDOWN_HISTORY", "Bridge Borrower", "Broker"));
		
		log.info("VP: Verify that 'Owner' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("OWNER_DROPDOWN_HISTORY", "Dennis Spivey", "Dwell"));
		
		log.info("VP: Verify that 'Expected Close Date' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("EXPECTED_CLOSED_DATE_TEXTBOX_HISTORY", validDate, newValidDate));
		
		log.info("VP: Verify that 'Cancellation Reason' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("CANCELLATION_REASON_TEXTBOX_HISTORY", validText, newValidText));
	
		log.info("VP: Verify that 'Kick Off Date' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("KICK_OFF_DATE_TEXTBOX_HISTORY", validDate, newValidDate));
	
		log.info("VP: Verify that 'Recourse' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("RECOURSE_DROPDOWN_HISTORY", "Yes", "No"));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender;
	private String loanName, loanStatus, accountNumber, applicantName;
	private String state, companyType;
	private String validEmail, validCurrency, validDate, validPhoneNumber, validText, validZipCode, validWholeNumber, validNumber;
	private String newLoanName, newLoanStatus, newAccountNumber;
	private String newState, newCompanyType;
	private String newValidEmail, newValidCurrency, newValidDate, newValidPhoneNumber, newValidText, newValidZipCode, newValidWholeNumber, newValidNumber;
	private String loanTerm, newLoanTerm;
	}