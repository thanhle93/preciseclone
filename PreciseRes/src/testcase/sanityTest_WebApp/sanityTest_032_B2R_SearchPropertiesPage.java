package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Constant;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;

public class sanityTest_032_B2R_SearchPropertiesPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower New";
		propertiesFileName = "B2R Required.xlsx";
		propertyAddress = "37412 OAKHILL ST";
		propertySearchAddress = "37412 OAKHILL ST" +accountNumber;
		documentType1 = "P2 - Lease Agreement";
		documentFileName = "datatest.pdf";
	}

	@Test(groups = { "regression" }, description = "SearchPropertiesPage 01 - Search by Property Address")
	public void SearchPropertiesPage_01_SearchByPropertyAddress() {
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Precondition 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);

		log.info("Precondition 04. Change status of Loan");
		loansPage.selectLoanStatusBasicDetail("Term Sheet Sent");

		log.info("Precondition 05. Input Loan Amount Requested");
		loansPage.inputLoanAmountRequested("1000");

		log.info("Precondition 06. Click Save button");
		loansPage.clickSaveButton();

		log.info("Precondition 07. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Precondition 08. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("SearchPropertiesPage_01 - Step 01. Open Property Details page");
		propertiesPage = loansPage.openPropertyDetail(propertyAddress);
		
		log.info("SearchPropertiesPage_01 - Step 02. Input Address for searching");
		propertiesPage.inputAddress(propertySearchAddress);

		log.info("SearchPropertiesPage_01 - Step 03. Click 'View All' button");
		propertiesPage.clickSaveButton();
		
		log.info("SearchPropertiesPage_01 - Step 04. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);

		log.info("SearchPropertiesPage_01 - Step 03. Search by Loan name");
		propertiesPage.searchAddress(propertySearchAddress);

		city = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_City");
		state = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeState datacell_State");
		zip = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeZipCode datacell_Zip");
		applicant = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeForeignKey datacell_Borrower");
		loanIndex = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeForeignKey datacell_LoanApplication");
		dateModified = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeDate datacell_LastModifiedDate");
		assetID = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeInteger datacell_LoanPropertyID");
		active = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeBoolean datacell_IsActive");
		documents = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_DocumentsNew");
		reviewed = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_ReviewsNew");

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex, dateModified, assetID,
				active, documents, reviewed));
	}

	@Test(groups = { "regression" }, description = "SearchPropertiesPage 02 - Search by City")
	public void SearchPropertiesPage_02_SearchByCity() {
		
		log.info("SearchPropertiesPage_02 - Step 01. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchPropertiesPage_02 - Step 02. Search by City");
		propertiesPage.searchByCity(propertySearchAddress, city);

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex, dateModified, assetID,
				active, documents, reviewed));
	}
	
	@Test(groups = { "regression" }, description = "SearchPropertiesPage 03 - Search by State")
	public void SearchPropertiesPage_03_SearchByState() {
		
		log.info("SearchPropertiesPage_03 - Step 01. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchPropertiesPage_03 - Step 02. Search by State");
		propertiesPage.searchByState(propertySearchAddress, state);

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex, dateModified, assetID,
				active, documents, reviewed));
	}
	
	@Test(groups = { "regression" }, description = "SearchPropertiesPage 04 - Search by Zip")
	public void SearchPropertiesPage_04_SearchByZip() {
		
		log.info("SearchPropertiesPage_04 - Step 01. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchPropertiesPage_04 - Step 02. Search by Zip");
		propertiesPage.searchByZip(propertySearchAddress, zip);

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex, dateModified, assetID,
				active, documents, reviewed));
	}
	
	@Test(groups = { "regression" }, description = "SearchPropertiesPage 05 - Search by Applicant")
	public void SearchPropertiesPage_05_SearchByApplicant() {
		
		log.info("SearchPropertiesPage_05 - Step 01. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchPropertiesPage_05 - Step 02. Search by Applicant name");
		propertiesPage.searchByApplicant(propertySearchAddress, applicant);

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex, dateModified, assetID,
				active, documents, reviewed));
	}
	
	@Test(groups = { "regression" }, description = "SearchPropertiesPage 06 - Search by Loan Application")
	public void SearchPropertiesPage_06_SearchByLoanIndex() {
		
		log.info("SearchPropertiesPage_06 - Step 01. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchPropertiesPage_06 - Step 02. Search by Loan Application");
		propertiesPage.searchByLoanApplication(propertySearchAddress, loanIndex);

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex, dateModified, assetID,
				active, documents, reviewed));
	}
	
	@Test(groups = { "regression" }, description = "SearchPropertiesPage 07 - Search by Date Modified")
	public void SearchPropertiesPage_07_SearchByDateModified() {
		
		log.info("SearchPropertiesPage_07 - Step 01. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchPropertiesPage_07 - Step 02. Search by Loan name");
		propertiesPage.searchByDateModified(propertySearchAddress, dateModified);

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex, dateModified, assetID,
				active, documents, reviewed));
	}
	
	@Test(groups = { "regression" }, description = "SearchPropertiesPage 08 - Search by Asset ID")
	public void SearchPropertiesPage_08_SearchByAssetID() {
		
		log.info("SearchPropertiesPage_08 - Step 01. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchPropertiesPage_08 - Step 02. Search by Loan name");
		propertiesPage.searchByAssetID(propertySearchAddress, assetID);

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex, dateModified, assetID,
				active, documents, reviewed));
	}
	
	@Test(groups = { "regression" }, description = "SearchPropertiesPage 09 - Search by Active")
	public void SearchPropertiesPage_09_SearchByActive() {
		
		log.info("SearchPropertiesPage_09 - Step 01. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchPropertiesPage_09 - Step 02. Search by Active status");
		propertiesPage.selectActiveRadioButton("Yes");
		propertiesPage.searchAddress(propertySearchAddress);

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex, dateModified, assetID,
				active, documents, reviewed));
	}
	
	@Test(groups = { "regression" }, description = "SearchPropertiesPage 10 - Search by Missing documents")
	public void SearchPropertiesPage_10_SearchByMissingDocuments() {
		
		log.info("SearchPropertiesPage_10 - Step 01. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		//Check for Documents "0 of x" type
		
		log.info("SearchPropertiesPage_10 - Step 02. Search by Missing documents status");
		propertiesPage.selectMissingDocumentsRadioButton("Yes");
		propertiesPage.searchAddress(propertySearchAddress);
		
		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex, dateModified, assetID,
				active, documents, reviewed));
		
		//Check for Documents "No Documents" type
		
		log.info("SearchPropertiesPage_10 - Step 03. Open Properties detail page");
		propertiesPage.openPropertyDetail(propertySearchAddress);
		
		log.info("SearchPropertiesPage_10 - Step 04. Delete all empty placeholders");
		propertiesPage.deleteAllPlaceholders();
		
		log.info("SearchPropertiesPage_02 - Step 05. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchPropertiesPage_10 - Step 06. Search by Missing documents status");
		propertiesPage.selectMissingDocumentsRadioButton("No");
		propertiesPage.searchAddress(propertySearchAddress);
		
		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex, dateModified, assetID,
				active, "No Documents", "No Documents"));
		
		log.info("SearchPropertiesPage_10 - Step 07. Search by Missing documents status");
		propertiesPage.selectMissingDocumentsRadioButton("");
		propertiesPage.searchAddress(propertySearchAddress);
		
		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex, dateModified, assetID,
				active, "No Documents", "No Documents"));
		
		//Check for Documents "x of x" type
		
		log.info("SearchPropertiesPage_10 - Step 08. Open Property detail page");
		propertiesPage.openPropertyDetail(propertySearchAddress);
		
		log.info("SearchPropertiesPage_10 - Step 07. Click New document button");
		propertiesPage.clickNewDocumentButton();

		log.info("SearchPropertiesPage_10 - Step 09. Select document type");
		propertiesPage.selectDocumentType(documentType1);

		log.info("SearchPropertiesPage_10 - Step 10. Upload document file");
		propertiesPage.uploadDocumentFile(documentFileName);

		log.info("SearchPropertiesPage_10 - Step 11. Click 'Save' button");
		propertiesPage.clickSaveButton();

		log.info("SearchPropertiesPage_10 - Step 12. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchPropertiesPage_10 - Step 13. Search by Missing documents status");
		propertiesPage.selectMissingDocumentsRadioButton("No");
		propertiesPage.searchAddress(propertySearchAddress);
		
		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex, dateModified, assetID,
				active, "1 of 1", "0 of 1"));
	}
	
	@Test(groups = { "regression" }, description = "SearchPropertiesPage 10 - Search by Reviewed documents")
	public void SearchPropertiesPage_11_SearchByReviewedDocuments() {
		
		log.info("SearchPropertiesPage_11 - Step 01. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		//Check for Documents "0 of x" type
		
		log.info("SearchPropertiesPage_11 - Step 02. Search by Missing documents status");
		propertiesPage.selectReviewedDocumentsRadioButton("No");
		propertiesPage.searchAddress(propertySearchAddress);
		
		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex, dateModified, assetID,
				active, "1 of 1", "0 of 1"));
		
		//Check for Documents "x of x" type
		
		log.info("SearchPropertiesPage_11 - Step 03. Open Properties detail page");
		propertiesPage.openPropertyDetail(propertySearchAddress);
		
		log.info("SearchPropertiesPage_11 - Step 04. Open Properties document type");
		propertiesPage.openPropertyDocumentType(documentType1);
		
		log.info("SearchPropertiesPage_11 - Step 05. Click on Mark as Reviewed button");
		propertiesPage.clickOnMarkAsReviewedButton();
		
		log.info("SearchPropertiesPage_11 - Step 06. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchPropertiesPage_11 - Step 07. Search by Missing documents status");
		propertiesPage.selectReviewedDocumentsRadioButton("Yes");
		propertiesPage.searchAddress(propertySearchAddress);
		
		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex, dateModified, assetID,
				active, "1 of 1", "1 of 1"));
		
		log.info("SearchPropertiesPage_11 - Step 08. Search by Missing documents status");
		propertiesPage.selectReviewedDocumentsRadioButton("");
		propertiesPage.searchAddress(propertySearchAddress);
		
		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex, dateModified, assetID,
				active, "1 of 1", "1 of 1"));
		
		//Check for Documents "No documents" type
		
		log.info("SearchPropertiesPage_11 - Step 09. Open Property detail page");
		propertiesPage.openPropertyDetail(propertySearchAddress);
		
		log.info("SearchPropertiesPage_11 - Step 10. Delete all empty placeholders");
		propertiesPage.deleteAllPlaceholders();

		log.info("SearchPropertiesPage_11 - Step 11. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchPropertiesPage_11 - Step 12. Search by Missing documents status");
		propertiesPage.selectReviewedDocumentsRadioButton("No");
		propertiesPage.searchAddress(propertySearchAddress);
		
		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex, dateModified, assetID,
				active, "No Documents", "No Documents"));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private String usernameLender, passwordLender;
	private String loanName, accountNumber, applicantName, propertiesFileName, loanStatus, propertyAddress, propertySearchAddress;
	private String city, state, zip, applicant, loanIndex, dateModified, assetID, active, documents, reviewed;
	private String documentType1, documentFileName;
}