package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Common;
import common.Constant;

public class sanityTest_005_FNF_UpdateApplicantValidAllFields extends
		AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		day = Common.getCommon().getCurrentDayOfWeek();
		month = Common.getCommon().getCurrentMonthOfWeek();
		year = Common.getCommon().getCurrentYearOfWeek();
		street = "P.O. Box 990, 8268 Morbi Avenue";
		phone = "(433) 792-6263";
		fax = "(433) 792-6264";
		city = "Birmingham";
		zip = "35695";
		state = "AL - Alabama";
		corporateStructure = "Limited Liability Company";
		netIncome = "$50,000.00";
		companySite = "http://preciseres.com";
		stateOfIncorporation = "CA - California";
		borrowerFirstName = "FNF Borr " + getUniqueNumber();
		borrowerLastName = "FNF Borr " + getUniqueNumber();
		borrowerPhone = "8284299006";
		borrowerCell = "8284299007";
		borrowerEmail = "minhdam06@gmail.com";
		borrowerStreet = "100-7137 Facilisis, Rd";
		borrowerCity = "Baltimore";
		borrowerState = "MD - Maryland";
		borrowerZip = "85341";
	}

	@Test(groups = { "regression" }, description = "Applicant 07 - Login as the applicant and input valid all fields in this applicant screen and verify that are stored correctly'")
	public void Applicant_13_Lender_InputValidAllFields() {
		log.info("Applicant_24 - Step 01: Login with valid username password");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);

		log.info("Applicant_24 - Step 02: Go to 'Applicant' page");
		borrowersPage = homePage.openBorrowersPage(driver, ipClient);
		
		log.info("Applicant_24 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		borrowersPage.clickOnSearchButton(companyName);
		
		log.info("Applicant_24 - Step 04: Click 'Applicant Name'");
		borrowersPage.clickOnApplicantName();

		log.info("Applicant_24 - Step 05: Input valid  'Company/ Borrower Name, Address of Entity/ Borrower, Website'");
		borrowersPage.inputValidCompanyNameAddressSiteCity(companyName, street, city, companySite, borrowerFirstName, borrowerLastName, borrowerStreet, borrowerCity);

		log.info("Applicant_24 - Step 06: Input valid  'Company/ Borrower State, Corporate Structure, Originator, State of Incorporation/Formation'");
		borrowersPage.inputValidAllComboboxFields(state, corporateStructure, stateOfIncorporation, borrowerState);
		
		log.info("Applicant_24 - Step 07: Input valid 'Phone, Fax, Primary Phone/ Cell/ Fax'");
		borrowersPage.inputPhoneCellFax(phone, fax, borrowerPhone, borrowerCell, fax);
		
		log.info("Applicant_24 - Step 08: Input valid 'Email, Zip code, Borrower Email/ Zip code'");
		borrowersPage.inputEmailZipCode(zip, borrowerEmail, borrowerZip);
		
		log.info("Applicant_24 - Step 09: Input valid 'Net Income'");
		borrowersPage.inputCurrency(netIncome);
		
		log.info("Applicant_24 - Step 10: Input valid 'Date of Formation, Date of Fiancials'");
		borrowersPage.inputDate(month+"/"+day+"/"+year, month+"/"+day+"/"+year);
		
		log.info("Applicant_24 - Step 11: Click on 'Save' button");
		borrowersPage.clickOnSaveButton();

		log.info("VP: Record Saved message displays");
		verifyTrue(borrowersPage.isRecordSavedMessageDisplays());

		log.info("VP: Verify that 'Company/ Borrower Name, Address of Entity/ Borrower, Website' is changed successfully");
		verifyTrue(borrowersPage.isCompanyNameAddressSiteCitySaved(companyName, street, city, companySite, borrowerFirstName, borrowerLastName, borrowerStreet, borrowerCity));
		
		log.info("VP: Verify that 'Company/ Borrower State, Corporate Structure, State of Incorporation/Formation' is changed successfully");
		verifyTrue(borrowersPage.isAllComboboxFieldsSaved(state, corporateStructure, stateOfIncorporation, borrowerState));
		
		log.info("VP: Verify that 'Phone, Fax, BorrowerPhone/ Cell/ Fax' is changed successfully");
		verifyTrue(borrowersPage.isPhoneCellFaxSaved(phone, fax, borrowerPhone, borrowerCell, fax));
		
		log.info("VP: Verify that 'Email, Zip code, Borrower Email/ Zip code' is changed successfully");
		verifyTrue(borrowersPage.isEmailZipCodeSaved(zip, borrowerEmail, borrowerZip));
		
		log.info("VP: Verify that 'Net Income' is changed successfully");
		verifyTrue(borrowersPage.isNetIncomeSaved(netIncome));
		
		log.info("VP: Verify that 'Date of Formation, Date of Fiancials, Deposit Date Received, Fees Paid Date, Date Of Birth' is changed successfully");
		verifyTrue(borrowersPage.isDateSaved(month+"/"+day+"/"+year, month+"/"+day+"/"+year));
	}
	
	@Test(groups = { "regression" }, description = "Applicant 08 - Login as Applicant and make sure he can see them also")
	public void Applicant_14_Borrower_MakeSureHeCanSeeThemAlso() {
		
		log.info("Applicant_25 - Step 01: Login with Applicant");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(FNFBorrowerUsername,FNFBorrowerPassword, false);

		log.info("Applicant_25 - Step 02: Go to 'My Information' page");
		borrowersPage = homePage.openFNFMyInformationPage(driver, ipClient);
	
		log.info("VP: Verify that 'Company/ Borrower Name, Address of Entity/ Borrower, Website' is changed successfully");
		verifyTrue(borrowersPage.isCompanyNameAddressSiteCitySaved(companyName, street, city, companySite, borrowerFirstName, borrowerLastName, borrowerStreet, borrowerCity));
		
		log.info("VP: Verify that 'Company/ Borrower State, Corporate Structure, State of Incorporation/Formation' is changed successfully");
		verifyTrue(borrowersPage.isAllComboboxFieldsSaved(state, corporateStructure, stateOfIncorporation, borrowerState));
		
		log.info("VP: Verify that 'Phone, Fax, BorrowerPhone/ Cell/ Fax' is changed successfully");
		verifyTrue(borrowersPage.isPhoneCellFaxSaved(phone, fax, borrowerPhone, borrowerCell, fax));

		log.info("VP: Verify that 'Email, Zip code, Borrower Email/ Zip code' is changed successfully");
		verifyTrue(borrowersPage.isEmailZipCodeSaved(zip, borrowerEmail, borrowerZip));
		
		log.info("VP: Verify that 'Net Income' is changed successfully");
		verifyTrue(borrowersPage.isNetIncomeSaved(netIncome));
		
		log.info("VP: Verify that 'Date of Formation, Date of Fiancials, Deposit Date Received, Fees Paid Date, Date Of Birth' is changed successfully");
		verifyTrue(borrowersPage.isDateSaved(month+"/"+day+"/"+year, month+"/"+day+"/"+year));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage borrowersPage;
	private String FNFLenderUsername, FNFLenderPassword;
	private String FNFBorrowerUsername, FNFBorrowerPassword;
	private String companyName, street, phone, fax, city, zip, state;
	private String corporateStructure, netIncome, companySite;
	private String stateOfIncorporation;
	private String accountNumber;
	private String borrowerFirstName, borrowerLastName, borrowerPhone,borrowerCell, borrowerEmail;
	private String borrowerStreet, borrowerCity, borrowerState, borrowerZip;
	private int day, month, year;
}