package common;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.Stack;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.openqa.selenium.WebDriver;
import org.sikuli.script.Screen;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.jacob.com.LibraryLoader;
import com.sun.jna.Native;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;

import au.com.bytecode.opencsv.CSVReader;
import autoitx4java.AutoItX;
import config.ProviderConfiguration;


public class Common {

	public static synchronized Common getCommon() {
		if (instance == null) {
			instance = new Common();
		}
		return instance;
	}

	/**
	 * Get day of week
	 * 
	 * @param day
	 * @param month
	 * @param year
	 * @return
	 * @throws ParseException
	 */
	public String getDayOfWeek(int day, int month, int year) {
		try {
			String strDate = day + "/" + month + "/" + year;
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			Date dt = format.parse(strDate);
			DateFormat dateFormat = new SimpleDateFormat("EEEE");
			return dateFormat.format(dt);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return null;
		}
	}

	/**
	 * Convert rgba to hex
	 * 
	 * @param r
	 * @param g
	 * @param b
	 * @return
	 */
	public String toBrowserHexValue(int number) {
		StringBuilder builder = new StringBuilder(Integer.toHexString(number & 0xff));
		while (builder.length() < 2) {
			builder.append("0");
		}
		return builder.toString().toUpperCase();
	}

	/**
	 * Convert rgba to hex
	 * 
	 * @param r
	 * @param g
	 * @param b
	 * @return
	 */
	public String toHex(int r, int g, int b) {
		return "#" + toBrowserHexValue(r) + toBrowserHexValue(g) + toBrowserHexValue(b);
	}

	/**
	 * Comverton month
	 * 
	 * @param month
	 * @return
	 */
	public String convertMonth(int month) {
		String m = null;
		switch (month) {
		case 1:
			m = "January";
			break;
		case 2:
			m = "February";
			break;
		case 3:
			m = "March";
			break;
		case 4:
			m = "April";
			break;
		case 5:
			m = "May";
			break;
		case 6:
			m = "June";
			break;
		case 7:
			m = "July";
			break;
		case 8:
			m = "August";
			break;
		case 9:
			m = "September";
			break;
		case 10:
			m = "October";
			break;
		case 11:
			m = "November";
			break;
		case 12:
			m = "December";
			break;
		default:
			break;

		}
		return m;
	}

	/**
	 * convert to month
	 * 
	 * @param monthName
	 * @return month
	 */
	public int convertMonth(String monthName) {
		int m = 0;
		if (monthName.toUpperCase().contains("JAN"))
			m = 1;
		else if (monthName.toUpperCase().contains("FEB"))
			m = 2;
		else if (monthName.toUpperCase().contains("MAR"))
			m = 3;
		else if (monthName.toUpperCase().contains("APR"))
			m = 4;
		else if (monthName.toUpperCase().contains("MAY"))
			m = 5;
		else if (monthName.toUpperCase().contains("JUN"))
			m = 6;
		else if (monthName.toUpperCase().contains("JUL"))
			m = 7;
		else if (monthName.toUpperCase().contains("AUG"))
			m = 8;
		else if (monthName.toUpperCase().contains("SEP"))
			m = 9;
		else if (monthName.toUpperCase().contains("OCT"))
			m = 10;
		else if (monthName.toUpperCase().contains("NOV"))
			m = 11;
		else if (monthName.toUpperCase().contains("DEC"))
			m = 12;
		return m;
	}

	/**
	 * get current browser
	 * 
	 * @return driver
	 */
	public String getCurrentBrowser() {
		ProviderConfiguration provider = ProviderConfiguration.getProvider();
		provider.loadInstance("PreciseRes");
		String driverClass = ProviderConfiguration.getProvider().getSelenium().getDriver();
		return driverClass;
	}

	/**
	 * get current day with plus day
	 * 
	 * @param days
	 * @return day
	 */
	public int getCurrentDayWithPlusDays(int days) {
		DateTime dt = DateTime.now();
		return dt.plusDays(days).getDayOfMonth();
	}

	/**
	 * get month when current day plus days
	 * 
	 * @param days
	 * @return
	 */
	public int getCurrentMonthWithPlusDays(int days) {
		DateTime dt = DateTime.now();
		return dt.plusDays(days).getMonthOfYear();
	}

	/**
	 * get year when current day plus days
	 * 
	 * @param days
	 * @return
	 */
	public int getCurrentYearWithPlusDays(int days) {
		DateTime dt = DateTime.now();
		return dt.plusDays(days).getYear();
	}

	/**
	 * get current day
	 * 
	 * @return
	 */
	public int getCurrentDay() {
		DateTime now = DateTime.now();
		return now.getDayOfMonth();
	}

	/**
	 * get current month
	 * 
	 * @return
	 */
	public int getCurrentMonth() {
		DateTime now = DateTime.now();
		return now.getMonthOfYear();
	}

	/**
	 * get current year
	 * 
	 * @return
	 */
	public int getCurrentYear() {
		DateTime now = DateTime.now();
		return now.getYear();
	}

	/**
	 * get current hour
	 * 
	 * @return
	 */
	public int getCurrentHours() {
		DateTime now = DateTime.now();
		return now.getHourOfDay();
	}

	/**
	 * get current minute
	 * 
	 * @return
	 */
	public int getCurrentMinutes() {
		DateTime now = DateTime.now();
		return now.getMinuteOfHour();
	}

	/**
	 * get logout link
	 * 
	 * @return
	 */
	public String getLogoutLink() {
		return logOutLink;
	}

	/**
	 * set logout link
	 * 
	 * @param url
	 */
	public void setLogoutLink(String url) {
		this.logOutLink = url;
	}

	/**
	 * set aut url
	 * 
	 * @param url
	 */
	public void setUrl(String url) {
		this.autLink = url;
	}

	/**
	 * get logout link
	 * 
	 * @return
	 */
	public String getUrl() {
		return autLink;
	}

	/**
	 * get en on mac os
	 * 
	 * @return
	 */
	public String getEnOnMac() {
		try {
			Runtime runtime = Runtime.getRuntime();
			Process process = runtime.exec("ifconfig -u -l");
			InputStream stdout = process.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(stdout));
			String line;
			while ((line = reader.readLine()) != null) {
				int index = line.indexOf("en");
				if (index > 0) {
					en = line.substring(index, 7);
					break;
				}
			}
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
		return en;
	}

	/**
	 * disconnect network on mac
	 */
	public void disconnectNetWorkOnMac() {
		String output = "";
		if (en == "") {
			en = getEnOnMac();
		}
		try {
			String cmd = "sudo ifconfig " + en + " down";
			Runtime runtime = Runtime.getRuntime();
			Process process = runtime.exec(cmd);
			process.waitFor();
			InputStream stdout = process.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(stdout));
			String line;
			while ((line = reader.readLine()) != null) {
				output = output + line;
			}
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}

	/**
	 * re-connect network on mac
	 */
	public void reconnectNetWorkOnMac() {
		String output = "";
		try {
			String cmd = "sudo ifconfig " + en + " up";
			Runtime runtime = Runtime.getRuntime();
			Process process = runtime.exec(cmd);
			InputStream stdout = process.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(stdout));
			String line;
			while ((line = reader.readLine()) != null) {
				output = output + line;
			}
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}

	/**
	 * close a new window when click on link with target is _blank
	 * 
	 * @param driver
	 */
	public void closeNewWindow(WebDriver driver) {
		String currentHandle = driver.getWindowHandle();
		openWindowHandles.push(driver.getWindowHandle());
		Set<String> newHandles = driver.getWindowHandles();
		newHandles.removeAll(openWindowHandles);
		String handle = newHandles.iterator().next();
		driver.switchTo().window(handle);
		driver.close();
		driver.switchTo().window(currentHandle);
	}

	/**
	 * Define User32
	 *
	 */
	public interface User32 extends StdCallLibrary {
		User32 INSTANCE = (User32) Native.loadLibrary("user32", User32.class, W32APIOptions.DEFAULT_OPTIONS);

		HWND FindWindow(String lpClassName, String lpWindowName);

		HWND SetForegroundWindow(HWND hWnd);

		HWND ShowWindow(HWND hWnd, int nCmdShow);

		HWND SetFocus(HWND hwnd);
	}

	/**
	 * Use User32 to focus window
	 * 
	 * @param className
	 * @param windowName
	 */
	public void focusWindow(String className, String windowName) {
		HWND hWnd = User32.INSTANCE.FindWindow(className, windowName);
		if (hWnd != null) {
			User32.INSTANCE.ShowWindow(hWnd, 10);
			User32.INSTANCE.SetForegroundWindow(hWnd);
			User32.INSTANCE.SetFocus(hWnd);
		} else {
			System.out.println("Can not find window with class: " + className + " window name: " + windowName);
		}
	}

	/**
	 * Comverton month
	 * 
	 * @param month
	 * @return short name month
	 */
	public String convertShortMonth(int month) {
		String m = null;
		switch (month) {
		case 1:
			m = "Jan";
			break;
		case 2:
			m = "Feb";
			break;
		case 3:
			m = "Mar";
			break;
		case 4:
			m = "Apr";
			break;
		case 5:
			m = "May";
			break;
		case 6:
			m = "Jun";
			break;
		case 7:
			m = "Jul";
			break;
		case 8:
			m = "Aug";
			break;
		case 9:
			m = "Sep";
			break;
		case 10:
			m = "Oct";
			break;
		case 11:
			m = "Nov";
			break;
		case 12:
			m = "Dec";
			break;
		default:
			break;

		}
		return m;
	}

	/**
	 * init autoIT
	 */
	public AutoItX initAutoIT() {
		String tmp = System.getProperty("os.arch");
		File file;
		if (tmp.contains("64"))
			file = new File("lib", "jacob-1.18-M2-x64.dll");
		else
			file = new File("lib", "jacob-1.18-M2-x86.dll");
		System.setProperty(LibraryLoader.JACOB_DLL_PATH, file.getAbsolutePath());

		AutoItX autoIT = new AutoItX();
		return autoIT;
	}

	/**
	 * open file to upload using autoitx lib
	 * @param fileName
	 */
	public void openFileForUpload(WebDriver driver, String fileName) {
		try {
			AutoItX autoIT = initAutoIT();
			String title;
			String path = getPathFile(Constant.PRESICERES_DATA_PATH + fileName);
			if (driver.toString().contains("firefox"))
				title = "File Upload";
			else if (driver.toString().contains("chrome"))
				title = "Open";
			else
				title = "Choose File to Upload";
			autoIT.winWaitActive(title, "", 5);
			if (autoIT.winExists(title)) {
				autoIT.winActivate(title);
				autoIT.controlClick(title, "", "[CLASS:Edit; INSTANCE:1]");
				Thread.sleep(3000);
				autoIT.controlSend(title, "", "[CLASS:Edit; INSTANCE:1]", path);
				Thread.sleep(2000);
				autoIT.controlClick(title, "", "[CLASS:Button; INSTANCE:1]");
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (autoIT.winExists(title)) {
					autoIT.controlClick(title, "", "[CLASS:Button; INSTANCE:1]");
					autoIT.controlClick(title, "", "[CLASS:Button; INSTANCE:2]");
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * open file to upload using autoitx lib
	 * @param fileName
	 */
	public void openFileForUpload(WebDriver driver, String fileName1, String fileName2, String fileName3) {
		try {
			AutoItX autoIT = initAutoIT();
			String title;
			String path ="\"" + getPathFile(Constant.PRESICERES_DATA_PATH + fileName1) + "\" \"" +getPathFile(Constant.PRESICERES_DATA_PATH + fileName2)+"\"" + " \"" +getPathFile(Constant.PRESICERES_DATA_PATH + fileName3)+"\"";
			if (driver.toString().contains("firefox"))
				title = "File Upload";
			else if (driver.toString().contains("chrome"))
				title = "Open";
			else
				title = "Choose File to Upload";
			autoIT.winWaitActive(title, "", 5);
			if (autoIT.winExists(title)) {
				autoIT.winActivate(title);
				autoIT.controlClick(title, "", "[CLASS:Edit; INSTANCE:1]");
				Thread.sleep(3000);
				autoIT.controlSend(title, "", "[CLASS:Edit; INSTANCE:1]", path);
				Thread.sleep(2000);
				autoIT.controlClick(title, "", "[CLASS:Button; INSTANCE:1]");
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (autoIT.winExists(title)) {
					autoIT.controlClick(title, "", "[CLASS:Button; INSTANCE:1]");
					autoIT.controlClick(title, "", "[CLASS:Button; INSTANCE:2]");
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * get full path of file
	 */
	public String getPathFile(String fileName) {
		File file = new File(fileName);
		return file.getAbsolutePath();
	}

	/**
	 * check file is existed
	 * 
	 * @param file
	 * @return true/false
	 */
	public boolean isFileExists(String file) {
		try {
			String pathFolderDownload = getPathContainDownload();
			File files = new File(pathFolderDownload + file);
			boolean exists = files.exists();
			return exists;
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return false;
		}
	}

	/**
	 * check file is contains
	 * 
	 * @param file
	 * @return true/false
	 */
	public boolean isFileContains(String fileName) {
		try {
			boolean flag = false;
			String pathFolderDownload = getPathContainDownload();
			File dir = new File(pathFolderDownload);
			File[] files = dir.listFiles();
			if (files == null || files.length == 0) {
				flag = false;
			}
			for (int i = 1; i < files.length; i++) {
				if (files[i].getName().contains(fileName)) {
					flag = true;
				}
			}
			return flag;
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return false;
		}
	}

	/**
	 * get path contain download file
	 * 
	 * @return path
	 */
	public String getPathContainDownload() {
		String path = "";
		String machine_name;
		machine_name = System.getProperty("user.home");
		path = String.format(Constant.FOLDER_DOWNLOAD_ONE_WIN, machine_name);
		return path;
	}

	/**
	 * Delete a file from local PC
	 * 
	 * @param fileName
	 */
	public void deleteFileFullName(String fileName) {
		try {
			if (isFileExists(fileName)) {
				String pathFolderDownload = getPathContainDownload();
				File files = new File(pathFolderDownload + fileName);
				files.delete();
			}
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}

	/**
	 * Delete file contains name in folder from local PC
	 * 
	 * @param fileName
	 */
	public void deleteFileContainsName(String fileName) {
		try {
			String files;
			String pathFolderDownload = getPathContainDownload();
			File file = new File(pathFolderDownload);
			File[] listOfFiles = file.listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					files = listOfFiles[i].getName();
					if (files.contains(fileName)) {
						new File(listOfFiles[i].toString()).delete();
					}
				}
			}
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}

	/**
	 * Delete all files in folder from local PC
	 */
	public void deleteAllFileInFolder() {
		try {
			String pathFolderDownload = getPathContainDownload();
			File file = new File(pathFolderDownload);
			File[] listOfFiles = file.listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					new File(listOfFiles[i].toString()).delete();
				}
			}
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}

	/**
	 * Count files in a directory (including files in all subdirectories)
	 * 
	 * @param directory
	 *            the directory to start in
	 * @return the total number of files
	 * @throws UnknownHostException
	 */
	public int countFilesInDirectory() {
		String pathFolderDownload = getPathContainDownload();
		File file = new File(pathFolderDownload);
		int i = 0;
		for (File listOfFiles : file.listFiles()) {
			if (listOfFiles.isFile()) {
				i++;
			}
		}
		return i;
	}

	/**
	 * Get filename in directory
	 */
	public String getFileNameInDirectory() {
		String pathFolderDownload = getPathContainDownload();
		File file = new File(pathFolderDownload);
		File[] listFile = file.listFiles();
		String fileName = null;
		for (int i = 0; i < listFile.length; i++) {
			if (listFile[i].isFile()) {
				fileName = listFile[i].getName();
			}
		}
		return fileName;
	}

	/**
	 * Delete a file from local PC
	 * 
	 * @param file
	 */
	public void waitForFileExist(String file, int times) {
		try {
			boolean isDownloaded = isFileExists(file);
			int i = 0;
			while (isDownloaded == false && i <= times) {
				Thread.sleep(1000);
				i++;
				isDownloaded = isFileExists(file);
			}
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}

	
	/**
	 * get current day
	 * 
	 * @return
	 */
	public int getCurrentDayOfWeek() {
		DateTime now = DateTime.now();

		String dateOfWeek = Common.getCommon().getDayOfWeek(now.getDayOfMonth(), now.getMonthOfYear(), now.getYear());
		// check year
		int dayminus = 0;
		if (dateOfWeek.toLowerCase().equals("saturday")) {
			dayminus = 1;
		} else if (dateOfWeek.toLowerCase().equals("sunday")) {
			dayminus = 2;
		}

		return getCurrentDayWithMinusDays(dayminus);
	}

	/**
	 * get current month
	 * 
	 * @return
	 */
	public int getCurrentMonthOfWeek() {
		DateTime now = DateTime.now();
		String dateOfWeek = Common.getCommon().getDayOfWeek(now.getDayOfMonth(), now.getMonthOfYear(), now.getYear());
		// check year
		int dayminus = 0;
		if (dateOfWeek.toLowerCase().equals("saturday")) {
			dayminus = 1;
		} else if (dateOfWeek.toLowerCase().equals("sunday")) {
			dayminus = 2;
		}
		return getCurrentMonthWitMinusDays(dayminus);
	}

	/**
	 * get current year
	 * 
	 * @return
	 */
	public int getCurrentYearOfWeek() {
		DateTime now = DateTime.now();
		String dateOfWeek = Common.getCommon().getDayOfWeek(now.getDayOfMonth(), now.getMonthOfYear(), now.getYear());
		// check year
		int dayminus = 0;
		if (dateOfWeek.toLowerCase().equals("saturday")) {
			dayminus = 1;
		} else if (dateOfWeek.toLowerCase().equals("sunday")) {
			dayminus = 2;
		}

		return getCurrentYearWithMinusDays(dayminus);
	}

	/**
	 * get current day with plus day
	 * 
	 * @param days
	 * @return day
	 */
	public int getCurrentDayWithMinusDays(int days) {
		DateTime dt = DateTime.now();
		return dt.minusDays(days).getDayOfMonth();
	}

	/**
	 * get month when current day plus days
	 * 
	 * @param days
	 * @return
	 */
	public int getCurrentMonthWitMinusDays(int days) {
		DateTime dt = DateTime.now();
		return dt.minusDays(days).getMonthOfYear();
	}

	/**
	 * get year when current day plus days
	 * 
	 * @param days
	 * @return
	 */
	public int getCurrentYearWithMinusDays(int days) {
		DateTime dt = DateTime.now();
		return dt.minusDays(days).getYear();
	}

	private final Stack<String> openWindowHandles = new Stack<String>();

	private Common() {
	}
	
	/**
	 * Open the Excel file
	 * 
	 * @param sheetName
	 * @throws Exception
	 */
	public void setExcelFile(String sheetName) {
		try {
			String fileName = getFileNameInDirectory();
			System.out.println(fileName);
			String pathFolderDownload = getPathContainDownload();
			System.out.println(pathFolderDownload);
			FileInputStream excelFile = new FileInputStream(pathFolderDownload + fileName);
			System.out.println(pathFolderDownload + fileName);
			excelBook = new XSSFWorkbook(excelFile);
			excelSheet = excelBook.getSheet(sheetName);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}

	/**
	 * Get data in excel file
	 * @param rowNum
	 * @param colNum
	 * @return
	 */
	public String getCellData(int rowNum, int colNum) {
		try {
			cell = excelSheet.getRow(rowNum).getCell(colNum);
			String CellData = cell.getStringCellValue();
			return CellData;
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * Write data in excel file
	 * @param value
	 * @param rowNum
	 * @param colNum
	 */
	public void setCellData(String value, int rowNum, int colNum) {
		try {
			String fileName = getFileNameInDirectory();
			String pathFolderDownload = getPathContainDownload();
			row = excelSheet.getRow(rowNum);
			cell = row.getCell(colNum, row.RETURN_BLANK_AS_NULL);
			if (cell == null) {
				cell = row.createCell(colNum);
				cell.setCellValue(value);
			} else {
				cell.setCellValue(value);
			}
			FileOutputStream fileOut = new FileOutputStream(pathFolderDownload + fileName);
			excelBook.write(fileOut);
			fileOut.flush();
			fileOut.close();
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}
	
	public Boolean isTextDisplayedInCSVFile(String csvFileName, String text){
		try {
		String pathFolderDownload = getPathContainDownload();

		CSVReader reader = new CSVReader(new FileReader(pathFolderDownload+"/"+csvFileName));
		 
		 // this will load content into list
		  List<String[]> li=reader.readAll();
		  reader.close();
		            	
		 // create Iterator reference
		  Iterator<String[]>i1= li.iterator();
		    
		 // Iterate all values 
		 while(i1.hasNext()){
		     
		 String[] str=i1.next();
		 
		 for(int i=0;i<str.length;i++)
		 {
			System.out.println(str[i]);	
			if(str[i].equals(text)){
				System.out.print("Index: "+ i);	
				return true;
			}
		 }
		 }
		 return false;
		     
		 }
		catch (Exception e) {
			System.out.print(e.getMessage());
			return false;
		}
		
	}
	
	public Boolean isTextDisplayedInCSVFile(String csvFileName, String text, String collumnName){
		try {
		String pathFolderDownload = getPathContainDownload();
		int collumnIndex = 100;

		CSVReader reader = new CSVReader(new FileReader(pathFolderDownload+"/"+csvFileName));

		// this will load content into list
		  List<String[]> li=reader.readAll();
		  reader.close();

		  // create Iterator reference
		  Iterator<String[]>i1= li.iterator();

		  // Iterate all values 
		  while(i1.hasNext() && (collumnIndex==100)){
		     
		 	String[] str=i1.next();
		 
		 	for(int i=0;i<str.length;i++)
		 	{
		 		if(str[i].equals(collumnName))
		 		{
				collumnIndex = i;
				break;
		 		}
		 	}
		 }
		 
		 if (collumnIndex==100){
			 System.out.println("Failed. Can't find Collumn");
			 return false;
		 }
		 while(i1.hasNext()){
			 	String[] str = i1.next();
			 	if(str[collumnIndex].equals(text))
				return true;
		}
		 return false;
		 }
		catch (Exception e) {
			System.out.print(e.getMessage());
			return false;
		}
	}
	/**
	 * Check record exported correctly
	 * @param csvFileName
	 * @param uniqueText
	 * @param collumnName
	 * @param numberOfRecords
	 * @param values
	 * @return
	 */
	public Boolean checkRecordExportedCorrectly(String csvFileName, String uniqueText, String collumnName, int numberOfRecords, String[] values){
		try {
		String pathFolderDownload = getPathContainDownload();
		int collumnIndex = 100;
		
		CSVReader reader = new CSVReader(new FileReader(pathFolderDownload+"/"+csvFileName));

		// this will load content into list
		  List<String[]> li=reader.readAll();
		  if(numberOfRecords!=(li.size()-3)){
			  System.out.println("Missing records!!! Found only: " +(li.size()-3)+"/"+numberOfRecords);
			  reader.close();
			  return false;
		  }
		  
		  reader.close();

		  // create Iterator reference
		  Iterator<String[]>i1= li.iterator();

		  // Iterate all values 
		  while(i1.hasNext() && (collumnIndex==100)){
		     
		 	String[] str=i1.next();
		 
		 	for(int i=0;i<str.length;i++)
		 	{
		 		if(str[i].equals(collumnName))
		 		{
				collumnIndex = i;
				break;
		 		}
		 	}
		 }
		 
		 if (collumnIndex==100){
			 System.out.println("Failed. Can't find Collumn");
			 return false;
		 }
		 int contain = 0;
		 while(i1.hasNext()){
			 	String[] str = i1.next();
			 	if(str[collumnIndex].equals(uniqueText)){
			 		for (String s:values){
			 			contain=0;
			 			if(s.isEmpty() || s.equals("null"))
			 				contain=1;
			 			else for(int i=0;i<str.length;i++)
					 	{
//			 				System.out.println("Records value exported correctly: " +str[i]+" .Expected: "+s);
					 		if(str[i].trim().startsWith(s.trim()) || s.trim().startsWith(str[i].trim()))
					 		{
					 			System.out.println("Records value exported correctly: " +s+" .Expected: "+str[i]);
					 			contain=1;	
					 			break;
					 		}
					 	}
			 			 
			 			if(contain==0) {
			 				System.out.println("Failed. Record value isn't correctly: "+s);
			 				return false;
			 			}
			 		}
			 		return true;
			 	}
		}
		 System.out.println("Failed. Can't find unique text");
		 return false;
		}
		catch (Exception e) {
			System.out.print(e.getMessage());
			return false;
		}
		
	}
	
	/**
	 * Edit Data tape file
	 * @param csvFileName
	 * @param sheetName
	 * @param oldValue
	 * @param newValue
	 */
	public void editDataTapeFile(String csvFileName, String sheetName, String oldValue, String newValue){
		
		String path = getPathFile(Constant.PRESICERES_DATA_PATH + csvFileName);

		InputStream XlsxFileToRead = null;
		XSSFWorkbook workbook = null;
		try {
			XlsxFileToRead = new FileInputStream(path);
			//Getting the workbook instance for xlsx file
			workbook = new XSSFWorkbook(XlsxFileToRead);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		    XSSFSheet sheet = workbook.getSheet(sheetName);
		    XSSFRow row;
			XSSFCell cell;
			Iterator rows = sheet.rowIterator();
			
			while (rows.hasNext()) {
				row = (XSSFRow) rows.next();
				
				//Iterating all the cells of the current row
				Iterator cells = row.cellIterator();

				while (cells.hasNext()) {
					cell = (XSSFCell) cells.next();
					if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) {
						if (cell.getStringCellValue().trim().contains(oldValue)) {
//							System.out.print(cell.getStringCellValue() + " ");
							cell.setCellValue(newValue);
					}
					
				}
				}
			}
			
			try {
				XlsxFileToRead.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		    try{
		    FileOutputStream fileOut = new FileOutputStream(path);
		    workbook.write(fileOut);
		    fileOut.close();
		    } catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	public List getGuarantorListFromXmlFile(String fileName, String tagName, String attributeName, Boolean isRepeated, String attributeValue){
		List<String> arrayResult = new ArrayList<String>();
		String path = getPathFile(Constant.PRESICERES_DATA_PATH + fileName);
		try {

			File fXmlFile = new File(path);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
					
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName(tagName);
					
			System.out.println("List Guarantor from XML file: ");
			
			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);
						
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					
					Element eElement = (Element) nNode;
					if(eElement.getAttribute(attributeName).contains(attributeValue))
					{
						if((isRepeated && eElement.getAttribute("RepeatContext").contains("[")) || (!isRepeated && !eElement.getAttribute("RepeatContext").contains("["))){
							System.out.println(attributeValue+" : " + eElement.getElementsByTagName("Value").item(0).getTextContent());
							arrayResult.add(eElement.getElementsByTagName("Value").item(0).getTextContent());
						}
					}
				}
			}
			
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		return arrayResult;
	}
	
	public String getDataFromXmlFile(String fileName, String tagName, String attributeName, String attributeValue){
		String path = getPathFile(Constant.PRESICERES_DATA_PATH + fileName);
		try {

			File fXmlFile = new File(path);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
					
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName(tagName);
					
			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);
						
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					
					Element eElement = (Element) nNode;
					if(eElement.getAttribute(attributeName).contains(attributeValue))
					{
					System.out.println(attributeValue+" : " + eElement.getElementsByTagName("Value").item(0).getTextContent());
					return eElement.getElementsByTagName("Value").item(0).getTextContent();
					}
				}
			}
			
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		return "";
	}

	public String convertDate(String oldDateType, String newDateType, String date)
	{
		String newDateString;

		SimpleDateFormat sdf = new SimpleDateFormat(oldDateType);
		Date d = new Date();
		try {
			d = sdf.parse(date.replace("-", "/"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sdf.applyPattern(newDateType);
		newDateString = sdf.format(d);
		return newDateString.toString();
	}
	
	public void uploadFileSikuli(String fileName){
		
		String path = getPathFile(Constant.PRESICERES_DATA_PATH + fileName);
		String openImage = getPathFile(Constant.PRESICERES_DATA_PATH + "open.png");
		Screen scr = new Screen();
		 try{

			   scr.type(path);

			   // Click on 'Open' button

			   scr.click(openImage);

			  }catch(Throwable t){

			   System.out.println("Exeception came while selecting File :"+t.getMessage());

			  }

			  System.out.println("Test Completed");
	}
	
	public void dragDropFile(String source, String destination){
		
		String sourcePath = getPathFile(Constant.PRESICERES_DATA_PATH + source);
		String destinationPath = getPathFile(Constant.PRESICERES_DATA_PATH + destination);
		Screen scr = new Screen();
		 try{
			 
			   scr.dragDrop(sourcePath, destinationPath);

			  }catch(Throwable t){

			   System.out.println("Exeception came while selecting File :"+t.getMessage());

			  }

			  System.out.println("Test Completed");
	}
	
	public boolean isImageDisplayed(String imageName){
		
		String imageURL = getPathFile(Constant.PRESICERES_DATA_PATH + imageName);
		Screen scr = new Screen();
		 try{
			 
			   if(scr.exists(imageURL)!= null) return true;

			  }catch(Throwable t){

			   System.out.println("Exeception came while selecting File :"+t.getMessage());

			  }

			  System.out.println("Test Completed");
		return false;
	}

	public static String en = "";
	private String logOutLink = "";
	private String autLink = "";
	private static Common instance = null;
	private static XSSFSheet excelSheet;
	private static XSSFWorkbook excelBook;
	private static XSSFCell cell;
	private static XSSFRow row;

};