package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.ApplicantsPage;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_016_B2R_CreateLoanApplicantInOtherUser extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		usernameLender2 = Constant.USERNAME_CAF_LENDER;
		passwordLender2 = Constant.PASSWORD_CAF_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan-CAF"+accountNumber;
		loanStatus = Constant.LOAN_STATUS_CAF;
		applicantName = "CAF Demo Applicant";
		firstName = "applicant"+accountNumber;
		lastName = "CAF"+accountNumber;
		emailContact = "applicantCAF"+accountNumber+"@gmail.com";
		newApplicantName = "applicantCAF"+accountNumber;
		loanNameBefore = "Golden Acress";
	}
	
	@Test(groups = { "regression" },description = "Security01 - Create New Loan With Other User")
	public void SecurityTest_01_CreateNewLoanWithOtherUser()
	{
		log.info("SecurityTest_01 - Step 01. Login with lender 2");
		homePage = loginPage.loginAsLender(usernameLender2, passwordLender2, false);
		
		log.info("SecurityTest_01 - Step 02. Open Loans Page");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		log.info("SecurityTest_01 - Step 03. Create new loan item");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus);
		
		log.info("VP: Loan item display with correct info");
		verifyTrue(loansPage.isLoansDisplayWithCorrectInfo(loanName, loanStatus));
		
		log.info("VP: User displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(applicantName));
	}
	
	@Test(groups = { "regression" },description = "Security02 - Create New Applicant With Other User")
	public void SecurityTest_02_CreateNewApplicantWithOtherUser()
	{
		log.info("SecurityTest_02 - Step 01. Login with CAF lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameLender2, passwordLender2, false);
		
		log.info("SecurityTest_02 - Step 02. Open Applicant tab");
		applicantsPage = homePage.openApplicantsPage(driver, ipClient);
		
		log.info("SecurityTest_02 - Step 03. Click new Applicant button");
		applicantsPage.clickNewApplicantButton();
		
		log.info("SecurityTest_02 - Step 04. Input Applicant information");
		applicantsPage.inputApplicantInfo(accountNumber, newApplicantName);
		
		log.info("SecurityTest_02 - Step 05. Click Next button");
		applicantsPage.clickNextButton();
		
		log.info("SecurityTest_02 - Step 06. Open Applicant tab");
		applicantsPage.openApplicantsPage(driver, ipClient);
		
		log.info("SecurityTest_02 - Step 07. Search Applicant");
		applicantsPage.searchApplicantByName(newApplicantName);
		
		log.info("SecurityTest_02 - Step 08. Click on Application");
		applicantsPage.openApplicantsDetailPage(newApplicantName);
		
		log.info("SecurityTest_02 - Step 09. Click new contact detail");
		applicantsPage.clickNewContactDetail();
		applicantsPage.switchToApplicantContactFrame(driver);
		
		log.info("SecurityTest_02 - Step 10. Input contact info");
		applicantsPage.inputApplicantContactInfo(firstName, lastName, emailContact);
		
		log.info("SecurityTest_02 - Step 11. Click Save Applicant Contact Details");
		applicantsPage.clickSaveApplicantContact();
		applicantsPage.switchToTopWindowFrame(driver);
		
		log.info("SecurityTest_02 - Step 12. Click save Applicant Details");
		applicantsPage.clickSaveApplicantDetail();
		
		log.info("VP: Applicant details is saved");
		verifyTrue(applicantsPage.isApplicantDetailPageSaved());
		
		log.info("VP: The contact displays on Contact details section");
		verifyTrue(applicantsPage.isUserDisplayOnContactDetailTable(firstName+" "+lastName));
	}
	
	@Test(groups = { "regression" },description = "Security03 - Search Loan In Original Client")
	public void SecurityTest_03_SearchLoanInOriginalClient()
	{
		log.info("SecurityTest_03 - Step 01. Login with lender 1");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("SecurityTest_03 - Step 02. Open Loans Page");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		log.info("SecurityTest_03 - Step 03. Search the loan item is created by lender 2");
		loansPage.searchLoanByName(loanName);
		
		log.info("VP: The loan does not display in table");
		verifyFalse(loansPage.isLoansDisplayOnSearchLoanPage(loanName));
	}
	
	@Test(groups = { "regression" },description = "Security04 - Search Applicant In Dropdown list With Original Client")
	public void SecurityTest_04_SearchApplicantInDropdownListWithOriginalClient()
	{
		log.info("SecurityTest_04 - Step 01. Click new loan button");
		loansPage.clickNewLoanButton();
		
		log.info("VP: The applicant does not display in applicant dropdown list");
		verifyFalse(loansPage.isApplicantExistInDropdownListOnAddNewLoan(newApplicantName));	
	}
	
	@Test(groups = { "regression" },description = "Security05 - Search Applicant In Applicant Page With Original Client")
	public void SecurityTest_05_SearchApplicantInApplicantPageWithOriginalClient()
	{
		log.info("SecurityTest_05 - Step 01. Open Applicant tab");
		applicantsPage = loansPage.openApplicantsPage(driver, ipClient);
		
		log.info("SecurityTest_05 - Step 02. Search Applicant");
		applicantsPage.searchApplicantByName(newApplicantName);
		
		log.info("VP: The applicant does not display in table");
		verifyFalse(applicantsPage.isApplicantDisplayOnSearchPage(newApplicantName));		
	}
	
	@Test(groups = { "regression" },description = "Security06 - Search Applicant In Dropdown Property page")
	public void SecurityTest_06_SearchApplicantInDropdownPropertyPage()
	{
		log.info("SecurityTest_06 - Step 01. Open Properties page");
		propertiesPage = applicantsPage.openPropertiesPage(driver, ipClient);
		
		log.info("VP: The applicant does not display in applicant dropdown list in properties page");
		verifyFalse(propertiesPage.isApplicantExistInDropdownListOnPropertiesPage(newApplicantName));
		
		log.info("SecurityTest_06 - Step 02. Open Loans Page");
		loansPage = propertiesPage.openLoansPage(driver, ipClient);
		
		log.info("SecurityTest_06 - Step 03. Search the loan item is created before");
		loansPage.searchLoanByName(loanNameBefore);
		
		log.info("SecurityTest_06 - Step 04. Open loan detail page");
		loansPage.openLoansDetailPage(loanNameBefore);
		
		log.info("SecurityTest_06 - Step 05. Open properties tab");
		loansPage.openPropertiesTab();
		
		log.info("VP: The applicant does not display in applicant dropdown list in properties tab");
		verifyFalse(loansPage.isApplicantExistInDropdownListOnLoanPropertiesTab(newApplicantName));
	}
	
	@Test(groups = { "regression" },description = "Security07 - Search Applicant In Dropdown On New Loan Application Entity")
	public void SecurityTest_07_SearchApplicantInDropdownOnNewLoanApplicationEntity()
	{
		log.info("SecurityTest_07 - Step 01. Open Loans Page");
		loansPage = loansPage.openLoansPage(driver, ipClient);
		
		log.info("SecurityTest_07 - Step 02. Search the loan item is created before");
		loansPage.searchLoanByName(loanNameBefore);
		
		log.info("SecurityTest_07 - Step 03. Open loan detail page");
		loansPage.openLoansDetailPage(loanNameBefore);
		
		log.info("SecurityTest_07 - Step 04. Click new loan application entity");
		loansPage.clickNewLoanApplicationEntity();
		
		log.info("SecurityTest_07 - Step 05. Select Applicant in Entity type dropdown list");
		loansPage.selectEntityType("Applicant");
		
		log.info("VP: The applicant does not display in applicant dropdown list in New Loan Application Entity");
		verifyFalse(loansPage.isApplicantExistInDropdownListOnNewLoanApplicationEntity(newApplicantName));		
	}
	
	@Test(groups = { "regression" },description = "Security08 - Search Loan With Original Client In Dashboard")
	public void SecurityTest_08_SearchLoanWithOriginalClientInDashboard()
	{
		log.info("SecurityTest_08 - Step 01. Open Home Page");
		homePage = loansPage.openHomePage(driver, ipClient);
		
		log.info("VP: The loan does not display in dashboard with Original Client");
		verifyFalse(homePage.isLoanDisplayOnDashboard(loanName));		
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private ApplicantsPage applicantsPage;
	private PropertiesPage propertiesPage;
	private String usernameLender, passwordLender, newApplicantName, loanNameBefore;
	private String loanName, accountNumber, applicantName, loanStatus, emailContact;
	private String usernameLender2, passwordLender2, firstName, lastName;
}