package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_015_CAF_DocumentHoaConditionalPlaceHolders extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan"+accountNumber;
		loanStatus = Constant.LOAN_STATUS_CAF;
		applicantName = "CAF Demo Applicant";
		propertiesFileName = "CAF Data Tape.xlsx";
		address1 = "504 W Euclid";
		address2 = "318 E Broad";
		address3 = "502 E Hugh";
		documentType1 = "HOA CC&R";
		documentType2 = "HOA Statement (Estoppel, Certificate, Resale Disclosure, Demand)";
		documentType3 = "HOA Most Recent Operating Statement and Budget";
		documentType4 = "Other HOA documents";
	}

	@Test(groups = { "regression" }, description = "Document30 - HOA Conditional Placeholdres (greater than 0)")
	public void DocumentTest_30_HOAFieldGreaterThanZero() {
		log.info("DocumentTest_30 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender,false);

		log.info("DocumentTest_30 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_30 - Step 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus);
		
		log.info("DocumentTest_30 - Step 04. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("DocumentTest_30 - Step 05. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("DocumentTest_30 - Step 06. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("DocumentTest_30 - Step 07. Enter 'HOA' field greater than 0");
		propertiesPage.enterHOA("100");

		log.info("DocumentTest_30 - Step 08. Click save button");
		propertiesPage.clickSaveButton();

		log.info("VP: Document placeholders should be added to that Property document placeholders");
		verifyTrue(propertiesPage.isDocumentTypeOnPropertiesDetailLoaded(documentType1));
		verifyTrue(propertiesPage.isDocumentTypeOnPropertiesDetailLoaded(documentType2));
		verifyTrue(propertiesPage.isDocumentTypeOnPropertiesDetailLoaded(documentType3));
		verifyTrue(propertiesPage.isDocumentTypeOnPropertiesDetailLoaded(documentType4));
	}
	
	@Test(groups = { "regression" }, description = "Document31 - HOA Conditional Placeholdres (equal 0)")
	public void DocumentTest_31_HOAFieldEqualZero() {

		log.info("DocumentTest_31 - Step 01. Click 'Properties List' button");
		propertiesPage.clickOnPropertyListButton();

		log.info("DocumentTest_31 - Step 02. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address2);

		log.info("DocumentTest_31 - Step 03. Enter 'HOA' field equal 0");
		propertiesPage.enterHOA("0");

		log.info("DocumentTest_31 - Step 04. Click save button");
		propertiesPage.clickSaveButton();

		log.info("VP: Document placeholders shouldn't be added to that Property document placeholders");
		verifyFalse(propertiesPage.isDocumentTypeOnPropertiesDetailLoaded(documentType1));
		verifyFalse(propertiesPage.isDocumentTypeOnPropertiesDetailLoaded(documentType2));
		verifyFalse(propertiesPage.isDocumentTypeOnPropertiesDetailLoaded(documentType3));
		verifyFalse(propertiesPage.isDocumentTypeOnPropertiesDetailLoaded(documentType4));
	}
	
	@Test(groups = { "regression" }, description = "Document32 - HOA Conditional Placeholdres (less than 0)")
	public void DocumentTest_32_HOAFieldLessThanZero() {

		log.info("DocumentTest_32 - Step 01. Click 'Properties List' button");
		propertiesPage.clickOnPropertyListButton();

		log.info("DocumentTest_32 - Step 02. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address3);

		log.info("DocumentTest_32 - Step 03. Enter 'HOA' field less than 0");
		propertiesPage.enterHOA("-100");

		log.info("DocumentTest_32 - Step 04. Click save button");
		propertiesPage.clickSaveButton();

		log.info("VP: Document placeholders shouldn't be added to that Property document placeholders");
		verifyFalse(propertiesPage.isDocumentTypeOnPropertiesDetailLoaded(documentType1));
		verifyFalse(propertiesPage.isDocumentTypeOnPropertiesDetailLoaded(documentType2));
		verifyFalse(propertiesPage.isDocumentTypeOnPropertiesDetailLoaded(documentType3));
		verifyFalse(propertiesPage.isDocumentTypeOnPropertiesDetailLoaded(documentType4));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private String usernameLender, passwordLender, address1, address2, address3 ;
	private String loanName, accountNumber, applicantName, propertiesFileName,
			loanStatus;
	private String documentType1, documentType2,
			documentType3, documentType4;
}