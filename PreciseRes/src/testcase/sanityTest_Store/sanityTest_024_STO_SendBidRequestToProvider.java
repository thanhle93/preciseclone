package sanityTest_Store;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.MailHomePage;
import page.MailLoginPage;
import page.PageFactory;
import page.TransactionPage;
import page.UsersPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_024_STO_SendBidRequestToProvider extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + getUniqueNumber();
		surveyProvider = "Survey Provider no. ";
		phaseIProvider = "Phase I Provider no. ";
		titleProvider = "Title Provider no. ";
		pcaProvider = "PCA Provider no. ";
		email = "minhdam06@gmail.com";
		providerType1 = "Survey Provider";
		providerType2 = "Phase I Provider";
		providerType3 = "PCA Provider";
		providerType4 = "Title Provider";
		propertiesFileName = "StoreDataTape_Updated.xlsx";
		propertyAddress = "1020 Bonforte Blvd Updated";
		mailPageUrl = "https://accounts.google.com";
		usernameEmail = "minhdam06@gmail.com";
		passwordEmail = "dam123!@#!@#";
		emailSubjectBidRequest = "Request for Bid on Transaction";
		emailMessageBidRequest = "We would like to request a quote to engage your services";
		emailSubjectBidWon = "Engagement for Transaction";
		emailMessageBidWon = "We would like to engage your services for ";
	}

	@Test(groups = { "regression" }, description = "Send Bid Request To Provider - SendBidRequestToMultipleProvider")
	public void SendBidRequestToProvider_01_SendBidRequestToMultipleProvider() {
		
		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);
		
		log.info("Precondition - Step 02: Open Users page");
		usersPage = transactionPage.openUsersPage();
		
		log.info("Precondition - Step 03: Create three Survey Provider");
		usersPage.createNewUser(surveyProvider+"1", providerType1, email);
		usersPage.createNewUser(surveyProvider+"2", providerType1, email);
		usersPage.createNewUser(surveyProvider+"3", providerType1, email);
		
		log.info("Precondition - Step 04: Create three Phase I Provider");
		usersPage.createNewUser(phaseIProvider+"1", providerType2, email);
		usersPage.createNewUser(phaseIProvider+"2", providerType2, email);
		usersPage.createNewUser(phaseIProvider+"3", providerType2, email);
		
		log.info("Precondition - Step 05: Create three PCA Provider");
		usersPage.createNewUser(pcaProvider+"1", providerType3, email);
		usersPage.createNewUser(pcaProvider+"2", providerType3, email);
		usersPage.createNewUser(pcaProvider+"3", providerType3, email);
		
		log.info("Precondition - Step 06: Create three Title Provider");
		usersPage.createNewUser(titleProvider+"1", providerType4, email);
		usersPage.createNewUser(titleProvider+"2", providerType4, email);
		usersPage.createNewUser(titleProvider+"3", providerType4, email);
		
		log.info("SendBidRequestToProvider_01 - Step 07: Open Transaction page");
		transactionPage = usersPage.openTransactionPage();
		
		log.info("SendBidRequestToProvider_01 - Step 08: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("SendBidRequestToProvider_01 - Step 09: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("SendBidRequestToProvider_01 - Step 10: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("SendBidRequestToProvider_01 - Step 11: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("SendBidRequestToProvider_01 - Step 12: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: 'Bid request' button is displayed for all Workflow status");
		verifyTrue(transactionPage.isBidRequestButtonDisplayedForAllStatuses());
		
		log.info("SendBidRequestToProvider_01 - Step 13: Open Property tab");
		transactionPage.openTransactionDocumentsTab();
		
		log.info("VP: 'Title Cost Quote' documents type isn't displayed before choose winner");
		verifyFalse(transactionPage.isDocumentTypeDisplayedCorrectly("Title Cost Quote"));
		
		log.info("SendBidRequestToProvider_01 - Step 14: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("SendBidRequestToProvider_01 - Step 15: Click 'Add Properties' button");
		transactionPage.clickAddPropertyButton();

		log.info("SendBidRequestToProvider_01 - Step 16: Upload Excel filename");
		transactionPage.uploadFileProperties(propertiesFileName);
		
		log.info("SendBidRequestToProvider_01 - Step 17: Open Basic details tab");
		transactionPage.openBasicDetailsTab();
		
		log.info("SendBidRequestToProvider_01 - Step 18: Click on Bid Request button");
		transactionPage.clickBidRequestButton();
		
		log.info("SendBidRequestToProvider_01 - Step 19: Click on Select Survey Provider button");
		transactionPage.clickOnProviderSelector(providerType1);
		
		log.info("SendBidRequestToProvider_01 - Step 20: Check all Survey Provider we just created");
		transactionPage.clickOnProviderCheckbox(surveyProvider+"1");
		transactionPage.clickOnProviderCheckbox(surveyProvider+"2");
		transactionPage.clickOnProviderCheckbox(surveyProvider+"3");
		
		log.info("SendBidRequestToProvider_01 - Step 21: Click on Select Phase I Provider button");
		transactionPage.clickOnProviderSelector(providerType2);
		
		log.info("SendBidRequestToProvider_01 - Step 22: Check all Phase I Provider we just created");
		transactionPage.clickOnProviderCheckbox(phaseIProvider+"1");
		transactionPage.clickOnProviderCheckbox(phaseIProvider+"2");
		transactionPage.clickOnProviderCheckbox(phaseIProvider+"3");
		
		log.info("SendBidRequestToProvider_01 - Step 23: Click on Select PCA Provider button");
		transactionPage.clickOnProviderSelector(providerType3);
		
		log.info("SendBidRequestToProvider_01 - Step 24: Check all PCA Provider we just created");
		transactionPage.clickOnProviderCheckbox(pcaProvider+"1");
		transactionPage.clickOnProviderCheckbox(pcaProvider+"2");
		transactionPage.clickOnProviderCheckbox(pcaProvider+"3");
		
		log.info("SendBidRequestToProvider_01 - Step 25: Click on Select Title Provider button");
		transactionPage.clickOnProviderSelector(providerType4);
		
		log.info("SendBidRequestToProvider_01 - Step 26: Check all Title Provider we just created");
		transactionPage.clickOnProviderCheckbox(titleProvider+"1");
		transactionPage.clickOnProviderCheckbox(titleProvider+"2");
		transactionPage.clickOnProviderCheckbox(titleProvider+"3");
		
		log.info("SendBidRequestToProvider_01 - Step 27: Click on OK button");
		transactionPage.clickOnProviderSelectorOkButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("SendBidRequestToProvider_01 - Step 28: Open Third party bids tab");
		transactionPage.openThirdParyBidsTab();
		
		log.info("SendBidRequestToProvider_01 - Step 29: Open Survey Bid type ");
		transactionPage.openBidTypeLink(providerType1);
		
		log.info("VP: All selected Survey provider are displayed here");
		verifyTrue(transactionPage.isProviderDisplayCorrectly(surveyProvider+"1","Sent"));
		verifyTrue(transactionPage.isProviderDisplayCorrectly(surveyProvider+"2","Sent"));
		verifyTrue(transactionPage.isProviderDisplayCorrectly(surveyProvider+"3","Sent"));
		
		log.info("SendBidRequestToProvider_01 - Step 30: Go back to third party bids");
		transactionPage.goBackToThirdPartyBids();
		
		log.info("SendBidRequestToProvider_01 - Step 31: Open Phase I Bid type ");
		transactionPage.openBidTypeLink(providerType2);
		
		log.info("VP: All selected Phase I provider are displayed here");
		verifyTrue(transactionPage.isProviderDisplayCorrectly(phaseIProvider+"1","Sent"));
		verifyTrue(transactionPage.isProviderDisplayCorrectly(phaseIProvider+"2","Sent"));
		verifyTrue(transactionPage.isProviderDisplayCorrectly(phaseIProvider+"3","Sent"));
		
		log.info("SendBidRequestToProvider_01 - Step 32: Go back to third party bids");
		transactionPage.goBackToThirdPartyBids();
		
		log.info("SendBidRequestToProvider_01 - Step 33: Open PCA Bid type ");
		transactionPage.openBidTypeLink(providerType3);
		
		log.info("VP: All selected PCA provider are displayed here");
		verifyTrue(transactionPage.isProviderDisplayCorrectly(pcaProvider+"1","Sent"));
		verifyTrue(transactionPage.isProviderDisplayCorrectly(pcaProvider+"2","Sent"));
		verifyTrue(transactionPage.isProviderDisplayCorrectly(pcaProvider+"3","Sent"));
		
		log.info("SendBidRequestToProvider_01 - Step 34: Go back to third party bids");
		transactionPage.goBackToThirdPartyBids();
		
		log.info("SendBidRequestToProvider_01 - Step 35: Open Title Bid type ");
		transactionPage.openBidTypeLink(providerType4);
		
		log.info("VP: All selected Title provider are displayed here");
		verifyTrue(transactionPage.isProviderDisplayCorrectly(titleProvider+"1","Sent"));
		verifyTrue(transactionPage.isProviderDisplayCorrectly(titleProvider+"2","Sent"));
		verifyTrue(transactionPage.isProviderDisplayCorrectly(titleProvider+"3","Sent"));
		presPreviousURL = transactionPage.getCurrentUrl(driver);
		
		log.info("SendInformation_01 - Step 36. Open Email url");
		mailLoginPage = transactionPage.openMailLink(driver, mailPageUrl);
		mailLoginPage.loginToGmail(usernameEmail, passwordEmail);

		log.info("SendInformation_01 - Step 37. Click Submit button");
		mailHomePage = mailLoginPage.clickSubmitButton(driver, ipClient);

		log.info("SendInformation_01 - Step 38. Click 'Google Apps' title");
		mailHomePage.clickGoogleAppsTitle();

		log.info("SendInformation_01 - Step 39. Click 'Gmail' application");
		mailHomePage.clickGmailApplication();

		log.info("VP. Gmail home page is displayed");
		verifyTrue(mailHomePage.isGmailHomePageDisplay());

		log.info("SendInformation_01 - Step 40. Click 'Inbox' title");
		mailHomePage.clickInboxTitle();

		log.info("SendInformation_01 - Step 41. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmailDynamic(emailSubjectBidRequest);
		
		log.info("SendInformation_01 - Step 42. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubjectBidRequest));
		
		log.info("SendInformation_01 - Step 43. Verify Email Message");
		verifyTrue(mailHomePage.isEmailContentDisplayed(emailMessageBidRequest));
		verifyTrue(mailHomePage.isEmailContentDisplayed("Survey and Phase I and PCA and Title"));
		verifyTrue(mailHomePage.isEmailTableContentDisplayed("1020 Bonforte Blvd Updated", "", "807", "84", ""));
		
		log.info("SendInformation_01 - Step 44. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_01 - Step 45. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		log.info("SendInformation_01 - Step 46. Go back to Prescise page");
		mailHomePage.openLink(driver, presPreviousURL);
	}
	
	@Test(groups = { "regression" }, description = "Send Bid Request To Provider - Choose Winner Provider")
	public void SendBidRequestToProvider_02_ChooseWinnerSurveyProvider() {
		log.info("SendBidRequestToProvider_02 - Step 01: Go back to third party bids");
		transactionPage.goBackToThirdPartyBids();
		
		log.info("SendBidRequestToProvider_02 - Step 02: Open Survey Bid type ");
		transactionPage.openBidTypeLink(providerType1);
		
		log.info("SendBidRequestToProvider_02 - Step 03: Check a survey Provider checkbox");
		transactionPage.clickOnProviderCheckbox(surveyProvider+"1");
		
		log.info("SendBidRequestToProvider_02 - Step 04: Click on Choose winner button");
		transactionPage.clickOnChooseWinnerButton();
		
		log.info("SendBidRequestToProvider_02 - Step 05: Accept the confirmation ");
		transactionPage.acceptJavascriptAlert(driver);
		
		log.info("SendBidRequestToProvider_02 - Step 06: Switch to third party bid iframe");
		transactionPage.switchToThirdPartyBidInstructionFrame(driver);
		
		log.info("SendBidRequestToProvider_02 - Step 07: Save the instruction");
		transactionPage.clickOnSaveInstructionButton();
		transactionPage.switchToTopWindowFrame(driver);
		
		log.info("VP: All Survey provider are displayed correctly");
		verifyTrue(transactionPage.isProviderDisplayCorrectly(surveyProvider+"1","Won"));
		verifyTrue(transactionPage.isProviderDisplayCorrectly(surveyProvider+"2","Lost"));
		verifyTrue(transactionPage.isProviderDisplayCorrectly(surveyProvider+"3","Lost"));
		
		log.info("VP:Choose Winner button is disappeared");
		verifyFalse(transactionPage.isChooseWinnerButtonDisplay());
		
		log.info("SendBidRequestToProvider_02 - Step 08: Save the instruction");
		transactionPage.openProviderDetailPage(surveyProvider+"1");
		presPreviousURL = transactionPage.getCurrentUrl(driver);
		
		log.info("SendBidRequestToProvider_02 - Step 09: Survey and Zoning Letter documents are displayed");
		verifyTrue(transactionPage.isDocumentNameDisplayedCorrectly(propertyAddress+ " - " +"Survey" ));
		verifyTrue(transactionPage.isDocumentNameDisplayedCorrectly(propertyAddress+ " - " +"Zoning Letter"));
		
//		log.info("SendBidRequestToProvider_02 - Step 10. Open Email url");
//		mailLoginPage = transactionPage.openMailLink(driver, "https://mail.google.com");
//		
//		log.info("SendBidRequestToProvider_02 - Step 11. Click 'Subject first email'");
//		mailHomePage.clickLastestSubjectEmailDynamic(emailSubjectBidWon);
//		
//		log.info("SendBidRequestToProvider_02 - Step 12. Verify Email Subject");
//		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubjectBidWon));
//		
//		log.info("SendBidRequestToProvider_02 - Step 13. Verify Email Message");
//		verifyTrue(mailHomePage.isEmailContentDisplayed(emailMessageBidWon));
//		verifyTrue(mailHomePage.isEmailContentDisplayed(providerType1));
//		verifyTrue(mailHomePage.isEmailTableContentDisplayed("1020 Bonforte Blvd Updated", "Restaurants and Other Eating Places", "807", "84", "5040000.00"));
//		
//		log.info("SendBidRequestToProvider_02 - Step 14. Click data tooltip 'More' dropdown");
//		mailHomePage.clickDataTooltipMoreDropdown();
//
//		log.info("SendBidRequestToProvider_02 - Step 15. Click 'Delete this message' title");
//		mailHomePage.clickDeleteThisMessageTitle();
//		
//		log.info("SendBidRequestToProvider_02 - Step 16. Go back to Prescise page");
//		mailHomePage.openLink(driver, presPreviousURL);
	}
	
	@Test(groups = { "regression" }, description = "Send Bid Request To Provider - Choose Winner PCA Provider")
	public void SendBidRequestToProvider_03_ChooseWinnerPhaseIProvider() {
		
		log.info("SendBidRequestToProvider_03 - Step 01: Open Transaction page");
		transactionPage = usersPage.openTransactionPage();
		presPreviousURL = transactionPage.getCurrentUrl(driver);
		
		log.info("SendBidRequestToProvider_03 - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("SendBidRequestToProvider_03 - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("SendBidRequestToProvider_03 - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("SendBidRequestToProvider_03 - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("SendBidRequestToProvider_03 - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("SendBidRequestToProvider_03 - Step 07: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("SendBidRequestToProvider_03 - Step 08: Click 'Add Properties' button");
		transactionPage.clickAddPropertyButton();

		log.info("SendBidRequestToProvider_03 - Step 09: Upload Excel filename");
		transactionPage.uploadFileProperties(propertiesFileName);
		
		log.info("SendBidRequestToProvider_03 - Step 10: Open Basic details tab");
		transactionPage.openBasicDetailsTab();
		
		log.info("SendBidRequestToProvider_03 - Step 11: Click on Bid Request button");
		transactionPage.clickBidRequestButton();
		
		log.info("SendBidRequestToProvider_03 - Step 12: Click on Select Phase I Provider button");
		transactionPage.clickOnProviderSelector(providerType2);
		
		log.info("SendBidRequestToProvider_03 - Step 13: Check all Phase I Provider we just created");
		transactionPage.clickOnProviderCheckbox(phaseIProvider+"1");
		transactionPage.clickOnProviderCheckbox(phaseIProvider+"2");
		transactionPage.clickOnProviderCheckbox(phaseIProvider+"3");
		
		log.info("SendBidRequestToProvider_03 - Step 14: Click on OK button");
		transactionPage.clickOnProviderSelectorOkButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("SendBidRequestToProvider_03 - Step 15: Open Third party bids tab");
		transactionPage.openThirdParyBidsTab();
		
		log.info("SendBidRequestToProvider_03 - Step 16: Open Phase I Bid type ");
		transactionPage.openBidTypeLink(providerType2);
		
		log.info("SendBidRequestToProvider_03 - Step 17: Check a Phase I Provider checkbox");
		transactionPage.clickOnProviderCheckbox(phaseIProvider+"1");
		
		log.info("SendBidRequestToProvider_03 - Step 18: Click on Choose winner button");
		transactionPage.clickOnChooseWinnerButton();
		
		log.info("SendBidRequestToProvider_03 - Step 19: Accept the confirmation ");
		transactionPage.acceptJavascriptAlert(driver);
		
		log.info("SendBidRequestToProvider_03 - Step 20: Switch to third party bid iframe");
		transactionPage.switchToThirdPartyBidInstructionFrame(driver);
		
		log.info("SendBidRequestToProvider_03 - Step 21: Save the instruction");
		transactionPage.clickOnSaveInstructionButton();
		transactionPage.switchToTopWindowFrame(driver);
		
		log.info("VP: All Survey provider are displayed correctly");
		verifyTrue(transactionPage.isProviderDisplayCorrectly(phaseIProvider+"1","Won"));
		verifyTrue(transactionPage.isProviderDisplayCorrectly(phaseIProvider+"2","Lost"));
		verifyTrue(transactionPage.isProviderDisplayCorrectly(phaseIProvider+"3","Lost"));
		
		log.info("VP:Choose Winner button is disappeared");
		verifyFalse(transactionPage.isChooseWinnerButtonDisplay());
		
		log.info("SendBidRequestToProvider_03 - Step 22: Save the instruction");
		transactionPage.openProviderDetailPage(phaseIProvider+"1");
		
		log.info("SendBidRequestToProvider_03 - Step 23: Phase I documents are displayed");
		verifyTrue(transactionPage.isDocumentNameDisplayedCorrectly(propertyAddress+ " - " +"Phase I" ));
		
//		log.info("SendBidRequestToProvider_03 - Step 24. Open Email url");
//		mailLoginPage = transactionPage.openMailLink(driver, "https://mail.google.com");
//		
//		log.info("SendBidRequestToProvider_03 - Step 25. Click 'Subject first email'");
//		mailHomePage.clickLastestSubjectEmailDynamic(emailSubjectBidWon);
//		
//		log.info("SendBidRequestToProvider_03 - Step 26. Verify Email Subject");
//		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubjectBidWon));
//		
//		log.info("SendBidRequestToProvider_03 - Step 27. Verify Email Message");
//		verifyTrue(mailHomePage.isEmailContentDisplayed(emailMessageBidWon));
//		verifyTrue(mailHomePage.isEmailContentDisplayed(providerType2));
//		verifyTrue(mailHomePage.isEmailTableContentDisplayed("1020 Bonforte Blvd Updated", "Restaurants and Other Eating Places", "807", "84", "5040000.00"));
//		
//		log.info("SendBidRequestToProvider_03 - Step 28. Click data tooltip 'More' dropdown");
//		mailHomePage.clickDataTooltipMoreDropdown();
//
//		log.info("SendBidRequestToProvider_03 - Step 29. Click 'Delete this message' title");
//		mailHomePage.clickDeleteThisMessageTitle();
//		
//		log.info("SendBidRequestToProvider_03 - Step 30. Go back to Prescise page");
//		mailHomePage.openLink(driver, presPreviousURL);
	}
	
	@Test(groups = { "regression" }, description = "Send Bid Request To Provider - Choose Winner PCA Provider")
	public void SendBidRequestToProvider_04_ChooseWinnerPCAProvider() {
		
		log.info("SendBidRequestToProvider_04 - Step 01: Open Transaction page");
		transactionPage = usersPage.openTransactionPage();
		
		log.info("SendBidRequestToProvider_04 - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("SendBidRequestToProvider_04 - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("SendBidRequestToProvider_04 - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("SendBidRequestToProvider_04 - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("SendBidRequestToProvider_04 - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("SendBidRequestToProvider_04 - Step 07: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("SendBidRequestToProvider_04 - Step 08: Click 'Add Properties' button");
		transactionPage.clickAddPropertyButton();

		log.info("SendBidRequestToProvider_04 - Step 09: Upload Excel filename");
		transactionPage.uploadFileProperties(propertiesFileName);
		
		log.info("SendBidRequestToProvider_04 - Step 10: Open Basic details tab");
		transactionPage.openBasicDetailsTab();
		
		log.info("SendBidRequestToProvider_04 - Step 11: Click on Bid Request button");
		transactionPage.clickBidRequestButton();
		
		log.info("SendBidRequestToProvider_04 - Step 12: Click on Select PCA Provider button");
		transactionPage.clickOnProviderSelector(providerType3);
		
		log.info("SendBidRequestToProvider_04 - Step 13: Check all PCA Provider we just created");
		transactionPage.clickOnProviderCheckbox(pcaProvider+"1");
		transactionPage.clickOnProviderCheckbox(pcaProvider+"2");
		transactionPage.clickOnProviderCheckbox(pcaProvider+"3");
		
		log.info("SendBidRequestToProvider_04 - Step 14: Click on OK button");
		transactionPage.clickOnProviderSelectorOkButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("SendBidRequestToProvider_04 - Step 15: Open Third party bids tab");
		transactionPage.openThirdParyBidsTab();
		
		log.info("SendBidRequestToProvider_04 - Step 16: Open PCA Bid type ");
		transactionPage.openBidTypeLink(providerType3);
		
		log.info("SendBidRequestToProvider_04 - Step 17: Check a PCA Provider checkbox");
		transactionPage.clickOnProviderCheckbox(pcaProvider+"1");
		
		log.info("SendBidRequestToProvider_04 - Step 18: Click on Choose winner button");
		transactionPage.clickOnChooseWinnerButton();
		
		log.info("SendBidRequestToProvider_04 - Step 19: Accept the confirmation ");
		transactionPage.acceptJavascriptAlert(driver);
		
		log.info("SendBidRequestToProvider_04 - Step 20: Switch to third party bid iframe");
		transactionPage.switchToThirdPartyBidInstructionFrame(driver);
		
		log.info("SendBidRequestToProvider_04 - Step 21: Save the instruction");
		transactionPage.clickOnSaveInstructionButton();
		transactionPage.switchToTopWindowFrame(driver);
		
		log.info("VP: All Survey provider are displayed correctly");
		verifyTrue(transactionPage.isProviderDisplayCorrectly(pcaProvider+"1","Won"));
		verifyTrue(transactionPage.isProviderDisplayCorrectly(pcaProvider+"2","Lost"));
		verifyTrue(transactionPage.isProviderDisplayCorrectly(pcaProvider+"3","Lost"));
		
		log.info("VP:Choose Winner button is disappeared");
		verifyFalse(transactionPage.isChooseWinnerButtonDisplay());
		
		log.info("SendBidRequestToProvider_04 - Step 22: Save the instruction");
		transactionPage.openProviderDetailPage(pcaProvider+"1");
		
		log.info("SendBidRequestToProvider_04 - Step 23: Property Condition Report documents are displayed");
		verifyTrue(transactionPage.isDocumentNameDisplayedCorrectly(propertyAddress+ " - " +"Property Condition Report" ));
	
//		log.info("SendBidRequestToProvider_04 - Step 24. Open Email url");
//		mailLoginPage = transactionPage.openMailLink(driver, "https://mail.google.com");
//		
//		log.info("SendBidRequestToProvider_04 - Step 25. Click 'Subject first email'");
//		mailHomePage.clickLastestSubjectEmailDynamic(emailSubjectBidWon);
//		
//		log.info("SendBidRequestToProvider_04 - Step 26. Verify Email Subject");
//		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubjectBidWon));
//		
//		log.info("SendBidRequestToProvider_04 - Step 27. Verify Email Message");
//		verifyTrue(mailHomePage.isEmailContentDisplayed(emailMessageBidWon));
//		verifyTrue(mailHomePage.isEmailContentDisplayed(providerType3));
//		verifyTrue(mailHomePage.isEmailTableContentDisplayed("1020 Bonforte Blvd Updated", "Restaurants and Other Eating Places", "807", "84", "5040000.00"));
//		
//		log.info("SendBidRequestToProvider_04 - Step 28. Click data tooltip 'More' dropdown");
//		mailHomePage.clickDataTooltipMoreDropdown();
//
//		log.info("SendBidRequestToProvider_04 - Step 29. Click 'Delete this message' title");
//		mailHomePage.clickDeleteThisMessageTitle();
//		
//		log.info("SendBidRequestToProvider_04 - Step 30. Go back to Prescise page");
//		mailHomePage.openLink(driver, presPreviousURL);
	}
	
	@Test(groups = { "regression" }, description = "Send Bid Request To Provider - Choose Winner Title Provider")
	public void SendBidRequestToProvider_05_ChooseWinnerTitleProvider() {
		
		log.info("SendBidRequestToProvider_05 - Step 01: Open Transaction page");
		transactionPage = usersPage.openTransactionPage();
		
		log.info("SendBidRequestToProvider_05 - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("SendBidRequestToProvider_05 - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("SendBidRequestToProvider_05 - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("SendBidRequestToProvider_05 - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("SendBidRequestToProvider_05 - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("SendBidRequestToProvider_05 - Step 07: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("SendBidRequestToProvider_05 - Step 08: Click 'Add Properties' button");
		transactionPage.clickAddPropertyButton();

		log.info("SendBidRequestToProvider_05 - Step 09: Upload Excel filename");
		transactionPage.uploadFileProperties(propertiesFileName);
		
		log.info("SendBidRequestToProvider_05 - Step 10: Open Basic details tab");
		transactionPage.openBasicDetailsTab();
		
		log.info("SendBidRequestToProvider_05 - Step 11: Click on Bid Request button");
		transactionPage.clickBidRequestButton();
		
		log.info("SendBidRequestToProvider_05 - Step 12: Click on Select Title Provider button");
		transactionPage.clickOnProviderSelector(providerType4);
		
		log.info("SendBidRequestToProvider_05 - Step 13: Check all Title Provider we just created");
		transactionPage.clickOnProviderCheckbox(titleProvider+"1");
		transactionPage.clickOnProviderCheckbox(titleProvider+"2");
		transactionPage.clickOnProviderCheckbox(titleProvider+"3");
		
		log.info("SendBidRequestToProvider_05 - Step 14: Click on OK button");
		transactionPage.clickOnProviderSelectorOkButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("SendBidRequestToProvider_05 - Step 15: Open Third party bids tab");
		transactionPage.openThirdParyBidsTab();
		
		log.info("SendBidRequestToProvider_05 - Step 16: Open Title Bid type ");
		transactionPage.openBidTypeLink(providerType4);
		
		log.info("SendBidRequestToProvider_05 - Step 17: Check a PCA Provider checkbox");
		transactionPage.clickOnProviderCheckbox(titleProvider+"1");
		
		log.info("SendBidRequestToProvider_05 - Step 18: Click on Choose winner button");
		transactionPage.clickOnChooseWinnerButton();
		
		log.info("SendBidRequestToProvider_05 - Step 19: Accept the confirmation ");
		transactionPage.acceptJavascriptAlert(driver);
		
		log.info("SendBidRequestToProvider_05 - Step 20: Switch to third party bid iframe");
		transactionPage.switchToThirdPartyBidInstructionFrame(driver);
		
		log.info("SendBidRequestToProvider_05 - Step 21: Save the instruction");
		transactionPage.clickOnSaveInstructionButton();
		transactionPage.switchToTopWindowFrame(driver);
		
		log.info("VP: All title provider are displayed correctly");
		verifyTrue(transactionPage.isProviderDisplayCorrectly(titleProvider+"1","Won"));
		verifyTrue(transactionPage.isProviderDisplayCorrectly(titleProvider+"2","Lost"));
		verifyTrue(transactionPage.isProviderDisplayCorrectly(titleProvider+"3","Lost"));
		
		log.info("VP:Choose Winner button is disappeared");
		verifyFalse(transactionPage.isChooseWinnerButtonDisplay());
		
		log.info("SendBidRequestToProvider_05 - Step 22: Save the instruction");
		transactionPage.openProviderDetailPage(titleProvider+"1");
		
		log.info("SendBidRequestToProvider_05 - Step 23: Flood certification, Tax information and Title Cost Quote documents are displayed");
		verifyTrue(transactionPage.isDocumentNameDisplayedCorrectly(propertyAddress+ " - " +"Flood Certification" ));
		verifyTrue(transactionPage.isDocumentNameDisplayedCorrectly(propertyAddress+ " - " +"Tax information" ));
		verifyTrue(transactionPage.isDocumentNameDisplayedCorrectly("Title Cost Quote" ));
		
		log.info("SendBidRequestToProvider_05 - Step 24: Open Property tab");
		transactionPage.openTransactionDocumentsTab();
		
		log.info("VP: 'Title Cost Quote' documents type is displayed before choose winner");
		verifyTrue(transactionPage.isDocumentTypeDisplayedCorrectly("Title Cost Quote"));
		
//		log.info("SendBidRequestToProvider_05 - Step 25. Open Email url");
//		mailLoginPage = transactionPage.openMailLink(driver, "https://mail.google.com");
//		
//		log.info("SendBidRequestToProvider_05 - Step 26. Click 'Subject first email'");
//		mailHomePage.clickLastestSubjectEmailDynamic(emailSubjectBidWon);
//		
//		log.info("SendBidRequestToProvider_05 - Step 27. Verify Email Subject");
//		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubjectBidWon));
//		
//		log.info("SendBidRequestToProvider_05 - Step 28. Verify Email Message");
//		verifyTrue(mailHomePage.isEmailContentDisplayed(emailMessageBidWon));
//		verifyTrue(mailHomePage.isEmailContentDisplayed(providerType4));
//		verifyTrue(mailHomePage.isEmailTableContentDisplayed("1020 Bonforte Blvd Updated", "Restaurants and Other Eating Places", "807", "84", "5040000.00"));
//		
//		log.info("SendBidRequestToProvider_05 - Step 29. Click data tooltip 'More' dropdown");
//		mailHomePage.clickDataTooltipMoreDropdown();
//
//		log.info("SendBidRequestToProvider_05 - Step 30. Click 'Delete this message' title");
//		mailHomePage.clickDeleteThisMessageTitle();
//		
//		log.info("SendBidRequestToProvider_05 - Step 31. Go back to Prescise page");
//		mailHomePage.openLink(driver, presPreviousURL);
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private UsersPage usersPage;
	private MailLoginPage mailLoginPage;
	private MailHomePage mailHomePage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName;
	private String mailPageUrl, usernameEmail, passwordEmail;
	private String presPreviousURL, emailSubjectBidRequest, emailMessageBidRequest, emailSubjectBidWon, emailMessageBidWon;
	private String surveyProvider, phaseIProvider, titleProvider, pcaProvider;
	private String email, providerType1, providerType2, providerType3, providerType4, propertiesFileName, propertyAddress;
	}