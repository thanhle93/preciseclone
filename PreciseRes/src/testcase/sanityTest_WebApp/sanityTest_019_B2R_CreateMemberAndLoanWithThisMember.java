package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LendersPage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import page.TermsOfUsePage;
import page.UserLoginsPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_019_B2R_CreateMemberAndLoanWithThisMember extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameUser = Constant.USERNAME_ADMIN;
		passwordUser = Constant.PASSWORD_ADMIN;
		accountNumber = getUniqueNumber();
		loginId = "B2R-member"+accountNumber;
		password = "change";
		userRole = "Member";
		lastName = "B2R-member"+accountNumber;
		userEmail = "B2R-member"+accountNumber+"@uditeam.com";
		loanApplication = "B2R";
		lenderName = "Consolidation Demo Lender";
		securityAnswer = "securityAnswer";
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan"+accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower";
		propertiesFileName = "B2R Required.xlsx";
		address1 = "37412 OAKHILL ST";
		documentType2 = "P3 - Purchase Contract/HUD-1 Settlement Stmt";
		documentFileName = "datatest.pdf";
	}
	
	@Test(groups = { "regression" },description = "Security23 - Create New Member And Add to Lender Contact")
	public void SecurityTest_23_CreateNewMemberAndAddToLenderContact()
	{
		log.info("SecurityTest_23 - Step 01. Login with Admin");
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("SecurityTest_23 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("SecurityTest_23 - Step 03. Click new button to create new user");
		userLoginsPage.clickNewUserButton();
		
		log.info("SecurityTest_23 - Step 04. Input last name");
		userLoginsPage.inputUserLastName(lastName);
		
		log.info("SecurityTest_23 - Step 05. Input login ID");
		userLoginsPage.inputUserLoginID(loginId);
		
		log.info("SecurityTest_23 - Step 06. Input Password");
		userLoginsPage.inputPassword(password);
		
		log.info("SecurityTest_23 - Step 07. Retype Password");
		userLoginsPage.retypePassword(password);
		
		log.info("SecurityTest_23 - Step 08. Select User Role (Member)");
		userLoginsPage.selectUserRole(userRole);
		
		log.info("SecurityTest_23 - Step 09. Input email");
		userLoginsPage.inputEmail(userEmail);
		
		log.info("SecurityTest_23 - Step 10. Select loan application");
		userLoginsPage.selectLoanApplication(loanApplication);
		
		log.info("SecurityTest_23 - Step 11. Check Consolidate check box");
		userLoginsPage.checkConsolidateCheckbox();
		
		log.info("SecurityTest_23 - Step 12. Click Save button to save New User Info");
		userLoginsPage.clickSaveUserDetailButton();		
		
		log.info("SecurityTest_23 - Step 13. Open Lender tab");
		lendersPage = userLoginsPage.openLendersPage(driver, ipClient);
		
		log.info("SecurityTest_23 - Step 14. Search Lender by name");
		lendersPage.searchLenderByName(lenderName);
		
		log.info("SecurityTest_23 - Step 15. Open Lender Detail page");
		lendersPage.openLenderDetailPage(lenderName);
		
		log.info("SecurityTest_23 - Step 16. Click new contact detail button");
		lendersPage.clickNewContactDetailButton();
	//	lendersPage.switchToLenderContactDetailFrame(driver);
		
		log.info("SecurityTest_23 - Step 17. Input last name");
		lendersPage.inputLastNameOnLenderContactDetail(lastName);
		
		log.info("SecurityTest_23 - Step 18. Select login value");
		lendersPage.selectLoginValueOnLenderContactDetail(lastName+" ("+lastName+")");
		
//		log.info("SecurityTest_23 - Step 19. Click save lender Contact Detail");
//		lendersPage.clickSaveLenderContactDetail();
		
		log.info("SecurityTest_23 - Step 19. Click Save Lender Detail");
		lendersPage.clickSaveLenderDetail();
		
		log.info("SecurityTest_23 - Step 20. Click Back to Lender");
		lendersPage.clickBackToLender();
		
		log.info("VP: New memder displays on Lender Contact detail table");
		verifyTrue(lendersPage.isUserDisplayOnLenderContactDetailTable(lastName));
	}
	
	@Test(groups = { "regression" },description = "Security24 - Create new loan with new member")
	public void SecurityTest_24_CreateNewLoanWithNewMember()
	{
		log.info("SecurityTest_24 - Step 01. Login with new user");
		loginPage = logout(driver, ipClient);
		termsOfUsePage = loginPage.loginAsNewUser(loginId, password, false);
		
		log.info("VP: New user logins successfully");
		termsOfUsePage.isTermsOfUseTitleDisplay();
		
		log.info("VP: Username displays in left top corner");
		verifyTrue(termsOfUsePage.isUsernameDisplayTopLeftCorner("PreciseRES"));
		
		log.info("VP: 'Logged in as Username' displays in right top corner");
		verifyTrue(termsOfUsePage.isLoggedAsUsernameDisplayRightLeftCorner(lastName));
		
		log.info("SecurityTest_24 - Step 02. Check read terms of use checkbox");
		termsOfUsePage.checkReadTermsUseCheckbox();
		
		log.info("SecurityTest_24 - Step 03. Click Continue button");
		termsOfUsePage.clickContinueButton();
		
		log.info("SecurityTest_24 - Step 04. Input Security Answer");
		termsOfUsePage.inputSecurityAnswer(securityAnswer);
		
		log.info("SecurityTest_24 - Step 05. Finish Password retrieval information");
		homePage = termsOfUsePage.finishPasswordRetrievalInfoForMember();
		
		log.info("SecurityTest_24 - Step 06. Open Loans Page");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		log.info("SecurityTest_24 - Step 07. Create new loan item");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);
		
		log.info("VP: User displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(applicantName));
		
		log.info("SecurityTest_24 - Step 08. Open Properties tab");
		loansPage.openPropertiesTab();
		
		log.info("SecurityTest_24 - Step 09. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("SecurityTest_24 - Step 10. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);
		
		log.info("SecurityTest_24 - Step 11. Open Document type detail");
		propertiesPage.openDocumentTypeDetail(documentType2);
		
		log.info("SecurityTest_24 - Step 12. Select document type");
		propertiesPage.selectDocumentType(documentType2);
		
		log.info("SecurityTest_24 - Step 13. Upload document file");
		propertiesPage.uploadDocumentFile(documentFileName);
		
		log.info("SecurityTest_24 - Step 14. Click save button");
		propertiesPage.clickSaveButton();
		
		log.info("SecurityTest_24 - Step 15. Click Go To Property button");
		propertiesPage.clickGoToPropertyButton();
		
		log.info("VP: Document is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType2, documentFileName));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private UserLoginsPage userLoginsPage;
	private TermsOfUsePage termsOfUsePage;
	private LendersPage lendersPage;
	private PropertiesPage propertiesPage;
	private String usernameUser, passwordUser, loginId, lenderName, loanName, address1, documentFileName;
	private String accountNumber, loanApplication, password, applicantName, propertiesFileName;
	private String userRole, securityAnswer, lastName, userEmail, loanStatus,documentType2;
}