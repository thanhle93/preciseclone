package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.DocumentsPage;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_009_B2R_BorrowerTestCase extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		usernameBorrower = Constant.USERNAME_B2R_BORROWER;
		passwordBorrower = Constant.PASSWORD_B2R_BORROWER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan"+accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower";
		documentsSection = "General Loan Documents - Property Management";
		documentsType = "L32 - Management Agreement";
		documentFileName = "datatest.pdf";
		documentSectionSearch = "Property Management";
	}
	
	@Test(groups = { "regression" },description = "Borrower01 - login through the Applicant opened in the loan")
	public void BorrowerTest_01_BorrowerCanSeeHisLoan()
	{
		log.info("BorrowerTest_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("BorrowerTest_01 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);
			
		log.info("BorrowerTest_01 - Step 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);
		
		log.info("VP: Applicant displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(applicantName));
		
		log.info("BorrowerTest_01 - Step 04. Login with applicant");
		loginPage = logout(driver, ipClient);
		loansPage = loginPage.loginAsBorrower(usernameBorrower, passwordBorrower, false);
		
		log.info("BorrowerTest_01 - Step 05. Search loan item");
		loansPage.searchLoanByName(loanName);
		
		log.info("VP: The loan item displays on table");
		verifyTrue(loansPage.isLoansDisplayOnSearchLoanPage(loanName));
	}
	
	@Test(groups = { "regression" },description = "Borrower02 - Borrower upload loan documents")
	public void BorrowerTest_02_BorrowerUploadLoanDocuments()
	{
		log.info("BorrowerTest_02 - Step 01. Open loan item");
		loansPage.openLoansDetailPage(loanName);
		
		log.info("BorrowerTest_02 - Step 02. Open General Loan Documents tab");
		documentsPage = loansPage.openGeneralDocumentTab();
		
		log.info("BorrowerTest_02 - Step 03. Click new button");
		documentsPage.clickNewButton();
		
		log.info("BorrowerTest_02 - Step 04. Select section value");
		documentsPage.selectGeneralDocumentSection(documentsSection);
		
		log.info("BorrowerTest_02 - Step 05. Select document type");
		documentsPage.selectGeneralDocumentType(documentsType);
		
		log.info("BorrowerTest_02 - Step 06. Upload general document");
		documentsPage.uploadDocumentFile(documentFileName);
		
		log.info("BorrowerTest_02 - Step 07. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("BorrowerTest_02 - Step 08. Click Document List button");
		documentsPage.clickDocumentListButton();
		
		log.info("BorrowerTest_02 - Step 09. Click 'Section' search");
		documentsPage.searchSectionAndDocumentType(documentSectionSearch, "");
		
		log.info("VP: Document is loaded to loan item");
		verifyTrue(documentsPage.isDocumentUploadedToDocumentType(documentsType));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private DocumentsPage documentsPage;
	private String usernameLender, passwordLender, loanStatus, passwordBorrower, documentsType;
	private String loanName, accountNumber, applicantName, usernameBorrower, documentsSection;
	private String documentFileName, documentSectionSearch;
}