package sanityTest_Store;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;

import common.AbstractTest;
import common.Constant;

public class sanityTest_001_STO_CreateNewAndDownloadListTransaction extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileName = "Transaction New.xml";
		productType = "Lease";
		portfolioCompany = "STORE Capital Corporation";
		companyName = "Testing QA " + getUniqueNumber();
		leaseExpirationDate = "12/31/2030";
		firstPaymentDate = "1/1/2016";
		capRate = "100";
		initialRent = "444";
		csvExtension = "List of Transactions.csv";
	}

	@Test(groups = { "regression" }, description = "New And Download Transaction 01 - Add new Transaction (Load Transaction by xml file)")
	public void NewAndDownloadTransations_01_LoadTransactionByXMLFile() {
		log.info("NewAndDownloadTransations_01 - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("NewAndDownloadTransations_01 - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();
		
		log.info("NewAndDownloadTransations_01 - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();
		
		log.info("NewAndDownloadTransations_01 - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);
		
		log.info("VP: 'Load data has been done' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Load data has been done"));
	}
	
	@Test(groups = { "regression" }, description = "New And Download Transaction 02 - Add new Transaction (Enter required data into screen)")
	public void NewAndDownloadTransations_02_EnterRequiredDataIntoScreen() {
		
		log.info("NewAndDownloadTransations_02 - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("NewAndDownloadTransations_02 - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();
		
		log.info("NewAndDownloadTransations_02 - Step 03: Select 'Product Type' = Lease");
		transactionPage.selectProductType(productType);
		
		log.info("NewAndDownloadTransations_02 - Step 04: Input data into 'Company' field");
		transactionPage.inputCompanyName(companyName);
		
		log.info("NewAndDownloadTransations_02 - Step 05: Input data into 'Portfolio Company' field");
		transactionPage.inputPortfolioCompany(portfolioCompany);
		
		log.info("NewAndDownloadTransations_02 - Step 06: Input data into 'Lease Expiration Date' field");
		transactionPage.inputLeaseExpirationDate(leaseExpirationDate);
		
		log.info("NewAndDownloadTransations_02 - Step 07: Input data into '1st Payment Date' field");
		transactionPage.inputFirstPaymentDate(firstPaymentDate);
		
		log.info("NewAndDownloadTransations_02 - Step 08: Input data into 'Cap Rate' field");
		transactionPage.inputCapRate(capRate);
		
		log.info("NewAndDownloadTransations_02 - Step 09: Input data into 'Initial Rent' field");
		transactionPage.inputInitialRent(initialRent);
		
		log.info("NewAndDownloadTransations_02 - Step 09: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: Verify that 'Product Type' is saved successfully");
		verifyTrue(transactionPage.isProductTypeSavedSuccessfully(productType));
		
		log.info("VP: Verify that 'Company Name' is saved successfully");
		verifyTrue(transactionPage.isCompanyNameSavedSuccessfully(companyName));
		
		log.info("VP: Verify that 'Portfolio Company' is saved successfully");
		verifyTrue(transactionPage.isPortfolioCompanySavedSuccessfully(portfolioCompany));
		
		log.info("VP: Verify that 'Lease Expiration Date' is saved successfully");
		verifyTrue(transactionPage.isLeaseExpirationDateSavedSuccessfully(leaseExpirationDate));
		
		log.info("VP: Verify that '1st Payment Date' is saved successfully");
		verifyTrue(transactionPage.isFirstPaymentDateSavedSuccessfully(firstPaymentDate));
		
		log.info("VP: Verify that 'Cap Rate' is saved successfully");
		verifyTrue(transactionPage.isCapRateSavedSuccessfully(capRate + "%"));
		
		log.info("VP: Verify that 'Initial Rent' is saved successfully");
		verifyTrue(transactionPage.isInitialRentSavedSuccessfully("$" + initialRent + ".00"));
	}
	
	@Test(groups = { "regression" }, description = "New And Download Transaction 02 - Download list transaction (Excel file that is created)")
	public void NewAndDownloadTransations_03_DownloadListTransaction() {
		
		log.info("NewAndDownloadTransations_03 - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("NewAndDownloadTransations_03 - Step 02: Click 'Download CSV' image");
		transactionPage.deleteAllFileInFolder();
		transactionPage.clickOnDownloadCSVImage();
		
		log.info("NewAndDownloadTransations_03 - Step 03. Wait for file downloaded complete");
		transactionPage.waitForDownloadFileFullnameCompleted(csvExtension);
		
		log.info("VP: File number in folder equal 1");
		countFile = transactionPage.countFilesInDirectory();
		verifyEquals(countFile, 1);
		
		log.info("NewAndDownloadTransations_03 - Step 04. Delete the downloaded file");
		transactionPage.deleteContainsFileName(csvExtension);
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName, portfolioCompany, csvExtension;
	private String productType, leaseExpirationDate, firstPaymentDate, capRate, initialRent;
	private int countFile;
	}