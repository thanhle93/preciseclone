package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_035_B2R_BucketAtDashboardPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		status01 = "Pre-RentRoll";
		status02 = "Rent Roll Received";
		status03 = "Loan Sizing";
		status04 = "Desk Pricing";
		status05 = "Quoted";
		status06 = "Term Sheet Sent";
		status07 = "Deposit Received";
		status08 = "Due Diligence";
		status09 = "Underwriting";
		status10 = "Committee Approved";
		status11 = "In Closing";
		status12 = "Funded";
		status13 = "Post Closing";
		status14 = "Hold";
		status15 = "Dead";
	}

	@Test(groups = { "regression" }, description = "Dashboard 01 - Pre-Rent Roll Status")
	public void Dashboard_01_PreRentRollStatus() {
		log.info("Dashboard_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Dashboard_01 - Step 02. Select Loan status is 'Pre-Rent Roll'");
		homePage.selectDefaultLoanStatus(status01);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status01));

		log.info("Dashboard_01 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status01);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucketB2R(totalNumberLoan));

		log.info("Dashboard_01 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status01);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}

	@Test(groups = { "regression" }, description = "Dashboard 02 - Rent Roll Received Status")
	public void Dashboard_02_RentRollReceivedStatus() {
		
		log.info("Dashboard_02 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_02 - Step 02. Select Loan status is 'Rent Roll Received'");
		homePage.selectDefaultLoanStatus(status02);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status02));

		log.info("Dashboard_02 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status02);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucketB2R(totalNumberLoan));

		log.info("Dashboard_02 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status02);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 03 - Loan Sizing Status")
	public void Dashboard_03_LoanSizingStatus() {
		
		log.info("Dashboard_03 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_03 - Step 02. Select Loan status is 'Loan Sizing'");
		homePage.selectDefaultLoanStatus(status03);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status03));

		log.info("Dashboard_03 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status03);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucketB2R(totalNumberLoan));

		log.info("Dashboard_03 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status03);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 04 - Desk Pricing Status")
	public void Dashboard_04_DeskPricingStatus() {
		
		log.info("Dashboard_04 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_04 - Step 02. Select Loan status is 'Desk Pricing'");
		homePage.selectDefaultLoanStatus(status04);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status04));

		log.info("Dashboard_04 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status04);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucketB2R(totalNumberLoan));

		log.info("Dashboard_04 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status04);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 05 - Quoted Status")
	public void Dashboard_05_QuotedStatus() {
		
		log.info("Dashboard_05 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_05 - Step 02. Select Loan status is 'Quoted'");
		homePage.selectDefaultLoanStatus(status05);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status05));

		log.info("Dashboard_05 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status05);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucketB2R(totalNumberLoan));

		log.info("Dashboard_05 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status05);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 06 - Term Sheet Sent Status")
	public void Dashboard_06_TermSheetSentStatus() {
		
		log.info("Dashboard_06 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_06 - Step 02. Select Loan status is 'Term Sheet Sent'");
		homePage.selectDefaultLoanStatus(status06);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status06));

		log.info("Dashboard_06 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status06);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucketB2R(totalNumberLoan));

		log.info("Dashboard_06 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status06);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 07 - Deposit Received Status")
	public void Dashboard_07_DepositReceivedStatus() {
		
		log.info("Dashboard_07 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_07 - Step 02. Select Loan status is 'Deposit Received'");
		homePage.selectDefaultLoanStatus(status07);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status07));

		log.info("Dashboard_07 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status07);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucketB2R(totalNumberLoan));

		log.info("Dashboard_07 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status07);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 08 - Due Diligence Status")
	public void Dashboard_08_DueDiligenceStatus() {
		
		log.info("Dashboard_08 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_08 - Step 02. Select Loan status is 'Due Diligence'");
		homePage.selectDefaultLoanStatus(status08);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status08));

		log.info("Dashboard_08 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status08);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucketB2R(totalNumberLoan));

		log.info("Dashboard_08 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status08);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 09 - Underwriting Status")
	public void Dashboard_09_UnderwritingStatus() {
		
		log.info("Dashboard_09 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_09 - Step 02. Select Loan status is 'Underwriting'");
		homePage.selectDefaultLoanStatus(status09);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status09));

		log.info("Dashboard_09 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status09);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucketB2R(totalNumberLoan));

		log.info("Dashboard_09 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status09);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 10 - Committee Approved Status")
	public void Dashboard_10_CommitteeApprovedStatus() {
		
		log.info("Dashboard_10 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_10 - Step 02. Select Loan status is 'Committee Approved'");
		homePage.selectDefaultLoanStatus(status10);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status10));

		log.info("Dashboard_10 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status10);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucketB2R(totalNumberLoan));

		log.info("Dashboard_10 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status10);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 11 - In Closing Status")
	public void Dashboard_11_InClosingStatus() {
		
		log.info("Dashboard_11 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_11 - Step 02. Select Loan status is 'In Closing'");
		homePage.selectDefaultLoanStatus(status11);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status11));

		log.info("Dashboard_11 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status11);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucketB2R(totalNumberLoan));

		log.info("Dashboard_11 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status11);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 12 - Funded Status")
	public void Dashboard_12_FundedStatus() {
		
		log.info("Dashboard_12 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_12 - Step 02. Select Loan status is 'Funded'");
		homePage.selectDefaultLoanStatus(status12);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status12));

		log.info("Dashboard_12 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status12);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucketB2R(totalNumberLoan));

		log.info("Dashboard_12 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status12);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 13 - Post Closing Status")
	public void Dashboard_13_PostClosingStatus() {
		
		log.info("Dashboard_13 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_13 - Step 02. Select Loan status is 'Post Closing'");
		homePage.selectDefaultLoanStatus(status13);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status13));

		log.info("Dashboard_13 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status13);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucketB2R(totalNumberLoan));

		log.info("Dashboard_13 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status13);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 14 - Hold Status")
	public void Dashboard_14_HoldStatus() {
		
		log.info("Dashboard_14 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_14 - Step 02. Select Loan status is 'Hold'");
		homePage.selectDefaultLoanStatus(status14);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status14));

		log.info("Dashboard_14 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status14);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucketB2R(totalNumberLoan));

		log.info("Dashboard_14 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status14);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 15 - Dead Status")
	public void Dashboard_15_DeadStatus() {
		
		log.info("Dashboard_15 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_15 - Step 02. Select Loan status is 'Dead'");
		homePage.selectDefaultLoanStatus(status15);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status15));

		log.info("Dashboard_15 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status15);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucketB2R(totalNumberLoan));

		log.info("Dashboard_15 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status15);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender, status01, status02, status03, status04, status05, status06;
	private String status07, status08, status09, status10, status11, status12, status13, status14, status15;
	private int totalNumberLoan;
}