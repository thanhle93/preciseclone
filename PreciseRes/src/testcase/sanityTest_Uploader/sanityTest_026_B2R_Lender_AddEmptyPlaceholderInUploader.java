package sanityTest_Uploader;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.UploaderLoginPage;
import page.UploaderPage;

import common.AbstractTest;
import common.Constant;

public class sanityTest_026_B2R_Lender_AddEmptyPlaceholderInUploader extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		loanName = "UploaderUdiTesting " + getUniqueNumber();
		applicantName = "Consolidation Demo Borrower New";
		loanStatus = Constant.LOAN_STATUS_B2R;
		propertiesFileName = "B2RDataTape_multi-unit.xlsx";
		fileNameDocument1 = "datatest.pdf";
		documentType1 = "P5 - Zoning Compliance Letter";
		documentType2 = "P9 - Marked Up Title Commitment";
		property1Name = "Oakhill";
		property2Name = "37225 OAKHILL ST - suite 15";
		property3Name = "Rosemarie";
		property4Name = "37913 ROSEMARIE ST - suite 34";
		property5Name = "10105 Spring ST";
		documentType3 = "Add Empty Placeholders " + getUniqueNumber();
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}
	
	@Test(groups = { "regression" },description = "UploaderLender28 - Add an empty placeholder to uploader - without a document")
	public void Uploader_24_AddEmptyPlaceholderToUploader()
	{
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("Precondition 02. Create new loans and get loans' id");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);

		log.info("Precondition 03. Upload data tape");
		loansPage.openPropertiesTab();
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("Precondition 04. Click on logged user name on the top right corner");
//		loansPage.clickOnLoggedUserName(driver);
//		uploaderPageUrl = loansPage.getUploaderPageUrl(driver);
//		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
		
		log.info("Precondition 05. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("Precondition 06. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameLender, passwordLender);
		
		log.info("Precondition 07. Click on 'Keywords' button");
		uploaderPage.clickOnKeywordsButton();
		
		log.info("Precondition 08. Add new Keywords");
		uploaderPage.enterNewKeyword(documentType3);
				
		log.info("Precondition 09. Click Ok button");
		uploaderPage.clickOkButtonOnKeywords();
		
		log.info("Uploader_24 - Step 01. Select loans item");
		uploaderPage.clickChangeGroupedView();
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("Uploader_24 - Step 02. Open Property folder");
		uploaderPage.clickPropertiesFolder();
		
		log.info("Uploader_24 - Step 03. Select 'Oakhill' property");
		uploaderPage.selectPropertyFolder(property1Name);
		
		log.info("Uploader_24 - Step 04. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherNoDocumentsUpload();
		
		log.info("Uploader_24 - Step 05. Select New document type");
		uploaderPage.selectDocumentType(documentType1);
		
		log.info("Uploader_24 - Step 06. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType1));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender29 - Load a document to this empty placeholder")
	public void Uploader_25_LoadDocumentToThisEmptyPlaceholder()
	{
		log.info("Uploader_25 - Step 01. Upload document for document type");
		uploaderPage.clickOnSelectNewFileIconByType(documentType1);
		uploaderPage.uploadFileOnUploader(fileNameDocument1);
		
		log.info("Uploader_25 - Step 02. Wait for upload complete");
		uploaderPage.waitForUploadCompleteOnUploader(fileNameDocument1);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(fileNameDocument1));
		
		log.info("VP: The document type display correctly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, documentType1));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender30 - Add a placeholder to uploader - attached document")
	public void Uploader_26_AddPlaceholderAttachedDocumentToUploader()
	{
		log.info("Uploader_26 - Step 01. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherNoDocumentsUpload();
		
		log.info("Uploader_26 - Step 02. Select New document type");
		uploaderPage.selectDocumentType(documentType2);
		
		log.info("Uploader_26 - Step 03. Click Load File button");
		uploaderPage.clickLoadFileButton();
		
		log.info("Uploader_26 - Step 04. Select file to upload");
		uploaderPage.uploadFileOnUploader(fileNameDocument1);
		
		log.info("Uploader_26 - Step 05. Click 'OK' Button");
		uploaderPage.waitForUploadCompleteOnUploader(fileNameDocument1);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(fileNameDocument1));
		
		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, documentType2));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender31 - Add a placeholder to All Properties")
	public void Uploader_27_AddPlaceholderToAllProperties()
	{		
		log.info("Uploader_27 - Step 01. Click 'Add Placeholder to All Properties' button");
		uploaderPage.clickAddPlaceholderToAllProperties();
		
		log.info("Uploader_27 - Step 02. Select New document type");
		uploaderPage.selectDocumentType(documentType3);
		
		log.info("Uploader_27 - Step 03. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.waitForUploadCompleteOnUploader(documentType3);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType3));
		
		log.info("Uploader_27 - Step 04. Expand 'Oakhill' group");
		uploaderPage.expandPropertyGroup(property1Name);
		
		log.info("Uploader_27 - Step 05. Select 'Oakhill' property");
		uploaderPage.selectPropertyFolder(property1Name);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType3));
		
		log.info("Uploader_27 - Step 06. Select '37225 OAKHILL ST - suite 15' property");
		uploaderPage.selectSubPropertyFolder(property2Name);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType3));
		
		log.info("Uploader_27 - Step 07. Expand 'Rosemarie' group");
		uploaderPage.expandPropertyGroup(property3Name);
		
		log.info("Uploader_27 - Step 08. Select 'Rosemarie' property");
		uploaderPage.selectPropertyFolder(property3Name);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType3));
		
		log.info("Uploader_27 - Step 09. Select '37913 ROSEMARIE ST - suite 34' property");
		uploaderPage.selectSubPropertyFolder(property4Name);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType3));
		
		log.info("Uploader_27 - Step 10. Select '10105 Spring ST' property");
		uploaderPage.selectPropertyFolder(property5Name);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType3));
		
		log.info("Uploader_27 - Step 11. Click on 'Keywords' button");
		uploaderPage.clickOnKeywordsButton();
		
		log.info("Uploader_27 - Step 12. Delete new Keywords");
		uploaderPage.deleteNewKeyword(documentType3);
		
		log.info("Uploader_27 - Step 13. Click Ok button");
		uploaderPage.clickOkButtonOnKeywords();
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender, applicantName, loanStatus;
	private String loanName, uploaderPageUrl, propertiesFileName, fileNameDocument1, documentType1, documentType2, documentType3;
	private String property1Name, property2Name, property3Name, property4Name, property5Name;
}