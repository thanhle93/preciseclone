package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_004_B2R_CreateNewLoanApplicationEntities extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan"+accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower New";
		guarantorName = "uditeam-guarantor"+accountNumber;
		borrowerName = "uditeam-borrower"+accountNumber;
		sponsorName = "uditeam-sponsor"+accountNumber;
		pledgorName = "uditeam-pledgor"+accountNumber;
		addressNumber = "street"+accountNumber;
		addressDictrict = "dictrict"+accountNumber;
		city = "Los Angeles";
		state = "CA - California";
		zipCode = "90001";
	}
	
	@Test(groups = { "regression" },description = "Loan37 - Create new Guarantor for Loan")
	public void NewLoanApplicantEntity_01_CreateNewGuarantorForLoan()
	{
		log.info("NewLoanApplicantEntity_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("NewLoanApplicantEntity_01 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		log.info("NewLoanApplicantEntity_01 - Step 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);
		
		log.info("VP: Loan item display with correct info");
		verifyTrue(loansPage.isLoansReadOnlyDisplayWithCorrectInfo(loanName, loanStatus));
		
		log.info("VP: Applicant displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(applicantName));
		
		log.info("NewLoanApplicantEntity_01 - Step 04. Click new loan application entity");
		loansPage.clickNewLoanApplicationEntity();
		
		log.info("NewLoanApplicantEntity_01 - Step 05. Select Guarantor in Entity type dropdown list");
		loansPage.selectEntityType("Guarantor");
		
		log.info("NewLoanApplicantEntity_01 - Step 06. Click new Guarantor link");
		loansPage.clickNewGuarantorLink();
		loansPage.switchToNewGuarantorEntityFrame(driver);
		
		log.info("NewLoanApplicantEntity_01 - Step 07. Input Guarantor information in Create new Guarantor popup");
		loansPage.inputGuarantorInfoToCreate(accountNumber, guarantorName, addressNumber, addressDictrict, city, state, zipCode);
		
		log.info("NewLoanApplicantEntity_01 - Step 08. Click save button in Create new Guarantor popup");
		loansPage.clickSaveNewGuarantorButton();
		loansPage.switchToTopWindowFrame(driver);
		
		log.info("NewLoanApplicantEntity_01 - Step 09. Click Save New Loan Application Entity");
		loansPage.clickSaveLoansApplicantEntityButton();
		
		log.info("VP: New Guarantor is saved");
		verifyTrue(loansPage.isNewGuarantorSaved(guarantorName));
		
		log.info("NewLoanApplicantEntity_01 - Step 10. Click Back button on New Loan Application Entity");
		loansPage.clickBackButton();	
		
		log.info("VP: The Guarantor displays on Loan Application Entities table");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(guarantorName));
	}
	
	@Test(groups = { "regression" },description = "Loan38 - Create new Borrower for Loan")
	public void NewLoanApplicantEntity_02_CreateNewBorrowerForLoan()
	{
		log.info("NewLoanApplicantEntity_02 - Step 01. Click new loan application entity");
		loansPage.clickNewLoanApplicationEntity();
		
		log.info("NewLoanApplicantEntity_02 - Step 02. Select Borrower in Entity type dropdown list");
		loansPage.selectEntityType("Borrower");
		
		log.info("NewLoanApplicantEntity_02 - Step 03. Click new Borrower link");
		loansPage.clickNewBorrowerLink();
		loansPage.switchToNewBorrowerEntityFrame(driver);
		
		log.info("NewLoanApplicantEntity_02 - Step 04. Input Borrower information in Create new Borrower popup");
		loansPage.inputBorrowerInfoToCreate(accountNumber, borrowerName, addressNumber,addressDictrict, city, state, zipCode);
		
		log.info("NewLoanApplicantEntity_02 - Step 05. Click save button in Create new Borrower popup");
		loansPage.clickSaveNewBorrowerButton();
		loansPage.switchToTopWindowFrame(driver);
		
		log.info("NewLoanApplicantEntity_02 - Step 06. Click Save New Loan Application Entity");
		loansPage.clickSaveLoansApplicantEntityButton();
		
		log.info("VP: New Borrower is saved");
		verifyTrue(loansPage.isNewBorrowerSaved(borrowerName));
		
		log.info("NewLoanApplicantEntity_02 - Step 07. Click Back button on New Loan Applicant Entity");
		loansPage.clickBackButton();	
		
		log.info("VP: The Borrower displays on Loan Application Entities table");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(borrowerName));
	}
	
	@Test(groups = { "regression" },description = "Loan39 - Create new Sponsor for Loan")
	public void NewLoanApplicantEntity_03_CreateNewSponsorForLoan()
	{
		log.info("NewLoanApplicantEntity_03 - Step 01. Click new loan application entity");
		loansPage.clickNewLoanApplicationEntity();
		
		log.info("NewLoanApplicantEntity_03 - Step 02. Select Sponsor in Entity type dropdown list");
		loansPage.selectEntityType("Sponsor");
		
		log.info("NewLoanApplicantEntity_03 - Step 03. Click new Sponsor link");
		loansPage.clickNewSponsorLink();
		loansPage.switchToNewSponsorEntityFrame(driver);
		
		log.info("NewLoanApplicantEntity_03 - Step 04. Input Sponsor information in Create new Sponsor popup");
		loansPage.inputSponsorInfoToCreate(accountNumber, sponsorName, addressNumber,addressDictrict, city, state, zipCode);
		
		log.info("NewLoanApplicantEntity_03 - Step 05. Click save button in Create new Sponsor popup");
		loansPage.clickSaveNewSponsorButton();
		loansPage.switchToTopWindowFrame(driver);
		
		log.info("NewLoanApplicantEntity_03 - Step 06. Click Save New Loan Application Entity");
		loansPage.clickSaveLoansApplicantEntityButton();
		
		log.info("VP: New Sponsor is saved");
		verifyTrue(loansPage.isNewSponsorSaved(sponsorName));
		
		log.info("NewLoanApplicantEntity_03 - Step 07. Click Back button on New Loan Applicant Entity");
		loansPage.clickBackButton();	
		
		log.info("VP: The Sponsor displays on Loan Application Entities table");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(sponsorName));
	}
	
	@Test(groups = { "regression" },description = "Loan40 - Create new Pledgor for Loan")
	public void NewLoanApplicantEntity_04_CreateNewPledgorForLoan()
	{
		log.info("NewLoanApplicantEntity_04 - Step 01. Click new loan application entity");
		loansPage.clickNewLoanApplicationEntity();
		
		log.info("NewLoanApplicantEntity_04 - Step 02. Select Pledgor in Entity type dropdown list");
		loansPage.selectEntityType("Pledgor");
		
		log.info("NewLoanApplicantEntity_04 - Step 03. Click new Pledgor link");
		loansPage.clickNewPledgorLink();
		loansPage.switchToNewPledgorEntityFrame(driver);
		
		log.info("NewLoanApplicantEntity_04 - Step 04. Input Pledgor information in Create new Pledgor popup");
		loansPage.inputPledgorInfoToCreate(accountNumber, pledgorName, addressNumber,addressDictrict, city, state, zipCode);
		
		log.info("NewLoanApplicantEntity_04 - Step 05. Click save button in Create new Pledgor popup");
		loansPage.clickSaveNewPledgorButton();
		loansPage.switchToTopWindowFrame(driver);
		
		log.info("NewLoanApplicantEntity_04 - Step 06. Click Save New Loan Application Entity");
		loansPage.clickSaveLoansApplicantEntityButton();
		
		log.info("VP: New Pledgor is saved");
		verifyTrue(loansPage.isNewPledgorSaved(pledgorName));
		
		log.info("NewLoanApplicantEntity_04 - Step 07. Click Back button on New Loan Applicant Entity");
		loansPage.clickBackButton();	
		
		log.info("VP: The Pledgor displays on Loan Application Entities table");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(pledgorName));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender, addressDictrict, loanStatus;
	private String loanName, accountNumber, applicantName, addressNumber;
	private String city, state, zipCode;
	private String borrowerName, sponsorName, pledgorName, guarantorName;
}