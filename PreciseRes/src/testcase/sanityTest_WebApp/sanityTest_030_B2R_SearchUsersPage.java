package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Constant;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.UsersPage;

public class sanityTest_030_B2R_SearchUsersPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		applicantName = "Users " + accountNumber;
		firstName = "Applicant" + accountNumber;
		lastName = "B2R" + accountNumber;
		emailContact = "ApplicantCAF" + accountNumber + "@gmail.com";
		city = "Colorado";
		state = "CO - Colorado";
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
	}

	@Test(groups = { "regression" }, description = "SearchUsersPage 01 - Search by User Legal Name")
	public void SearchUsersPage_01_SearchByUserLegalName() {
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Precondition 02. Open Users page");
		usersPage = homePage.openUsersPage(driver, ipClient);

		log.info("Precondition 03. Click New button");
		usersPage.clickNewButton();

		log.info("Precondition 04. Input Applicant information");
		usersPage.inputApplicantInfo(applicantName, applicantName);

		log.info("Precondition 05. Click Next button");
		usersPage.clickNextButton();

		log.info("VP: Applicant details is saved");
		verifyTrue(usersPage.isApplicantDetailPageSaved());

		log.info("Precondition 06. Open Users page");
		usersPage.openUsersPage(driver, ipClient);

		log.info("Precondition 07.  Search Applicant");
		usersPage.searchUserLegalName(applicantName);

		log.info("Precondition 08.  Open Applicant detail");
		usersPage.openApplicantsDetailPage(applicantName);

		log.info("Precondition 09. Input City and State information");
		usersPage.inputCityAndStateInfo(city, state);

		log.info("Precondition 10. Click Save button");
		usersPage.clickNextButton();

		log.info("VP: Applicant details is saved");
		verifyTrue(usersPage.isApplicantDetailPageSaved());

		log.info("Precondition 11. Click new contact detail");
		usersPage.clickNewContactDetail();
		usersPage.switchToApplicantContactFrame(driver);

		log.info("Precondition 12. Input Applicant Contact info");
		usersPage.inputApplicantContactInfo(firstName, lastName, emailContact);

		log.info("Precondition 13. Click Save Applicant Contact Details");
		usersPage.clickSaveApplicantContact();
		usersPage.switchToTopWindowFrame(driver);

		log.info("Precondition 14. Click Save Applicant Details");
		usersPage.clickSaveApplicantDetail();

		log.info("VP: Applicant details is saved");
		verifyTrue(usersPage.isApplicantDetailPageSaved());

		log.info("Precondition 15. Open Loans Page");
		loansPage = usersPage.openLoansPage(driver, ipClient);

		log.info("Precondition 16. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);

		log.info("Precondition 17. Open Users page");
		usersPage = loansPage.openUsersPage(driver, ipClient);

		log.info("SearchUsersPage_01 - Step 01. Search with User Legal Name");
		usersPage.searchUserLegalName(applicantName);

		log.info("VP: User Legal Name display in Users search table");
		verifyTrue(usersPage.isAllValueDisplayOnSearchUsersTable(applicantName, "Applicant", "1", firstName + "." + lastName, emailContact, city, "CO"));
		userLogin = usersPage.getUserLogin();
	}

	@Test(groups = { "regression" }, description = "SearchUsersPage 02 - Search by Name")
	public void SearchUsersPage_02_SearchByName() {

		log.info("SearchUsersPage_02 - Step 01. Search with Name");
		usersPage.openUsersPage(driver, ipClient);
		usersPage.searchName(applicantName);

		log.info("VP: Name display in Users search table");
		verifyTrue(usersPage.isAllValueDisplayOnSearchUsersTable(applicantName, "Applicant", "1", firstName + "." + lastName, emailContact, city, "CO"));
	}

	@Test(groups = { "regression" }, description = "SearchUsersPage 03 - Search by Entity Type")
	public void SearchUsersPage_03_SearchByEntityType() {

		log.info("SearchUsersPage_03 - Step 01. Search with Entity Type is Applicant");
		usersPage.openUsersPage(driver, ipClient);
		usersPage.searchEntityType(applicantName, "Applicant");

		log.info("VP: Entity Type with Applicant display in Users search table");
		verifyTrue(usersPage.isAllValueDisplayOnSearchUsersTable(applicantName, "Applicant", "1", firstName + "." + lastName, emailContact, city, "CO"));

		log.info("SearchUsersPage_03 - Step 02. Search with Entity Type is Borrower");
		usersPage.searchEntityType("UdiTeamBorrower", "Borrower");

		log.info("VP: Entity Type with Borrower display in Users search table");
		verifyTrue(usersPage.isNameAndEntityDisplayOnSearchUsersTable("UdiTeamBorrower", "Borrower"));

		log.info("SearchUsersPage_03 - Step 03. Search with Entity Type is Pledgor");
		usersPage.searchEntityType("UdiTeamPledgor", "Pledgor");

		log.info("VP: Entity Type with Pledgor display in Users search table");
		verifyTrue(usersPage.isNameAndEntityDisplayOnSearchUsersTable("UdiTeamPledgor", "Pledgor"));

		log.info("SearchUsersPage_03 - Step 04. Search with Entity Type is Guarantor");
		usersPage.searchEntityType("UdiTeamGuarantor", "Guarantor");

		log.info("VP: Entity Type with Guarantor display in Users search table");
		verifyTrue(usersPage.isNameAndEntityDisplayOnSearchUsersTable("UdiTeamGuarantor", "Guarantor"));

		log.info("SearchUsersPage_03 - Step 05. Search with Entity Type is Sponsor");
		usersPage.searchEntityType("UdiTeamSponsor", "Sponsor");

		log.info("VP: Entity Type with Sponsor display in Users search table");
		verifyTrue(usersPage.isNameAndEntityDisplayOnSearchUsersTable("UdiTeamSponsor", "Sponsor"));
	}

	@Test(groups = { "regression" }, description = "SearchUsersPage 04 - Search by User Login")
	public void SearchUsersPage_04_SearchByUserLogin() {

		log.info("SearchUsersPage_04 - Step 01. Search with User Login");
		usersPage.openUsersPage(driver, ipClient);
		usersPage.searchUserLogin(applicantName, userLogin);

		log.info("VP: User Login display in Users search table");
		verifyTrue(usersPage.isAllValueDisplayOnSearchUsersTable(applicantName, "Applicant", "1", firstName + "." + lastName, emailContact, city, "CO"));
	}

	@Test(groups = { "regression" }, description = "SearchUsersPage 05 - Search by Email")
	public void SearchUsersPage_05_SearchByEmail() {

		log.info("SearchUsersPage_05 - Step 01. Search with Email");
		usersPage.openUsersPage(driver, ipClient);
		usersPage.searchEmail(applicantName, emailContact);

		log.info("VP: Email display in Users search table");
		verifyTrue(usersPage.isAllValueDisplayOnSearchUsersTable(applicantName, "Applicant", "1", firstName + "." + lastName, emailContact, city, "CO"));
	}

	@Test(groups = { "regression" }, description = "SearchUsersPage 06 - Search by City")
	public void SearchUsersPage_06_SearchByCity() {

		log.info("SearchUsersPage_06 - Step 01. Search with City");
		usersPage.openUsersPage(driver, ipClient);
		usersPage.searchCity(applicantName, city);

		log.info("VP: City display in Users search table");
		verifyTrue(usersPage.isAllValueDisplayOnSearchUsersTable(applicantName, "Applicant", "1", firstName + "." + lastName, emailContact, city, "CO"));
	}

	@Test(groups = { "regression" }, description = "SearchUsersPage 07 - Search by State")
	public void SearchUsersPage_07_SearchByState() {

		log.info("SearchUsersPage_07 - Step 01. Search with State");
		usersPage.openUsersPage(driver, ipClient);
		usersPage.searchState(applicantName, state);

		log.info("VP: State display in Users search table");
		verifyTrue(usersPage.isAllValueDisplayOnSearchUsersTable(applicantName, "Applicant", "1", firstName + "." + lastName, emailContact, city, "CO"));
	}
	
	@Test(groups = { "regression" }, description = "SearchUsersPage 08 - Search by Loan")
	public void SearchUsersPage_08_SearchByLoan() {

		log.info("SearchUsersPage_08 - Step 01. Search with Loan");
		usersPage.openUsersPage(driver, ipClient);
		usersPage.searchLoan(applicantName, loanName);

		log.info("VP: Loan display in Users search table");
		verifyTrue(usersPage.isAllValueDisplayOnSearchUsersTable(applicantName, "Applicant", "1", firstName + "." + lastName, emailContact, city, "CO"));
	}
	    
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private UsersPage usersPage;
	private String usernameLender, passwordLender, loanName, loanStatus;
	private String accountNumber, applicantName, firstName, lastName, emailContact, city, state;
	private String userLogin;
}