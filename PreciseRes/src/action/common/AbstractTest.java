package common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

import page.LoginPage;
import page.PageFactory;
import PreciseRes.Interfaces;
import common.AutomationControl;
import common.Browser;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.Random;
import java.util.UUID;

public abstract class AbstractTest {

	protected boolean verifyTrue(boolean condition, boolean halt) {
		boolean pass = true;
		if (halt == false) {
			try {
				Assert.assertTrue(condition);
			} catch (Throwable e) {
				pass = false;
				VerificationFailures.getFailures().addFailureForTest(Reporter.getCurrentTestResult(), e);
				Reporter.getCurrentTestResult().setThrowable(e);
			}
		} else {
			Assert.assertTrue(condition);
		}
		return pass;
	}

	protected boolean verifyTrue(boolean condition) {
		return verifyTrue(condition, false);
	}

	protected boolean verifyFalse(boolean condition, boolean halt) {
		boolean pass = true;
		if (halt == false) {
			try {
				Assert.assertFalse(condition);
			} catch (Throwable e) {
				pass = false;
				VerificationFailures.getFailures().addFailureForTest(Reporter.getCurrentTestResult(), e);
				Reporter.getCurrentTestResult().setThrowable(e);
			}
		} else {
			Assert.assertFalse(condition);
		}
		return pass;

	}

	protected boolean verifyFalse(boolean condition) {
		return verifyFalse(condition, false);
	}

	protected boolean verifyEquals(Object actual, Object expected, boolean halt) {
		boolean pass = true;
		if (halt == false) {
			try {
				Assert.assertEquals(actual, expected);
			} catch (Throwable e) {
				pass = false;
				VerificationFailures.getFailures().addFailureForTest(Reporter.getCurrentTestResult(), e);
				Reporter.getCurrentTestResult().setThrowable(e);
			}
		} else {
			Assert.assertEquals(actual, expected);
		}
		return pass;
	}

	protected boolean verifyEquals(Object actual, Object expected) {
		return verifyEquals(actual, expected, false);
	}

	protected void refreshBrowser(WebDriver driver) {
		driver.navigate().refresh();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * open browser and navigate to url
	 * 
	 * @param browser
	 * @throws Exception
	 */
	protected WebDriver openBrowser(String browser, String port, String ipClient) {
		Browser br = new Browser();
		WebDriver driver = br.launch(browser, port, ipClient);
		if (driver.toString().toLowerCase().contains("chrome") || driver.toString().toLowerCase().contains("firefox")
				|| driver.toString().toLowerCase().contains("internetexplorer")) {
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			// move mouse to 0,0
			try {
				Robot robot = new Robot();
				// SET THE MOUSE X Y POSITION
				robot.mouseMove(0, 0);
			} catch (AWTException e) {
				e.printStackTrace();
			}
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.focus();");
		}
		driver.manage().timeouts();
		return driver;
	}

	/**
	 * Close browser and kill chromedriver.exe/IEDriverServer.exe process
	 * 
	 * @param driver
	 */
	protected void closeBrowser(WebDriver driver) {
		try {
			driver.quit();
			System.gc();
			if (driver.toString().toLowerCase().contains("chrome")) {
				String cmd = "taskkill /IM chromedriver.exe /F";
				Process process = Runtime.getRuntime().exec(cmd);
				process.waitFor();
			}
			if (driver.toString().toLowerCase().contains("internetexplorer")) {
				String cmd = "taskkill /IM IEDriverServer.exe /F";
				Process process = Runtime.getRuntime().exec(cmd);
				process.waitFor();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Logout page at any current page
	 */
	protected LoginPage logout(WebDriver driver, String ipClient) {
		try {
			String url = "";
			if (Common.getCommon().getLogoutLink().equals("")) {
				Thread.sleep(2000);
				control.setPage("AbstractPage");
				url = control.findElement(driver, Interfaces.AbstractPage.LOGOUT_LINK).getAttribute("href");
				Common.getCommon().setLogoutLink(url);
			} else {
				url = Common.getCommon().getLogoutLink();
			}
			driver.get(url);
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getLoginPage(driver, ipClient);
	}

	/**
	 * Return a random unique string
	 * 
	 * @return unique string
	 */
	protected String getUniqueName() {
		return UUID.randomUUID().toString().replace("-", "");
	}

	/**
	 * Return a random unique number
	 * 
	 * @return unique number
	 */
	protected String getUniqueNumber() {
		Random rand = new Random();
		int number = rand.nextInt(90000000) + 1;
		String numberString = Integer.toString(number);
		return numberString;
	}

	public void clearCookie(WebDriver driver) {
		driver.manage().deleteAllCookies();
	}

	protected AbstractTest() {
		log = LogFactory.getLog(getClass());
	}

	/**
	 * Get aut url
	 * 
	 * @return aut url
	 */
	protected String getPreciseResUrl() {
		return Common.getCommon().getUrl();
	}

	/**
	 * switch Other Window
	 * 
	 * @param driver
	 * @return
	 */
	public WebDriver switchOtherWindow(WebDriver driver) {
		WebDriver tmp = driver;
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (String winHandle : driver.getWindowHandles()) {
			tmp = driver.switchTo().window(winHandle);
		}
		return tmp;
	}

	/**
	 * get Current Driver
	 * 
	 * @param driver
	 * @return
	 */
	public String getCurrentDriver(WebDriver driver) {
		return driver.getWindowHandle();
	}

	/**
	 * switch Current Driver
	 * 
	 * @param driver
	 * @param currentHandle
	 * @return
	 */
	public WebDriver switchCurrentDriver(WebDriver driver, String currentHandle) {
		try {
			driver.close();
			return driver.switchTo().window(currentHandle);
		} catch (Exception ex) {
			System.out.println(ex);
		}
		return driver;
	}

	protected final Log log;
	protected WebDriver driver;
	protected String ipClient;
	protected AutomationControl control = new AutomationControl();
}
