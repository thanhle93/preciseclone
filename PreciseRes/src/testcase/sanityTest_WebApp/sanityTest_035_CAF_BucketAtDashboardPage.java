package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_035_CAF_BucketAtDashboardPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		status01 = "Deals Under Review";
		status02 = "Data Tape Received";
		status03 = "Term Sheet Issued";
		status04 = "Term Sheet Signed/Deposit Collected";
		status05 = "Due Diligence";
		status06 = "Approved by Committee";
		status07 = "Confirmed Funding / Closed";
		status08 = "Rejected";
		status09 = "Borrower Withdrawal";
		status10 = "Lost to First Key";
		status11 = "Lost to B2R";
		status12 = "Lost to Bank";
		status13 = "Cancelled";
	}

	@Test(groups = { "regression" }, description = "Dashboard 01 - Deals Under Review Status")
	public void Dashboard_01_DealsUnderReviewStatus() {
		log.info("Dashboard_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Dashboard_01 - Step 02. Select Loan status is 'Deals Under Review'");
		homePage.selectDefaultLoanStatus(status01);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status01));

		log.info("Dashboard_01 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status01);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucket(totalNumberLoan));

		log.info("Dashboard_01 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status01);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}

	@Test(groups = { "regression" }, description = "Dashboard 02 - Data Tape Received Status")
	public void Dashboard_02_DataTapeReceivedStatus() {
		
		log.info("Dashboard_02 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_02 - Step 02. Select Loan status is 'Data Tape Received'");
		homePage.selectDefaultLoanStatus(status02);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status02));

		log.info("Dashboard_02 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status02);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucket(totalNumberLoan));

		log.info("Dashboard_02 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status02);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 03 - Term Sheet Issued Status")
	public void Dashboard_03_TermSheetIssuedStatus() {
		
		log.info("Dashboard_03 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_03 - Step 02. Select Loan status is 'Term Sheet Issued'");
		homePage.selectDefaultLoanStatus(status03);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status03));

		log.info("Dashboard_03 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status03);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucket(totalNumberLoan));

		log.info("Dashboard_03 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status03);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 04 - Term Sheet Signed/Deposit Collected Status")
	public void Dashboard_04_TermSheetSignedDepositCollectedStatus() {
		
		log.info("Dashboard_04 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_04 - Step 02. Select Loan status is 'Term Sheet Signed/Deposit Collected'");
		homePage.selectDefaultLoanStatus(status04);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status04));

		log.info("Dashboard_04 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status04);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucket(totalNumberLoan));

		log.info("Dashboard_04 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status04);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 05 - Due Diligence Status")
	public void Dashboard_05_DueDiligenceStatus() {
		
		log.info("Dashboard_05 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_05 - Step 02. Select Loan status is 'Due Diligence'");
		homePage.selectDefaultLoanStatus(status05);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status05));

		log.info("Dashboard_05 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status05);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucket(totalNumberLoan));

		log.info("Dashboard_05 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status05);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 06 - Approved by Committee Status")
	public void Dashboard_06_ApprovedByCommitteStatus() {
		
		log.info("Dashboard_06 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_06 - Step 02. Select Loan status is 'Approved by Committee'");
		homePage.selectDefaultLoanStatus(status06);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status06));

		log.info("Dashboard_06 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status06);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucket(totalNumberLoan));

		log.info("Dashboard_06 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status06);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 07 - Confirmed Funding / Closed Status")
	public void Dashboard_07_ConfirmedFundingClosedStatus() {
		
		log.info("Dashboard_07 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_07 - Step 02. Select Loan status is 'Confirmed Funding / Closed'");
		homePage.selectDefaultLoanStatus(status07);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status07));

		log.info("Dashboard_07 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status07);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucket(totalNumberLoan));

		log.info("Dashboard_07 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status07);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 08 - Rejected Status")
	public void Dashboard_08_RejectedStatus() {
		
		log.info("Dashboard_08 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_08 - Step 02. Select Loan status is 'Rejected'");
		homePage.selectDefaultLoanStatus(status08);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status08));

		log.info("Dashboard_08 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status08);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucket(totalNumberLoan));

		log.info("Dashboard_08 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status08);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 09 - Borrower Withdrawal Status")
	public void Dashboard_09_BorrowerWithdrawalStatus() {
		
		log.info("Dashboard_09 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_09 - Step 02. Select Loan status is 'Borrower Withdrawal'");
		homePage.selectDefaultLoanStatus(status09);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status09));

		log.info("Dashboard_09 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status09);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucket(totalNumberLoan));

		log.info("Dashboard_09 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status09);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 10 - Lost to First Key Status")
	public void Dashboard_10_LostToFirstKeyStatus() {
		
		log.info("Dashboard_10 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_10 - Step 02. Select Loan status is 'Lost to First Key'");
		homePage.selectDefaultLoanStatus(status10);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status10));

		log.info("Dashboard_10 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status10);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucket(totalNumberLoan));

		log.info("Dashboard_10 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status10);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 11 - Lost to B2R Status")
	public void Dashboard_11_LostToB2RStatus() {
		
		log.info("Dashboard_11 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_11 - Step 02. Select Loan status is 'Lost to B2R'");
		homePage.selectDefaultLoanStatus(status11);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status11));

		log.info("Dashboard_11 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status11);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucket(totalNumberLoan));

		log.info("Dashboard_11 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status11);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 12 - Lost to Bank Status")
	public void Dashboard_12_LostToBankStatus() {
		
		log.info("Dashboard_12 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_12 - Step 02. Select Loan status is 'Lost to Bank'");
		homePage.selectDefaultLoanStatus(status12);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status12));

		log.info("Dashboard_12 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status12);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucket(totalNumberLoan));

		log.info("Dashboard_12 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status12);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@Test(groups = { "regression" }, description = "Dashboard 13 - Cancelled Status")
	public void Dashboard_13_CancelledStatus() {
		
		log.info("Dashboard_13 - Step 01. Open Homepage");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("Dashboard_13 - Step 02. Select Loan status is 'Cancelled'");
		homePage.selectDefaultLoanStatus(status13);

		log.info("VP: Verify Bucket status is selected");
		verifyTrue(homePage.isBucketStatusSelected(status13));

		log.info("Dashboard_13 - Step 03. Get Total number Loans of Status the selected");
		totalNumberLoan = homePage.getTotalNumberAtBucketStatus(status13);

		log.info("VP: Verify Total number loan equal with the Loan below");
		verifyTrue(homePage.isLoanDetailEqualTotalLoanNumberInDeadBucket(totalNumberLoan));

		log.info("Dashboard_13 - Step 04. Open bucket status category");
		loansPage = homePage.openCategory(status13);

		log.info("VP: Verify Total number loan equal Loan Applicants list");
		verifyTrue(loansPage.isTotalNumberLoanEqualLoanApplicantList(totalNumberLoan));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender, status01, status02, status03, status04, status05, status06;
	private String status07, status08, status09, status10, status11, status12, status13;
	private int totalNumberLoan;
}