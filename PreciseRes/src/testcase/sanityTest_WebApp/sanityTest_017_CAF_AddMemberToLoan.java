package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_017_CAF_AddMemberToLoan extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		usernameBorrower = "CAF.Dorner";
		passwordBorrower = "change";
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan-CAF"+accountNumber;
		loanName2 = "UdiTeam-Loan2-CAF"+accountNumber;
		loanStatus = Constant.LOAN_STATUS_CAF;
		applicantName = "Udi Dorner";
		memberName = "auto-CAF-member2014";
		memberLoginId = "auto-CAF-member2014";
		memberPassword = "change";
		propertiesFileName = "CAF Required.xlsx";
		address1 = "504 W Euclid";
		documentFileName = "datatest.pdf";
		documentType1 = "HUD-1/Closing Statement";
		documentType2 = "Evidence of Insurance";
		documentType3 = "Current Title Policy";
		documentType4 = "Lease Agreement";
		documentFileName2 = "Uploader_testing_document.txt";
		excelExtension = ".xlsx";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}
	
	@Test(groups = { "regression" },description = "Security09 - Member can view the loan item which he is added to")
	public void SecurityTest_09_MemberCanViewLoanAfterAdded()
	{
		log.info("Pre-Condition - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("Pre-Condition - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		log.info("Pre-Condition - Step 03. Create new loan");
		loansPage.createNewLoan(loanName2, applicantName, "", loanStatus);
		
		log.info("VP: Loan item display with correct info");
		verifyTrue(loansPage.isLoansDisplayWithCorrectInfo(loanName2, loanStatus));
		
		log.info("VP: Applicant displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(applicantName));
		
		log.info("Pre-Condition - Step 04. Add member to loan team");
		loansPage.inputKickOffDate("11/25/2015");
		loansPage.inputLoanAmountRequested("11252015");
		loansPage.inputFloor("10");
		loansPage.selectBridgeBorrower("Yes");
		loansPage.addMemberInBasicDetailTeam(memberName);
		verifyTrue(loansPage.isSaveSuccessMessageDisplay());
		
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			log.info("SecurityTest_09. Open Loans tab");
			loansPage.openLoansPage(driver, ipClient);

			log.info("SecurityTest_09. Search loan item");
			loansPage.searchLoanByName(loanName2);

			log.info("SecurityTest_09. Open detail loan item");
			loansPage.openLoansDetailPage(loanName2);
		}
		log.info("VP: The Member displays on Team table");
		verifyTrue(loansPage.isMemberDisplayOnTeamTable(memberName));

		log.info("SecurityTest_09 - Step 01. Login with the member");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(memberLoginId, memberPassword, false);
		
		log.info("VP: Member can see the loan");
		verifyTrue(homePage.isLoanDisplayOnDashboard(loanName2));
		
		log.info("SecurityTest_09 - Step 02. Go to Loan page");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		log.info("SecurityTest_09 - Step 03. Search loan by name");
		loansPage.searchLoanByName(loanName2);
		
		log.info("VP: The loan displays in table");
		verifyTrue(loansPage.isLoansDisplayOnSearchLoanPage(loanName2));
	}
	
	@Test(groups = { "regression" },description = "Security10 - Create New Loan With Member")
	public void SecurityTest_10_CreateNewLoanWithMember()
	{
		log.info("SecurityTest_10 - Step 01. Create new loan item");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus);
		
		log.info("VP: Loan item display with correct info");
		verifyTrue(loansPage.isLoansDisplayWithCorrectInfo(loanName, loanStatus));
		
		log.info("VP: User displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(applicantName));
		
		log.info("SecurityTest_10 - Step 02. Open Properties tab");
		loansPage.openPropertiesTab();
		
		log.info("SecurityTest_10 - Step 03. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);
	}
	
	@Test(groups = { "regression" },description = "Security11 - Add Public Document For Property By clicking Place Holder")
	public void SecurityTest_11_AddPublicDocumentForPropertyByPlaceHolder()
	{
		log.info("SecurityTest_11 - Step 01. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);
		
		log.info("SecurityTest_11 - Step 02. Open Document type detail");
		propertiesPage.openDocumentTypeDetail(documentType3);
		
		log.info("SecurityTest_11 - Step 03. Select document type");
		propertiesPage.selectDocumentType(documentType3);
				
		log.info("SecurityTest_11 - Step 04. Upload document file");
		propertiesPage.uploadDocumentFile(documentFileName);
		
		log.info("SecurityTest_11 - Step 05. Click save button");
		propertiesPage.clickSaveButton();
		
		log.info("SecurityTest_11 - Step 06. Click Go To Property button");
		propertiesPage.clickGoToPropertyButton();
		
		log.info("VP: Document is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType3, documentFileName));
	}
	
	@Test(groups = { "regression" },description = "Security12 - Add Public Document For Property By clicking New Button")
	public void SecurityTest_12_AddPublicDocumentForPropertyByNewButton()
	{
		log.info("Pre-Condition - Step 01. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		log.info("Pre-Condition - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);
		
		log.info("Pre-Condition - Step 03. Open detail loan item");
		loansPage.openLoansDetailPage(loanName);
		
		log.info("Pre-Condition - Step 04. Open Properties tab");
		loansPage.openPropertiesTab();
		
		log.info("Pre-Condition - Step 05. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);
		
		log.info("SecurityTest_12 - Step 01. Click New document button");
		propertiesPage.clickNewDocumentButton();
		
		log.info("SecurityTest_12 - Step 02. Select document type");
		propertiesPage.selectDocumentType(documentType1);
		propertiesPage.uncheckPrivateCheckbox();
		
		log.info("SecurityTest_12 - Step 03. Upload document file");
		propertiesPage.uploadDocumentFile(documentFileName);
		
		log.info("SecurityTest_12 - Step 04. Click save button");
		propertiesPage.clickSaveButton();
		
		log.info("SecurityTest_12 - Step 05. Click Go To Property button");
		propertiesPage.clickGoToPropertyButton();
		
		log.info("VP: Document is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, documentFileName));
	}
	
	@Test(groups = { "regression" },description = "Security13 - Upload document to loan throught uploader")
	public void SecurityTest_13_UploadDocumentForPropertyOnUploader()
	{
		log.info("SecurityTest_13 - Step 01. Click on logged user name on the top right corner");
//		loansPage = propertiesPage.openLoansPage(driver, ipClient);
//		loansPage.clickOnLoggedUserName(driver);
//		uploaderPageUrl = homePage.getUploaderPageUrl(driver);
		
		log.info("SecurityTest_13 - Step 02. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("SecurityTest_13 - Step 03. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameLender, passwordLender);
		
		log.info("SecurityTest_13 - Step 04. Select loans item");
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("SecurityTest_13 - Step 05. Select folder 'Properties'");
		uploaderPage.clickPropertiesFolder();
		
		log.info("SecurityTest_13 - Step 06. Open Property item");
		uploaderPage.selectPropertyFolder(address1);
		
		log.info("SecurityTest_13 - Step 07. Upload document for document type");
		uploaderPage.clickOnSelectNewFileIconByType(documentType2);
		uploaderPage.uploadFileOnUploader(documentFileName2);
		
		log.info("SecurityTest_13 - Step 08. Wait for upload complete");
		uploaderPage.waitForUploadCompleteOnUploader(documentFileName2);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(documentFileName2));
		
		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(documentFileName2, documentType2));
	}
	
	@Test(groups = { "regression" },description = "Security14 - Member export data tape")
	public void SecurityTest_14_MemberExportDateTape()
	{
		log.info("SecurityTest_14 - Step 01. Open web app");
		uploaderLoginPage = uploaderPage.logoutUploader();
		loginPage = uploaderLoginPage.openPreciseResUrlByBorrower(driver, ipClient, getPreciseResUrl());
		
		log.info("SecurityTest_14 - Step 02. Login with Member");
		homePage = loginPage.loginAsLender(memberLoginId, memberPassword, false);
		
		log.info("SecurityTest_14 - Step 03. Open Loans detail");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		loansPage.openLoansDetailPage(loanName);
		
		log.info("SecurityTest_14 - Step 04. Open Properties tab");
		loansPage.openPropertiesTab();
		
		log.info("SecurityTest_14 - Step 05. Click 'Export datatape' button");
		loansPage.deleteAllFileInFolder();
		loansPage.clickExportDatatapeButton();
		
		log.info("SecurityTest_14 - Step 06. Click 'Export all Properties' radio button");
		loansPage.clickExportAllPropertiesRadioButton("1");
		
		log.info("SecurityTest_14 - Step 07. Wait for file downloaded complete");
		loansPage.waitForDownloadFileContainsNameCompleted(excelExtension);
		
		log.info("VP: File number in folder equal 1");
		countFile = loansPage.countFilesInDirectory();
		verifyEquals(countFile, 1);
		
		log.info("SecurityTest_14 - Step 08. Delete the downloaded file");
		loansPage.deleteContainsFileName(excelExtension);
		
		log.info("SecurityTest_14 - Step 09. Click 'Export datatape' button");
		loansPage.deleteAllFileInFolder();
		loansPage.clickExportDatatapeButton();
		
		log.info("SecurityTest_14 - Step 10. Click 'Don't export Parent Properties' radio button");
		loansPage.clickExportAllPropertiesRadioButton("2");
		
		log.info("SecurityTest_14 - Step 11. Wait for file downloaded complete");
		loansPage.waitForDownloadFileContainsNameCompleted(excelExtension);
		
		log.info("VP: File number in folder equal 1");
		countFile = loansPage.countFilesInDirectory();
		verifyEquals(countFile, 1);
		
		log.info("SecurityTest_14 - Step 12. Delete the downloaded file");
		loansPage.deleteContainsFileName(excelExtension);
		
		log.info("SecurityTest_14 - Step 13. Click 'Export datatape' button");
		loansPage.deleteAllFileInFolder();
		loansPage.clickExportDatatapeButton();
		
		log.info("SecurityTest_14 - Step 14. Click 'Don't Export Sub-units' radio button");
		loansPage.clickExportAllPropertiesRadioButton("3");
		
		log.info("SecurityTest_14 - Step 15. Wait for file downloaded complete");
		loansPage.waitForDownloadFileContainsNameCompleted(excelExtension);
		
		log.info("VP: File number in folder equal 1");
		countFile = loansPage.countFilesInDirectory();
		verifyEquals(countFile, 1);
		
		log.info("SecurityTest_14 - Step 16. Delete the downloaded file");
		loansPage.deleteContainsFileName(excelExtension);
	}
	
	@Test(groups = { "regression" },description = "Security15 - Member mark document as lender only")
	public void SecurityTest_15_MemberMarkDocumentAsLenderOnly()
	{
		log.info("SecurityTest_15 - Step 01. Click on logged user name on the top right corner");
//		loansPage.clickOnLoggedUserName(driver);
//		uploaderPageUrl = homePage.getUploaderPageUrl(driver);
		
		log.info("SecurityTest_15 - Step 02. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("SecurityTest_15 - Step 03. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameLender, passwordLender);
		
		log.info("SecurityTest_15 - Step 04. Select loans item");
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("SecurityTest_15 - Step 05. Select folder 'Properties'");
		uploaderPage.clickPropertiesFolder();
		
		log.info("SecurityTest_15 - Step 06. Open Property item");
		uploaderPage.selectPropertyFolder(address1);
		
		log.info("SecurityTest_15 - Step 07. Select checkbox for document type");
		uploaderPage.selectCheckboxForDocumentType(documentType2);
		
		log.info("SecurityTest_15 - Step 08. Select 'Make Lender Private only' link");
		uploaderPage.clickMakeLenderOnlyLink();
		uploaderPage.clickYesButton();
		
		log.info("VP: Check mark Lender Private for document type");
		verifyTrue(uploaderPage.isLenderPrivateCheckmarkForType(documentType2));
	}
	
	@Test(groups = { "regression" },description = "Security16 - login through the Applicant opened in the loan")
	public void SecurityTest_16_BorrowerCanSeeHisLoan()
	{
		log.info("SecurityTest_16 - Step 01. Open web app");
		uploaderLoginPage = uploaderPage.logoutUploader();
		loginPage = uploaderLoginPage.openPreciseResUrlByBorrower(driver, ipClient, getPreciseResUrl());
		
		log.info("SecurityTest_16 - Step 02. Login with Borrower");
		loansPage = loginPage.loginAsBorrower(usernameBorrower, passwordBorrower, false);
		
		log.info("SecurityTest_16 - Step 03. Search loan item");
		loansPage.searchLoanByName(loanName);	
		
		log.info("VP: The loan item displays on table");
		verifyTrue(loansPage.isLoansDisplayOnSearchLoanPage(loanName));
		
		log.info("SecurityTest_16 - Step 04. Open loan item");
		loansPage.openLoansDetailPage(loanName);
		
		log.info("SecurityTest_16 - Step 05. Open property tab");
		loansPage.openPropertiesTab();
		
		log.info("SecurityTest_16 - Step 06. Open property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);
		
		log.info("VP: Borrower can see the public document from Lender");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, documentFileName));
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType3, documentFileName));
		verifyFalse(propertiesPage.isDocumentLoadedToDocumentType(documentType2, documentFileName2));
	}	
	
	@Test(groups = { "regression" },description = "Security17 - Borrower upload 1 private property document")
	public void SecurityTest_17_BorrowerUploadPrivatePropertyDocument()
	{
		log.info("SecurityTest_17 - Step 01. Open Document type detail");
		propertiesPage.openDocumentTypeDetail(documentType4);
		
		log.info("SecurityTest_17 - Step 02. Select document type");
		propertiesPage.selectDocumentType(documentType4);
				
		log.info("SecurityTest_17 - Step 03. Check private checkbox");
		propertiesPage.checkPrivateCheckbox();
		
		log.info("SecurityTest_17 - Step 04. Upload document file");
		propertiesPage.uploadDocumentFile(documentFileName);
		
		log.info("SecurityTest_17 - Step 05. Click save button");
		propertiesPage.clickSaveButton();
		
		log.info("SecurityTest_17 - Step 06. Click Go To Property button");
		propertiesPage.clickGoToPropertyButton();
		
		log.info("VP: Document is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType4, documentFileName));
		
		log.info("SecurityTest_17 - Step 07. Login with lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("SecurityTest_17 - Step 08. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		log.info("SecurityTest_17 - Step 09. Search loan item");
		loansPage.searchLoanByName(loanName);
		
		log.info("SecurityTest_17 - Step 10. Open detail loan item");
		loansPage.openLoansDetailPage(loanName);
		
		log.info("SecurityTest_17 - Step 11. Open Properties tab");
		loansPage.openPropertiesTab();
		
		log.info("SecurityTest_17 - Step 12. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);
		
		log.info("VP: Make sure lender-admin can't see this document");
		verifyFalse(propertiesPage.isDocumentLoadedToDocumentType(documentType4, documentFileName));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private UploaderLoginPage uploaderLoginPage;
	private UploaderPage uploaderPage;
	private String usernameLender, passwordLender, loanStatus, address1, documentFileName;
	private String loanName, accountNumber, applicantName, propertiesFileName;
	private String memberName, memberLoginId, memberPassword, documentType3, documentType1;
	private String uploaderPageUrl, documentType2, documentFileName2, excelExtension;
	private String usernameBorrower, passwordBorrower, loanName2, documentType4;
	private int countFile;
}