package sanityTest_AutoIT;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import common.AbstractTest;
import common.Constant;

public class demoAutoIT extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		address1 = "37412 OAKHILL ST";
		documentType1 = "P8 - HOA Invoice/Account Statement";
		documentFileName = "datatest.pdf";
	}

	@Test(groups = {"regression" }, description = "Document01 - Add Public Document For Property By clicking New Button")
	public void DocumentTest_01_AddPublicDocumentForPropertyByNewButtonLender() {
		log.info("DocumentTest_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("DocumentTest_01 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_01 - Step 03. Search loan item");
//		loansPage.searchLoanByName("UdiTeam-Loan4630205");
		loansPage.searchLoanByName("UdiTeam-Loan6863102");

		log.info("DocumentTest_01 - Step 04. Open detail loan item");
//		loansPage.openLoansDetailPage("UdiTeam-Loan4630205");
		loansPage.openLoansDetailPage("UdiTeam-Loan6863102");

		log.info("DocumentTest_01 - Step 05. Open 'Properties' tab");
		loansPage.openPropertiesTab();

		log.info("DocumentTest_01 - Step 06. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);
		
//		log.info("DocumentTest_02 - Step 07. Add document 1 by clicking the place holder");
//		propertiesPage.uploadDynamicDocumentFileByPlaceHolder(documentType1, documentFileName);
		
		log.info("DocumentTest_01 - Step 08. Add document through the 'New' button");
		propertiesPage.clickNewDocumentButton();
		
		log.info("DocumentTest_01 - Step 09. Select document type");
		propertiesPage.selectDocumentType(documentType1);
		
		log.info("DocumentTest_01 - Step 10. Upload document file name");
		propertiesPage.uploadDocumentFile(documentFileName);
		
		log.info("DocumentTest_01 - Step 11. Click Save button");
		propertiesPage.clickSaveButton();
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private String usernameLender, passwordLender, address1, documentType1, documentFileName;
}