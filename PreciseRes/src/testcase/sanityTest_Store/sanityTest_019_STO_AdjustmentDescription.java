package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import common.AbstractTest;
import common.Constant;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;

public class sanityTest_019_STO_AdjustmentDescription extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileName = "Transaction New.xml";
	}
	
	@Test(groups = { "regression" },description = "AdjustmentDescription_01 - Input information for Adjustment Description from xml file")
	public void AdjustmentDescription_01_ImportFromXmlFile()
	{
		
		log.info("AdjustmentDescription_01 - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);
		
		///Adjustment00
		
		log.info("AdjustmentDescription_01 - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();
		
		log.info("AdjustmentDescription_01 - Step 03: Click 'Load Transaction' button");
		transactionPage.clickLoadTransactionButton();	
		
		log.info("AdjustmentDescription_01 - Step 04: Upload XML 00 - Upload file with invalid increase rate ");
		xmlFileName = "Adjustment00.xml";
		transactionPage.uploadTransactionXML(xmlFileName);
		
		log.info("VP: 'Field 'Increase Rate' - Please enter correct value ###.####%. in Lease Details. ' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please enter correct value ###.####%. in Lease Details."));
		
		///Adjustment01
		
		log.info("AdjustmentDescription_01 - Step 04: Upload XML 01 - Annually, {Increase Rate}%");
		xmlFileName = "Adjustment01.xml";
		increaseRate = "3.5564%";
		leverageFactor = "0";
		increaseFrequency = "12";
		adjustmentDescription = String.format("Annually, %s", increaseRate);
		transactionPage.uploadTransactionXML(xmlFileName);
		
		log.info("VP: 'Load data has been done' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Load data has been done"));
		
		log.info("AdjustmentDescription_01 - Step 05: Click 'Lease Details' button");
		transactionPage.openLeaseDetailsTab();
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment02
		
		log.info("AdjustmentDescription_01 - Step 02: Open 'Basic details' tab");
		transactionPage.openBasicDetailsTab();
		
		log.info("AdjustmentDescription_01 - Step 03: Click 'Load Transaction' button");
		transactionPage.clickLoadTransactionButton();
		
		log.info("AdjustmentDescription_01 - Step 04: Upload XML 02 - Annually, lesser of %s or CPI");
		xmlFileName = "Adjustment02.xml";
		increaseRate = "3.5564%";
		leverageFactor = "1";
		increaseFrequency = "12";
		adjustmentDescription = String.format("Annually, lesser of %s or CPI", increaseRate);
		transactionPage.uploadTransactionXML(xmlFileName);
		
		log.info("VP: 'Load data has been done' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Load data has been done"));
		
		log.info("AdjustmentDescription_01 - Step 05: Click 'Lease Details' button");
		transactionPage.openLeaseDetailsTab();
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment03
		
		log.info("AdjustmentDescription_01 - Step 02: Open 'Basic details' tab");
		transactionPage.openBasicDetailsTab();
		
		log.info("AdjustmentDescription_01 - Step 03: Click 'Load Transaction' button");
		transactionPage.clickLoadTransactionButton();
		
		log.info("AdjustmentDescription_01 - Step 04: Upload XML 03 - Annually, lesser of {Increase Rate}% or {Leverage Factor}x CPI");
		xmlFileName = "Adjustment03.xml";
		increaseRate = "3.5564%";
		leverageFactor = "3.5564";
		increaseFrequency = "12";
		adjustmentDescription = String.format("Annually, lesser of %s or %sx CPI", increaseRate, leverageFactor);
		transactionPage.uploadTransactionXML(xmlFileName);
		
		log.info("VP: 'Load data has been done' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Load data has been done"));
		
		log.info("AdjustmentDescription_01 - Step 05: Click 'Lease Details' button");
		transactionPage.openLeaseDetailsTab();
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment04
		
		log.info("AdjustmentDescription_01 - Step 02: Open 'Basic details' tab");
		transactionPage.openBasicDetailsTab();
		
		log.info("AdjustmentDescription_01 - Step 03: Click 'Load Transaction' button");
		transactionPage.clickLoadTransactionButton();
		
		log.info("AdjustmentDescription_01 - Step 04: Upload XML 04 - Every {Increase Frequency} years, {Increase Rate}%");
		xmlFileName = "Adjustment04.xml";
		increaseRate = "3.5564%";
		leverageFactor = "0";
		increaseFrequency = "36";
		adjustmentDescription = String.format("Every %s years, %s", Integer.toString(Integer.parseInt(increaseFrequency)/12), increaseRate);
		transactionPage.uploadTransactionXML(xmlFileName);
		
		log.info("VP: 'Load data has been done' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Load data has been done"));
		
		log.info("AdjustmentDescription_01 - Step 05: Click 'Lease Details' button");
		transactionPage.openLeaseDetailsTab();
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment05
		
		log.info("AdjustmentDescription_01 - Step 02: Open 'Basic details' tab");
		transactionPage.openBasicDetailsTab();
		
		log.info("AdjustmentDescription_01 - Step 03: Click 'Load Transaction' button");
		transactionPage.clickLoadTransactionButton();
		
		log.info("AdjustmentDescription_01 - Step 04: Upload XML 05 - Every {Increase Frequency} years, lesser of {Increase Rate}% or {Leverage Factor}x {Increase Frequency} year CPI");
		xmlFileName = "Adjustment05.xml";
		increaseRate = "3.5564%";
		leverageFactor = "1";
		increaseFrequency = "36";
		adjustmentDescription = String.format("Every %s years, lesser of %s or %sx %s year CPI",Integer.toString(Integer.parseInt(increaseFrequency)/12), increaseRate, leverageFactor, Integer.toString(Integer.parseInt(increaseFrequency)/12));
		transactionPage.uploadTransactionXML(xmlFileName);
		
		log.info("VP: 'Load data has been done' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Load data has been done"));
		
		log.info("AdjustmentDescription_01 - Step 05: Click 'Lease Details' button");
		transactionPage.openLeaseDetailsTab();
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment06
		
		log.info("AdjustmentDescription_01 - Step 02: Open 'Basic details' tab");
		transactionPage.openBasicDetailsTab();
		
		log.info("AdjustmentDescription_01 - Step 03: Click 'Load Transaction' button");
		transactionPage.clickLoadTransactionButton();
		
		log.info("AdjustmentDescription_01 - Step 04: Upload XML 06 - Every {Increase Frequency} years, lesser of {Increase Rate}% or {Leverage Factor}x {Increase Frequency} year CPI");
		xmlFileName = "Adjustment06.xml";
		increaseRate = "3.5564%";
		leverageFactor = "3.5564";
		increaseFrequency = "36";
		adjustmentDescription = String.format("Every %s years, lesser of %s or %sx %s year CPI", Integer.toString(Integer.parseInt(increaseFrequency)/12), increaseRate, leverageFactor, Integer.toString(Integer.parseInt(increaseFrequency)/12));
		transactionPage.uploadTransactionXML(xmlFileName);
		
		log.info("VP: 'Load data has been done' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Load data has been done"));
		
		log.info("AdjustmentDescription_01 - Step 05: Click 'Lease Details' button");
		transactionPage.openLeaseDetailsTab();
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment07
		
		log.info("AdjustmentDescription_01 - Step 02: Open 'Basic details' tab");
		transactionPage.openBasicDetailsTab();
		
		log.info("AdjustmentDescription_01 - Step 03: Click 'Load Transaction' button");
		transactionPage.clickLoadTransactionButton();
		
		log.info("AdjustmentDescription_01 - Step 04: Upload XML 07 - Rent does not increase or reset");
		xmlFileName = "Adjustment07.xml";
		increaseRate = "0%";
		leverageFactor = "3.5564";
		increaseFrequency = "36";
		adjustmentDescription = String.format("Rent does not increase or reset");
		transactionPage.uploadTransactionXML(xmlFileName);
		
		log.info("VP: 'Load data has been done' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Load data has been done"));
		
		log.info("AdjustmentDescription_01 - Step 05: Click 'Lease Details' button");
		transactionPage.openLeaseDetailsTab();
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment08
		
		log.info("AdjustmentDescription_01 - Step 02: Open 'Basic details' tab");
		transactionPage.openBasicDetailsTab();
		
		log.info("AdjustmentDescription_01 - Step 03: Click 'Load Transaction' button");
		transactionPage.clickLoadTransactionButton();
		
		log.info("AdjustmentDescription_01 - Step 04: Upload XML 08 - Rent does not increase or reset");
		xmlFileName = "Adjustment08.xml";
		increaseRate = "3.5564%";
		leverageFactor = "3.5564";
		increaseFrequency = "0";
		adjustmentDescription = String.format("Rent does not increase or reset");
		transactionPage.uploadTransactionXML(xmlFileName);
		
		log.info("VP: 'Load data has been done' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Load data has been done"));
		
		log.info("AdjustmentDescription_01 - Step 05: Click 'Lease Details' button");
		transactionPage.openLeaseDetailsTab();
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment09
		
		log.info("AdjustmentDescription_01 - Step 02: Open 'Basic details' tab");
		transactionPage.openBasicDetailsTab();
		
		log.info("AdjustmentDescription_01 - Step 03: Click 'Load Transaction' button");
		transactionPage.clickLoadTransactionButton();
		
		log.info("AdjustmentDescription_01 - Step 04: Upload XML 09 - Annually, lesser of {Increase Rate}% or {Leverage Factor}x CPI");
		xmlFileName = "Adjustment09.xml";
		increaseRate = "3%";
		leverageFactor = "3";
		increaseFrequency = "12";
		adjustmentDescription = String.format("Annually, lesser of %s or %sx CPI", increaseRate, leverageFactor);
		transactionPage.uploadTransactionXML(xmlFileName);
		
		log.info("VP: 'Load data has been done' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Load data has been done"));
		
		log.info("AdjustmentDescription_01 - Step 05: Click 'Lease Details' button");
		transactionPage.openLeaseDetailsTab();
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment10
		
		log.info("AdjustmentDescription_01 - Step 02: Open 'Basic details' tab");
		transactionPage.openBasicDetailsTab();
		
		log.info("AdjustmentDescription_01 - Step 03: Click 'Load Transaction' button");
		transactionPage.clickLoadTransactionButton();
		
		log.info("AdjustmentDescription_01 - Step 04: Upload XML 10 - Annually, lesser of {Increase Rate}% or {Leverage Factor}x CPI");
		xmlFileName = "Adjustment10.xml";
		increaseRate = "3%";
		leverageFactor = "3";
		increaseFrequency = "12";
		adjustmentDescription = String.format("Annually, lesser of %s or %sx CPI", increaseRate, leverageFactor);
		transactionPage.uploadTransactionXML(xmlFileName);
		
		log.info("VP: 'Load data has been done' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Load data has been done"));
		
		log.info("AdjustmentDescription_01 - Step 05: Click 'Lease Details' button");
		transactionPage.openLeaseDetailsTab();
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment11
		
		log.info("AdjustmentDescription_01 - Step 02: Open 'Basic details' tab");
		transactionPage.openBasicDetailsTab();
		
		log.info("AdjustmentDescription_01 - Step 03: Click 'Load Transaction' button");
		transactionPage.clickLoadTransactionButton();
		
		log.info("AdjustmentDescription_01 - Step 04: Upload XML 11 - Rent does not increase or reset");
		xmlFileName = "Adjustment11.xml";
		increaseRate = "0.526%";
		leverageFactor = "0.526";
		increaseFrequency = "0";
		adjustmentDescription = String.format("Rent does not increase or reset");
		transactionPage.uploadTransactionXML(xmlFileName);
		
		log.info("VP: 'Load data has been done' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Load data has been done"));
		
		log.info("AdjustmentDescription_01 - Step 05: Click 'Lease Details' button");
		transactionPage.openLeaseDetailsTab();
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment12
		
		log.info("AdjustmentDescription_01 - Step 02: Open 'Basic details' tab");
		transactionPage.openBasicDetailsTab();
		
		log.info("AdjustmentDescription_01 - Step 03: Click 'Load Transaction' button");
		transactionPage.clickLoadTransactionButton();
		
		log.info("AdjustmentDescription_01 - Step 04: Upload XML 12 - Increase Frequency isn't divisible by 12");
		xmlFileName = "Adjustment12.xml";
		increaseRate = "0.526%";
		leverageFactor = "0.526";
		increaseFrequency = "13";
		adjustmentDescription = String.format("");
		transactionPage.uploadTransactionXML(xmlFileName);
		
		log.info("VP: 'Load data has been done' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Transaction is imported, but some data is not valid:"));
		
		log.info("AdjustmentDescription_01 - Step 05: Click 'Lease Details' button");
		transactionPage.openLeaseDetailsTab();
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
	}
	
	@Test(groups = { "regression" },description = "AdjustmentDescription_02 - Input information for Adjustment Description by manual")
	public void AdjustmentDescription_02_InputInformationManually()
	{
		
		///Adjustment00
		
		log.info("AdjustmentDescription_02 - Step 04: Input with invalid increase rate ");
		increaseRate = "3.556465%";
		leverageFactor = "0";
		increaseFrequency = "12";
		transactionPage.inputDataForAdjustmentDescription(increaseRate, leverageFactor, increaseFrequency);
		
		log.info("VP: 'Field 'Increase Rate' - Please enter correct value ###.####%.' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please enter correct value ###.####%."));
		
		//Adjustment01
		
		log.info("AdjustmentDescription_02 - Step 04: Input 01 - Annually, {Increase Rate}%");
		increaseRate = "3.5564%";
		leverageFactor = "0";
		increaseFrequency = "12";
		adjustmentDescription = String.format("Annually, %s", increaseRate);
		transactionPage.inputDataForAdjustmentDescription(increaseRate, leverageFactor, increaseFrequency);
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment02
		
		log.info("AdjustmentDescription_02 - Step 04: Input 02 - Annually, lesser of %s or CPI");
		increaseRate = "3.5564%";
		leverageFactor = "1";
		increaseFrequency = "12";
		adjustmentDescription = String.format("Annually, lesser of %s or CPI", increaseRate);
		transactionPage.inputDataForAdjustmentDescription(increaseRate, leverageFactor, increaseFrequency);

		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment03
		
		log.info("AdjustmentDescription_02 - Step 04: Input 03 - Annually, lesser of {Increase Rate}% or {Leverage Factor}x CPI");
		increaseRate = "3.5564%";
		leverageFactor = "3.5564";
		increaseFrequency = "12";
		adjustmentDescription = String.format("Annually, lesser of %s or %sx CPI", increaseRate, leverageFactor);
		transactionPage.inputDataForAdjustmentDescription(increaseRate, leverageFactor, increaseFrequency);
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment04
		
		log.info("AdjustmentDescription_02 - Step 04: Input 04 - Every {Increase Frequency} years, {Increase Rate}%");
		increaseRate = "3.5564%";
		leverageFactor = "0";
		increaseFrequency = "36";
		adjustmentDescription = String.format("Every %s years, %s", Integer.toString(Integer.parseInt(increaseFrequency)/12), increaseRate);
		transactionPage.inputDataForAdjustmentDescription(increaseRate, leverageFactor, increaseFrequency);
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment05
		
		log.info("AdjustmentDescription_02 - Step 04: Input 05 - Every {Increase Frequency} years, lesser of {Increase Rate}% or {Leverage Factor}x {Increase Frequency} year CPI");
		increaseRate = "3.5564%";
		leverageFactor = "1";
		increaseFrequency = "36";
		adjustmentDescription = String.format("Every %s years, lesser of %s or %sx %s year CPI",Integer.toString(Integer.parseInt(increaseFrequency)/12), increaseRate, leverageFactor, Integer.toString(Integer.parseInt(increaseFrequency)/12));
		transactionPage.inputDataForAdjustmentDescription(increaseRate, leverageFactor, increaseFrequency);
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment06
		
		log.info("AdjustmentDescription_02 - Step 04: Input 06 - Every {Increase Frequency} years, lesser of {Increase Rate}% or {Leverage Factor}x {Increase Frequency} year CPI");
		increaseRate = "3.5564%";
		leverageFactor = "3.5564";
		increaseFrequency = "36";
		adjustmentDescription = String.format("Every %s years, lesser of %s or %sx %s year CPI", Integer.toString(Integer.parseInt(increaseFrequency)/12), increaseRate, leverageFactor, Integer.toString(Integer.parseInt(increaseFrequency)/12));
		transactionPage.inputDataForAdjustmentDescription(increaseRate, leverageFactor, increaseFrequency);
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment07
		
		log.info("AdjustmentDescription_02 - Step 04: Input 07 - Rent does not increase or reset");
		increaseRate = "0%";
		leverageFactor = "3.5564";
		increaseFrequency = "36";
		adjustmentDescription = String.format("Rent does not increase or reset");
		transactionPage.inputDataForAdjustmentDescription(increaseRate, leverageFactor, increaseFrequency);
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment08
		
		log.info("AdjustmentDescription_02 - Step 04: Input 08 - Rent does not increase or reset");
		increaseRate = "3.5564%";
		leverageFactor = "3.5564";
		increaseFrequency = "0";
		adjustmentDescription = String.format("Rent does not increase or reset");
		transactionPage.inputDataForAdjustmentDescription(increaseRate, leverageFactor, increaseFrequency);
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment09
		
		log.info("AdjustmentDescription_02 - Step 04: Input 09 - Annually, lesser of {Increase Rate}% or {Leverage Factor}x CPI");
		increaseRate = "3%";
		leverageFactor = "3";
		increaseFrequency = "12";
		adjustmentDescription = String.format("Annually, lesser of %s or %sx CPI", increaseRate, leverageFactor);
		transactionPage.inputDataForAdjustmentDescription(increaseRate, leverageFactor, increaseFrequency);
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment10
		
		log.info("AdjustmentDescription_02 - Step 04: Input 10 - Annually, lesser of {Increase Rate}% or {Leverage Factor}x CPI");
		increaseRate = "3..";
		leverageFactor = "3..";
		increaseFrequency = "12";
		adjustmentDescription = String.format("Annually, lesser of %s or %sx CPI", increaseRate, leverageFactor);
		transactionPage.inputDataForAdjustmentDescription(increaseRate, leverageFactor, increaseFrequency);
		
		log.info("VP: 'Field 'Increase Rate' - Please enter correct value ###.####%.' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please enter correct value ###.####%."));
		
		log.info("VP: 'Field 'Leverage Factor' - Value must be a number' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Value must be a number"));
		
		///Adjustment11
		
		log.info("AdjustmentDescription_02 - Step 04: Input 11 - Rent does not increase or reset");
		increaseRate = "0.526%";
		leverageFactor = "0.526";
		increaseFrequency = "0";
		adjustmentDescription = String.format("Rent does not increase or reset");
		transactionPage.inputDataForAdjustmentDescription(increaseRate, leverageFactor, increaseFrequency);
		
		log.info("VP: Increase Rate imported correctly");
		verifyEquals(transactionPage.getIncreaseRate(),increaseRate);
		
		log.info("VP: Leverage Factor imported correctly");
		verifyEquals(transactionPage.getLeverageFactor(),leverageFactor);
		
		log.info("VP: Increase Frequency imported correctly");
		verifyEquals(transactionPage.getIncreaseFrequency(),increaseFrequency);
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		///Adjustment12
		
		log.info("AdjustmentDescription_02 - Step 04: Input 12 - Input increase Frequency isn't divisible by 12");
		increaseFrequency = "15";
		adjustmentDescription = String.format("");
		transactionPage.inputIncreaseFrequency(increaseFrequency);
		transactionPage.keyPressing("enter");
		transactionPage.sleep(10);
		
		log.info("VP: Warning message is displayed correctly");
		verifyEquals(transactionPage.getTextJavascriptAlert(driver),"Increase Frequency is not divisible by 12, please set Adjustment Description manually.");
		transactionPage.clickOnAdjustment();
		
		log.info("VP: Adjustment Description is displayed correctly");
		verifyEquals(transactionPage.getAdjustmentDescription(), adjustmentDescription);
		
		
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName;
	private String increaseRate, leverageFactor, increaseFrequency, adjustmentDescription;
}