package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.DocumentsPage;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_039_CAF_LoanChangeDate extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_CAF;
		address1 = "502 E Hugh";
		applicantName = "CAF Demo Applicant";
		propertiesFileNameRequired = "CAF Data Tape.xlsx";
	}
	
	@Test(groups = { "regression" }, description = "Loan - Check Loan changed date in Property details page")
	public void LoanChangeDate_01_PropertyDetailsPage() {

		log.info("DataTape_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("DataTape_01 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		log.info("DataTape_01 - Step 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus);
		
		log.info("DataTape_01 - Step 04. Open Properties tab");
		loansPage.openPropertiesTab();
		
		log.info("DataTape_01 - Step 05. Load test file");
		loansPage.uploadFileProperties(propertiesFileNameRequired);

		log.info("DocumentTest_01 - Step 06. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);
		
		log.info("DocumentTest_01 - Step 07. Check Loan Date change displays correctly");
		propertiesPage.isLoanDateChangeDisplayedCorrectly();
	}
	
	@Test(groups = { "regression" }, description = "Loan - Check Loan changed date in General Loan Documents page")
	public void LoanChangeDate_02_GeneralLoanDocuments() {
		
		log.info("LoanChangeDate_02 - Step 01. Open General Documents Tab");
		documentsPage = loansPage.openGeneralDocumentTab();
		
		log.info("LoanChangeDate_02 - Step 02. Check Loan Date change displays correctly");
		documentsPage.isLastChangedDateDisplayedCorrectly();
	}
	
	@Test(groups = { "regression" }, description = "Loan - Check Loan changed date in Corporate Entity Documents page")
	public void LoanChangeDate_03_CorporateEntityDocuments() {
		
		log.info("LoanChangeDate_03 - Step 01. Open Corporate Entity Documents tab");
		documentsPage = loansPage.openCorporateEntityDocumentsTab();
		
		log.info("LoanChangeDate_03 - Step 02. Check Loan Date change displays correctly");
		documentsPage.isLastChangedDateDisplayedCorrectly();
	}

	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private DocumentsPage documentsPage;
	private String usernameLender, passwordLender, address1;
	private String loanName, accountNumber, applicantName, loanStatus;
	private String propertiesFileNameRequired;
}