package sanityTest_WithoutApplicant;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_001_B2R_Loan extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = "Term Sheet Sent";
		newLoanStatus = "Rent Roll Received";
		memberName = "auto-B2R-member2014";
		memberLoginId = "auto-B2R-member2014";
		memberPassword = "change";
		numberOfProperty = 8;
		propertiesFileName = "B2R Data Tape.xlsx";
		propertiesFileNameMulti = "B2RDataTape_multi-unit.xlsx";
		parentAddress1 = "Oakhill";
		parentAddress2 = "Rosemarie";
		propertyAddress = "10105 Spring ST";
		documentType = "Lease Agreement";
		documentFileName = "datatest.pdf";
	}

	@Test(groups = { "regression" }, description = "Loan 01 - Login through the lender-admin and open new loan  - with no applicant")
	public void Loan_01_CreateNewApplicantWithoutApplicant() {
		log.info("Loan_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Loan_01 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Loan_01 - Step 03. Click new loan button");
		loansPage.clickNewLoanButton();

		log.info("Loan_01 - Step 04. Input loan legal name");
		loansPage.inputLegalNameForLoan(loanName);

		log.info("Loan_01 - Step 05. Input loan ID");
		loansPage.inputLoanID(loanName);

		log.info("Loan_01 - Step 06. Select Loan Application Status");
		loansPage.selectLoanStatus(loanStatus);

		log.info("Loan_01 - Step 07. Click save loan application button");
		loansPage.clickSaveLoanApplicationButton();
	}

	@Test(groups = { "regression" }, description = "Loan 02 - Update fields in loan")
	public void Loan_02_UpdateFieldsInLoan() {
		log.info("VP: Verify the fields are stored correctly");
		verifyTrue(loansPage.isLoansReadOnlyDisplayWithCorrectInfo(loanName, loanStatus));
	}

	@Test(groups = { "regression" }, description = "Loan 03- Go to loan-basic details screen and Change the status of the loan")
	public void Loan_03_ChangeLoanStatusOnBasicDetail() {

		log.info("Loan_03 - Step 01. Select new Loan status");
		loansPage.selectLoanStatusBasicDetail(newLoanStatus);

		log.info("Loan_03 - Step 02. Click save button");
		loansPage.clickSaveButton();

		log.info("VP: Verify that Status is changed successfully");
		verifyTrue(loansPage.isLoansDisplayWithCorrectInfo(loanName, newLoanStatus));
	}

	@Test(groups = {"regression" }, description = "Loan 04 - Go to dashboard - verify the Loan Amount Requested from the loan was removed from the old status bucket and added to the new status bucket")
	public void Loan_04_LoanMoveToNewStatusBucket() {
		log.info("Loan_04 - Step 01. Go to Home page");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("VP: Verify the loan is moved to new status bucket");
		verifyTrue(homePage.isLoansDisplayInBucket(loanName, newLoanStatus));
	}

	@Test(groups = { "regression" }, description = "Loan 05 - Go to dashboard - verify the Loan appears in the right category in the list of loan bellow")
	public void Loan_05_LoanDisplaysInRightCategory() {
		log.info("Loan_05 - Step 01. Open new status category");
		loansPage = homePage.openCategory(newLoanStatus);

		log.info("Loan_05 - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("VP: The loan displays in right category");
		verifyTrue(loansPage.isLoansDisplayOnSearchLoanPage(loanName));
	}

	@Test(groups = { "regression" }, description = "Loan 06 - Go to dashboard and make sure you see your loan")
	public void Loan_06_MemberCanViewLoanAfterAdded() {
		log.info("Loan_06 - Step 01. Open loan basic detail tab");
		loansPage.openLoansDetailPage(loanName);

		log.info("Loan_06 - Step 02. Add member to loan team");
		loansPage.selectMemberInBasicDetailTeam(memberName);

		log.info("Loan_06 - Step 03. Click Save button");
		loansPage.clickSaveButton();

		log.info("VP: The Member displays on Team table");
		verifyTrue(loansPage.isSaveSuccessMessageDisplay());
		verifyTrue(loansPage.isMemberDisplayOnTeamTable(memberName));

		log.info("Loan_06 - Step 04. Login with the member");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(memberLoginId, memberPassword, false);

		log.info("VP: Member can see the loan");
		verifyTrue(homePage.isLoanDisplayOnDashboard(loanName));
	}

	@Test(groups = { "regression" }, description = "Loan_07 - Verify you can see the loan with a search by loan name")
	public void Loan_07_MemberCanSearchByLoanName() {
		log.info("Loan_07 - Step 01. Go to Loan page");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Loan_07 - Step 02. Search loan by name");
		loansPage.searchLoanByName(loanName);

		log.info("VP: The loan displays in table");
		verifyTrue(loansPage.isLoansDisplayOnSearchLoanPage(loanName));
	}

	@Test(groups = { "regression" }, description = "Loan 09 - Upload Datatape and check info")
	public void Loan_09_UploadAndCheckInfoDatatape() {

		log.info("Loan_09 - Step 01. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Loan_09 - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("Loan_09 - Step 03. Open loan basic detail tab");
		loansPage.openLoansDetailPage(loanName);

		log.info("Loan_09 - Step 04. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Loan_09 - Step 02. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("Loan_09 - Step 03. Get Property number");
		propertyNumberProtab = loansPage.getNumberOfPropertyInPropertiesTab();

		log.info("VP: Number of property is uploaded enough");
		verifyEquals(propertyNumberProtab, numberOfProperty);
	}

	@Test(groups = { "regression" }, description = "Loan 10 - Check that parents are created properly")
	public void Loan_10_PropertyParents() {

		log.info("Loan_10 - Step 01. Load test file");
		loansPage.uploadFileProperties(propertiesFileNameMulti);

		log.info("VP: Check that parents are created properly");
		verifyTrue(loansPage.isParentsPropertyDisplayCorrectly(parentAddress1));
		verifyTrue(loansPage.isParentsPropertyDisplayCorrectly(parentAddress2));
	}

	@Test(groups = { "regression" }, description = "Loan 11 - Add a document to a property")
	public void Loan_11_AddADocumentToProperty() {
		log.info("Loan_11 - Step 01. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(propertyAddress);

		log.info("Loan_11 - Step 02. Open Document type detail");
		propertiesPage.openDocumentTypeDetail(documentType);

		log.info("Loan_11 - Step 03. Select document type");
		propertiesPage.selectDocumentType(documentType);

		log.info("Loan_11 - Step 04. Uncheck private checkbox");
		propertiesPage.uncheckPrivateCheckbox();

		log.info("Loan_11 - Step 05. Upload document file");
		propertiesPage.uploadDocumentFile(documentFileName);

		log.info("Loan_11 - Step 06. Click save button");
		propertiesPage.clickSaveButton();

		log.info("Loan_11 - Step 07. Click Go To Property button");
		propertiesPage.clickGoToPropertyButton();

		log.info("VP: Document is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType, documentFileName));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private String usernameLender, passwordLender, loanStatus;
	private String loanName, accountNumber;
	private String memberName, memberLoginId, memberPassword;
	private String documentType, documentFileName;
	private String propertiesFileName, propertiesFileNameMulti;
	private int numberOfProperty;
	private int propertyNumberProtab;
	private String parentAddress2, parentAddress1;
	private String propertyAddress, newLoanStatus;
}