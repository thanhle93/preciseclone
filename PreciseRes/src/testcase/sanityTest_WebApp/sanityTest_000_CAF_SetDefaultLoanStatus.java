package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_000_CAF_SetDefaultLoanStatus extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		usernameApplicant = Constant.USERNAME_CAF_BORROWER;
		passwordApplicant = Constant.PASSWORD_CAF_BORROWER;
		memberUsername = "auto-CAF-member2014";
		memberPassword = "change";
		loanStatus1 = Constant.LOAN_STATUS_CAF;
		loanStatus2 = "Deals Under Review";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}

	@Test(groups = { "regression" }, description = "DefaultLoanStatus - Lender - Set default Loan status")
	public void DefaultLoanStatus_01_Lender_SetDefaultLoanStatus() {
		log.info("DefaultLoanStatus_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("DefaultLoanStatus_01 - Step 02. Select Default Loan Status combobox");
		homePage.selectDefaultLoanStatus(loanStatus1);

		log.info("DefaultLoanStatus_01 - Step 03. Click Save default status button");
		homePage.clickSaveDefaultStatusbutton();

		log.info("VP: Default loan status selected display on Dashboard");
		verifyTrue(homePage.isDefaultLoanStatusSelectedDisplayOnDashboard(loanStatus1));

		log.info("VP: Default loan status first display on Dashboard");
		verifyTrue(homePage.isDefaultLoanStatusDisplayFirstOnDashboard(loanStatus1));

		log.info("VP: The rest loan status not display on Dashboard");
		verifyFalse(homePage.isDefaultLoanStatusDisplayFirstOnDashboard(loanStatus2));
	}

	@Test(groups = { "regression" }, description = "DefaultLoanStatus - Member - Set default Loan status")
	public void DefaultLoanStatus_02_Member_SetDefaultLoanStatus() {

		log.info("DefaultLoanStatus_02 - Step 01. Log out and login with Member");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(memberUsername, memberPassword, false);

		log.info("DefaultLoanStatus_02 - Step 02. Select Default Loan Status combobox");
		homePage.selectDefaultLoanStatus(loanStatus1);

		log.info("DefaultLoanStatus_02 - Step 03. Click Save default status button");
		homePage.clickSaveDefaultStatusbutton();

		log.info("VP: Default loan status selected display on Dashboard");
		verifyTrue(homePage.isDefaultLoanStatusSelectedDisplayOnDashboard(loanStatus1));

		log.info("VP: Default loan status first display on Dashboard");
		verifyTrue(homePage.isDefaultLoanStatusDisplayFirstOnDashboard(loanStatus1));

		log.info("VP: The rest loan status not display on Dashboard");
		verifyFalse(homePage.isDefaultLoanStatusDisplayFirstOnDashboard(loanStatus2));
	}

	@Test(groups = { "regression" }, description = "DefaultLoanStatus - Borrower - Set default Loan status")
	public void DefaultLoanStatus_03_Borrower_SetDefaultLoanStatus() {

		log.info("DefaultLoanStatus_03 - Step 01. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);

		log.info("DefaultLoanStatus_03 - Step 02. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameApplicant, passwordApplicant);

		log.info("DefaultLoanStatus_03 - Step 03. Select Loan Status combobox");
		uploaderPage.selectDefaultLoanStatus(loanStatus1);

		log.info("DefaultLoanStatus_03 - Step 04. Click Save as my Default button");
		uploaderPage.clickSaveDefaultStatusbutton();

		log.info("VP: Default loan status selected display correctly on Uploader");
		verifyTrue(uploaderPage.isDefaultLoanStatusSelectedDisplayOnUploader(loanStatus1));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private String usernameLender, passwordLender;
	private String memberUsername, memberPassword, usernameApplicant, passwordApplicant;
	private String loanStatus1, loanStatus2, uploaderPageUrl;
}