package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_006_B2R_ChangeStatusAndAddMemberToLoanTeam extends
		AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower New";
		memberName = "auto-B2R-member2014";
		memberLoginId = "auto-B2R-member2014";
		memberPassword = "change";
		newLoanStatus = "Term Sheet Sent";
	}

	@Test(groups = { "regression" }, description = "Loan34 - Change Loan Status On Basic Detail")
	public void ChangeLoanStatusAddMember_01_ChangeLoanStatusOnBasicDetail() {
		log.info("ChangeLoanStatus_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender,false);

		log.info("ChangeLoanStatus_01 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("ChangeLoanStatus_01 - Step 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus,	loanName);
		
		log.info("ChangeLoanStatus_01 - Step 04. Select new Loan status");
		loansPage.selectLoanStatusBasicDetail(newLoanStatus);
		
		log.info("ChangeLoanStatus_01 - Step 05. Click save button");
		loansPage.clickSaveButton();

		log.info("VP: Verify that Status is added successfully");
		verifyTrue(loansPage.isLoansReadOnlyDisplayWithCorrectInfo(loanName,newLoanStatus));
	}

	@Test(groups = { "regression" }, description = "Loan35 - The loan move to new status bucket")
	public void ChangeLoanStatusAddMember_02_LoanMoveToNewStatusBucket() {
		log.info("ChangeLoanStatus_02 - Step 01. Go to Home page");
		homePage = loansPage.openHomePage(driver, ipClient);
		
		log.info("ChangeLoanStatus_02 - Step 02. Select Loan Status combobox");
		homePage.selectDefaultLoanStatus(newLoanStatus);

		log.info("VP: Verify the loan is moved to new status bucket");
		verifyTrue(homePage.isLoansDisplayInBucket(loanName, newLoanStatus));
	}

	@Test(groups = { "regression" }, description = "Loan36 - The loan displays in right category")
	public void ChangeLoanStatusAddMember_03_LoanDisplaysInRightCategory() {
		
		log.info("ChangeLoanStatus_03 - Step 01. Open new status category");
		loansPage = homePage.openCategory(newLoanStatus);

		log.info("ChangeLoanStatus_03 - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("VP: The loan displays in right category");
		verifyTrue(loansPage.isLoansDisplayOnSearchLoanPage(loanName));
	}

	@Test(groups = { "regression" }, description = "Loan46 - Add new member to team in Basic detail tab")
	public void ChangeLoanStatusAddMember_04_AddMemberForLoan() {
		log.info("AddMemberForLoan_04 - Step 01. Open loan basic detail tab");
		loansPage.openLoansDetailPage(loanName);

		log.info("AddMemberForLoan_04 - Step 02. Add member to loan team");
		loansPage.addMemberInBasicDetailTeam(memberName);

		log.info("VP: The Member displays on Team table");
		verifyTrue(loansPage.isSaveSuccessMessageDisplay());
		
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			log.info("AddMemberForLoan_04. Open Loans tab");
			loansPage.openLoansPage(driver, ipClient);

			log.info("AddMemberForLoan_04. Search loan item");
			loansPage.searchLoanByName(loanName);

			log.info("AddMemberForLoan_04. Open detail loan item");
			loansPage.openLoansDetailPage(loanName);
		}
		verifyTrue(loansPage.isMemberDisplayOnTeamTable(memberName));
	}

	@Test(groups = { "regression" }, description = "Loan47 - Member can view the loan item which he is added to")
	public void ChangeLoanStatusAddMember_05_MemberCanViewLoanAfterAdded() {
		log.info("AddMemberForLoan_05 - Step 01. Login with the member");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(memberLoginId, memberPassword, false);
		
		log.info("AddMemberForLoan_05 - Step 02. Select Loan Status combobox");
		homePage.selectDefaultLoanStatus(newLoanStatus);

		log.info("VP: Member can see the loan");
		verifyTrue(homePage.isLoansDisplayInBucket(loanName, newLoanStatus));

		log.info("AddMemberForLoan_05 - Step 03. Go to Loan page");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("AddMemberForLoan_05 - Step 04. Search loan by name");
		loansPage.searchLoanByName(loanName);

		log.info("VP: The loan displays in table");
		verifyTrue(loansPage.isLoansDisplayOnSearchLoanPage(loanName));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender, loanStatus, newLoanStatus;
	private String loanName, accountNumber, applicantName;
	private String memberName, memberLoginId, memberPassword;
}