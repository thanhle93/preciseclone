package sanityTest_Uploader;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_000_B2R_Lender_TestDragDrop extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		loanName = "UploaderUdiTesting"+getUniqueNumber();
		applicantName = "Consolidation Demo Borrower New";
		loanStatus = Constant.LOAN_STATUS_B2R;
		propertiesFileName = "B2R Data Tape.xlsx";
		fileNameDocument1 = "datatest.pdf";
		documentType1 = "P5 - Zoning Compliance Letter";
		documentType2 = "P3 - Purchase Contract/HUD-1 Settlement Stmt";
		property1Name = "37412 OAKHILL ST";
		fileNameDocument2 = "Uploader_testing_document.txt";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}
	
	@Test(groups = { "regression" },description = "UploaderLender01 - Add file then check if uploader matches the Web app")
	public void Uploader_01_AddFileAndCheckMatch()
	{
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("Precondition 02. Create new loans and get loans' id");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);
		
		log.info("Precondition 03. Upload data tape");
		loansPage.openPropertiesTab();
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("Precondition 04. Click on logged user name on the top right corner");
//		loansPage.clickOnLoggedUserName();
//		uploaderPageUrl = loansPage.getUploaderPageUrl();
		
		log.info("Precondition 05. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("Precondition 06. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameLender, passwordLender);
		
		log.info("Uploader_01 - Step 01. Select loans item");
		uploaderPage.clickChangeGroupedView();
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("Uploader_01 - Step 02. Open Property folder");
		uploaderPage.clickPropertiesFolder();
		
		log.info("Uploader_01 - Step 03. Open Property item");
		uploaderPage.selectPropertyFolder(property1Name);
		
		log.info("Uploader_01 - Step 04. Upload document for a document type");
		uploaderPage.clickOnSelectNewFileIconByType(documentType1);
		uploaderPage.uploadFileOnUploader(fileNameDocument1);
		
		log.info("Uploader_01 - Step 05. Wait for upload complete");
		uploaderPage.waitForUploadCompleteOnUploader(fileNameDocument1);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(fileNameDocument1));
		
		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, documentType1));
		
		log.info("Uploader_01 - Step 05. Wait for upload complete");
		uploaderPage.dragDropFile("datatest.png","p4 final.png");
	}
	
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private String usernameLender, passwordLender, applicantName, loanStatus;
	private String loanName, uploaderPageUrl, propertiesFileName, fileNameDocument2;
	private String property1Name, fileNameDocument1, documentType1, documentType2;
}