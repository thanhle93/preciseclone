package sanityTest_WithoutApplicant;

import java.text.DecimalFormat;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.DocumentsPage;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_003_B2R_DocumentReviewSelected extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		applicantUsername = Constant.USERNAME_B2R_BORROWER;
		applicantPassword = Constant.PASSWORD_B2R_BORROWER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = "Term Sheet Sent";
		address1 = "37412 OAKHILL ST";
		documentType1 = "HUD-1/Closing Statement";
		documentType2 = "EDR Report";
		documentType3 = "Current Title Policy";
		documentType4 = "Deeds";
		documentType5 = "Lease Agreement";
		documentFileName = "datatest.pdf";
		reviewedBy = "Consolidation,Demo Lender";
		propertiesFileNameRequired = "B2R Required.xlsx";
		documentSection1 = "General Loan Documents - KYC";
		documentType6 = "Credit Report";
		documentSectionSearch = "Legal Documents";
		documentSection2 = "Corporate Entity Documents - Borrower";
		documentType7 = "Organizational Chart";
	}

	@Test(groups = { "regression" }, description = "Loan46 - Calculate the sum of all the property document + general loan document + corporate entity document")
	public void DocumentTest_01_CalculateTheSumOfAllDocuments() {

		log.info("DocumentTest_01 - Step 01. Login with lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("DocumentTest_01 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_01 - Step 03. Create new loan");
		loansPage.createNewLoan(loanName, "", "", loanStatus, loanName);

		log.info("DocumentTest_01 - Step 04. Open 'Properties' tab");
		loansPage.openPropertiesTab();

		log.info("DocumentTest_01 - Step 05. Load test file");
		loansPage.uploadFileProperties(propertiesFileNameRequired);

		log.info("DocumentTest_01 - Step 06. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("DocumentTest_01 - Step 07. Add document through the 'New' button");
		propertiesPage.clickNewDocumentButton();
		propertiesPage.selectDocumentType(documentType1);
		propertiesPage.uploadDocumentFile(documentFileName);
		propertiesPage.uncheckPrivateCheckbox();
		propertiesPage.clickSaveButton();
		propertiesPage.clickGoToPropertyButton();

		log.info("VP: Document is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, documentFileName));
		loansPage = propertiesPage.clickOnPropertyListButton();

		log.info("DocumentTest_01 - Step 08. Get 'Property' number");
		documentNumberProtab = loansPage.getNumberDocumentsRequiredOfPropertyInPropertiesTab();

		log.info("DocumentTest_01 - Step 09. Open 'General Loan Documents' page");
		documentsPage = loansPage.openGeneralDocumentTab();

		log.info("DocumentTest_01 - Step 10. New 'General Loan Document' file");
		documentsPage.clickNewButton();
		documentsPage.selectGeneralDocumentSection(documentSection1);
		documentsPage.selectGeneralDocumentType(documentType6);
		documentsPage.uploadDocumentFile(documentFileName);
		documentsPage.clickSaveButton();

		log.info("DocumentTest_01 - Step 11. Click Document List button");
		documentsPage.clickDocumentListButton();

		log.info("DocumentTest_01 - Step 12. Get 'General Loan Documents' number");
		documentNumberGeneraltab = documentsPage.getNumberOfPropertyInPropertiesTab();

		log.info("DocumentTest_01 - Step 13. Click 'Section' search");
		documentsPage.searchSectionAndDocumentType(documentSectionSearch, "");

		log.info("DocumentTest_01 - Step 14. Get 'General Loan Documents' number with Section is 'Legal Documents'");
		documentNumberGeneraltab1 = documentsPage.getNumberOfPropertyInPropertiesTab();

		log.info("DocumentTest_01 - Step 15. Open 'Corporate Entity Documents' page");
		documentsPage.openCorporateEntityDocumentsTab();

		log.info("DocumentTest_01 - Step 16. New 'Corporate Entity Documents' file");
		documentsPage.clickNewButton();
		documentsPage.selectGeneralDocumentSection(documentSection2);
		documentsPage.selectGeneralDocumentType(documentType7);
		documentsPage.uploadDocumentFile(documentFileName);
		documentsPage.clickSaveButton();

		log.info("DocumentTest_01 - Step 17. Click Document List button");
		documentsPage.clickDocumentListButton();

		log.info("DocumentTest_01 - Step 18. Get 'Corporate Entity Documents' number");
		documentNumberCorporatetab = documentsPage.getNumberOfPropertyInPropertiesTab();

		log.info("DocumentTest_01 - Step 19. Go to dashboard page");
		homePage = documentsPage.openHomePage(driver, ipClient);
		sumDocument = documentNumberProtab + documentNumberGeneraltab + documentNumberCorporatetab - documentNumberGeneraltab1;

		log.info("VP: The value in 'Required Docs' number on dashboard displays correctly");
		verifyTrue(homePage.isNumberRequiredDocsOfLoanDisplayCorrect(loanName, sumDocument + ""));
	}

	@Test(groups = { "regression" }, description = "Document 02 - Add 5 documents for Property by clicking the place holder and through the 'new' button)")
	public void DocumentTest_02_Add5DocumentForPropertyByPlaceHolderAndNewButton() {
		log.info("DocumentTest_02 - Step 01. Login with lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("DocumentTest_02 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_02 - Step 03. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("DocumentTest_02 - Step 04. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("DocumentTest_02 - Step 05. Add document 2 through the 'new' button");
		propertiesPage.clickNewDocumentButton();
		propertiesPage.selectDocumentType(documentType2);
		propertiesPage.uncheckPrivateCheckbox();
		propertiesPage.uploadDocumentFile(documentFileName);
		propertiesPage.clickSaveButton();
		propertiesPage.clickGoToPropertyButton();

		log.info("VP: Document 2 is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType2, documentFileName));

		log.info("DocumentTest_02 - Step 06. Add document 3 through the 'new' button");
		propertiesPage.clickNewDocumentButton();
		propertiesPage.selectDocumentType(documentType3);
		propertiesPage.uncheckPrivateCheckbox();
		propertiesPage.uploadDocumentFile(documentFileName);
		propertiesPage.clickSaveButton();
		propertiesPage.clickGoToPropertyButton();

		log.info("VP: Document 3 is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType3, documentFileName));

		log.info("DocumentTest_02 - Step 07. Add document 4 by clicking the place holder");
		propertiesPage.uploadDynamicDocumentFileByPlaceHolder(documentType4, documentFileName);

		log.info("DocumentTest_02 - Step 08. Add document 5 by clicking the place holder");
		propertiesPage.uploadDynamicDocumentFileByPlaceHolder(documentType5, documentFileName);

		log.info("DocumentTest_02 - Step 09. Click 'Save' button");
		propertiesPage.clickSaveButton();

		log.info("VP: Document 4 is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType4, documentFileName));

		log.info("VP: Document 5 is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType5, documentFileName));
	}

	@Test(groups = { "regression" }, description = "Document 03 - Search for the 5 new documents and press 'Review selected'")
	public void DocumentTest_03_Search5NewDocumentsAndPressReviewSelected() {

		log.info("DocumentTest_03 - Step 01. Login with lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("DocumentTest_03 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_03 - Step 03. Open Property Documents tab");
		propertiesPage.openPropertyDocumentsTab();

		log.info("DocumentTest_03 - Step 04. Click 'Review Selected' button");
		propertiesPage.clickReviewSelectedButton();

		log.info("VP: Document 1 is loaded to document type after Reviewed");
		verifyTrue(propertiesPage.isReviewSelectedDocumentLoadedToDocumentType(documentType1, reviewedBy));

		log.info("VP: Document 2 is loaded to document type after Reviewed");
		verifyTrue(propertiesPage.isReviewSelectedDocumentLoadedToDocumentType(documentType2, reviewedBy));

		log.info("VP: Document 3 is loaded to document type after Reviewed");
		verifyTrue(propertiesPage.isReviewSelectedDocumentLoadedToDocumentType(documentType3, reviewedBy));

		log.info("VP: Document 4 is loaded to document type after Reviewed");
		verifyTrue(propertiesPage.isReviewSelectedDocumentLoadedToDocumentType(documentType4, reviewedBy));

		log.info("VP: Document 5 is loaded to document type after Reviewed");
		verifyTrue(propertiesPage.isReviewSelectedDocumentLoadedToDocumentType(documentType5, reviewedBy));

		log.info("DocumentTest_03 - Step 05. Get number of total document");
		numberOfDocument = propertiesPage.getNumberOfDocumentInPropertyDocumentsTab();

		log.info("VP: Make sure document is included in the document count");
		verifyEquals(numberOfDocument, 5);
	}

	@Test(groups = { "regression" }, description = "Document 04 - Make sure that Borrower can see the document Lender")
	public void DocumentTest_04_MakeSureBorrowerCanSeeTheDocumentLender() {

		log.info("DocumentTest_04 - Step 01. Log out and login with applicant");
		loginPage = logout(driver, ipClient);
		loansPage = loginPage.loginAsBorrower(applicantUsername, applicantPassword, false);

		log.info("DocumentTest_04 - Step 02. Open Loans tab");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_04 - Step 03. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_04 - Step 04. Open property tab");
		loansPage.openPropertiesTab();

		log.info("DocumentTest_04 - Step 05. Open property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("VP: Borrower can see the public document from Lender");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, documentFileName));
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType2, documentFileName));
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType3, documentFileName));
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType4, documentFileName));
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType5, documentFileName));
	}

	@Test(groups = { "regression" }, description = "Document 04 - Go to the dashboard and verify the count of required document for this loan is increased by 5")
	public void DocumentTest_05_VerifyRequiredDocumentAtDashboardIncreasedBy5() {

		log.info("DocumentTest_05 - Step 01. Login with lender and go to dashboard page");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		sumDocumentReview = sumDocument + 2;

		log.info("VP: The value in 'Required Docs' number on dashboard increased");
		verifyTrue(homePage.isNumberRequiredDocsOfLoanDisplayCorrect(loanName, sumDocumentReview + ""));
	}

	@Test(groups = { "regression" }, description = "Document 05 - Verify that % Loaded column and % Reviewed column are correct")
	public void DocumentTest_06_CheckPercentLoadedAndPercentReviewed() {
		log.info("DocumentTest_06 - Step 01. Get '% Loaded' number in Homepage");
		percentLoad = homePage.getPercentLoaded(loanName);

		log.info("VP: Verify that % Loaded column are correct");
		numberLoad = (float) sumDocumentReview;
		numberLoad = (7 / numberLoad) * 100;
		DecimalFormat df1 = new DecimalFormat("0");
		percentLoaded = df1.format(numberLoad);
		verifyEquals(percentLoad, percentLoaded + "%");

		log.info("DocumentTest_06 - Step 02. Get '% Reviewed' number in Homepage");
		percentReview = homePage.getPercentReviewed(loanName);

		log.info("VP: Verify that % Reviewed column are correct");
		numberReview = (float) sumDocumentReview;
		numberReview = (5 / numberReview) * 100;
		DecimalFormat df2 = new DecimalFormat("0");
		percentReviewed = df2.format(numberReview);
		verifyEquals(percentReview, percentReviewed + "%");
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private DocumentsPage documentsPage;
	private String usernameLender, passwordLender, address1, documentFileName;
	private String applicantUsername, applicantPassword;
	private String loanName, accountNumber, loanStatus;
	private String documentType1, documentType2, documentType3, documentType4, documentType5, documentType6, documentType7;
	private int numberOfDocument;
	private String reviewedBy, propertiesFileNameRequired, documentSection1, documentSection2, documentSectionSearch;
	private int documentNumberProtab, documentNumberGeneraltab, documentNumberGeneraltab1, documentNumberCorporatetab, sumDocument, sumDocumentReview;
	private float numberLoad, numberReview;
	private String percentLoad, percentReview, percentLoaded, percentReviewed;
}