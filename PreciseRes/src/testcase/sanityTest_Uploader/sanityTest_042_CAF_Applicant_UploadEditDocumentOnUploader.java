package sanityTest_Uploader;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_042_CAF_Applicant_UploadEditDocumentOnUploader extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameMember = "auto-CAF-member2014";
		passwordMember = "change";
		usernameApplicant = Constant.USERNAME_CAF_BORROWER;
		passwordApplicant = Constant.PASSWORD_CAF_BORROWER;
		loanName = "UploaderUdiTesting"+getUniqueNumber();
		applicantName = "Udi Dorner";
		loanStatus = Constant.LOAN_STATUS_CAF;
		propertiesFileName = "CAF Data Tape.xlsx";
		fileNameDocument1 = "datatest.pdf";
		fileNameDocument2 = "datatest.pdf";
		documentType1 = "Evidence of Insurance";
		documentType2 = "Current Title Policy";
		property1Name = "504 W Euclid";
		property2Name = "110 E Emily";
		editFileNameDocument1 = "editdatatest.pdf";
		specialCharacter = "-�-,.!@#$%^&*()[]{}/?<>";
		editFileNameDocument2 = specialCharacter + ".pdf";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}
	
	@Test(groups = { "regression" },description = "UploaderApplicant04 - Upload public document and check match")
	public void Uploader_04_UploadPublicDocumentAndCheckMatch()
	{
		log.info("Uploader_04 - Step 01. Login with member");
		homePage = loginPage.loginAsLender(usernameMember, passwordMember, false);
		
		log.info("Uploader_04 - Step 02. Create new loans and get loans' id");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus);
		
		log.info("Uploader_04 - Step 03. Upload data tape");
		loansPage.openPropertiesTab();
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("Uploader_04 - Step 04. Login with Applicant");
		loginPage = logout(driver, ipClient);
		loansPage = loginPage.loginAsBorrower(usernameApplicant, passwordApplicant, false);
		
		log.info("Uploader_04 - Step 05. Open Loans detail");
		loansPage.searchLoanByName(loanName);
		loansPage.openLoansDetailPage(loanName);
		
		log.info("Uploader_04 - Step 06. Open Properties tab");
		loansPage.openPropertiesTab();
		
		log.info("Uploader_04 - Step 07. Open property detail");
		propertiesPage = loansPage.openPropertyDetail(property1Name);
		
		log.info("Uploader_04 - Step 08. Open Document type detail");
		propertiesPage.openDocumentTypeDetail(documentType1);
		
		log.info("Uploader_04 - Step 09. Select document type");
		propertiesPage.selectDocumentType(documentType1);
			
		log.info("Uploader_04 - Step 10. Upload document file");
		propertiesPage.uploadDocumentFile(fileNameDocument1);
		
		log.info("Uploader_04 - Step 11. Click save button");
		propertiesPage.clickSaveButton();
		
		log.info("Uploader_04 - Step 12. Click Go To Property button");
		propertiesPage.clickGoToPropertyButton();
		
		log.info("VP: Document is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, fileNameDocument1));
		
		log.info("Uploader_04 - Step 13. Click on logged user name on the top right corner");
//		loansPage = propertiesPage.openLoansPage(driver, ipClient);
//	    loansPage.clickOnLoggedUserName( driver );
//	    uploaderPageUrl = loansPage .getUploaderPageUrl( driver);
//		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
		
		log.info("Uploader_04 - Step 14. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("Uploader_04 - Step 15. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameApplicant, passwordApplicant);
		
		log.info("Uploader_04 - Step 16. Select loans item");
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("Uploader_04 - Step 17. Open Property folder");
		uploaderPage.clickPropertiesFolder();
		
		log.info("Uploader_04 - Step 18. Open Property item");
		uploaderPage.selectPropertyFolder(property1Name);
		
		log.info("VP: Make sure the file is updated in the uploader and unmarked with V as Lender private");
		verifyFalse(uploaderPage.isLenderPrivateCheckmarkForType(documentType1));
	}
	
	@Test(groups = { "regression" },description = "UploaderApplicant05 - Change Document Name")
	public void Uploader_05_ChangeDocumentName()
	{
		log.info("Uploader_05 - Step 01. Update document type for document");
		uploaderPage.clickOnEditDocumentIcon(fileNameDocument1);
		
		log.info("Uploader_05 - Step 02. Select new document type");
		uploaderPage.typeDocumentName(editFileNameDocument1);
		
		log.info("Uploader_05 - Step 03. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.clickYesButton();
		
		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(editFileNameDocument1, documentType1));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender06 - Change Document Name With Special Characters")
	public void Uploader_06_ChangeDocumentNameWithSpecialCharacters()
	{
		log.info("Uploader_06 - Step 01. Upload document for a document type");
		uploaderPage.clickOnSelectNewFileIconByType(documentType2);
		uploaderPage.uploadFileOnUploader(fileNameDocument2);
		
		log.info("Uploader_06 - Step 02. Wait for upload complete");
		uploaderPage.waitForUploadCompleteOnUploader(fileNameDocument2);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(fileNameDocument2));
		
		log.info("VP: The document type display correctly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument2, documentType2));
		
		log.info("Uploader_06 - Step 03. Update document type for document");
		uploaderPage.clickOnEditDocumentIcon(fileNameDocument2);
		
		log.info("Uploader_06 - Step 04. Input new document type name");
		uploaderPage.typeDocumentName(editFileNameDocument2);
		
		log.info("Uploader_06 - Step 05. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.clickYesButton();
		
		log.info("VP: The document type display correctly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(editFileNameDocument2, documentType2));
	}
	
	@Test(groups = { "regression" },description = "UploaderApplicant07 - Upload New document")
	public void Uploader_07_UploadNewDocument()
	{
		log.info("Uploader_07 - Step 01. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherDocuments();
		
		log.info("Uploader_07 - Step 02. Select file to upload");
		uploaderPage.uploadFileOnUploader(fileNameDocument2);
			
		log.info("Uploader_07 - Step 03. Click 'OK' Button");
		uploaderPage.waitForUploadCompleteOnUploader(fileNameDocument2);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(fileNameDocument2));
		
		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument2, "Unknown"));
	}
	
	@Test(groups = { "regression" },description = "UploaderApplicant08 - View Document")
	public void Uploader_08_ViewDocument()
	{
		if(!driver.toString().toLowerCase().contains("internetexplorer")){
		log.info("Uploader_08 - Step 01. Click on 'Eye' icon to view document");
		uploaderPage.clickOnViewDocumentIcon(fileNameDocument2);
		
		log.info("VP: The document is openned to view");
		verifyTrue(uploaderPage.isDocumentViewable(fileNameDocument2));
		}
	}
	
	@Test(groups = { "regression" },description = "UploaderApplicant09 - Attach document through the pencil screen")
	public void Uploader_09_AttachDocumentThroughThePencilScreen()
	{
		log.info("Uploader_09 - Step 01. Click on 'pencil' icon to edit document");
		uploaderPage.clickOnEditDocumentIcon(editFileNameDocument1);
		
		log.info("Uploader_09 - Step 02. Select other property on edit document popup");
		uploaderPage.selectPropertyOnEditDocument(property2Name);
		
		log.info("Uploader_09 - Step 03. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.clickYesButton();
		
		log.info("VP: Document is removed from property1 to property2");
		verifyFalse(uploaderPage.isDocumentTypeDisplay(editFileNameDocument1, documentType1));
		uploaderPage.selectPropertyFolder(property2Name);
		verifyTrue(uploaderPage.isDocumentTypeDisplay(editFileNameDocument1, documentType1));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private String usernameMember, passwordMember, applicantName, loanStatus;
	private String loanName, uploaderPageUrl, propertiesFileName, fileNameDocument2, property2Name;
	private String property1Name, fileNameDocument1, documentType1, editFileNameDocument1;
	private String  documentType2, editFileNameDocument2, specialCharacter;
	private String usernameApplicant, passwordApplicant;
}