package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;

import common.AbstractTest;
import common.Constant;

public class sanityTest_021_STO_DateOfLastChange extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + getUniqueNumber();
		propertiesFileName = "StoreProperties.xlsx";
		address1 = "148 Craft Drive";
		documentType = "Title Commitment";
		documentFileName = "datatest1.pdf";
	}

	@Test(groups = { "regression" }, description = "Date of last change 01 - Check Properties Details page")
	public void DateOfLastChange_01_CheckPropertiesDetailsPage() {

		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("Precondition - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("DateOfLastChange_01 - Step 07: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("DateOfLastChange_01 - Step 08: Click 'Add Properties' button");
		transactionPage.clickAddPropertyButton();

		log.info("DateOfLastChange_01 - Step 09: Upload Excel filename");
		transactionPage.uploadFileProperties(propertiesFileName);
		
		log.info("DateOfLastChange_01 - Step 10. Search with Address name is '148 Craft Drive'");
		transactionPage.searchPropertyAddress(address1);
		
		log.info("DateOfLastChange_01 - Step 11. Open a property detail");
		transactionPage.openPropertyDetail(address1);
		
		log.info("VP: Date of last change isn't displayed");
		verifyTrue(transactionPage.isDateOfLastChangeDisplayedCorrectly());

		log.info("Precondition - Step 01. Click 'New Property Documents' button");
		transactionPage.clickNewPropertyDocumentsButton();

		log.info("Precondition - Step 02. Select document type");
		transactionPage.selectDocumentType(documentType);

		log.info("Precondition - Step 03. Upload document file name");
		transactionPage.uploadDocumentFileName(documentFileName);

		log.info("Precondition - Step 04. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("Precondition - Step 05. Click 'Go to Property' button");
		transactionPage.clickGoToPropertyButtonAtDocumentTypeDetails();
		
		log.info("VP: Date of last change is displayed");
		verifyTrue(transactionPage.isDateOfLastChangeDisplayedCorrectly());
	}
	
	@Test(groups = { "regression" }, description = "Date of last change 02 - Check Transaction Documents page")
	public void DateOfLastChange_02_CheckTransactionDocuments() {

		log.info("DateOfLastChange_02 - Step 01: Open Transaction Documents page");
		transactionPage.openTransactionDocumentsTab();

		log.info("VP: Date of last change isn't displayed");
		verifyTrue(transactionPage.isLastChangeByDisplayedCorrectly());
	}
	
	@Test(groups = { "regression" }, description = "Date of last change 03 - Check Property Documents page")
	public void DateOfLastChange_03_CheckPropertyDocuments() {

		log.info("DateOfLastChange_03 - Step 01: Open Property Documents page");
		transactionPage.openPropertyDocumentsTab();

		log.info("VP: Date of last change isn't displayed");
		verifyTrue(transactionPage.isLastChangeByDisplayedCorrectly());
	}


	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName;
	private String propertiesFileName, address1, documentType, documentFileName;
	
}