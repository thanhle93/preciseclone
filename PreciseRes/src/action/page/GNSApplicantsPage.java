//package page;
//
//import org.openqa.selenium.WebDriver;
//import PreciseRes.Interfaces;
//import common.Common;
//
//public class GNSApplicantsPage extends AbstractPage {
//	public GNSApplicantsPage(WebDriver driver, String ipClient) {
//		control.setPage(this.getClass().getSimpleName());
//		this.driver = driver;
//	}
//
//	/**
//	 * click On 'New Applicant' button
//	 */
//	public void clickOnNewApplicantButton() {
//		waitForControl(driver, Interfaces.ApplicantsPage.NEW_APPLICANTS_BUTTON,
//				timeWait);
//		click(driver, Interfaces.ApplicantsPage.NEW_APPLICANTS_BUTTON);
//		sleep();
//	}
//	
//	/**
//	 * input New Applicant Info [GNS]
//	 */
//	public void inputNewApplicantInformation(String companyName, String primaryContactName, String email) {
//		waitForControl(driver, Interfaces.ApplicantsPage.COMPANY_NAME_TEXTBOX, timeWait);
//		type(driver, Interfaces.ApplicantsPage.COMPANY_NAME_TEXTBOX,	companyName);
//		sleep(1);
//		type(driver, Interfaces.ApplicantsPage.PRIMARY_CONTACT_LAST_NAME_TEXTBOX, primaryContactName);
//		sleep(1);
//		type(driver, Interfaces.ApplicantsPage.EMAIL_TEXTBOX, email);
//		sleep(1);
//	}
//	
//	/**
//	 * click On 'Invite New Applicant' button
//	 */
//	public void clickOnInviteNewApplicantButton() {
//		waitForControl(driver,
//				Interfaces.ApplicantsPage.INVITE_NEW_APPLICANT_BUTTON, timeWait);
//		executeJavaScript(driver, "document.getElementById('SaveNewApplicant').click();");
//		sleep(1);
//	}
//	
//	/**
//	 * check Detail Message Email
//	 */
//	public boolean isDetailMessageEmailDisplay(String detailMessageEmail) {
//		waitForControl(driver,
//				Interfaces.ApplicantsPage.EMAIL_MESSAGE_WITH_TEXT,
//				detailMessageEmail, timeWait);
//		return isControlDisplayed(driver,
//				Interfaces.ApplicantsPage.EMAIL_MESSAGE_WITH_TEXT,
//				detailMessageEmail);
//	}
//	
//	/**
//	 * click 'Applicant Name'
//	 */
//	public void clickOnApplicantName() {
//		waitForControl(driver, Interfaces.ApplicantsPage.BORROWER_NAME_ITEM,
//				timeWait);
//		click(driver, Interfaces.ApplicantsPage.BORROWER_NAME_ITEM);
//		sleep(1);
//	}
//	
//	/**
//	 * select Corporate Structure
//	 */
//	public void selectCorporateStructure(String corporateStructure) {
//		waitForControl(driver,
//				Interfaces.ApplicantsPage.FNF_CORPORATE_STRUCTURE_COMBOBOX,
//				timeWait);
//		selectItemCombobox(driver,
//				Interfaces.ApplicantsPage.FNF_CORPORATE_STRUCTURE_COMBOBOX,
//				corporateStructure);
//	}
//	
//	/**
//	 * change Company Name, Primary Contact Name, Email
//	 */
//	public void changeCompanyNamePrimaryContact(String companyName,	String primaryContactName, String email) {
//		waitForControl(driver, Interfaces.ApplicantsPage.GNS_COMPANY_NAME_TEXTBOX, companyName, timeWait);
//		type(driver, Interfaces.ApplicantsPage.GNS_COMPANY_NAME_TEXTBOX, companyName);
//		sleep(1);
//		type(driver, Interfaces.ApplicantsPage.GNS_PRIMARY_CONTACT_NAME_TEXTBOX, primaryContactName);
//		sleep(1);
//		type(driver, Interfaces.ApplicantsPage.GNS_EMAIL_TEXTBOX, email);
//		sleep(1);
//	}
//	
//	/**
//	 * check 'Company Name, Primary Contact Name, Email' Saved
//	 */
//	public boolean isGNSCompanyNamePrimaryContactSaved(String companyName,	String primaryContactName, String email) {
//		waitForControl(driver, Interfaces.ApplicantsPage.GNS_COMPANY_NAME_TEXTBOX, timeWait);
//		String realCompany = getAttributeValue(driver, Interfaces.ApplicantsPage.GNS_COMPANY_NAME_TEXTBOX, "value");
//		String realPrimaryContact = getAttributeValue(driver, Interfaces.ApplicantsPage.GNS_PRIMARY_CONTACT_NAME_TEXTBOX, "value");
//		String realEmail = getAttributeValue(driver, Interfaces.ApplicantsPage.GNS_EMAIL_TEXTBOX, "value");
//		return realCompany.contains(companyName) && realPrimaryContact.contains(primaryContactName) && realEmail.contains(email);
//	}
//	
//	/**
//	 * click On 'Save' button
//	 */
//	public void clickOnSaveButton() {
//		waitForControl(driver, Interfaces.ApplicantsPage.SAVE_BUTTON, timeWait);
//		executeJavaScript(driver, "document.getElementById('Save').click();");
//		sleep(1);
//	}
//	
//	/**
//	 * check Record Saved Message Displays
//	 * 
//	 * @return
//	 */
//	public boolean isRecordSavedMessageDisplays() {
//		waitForControl(driver, Interfaces.ApplicantsPage.RECORD_SAVED_MESSAGE,
//				timeWait);
//		return isControlDisplayed(driver,
//				Interfaces.ApplicantsPage.RECORD_SAVED_MESSAGE);
//	}
//	
//	/**
//	 * check Detail Message Display With Text [GNS]
//	 */
//	public boolean isSavedNameMessageDisplayWithText2(String messageContent) {
//		waitForControl(driver,
//				Interfaces.ApplicantsPage.GNS_SAVE_NAME_MESSAGE_DISPLAY_WITH_TEXT,
//				messageContent, timeWait);
//		return isControlDisplayed(driver,
//				Interfaces.ApplicantsPage.GNS_SAVE_NAME_MESSAGE_DISPLAY_WITH_TEXT,
//				messageContent);
//	}
//	
//	/**
//	 * click On 'Search' button
//	 */
//	public void clickOnSearchButton(String applicantName) {
//		waitForControl(driver, Interfaces.ApplicantsPage.BORROWER_NAME_TEXTBOX, timeWait);
//		type(driver, Interfaces.ApplicantsPage.BORROWER_NAME_TEXTBOX, applicantName);
//		sleep(1);
//		executeJavaScript(driver, "document.getElementById('Search').click();");
//		sleep(1);
//	}
//	
//	/**
//	 * get value 'Company Name' field
//	 */
//	public String getValueCompanyName() {
//		waitForControl(driver, Interfaces.ApplicantsPage.FNF_APPLICANT_COMPANY_NAME_TEXTBOX, timeWait);
//		String companyName = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_APPLICANT_COMPANY_NAME_TEXTBOX, "value");
//		return companyName;
//	}
//	
//	/**
//	 * upload Dynamic Document File by Place Holder
//	 */
//	public void uploadDynamicDocumentFileByPlaceHolder(String documentType, String fileName) {
//		waitForControl(driver, Interfaces.ApplicantsPage.DYNAMIC_CHOOSE_FILE_BUTTON_PLACE_HOLDER, documentType, timeWait);
//		click(driver, Interfaces.ApplicantsPage.DYNAMIC_CHOOSE_FILE_BUTTON_PLACE_HOLDER, documentType);
//		if(driver.toString().toLowerCase().contains("internetexplorerdriver"))
//		{
//			keyPressing("space");
//		}
//		sleep(1);
//		Common.getCommon().openFileForUpload(driver, fileName);
//		sleep();
//	}
//	
//	/**
//	 * check Document Loaded To Document Type, Status Change, Add On
//	 */
//	public boolean isDocumentLoadedToDocumentType(String documentType, String fileName, String statusChange, String addOn) {
//		waitForControl(driver, Interfaces.ApplicantsPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL, timeWait);
//		return isControlDisplayed(driver, Interfaces.ApplicantsPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL, documentType, fileName, statusChange, addOn);
//	}
//	
//	/**
//	 * click 'Document Type' detail
//	 */
//	public void clickDocumentTypeDetail(String documentFile, String documentType) {
//		waitForControl(driver, Interfaces.ApplicantsPage.DYNAMIC_DOCUMENT_TYPE_DETAIL_PLACE_HOLDER, documentFile, documentType, timeWait);
//		click(driver, Interfaces.ApplicantsPage.DYNAMIC_DOCUMENT_TYPE_DETAIL_PLACE_HOLDER, documentFile, documentType);
//		sleep();
//	}
//	
//	/**
//	 * check Document History Loaded To Document File, Submit Via, Submitted On, Previous Status, Status
//	 */
//	public boolean isDocumentHistoryLoadedToDocumentType(String documentFile, String submitVia, String submitOn, String previousStatus, String status) {
//		waitForControl(driver, Interfaces.ApplicantsPage.HISTORY_DYNAMIC_DOCUMENT_LOADED_FOR_DOCTYPE, documentFile, submitVia, submitOn, previousStatus, status, timeWait);
//		return isControlDisplayed(driver, Interfaces.ApplicantsPage.HISTORY_DYNAMIC_DOCUMENT_LOADED_FOR_DOCTYPE, documentFile, submitVia, submitOn, previousStatus, status);
//	}
//
//	/**
//	 * check 'Guarantor' Saved [GNS]
//	 */
//	public boolean isGuarantorSavedGNS(String firstName, String lastName, String percentageOwned) {
//		waitForControl(driver,Interfaces.ApplicantsPage.GNS_SAVE_GUARANTOR_MESSAGE_DISPLAY_WITH_TEXT,
//				firstName, lastName, percentageOwned, timeWait);
//		return isControlDisplayed(driver,Interfaces.ApplicantsPage.GNS_SAVE_GUARANTOR_MESSAGE_DISPLAY_WITH_TEXT,
//				firstName, lastName, percentageOwned);
//	}
//	
//	/**
//	 * click Open 'Signer' link
//	 */
//	public void clickOpenSignerLink(String signer) {
//		waitForControl(driver, Interfaces.ApplicantsPage.GNS_OPEN_SIGNER_LINK, signer, timeWait);
//		click(driver, Interfaces.ApplicantsPage.GNS_OPEN_SIGNER_LINK, signer);
//		sleep();
//	}
//	
//	private WebDriver driver;
//}