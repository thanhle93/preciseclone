package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.DocumentsPage;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Common;
import common.Constant;

public class sanityTest_014_B2R_DocumentCorporateEntityDocuments extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		applicantUsername = Constant.USERNAME_B2R_BORROWER;
		applicantPassword = Constant.PASSWORD_B2R_BORROWER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower";
		borrowerName = "UdiTeamBorrower";
		fileName01 = "datatest1.pdf";
		fileName02 = "datatest2.pdf";
		documentSection = "Corporate Entity Documents - Borrower (SPE)";
		documentSectionSearch = "Borrower (SPE)";
		documentType = "L8 - Borrower Org Chart";
		year = Common.getCommon().getCurrentYearOfWeek();
		submitVia = "Demo Lender Consolidation";
	}

	@Test(groups = { "regression" }, description = "Document24 - Corporate Entity Documents_Add new document and  make private")
	public void DocumentTest_24_AddNewDocumentAndMakePrivate() {
		log.info("DocumentTest_24 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender,	false);

		log.info("DocumentTest_24 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_24 - Step 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, borrowerName, loanStatus, loanName);
		
		log.info("DocumentTest_24 - Step 04. Log out and login with applicant");
		loginPage = logout(driver, ipClient);
		loansPage = loginPage.loginAsBorrower(applicantUsername,	applicantPassword, false);

		log.info("DocumentTest_24 - Step 05. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_24 - Step 06. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_24 - Step 07. Open Corporate Entity Documents tab");
		documentsPage = loansPage.openCorporateEntityDocumentsTab();
		
		log.info("DocumentTest_24 - Step 08. Get 'Corporate Entity Documents' number of Borrower");
		numberOfDocumentBorrower = documentsPage.getNumberOfPropertyInPropertiesTab();
		
		log.info("DocumentTest_24 - Step 09. Login with lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("DocumentTest_24 - Step 10. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_24 - Step 11. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_24 - Step 12. Open loan detail");
		loansPage.openLoansDetailPage(loanName);
		
		log.info("DocumentTest_24 - Step 13. Open Corporate Entity Documents tab");
		documentsPage = loansPage.openCorporateEntityDocumentsTab();
		
		log.info("DocumentTest_24 - Step 14. Get 'Corporate Entity Documents' number");
		numberOfDocumentLender = documentsPage.getNumberOfPropertyInPropertiesTab();
		
		log.info("DocumentTest_24 - Step 15. Click new button");
		documentsPage.clickNewButton();

		log.info("DocumentTest_24 - Step 16. Select section value");
		documentsPage.selectGeneralDocumentSection(documentSection);

		log.info("DocumentTest_24 - Step 17. Select document type");
		documentsPage.selectGeneralDocumentType(documentType);

		log.info("DocumentTest_24 - Step 18. Upload general document");
		documentsPage.uploadDocumentFile(fileName01);

		log.info("DocumentTest_24 - Step 19. Check private checkbox");
		documentsPage.checkPrivateCheckbox();

		log.info("DocumentTest_24 - Step 20. Click Save button");
		documentsPage.clickSaveButton();
	}

	@Test(groups = { "regression" }, description = "Document24 - Make sure that you can see the document")
	public void DocumentTest_25_MakeSureLenderCanSeeTheDocumentLender() {

		log.info("DocumentTest_25 - Step 01. Open Loans tab");
		loansPage = documentsPage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_25 - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_25 - Step 03. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_25 - Step 04. Open Corporate Entity Documents tab");
		documentsPage = loansPage.openCorporateEntityDocumentsTab();

		log.info("DocumentTest_25 - Step 05. Click Document Type search");
		documentsPage.searchDocumentType(documentType);

		log.info("VP: Lender can see the public document from Lender");
		verifyTrue(documentsPage.isDocumentUploadedToDocumentType(documentType));
	}

	@Test(groups = { "regression" }, description = "Document26 - Make sure the document is included in the document count")
	public void DocumentTest_26_AllDocumentsIncludedInLenderDocumentCountLender() {

		log.info("DocumentTest_26 - Step 01. Open Loans tab");
		loansPage = documentsPage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_26 - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_26 - Step 03. Open detail loan item");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_26 - Step 04. Open Corporate Entity Documents tab");
		loansPage.openCorporateEntityDocumentsTab();

		log.info("DocumentTest_26 - Step 05. Get number of total document");
		numberOfDocumentIncreased = loansPage.getNumberOfDocumentInPropertyDocumentsTab();
		numberOfDocumentLender = numberOfDocumentLender +1;

		log.info("VP: Make sure document is included in the document count");
		verifyEquals(numberOfDocumentLender, numberOfDocumentIncreased);
	}

	@Test(groups = { "regression" }, description = "DocumentTest27 - Login as borrower and make sure he can't see the document")
	public void DocumentTest_27_MakeSureBorrowerCanNotSeeTheDocumentLender() {

		log.info("DocumentTest_27 - Step 01. Log out and login with applicant");
		loginPage = logout(driver, ipClient);
		loansPage = loginPage.loginAsBorrower(applicantUsername,applicantPassword, false);

		log.info("DocumentTest_27 - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_27 - Step 03. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_27 - Step 04. Open Corporate Entity Documents tab");
		documentsPage = loansPage.openCorporateEntityDocumentsTab();

		log.info("DocumentTest_27 - Step 05. Click Document Type search");
		documentsPage.searchDocumentType(documentType);

		log.info("VP: Borrower cannot see the private document from Lender");
		verifyFalse(documentsPage.isDocumentUploadedToDocumentType(documentType));
	}

	@Test(groups = { "regression" }, description = "Document28 - Make sure the private document is not included in the document count")
	public void DocumentTest_28_PrivateDocumentNotIncludedInBorrowerDocumentCountBorrower() {

		log.info("DocumentTest_28 - Step 01. Open Loan page");
		loansPage = documentsPage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_28 - Step 02. Search Loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_28 - Step 03. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_28 - Step 04. Open Corporate Entity Documents tab");
		loansPage.openCorporateEntityDocumentsTab();

		log.info("DocumentTest_28 - Step 05. Get number of total document");
		numberOfDocumentDecreased = loansPage.getNumberOfDocumentInPropertyDocumentsTab();

		log.info("VP: Make sure private document is not included in the borrower document count");
		verifyEquals(numberOfDocumentBorrower, numberOfDocumentDecreased);
	}
	
	@Test(groups = { "regression" }, description = "Document29 - Find loan documents in Corporate Entity Documents when search by 'Section'")
	public void DocumentTest_29_FindLoanDocumentsBySection() {

		log.info("DocumentTest_29 - Step 01. Login with lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameLender, passwordLender,	false);

		log.info("DocumentTest_29 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);
				
		log.info("DocumentTest_29 - Step 03. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_29 - Step 04. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_29 - Step 05. Open Corporate Entity Documents tab");
		documentsPage = loansPage.openCorporateEntityDocumentsTab();

		log.info("DocumentTest_29 - Step 06. Click 'Section' search");
		documentsPage.searchSectionAndDocumentType(documentSectionSearch, "");

		log.info("VP: Document is loaded to loan item");
		verifyTrue(documentsPage.isDocumentUploadedToDocumentType(documentType));
	}
	
	@Test(groups = { "regression" }, description = "Document30 - Find loan documents in Corporate Entity Documents when search by 'Section + Document Type'")
	public void DocumentTest_30_FindLoanDocumentsBySectionAndDocumentType() {

		log.info("DocumentTest_30 - Step 01. Open Loans tab");
		loansPage = documentsPage.openLoansPage(driver, ipClient);
				
		log.info("DocumentTest_30 - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_30 - Step 03. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_30 - Step 04. Open Corporate Entity Documents tab");
		documentsPage = loansPage.openCorporateEntityDocumentsTab();
		
		log.info("DocumentTest_30 - Step 05. Click 'Section, Document Type' search");
		documentsPage.searchSectionAndDocumentType(documentSectionSearch, documentType);

		log.info("VP: Document is loaded to loan item");
		verifyTrue(documentsPage.isDocumentUploadedToDocumentType(documentType));
	}
	
	@Test(groups = { "regression" },description = "Document31 - Make sure borrower can see both the old and new document")
	public void DocumentTest_31_OpenBothTheOldDocumentAndNewDocument()
	{
		log.info("DocumentTest_31 - Step 01. Open document type detail");
		documentsPage.openDocumentDetail(documentType);
		
		log.info("DocumentTest_31 - Step 02. Upload general document");
		documentsPage.uploadDocumentFile(fileName02);
		
		log.info("DocumentTest_31 - Step 03. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("VP: Document file name 01 is uploaded successfully");
		verifyTrue(documentsPage.isDocumentHistoryLoadedToDocumentType(fileName01, submitVia, "/" + year));

		log.info("VP: Document file name 02 is uploaded successfully");
		verifyTrue(documentsPage.isDocumentHistoryLoadedToDocumentType(fileName02, submitVia, "/" + year));
	}
	
	@Test(groups = { "regression" }, description = "Document32 - Find Corporate Entity Documents in Documents tab (top nav)")
	public void DocumentTest_32_FindCorporateEntityDocumentInDocumentTabNav() {

		log.info("DocumentTest_32 - Step 01. Open Loans tab");
		loansPage = documentsPage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_32 - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_32 - Step 03. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_32 - Step 04. Open Corporate Entity Documents tab");
		documentsPage = loansPage.openCorporateEntityDocumentsTab();
		
		log.info("DocumentTest_32 - Step 05. Click 'Document Type' search");
		documentsPage.searchDocumentType(documentType);
		
		log.info("VP: Document is loaded to loan item");
		verifyTrue(documentsPage.isDocumentUploadedToDocumentType(documentType));
		
		log.info("DocumentTest_32 - Step 06. Get value 'Document#'");
		documentID = documentsPage.getDocumentID();
		
		log.info("DocumentTest_32 - Step 07. Open 'Documents' Tab Nav");
		documentsPage.openDocumentsPage(driver, ipClient);
		
		log.info("DocumentTest_32 - Step 08. Input 'Document #'");
		documentsPage.searchDocumentID(documentID);
		
		log.info("DocumentTest_32 - Step 09. Click 'Document Type' search");
		documentsPage.searchDocumentType(documentType);
		
		log.info("VP: Document is loaded to Document Tab Nav");
		verifyTrue(documentsPage.isDocumentLoadedInDocumentsTabNav(documentID, documentType));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private DocumentsPage documentsPage;
	private String usernameLender, passwordLender, fileName01, fileName02;
	private String loanName, accountNumber, applicantName,loanStatus;
	private String applicantUsername, applicantPassword;
	private int numberOfDocumentLender, numberOfDocumentIncreased, numberOfDocumentDecreased;
	private int numberOfDocumentBorrower, year;
	private String documentSection, documentType, borrowerName, documentSectionSearch, documentID, submitVia;
}