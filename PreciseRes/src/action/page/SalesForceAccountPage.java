package page;

import org.openqa.selenium.WebDriver;

import PreciseRes.Interfaces;

public class SalesForceAccountPage extends AbstractPage {

	public SalesForceAccountPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}
	
	/**
	 * check SalesForce Account page display
	 * @param N/A
	 */
	public boolean isSalesForceAccountPageDisplay() {
		waitForControl(driver, Interfaces.SalesForceAccountPage.ACCOUNT_TITLE_ICON, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceAccountPage.ACCOUNT_TITLE_ICON);
	}
	
	/**
	 * click New button
	 */
	public void clickNewButton() {
		waitForControl(driver,Interfaces.SalesForceAccountPage.NEW_BUTTON,timeWait);
		click(driver, Interfaces.SalesForceAccountPage.NEW_BUTTON);
		sleep();
	}
	
	/**
	 * click Edit button
	 */
	public void clickEditButton() {
		waitForControl(driver,Interfaces.SalesForceAccountPage.EDIT_BUTTON,timeWait);
		click(driver, Interfaces.SalesForceAccountPage.EDIT_BUTTON);
		sleep();
	}
	
	/**
	 * click Continue button
	 */
	public void clickContinueButton() {
		waitForControl(driver,Interfaces.SalesForceAccountPage.CONTINUE_BUTTON, timeWait);
		click(driver, Interfaces.SalesForceAccountPage.CONTINUE_BUTTON);
		sleep();
	}

	/**
	 *select Record Type New Dropdown
	 */
	public void selectRecordTypeNewDropdown(String borrowerName) {
		waitForControl(driver,Interfaces.SalesForceAccountPage.RECORD_TYPE_NEW_DROPDOWN, borrowerName, timeWait);
		selectItemCombobox(driver, Interfaces.SalesForceAccountPage.RECORD_TYPE_NEW_DROPDOWN, borrowerName);
		sleep();
	}
	
	/**
	 * type Account name
	 * @param accountName
	 */
	public void typeAccountName(String accountName) {
		waitForControl(driver, Interfaces.SalesForceAccountPage.ACCOUNT_NAME_TEXTBOX, timeWait);
		type(driver,Interfaces.SalesForceAccountPage.ACCOUNT_NAME_TEXTBOX, accountName);
	}
	
	/**
	 * type Introduction Source
	 * @param introductionSource
	 */
	public void selectIntroductionSource(String introductionSource) {
		waitForControl(driver, Interfaces.SalesForceAccountPage.INTRODUCTION_SOUCE_DROPDOWN, timeWait);
		type(driver,Interfaces.SalesForceAccountPage.INTRODUCTION_SOUCE_DROPDOWN, introductionSource);
	}
	
	/**
	 *select Strategy Dropdown
	 */
	public void selectStrategyDropdown(String strategy) {
		waitForControl(driver,Interfaces.SalesForceAccountPage.STRATEGY_DROPDOWN, strategy, timeWait);
		selectItemCombobox(driver, Interfaces.SalesForceAccountPage.STRATEGY_DROPDOWN, strategy);
		sleep();
	}
	
	/**
	 *select Loan Product Dropdown
	 */
	public void selectLoanProductDropdown(String loanProducts) {
		waitForControl(driver,Interfaces.SalesForceAccountPage.LOAN_PRODUCT_DROPDOWN, loanProducts, timeWait);
		selectItemCombobox(driver, Interfaces.SalesForceAccountPage.LOAN_PRODUCT_DROPDOWN, loanProducts);
		sleep();
	}
	
	/**
	 * type Requested Loan Size
	 * @param requestedLoanSize
	 */
	public void typeRequestedLoanSize(String requestedLoanSize) {
		waitForControl(driver, Interfaces.SalesForceAccountPage.REQUESTED_LOAN_SIZE_TEXTBOX, timeWait);
		type(driver,Interfaces.SalesForceAccountPage.REQUESTED_LOAN_SIZE_TEXTBOX, requestedLoanSize);
	}
	
	/**
	 * click Save button
	 */
	public void clickSaveButton() {
		waitForControl(driver,Interfaces.SalesForceAccountPage.SAVE_BUTTON, timeWait);
		click(driver, Interfaces.SalesForceAccountPage.SAVE_BUTTON);
		sleep();
	}
	
	/**
	 * check Account name saved successfully
	 * @param N/A
	 */
	public boolean isAccountNameSavedSuccessfully(String accountName) {
		waitForControl(driver, Interfaces.SalesForceAccountPage.DYNAMIC_ACCOUNT_NAME_TITLE, accountName, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceAccountPage.DYNAMIC_ACCOUNT_NAME_TITLE, accountName);
	}
	
	/**
	 * check Introduction Source saved successfully
	 * @param N/A
	 */
	public boolean isIntroductionSourceSavedSuccessfully(String introductionSource) {
		waitForControl(driver, Interfaces.SalesForceAccountPage.DYNAMIC_INTRODUCTION_SOUCE_TITLE, introductionSource, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceAccountPage.DYNAMIC_INTRODUCTION_SOUCE_TITLE, introductionSource);
	}
	
	/**
	 * check Strategy saved successfully
	 * @param N/A
	 */
	public boolean isStrategySavedSuccessfully(String strategy) {
		waitForControl(driver, Interfaces.SalesForceAccountPage.DYNAMIC_STRATEGY_TITLE, strategy, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceAccountPage.DYNAMIC_STRATEGY_TITLE, strategy);
	}
	
	/**
	 * check Loan Products saved successfully
	 * @param N/A
	 */
	public boolean isLoanProductsSavedSuccessfully(String loanProducts) {
		waitForControl(driver, Interfaces.SalesForceAccountPage.DYNAMIC_LOAN_PRODUCTS_TITLE, loanProducts, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceAccountPage.DYNAMIC_LOAN_PRODUCTS_TITLE, loanProducts);
	}
	
	/**
	 * check Requested Loan Size saved successfully
	 * @param N/A
	 */
	public boolean isRequestedLoanSizeSavedSuccessfully(String requestedLoanSize) {
		waitForControl(driver, Interfaces.SalesForceAccountPage.DYNAMIC_REQUESTED_LOAN_SIZE_TITLE, requestedLoanSize, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceAccountPage.DYNAMIC_REQUESTED_LOAN_SIZE_TITLE, requestedLoanSize);
	}
	
	/**
	 * click New Contact button
	 */
	public SalesForceContactsPage clickNewContactButton() {
		waitForControl(driver,Interfaces.SalesForceAccountPage.NEW_CONTACT_BUTTON, timeWait);
		click(driver, Interfaces.SalesForceAccountPage.NEW_CONTACT_BUTTON);
		sleep();
		return PageFactory.getSalesforceContactsPage(driver, ipClient);
	}
	
	/**
	 * open Contact Name
	 */
	public SalesForceContactsPage openContactName(String contactName) {
		waitForControl(driver,Interfaces.SalesForceAccountPage.CONTACT_NAME_TITLE, contactName, timeWait);
		click(driver, Interfaces.SalesForceAccountPage.CONTACT_NAME_TITLE, contactName);
		sleep();
		return PageFactory.getSalesforceContactsPage(driver, ipClient);
	}
	
	/**
	 * click Account Name
	 */
	public void clickAccountName(String accountName) {
		waitForControl(driver,Interfaces.SalesForceAccountPage.ACCOUNT_NAME_TITLE, accountName, timeWait);
		click(driver, Interfaces.SalesForceAccountPage.ACCOUNT_NAME_TITLE, accountName);
		sleep();
	}
	
	private WebDriver driver;
	private String ipClient;
}