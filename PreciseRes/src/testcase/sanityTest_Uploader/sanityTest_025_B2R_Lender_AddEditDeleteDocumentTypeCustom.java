package sanityTest_Uploader;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.UploaderLoginPage;
import page.UploaderPage;

import common.AbstractTest;
import common.Constant;

public class sanityTest_025_B2R_Lender_AddEditDeleteDocumentTypeCustom extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		loanName = "UploaderUdiTesting " + getUniqueNumber();
		applicantName = "Consolidation Demo Borrower New";
		loanStatus = Constant.LOAN_STATUS_B2R;
		documentTypeCustom1 = "Custom Type 01 " + getUniqueNumber();
		documentTypeCustom2 = "Custom Type 02 " + getUniqueNumber();
		specialCharacter = "-�-,.!@#$%^&*()[]{}/?<>";
		documentTypeCustom3 = "Custom " + getUniqueNumber() + specialCharacter;
		borrowerName = "UdiTeamBorrower";
		propertiesFileName = "B2R Data Tape Keyword.xlsx";
		sectionGeneral = "Property Management";
		documentType1 = "L32 - Management Agreement";
		sectionCorporate = "Borrower (SPE) - UdiTeamBorrower";
		documentType2 = "L8 - Borrower Org Chart";
		property1Name = "37412 OAKHILL ST";
		documentType3 = "P5 - Zoning Compliance Letter";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}
	
	@Test(groups = { "regression" },description = "UploaderLender25 - Add document type from keywords and verified that placeholders added in the list document type")
	public void Uploader_21_AddDocumentTypeCustom()
	{
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender,	false);

		log.info("Precondition 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, borrowerName, loanStatus, loanName);
		
		log.info("Precondition 04. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Precondition 05. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("Precondition 06. Click on logged user name on the top right corner");
//		loansPage.clickOnLoggedUserName(driver);
//		uploaderPageUrl = loansPage.getUploaderPageUrl(driver);
//		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
		
		log.info("Precondition 07. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("Precondition 08. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameLender, passwordLender);
		
		log.info("Uploader_21 - Step 01. Click on 'Keywords' button");
		uploaderPage.clickOnKeywordsButton();
		
		log.info("Uploader_21 - Step 02. Add new Keywords");
		uploaderPage.enterNewKeyword(documentTypeCustom1);
				
		log.info("Uploader_21 - Step 03. Click Ok button");
		uploaderPage.clickOkButtonOnKeywords();
		
		log.info("Uploader_21 - Step 04. Click on 'Keywords' button");
		uploaderPage.clickOnKeywordsButton();
		
		log.info("VP: The new Keywords display on table");
		verifyTrue(uploaderPage.isNewKeywordDisplayOnTable(documentTypeCustom1));
		uploaderPage.clickOkButtonOnKeywords();
		
		log.info("Uploader_21 - Step 05. Select loans item");
		uploaderPage.clickChangeGroupedView();
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("Uploader_21 - Step 06. Select folder General Loan Documents");
		uploaderPage.selectSubFolderLevel2("General Loan Documents");
		
		log.info("Uploader_21 - Step 07. Select sub-folder Property Management");
		uploaderPage.selectSubFolderLevel3(sectionGeneral);
		
		log.info("Uploader_21 - Step 08. Click on 'Pencil' icon to edit document");
		uploaderPage.clickOnEditDocumentIcon(documentType1);
		
		log.info("VP: The new keyword display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom1));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_21 - Step 09. Click on 'Select all' checkbox");
		uploaderPage.checkSelectAllCheckbox();
		
		log.info("Uploader_21 - Step 10. Click on change document type link");
		uploaderPage.selectChangeDocumentTypeLink();
		
		log.info("VP: The new keyword display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom1));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_21 - Step 11. Select folder Corporate Entity Documents");
		uploaderPage.selectSubFolderLevel2("Corporate Entity Documents");
		
		log.info("Uploader_21 - Step 12. Select sub-folder 'Borrower (SPE) - UdiTeamBorrower'");
		uploaderPage.selectSubFolderLevel3(sectionCorporate);
		
		log.info("Uploader_21 - Step 13. Click on 'Pencil' icon to edit document");
		uploaderPage.clickOnEditDocumentIcon(documentType2);
		
		log.info("VP: The new keyword display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom1));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_21 - Step 14. Click on 'Select all' checkbox");
		uploaderPage.checkSelectAllCheckbox();
		
		log.info("Uploader_21 - Step 15. Click on change document type link");
		uploaderPage.selectChangeDocumentTypeLink();
		
		log.info("VP: The new keyword display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom1));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_21 - Step 16. Open Property folder");
		uploaderPage.clickPropertiesFolder();
				
		log.info("Uploader_21 - Step 17. Open Property item");
		uploaderPage.selectPropertyFolder(property1Name);
		
		log.info("Uploader_21 - Step 18. Click on 'Pencil' icon to edit document");
		uploaderPage.clickOnEditDocumentIcon(documentType3);
		
		log.info("VP: The new keyword display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom1));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_21 - Step 19. Click on 'Select all' checkbox");
		uploaderPage.checkSelectAllCheckbox();
		
		log.info("Uploader_21 - Step 20. Click on change document type link");
		uploaderPage.selectChangeDocumentTypeLink();
		
		log.info("VP: The new keyword display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom1));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_21 - Step 21. Select folder 'Uncategorized Property Documents'");
		uploaderPage.selectSubFolderLevel2("Uncategorized Property Documents");
		
		log.info("Uploader_21 - Step 22. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherNoDocumentsUpload();
		
		log.info("VP: The new keyword display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom1));
		uploaderPage.clickCancelButtonOnEditDocument();
	}
	
	@Test(groups = { "regression" },description = "UploaderLender26 - Edit document type from keywords and verified that placeholders edited in the list document type")
	public void Uploader_22_EditDocumentTypeCustom()
	{
		log.info("Uploader_22 - Step 01. Click on 'Keywords' button");
		uploaderPage.clickOnKeywordsButton();
		
		log.info("Uploader_22 - Step 02. Edit new Keywords");
		uploaderPage.editNewKeyword(documentTypeCustom1, documentTypeCustom2);
		
		log.info("Uploader_22 - Step 03. Click Ok button");
		uploaderPage.clickOkButtonOnKeywords();
		
		log.info("Uploader_22 - Step 04. Click on 'Keywords' button");
		uploaderPage.clickOnKeywordsButton();
		
		log.info("VP: The new Keywords 01 not display on table");
		verifyFalse(uploaderPage.isNewKeywordDisplayOnTable(documentTypeCustom1));
		
		log.info("VP: The new Keywords 02 display on table");
		verifyTrue(uploaderPage.isNewKeywordDisplayOnTable(documentTypeCustom2));
		uploaderPage.clickOkButtonOnKeywords();
		
		log.info("Uploader_22 - Step 05. Select sub-folder Property Management");
		uploaderPage.selectSubFolderLevel3(sectionGeneral);
		
		log.info("Uploader_22 - Step 06. Click on 'Pencil' icon to edit document");
		uploaderPage.clickOnEditDocumentIcon(documentType1);
		
		log.info("VP: The new Keyword 01 not display on edit document popup");
		verifyFalse(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom1));
		
		log.info("VP: The new Keyword 02 display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom2));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_22 - Step 07. Click on 'Select all' checkbox");
		uploaderPage.checkSelectAllCheckbox();
		
		log.info("Uploader_22 - Step 08. Click on change document type link");
		uploaderPage.selectChangeDocumentTypeLink();
		
		log.info("VP: The new Keyword 01 not display on edit document popup");
		verifyFalse(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom1));
		
		log.info("VP: The new Keyword 02 display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom2));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_22 - Step 09. Select sub-folder 'Borrower (SPE) - UdiTeamBorrower'");
		uploaderPage.selectSubFolderLevel3(sectionCorporate);
		
		log.info("Uploader_22 - Step 10. Click on 'Pencil' icon to edit document");
		uploaderPage.clickOnEditDocumentIcon(documentType2);
		
		log.info("VP: The new Keyword 01 not display on edit document popup");
		verifyFalse(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom1));
		
		log.info("VP: The new Keyword 02 display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom2));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_22 - Step 11. Click on 'Select all' checkbox");
		uploaderPage.checkSelectAllCheckbox();
		
		log.info("Uploader_22 - Step 12. Click on change document type link");
		uploaderPage.selectChangeDocumentTypeLink();
		
		log.info("VP: The new Keyword 01 not display on edit document popup");
		verifyFalse(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom1));
		
		log.info("VP: The new Keyword 02 display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom2));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_22 - Step 13. Open Property item");
		uploaderPage.selectPropertyFolder(property1Name);
		
		log.info("Uploader_22 - Step 14. Click on 'Pencil' icon to edit document");
		uploaderPage.clickOnEditDocumentIcon(documentType3);
		
		log.info("VP: The new Keyword 01 not display on edit document popup");
		verifyFalse(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom1));
		
		log.info("VP: The new Keyword 02 display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom2));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_22 - Step 15. Click on 'Select all' checkbox");
		uploaderPage.checkSelectAllCheckbox();
		
		log.info("Uploader_22 - Step 16. Click on change document type link");
		uploaderPage.selectChangeDocumentTypeLink();
		
		log.info("VP: The new Keyword 01 not display on edit document popup");
		verifyFalse(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom1));
		
		log.info("VP: The new Keyword 02 display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom2));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_22 - Step 17. Select folder 'Uncategorized Property Documents'");
		uploaderPage.selectSubFolderLevel2("Uncategorized Property Documents");
		
		log.info("Uploader_22 - Step 18. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherNoDocumentsUpload();
		
		log.info("VP: The new Keyword 01 not display on edit document popup");
		verifyFalse(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom1));
		
		log.info("VP: The new Keyword 02 display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom2));
		uploaderPage.clickCancelButtonOnEditDocument();
	}
	
	@Test(groups = { "regression" },description = "UploaderLender27 - Edit document type from keywords with special characters and verified that placeholders edited in the list document type")
	public void Uploader_23_EditDocumentTypeCustomWithSpecialCharacters()
	{
		log.info("Uploader_23 - Step 01. Click on 'Keywords' button");
		uploaderPage.clickOnKeywordsButton();
		
		log.info("Uploader_23 - Step 02. Edit new Keywords");
		uploaderPage.editNewKeyword(documentTypeCustom2, documentTypeCustom3);
		
		log.info("Uploader_23 - Step 03. Click Ok button");
		uploaderPage.clickOkButtonOnKeywords();
		
		log.info("Uploader_23 - Step 04. Click on 'Keywords' button");
		uploaderPage.clickOnKeywordsButton();
		
		log.info("VP: The new Keywords 03 display on table");
		verifyTrue(uploaderPage.isNewKeywordDisplayOnTable(documentTypeCustom3));
		uploaderPage.clickOkButtonOnKeywords();
		
		log.info("Uploader_23 - Step 05. Select sub-folder Property Management");
		uploaderPage.selectSubFolderLevel3(sectionGeneral);
		
		log.info("Uploader_23 - Step 06. Click on 'Pencil' icon to edit document");
		uploaderPage.clickOnEditDocumentIcon(documentType1);
		
		log.info("VP: The new Keyword 03 display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom3));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_23 - Step 07. Click on 'Select all' checkbox");
		uploaderPage.checkSelectAllCheckbox();
		
		log.info("Uploader_23 - Step 08. Click on change document type link");
		uploaderPage.selectChangeDocumentTypeLink();
		
		log.info("VP: The new Keyword 03 display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom3));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_23 - Step 09. Select sub-folder 'Borrower (SPE) - UdiTeamBorrower'");
		uploaderPage.selectSubFolderLevel3(sectionCorporate);
		
		log.info("Uploader_23 - Step 10. Click on 'Pencil' icon to edit document");
		uploaderPage.clickOnEditDocumentIcon(documentType2);
		
		log.info("VP: The new Keyword 03 display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom3));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_23 - Step 11. Click on 'Select all' checkbox");
		uploaderPage.checkSelectAllCheckbox();
		
		log.info("Uploader_23 - Step 12. Click on change document type link");
		uploaderPage.selectChangeDocumentTypeLink();
		
		log.info("VP: The new Keyword 03 display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom3));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_23 - Step 13. Open Property item");
		uploaderPage.selectPropertyFolder(property1Name);
		
		log.info("Uploader_23 - Step 14. Click on 'Pencil' icon to edit document");
		uploaderPage.clickOnEditDocumentIcon(documentType3);
		
		log.info("VP: The new Keyword 03 display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom3));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_23 - Step 15. Click on 'Select all' checkbox");
		uploaderPage.checkSelectAllCheckbox();
		
		log.info("Uploader_23 - Step 16. Click on change document type link");
		uploaderPage.selectChangeDocumentTypeLink();
		
		log.info("VP: The new Keyword 02 display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom3));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_23 - Step 17. Select folder 'Uncategorized Property Documents'");
		uploaderPage.selectSubFolderLevel2("Uncategorized Property Documents");
		
		log.info("Uploader_23 - Step 18. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherNoDocumentsUpload();
		
		log.info("VP: The new Keyword 02 display on edit document popup");
		verifyTrue(uploaderPage.isKeywordDisplayOnEditDocument(documentTypeCustom3));
		uploaderPage.clickCancelButtonOnEditDocument();
	}
	
	@Test(groups = { "regression" },description = "UploaderLender28 - Delete document type from keywords and verified that placeholders deleted in the list document type")
	public void Uploader_24_DeleteDocumentTypeCustom()
	{
		log.info("Uploader_24 - Step 01. Click on 'Keywords' button");
		uploaderPage.clickOnKeywordsButton();
		
		log.info("Uploader_24 - Step 02. Delete new Keywords");
		uploaderPage.deleteNewKeyword(documentTypeCustom3);
		
		log.info("Uploader_24 - Step 03. Click Ok button");
		uploaderPage.clickOkButtonOnKeywords();
		
		log.info("Uploader_24 - Step 04. Click on 'Keywords' button");
		uploaderPage.clickOnKeywordsButton();
		
		log.info("VP: The new Keywords not display on table");
		verifyFalse(uploaderPage.isNewKeywordDisplayOnTable(documentTypeCustom3));
		uploaderPage.clickOkButtonOnKeywords();
		
		log.info("Uploader_24 - Step 05. Select sub-folder Property Management");
		uploaderPage.selectSubFolderLevel3(sectionGeneral);
		
		log.info("Uploader_24 - Step 06. Click on 'Pencil' icon to edit document");
		uploaderPage.clickOnEditDocumentIcon(documentType1);
		
		log.info("VP: The new Keywords not display on table");
		verifyFalse(uploaderPage.isNewKeywordDisplayOnTable(documentTypeCustom3));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_24 - Step 07. Click on 'Select all' checkbox");
		uploaderPage.checkSelectAllCheckbox();
		
		log.info("Uploader_24 - Step 08. Click on change document type link");
		uploaderPage.selectChangeDocumentTypeLink();
		
		log.info("VP: The new Keywords not display on table");
		verifyFalse(uploaderPage.isNewKeywordDisplayOnTable(documentTypeCustom3));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_24 - Step 09. Select sub-folder 'Borrower (SPE) - UdiTeamBorrower'");
		uploaderPage.selectSubFolderLevel3(sectionCorporate);
		
		log.info("Uploader_24 - Step 10. Click on 'Pencil' icon to edit document");
		uploaderPage.clickOnEditDocumentIcon(documentType2);
		
		log.info("VP: The new Keywords not display on table");
		verifyFalse(uploaderPage.isNewKeywordDisplayOnTable(documentTypeCustom3));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_24 - Step 11. Click on 'Select all' checkbox");
		uploaderPage.checkSelectAllCheckbox();
		
		log.info("Uploader_24 - Step 12. Click on change document type link");
		uploaderPage.selectChangeDocumentTypeLink();
		
		log.info("VP: The new Keywords not display on table");
		verifyFalse(uploaderPage.isNewKeywordDisplayOnTable(documentTypeCustom3));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_24 - Step 13. Open Property item");
		uploaderPage.selectPropertyFolder(property1Name);
		
		log.info("Uploader_24 - Step 14. Click on 'Pencil' icon to edit document");
		uploaderPage.clickOnEditDocumentIcon(documentType3);
		
		log.info("VP: The new Keywords not display on table");
		verifyFalse(uploaderPage.isNewKeywordDisplayOnTable(documentTypeCustom3));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_24 - Step 15. Click on 'Select all' checkbox");
		uploaderPage.checkSelectAllCheckbox();
		
		log.info("Uploader_24 - Step 16. Click on change document type link");
		uploaderPage.selectChangeDocumentTypeLink();
		
		log.info("VP: The new Keywords not display on table");
		verifyFalse(uploaderPage.isNewKeywordDisplayOnTable(documentTypeCustom3));
		uploaderPage.clickCancelButtonOnEditDocument();
		
		log.info("Uploader_24 - Step 17. Select folder 'Uncategorized Property Documents'");
		uploaderPage.selectSubFolderLevel2("Uncategorized Property Documents");
		
		log.info("Uploader_24 - Step 18. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherNoDocumentsUpload();
		
		log.info("VP: The new Keywords not display on table");
		verifyFalse(uploaderPage.isNewKeywordDisplayOnTable(documentTypeCustom3));
		uploaderPage.clickCancelButtonOnEditDocument();
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender, applicantName, borrowerName, loanStatus;
	private String loanName, uploaderPageUrl;
	private String propertiesFileName, sectionGeneral, sectionCorporate, property1Name;
	private String documentType1, documentType2, documentType3;
	private String documentTypeCustom1, documentTypeCustom2;
	private String documentTypeCustom3, specialCharacter;
}