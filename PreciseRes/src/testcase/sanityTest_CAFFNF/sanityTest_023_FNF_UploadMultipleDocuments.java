package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFDocumentPage;
import page.FNFLoansPage;
import page.FNFPropertiesPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_023_FNF_UploadMultipleDocuments extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		loanName = "Edit Loan Auto " + getUniqueNumber();
		documentFolder1 = "Entity Documents";
		documentFolder2 = "Financials";
		documentFileName1 = "datatest1.pdf";
		documentFileName2 = "datatest2.pdf";
		documentFileName3 = "datatest3.pdf";
		documentFileName4 = "datatest 4.pdf";
		documentFileName5 = "datatest 5.pdf";
		documentFileName6 = "datatest 6.pdf";
		locClosingDate = "04/12/2014";
		addressLender = "CAF-FNF Lender Address " + getUniqueNumber();
		city = "Fresno";
		state = "CA - California";
		zip = "95542";
		date = "04/12/2016";
		number = "30";
		price = "$40.00";
		propertyType ="SFR";
		transactionType ="Refinance";
		titleCompany= "testing company global";
		escrowAgent = "testing escrow global";
		insuranceCompany = "testing insurance global";
	}

	@Test(groups = { "regression" }, description = "Lender - Login as Lender - Make sure upload multi documents file working correctly")
	public void UploadMultipleDocuments_01_Lender_UploadMultipleFilesLoanPage() {

		log.info("UploadMultipleDocuments_01 - Step 01. Login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);

		log.info("UploadMultipleDocuments_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("UploadMultipleDocuments_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("UploadMultipleDocuments_01 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("UploadMultipleDocuments_01 - Step 05: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("UploadMultipleDocuments_01 - Step 06: Input loan name");
		loanPage.inputRequiredData(loanName, locClosingDate);
		
		log.info("UploadMultipleDocuments_01 - Step 07: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("UploadMultipleDocuments_01 - Step 08. Upload document type by clicking the place holder");
		loanPage.uploadDynamicDocumentFileByPlaceHolder(documentFolder1, documentFileName1, documentFileName2, documentFileName3);
		
		log.info("VP: Check folder icon displays");
		verifyTrue(loanPage.isImageDisplayed("folderIcon.png"));
		
		log.info("UploadMultipleDocuments_01 - Step 08. Click on Save button");
		loanPage.clickOnSaveButton();
		
		log.info("VP: All Documents are loaded in 'Document' child-table with right folder");
		verifyTrue(loanPage.isDocumentLoadedToDocumentType(documentFolder1, documentFileName1.replace(".pdf","")));
		verifyTrue(loanPage.isDocumentLoadedToDocumentType(documentFolder1, documentFileName2.replace(".pdf","")));
		verifyTrue(loanPage.isDocumentLoadedToDocumentType(documentFolder1, documentFileName3.replace(".pdf","")));

		log.info("VP: Documents name displayed correctly with documents-count");
		verifyTrue(loanPage.isDocumentFolderNameDisplayedCorrectly(documentFolder1, "3"));
		
		log.info("UploadMultipleDocuments_01 - Step 09. Open document folder");
		documentsPage = loanPage.openDocumentsFolder(documentFolder1);
		
		log.info("VP: All loaded Documents are displayed in this folder");
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName1));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName2));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName3));
		
		log.info("UploadMultipleDocuments_01 - Step 10. Upload document file name 01");
		documentsPage.uploadDocumentFileForFolder(documentFileName4, documentFileName5, documentFileName6);
		
		log.info("UploadMultipleDocuments_01 - Step 11. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("VP: All loaded Documents are displayed in this folder");
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName4));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName5));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName6));
	}

	@Test(groups = { "regression" }, description = "Lender - Login as Lender - Make sure upload multi documents file working correctly in Property details page")
	public void UploadMultipleDocuments_02_Lender_UploadMultipleFilesPropertyDetailsPage() {
		
		log.info("UploadMultipleDocuments_02 - Step 01: Click 'New Loan' button");
		loanPage = documentsPage.clickBackToLoanButton();
		
		log.info("VP: Documents name displayed correctly with documents-count");
		verifyTrue(loanPage.isDocumentFolderNameDisplayedCorrectly(documentFolder1, "6"));
		
		log.info("VP: Check 'More...' link is displayed");
		verifyTrue(loanPage.isDocumentLoadedToDocumentType(documentFolder1, "More..."));
		
		log.info("UploadMultipleDocuments_02 - Step 02. Open document folder");
		documentsPage = loanPage.clickOnPlaceholderLink(documentFolder1, "More...");
		
		log.info("VP: All loaded Documents are displayed in this folder");
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName1));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName2));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName3));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName4));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName5));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName6));
		
		log.info("UploadMultipleDocuments_02 - Step 03: Click 'New Loan' button");
		loanPage = documentsPage.clickBackToLoanButton();
		
		log.info("UploadMultipleDocuments_02 - Step 04: Go to 'Properties' page");
		propertiesPage = loanPage.openPropertiesPageFNF(driver, ipClient);
		
		log.info("UploadMultipleDocuments_02 - Step 05: Click on 'New' button");
		propertiesPage.clickNewPropertyButton();
		
		log.info("UploadMultipleDocuments_02 - Step 06: Input valid  all required information");
		propertiesPage.inputRequiredInformationProperties(companyName, loanName, addressLender, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany);
		
		log.info("UploadMultipleDocuments_02 - Step 07: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();

		log.info("VP: Record Saved message displays");
		verifyTrue(propertiesPage.isRecordSavedMessageDisplays());
		
		log.info("UploadMultipleDocuments_02 - Step 08. Upload document type by clicking the place holder");
		loanPage.uploadDynamicDocumentFileByPlaceHolder(documentFolder1, documentFileName1, documentFileName2, documentFileName3);

		log.info("VP: Check folder icon displays");
		verifyTrue(documentsPage.isImageDisplayed("folderIcon.png"));
		
		log.info("UploadMultipleDocuments_02 - Step 09. Click on Save button");
		loanPage.clickOnSaveButton();
		
		log.info("VP: All Documents are loaded in 'Document' child-table with right folder");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentFolder1, documentFileName1.replace(".pdf","")));
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentFolder1, documentFileName2.replace(".pdf","")));
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentFolder1, documentFileName3.replace(".pdf","")));
		
		log.info("VP: Documents name displayed correctly with documents-count");
		verifyTrue(propertiesPage.isDocumentFolderNameDisplayedCorrectly(documentFolder1, "3"));
		
		log.info("UploadMultipleDocuments_02 - Step 10. Open document folder");
		documentsPage = propertiesPage.openDocumentsFolder(documentFolder1);
		
		log.info("VP: All loaded Documents are displayed in this folder");
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName1));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName2));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName3));
		
		log.info("UploadMultipleDocuments_02 - Step 11. Upload document file name 01");
		documentsPage.uploadDocumentFileForFolder(documentFileName4, documentFileName5, documentFileName6);
		
		log.info("UploadMultipleDocuments_02 - Step 12. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("VP: All loaded Documents are displayed in this folder");
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName4));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName5));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName6));
		
		log.info("UploadMultipleDocuments_02 - Step 13. Click Back to Property button");
		propertiesPage = documentsPage.clickBackToProperty();
		
		log.info("VP: Documents name displayed correctly with documents-count");
		verifyTrue(propertiesPage.isDocumentFolderNameDisplayedCorrectly(documentFolder1, "6"));
		
		log.info("VP: Check 'More...' link is displayed");
		verifyTrue(loanPage.isDocumentLoadedToDocumentType(documentFolder1, "More..."));
		
		log.info("UploadMultipleDocuments_02 - Step 14. Open document folder");
		documentsPage = loanPage.clickOnPlaceholderLink(documentFolder1, "More...");
		
		log.info("VP: All loaded Documents are displayed in this folder");
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName1));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName2));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName3));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName4));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName5));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName6));
	}
	
	@Test(groups = { "regression" }, description = "Applicant - Login as Borrower - Make sure upload multi documents file working correctly")
	public void UploadMultipleDocuments_03_Borrower_UploadMultipleFilesLoanPage() {

		log.info("UploadMultipleDocuments_03 - Step 01. Login with Borrower");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);

		log.info("UploadMultipleDocuments_03 - Step 02: Open 'Loan Name'");
		loanPage = applicantsPage.openLoanNameInOpportunityTable(loanName);
	
		log.info("UploadMultipleDocuments_03 - Step 03. Upload document type by clicking the place holder");
		loanPage.uploadDynamicDocumentFileByPlaceHolder(documentFolder2, documentFileName1, documentFileName2, documentFileName3);

		log.info("VP: Check folder icon displays");
		verifyTrue(loanPage.isImageDisplayed("folderIcon.png"));
		
		log.info("UploadMultipleDocuments_03 - Step 04. Click on Save button");
		loanPage.clickOnSaveButton();
		
		log.info("VP: All Documents are loaded in 'Document' child-table with right folder");
		verifyTrue(loanPage.isDocumentLoadedToDocumentType(documentFolder2, documentFileName1.replace(".pdf","")));
		verifyTrue(loanPage.isDocumentLoadedToDocumentType(documentFolder2, documentFileName2.replace(".pdf","")));
		verifyTrue(loanPage.isDocumentLoadedToDocumentType(documentFolder2, documentFileName3.replace(".pdf","")));
		
		log.info("VP: Documents name displayed correctly with documents-count");
		verifyTrue(loanPage.isDocumentFolderNameDisplayedCorrectly(documentFolder2, "3"));
		
		log.info("UploadMultipleDocuments_03 - Step 05. Open document folder");
		documentsPage = loanPage.openDocumentsFolder(documentFolder2);
		
		log.info("VP: All loaded Documents are displayed in this folder");
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName1));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName2));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName3));
		
		log.info("UploadMultipleDocuments_03 - Step 06. Upload document file name 01");
		documentsPage.uploadDocumentFileForFolder(documentFileName4, documentFileName5, documentFileName6);
		
		log.info("UploadMultipleDocuments_03 - Step 07. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("VP: All loaded Documents are displayed in this folder");
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName4));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName5));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName6));
	}

	@Test(groups = { "regression" }, description = "Applicant - Login as Borrower - Make sure upload multi documents file working correctly in Property details page")
	public void UploadMultipleDocuments_04_Borrower_UploadMultipleFilesPropertyDetailsPage() {
		
		log.info("UploadMultipleDocuments_04 - Step 01: Click 'New Loan' button");
		loanPage = documentsPage.clickBackToLoanButton();
		
		log.info("VP: Documents name displayed correctly with documents-count");
		verifyTrue(loanPage.isDocumentFolderNameDisplayedCorrectly(documentFolder2, "6"));
		
		log.info("VP: Check 'More...' link is displayed");
		verifyTrue(loanPage.isDocumentLoadedToDocumentType(documentFolder2, "More..."));
		
		log.info("UploadMultipleDocuments_04 - Step 02. Click on More link");
		documentsPage = loanPage.clickOnPlaceholderLink(documentFolder2, "More...");
		
		log.info("VP: All loaded Documents are displayed in this folder");
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName1));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName2));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName3));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName4));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName5));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName6));
		
		log.info("UploadMultipleDocuments_04 - Step 03: Click 'New Loan' button");
		loanPage = documentsPage.clickBackToLoanButton();
		
		log.info("UploadMultipleDocuments_04 - Step 04: Go to 'Properties' page");
		propertiesPage = loanPage.openPropertiesPageFNF(driver, ipClient);
		
		log.info("UploadMultipleDocuments_04 - Step 05: Click on 'New' button");
		propertiesPage.clickNewPropertyButton();
		
		log.info("UploadMultipleDocuments_04 - Step 06: Input valid  all required information");
		propertiesPage.inputRequiredInformationProperties(companyName, loanName, addressLender, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany);
		
		log.info("UploadMultipleDocuments_04 - Step 07: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();

		log.info("VP: Record Saved message displays");
		verifyTrue(propertiesPage.isRecordSavedMessageDisplays());
		
		log.info("UploadMultipleDocuments_04 - Step 06. Upload document type by clicking the place holder");
		loanPage.uploadDynamicDocumentFileByPlaceHolder(documentFolder2, documentFileName1, documentFileName2, documentFileName3);
		loanPage.clickOnSaveButton();
		
		log.info("VP: All Documents are loaded in 'Document' child-table with right folder");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentFolder2, documentFileName1.replace(".pdf","")));
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentFolder2, documentFileName2.replace(".pdf","")));
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentFolder2, documentFileName3.replace(".pdf","")));
		
		log.info("VP: Documents name displayed correctly with documents-count");
		verifyTrue(propertiesPage.isDocumentFolderNameDisplayedCorrectly(documentFolder2, "3"));
		
		log.info("UploadMultipleDocuments_04 - Step 08. Open document folder");
		documentsPage = propertiesPage.openDocumentsFolder(documentFolder2);
		
		log.info("VP: All loaded Documents are displayed in this folder");
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName1));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName2));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName3));
		
		log.info("UploadMultipleDocuments_04 - Step 09. Upload document file name 01");
		documentsPage.uploadDocumentFileForFolder(documentFileName4, documentFileName5, documentFileName6);
		
		log.info("UploadMultipleDocuments_04 - Step 10. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("VP: All loaded Documents are displayed in this folder");
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName4));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName5));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName6));
		
		log.info("UploadMultipleDocuments_04 - Step 12. Click Back to Property button");
		propertiesPage = documentsPage.clickBackToProperty();
		
		log.info("VP: Documents name displayed correctly with documents-count");
		verifyTrue(propertiesPage.isDocumentFolderNameDisplayedCorrectly(documentFolder2, "6"));
		
		log.info("VP: Check 'More...' link is displayed");
		verifyTrue(loanPage.isDocumentLoadedToDocumentType(documentFolder2, "More..."));
		
		log.info("UploadMultipleDocuments_04 - Step 02. Click on More link");
		documentsPage = loanPage.clickOnPlaceholderLink(documentFolder2, "More...");
		
		log.info("VP: All loaded Documents are displayed in this folder");
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName1));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName2));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName3));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName4));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName5));
		verifyTrue(documentsPage.isDocumentLoadedToDocumentType(documentFileName6));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private FNFLoansPage loanPage;
	private FNFDocumentPage documentsPage;
	private FNFPropertiesPage propertiesPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername,	FNFBorrowerPassword;
	private String documentFolder1, documentFolder2;
	private String documentFileName1, documentFileName2, documentFileName3, documentFileName4, documentFileName5, documentFileName6;
	private String companyName, accountNumber, loanName, locClosingDate;
	private String addressLender, city, state, zip, date, number, price;
	private String propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany;
	}