package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import common.AbstractTest;
import common.Constant;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;

public class sanityTest_011_STO_DownloadFileFromPropertyScreen extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + getUniqueNumber();
		propertiesFileName = "StoreDataTape.xlsx";
		excelExtension = ".xlsx";
		csvExtension = "List of Properties.csv";
	}

	@Test(groups = { "regression" }, description = "Export Data Tape 01 - Download list Property (CSV)")
	public void ExportDataTape_01_DownloadCSV() {

		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("Precondition - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("ExportDataTape_01 - Step 01: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("ExportDataTape_01 - Step 02: Click 'Add Properties' button");
		transactionPage.clickAddPropertyButton();

		log.info("ExportDataTape_01 - Step 03: Load 'Property Excel Template' file");
		transactionPage.uploadFileProperties(propertiesFileName);

		log.info("ExportDataTape_01 - Step 04: Click 'Download CSV' image");
		transactionPage.deleteAllFileInFolder();
		transactionPage.clickOnDownloadCSVImage();

		log.info("ExportDataTape_01 - Step 05. Wait for file downloaded complete");
		transactionPage.waitForDownloadFileFullnameCompleted(csvExtension);

		log.info("VP: File number in folder equal 1");
		countFile = transactionPage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("ExportDataTape_01 - Step 06. Delete the downloaded file");
		transactionPage.deleteContainsFileName(csvExtension);
	}

	@Test(groups = { "regression" }, description = "Export Data Tape 02 - Export datatape (Export all Properties)")
	public void ExportDataTape_02_ExportDataTapeAllProperties() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("Precondition - Step 04: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("ExportDataTape_02 - Step 01. Click 'Export datatape' button");
		transactionPage.deleteAllFileInFolder();
		transactionPage.clickExportDatatapeButton();

		log.info("ExportDataTape_02 - Step 02. Click 'Export all Properties' radio button");
		transactionPage.clickExportAllPropertiesRadioButton("1");

		log.info("ExportDataTape_02 - Step 03. Wait for file downloaded complete");
		transactionPage.waitForDownloadFileContainsNameCompleted(excelExtension);

		log.info("VP: File number in folder equal 1");
		countFile = transactionPage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("ExportDataTape_02 - Step 04. Delete the downloaded file");
		transactionPage.deleteContainsFileName(excelExtension);
	}

	@Test(groups = { "regression" }, description = "Export Data Tape 03 - Export datatape (Don't export Parent Properties)")
	public void ExportDataTape_03_ExportDataTapeDontParentProperties() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("Precondition - Step 04: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("ExportDataTape_03 - Step 01. Click 'Export datatape' button");
		transactionPage.deleteAllFileInFolder();
		transactionPage.clickExportDatatapeButton();

		log.info("ExportDataTape_03 - Step 02. Click 'Don't export Parent Properties' radio button");
		transactionPage.clickExportAllPropertiesRadioButton("2");

		log.info("ExportDataTape_03 - Step 03. Wait for file downloaded complete");
		transactionPage.waitForDownloadFileContainsNameCompleted(excelExtension);

		log.info("VP: File number in folder equal 1");
		countFile = transactionPage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("ExportDataTape_03 - Step 04. Delete the downloaded file");
		transactionPage.deleteContainsFileName(excelExtension);
	}

	@Test(groups = { "regression" }, description = "Export Data Tape 04 - Export datatape (Don't Export Sub-units)")
	public void ExportDataTape_04_ExportDataTapeDontSubUnit() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("Precondition - Step 04: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("ExportDataTape_03 - Step 01. Click 'Export datatape' button");
		transactionPage.deleteAllFileInFolder();
		transactionPage.clickExportDatatapeButton();

		log.info("ExportDataTape_03 - Step 02. Click 'Don't Export Sub-units' radio button");
		transactionPage.clickExportAllPropertiesRadioButton("3");

		log.info("ExportDataTape_03 - Step 03. Wait for file downloaded complete");
		transactionPage.waitForDownloadFileContainsNameCompleted(excelExtension);

		log.info("VP: File number in folder equal 1");
		countFile = transactionPage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("ExportDataTape_03 - Step 04. Delete the downloaded file");
		transactionPage.deleteContainsFileName(excelExtension);
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName, propertiesFileName, excelExtension, csvExtension;
	private int countFile;
}