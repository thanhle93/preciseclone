package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_007_B2R_CreateNewLoanAppearInTheBucket extends
		AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		loanName1 = "UdiTeam-Loan1" + accountNumber;
		loanName2 = "UdiTeam-Loan2" + accountNumber;
		loanName3 = "UdiTeam-Loan3" + accountNumber;
		loanName4 = "UdiTeam-Loan4" + accountNumber;
		loanName5 = "UdiTeam-Loan5" + accountNumber;
		loanStatus1 = "Closed Lost";
		loanStatus2 = "Dead - Not Loan Qualified";
		loanStatus3 = "Dead After Deposit - Lost";
		loanStatus4 = "Dead After Deposit -Not Loan Qualified";
		loanStatus5 = "Dead - Not Real Deal/No RR";
		loanStatus = "Dead";
	}

	@Test(groups = { "regression" }, description = "Loan60 - Create new Loan with status Closed Lost")
	public void CreateNewLoanAppearInTheBucket_01_CreateNewLoanWithStatusClosedLost() {
		log.info("CreateNewLoanAppearInTheBucket_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("CreateNewLoanAppearInTheBucket_01 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("CreateNewLoanAppearInTheBucket_01 - Step 03. Create new loan");
		loansPage.createNewLoan(loanName1, "", "", loanStatus1, loanName1);
		
		log.info("VP: Verify that Status is added successfully");
		verifyTrue(loansPage.isLoansReadOnlyDisplayWithCorrectInfo(loanName1, loanStatus1));

		log.info("CreateNewLoanAppearInTheBucket_01 - Step 04. Go to Home page");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("CreateNewLoanAppearInTheBucket_01 - Step 05. Click the bucket 'Dead'");
		loansPage = homePage.openCategory(loanStatus);

		log.info("CreateNewLoanAppearInTheBucket_01 - Step 06. Search loan item");
		loansPage.searchLoanByName(loanName1);

		log.info("VP: The loan displays in right category");
		verifyTrue(loansPage.isLoansDisplayOnSearchLoanPage(loanName1));
	}

	@Test(groups = { "regression" }, description = "Loan61 - Create new loan with status Dead � Not Loan Qualified")
	public void CreateNewLoanAppearInTheBucket_02_CreateNewLoanWithStatusDeadNotLoanQualified() {

		log.info("CreateNewLoanAppearInTheBucket_02 - Step 01. Open Loans tab");
		loansPage.openLoansPage(driver, ipClient);

		log.info("CreateNewLoanAppearInTheBucket_02 - Step 02. Create new loan");
		loansPage.createNewLoan(loanName2, "", "", loanStatus2, loanName2);
		
		log.info("VP: Verify that Status is added successfully");
		verifyTrue(loansPage.isLoansReadOnlyDisplayWithCorrectInfo(loanName2, loanStatus2));

		log.info("CreateNewLoanAppearInTheBucket_02 - Step 03. Go to Home page");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("CreateNewLoanAppearInTheBucket_02 - Step 04. Open the bucket 'Dead'");
		loansPage = homePage.openCategory(loanStatus);

		log.info("CreateNewLoanAppearInTheBucket_02 - Step 05. Search loan item");
		loansPage.searchLoanByName(loanName2);

		log.info("VP: The loan displays in right category");
		verifyTrue(loansPage.isLoansDisplayOnSearchLoanPage(loanName2));
	}

	@Test(groups = { "regression" }, description = "Loan62 - Create new loan with status Dead After Deposit � Lost")
	public void CreateNewLoanAppearInTheBucket_03_CreateNewLoanWithStatusDeadAfterDepositLost() {

		log.info("CreateNewLoanAppearInTheBucket_03 - Step 01. Open Loans tab");
		loansPage.openLoansPage(driver, ipClient);

		log.info("CreateNewLoanAppearInTheBucket_03 - Step 02. Create new loan");
		loansPage.createNewLoan(loanName3, "", "", loanStatus3, loanName3);

		log.info("VP: Verify that Status is added successfully");
		verifyTrue(loansPage.isLoansReadOnlyDisplayWithCorrectInfo(loanName3, loanStatus3));

		log.info("CreateNewLoanAppearInTheBucket_03 - Step 03. Go to Home page");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("CreateNewLoanAppearInTheBucket_03 - Step 04. Open the bucket 'Dead'");
		loansPage = homePage.openCategory(loanStatus);

		log.info("CreateNewLoanAppearInTheBucket_03 - Step 05. Search loan item");
		loansPage.searchLoanByName(loanName3);

		log.info("VP: The loan displays in right category");
		verifyTrue(loansPage.isLoansDisplayOnSearchLoanPage(loanName3));
	}

	@Test(groups = { "regression" }, description = "Loan63 - Create new loan with status Dead After Deposit � Not Loan Qualifed")
	public void CreateNewLoanAppearInTheBucket_04_CreateNewLoanWithStatusDeadAfterDepositNotLoanQualified() {

		log.info("CreateNewLoanAppearInTheBucket_04 - Step 01. Open Loans tab");
		loansPage.openLoansPage(driver, ipClient);

		log.info("CreateNewLoanAppearInTheBucket_04 - Step 02. Create new loan");
		loansPage.createNewLoan(loanName4, "", "", loanStatus4, loanName4);
		
		log.info("VP: Verify that Status is added successfully");
		verifyTrue(loansPage.isLoansReadOnlyDisplayWithCorrectInfo(loanName4, loanStatus4));

		log.info("CreateNewLoanAppearInTheBucket_04 - Step 03. Go to Home page");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("CreateNewLoanAppearInTheBucket_04 - Step 04. Open the bucket 'Dead'");
		loansPage = homePage.openCategory(loanStatus);

		log.info("CreateNewLoanAppearInTheBucket_04 - Step 05. Search loan item");
		loansPage.searchLoanByName(loanName4);

		log.info("VP: The loan displays in right category");
		verifyTrue(loansPage.isLoansDisplayOnSearchLoanPage(loanName4));
	}

	@Test(groups = { "regression" }, description = "Loan64 - Create new loan with status Dead � Not Real Deal/No RR")
	public void CreateNewLoanAppearInTheBucket_05_CreateNewLoanWithStatusDeadAfterDepositLost() {

		log.info("CreateNewLoanAppearInTheBucket_05 - Step 01. Open Loans tab");
		loansPage.openLoansPage(driver, ipClient);

		log.info("CreateNewLoanAppearInTheBucket_05 - Step 02. Create new loan");
		loansPage.createNewLoan(loanName5, "", "", loanStatus5,	loanName5);
		
		log.info("VP: Verify that Status is added successfully");
		verifyTrue(loansPage.isLoansReadOnlyDisplayWithCorrectInfo(loanName5, loanStatus5));

		log.info("CreateNewLoanAppearInTheBucket_05 - Step 03. Go to Home page");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("CreateNewLoanAppearInTheBucket_05 - Step 04. Open the bucket 'Dead'");
		loansPage = homePage.openCategory(loanStatus);

		log.info("CreateNewLoanAppearInTheBucket_05 - Step 05. Search loan item");
		loansPage.searchLoanByName(loanName5);

		log.info("VP: The loan displays in right category");
		verifyTrue(loansPage.isLoansDisplayOnSearchLoanPage(loanName5));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender, loanStatus, loanStatus1, 
	loanStatus2, loanStatus3, loanStatus4, loanStatus5;
	private String loanName1, loanName2, loanName3, loanName4, loanName5,
	accountNumber;
}