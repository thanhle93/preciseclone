package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFLoanDashboardPage;
import page.FNFLoansPage;
import page.LoginPage;
import page.PageFactory;
import page.SalesForceAccountPage;
import page.SalesForceContactsPage;
import page.SalesForceDealsPage;
import page.SalesForceHomePage;
import page.SalesForceLoginPage;
import common.AbstractTest;
import common.Common;
import common.Constant;

public class sanityTest_001_FNF_SalesForce extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		fnfLenderUsername = Constant.USERNAME_FNF_LENDER;
		fnfLenderPassword = Constant.PASSWORD_FNF_LENDER;
		salesForceUsername = Constant.USERNAME_SALESFORCE;
		salesForcePassword = Constant.PASSWORD_SALESFORCE;
		accountNumber = Constant.ACCOUNT_NUMBER;
		date = Common.getCommon().getCurrentDay();
		month = Common.getCommon().getCurrentMonth();
		year = Common.getCommon().getCurrentYear();
		loggedUsername = "Uzi Livneh";
		salesForceUrl = "https://test.salesforce.com";
		recordTypeValue = "Borrower";
		accountName = "Test_Borrower " + accountNumber;
		strategy = "Fix/Flip";
		loanProducts = "Line of Credit";
		requestedLoanSize = "30";
		introductionSource = "CAF Website";
		email = "minhdam06@gmail.com";
		lastName = "Auto_" + accountNumber; 
		dealRecordType = "LOC Loan";
		dealName = "Loan Auto " + accountNumber;
		stageStatus = "Data Tape Received";
		closeDate = month + "/" + date + "/" + year; 
		locLoanTypeEntreprenuer = "LOC - Entreprenuer";
		productTypeEntreprenuer = "Entrepreneurial";
		loanOwner = "Uzi Livneh";
		loanSize = "30";
		rate = "10%";
		editAccountName = "Edit_Borrower " + accountNumber;
		editIntroductionSource = "CAF Website";
		editFax = "(650) 383-8444";
		editRequestedLoanSize = "60";
		editLastName = "EditAuto_" + accountNumber; 
		editCloseDate = month + "/" + date + "/" + (year - 1);
		editEmail = "minhdam06@gmail.com";
		editDealName = "Edit Loan Auto " + accountNumber;
		locLoanTypeInstitiutional = "LOC - Institiutional";
		productTypeInstitiutional = "Institutional";
		editLoanSize = "60";
	}

	@Test(groups = { "regression" }, description = "Sales force 01 - New Accounts/ Contacts/ Deals")
	public void Salesforce_01_NewAccountsContactsDeals() {
		//aaaaaaaa
		
		log.info("Salesforce_01 - Step 01. Open Salesforce hyperlink page");
		preciseRESUrl = loginPage.getCurrentUrl(driver);
		salesForceLoginPage = loginPage.openSalesForceLink(driver, salesForceUrl);
		
		log.info("Salesforce_01 - Step 02. Type in username and password");
		salesForceLoginPage.typeUsername(salesForceUsername);
		salesForceLoginPage.typePassword(salesForcePassword);
		
		log.info("Salesforce_01 - Step 03. Click 'Log In to Sandbox' button");
		salesForceHomePage = salesForceLoginPage.clickSubmitButton(driver, ipClient);
		
		log.info("VP. SalesForce homepage display correctly");
		verifyTrue(salesForceHomePage.isSalesForceHomePageDisplay());
		
		log.info("Salesforce_01 - Step 04. Open 'Account' tab");
		salesForceAccountPage = salesForceHomePage.openSalesforceAccountPage(driver, ipClient);		
		
		log.info("VP. SalesForce account page display correctly");
		verifyTrue(salesForceAccountPage.isSalesForceAccountPageDisplay());
		
		log.info("Salesforce_01 - Step 05: Click on 'New' button");
		salesForceAccountPage.clickNewButton();
		
		log.info("Salesforce_01 - Step 06: Select 'Borrower' in 'Record Type New' dropdown");
		salesForceAccountPage.selectRecordTypeNewDropdown(recordTypeValue);
		
		log.info("Salesforce_01 - Step 07: Click on 'Continue' button");
		salesForceAccountPage.clickContinueButton();
		
		log.info("Salesforce_01 - Step 08. Type in 'Account Name, Fax, Strategy, Loan Products and Requested Loan Size'");
		salesForceAccountPage.typeAccountName(accountName);
		salesForceAccountPage.selectIntroductionSource(introductionSource);
		salesForceAccountPage.selectStrategyDropdown(strategy);
		salesForceAccountPage.selectLoanProductDropdown(loanProducts);
		salesForceAccountPage.typeRequestedLoanSize(requestedLoanSize);
		
		log.info("Salesforce_01 - Step 09. Click 'Save' button'");
		salesForceAccountPage.clickSaveButton();
		
		log.info("VP. Account name saved correctly");
		verifyTrue(salesForceAccountPage.isAccountNameSavedSuccessfully(accountName));
		
		log.info("VP. Introduction Srouce saved correctly");
		verifyTrue(salesForceAccountPage.isIntroductionSourceSavedSuccessfully(introductionSource));
		
		log.info("VP. Strategy saved correctly");
		verifyTrue(salesForceAccountPage.isStrategySavedSuccessfully(strategy));
		
		log.info("VP. Loan Products saved correctly");
		verifyTrue(salesForceAccountPage.isLoanProductsSavedSuccessfully(loanProducts));
		
		log.info("VP. Requested Loan Size saved correctly");
		verifyTrue(salesForceAccountPage.isRequestedLoanSizeSavedSuccessfully("$" + requestedLoanSize));
		
		log.info("Salesforce_01 - Step 10. Click 'New Contact' button'");
		salesForceContactPage = salesForceAccountPage.clickNewContactButton();
		
		log.info("Salesforce_01 - Step 11. Type in 'Last Name, Inbound Date, Email, Introduction Source'");
		salesForceContactPage.typeLastName(lastName);
		salesForceContactPage.typeInboundDate(closeDate);
		salesForceContactPage.typeEmail(email);
		salesForceContactPage.typeIntroductionSource(introductionSource);
		
		log.info("Salesforce_01 - Step 12. Click 'Save' button'");
		salesForceContactPage.clickSaveButton();
		
		log.info("VP. Last name saved correctly");
		verifyTrue(salesForceContactPage.isLastNameSavedSuccessfully(lastName));
		
		log.info("VP. Inbound Date saved correctly");
		verifyTrue(salesForceContactPage.isInboundDateSavedSuccessfully(closeDate));
		
		log.info("VP. Email saved correctly");
		verifyTrue(salesForceContactPage.isEmailSavedSuccessfully(email));
		
		log.info("Salesforce_01 - Step 13. Click 'New Deal' button'");
		salesForceDealPage = salesForceContactPage.clickNewDealButton();
		
		log.info("Salesforce_01 - Step 14: Select 'LOC Loan' in 'Record Type New' dropdown");
		salesForceDealPage.selectRecordTypeNewDropdown(dealRecordType);
		
		log.info("Salesforce_01 - Step 15: Click on 'Continue' button");
		salesForceDealPage.clickContinueButton();
		
		log.info("Salesforce_01 - Step 16. Type in 'Deal Name, Stage, Close Date, LOC Loan Type, Loan Size and Rate'");
		salesForceDealPage.typeDealName(dealName);
		salesForceDealPage.selectStageDropdown(stageStatus);
		salesForceDealPage.typeCloseDate(closeDate);
		salesForceDealPage.selectLOCLoanTypeDropdown(locLoanTypeEntreprenuer);
		salesForceDealPage.typeLoanSize(loanSize);
		salesForceDealPage.selectRateDropdown(rate);
		salesForceDealPage.typePrimaryContact(lastName);
     	
		log.info("Salesforce_01 - Step 17. Click 'Save' button'");
		salesForceDealPage.clickSaveButton();
		
		log.info("VP. Deal name saved correctly");
		verifyTrue(salesForceDealPage.isDealNameSavedSuccessfully(dealName));
		
		log.info("VP. Stage saved correctly");
		verifyTrue(salesForceDealPage.isStageSavedSuccessfully(stageStatus));
		
		log.info("VP. Close Date saved correctly");
		verifyTrue(salesForceDealPage.isCloseDateSavedSuccessfully(closeDate));
		
		log.info("VP. Primary Contact saved correctly");
		verifyTrue(salesForceDealPage.isPrimaryContactSavedSuccessfully(lastName));
		
		log.info("VP. LOC Loan Type saved correctly");
		verifyTrue(salesForceDealPage.isLocLoanTypeSavedSuccessfully(locLoanTypeEntreprenuer));
		
		log.info("VP. Loan Size saved correctly");
		verifyTrue(salesForceDealPage.isLoanSizeSavedSuccessfully("$" + loanSize));
		
		log.info("VP. Rate saved correctly");
		verifyTrue(salesForceDealPage.isRateSavedSuccessfully(rate));
		
		log.info("Salesforce_01 - Step 18. Click 'PreciseRE' button'");
		salesForceDealPage.clickPreciseREButton();
		
		log.info("VP: Response message sent to PreciseRES displayed successfully");
		verifyTrue(salesForceDealPage.isDocumentViewable("Success!  Data was successfully sent to PRES."));
		
		log.info("Salesforce_01 - Step 19. Logout the Salesforce system");
		salesForceLoginPage = salesForceDealPage.clickOnLoggedUserName(loggedUsername);

		log.info("Salesforce_01 - Step 20. Open PreciseRES webapp");
		loginPage = salesForceLoginPage.openPreciseResUrlByBorrower(driver, ipClient, preciseRESUrl);
		
		log.info("Salesforce_01 - Step 21. Login with Lender");
		loanDashboardPage = loginPage.loginAsCAFFNFLender(fnfLenderUsername, fnfLenderPassword,false);
		
		log.info("Salesforce_01 - Step 22. Open Borrowers tab");
		borrowersPage = loanDashboardPage.openBorrowersPage(driver, ipClient);
		
		log.info("Salesforce_01 - Step 23: Input 'Borrower Name' and click 'Search' button");
		borrowersPage.clickOnSearchButton(accountName);
		
		log.info("Salesforce_01 - Step 24: Click 'Borrower Name'");
		borrowersPage.clickOnApplicantName();
		
		log.info("VP: Verify that 'Borrower Name, Fax, Last Name, Contact Email' is displayed correctly");
		verifyTrue(borrowersPage.isBorrowerPrimaryContactSaved(accountName, lastName, email));
		
		log.info("VP: Verify that 'Loan Name, Product Type, Loan Owner, Approved Line Amount' is displayed correctly in Opportunity table");
		verifyTrue(borrowersPage.isLoansDisplayInOpportunityTable(dealName, productTypeEntreprenuer, loanOwner, loanSize));
		
		log.info("Salesforce_01 - Step 25: Open 'Loan Name'");
		loansPage = borrowersPage.openLoanNameInOpportunityTable(dealName);
		
		log.info("VP: Verify that 'Loan Name, LOC Closing Date, Product Type, Approved Line Amount, Loan Record Type' is displayed correctly");
		verifyTrue(loansPage.isDataInLoanPageDisplay(dealName, closeDate, productTypeEntreprenuer, loanSize, dealRecordType));
	}
	
	@Test(groups = { "regression" }, description = "Sales force 02 - Edit Account (Borrower)")
	public void Salesforce_02_EditAccountsContactsDeals() {
		log.info("Salesforce_02 - Step 01. Open Salesforce hyperlink page");
		loginPage = logout(driver, ipClient);
		salesForceLoginPage = loginPage.openSalesForceLink(driver, salesForceUrl);
		
		log.info("Salesforce_02 - Step 02. Type in username and password");
		salesForceLoginPage.typeUsername(salesForceUsername);
		salesForceLoginPage.typePassword(salesForcePassword);
		
		log.info("Salesforce_02 - Step 03. Click 'Log In to Sandbox' button");
		salesForceHomePage = salesForceLoginPage.clickSubmitButton(driver, ipClient);
		
		log.info("Salesforce_02 - Step 04. Open 'Account' tab");
		salesForceAccountPage = salesForceHomePage.openSalesforceAccountPage(driver, ipClient);	
		
		log.info("Salesforce_02 - Step 05. Open 'Account Name' was created");
		salesForceAccountPage.clickAccountName(accountName);
		
		log.info("Salesforce_02 - Step 06: Click on 'Edit' button");
		salesForceAccountPage.clickEditButton();

		log.info("Salesforce_02 - Step 07. Type in 'Account Name, Fax, Requested Loan Size'");
		salesForceAccountPage.typeAccountName(editAccountName);
		salesForceAccountPage.selectIntroductionSource(editIntroductionSource);
		salesForceAccountPage.typeRequestedLoanSize(editRequestedLoanSize);
		
		log.info("Salesforce_02 - Step 08. Click 'Save' button'");
		salesForceAccountPage.clickSaveButton();
		
		log.info("VP. Account name saved correctly");
		verifyTrue(salesForceAccountPage.isAccountNameSavedSuccessfully(editAccountName));
		
		log.info("VP. Introduction Source saved correctly");
		verifyTrue(salesForceAccountPage.isIntroductionSourceSavedSuccessfully(editIntroductionSource));
		
		log.info("VP. Requested Loan Size saved correctly");
		verifyTrue(salesForceAccountPage.isRequestedLoanSizeSavedSuccessfully("$" + editRequestedLoanSize));

		log.info("Salesforce_02 - Step 09. Open 'Contact Name' was created");
		salesForceContactPage = salesForceAccountPage.openContactName(lastName);
		
		log.info("Salesforce_02 - Step 10: Click on 'Edit' button");
		salesForceContactPage.clickEditButton();
		
		log.info("Salesforce_02 - Step 11. Type in 'Last Name, Inbound Date, Email, Fax'");
		salesForceContactPage.typeLastName(editLastName);
		salesForceContactPage.typeInboundDate(editCloseDate);
		salesForceContactPage.typeEmail(editEmail);
		salesForceContactPage.typeFax(editFax);
		
		log.info("Salesforce_02 - Step 12. Click 'Save' button'");
		salesForceContactPage.clickSaveButton();
		
		log.info("VP. Last name saved correctly");
		verifyTrue(salesForceContactPage.isLastNameSavedSuccessfully(editLastName));
		
		log.info("VP. Inbound Date saved correctly");
		verifyTrue(salesForceContactPage.isInboundDateSavedSuccessfully(editCloseDate));
		
		log.info("VP. Email saved correctly");
		verifyTrue(salesForceContactPage.isEmailSavedSuccessfully(editEmail));
		
		log.info("VP. Fax saved correctly");
		verifyTrue(salesForceContactPage.isFaxSavedSuccessfully(editFax));
		
		log.info("Salesforce_02 - Step 13. Open 'Deal Name' was created'");
		salesForceDealPage = salesForceContactPage.openDealsName(dealName);
		
		log.info("Salesforce_02 - Step 14: Click on 'Edit' button");
		salesForceDealPage.clickEditButton();
		
		log.info("Salesforce_02 - Step 15. Type in 'Deal Name, Stage, Close Date, LOC Loan Type, Loan Size and Rate'");
		salesForceDealPage.typeDealName(editDealName);
		salesForceDealPage.typeCloseDate(editCloseDate);
		salesForceDealPage.selectLOCLoanTypeDropdown(locLoanTypeInstitiutional);
		salesForceDealPage.typeLoanSize(editLoanSize);
		
		log.info("Salesforce_02 - Step 16. Click 'Save' button'");
		salesForceDealPage.clickSaveButton();
		
		log.info("VP. Deal name saved correctly");
		verifyTrue(salesForceDealPage.isDealNameSavedSuccessfully(editDealName));
		
		log.info("VP. Close Date saved correctly");
		verifyTrue(salesForceDealPage.isCloseDateSavedSuccessfully(editCloseDate));
		
		log.info("VP. LOC Loan Type saved correctly");
		verifyTrue(salesForceDealPage.isLocLoanTypeSavedSuccessfully(locLoanTypeInstitiutional));
		
		log.info("VP. Loan Size saved correctly");
		verifyTrue(salesForceDealPage.isLoanSizeSavedSuccessfully("$" + editLoanSize));
		
		log.info("Salesforce_02 - Step 17. Click 'PreciseRE' button'");
		salesForceDealPage.clickPreciseREButton();
		
		log.info("VP: Response message sent to PreciseRES displayed successfully");
		verifyTrue(salesForceDealPage.isDocumentViewable("Success!  Data was successfully sent to PRES."));
		
		log.info("Salesforce_02 - Step 19. Logout the Salesforce system");
		salesForceLoginPage = salesForceDealPage.clickOnLoggedUserName(loggedUsername);

		log.info("Salesforce_02 - Step 20. Open PreciseRES webapp");
		loginPage = salesForceLoginPage.openPreciseResUrlByBorrower(driver, ipClient, preciseRESUrl);
		
		log.info("Salesforce_02 - Step 21. Login with Lender");
		loanDashboardPage = loginPage.loginAsCAFFNFLender(fnfLenderUsername, fnfLenderPassword,false);
		
		log.info("Salesforce_02 - Step 22. Open Borrowers tab");
		borrowersPage = loanDashboardPage.openBorrowersPage(driver, ipClient);
		
		log.info("Salesforce_02 - Step 23: Input 'Borrower Name' and click 'Search' button");
		borrowersPage.clickOnSearchButton(editAccountName);
		
		log.info("Salesforce_02 - Step 24: Click 'Borrower Name'");
		borrowersPage.clickOnApplicantName();
		
		log.info("VP: Verify that 'Borrower Name, Fax, Last Name, Contact Email, Contact Fax' is displayed correctly");
		verifyTrue(borrowersPage.isBorrowerPrimaryContactSaved(editAccountName, editLastName, editEmail));
		
		log.info("VP: Verify that 'Loan Name, Product Type, Loan Owner, Approved Line Amount' is displayed correctly in Opportunity table");
		verifyTrue(borrowersPage.isLoansDisplayInOpportunityTable(editDealName, productTypeInstitiutional, loanOwner, editLoanSize));
		
		log.info("Salesforce_02 - Step 25: Open 'Loan Name'");
		loansPage = borrowersPage.openLoanNameInOpportunityTable(editDealName);
		
		log.info("VP: Verify that 'Loan Name, LOC Closing Date, Product Type, Approved Line Amount, Loan Record Type' is displayed correctly");
		verifyTrue(loansPage.isDataInLoanPageDisplay(editDealName, editCloseDate, productTypeInstitiutional, editLoanSize, dealRecordType));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private SalesForceLoginPage salesForceLoginPage;
	private SalesForceHomePage salesForceHomePage;
	private SalesForceAccountPage salesForceAccountPage;
	private SalesForceContactsPage salesForceContactPage;
	private SalesForceDealsPage salesForceDealPage;
	private LoginPage loginPage;
	private FNFLoanDashboardPage loanDashboardPage;
	private FNFBorrowersPage borrowersPage;
	private FNFLoansPage loansPage;
	private String fnfLenderUsername, fnfLenderPassword, salesForceUsername, salesForcePassword, salesForceUrl;
	private String recordTypeValue, accountName, strategy, loanProducts, introductionSource, preciseRESUrl;
	private String email, lastName, requestedLoanSize, dealRecordType, loggedUsername, accountNumber;
	private String dealName, stageStatus, closeDate, loanSize, rate, locLoanTypeEntreprenuer;
	private String productTypeEntreprenuer, loanOwner, productTypeInstitiutional;
	private String editAccountName, editFax, editRequestedLoanSize, locLoanTypeInstitiutional;
	private String editLastName, editCloseDate, editEmail, editDealName, editLoanSize, editIntroductionSource;
	private int date, month, year;
	
}