package sanityTest_Store;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;

import common.AbstractTest;
import common.Constant;

public class sanityTest_022_STO_CheckConditionalMandatoryFieldLeaseDetailPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + getUniqueNumber();
	}

	@Test(groups = { "regression" }, description = "Check Conditional Mandatory Field 01  - Payments and Adjustments section")
	public void CheckConditionalMandatoryFieldLeaseDetailPage_01_PaymentsAndAdjustments() {
		
		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);
		
		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("Precondition - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 01: Open Lessee detail tab");
		transactionPage.openLeaseDetailsTab();
		
		//Increase type = CPIL
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 02: Select Icrease type CPIL");
		transactionPage.selectIncreaseType("CPIL");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 03: Input the data empty to 'Increase Frequency' field");
		transactionPage.inputIncreaseFrequency(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 04: Input the data empty to 'Increase Rate' field");
		transactionPage.inputIncreaseRate(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 04: Input the data empty to 'Increase Rate' field");
		transactionPage.inputNextIncreaseDate(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 05: Input the data empty to 'Leverage Factor' field");
		transactionPage.inputLeverageFactor(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Next Increase Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_NextIncrease", "Next Increase Date is required"));
		
		log.info("VP: 'Increase Frequency is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_IncreaseFrequency", "Increase Frequency is required"));
		
		log.info("VP: 'Increase Rate is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_IncreaseRate", "Increase Rate is required"));
		
		log.info("VP: 'Leverage Factor is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_LeverageFactor", "Leverage Factor is required"));
		
		//Increase type = PER
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 07: Select Icrease type PER");
		transactionPage.selectIncreaseType("PER");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 08: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Next Increase Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_NextIncrease", "Next Increase Date is required"));
		
		log.info("VP: 'Increase Frequency is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_IncreaseFrequency", "Increase Frequency is required"));
		
		log.info("VP: 'Increase Rate is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_IncreaseRate", "Increase Rate is required"));
		
		//Increase type contains CPI
		//CPIG
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 10: Select Icrease type CPIG");
		transactionPage.selectIncreaseType("CPIG");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 12: Input the data empty to 'Increase Frequency' field");
		transactionPage.inputIncreaseFrequency(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 13: Input the data empty to 'Increase Rate' field");
		transactionPage.inputNextIncreaseDate(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 14: Input the data empty to 'Leverage Factor' field");
		transactionPage.inputLeverageFactor(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 15: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Next Increase Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_NextIncrease", "Next Increase Date is required"));
		
		log.info("VP: 'Increase Frequency is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_IncreaseFrequency", "Increase Frequency is required"));
		
		log.info("VP: 'Leverage Factor is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_LeverageFactor", "Leverage Factor is required"));
		
		//CPIFX
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 16: Select Icrease type CPIFX");
		transactionPage.selectIncreaseType("CPIFX");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 17: Input the data empty to 'Increase Frequency' field");
		transactionPage.inputIncreaseFrequency(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 18: Input the data empty to 'Increase Rate' field");
		transactionPage.inputNextIncreaseDate(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 19: Input the data empty to 'Leverage Factor' field");
		transactionPage.inputLeverageFactor(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 20: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Next Increase Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_NextIncrease", "Next Increase Date is required"));
		
		log.info("VP: 'Increase Frequency is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_IncreaseFrequency", "Increase Frequency is required"));
		
		log.info("VP: 'Leverage Factor is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_LeverageFactor", "Leverage Factor is required"));
		
		//CPIO
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 21: Select Icrease type CPIO");
		transactionPage.selectIncreaseType("CPIO");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 22: Input the data empty to 'Increase Frequency' field");
		transactionPage.inputIncreaseFrequency(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 23: Input the data empty to 'Increase Rate' field");
		transactionPage.inputNextIncreaseDate(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 24: Input the data empty to 'Leverage Factor' field");
		transactionPage.inputLeverageFactor(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 25: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Next Increase Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_NextIncrease", "Next Increase Date is required"));
		
		log.info("VP: 'Increase Frequency is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_IncreaseFrequency", "Increase Frequency is required"));
		
		log.info("VP: 'Leverage Factor is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_LeverageFactor", "Leverage Factor is required"));
		
		//CPISG
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 26: Select Increase Type CPISG");
		transactionPage.selectIncreaseType("CPISG");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 27: Input the data empty to 'Increase Frequency' field");
		transactionPage.inputIncreaseFrequency(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 28: Input the data empty to 'Increase Rate' field");
		transactionPage.inputNextIncreaseDate(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 29: Input the data empty to 'Leverage Factor' field");
		transactionPage.inputLeverageFactor(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 30: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Next Increase Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_NextIncrease", "Next Increase Date is required"));
		
		log.info("VP: 'Increase Frequency is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_IncreaseFrequency", "Increase Frequency is required"));
		
		log.info("VP: 'Leverage Factor is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_LeverageFactor", "Leverage Factor is required"));
		
		//CPIPS
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 31: Select Increase Type CPIPS");
		transactionPage.selectIncreaseType("CPIPS");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 32: Input the data empty to 'Increase Frequency' field");
		transactionPage.inputIncreaseFrequency(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 33: Input the data empty to 'Increase Rate' field");
		transactionPage.inputNextIncreaseDate(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 34: Input the data empty to 'Leverage Factor' field");
		transactionPage.inputLeverageFactor(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 35: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Next Increase Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_NextIncrease", "Next Increase Date is required"));
		
		log.info("VP: 'Increase Frequency is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_IncreaseFrequency", "Increase Frequency is required"));
		
		log.info("VP: 'Leverage Factor is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_LeverageFactor", "Leverage Factor is required"));
		
		//VPCPI
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 36: Select Increase Type VPCPI");
		transactionPage.selectIncreaseType("VPCPI");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 37: Input the data empty to 'Increase Frequency' field");
		transactionPage.inputIncreaseFrequency(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 38: Input the data empty to 'Increase Rate' field");
		transactionPage.inputNextIncreaseDate(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 39: Input the data empty to 'Leverage Factor' field");
		transactionPage.inputLeverageFactor(" ");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_01 - Step 40: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Next Increase Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_NextIncrease", "Next Increase Date is required"));
		
		log.info("VP: 'Increase Frequency is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_IncreaseFrequency", "Increase Frequency is required"));
		
		log.info("VP: 'Leverage Factor is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_LeverageFactor", "Leverage Factor is required"));
	}
	
	@Test(groups = { "regression" }, description = "Check Conditional Mandatory Field 02  - Rent reset section")
	public void CheckConditionalMandatoryFieldLeaseDetailPage_02_RentResetSection() {
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_02 - Step 01: Check to 'Rent Reset' checkbox");
		transactionPage.selectRentReset();
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_02 - Step 02: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Next Reset Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_NextResetDate", "Next Reset Date is required"));
		
		log.info("VP: 'Reset Frequency is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_ResetFrequency", "Reset Frequency is required"));
	}
	
	@Test(groups = { "regression" }, description = "Check Conditional Mandatory Field 03  - Tenant Purchase Option section")
	public void CheckConditionalMandatoryFieldLeaseDetailPage_03_TenantPurchaseOption() {
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_03 - Step 01: Check to 'Tenant Purchase Option' checkbox");
		transactionPage.selectTenantPurchaseOption();
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_03 - Step 02: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Purchase Option Type is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_PurschareOptionType", "Purchase Option Type is required"));
		
		log.info("VP: 'Purchase Option Timing is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_PurschareOptionTiming", "Purchase Option Timing is required"));
		
		log.info("VP: 'Purchase Option Terms is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_PurschareOptionTerm", "Purchase Option Terms is required"));
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_03 - Step 03: Select 'NOCON' in Purchase Option Type dropdown");
		transactionPage.selectPurchaseOptionType("NOCON");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_03 - Step 04: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Date First Exercisable is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_DateFirstExersable", "Date First Exercisable is required"));
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_03 - Step 05: Select 'TIME' in Purchase Option Type dropdown");
		transactionPage.selectPurchaseOptionType("TIME");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_03 - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Date First Exercisable is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_DateFirstExersable", "Date First Exercisable is required"));
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_03 - Step 07: Select 'COMB' in Purchase Option Type dropdown");
		transactionPage.selectPurchaseOptionType("COMB");
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_03 - Step 08: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Date First Exercisable is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_DateFirstExersable", "Date First Exercisable is required"));
	}
	
	@Test(groups = { "regression" }, description = "Check Conditional Mandatory Field 02  - Other Terms section")
	public void CheckConditionalMandatoryFieldLeaseDetailPage_04_OtherTerms() {
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_04 - Step 01: Uncheck to 'TripleNet' checkbox");
		transactionPage.scrollPage(driver);
		transactionPage.unSelectTripleNet();
		
		log.info("CheckConditionalMandatoryFieldLeaseDetailPage_04 - Step 02: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'NN Type is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_NNType", "NN Type is required"));
		
		log.info("VP: 'Description of Landlord Obligations is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_LandlordObligations", "Description of Landlord Obligations is required"));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName;
	}