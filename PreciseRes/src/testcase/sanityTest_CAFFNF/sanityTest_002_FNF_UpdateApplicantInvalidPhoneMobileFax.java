package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;

import common.AbstractTest;
import common.Constant;

public class sanityTest_002_FNF_UpdateApplicantInvalidPhoneMobileFax extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		invalid = "(433) 792-6263*~^#%$?Fail";
	}

	@Test(groups = { "regression" }, description = "Applicant 02 - Login as Lender and input invalid 'Borrower Phone' field")
	public void Applicant_02_InputInvalidPhone() {
		log.info("Applicant_02 - Step 01: Login with valid username password");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);

		log.info("Applicant_02 - Step 02: Go to 'Borrower' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
		
		log.info("Applicant_02 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("Applicant_02 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("Applicant_02 - Step 05: Input invalid 'Borrower Phone'");
		applicantsPage.inputPhoneCellFax(invalid, "", "", "", "");

		log.info("Applicant_02 - Step 06: Click on 'Save' button");
		applicantsPage.clickOnSaveButton();

		log.info("VP: The error message should be displayed");
		verifyTrue(applicantsPage.isErrorMessageDisplayed("Not a valid Phone Number."));
	}
	
	@Test(groups = { "regression" }, description = "Applicant 02 - Login as Lender and input invalid 'Borrower Fax' field")
	public void Applicant_03_InputInvalidFax() {
		
		log.info("Applicant_03 - Step 01: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
		
		log.info("Applicant_03 - Step 02: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("Applicant_03 - Step 03: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("Applicant_03 - Step 04: Input invalid 'Borrower Fax'");
		applicantsPage.inputPhoneCellFax("", invalid, "", "", "");

		log.info("Applicant_03 - Step 05: Click on 'Save' button");
		applicantsPage.clickOnSaveButton();

		log.info("VP: The error message should be displayed");
		verifyTrue(applicantsPage.isErrorMessageDisplayed("Not a valid Fax Number."));
	}
	
	@Test(groups = { "regression" }, description = "Applicant 02 - Login as Lender and input invalid 'Primary Phone' field")
	public void Applicant_04_InputInvalidPrimaryPhone() {
		
		log.info("Applicant_04 - Step 01: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
		
		log.info("Applicant_04 - Step 02: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("Applicant_04 - Step 03: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();

		log.info("Applicant_04 - Step 04: Input invalid 'Primary Phone'");
		applicantsPage.inputPhoneCellFax("", "", invalid, "", "");

		log.info("Applicant_04 - Step 05: Click on 'Save' button");
		applicantsPage.clickOnSaveButton();

		log.info("VP: The error message should be displayed");
		verifyTrue(applicantsPage.isErrorMessageDisplayed("Not a valid Phone Number."));
	}
	
	@Test(groups = { "regression" }, description = "Applicant 02 - Login as Lender and input invalid 'Primary Mobile' field")
	public void Applicant_05_InputInvalidPrimaryMobile() {
		
		log.info("Applicant_05 - Step 01: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
		
		log.info("Applicant_05 - Step 02: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("Applicant_05 - Step 03: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();

		log.info("Applicant_05 - Step 04: Input invalid 'Primary Mobile'");
		applicantsPage.inputPhoneCellFax("", "", "", invalid, "");

		log.info("Applicant_05 - Step 05: Click on 'Save' button");
		applicantsPage.clickOnSaveButton();

		log.info("VP: The error message should be displayed");
		verifyTrue(applicantsPage.isErrorMessageDisplayed("Not a valid Phone Number."));
	}
	
	@Test(groups = { "regression" }, description = "Applicant 02 - Login as Lender and input invalid 'Primary Fax' field")
	public void Applicant_06_InputInvalidPrimaryFax() {
		
		log.info("Applicant_06 - Step 01: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
		
		log.info("Applicant_06 - Step 02: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("Applicant_06 - Step 03: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();

		log.info("Applicant_06 - Step 04: Input invalid 'Fax'");
		applicantsPage.inputPhoneCellFax("", "", "", "", invalid);

		log.info("Applicant_06 - Step 05: Click on 'Save' button");
		applicantsPage.clickOnSaveButton();

		log.info("VP: The error message should be displayed");
		verifyTrue(applicantsPage.isErrorMessageDisplayed("Not a valid Fax Number."));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private String FNFLenderUsername, FNFLenderPassword;
	private String companyName, invalid, accountNumber;
	}