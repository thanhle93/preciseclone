package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFLoansPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_009_FNF_CheckDefaultFoldersCreated extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		loanName01 = "Edit Loan Auto " + getUniqueNumber() + 1;
		companyName = "Edit_Borrower " + accountNumber;
		locClosingDate = "04/12/2014";
		documentFolder1 = "Financials";
		documentFolder2 = "Entity Documents";
		documentFolder3 = "Business Plan";
		documentFolder4 = "Searches";
		documentFolder5 = "Committee Memo";
		documentFolder6 = "Loan Documents";
		documentFolder7 = "Asset Diligence";
		documentFolder8 = "Advance Funding";
		documentFolder9 = "Insurance";
		documentFolder10 = "Misc Documents";
	}

	@Test(groups = { "regression" }, description = "Applicant 10 - Login as Lender and check that all default placeholders are created")
	public void ChangeCorporate_01_Lender_CheckAllDefaultFolderCreated() {

		log.info("ChangeCorporate_01 - Step 01. Login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("ChangeCorporate_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("ChangeCorporate_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("ChangeCorporate_01 - Step 04: Open 'Applicant Name' detail");
		applicantsPage.clickOnApplicantName();
		
		log.info("ChangeCorporate_01 - Step 05: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("ChangeCorporate_01 - Step 06: Input Loan name and Loc Closing date");
		loanPage.inputRequiredData(loanName01, locClosingDate);
		
		log.info("ChangeCorporate_01 - Step 07: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("VP: 10 default folders displayed correctly");
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder1));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder2));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder3));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder4));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder5));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder6));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder7));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder8));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder9));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder10));
	}
	
	@Test(groups = { "regression" }, description = "Applicant 09 - Login as applicant and check that all default placeholders are created")
	public void ChangeCorporate_02_Borrower_CheckAllDefaultFolderCreated() {

		log.info("ChangeCorporate_02 - Step 01: Login with Applicant");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);

		log.info("ChangeCorporate_02 - Step 02: Open 'Loan Name'");
		loanPage = applicantsPage.openLoanNameInOpportunityTable(loanName01);
		
		log.info("VP: 10 default folders displayed correctly");
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder1));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder2));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder3));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder4));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder5));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder6));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder7));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder8));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder9));
		verifyTrue(loanPage.isDocumentsFolderDisplayed(documentFolder10));
	}


	

	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private FNFLoansPage loanPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername,	FNFBorrowerPassword;
	private String documentFolder1, documentFolder2, documentFolder3, documentFolder4, documentFolder5, documentFolder6, documentFolder7, documentFolder8, documentFolder9, documentFolder10;
	private String companyName;
	private String accountNumber,	loanName01, locClosingDate;
	}