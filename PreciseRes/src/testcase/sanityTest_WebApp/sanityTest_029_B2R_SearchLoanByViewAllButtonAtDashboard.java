package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Constant;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;

public class sanityTest_029_B2R_SearchLoanByViewAllButtonAtDashboard extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "Loan" + accountNumber;
		loanNameWithoutApplicant = "LoanWithoutApp" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower New";
		propertiesFileName = "B2R Required.xlsx";
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 01 - Search by Loan name")
	public void SearchLoanHome_01_SearchByLoanName() {
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Precondition 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);

		log.info("Precondition 04. Change status of Loan");
		loansPage.selectLoanStatusBasicDetail("Term Sheet Sent");

		log.info("Precondition 05. Input Loan Amount Requested");
		loansPage.inputLoanAmountRequested("1000");

		log.info("Precondition 06. Click Save button");
		loansPage.clickSaveButton();
		loansPage.isRecordSavedMessageDisplays();

		log.info("Precondition 07. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Precondition 08. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("SearchLoanHome_01 - Step 01. Open Home page");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("SearchLoanHome_01 - Step 02. Click 'View All' button");
		homePage.clickOnViewAllButton();

		log.info("SearchLoanHome_01 - Step 03. Search by Loan name");
		homePage.searchForLoansHomePageB2R(loanName, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");

		statusChange = homePage.getTextLoanSearch("DataCell FieldTypeDate datacell_StatusChangeDate");
		termSheetIssued = homePage.getTextLoanSearch("DataCell FieldTypeDate datacell_TermsheetDate");
		properties = homePage.getTextLoanSearch("DataCell FieldTypeInteger datacell_PropertyTotal");
		estimatedValue = homePage.getTextLoanSearch("DataCell FieldTypeCurrency datacell_EstimatedPortfolioValue");
		reqLoanAmount = homePage.getTextLoanSearch("DataCell FieldTypeCurrency datacell_LoanAmount");
		requiredDocs = homePage.getTextLoanSearch("DataCell FieldTypeInteger datacell_RequiredDocs");
		noLoaded = homePage.getTextLoanSearch("DataCell FieldTypeInteger datacell_LoadedDocsCount");
		noReviewed = homePage.getTextLoanSearch("DataCell FieldTypeInteger datacell_ReviewedDocsCount");
		noRevised = homePage.getTextLoanSearch("DataCell FieldTypeHTML datacell_Revised");
		dateCreated = homePage.getTextLoanSearch("DataCell FieldTypeDate datacell_CreateDate");

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableB2R(loanName, applicantName, statusChange, termSheetIssued, properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded,
				noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 02 - Search by Applicant name")
	public void SearchLoanHome_02_SearchByApplicantName() {

		log.info("SearchLoanHome_02 - Step 01. Search by Applicant name");
		homePage.searchForLoansHomePageB2R(loanName, applicantName, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");

		log.info("VP: Applicant name display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableB2R(loanName, applicantName, statusChange, termSheetIssued, properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded,
				noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 03 - Search by Status Change")
	public void SearchLoanHome_03_SearchByStatusChange() {

		log.info("SearchLoanHome_03 - Step 01. Search by Status Change");
		homePage.searchForLoansHomePageB2R(loanName, " ", statusChange, " ", " ", " ", " ", " ", " ", " ", " ", " ");

		log.info("VP: Status Change display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableB2R(loanName, applicantName, statusChange, termSheetIssued, properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded,
				noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 04 - Search by Term Sheet Issued")
	public void SearchLoanHome_04_SearchByTermSheetIssued() {

		log.info("SearchLoanHome_04 - Step 01. Search by Term Sheet Issued");
		homePage.searchForLoansHomePageB2R(loanName, " ", " ", termSheetIssued, " ", " ", " ", " ", " ", " ", " ", " ");

		log.info("VP: Term Sheet Issued display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableB2R(loanName, applicantName, statusChange, termSheetIssued, properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded,
				noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 05 - Search by Properties")
	public void SearchLoanHome_05_SearchByProperties() {

		log.info("SearchLoanHome_05 - Step 01. Search by Properties");
		homePage.searchForLoansHomePageB2R(loanName, " ", " ", " ", properties, " ", " ", " ", " ", " ", " ", " ");

		log.info("VP: Properties display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableB2R(loanName, applicantName, statusChange, termSheetIssued, properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded,
				noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 06 - Search by Estimated Value")
	public void SearchLoanHome_06_SearchByEstimatedValue() {

		log.info("SearchLoanHome_06 - Step 01. Search by Estimated Value");
		homePage.searchForLoansHomePageB2R(loanName, " ", " ", " ", " ", estimatedValue, " ", " ", " ", " ", " ", " ");

		log.info("VP: Estimated Value display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableB2R(loanName, applicantName, statusChange, termSheetIssued, properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded,
				noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 07 - Search by Req. Loan Amount")
	public void SearchLoanHome_07_SearchByReqLoanAmount() {

		log.info("SearchLoanHome_07 - Step 01. Search by Req. Loan Amount");
		homePage.searchForLoansHomePageB2R(loanName, " ", " ", " ", " ", " ", reqLoanAmount, " ", " ", " ", " ", " ");

		log.info("VP: Req. Loan Amount display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableB2R(loanName, applicantName, statusChange, termSheetIssued, properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded,
				noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 08 - Search by Required Docs")
	public void SearchLoanHome_08_SearchByRequiredDocs() {

		log.info("SearchLoanHome_08 - Step 01. Search by Required Docs");
		homePage.searchForLoansHomePageB2R(loanName, " ", " ", " ", " ", " ", " ", requiredDocs, " ", " ", " ", " ");

		log.info("VP: Required Docs display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableB2R(loanName, applicantName, statusChange, termSheetIssued, properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded,
				noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 09 - Search by No. Loaded")
	public void SearchLoanHome_09_SearchByNoLoaded() {

		log.info("SearchLoanHome_09 - Step 01. Search by No. Loaded");
		homePage.searchForLoansHomePageB2R(loanName, " ", " ", " ", " ", " ", " ", " ", noLoaded, " ", " ", " ");

		log.info("VP: No. Loaded display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableB2R(loanName, applicantName, statusChange, termSheetIssued, properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded,
				noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 10 - Search by No. Reviewed")
	public void SearchLoanHome_10_SearchByNoReviewed() {

		log.info("SearchLoanHome_10 - Step 01. Search by No. Reviewed");
		homePage.searchForLoansHomePageB2R(loanName, " ", " ", " ", " ", " ", " ", " ", " ", noReviewed, " ", " ");

		log.info("VP: No. Reviewed display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableB2R(loanName, applicantName, statusChange, termSheetIssued, properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded,
				noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 11 - Search by No. Revised")
	public void SearchLoanHome_11_SearchByNoRevised() {

		log.info("SearchLoanHome_11 - Step 01. Search by No. Revised");
		homePage.searchForLoansHomePageB2R(loanName, " ", " ", " ", " ", " ", " ", " ", " ", " ", noRevised, " ");

		log.info("VP: No. Revised display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableB2R(loanName, applicantName, statusChange, termSheetIssued, properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded,
				noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 12 - Search by Date Created")
	public void SearchLoanHome_12_SearchByDateCreated() {

		log.info("SearchLoanHome_12 - Step 01. Search by Date Created");
		homePage.searchForLoansHomePageB2R(loanName, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", dateCreated);

		log.info("VP: Date Created display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableB2R(loanName, applicantName, statusChange, termSheetIssued, properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded,
				noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 13 - Search by Loan name without Applicant")
	public void SearchLoanHome_13_SearchByLoanNameWithoutApplicant() {

		log.info("Precondition 01. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 02. Create new loan");
		loansPage.createNewLoan(loanNameWithoutApplicant, "", "", loanStatus, loanNameWithoutApplicant);

		log.info("Precondition 03. Change status of Loan");
		loansPage.selectLoanStatusBasicDetail("Term Sheet Sent");

		log.info("Precondition 04. Input Loan Amount Requested");
		loansPage.inputLoanAmountRequested("1000");

		log.info("Precondition 05. Click Save button");
		loansPage.clickSaveButton();

		log.info("Precondition 06. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Precondition 07. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("SearchLoanHome_13 - Step 01. Open Home page");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("SearchLoanHome_13 - Step 02. Click 'View All' button");
		homePage.clickOnViewAllButton();

		log.info("SearchLoanHome_13 - Step 03. Search by Loan name");
		homePage.searchForLoansHomePageB2R(loanNameWithoutApplicant, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");

		statusChange = homePage.getTextLoanSearch("DataCell FieldTypeDate datacell_StatusChangeDate");
		termSheetIssued = homePage.getTextLoanSearch("DataCell FieldTypeDate datacell_TermsheetDate");
		properties = homePage.getTextLoanSearch("DataCell FieldTypeInteger datacell_PropertyTotal");
		estimatedValue = homePage.getTextLoanSearch("DataCell FieldTypeCurrency datacell_EstimatedPortfolioValue");
		reqLoanAmount = homePage.getTextLoanSearch("DataCell FieldTypeCurrency datacell_LoanAmount");
		requiredDocs = homePage.getTextLoanSearch("DataCell FieldTypeInteger datacell_RequiredDocs");
		noLoaded = homePage.getTextLoanSearch("DataCell FieldTypeInteger datacell_LoadedDocsCount");
		noReviewed = homePage.getTextLoanSearch("DataCell FieldTypeInteger datacell_ReviewedDocsCount");
		noRevised = homePage.getTextLoanSearch("DataCell FieldTypeHTML datacell_Revised");
		dateCreated = homePage.getTextLoanSearch("DataCell FieldTypeDate datacell_CreateDate");

		log.info("VP: Loan name without Applicant display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableB2R(loanNameWithoutApplicant, "", statusChange, termSheetIssued, properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded,
				noReviewed, noRevised, dateCreated));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender;
	private String loanName, accountNumber, applicantName, propertiesFileName, loanStatus, loanNameWithoutApplicant;
	private String statusChange, termSheetIssued, properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded, noReviewed, noRevised, dateCreated;
}