package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Constant;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;

public class sanityTest_016_STO_SearchActiveInactiveDownloadLesseeDocument extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + getUniqueNumber();
		documentSection = "Applicant";
		documentSectionNew = "Corporate Entity Documents - Applicant";
		documentType = "Applicant 01";
		newDocumentType = "New Document Type";
		documentFileName = "datatest1.pdf";
		csvExtension = "List of Lessee.csv";
		active = "Yes";
		inActive = "No";
		either = "Either";
		loadedYes = "Yes";
		loadedNo = "No";
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 01 - Search by Document #")
	public void SearchActiveInactive_01_SearchByDocumentNumber() {
		
		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("Precondition - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("Precondition - Step 07: Open Lessee tab");
		transactionPage.openLesseeTab();

		log.info("Precondition - Step 08. Click 'New Lessee Documents' button");
		transactionPage.clickNewTransactionDocumentLesseeButton();

		log.info("Precondition - Step 09. Select Section value");
		transactionPage.selectTransactionDocumentLesseeSection(documentSectionNew);
		
		log.info("Precondition - Step 10. Input to 'Other Document Type' textbox");
		transactionPage.inputOtherDocumentType(newDocumentType);
		
		log.info("Precondition - Step 11. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("Precondition - Step 12. Click 'Document List' button");
		transactionPage.clickDocumentListButtonAtDocumentTypeDetails();
		
		log.info("Precondition - Step 13. Click 'New Lessee Documents' button");
		transactionPage.clickNewTransactionDocumentLesseeButton();
		
		log.info("Precondition - Step 14. Select Section value");
		transactionPage.selectTransactionDocumentLesseeSection(documentSectionNew);

		log.info("Precondition - Step 15. Select Document Type");
		transactionPage.selectTransactionDocumentLesseeDocumentType(documentType);

		log.info("Precondition - Step 16. Upload Document file name");
		transactionPage.uploadDocumentFileName(documentFileName);

		log.info("Precondition - Step 17. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("Precondition - Step 18. Click 'Document List' button");
		transactionPage.clickDocumentListButtonAtDocumentTypeDetails();

		log.info("Precondition - Step 19. Select 'Section/ Document Type' search");
		transactionPage.searchSectionAndDocumentType(documentSection, documentType);

		log.info("VP: Document type is created successfully");
		verifyTrue(transactionPage.isDocumentUploadedToDocumentType(documentType));
		
		log.info("Precondition - Step 20. Get value 'Document #'");
		documentID = transactionPage.getDocumentID();
		
		log.info("Precondition - Step 21: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 22: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 23: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_01 - Step 01: Open Lessee tab");
		transactionPage.openLesseeTab();
		
		log.info("SearchActiveInactive_01 - Step 02. Search with Document #");
		transactionPage.searchPropertyDocumentsDocumentNumber(documentID);

		log.info("VP: The Documents with Document # display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, documentSection, documentType, loadedYes, active));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 02 - Search by Section")
	public void SearchActiveInactive_02_SearchBySection() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_02 - Step 01: Open Lessee tab");
		transactionPage.openLesseeTab();

		log.info("SearchActiveInactive_02 - Step 02. Search with Section");
		transactionPage.searchSectionAndDocumentType(documentSection, "");

		log.info("VP: The Documents with Section display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, documentSection, documentType, loadedYes, active));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 03 - Search by Document Type")
	public void SearchActiveInactive_03_SearchByDocumentType() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_03 - Step 01: Open Lessee tab");
		transactionPage.openLesseeTab();

		log.info("SearchActiveInactive_03 - Step 02. Search with Document Type");
		transactionPage.searchPropertyDocumentType(documentType);

		log.info("VP: The Documents with Document Type display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, documentSection, documentType, loadedYes, active));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 04 - Search by Document Loaded")
	public void SearchActiveInactive_04_SearchByDocumentLoaded() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_04 - Step 01: Open Lessee tab");
		transactionPage.openLesseeTab();

		log.info("SearchActiveInactive_04 - Step 02. Search with Document Loaded is Yes");
		transactionPage.searchTransactionLesseeDocumentLoaded(documentSection, loadedYes);

		log.info("VP: The Documents with Document Loaded is Yes display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, documentSection, documentType, loadedYes, active));

		log.info("SearchActiveInactive_04 - Step 03. Search with Document Loaded is No");
		transactionPage.searchTransactionLesseeDocumentLoaded(documentSection, loadedNo);

		log.info("VP: The Documents with Document Loaded is No display in table");
		verifyTrue(transactionPage.isDocumentTypeNoDisplayOnSearchPropertyTransactionLesseeTab(documentSection, newDocumentType, loadedNo, active));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 05 - Search by Document active status")
	public void SearchActiveInactive_05_SearchByActiveStatus() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_05 - Step 01: Open Lessee tab");
		transactionPage.openLesseeTab();

		log.info("SearchActiveInactive_05 - Step 02: Search with Active status");
		transactionPage.searchByActiveOrInactive(documentID, active);

		log.info("VP: The Documents with Document Active display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, documentSection, documentType, loadedYes, active));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 06 - Search by Document either status")
	public void SearchActiveInactive_06_SearchByEitherStatus() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_06 - Step 01: Open Lessee tab");
		transactionPage.openLesseeTab();

		log.info("SearchActiveInactive_06 - Step 02: Search with Either status");
		transactionPage.searchByActiveOrInactive(documentID, either);

		log.info("VP: The Documents with Document Active display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, documentSection, documentType, loadedYes, active));

		log.info("SearchActiveInactive_06 - Step 03: Open Document Type detail");
		transactionPage.openDocumentDetail(documentType);

		log.info("SearchActiveInactive_06 - Step 04. Uncheck active checkbox");
		transactionPage.uncheckActiveCheckbox();
		
		log.info("SearchActiveInactive_06 - Step 05. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("SearchActiveInactive_06 - Step 06. Click 'Document List' button");
		transactionPage.clickDocumentListButtonAtDocumentTypeDetails();
		
		log.info("SearchActiveInactive_06 - Step 07: Search with Either status");
		transactionPage.searchByActiveOrInactive(documentID, either);

		log.info("VP: The Documents with Document Inactive display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, documentSection, documentType, loadedYes, inActive));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 07 - Search by Document inactive status")
	public void SearchActiveInactive_07_SearchByInactiveStatus() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_07 - Step 01: Open Lessee tab");
		transactionPage.openLesseeTab();

		log.info("SearchActiveInactive_07 - Step 02: Search with Inactive status");
		transactionPage.searchByActiveOrInactive(documentID, inActive);

		log.info("VP: The Documents with Document Inactive display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, documentSection, documentType, loadedYes, inActive));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 08 - Make a Document is Active")
	public void SearchActiveInactive_08_MakeADocumentIsActive() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_08 - Step 01: Open Lessee tab");
		transactionPage.openLesseeTab();

		log.info("SearchActiveInactive_08 - Step 02: Search with Inactive status");
		transactionPage.searchByActiveOrInactive(documentID, inActive);

		log.info("VP: The Documents with Document Inactive display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, documentSection, documentType, loadedYes, inActive));

		log.info("SearchActiveInactive_08 - Step 03: Select Document checkbox in List Documents table");
		transactionPage.isSelectedPropertyCheckbox(documentID);

		log.info("SearchActiveInactive_08 - Step 04: Click 'Make Active' button");
		transactionPage.clickMakeActiveButtonAtTransactionLesseeTab();

		log.info("SearchActiveInactive_08 - Step 05: Search with Active status");
		transactionPage.searchByActiveOrInactive(documentID, active);

		log.info("VP: The Documents with Document Active display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, documentSection, documentType, loadedYes, active));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 09 - Make a Document is Inactive")
	public void SearchActiveInactive_09_MakeADocumentIsInactive() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_09 - Step 01: Open Lessee tab");
		transactionPage.openLesseeTab();

		log.info("SearchActiveInactive_09 - Step 02: Search Document by Active status");
		transactionPage.searchByActiveOrInactive(documentID, active);

		log.info("VP: The Documents with Document Active display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, documentSection, documentType, loadedYes, active));

		log.info("SearchActiveInactive_09 - Step 03: Open Document Type detail");
		transactionPage.openDocumentDetail(documentType);

		log.info("SearchActiveInactive_09 - Step 04. Uncheck active checkbox");
		transactionPage.uncheckActiveCheckbox();
		
		log.info("SearchActiveInactive_09 - Step 05. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("SearchActiveInactive_09 - Step 06. Click 'Document List' button");
		transactionPage.clickDocumentListButtonAtDocumentTypeDetails();
		
		log.info("SearchActiveInactive_09 - Step 07: Search Document by Inactive status");
		transactionPage.searchByActiveOrInactive(documentID, inActive);

		log.info("VP: The Documents with Document Inactive display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, documentSection, documentType, loadedYes, inActive));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 10 - Download list Document (Excel file that is created)")
	public void SearchActiveInactive_10_DownloadListDocumentCSV() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_10 - Step 01: Open Lessee tab");
		transactionPage.openLesseeTab();

		log.info("SearchActiveInactive_10 - Step 02: Click 'Download CSV' image");
		transactionPage.deleteAllFileInFolder();
		transactionPage.clickOnDownloadCSVImage();

		log.info("SearchActiveInactive_10 - Step 03. Wait for file downloaded complete");
		transactionPage.waitForDownloadFileFullnameCompleted(csvExtension);

		log.info("VP: File number in folder equal 1");
		countFile = transactionPage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("SearchActiveInactive_10 - Step 04. Delete the downloaded file");
		transactionPage.deleteContainsFileName(csvExtension);
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName, newDocumentType, documentSectionNew;
	private int countFile;
	private String documentSection, documentType, documentFileName;
	private String csvExtension, loadedYes, loadedNo, active, inActive, either, documentID;
}