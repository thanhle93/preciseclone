package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.MailHomePage;
import page.MailLoginPage;
import page.PageFactory;
import page.PropertiesPage;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_0000_B2R_CSVExportTesting extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower New";
		zipExtension = "CheckingDownloadFile.zip";
		zipDownload = ".zip";
		excelExtension = ".xls";
		excelTemplate = "B2RDataTape.xlsx";
		excelMissingSummary = "MissingDocumentsSummary.xlsx";
		propertiesFileName = "B2R Required.xlsx";
		address1 = "37412 OAKHILL ST";
		documentType1 = "P3 - Purchase Contract/HUD-1 Settlement Stmt";
		documentType2 = "P4 - Final Property Condition/Environmental) Report";
		documentType3 = "P5 - Zoning Compliance Letter";
		documentFileName = "datatest.pdf";
		mailPageUrl = "https://accounts.google.com";
		usernameEmail = "minhdam06@gmail.com";
		passwordEmail = "dam123!@#!@#";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
		applicantName = "Consolidation Demo Borrower New";
		propertiesFileName = "B2RDataTape_multi-unit.xlsx";
		parentPropertyAddress = "Oakhill";
		subPropertyAddress = "37225 OAKHILL ST - suite 15";
		standAlonePropertyAddress = "10105 Spring ST";
	}

	@Test(groups = { "regression" }, description = "Download 01 - Export CSV Dashboad list Loan items")
	public void Download_01_ExportCSVDashboard() {
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Precondition 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);

		log.info("Precondition 04. Change status of Loan");
		loansPage.selectLoanStatusBasicDetail("Term Sheet Sent");

		log.info("Precondition 05. Input Loan Amount Requested");
		loansPage.inputLoanAmountRequested("1000");

		log.info("Precondition 06. Click Save button");
		loansPage.clickSaveButton();

		log.info("Precondition 07. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Precondition 08. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("Download_01 - Step 07. Click CSV button");
		
//		homePage.clickOnExportCSVButton();
		
		log.info("Precondition 08. Click on Download CSV button");
		loansPage.deleteAllFileInFolder();
		loansPage.clickOnDownloadCsvFileButton(driver);

		log.info("Download_01 - Step 08. Wait for file downloaded complete");
		homePage.waitForDownloadFileFullnameCompleted("csv");

		log.info("File name is:"+ homePage.getFileNameInDirectory());
		
		verifyTrue(homePage.isCSVFileExportedCorrectly(homePage.getFileNameInDirectory(), "Address"));

		log.info("Download_01 - Step 09. Delete the downloaded file");
		homePage.deleteContainsFileName("csv");
	}

//	@Test(groups = { "regression" }, description = "Download 02 - Export datatape (Export all Properties)")
//	public void Download_02_ExportDataTapeAllProperties() {
//		log.info("Precondition 01. Open Loans tab");
//		loansPage = homePage.openLoansPage(driver, ipClient);
//
//		log.info("Precondition 02. Create new loan");
//		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);
//
//		log.info("Precondition 03. Open Properties tab");
//		loansPage.openPropertiesTab();
//
//		log.info("Precondition 04. Load test file");
//		loansPage.uploadFileProperties(propertiesFileName);
//
//		log.info("Precondition 05. Open a property detail");
//		propertiesPage = loansPage.openPropertyDetail(address1);
//
//		log.info("Precondition 06. Add document 1 by clicking the placeholder");
//		propertiesPage.uploadDynamicDocumentFileByPlaceHolder(documentType1, documentFileName);
//
//		log.info("Precondition 07. Add document 2 by clicking the placeholder");
//		propertiesPage.uploadDynamicDocumentFileByPlaceHolder(documentType2, documentFileName);
//
//		log.info("Precondition 08. Add document 3 by clicking the placeholder");
//		propertiesPage.uploadDynamicDocumentFileByPlaceHolder(documentType3, documentFileName);
//
//		log.info("Precondition 09. Click 'Save' button");
//		propertiesPage.clickSaveButton();
//
//		log.info("VP: Document 1 is loaded to document type");
//		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, documentFileName));
//
//		log.info("VP: Document 2 is loaded to document type");
//		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType2, documentFileName));
//
//		log.info("VP: Document 3 is loaded to document type");
//		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType3, documentFileName));
//
//		log.info("Precondition 10. Click 'Property List' button");
//		loansPage = propertiesPage.clickOnPropertyListButton();
//
//		log.info("Download_02 - Step 01. Click 'Export datatape' button");
//		loansPage.deleteAllFileInFolder();
//		loansPage.clickExportDatatapeButton();
//
//		log.info("Download_02 - Step 02. Click 'Export all Properties' radio button");
//		loansPage.clickExportAllPropertiesRadioButton("1");
//
//		log.info("Download_02 - Step 03. Wait for file downloaded complete");
//		loansPage.waitForDownloadFileContainsNameCompleted(excelExtension);
//
//		log.info("VP: File number in folder equal 1");
//		countFile = loansPage.countFilesInDirectory();
//		verifyEquals(countFile, 1);
//
//		log.info("Download_02 - Step 04. Delete the downloaded file");
//		loansPage.deleteContainsFileName(excelExtension);
//	}
//
//	@Test(groups = { "regression" }, description = "Download 03 - Export datatape (Don't export Parent Properties)")
//	public void Download_03_ExportDataTapeDontParentProperties() {
//		log.info("Download_03 - Step 01. Click 'Export datatape' button");
//		loansPage.deleteAllFileInFolder();
//		loansPage.clickExportDatatapeButton();
//
//		log.info("Download_03 - Step 02. Click 'Don't export Parent Properties' radio button");
//		loansPage.clickExportAllPropertiesRadioButton("2");
//
//		log.info("Download_03 - Step 03. Wait for file downloaded complete");
//		loansPage.waitForDownloadFileContainsNameCompleted(excelExtension);
//
//		log.info("VP: File number in folder equal 1");
//		countFile = loansPage.countFilesInDirectory();
//		verifyEquals(countFile, 1);
//
//		log.info("Download_03 - Step 04. Delete the downloaded file");
//		loansPage.deleteContainsFileName(excelExtension);
//	}
//
//	@Test(groups = { "regression" }, description = "Download 04 - Export datatape (Don't Export Sub-units)")
//	public void Download_04_ExportDataTapeDontSubUnit() {
//		log.info("Download_04 - Step 01. Click 'Export datatape' button");
//		loansPage.deleteAllFileInFolder();
//		loansPage.clickExportDatatapeButton();
//
//		log.info("Download_04 - Step 02. Click 'Don't Export Sub-units' radio button");
//		loansPage.clickExportAllPropertiesRadioButton("3");
//
//		log.info("Download_04 - Step 03. Wait for file downloaded complete");
//		loansPage.waitForDownloadFileContainsNameCompleted(excelExtension);
//
//		log.info("VP: File number in folder equal 1");
//		countFile = loansPage.countFilesInDirectory();
//		verifyEquals(countFile, 1);
//
//		log.info("Download_04 - Step 04. Delete the downloaded file");
//		loansPage.deleteContainsFileName(excelExtension);
//	}
//
//	@Test(groups = { "regression" }, description = "Download 05 - Download template")
//	public void Download_05_DownloadTemplate() {
//		log.info("Download_05 - Step 01. Click 'Download Template' button");
//		loansPage.deleteAllFileInFolder();
//		loansPage.clickDownloadTemplateButton();
//
//		log.info("Download_05 - Step 02. Wait for file downloaded complete");
//		loansPage.waitForDownloadFileFullnameCompleted(excelTemplate);
//
//		log.info("VP: File number in folder equal 1");
//		countFile = loansPage.countFilesInDirectory();
//		verifyEquals(countFile, 1);
//
//		log.info("Download_05 - Step 03. Delete the downloaded file");
//		loansPage.deleteFileFullName(excelTemplate);
//	}
//
//	@Test(groups = { "regression" }, description = "Download 06 - Missing Summary")
//	public void Download_06_DownloadMissingSummary() {
//		log.info("Download_06 - Step 01. Click 'Missing Summary' button");
//		loansPage.deleteAllFileInFolder();
//		loansPage.clickMissingSummaryButton();
//
//		log.info("Download_06 - Step 02. Wait for file downloaded complete");
//		loansPage.waitForDownloadFileFullnameCompleted(excelMissingSummary);
//
//		log.info("VP: File number in folder equal 1");
//		countFile = loansPage.countFilesInDirectory();
//		verifyEquals(countFile, 1);
//
//		log.info("Download_06 - Step 03. Delete the downloaded file");
//		loansPage.deleteFileFullName(excelMissingSummary);
//	}
//
//	@Test(groups = { "regression" }, description = "Download 07 - By Document Type")
//	public void Download_07_DownloadByDocumentType() {
//		log.info("Download_07 - Step 01. Open Property Documents tab");
//		loansPage.openPropertyDocumentsTab();
//
//		log.info("VP: Document 1 is loaded to document type in Property Documents tab");
//		verifyTrue(loansPage.isDocumentLoadedToDocumentType(documentType1));
//
//		log.info("VP: Document 2 is loaded to document type in Property Documents tab");
//		verifyTrue(loansPage.isDocumentLoadedToDocumentType(documentType2));
//
//		log.info("VP: Document 3 is loaded to document type in Property Documents tab");
//		verifyTrue(loansPage.isDocumentLoadedToDocumentType(documentType3));
//
//		log.info("Download_07 - Step 02. Click 'Download Documents' button");
//		loansPage.deleteAllFileInFolder();
//		loansPage.clickDownloadDocumentsButton();
//
//		log.info("Download_07 - Step 03. Click 'By Document Type' radio button");
//		loansPage.clickDocumentTypeRadioButton();
//
//		log.info("Download_07 - Step 04. Click 'Download' button");
//		loansPage.clickDownloadButton();
//
//		log.info("VP: A success message is displayed");
//		verifyTrue(loansPage.isSuccessMessageDisplayed("Complete"));
//
//		log.info("Download_07 - Step 05. Wait for file downloaded complete");
//		loansPage.waitForDownloadFileFullnameCompleted(zipExtension);
//
//		log.info("VP: File number in folder equal 1");
//		countFile = loansPage.countFilesInDirectory();
//		verifyEquals(countFile, 1);
//
//		log.info("Download_07 - Step 06. Delete the downloaded file");
//		loansPage.deleteFileFullName(zipExtension);
//	}
//
//	@Test(groups = { "regression" }, description = "Download 08 - By Address")
//	public void Download_08_DownloadByAddress() {
//		log.info("Download_08 - Step 01. Click 'Download Documents' button");
//		loansPage.clickCancelButton();
//		loansPage.clickDownloadDocumentsButton();
//
//		log.info("Download_08 - Step 02. Click 'By Address' radio button");
//		loansPage.clickAddressRadioButton();
//
//		log.info("Download_08 - Step 03. Click 'Download' button");
//		loansPage.deleteAllFileInFolder();
//		loansPage.clickDownloadButton();
//
//		log.info("VP: A success message is displayed");
//		verifyTrue(loansPage.isSuccessMessageDisplayed("Complete"));
//
//		log.info("Download_08 - Step 04. Wait for file downloaded complete");
//		loansPage.waitForDownloadFileFullnameCompleted(zipExtension);
//
//		log.info("VP: File number in folder equal 1");
//		countFile = loansPage.countFilesInDirectory();
//		verifyEquals(countFile, 1);
//
//		log.info("Download_08 - Step 05. Delete the downloaded file");
//		loansPage.deleteFileFullName(zipExtension);
//
//		log.info("Download_08 - Step 06. Click 'Cancel' button");
//		loansPage.clickCancelButton();
//	}
//
//	@Test(groups = { "regression" }, description = "Download 09 - Uploader - Missing Documents Summary")
//	public void Download_09_DownloadMissingDocumentsSummaryOnUploader() {
//		log.info("Download_09  - Step 01. Go to Uploader");
//		loginPage = logout(driver, ipClient);
//		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
//
//		log.info("Download_09 - Step 02. Login to Uploader page");
//		uploaderPage = uploaderLoginPage.loginUploader(usernameLender, passwordLender);
//
//		log.info("Download_09 - Step 03. Select loans item");
//		uploaderPage.clickChangeGroupedView();
//		uploaderPage.searchLoanItem(loanName);
//		uploaderPage.selectLoanItem(loanName);
//
//		log.info("Download_09 - Step 04.  Click Missing Documents Summary button");
//		uploaderPage.deleteAllFileInFolder();
//		uploaderPage.clickMissingDocumentsSummaryButton();
//
//		log.info("Download_09 - Step 05. Wait for file downloaded complete");
//		uploaderPage.waitForDownloadFileFullnameCompleted(excelMissingSummary);
//
//		log.info("VP: File number in folder equal 1");
//		countFile = uploaderPage.countFilesInDirectory();
//		verifyEquals(countFile, 1);
//
//		log.info("Download_09 - Step 06. Delete the downloaded file");
//		uploaderPage.deleteFileFullName(excelMissingSummary);
//	}
//
//	@Test(groups = { "regression" }, description = "Download 10 - Uploader - Download Documents")
//	public void Download_10_DownloadDocumentsOnUploader() {
//		log.info("Download_10 - Step 01.  Click Download Documents button");
//		uploaderPage.clickDownloadDocumentsButton();
//
//		log.info("VP: Document 1 is loaded to document download");
//		verifyTrue(uploaderPage.isDocumentTypeDisplaysInDownloadDocuments(documentType1));
//
//		log.info("VP: Document 2 is loaded to document download");
//		verifyTrue(uploaderPage.isDocumentTypeDisplaysInDownloadDocuments(documentType2));
//
//		log.info("VP: Document 3 is loaded to document download");
//		verifyTrue(uploaderPage.isDocumentTypeDisplaysInDownloadDocuments(documentType3));
//
//		log.info("Download_10 - Step 02.  Click OK button");
//		uploaderPage.clickOkButtonOnDownloadDocument();
//
//		log.info("VP: 'The system started generating a zip file with the requested documents.' message display");
//		verifyTrue(uploaderPage.isDownloadGeneratingMessageDisplays());
//
//		log.info("Download_10 - Step 03.  Click OK button");
//		uploaderPage.clickOkButtonOnDownloadDocument();
//
//		log.info("Download_10 - Step 04. Open Email url");
//		mailLoginPage = uploaderPage.openMailLink(driver, mailPageUrl);
//		mailLoginPage.loginToGmail(usernameEmail, passwordEmail);
//
//		log.info("Download_10 - Step 05. Click Submit button");
//		mailHomePage = mailLoginPage.clickSubmitButton(driver, ipClient);
//
//		log.info("Download_10 - Step 06. Click 'Google Apps' title");
//		mailHomePage.clickGoogleAppsTitle();
//
//		log.info("Download_10 - Step 07. Click 'Gmail' application");
//		mailHomePage.clickGmailApplication();
//
//		log.info("VP. Gmail home page is displayed");
//		verifyTrue(mailHomePage.isGmailHomePageDisplay());
//
//		log.info("Download_10 - Step 08. Click 'Inbox' title");
//		mailHomePage.clickInboxTitle();
//
//		log.info("Download_10 - Step 09. Click 'Subject first email'");
//		mailHomePage.clickSubjectFirstEmail();
//
//		log.info("VP. Link hyperlink download is displayed");
//		verifyTrue(mailHomePage.isLinkHyperlinkDisplay());
//
//		log.info("Download_10 - Step 10. Click 'Link' hyperlink");
//		mailHomePage.deleteAllFileInFolder();
//		mailHomePage.clickLinkHyperlink();
//
//		log.info("Download_10 - Step 11. Wait for file downloaded complete");
//		mailHomePage.waitForDownloadFileContainsNameCompleted(zipDownload);
//
//		log.info("VP: File number in folder equal 1");
//		countFile = mailHomePage.countFilesInDirectory();
//		verifyEquals(countFile, 1);
//
//		log.info("Download_10 - Step 12. Delete the downloaded file");
//		mailHomePage.deleteContainsFileName(zipDownload);
//
//		log.info("Download_10 - Step 13. Click data tooltip 'More' dropdown");
//		mailHomePage.clickDataTooltipMoreDropdown();
//
//		log.info("Download_10 - Step 14. Click 'Delete this message' title");
//		mailHomePage.clickDeleteThisMessageTitle();
//		while(mailHomePage.isLinkHyperlinkDisplay()==true){
//			mailHomePage.clickDataTooltipMoreDropdown();
//			mailHomePage.clickDeleteThisMessageTitle();
//		}
//		mailHomePage.openLink(driver, "https://mail.google.com/mail/logout?hl=en");
//	}
//
//	@Test(groups = { "regression" }, description = "Download 11 - Download All Documents")
//	public void Download_11_DownloadAllDocuments() {
//		
//		log.info("Download_11 - Step 01. Open PreciseRES webapp");
//		loginPage = mailHomePage.openPreciseResUrlByBorrower(driver, ipClient, preciseRESUrl);
//		
//		log.info("Download_11 - Step 02. Login with Lender");
//		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
//		
//		log.info("Download_11 - Step 03. Open Loan tab");
//		loansPage.openLoansPage(driver, ipClient);
//		
//		log.info("Download_11 - Step 04. Open Loan detail");
//		loansPage.searchLoanByName(loanName);
//		loansPage.openLoansDetailPage(loanName);
//
//		log.info("Download_11 - Step 05. Click 'Download All Document' button");
//		loansPage.clickDownloadAllDocumentsButton();
//
//		log.info("Download_11 - Step 06. Open Email url");
//		mailLoginPage = loansPage.openMailLink(driver, mailPageUrl);
//
//		log.info("Download_11 - Step 07. Input Usename and Password");
//		mailLoginPage.loginToGmail(usernameEmail, passwordEmail);
//
//		log.info("Download_11 - Step 08. Click Submit button");
//		mailHomePage = mailLoginPage.clickSubmitButton(driver, ipClient);
//
//		log.info("Download_11 - Step 09. Click 'Google Apps' title");
//		mailHomePage.clickGoogleAppsTitle();
//
//		log.info("Download_11 - Step 10. Click 'Gmail' application");
//		mailHomePage.clickGmailApplication();
//
//		log.info("VP. Gmail home page is displayed");
//		verifyTrue(mailHomePage.isGmailHomePageDisplay());
//
//		log.info("Download_11 - Step 11. Click 'Inbox' title");
//		mailHomePage.clickInboxTitle();
//
//		log.info("Download_11 - Step 12. Click 'Subject first email'");
//		mailHomePage.clickSubjectFirstEmail();
//
//		log.info("VP. Link hyperlink download is displayed");
//		verifyTrue(mailHomePage.isLinkHyperlinkDisplay());
//
//		log.info("Download_11 - Step 13. Click 'Link' hyperlink");
//		mailHomePage.deleteAllFileInFolder();
//		mailHomePage.clickLinkHyperlink();
//
//		log.info("Download_11 - Step 14. Wait for file downloaded complete");
//		mailHomePage.waitForDownloadFileContainsNameCompleted(zipDownload);
//
//		log.info("VP: File number in folder equal 1");
//		countFile = mailHomePage.countFilesInDirectory();
//		verifyEquals(countFile, 1);
//
//		log.info("Download_11 - Step 15. Delete the downloaded file");
//		mailHomePage.deleteContainsFileName(zipDownload);
//
//		log.info("Download_11 - Step 16. Click data tooltip 'More' dropdown");
//		mailHomePage.clickDataTooltipMoreDropdown();
//
//		log.info("Download_11 - Step 17. Click 'Delete this message' title");
//		mailHomePage.clickDeleteThisMessageTitle();
//		
//		while(mailHomePage.isLinkHyperlinkDisplay()==true){
//			mailHomePage.clickDataTooltipMoreDropdown();
//			mailHomePage.clickDeleteThisMessageTitle();
//		}
//	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private MailLoginPage mailLoginPage;
	private HomePage homePage;
	private MailHomePage mailHomePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private String excelExtension, zipExtension, zipDownload, excelTemplate, excelMissingSummary;
	private String usernameLender, passwordLender, address1, documentFileName;
	private String loanName, accountNumber, applicantName, propertiesFileName, loanStatus;
	private int countFile;
	private String documentType1, documentType2, documentType3, mailPageUrl;
	private String usernameEmail, passwordEmail, uploaderPageUrl, preciseRESUrl;
	private String parentPropertyAddress, subPropertyAddress;
	private String standAlonePropertyAddress;
}