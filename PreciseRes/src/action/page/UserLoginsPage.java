package page;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import PreciseRes.Interfaces;

public class UserLoginsPage extends AbstractPage {
	public UserLoginsPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
	}

	/**
	 * click New User Button
	 */
	public void clickNewUserButton() {
		waitForControl(driver, Interfaces.UserLoginsPage.NEW_USER_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('New').click();");
		sleep();
	}

	/**
	 * input User First Name
	 */
	public void inputUserFirstName(String firstName) {
		waitForControl(driver, Interfaces.UserLoginsPage.USER_FIRST_NAME_TEXTBOX, timeWait);
		if (!firstName.equals("")) {
			type(driver, Interfaces.UserLoginsPage.USER_FIRST_NAME_TEXTBOX, firstName);
			sleep(1);
		}
	}

	/**
	 * input User Last Name
	 */
	public void inputUserLastName(String lastName) {
		waitForControl(driver, Interfaces.UserLoginsPage.USER_LAST_NAME_TEXTBOX, timeWait);
		if (!lastName.equals("")) {
			type(driver, Interfaces.UserLoginsPage.USER_LAST_NAME_TEXTBOX, lastName);
			sleep(1);
		}
	}

	/**
	 * input User Login ID
	 */
	public void inputUserLoginID(String loginId) {
		waitForControl(driver, Interfaces.UserLoginsPage.USER_LOGIN_ID_TEXTBOX, timeWait);
		if (!loginId.equals("")) {
			type(driver, Interfaces.UserLoginsPage.USER_LOGIN_ID_TEXTBOX, loginId);
			sleep(1);
		}
	}

	/**
	 * input Password
	 */
	public void inputPassword(String password) {
		waitForControl(driver, Interfaces.UserLoginsPage.INPUT_PASSWORD_TEXTBOX, timeWait);
		if (!password.equals("")) {
			type(driver, Interfaces.UserLoginsPage.INPUT_PASSWORD_TEXTBOX, password);
			sleep(1);
		}
	}

	/**
	 * retype Password
	 */
	public void retypePassword(String password) {
		waitForControl(driver, Interfaces.UserLoginsPage.RETYPE_PASSWORD_TEXTBOX, timeWait);
		if (!password.equals("")) {
			type(driver, Interfaces.UserLoginsPage.RETYPE_PASSWORD_TEXTBOX, password);
			sleep(1);
		}
	}

	/**
	 * select User Role
	 */
	public void selectUserRole(String userRole) {
		waitForControl(driver, Interfaces.UserLoginsPage.USER_ROLE_COMBOBOX, timeWait);
		if (!userRole.equals("")) {
			selectItemCombobox(driver, Interfaces.UserLoginsPage.USER_ROLE_COMBOBOX, userRole);
			sleep(1);
		}
	}

	/**
	 * input Email
	 */
	public void inputEmail(String email) {
		waitForControl(driver, Interfaces.UserLoginsPage.EMAIL_TEXTBOX, timeWait);
		if (!email.equals("")) {
			type(driver, Interfaces.UserLoginsPage.EMAIL_TEXTBOX, email);
			sleep(1);
		}
	}

	/**
	 * select Default Dashboard
	 */
	public void selectDefaultDashboard(String dashboard) {
		waitForControl(driver, Interfaces.UserLoginsPage.DEFAULT_DASHBOARD_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.UserLoginsPage.DEFAULT_DASHBOARD_COMBOBOX, dashboard);
		sleep(1);
	}

	/**
	 * click Save User Detail Button
	 */
	public void clickSaveUserDetailButton() {
		waitForControl(driver, Interfaces.UserLoginsPage.SAVE_USER_DETAIL_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		sleep();
	}

	/**
	 * click List User Button
	 */
	public void clickListUserButton() {
		waitForControl(driver, Interfaces.UserLoginsPage.LIST_USER_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('ListUsers').click();");
		sleep();
	}

	/**
	 * search User by Id
	 */
	public void searchUserbyId(String userId) {
		waitForControl(driver, Interfaces.UserLoginsPage.LOGIN_ID_TEXTBOX, timeWait);
		type(driver, Interfaces.UserLoginsPage.LOGIN_ID_TEXTBOX, userId);
		sleep(1);
		clickSearchUserButton();
	}

	/**
	 * click Search User button
	 */
	public void clickSearchUserButton() {
		waitForControl(driver, Interfaces.UserLoginsPage.SEARCH_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep();
	}

	/**
	 * check User Display
	 */
	public boolean isUserDisplay(String userId) {
		waitForControl(driver, Interfaces.UserLoginsPage.DYNAMIC_USER_LOGIN_ID, userId, timeWait);
		return isControlDisplayed(driver, Interfaces.UserLoginsPage.DYNAMIC_USER_LOGIN_ID, userId);
	}

	/**
	 * check User Info Display Correctly
	 */
	public boolean isUserInfoDisplayCorrectly(String loginId, String firstName, String lastName, String userRole) {
		waitForControl(driver, Interfaces.UserLoginsPage.DYNAMIC_USER_LOGIN_ID, loginId, timeWait);
		boolean firstNameCorrect = isControlDisplayed(driver, Interfaces.UserLoginsPage.DYNAMIC_USER_NAME, loginId, firstName);
		boolean lastNameCorrect = isControlDisplayed(driver, Interfaces.UserLoginsPage.DYNAMIC_USER_NAME, loginId, lastName);
		boolean userRoleCorrect = isControlDisplayed(driver, Interfaces.UserLoginsPage.DYNAMIC_USER_ROLE, loginId, userRole);
		return firstNameCorrect && lastNameCorrect && userRoleCorrect;
	}

	/**
	 * open User Detail Page
	 */
	public void openUserDetailPage(String userId) {
		waitForControl(driver, Interfaces.UserLoginsPage.DYNAMIC_USER_LOGIN_ID, userId, timeWait);
		click(driver, Interfaces.UserLoginsPage.DYNAMIC_USER_OPEN_DETAIL_LINK, userId);
		sleep();
	}

	/**
	 * reset Password
	 */
	public void resetPassword(String newPassword) {
		waitForControl(driver, Interfaces.UserLoginsPage.RESET_PASSWORD_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('ResetPassword').click();");
		sleep();
		waitForControl(driver, Interfaces.UserLoginsPage.NEW_PASSWORD_TEXTBOX, timeWait);
		type(driver, Interfaces.UserLoginsPage.NEW_PASSWORD_TEXTBOX, newPassword);
		type(driver, Interfaces.UserLoginsPage.RETYPE_NEW_PASSWORD_TEXTBOX, newPassword);
		clickSaveUserDetailButton();
	}

	/**
	 * check Last Name User Detail Display
	 */
	public boolean isLastNameUserDetailDisplay(String userName) {
		waitForControl(driver, Interfaces.UserLoginsPage.LAST_NAME_USER_DETAIL_TEXTBOX, timeWait);
		String realName = getAttributeValue(driver, Interfaces.UserLoginsPage.LAST_NAME_USER_DETAIL_TEXTBOX, "value");
		return realName.contains(userName);
	}

	/**
	 * check Email User Detail Display
	 */
	public boolean isEmailUserDetailDisplay(String email) {
		waitForControl(driver, Interfaces.UserLoginsPage.EMAIL_USER_DETAIL_TEXTBOX, timeWait);
		String realEmail = getAttributeValue(driver, Interfaces.UserLoginsPage.EMAIL_USER_DETAIL_TEXTBOX, "value");
		return realEmail.contains(email);
	}

	/**
	 * select Loan Application
	 */
	public void selectLoanApplication(String loanApplication) {
		waitForControl(driver, Interfaces.UserLoginsPage.LOAN_APPLICATION_COMBOBOX, timeWait);
		if (!loanApplication.equals("")) {
			selectItemCombobox(driver, Interfaces.UserLoginsPage.LOAN_APPLICATION_COMBOBOX, loanApplication);
		}
		sleep(1);
	}

	/**
	 * check Error Message With Content Display
	 */
	public boolean isErrorMessageWithContentDisplay(String message) {
		waitForControl(driver, Interfaces.UserLoginsPage.DYNAMIC_USER_DETAIL_ERROR_MESSAGE, "please make sure to save record before you close this page", 5);
		if (isControlDisplayed(driver, Interfaces.UserLoginsPage.DYNAMIC_USER_DETAIL_ERROR_MESSAGE, "please make sure to save record before you close this page")) {
			clickSaveUserDetailButton();
		}
		waitForControl(driver, Interfaces.UserLoginsPage.DYNAMIC_USER_DETAIL_ERROR_MESSAGE, message, timeWait);
		return isControlDisplayed(driver, Interfaces.UserLoginsPage.DYNAMIC_USER_DETAIL_ERROR_MESSAGE, message);
	}

	/**
	 * check Error Message For Last Name Display
	 */
	public boolean isErrorMessageForLastNameDisplay(String message) {
		waitForControl(driver, Interfaces.UserLoginsPage.DYNAMIC_ERROR_MESSAGE_LASTNAME, message, timeWait);
		return isControlDisplayed(driver, Interfaces.UserLoginsPage.DYNAMIC_ERROR_MESSAGE_LASTNAME, message);
	}

	/**
	 * check Error Message For Login Id Display
	 */
	public boolean isErrorMessageForLoginIdDisplay(String message) {
		waitForControl(driver, Interfaces.UserLoginsPage.DYNAMIC_ERROR_MESSAGE_LOGINID, message, timeWait);
		return isControlDisplayed(driver, Interfaces.UserLoginsPage.DYNAMIC_ERROR_MESSAGE_LOGINID, message);
	}

	/**
	 * check Error Message For Role Display
	 */
	public boolean isErrorMessageForRoleDisplay(String message) {
		waitForControl(driver, Interfaces.UserLoginsPage.DYNAMIC_ERROR_MESSAGE_ROLE, message, timeWait);
		return isControlDisplayed(driver, Interfaces.UserLoginsPage.DYNAMIC_ERROR_MESSAGE_ROLE, message);
	}

	/**
	 * check Error Message For Password Display
	 */
	public boolean isErrorMessageForPasswordDisplay(String message) {
		waitForControl(driver, Interfaces.UserLoginsPage.DYNAMIC_ERROR_MESSAGE_PASSWORD, message, timeWait);
		return isControlDisplayed(driver, Interfaces.UserLoginsPage.DYNAMIC_ERROR_MESSAGE_PASSWORD, message);
	}

	/**
	 * check Error Message For Retype Password Display
	 */
	public boolean isErrorMessageForRetypePasswordDisplay(String message) {
		waitForControl(driver, Interfaces.UserLoginsPage.DYNAMIC_ERROR_MESSAGE_RETYPE_PASSWORD, message, timeWait);
		return isControlDisplayed(driver, Interfaces.UserLoginsPage.DYNAMIC_ERROR_MESSAGE_RETYPE_PASSWORD, message);
	}

	/**
	 * check Create New User Success Message Display
	 */
	public boolean isCreateNewUserSuccessMessageDisplay() {
		waitForControl(driver, Interfaces.UserLoginsPage.DYNAMIC_USER_DETAIL_ERROR_MESSAGE, "please make sure to save record before you close this page", 5);
		if (isControlDisplayed(driver, Interfaces.UserLoginsPage.DYNAMIC_USER_DETAIL_ERROR_MESSAGE, "please make sure to save record before you close this page")) {
			clickSaveUserDetailButton();
		}
		waitForControl(driver, Interfaces.UserLoginsPage.SAVED_USER_DETAIL_MESSAGE, timeWait);
		return isControlDisplayed(driver, Interfaces.UserLoginsPage.SAVED_USER_DETAIL_MESSAGE);
	}

	/**
	 * check Error Message Reset Password With Content Display
	 */
	public boolean isErrorMessageResetPasswordWithContentDisplay(String message) {
		waitForControl(driver, Interfaces.UserLoginsPage.DYNAMIC_ERROR_MESSAGE_RESET_PASSWORD, message, timeWait);
		return isControlDisplayed(driver, Interfaces.UserLoginsPage.DYNAMIC_ERROR_MESSAGE_RESET_PASSWORD, message);
	}

	/**
	 * check Error Message Reset Password For Password Display
	 */
	public boolean isErrorMessageResetPasswordForPasswordDisplay(String message) {
		waitForControl(driver, Interfaces.UserLoginsPage.DYNAMIC_ERROR_MESSAGE_RESET_PASSWORD_NEW, message, timeWait);
		return isControlDisplayed(driver, Interfaces.UserLoginsPage.DYNAMIC_ERROR_MESSAGE_RESET_PASSWORD_NEW, message);
	}

	/**
	 * check Error Message Reset Password For Retype Password Display
	 */
	public boolean isErrorMessageResetPasswordForRetypePasswordDisplay(String message) {
		waitForControl(driver, Interfaces.UserLoginsPage.DYNAMIC_ERROR_MESSAGE_RESET_PASSWORD_RETYPE, message, timeWait);
		return isControlDisplayed(driver, Interfaces.UserLoginsPage.DYNAMIC_ERROR_MESSAGE_RESET_PASSWORD_RETYPE, message);
	}

	/**
	 * input New Password For Reset Password
	 */
	public void inputNewPasswordForResetPassword(String password) {
		waitForControl(driver, Interfaces.UserLoginsPage.NEW_PASSWORD_TEXTBOX, timeWait);
		type(driver, Interfaces.UserLoginsPage.NEW_PASSWORD_TEXTBOX, password);
		sleep(1);
	}

	/**
	 * input Retype New Password For Reset Password
	 */
	public void inputRetypeNewPasswordForResetPassword(String password) {
		waitForControl(driver, Interfaces.UserLoginsPage.RETYPE_NEW_PASSWORD_TEXTBOX, timeWait);
		type(driver, Interfaces.UserLoginsPage.RETYPE_NEW_PASSWORD_TEXTBOX, password);
		sleep(1);
	}

	/**
	 * check Reset Password Button Display
	 */
	public boolean isResetPasswordButtonDisplay() {
		waitForControl(driver, Interfaces.UserLoginsPage.RESET_PASSWORD_BUTTON, timeWait);
		return isControlDisplayed(driver, Interfaces.UserLoginsPage.RESET_PASSWORD_BUTTON);
	}

	/**
	 * select Loan Application for search user
	 */
	public void selectLoanApplicationForSearchUser(String loanApplication) {
		waitForControl(driver, Interfaces.UserLoginsPage.LOAN_APPLICATION_SEARCH_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.UserLoginsPage.LOAN_APPLICATION_SEARCH_COMBOBOX, loanApplication);
		sleep(1);
	}

	/**
	 * check All User For Loan Application Display
	 */
	public boolean isAllUserForLoanApplicationDisplay(String loanApplication) {
		boolean returnValue = true;
		waitForControl(driver, Interfaces.UserLoginsPage.LOAN_APPLICATION_OF_ALL_USERS, timeWait);
		sleep();
		int numberofElement = countElement(driver, Interfaces.UserLoginsPage.LOAN_APPLICATION_OF_ALL_USERS);
		if (numberofElement == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver, Interfaces.UserLoginsPage.LOAN_APPLICATION_OF_ALL_USERS);
		for (WebElement element : elements) {
			String text = element.getText();
			if (!text.contains(loanApplication)) {
				returnValue = false;
				break;
			}
		}
		return returnValue;
	}

	/**
	 * enter Last name For Search User
	 */
	public void enterLastnameForSearchUser(String lastName) {
		waitForControl(driver, Interfaces.UserLoginsPage.LAST_NAME_SEARCH_TEXTBOX, timeWait);
		type(driver, Interfaces.UserLoginsPage.LAST_NAME_SEARCH_TEXTBOX, lastName);
		sleep(1);
	}

	/**
	 * check All User For Last Name Display
	 */
	public boolean isAllUserForLastNameDisplay(String lastName) {
		boolean returnValue = true;
		waitForControl(driver, Interfaces.UserLoginsPage.NAME_OF_ALL_USERS, timeWait);
		sleep();
		int numberofElement = countElement(driver, Interfaces.UserLoginsPage.NAME_OF_ALL_USERS);
		if (numberofElement == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver, Interfaces.UserLoginsPage.NAME_OF_ALL_USERS);
		for (WebElement element : elements) {
			String text = element.getText();
			if (!text.toLowerCase().contains(lastName.toLowerCase())) {
				returnValue = false;
				break;
			}
		}
		return returnValue;
	}

	/**
	 * enter First name For Search User
	 */
	public void enterFirstnameForSearchUser(String firstName) {
		waitForControl(driver, Interfaces.UserLoginsPage.FIRST_NAME_SEARCH_TEXTBOX, timeWait);
		type(driver, Interfaces.UserLoginsPage.FIRST_NAME_SEARCH_TEXTBOX, firstName);
		sleep(1);
	}

	/**
	 * check All User For First Name Display
	 */
	public boolean isAllUserForFirstNameDisplay(String firstName) {
		boolean returnValue = true;
		waitForControl(driver, Interfaces.UserLoginsPage.NAME_OF_ALL_USERS, timeWait);
		sleep();
		int numberofElement = countElement(driver, Interfaces.UserLoginsPage.NAME_OF_ALL_USERS);
		if (numberofElement == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver, Interfaces.UserLoginsPage.NAME_OF_ALL_USERS);
		for (WebElement element : elements) {
			String text = element.getText();
			if (!text.toLowerCase().contains(firstName.toLowerCase())) {
				returnValue = false;
				break;
			}
		}
		return returnValue;
	}

	/**
	 * enter Login Id For Search User
	 */
	public void enterLoginIdForSearchUser(String loginId) {
		waitForControl(driver, Interfaces.UserLoginsPage.LOGIN_ID_SEARCH_TEXTBOX, timeWait);
		type(driver, Interfaces.UserLoginsPage.LOGIN_ID_SEARCH_TEXTBOX, loginId);
		sleep(1);
	}

	/**
	 * check All User For Login Id Display
	 */
	public boolean isAllUserForLoginIdDisplay(String loginId) {
		boolean returnValue = true;
		waitForControl(driver, Interfaces.UserLoginsPage.LOGIN_ID_OF_ALL_USERS, timeWait);
		sleep();
		int numberofElement = countElement(driver, Interfaces.UserLoginsPage.LOGIN_ID_OF_ALL_USERS);
		if (numberofElement == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver, Interfaces.UserLoginsPage.LOGIN_ID_OF_ALL_USERS);
		for (WebElement element : elements) {
			String text = element.getText();
			if (!text.toLowerCase().contains(loginId.toLowerCase())) {
				returnValue = false;
				break;
			}
		}
		return returnValue;
	}

	/**
	 * select Role For Search User
	 */
	public void selectRoleForSearchUser(String role) {
		waitForControl(driver, Interfaces.UserLoginsPage.ROLE_SEARCH_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.UserLoginsPage.ROLE_SEARCH_COMBOBOX, role);
		sleep(1);
	}

	/**
	 * check All User For Role Display
	 */
	public boolean isAllUserForRoleDisplay(String role) {
		boolean returnValue = true;
		waitForControl(driver, Interfaces.UserLoginsPage.ROLE_OF_ALL_USERS, timeWait);
		sleep();
		int numberofElement = countElement(driver, Interfaces.UserLoginsPage.ROLE_OF_ALL_USERS);
		if (numberofElement == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver, Interfaces.UserLoginsPage.ROLE_OF_ALL_USERS);
		for (WebElement element : elements) {
			String text = element.getText();
			if (!text.contains(role)) {
				returnValue = false;
				break;
			}
		}
		return returnValue;
	}

	/**
	 * select Yes Active Status For Search User
	 */
	public void selectYesActiveStatusForSearchUser() {
		waitForControl(driver, Interfaces.UserLoginsPage.YES_ACTIVE_STATUS_RADIOBUTTON, timeWait);
		click(driver, Interfaces.UserLoginsPage.YES_ACTIVE_STATUS_RADIOBUTTON);
		sleep(1);
	}

	/**
	 * check All User For Active Status Display
	 */
	public boolean isAllUserForActiveStatusDisplay(String status) {
		boolean returnValue = true;
		waitForControl(driver, Interfaces.UserLoginsPage.ACTIVE_STATUS_OF_ALL_USERS, timeWait);
		sleep();
		int numberofElement = countElement(driver, Interfaces.UserLoginsPage.ACTIVE_STATUS_OF_ALL_USERS);
		if (numberofElement == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver, Interfaces.UserLoginsPage.ACTIVE_STATUS_OF_ALL_USERS);
		for (WebElement element : elements) {
			String text = element.getText();
			if (!text.contains(status)) {
				returnValue = false;
				break;
			}
		}
		return returnValue;
	}

	/**
	 * select No Active Status For Search User
	 */
	public void selectNoActiveStatusForSearchUser() {
		waitForControl(driver, Interfaces.UserLoginsPage.NO_ACTIVE_STATUS_RADIOBUTTON, timeWait);
		click(driver, Interfaces.UserLoginsPage.NO_ACTIVE_STATUS_RADIOBUTTON);
		sleep(1);
	}

	/**
	 * select Yes Consolidation Status For Search User
	 */
	public void selectYesConsolidationStatusForSearchUser() {
		waitForControl(driver, Interfaces.UserLoginsPage.YES_CONSOLIDATION_STATUS_RADIOBUTTON, timeWait);
		click(driver, Interfaces.UserLoginsPage.YES_CONSOLIDATION_STATUS_RADIOBUTTON);
		sleep(1);
	}

	/**
	 * check All User For Consolidation Status Display
	 */
	public boolean isAllUserForConsolidationStatusDisplay(String status) {
		boolean returnValue = true;
		waitForControl(driver, Interfaces.UserLoginsPage.CONSOLIDATION_STATUS_OF_ALL_USERS, timeWait);
		sleep();
		int numberofElement = countElement(driver, Interfaces.UserLoginsPage.CONSOLIDATION_STATUS_OF_ALL_USERS);
		if (numberofElement == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver, Interfaces.UserLoginsPage.CONSOLIDATION_STATUS_OF_ALL_USERS);
		for (WebElement element : elements) {
			String text = element.getText();
			if (!text.contains(status)) {
				returnValue = false;
				break;
			}
		}
		return returnValue;
	}

	/**
	 * select No Consolidation Status For Search User
	 */
	public void selectNoConsolidationStatusForSearchUser() {
		waitForControl(driver, Interfaces.UserLoginsPage.NO_CONSOLIDATION_STATUS_RADIOBUTTON, timeWait);
		click(driver, Interfaces.UserLoginsPage.NO_CONSOLIDATION_STATUS_RADIOBUTTON);
		sleep(1);
	}

	/**
	 * check Consolidate Checkbox
	 */
	public void checkConsolidateCheckbox() {
		waitForControl(driver, Interfaces.UserLoginsPage.CONSOLIDATION_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.UserLoginsPage.CONSOLIDATION_CHECKBOX);
		sleep(1);
	}

	private WebDriver driver;
}