package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.MailHomePage;
import page.MailLoginPage;
import page.PageFactory;
import page.PropertiesPage;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_027_CAF_DownloadAndCheckExistFile extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_CAF;
		applicantName = "Udi Dorner";
		zipExtension = "CheckingDownloadTemplateCAF.zip";
		excelExtension = ".xls";
		excelTemplate = "CAFDataTapeTemplate.xlsx";
		excelMissingSummary = "MissingDocumentsSummary.xlsx";
		propertiesFileName = "CAF Required.xlsx";
		address1 = "504 W Euclid";
		documentType1 = "Evidence of Insurance";
		documentType2 = "HUD-1/Closing Statement";
		documentType3 = "Current Title Policy";
		documentFileName = "datatest.pdf";
		zipDownload = ".zip";
		mailPageUrl = "https://accounts.google.com";
		usernameEmail = "minhdam06@gmail.com";
		passwordEmail = "dam123!@#!@#";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}

	@Test(groups = { "regression" }, description = "Download 01 - Export CSV Dashboad list Loan items")
	public void Download_01_ExportCSVDashboard() {
		log.info("Download_01 - Step 01. Login with lender");
		preciseRESUrl = loginPage.getCurrentUrl(driver);
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Download_01 - Step 02. Click on logged user name on the top right corner");
		homePage.clickOnLoggedUserName(driver);

		log.info("Download_01 - Step 03. Click on Settings title");
		homePage.clickOnSettingsTitle();

		log.info("Download_01 - Step 04. Input Email");
		homePage.typeEmail(usernameEmail);

		log.info("Download_01 - Step 05. Click Save button");
		homePage.clickSaveButton();

		log.info("Download_01- Step 06. Open Homepage tab");
		homePage.openHomePage(driver, ipClient);

		log.info("Download_01 - Step 07. Click CSV button");
		homePage.deleteAllFileInFolder();
		homePage.clickOnExportCSVButton();

		log.info("Download_01 - Step 08. Wait for file downloaded complete");
		homePage.waitForDownloadFileFullnameCompleted(excelExtension);

		log.info("VP: File number in folder equal 1");
		countFile = homePage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("Download_01 - Step 09. Delete the downloaded file");
		homePage.deleteContainsFileName(excelExtension);
	}

	@Test(groups = { "regression" }, description = "Download 02 - Export U/W Pipeline Dashboard")
	public void Download_02_ExportUWPipelineDashboard() {
		log.info("Download_02 - Step 01. Click U/W Pipeline button");
		homePage.deleteAllFileInFolder();
		homePage.clickOnUWPipelineButton();

		log.info("Download_02 - Step 02. Wait for file downloaded complete");
		homePage.waitForDownloadFileFullnameCompleted(excelExtension);

		log.info("VP: File number in folder equal 1");
		countFile = homePage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("Download_02 - Step 03. Delete the downloaded file");
		homePage.deleteContainsFileName(excelExtension);
	}

	@Test(groups = { "regression" }, description = "Download 03 - Export datatape (Export all Properties)")
	public void Download_03_ExportDataTapeAllProperties() {
		log.info("Precondition 01. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 02. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus);

		log.info("Precondition 03. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Precondition 04. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("Precondition 05. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("Precondition 06. Add document 1 by clicking the placeholder");
		propertiesPage.uploadDynamicDocumentFileByPlaceHolder(documentType1, documentFileName);

		log.info("Precondition 07. Add document 2 by clicking the placeholder");
		propertiesPage.uploadDynamicDocumentFileByPlaceHolder(documentType2, documentFileName);

		log.info("Precondition 08. Add document 3 by clicking the placeholder");
		propertiesPage.uploadDynamicDocumentFileByPlaceHolder(documentType3, documentFileName);

		log.info("Precondition 09. Click 'Save' button");
		propertiesPage.clickSaveButton();

		log.info("VP: Document 1 is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, documentFileName));

		log.info("VP: Document 2 is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType2, documentFileName));

		log.info("VP: Document 3 is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType3, documentFileName));

		log.info("Precondition 10. Click 'Property List' button");
		loansPage = propertiesPage.clickOnPropertyListButton();

		log.info("Download_03 - Step 01. Click 'Export datatape' button");
		loansPage.deleteAllFileInFolder();
		loansPage.clickExportDatatapeButton();

		log.info("Download_03 - Step 02. Click 'Export all Properties' radio button");
		loansPage.clickExportAllPropertiesRadioButton("1");

		log.info("Download_03 - Step 03. Wait for file downloaded complete");
		loansPage.waitForDownloadFileContainsNameCompleted(excelExtension);

		log.info("VP: File number in folder equal 1");
		countFile = loansPage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("Download_03 - Step 04. Delete the downloaded file");
		loansPage.deleteContainsFileName(excelExtension);
	}

	@Test(groups = { "regression" }, description = "Download 04 - Export datatape (Don't export Parent Properties)")
	public void Download_04_ExportDataTapeDontParentProperties() {
		log.info("Download_04 - Step 01. Click 'Export datatape' button");
		loansPage.deleteAllFileInFolder();
		loansPage.clickExportDatatapeButton();

		log.info("Download_04 - Step 02. Click 'Don't export Parent Properties' radio button");
		loansPage.clickExportAllPropertiesRadioButton("2");

		log.info("Download_04 - Step 03. Wait for file downloaded complete");
		loansPage.waitForDownloadFileContainsNameCompleted(excelExtension);

		log.info("VP: File number in folder equal 1");
		countFile = loansPage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("Download_04 - Step 04. Delete the downloaded file");
		loansPage.deleteContainsFileName(excelExtension);
	}

	@Test(groups = { "regression" }, description = "Download 05 - Export datatape (Don't Export Sub-units)")
	public void Download_05_ExportDataTapeDontSubUnit() {
		log.info("Download_05 - Step 01. Click 'Export datatape' button");
		loansPage.deleteAllFileInFolder();
		loansPage.clickExportDatatapeButton();

		log.info("Download_05 - Step 02. Click 'Don't Export Sub-units' radio button");
		loansPage.clickExportAllPropertiesRadioButton("3");

		log.info("Download_05 - Step 03. Wait for file downloaded complete");
		loansPage.waitForDownloadFileContainsNameCompleted(excelExtension);

		log.info("VP: File number in folder equal 1");
		countFile = loansPage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("Download_05 - Step 04. Delete the downloaded file");
		loansPage.deleteContainsFileName(excelExtension);
	}

	@Test(groups = { "regression" }, description = "Download 06 - Download template")
	public void Download_06_DownloadTemplate() {
		log.info("Download_06 - Step 01. Click 'Download Template' button");
		loansPage.deleteAllFileInFolder();
		loansPage.clickDownloadTemplateButton();

		log.info("Download_06 - Step 02. Wait for file downloaded complete");
		loansPage.waitForDownloadFileFullnameCompleted(excelTemplate);

		log.info("VP: File number in folder equal 1");
		countFile = loansPage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("Download_06 - Step 03. Delete the downloaded file");
		loansPage.deleteFileFullName(excelTemplate);
	}

	@Test(groups = { "regression" }, description = "Download 07 - Missing Summary")
	public void Download_07_DownloadMissingSummary() {
		log.info("Download_07 - Step 01. Click 'Missing Summary' button");
		loansPage.deleteAllFileInFolder();
		loansPage.clickMissingSummaryButton();

		log.info("Download_07 - Step 02. Wait for file downloaded complete");
		loansPage.waitForDownloadFileFullnameCompleted(excelMissingSummary);

		log.info("VP: File number in folder equal 1");
		countFile = loansPage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("Download_07 - Step 03. Delete the downloaded file");
		loansPage.deleteFileFullName(excelMissingSummary);
	}

	@Test(groups = { "regression" }, description = "Download 08 - By Document Type")
	public void Download_08_DownloadByDocumentType() {
		log.info("Download_08 - Step 01. Open Property Documents tab");
		loansPage.openPropertyDocumentsTab();

		log.info("VP: Document 1 is loaded to document type in Property Documents tab");
		verifyTrue(loansPage.isDocumentLoadedToDocumentType(documentType1));

		log.info("VP: Document 2 is loaded to document type in Property Documents tab");
		verifyTrue(loansPage.isDocumentLoadedToDocumentType(documentType2));

		log.info("VP: Document 3 is loaded to document type in Property Documents tab");
		verifyTrue(loansPage.isDocumentLoadedToDocumentType(documentType3));

		log.info("Download_08 - Step 02. Click 'Download Documents' button");
		loansPage.deleteAllFileInFolder();
		loansPage.clickDownloadDocumentsButton();

		log.info("Download_08 - Step 03. Click 'By Document Type' radio button");
		loansPage.clickDocumentTypeRadioButton();

		log.info("Download_08 - Step 04. Click 'Download' button");
		loansPage.clickDownloadButton();

		log.info("VP: A success message is displayed");
		verifyTrue(loansPage.isSuccessMessageDisplayed("Complete"));

		log.info("Download_08 - Step 05. Wait for file downloaded complete");
		loansPage.waitForDownloadFileFullnameCompleted(zipExtension);

		log.info("VP: File number in folder equal 1");
		countFile = loansPage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("Download_08 - Step 06. Delete the downloaded file");
		loansPage.deleteFileFullName(zipExtension);
	}

	@Test(groups = { "regression" }, description = "Download 09 - Uploader - Missing Documents Summary")
	public void Download_09_DownloadMissingDocumentsSummaryOnUploader() {
		log.info("Download_09  - Step 01. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);

		log.info("Download_09 - Step 02. Login to Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameLender, passwordLender);

		log.info("Download_09 - Step 03. Select loans item");
		uploaderPage.clickChangeGroupedView();
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);

		log.info("Download_09 - Step 04.  Click Missing Documents Summary button");
		uploaderPage.deleteAllFileInFolder();
		uploaderPage.clickMissingDocumentsSummaryButton();

		log.info("Download_09 - Step 05. Wait for file downloaded complete");
		uploaderPage.waitForDownloadFileFullnameCompleted(excelMissingSummary);

		log.info("VP: File number in folder equal 1");
		countFile = uploaderPage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("Download_09 - Step 06. Delete the downloaded file");
		uploaderPage.deleteFileFullName(excelMissingSummary);
	}

	@Test(groups = { "regression" }, description = "Download 10 - Uploader - Download Documents")
	public void Download_10_DownloadDocumentsOnUploader() {
		log.info("Download_10 - Step 01.  Click Download Documents button");
		uploaderPage.clickDownloadDocumentsButton();

		log.info("VP: Document 1 is loaded to document download");
		verifyTrue(uploaderPage.isDocumentTypeDisplaysInDownloadDocuments(documentType1));

		log.info("VP: Document 2 is loaded to document download");
		verifyTrue(uploaderPage.isDocumentTypeDisplaysInDownloadDocuments(documentType2));

		log.info("VP: Document 3 is loaded to document download");
		verifyTrue(uploaderPage.isDocumentTypeDisplaysInDownloadDocuments(documentType3));

		log.info("Download_10 - Step 02.  Click OK button");
		uploaderPage.clickOkButtonOnDownloadDocument();

		log.info("VP: 'The system started generating a zip file with the requested documents.' message display");
		verifyTrue(uploaderPage.isDownloadGeneratingMessageDisplays());

		log.info("Download_10 - Step 03.  Click OK button");
		uploaderPage.clickOkButtonOnDownloadDocument();

		log.info("Download_10 - Step 04. Open Email url");
		mailLoginPage = uploaderPage.openMailLink(driver, mailPageUrl);
		mailLoginPage.loginToGmail(usernameEmail, passwordEmail);

		log.info("Download_10 - Step 05. Click Submit button");
		mailHomePage = mailLoginPage.clickSubmitButton(driver, ipClient);

		log.info("Download_10 - Step 06. Click 'Google Apps' title");
		mailHomePage.clickGoogleAppsTitle();

		log.info("Download_10 - Step 07. Click 'Gmail' application");
		mailHomePage.clickGmailApplication();

		log.info("VP. Gmail home page is displayed");
		verifyTrue(mailHomePage.isGmailHomePageDisplay());

		log.info("Download_10 - Step 08. Click 'Inbox' title");
		mailHomePage.clickInboxTitle();

		log.info("Download_10 - Step 09. Click 'Subject first email'");
		mailHomePage.clickSubjectFirstEmail();

		log.info("VP. Link hyperlink download is displayed");
		verifyTrue(mailHomePage.isLinkHyperlinkDisplay());

		log.info("Download_10 - Step 10. Click 'Link' hyperlink");
		mailHomePage.deleteAllFileInFolder();
		mailHomePage.clickLinkHyperlink();

		log.info("Download_10 - Step 11. Wait for file downloaded complete");
		mailHomePage.waitForDownloadFileContainsNameCompleted(zipDownload);

		log.info("VP: File number in folder equal 1");
		countFile = mailHomePage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("Download_10 - Step 12. Delete the downloaded file");
		mailHomePage.deleteContainsFileName(zipDownload);

		log.info("Download_10 - Step 13. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("Download_10 - Step 14. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		while(mailHomePage.isLinkHyperlinkDisplay()==true){
			mailHomePage.clickDataTooltipMoreDropdown();
			mailHomePage.clickDeleteThisMessageTitle();
		}
		
		mailHomePage.openLink(driver, "https://mail.google.com/mail/logout?hl=en");
	}

	@Test(groups = { "regression" }, description = "Download 11 - Download All Documents")
	public void Download_11_DownloadAllDocuments() {
		
		log.info("Download_11 - Step 01. Open PreciseRES webapp");
		loginPage = mailHomePage.openPreciseResUrlByBorrower(driver, ipClient, preciseRESUrl);
		
		log.info("Download_11 - Step 02. Login with Lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("Download_11 - Step 03. Open Loan tab");
		loansPage.openLoansPage(driver, ipClient);
		
		log.info("Download_11 - Step 04. Open Loan detail");
		loansPage.searchLoanByName(loanName);
		loansPage.openLoansDetailPage(loanName);

		log.info("Download_11 - Step 05. Click 'Download All Document' button");
		loansPage.clickDownloadAllDocumentsButton();

		log.info("Download_11 - Step 06. Open Email url");
		mailLoginPage = loansPage.openMailLink(driver, mailPageUrl);

		log.info("Download_11 - Step 07. Input Usename and Password");
		mailLoginPage.loginToGmail(usernameEmail, passwordEmail);

		log.info("Download_11 - Step 08. Click Submit button");
		mailHomePage = mailLoginPage.clickSubmitButton(driver, ipClient);

		log.info("Download_11 - Step 09. Click 'Google Apps' title");
		mailHomePage.clickGoogleAppsTitle();

		log.info("Download_11 - Step 10. Click 'Gmail' application");
		mailHomePage.clickGmailApplication();

		log.info("VP. Gmail home page is displayed");
		verifyTrue(mailHomePage.isGmailHomePageDisplay());

		log.info("Download_11 - Step 11. Click 'Inbox' title");
		mailHomePage.clickInboxTitle();

		log.info("Download_11 - Step 12. Click 'Subject first email'");
		mailHomePage.clickSubjectFirstEmail();

		log.info("VP. Link hyperlink download is displayed");
		verifyTrue(mailHomePage.isLinkHyperlinkDisplay());

		log.info("Download_11 - Step 13. Click 'Link' hyperlink");
		mailHomePage.deleteAllFileInFolder();
		mailHomePage.clickLinkHyperlink();

		log.info("Download_11 - Step 14. Wait for file downloaded complete");
		mailHomePage.waitForDownloadFileContainsNameCompleted(zipDownload);

		log.info("VP: File number in folder equal 1");
		countFile = mailHomePage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("Download_11 - Step 15. Delete the downloaded file");
		mailHomePage.deleteContainsFileName(zipDownload);

		log.info("Download_11 - Step 16. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("Download_11 - Step 17. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		while(mailHomePage.isLinkHyperlinkDisplay()==true){
			mailHomePage.clickDataTooltipMoreDropdown();
			mailHomePage.clickDeleteThisMessageTitle();
		}
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private MailLoginPage mailLoginPage;
	private HomePage homePage;
	private MailHomePage mailHomePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private String excelExtension, zipExtension, zipDownload, excelTemplate, excelMissingSummary;
	private String usernameLender, passwordLender, address1, documentFileName;
	private String loanName, accountNumber, applicantName, propertiesFileName, loanStatus;
	private int countFile;
	private String documentType1, documentType2, documentType3, mailPageUrl;
	private String usernameEmail, passwordEmail, uploaderPageUrl, preciseRESUrl;
}