package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Constant;
import page.FNFBorrowersPage;
import page.FNFDocumentPage;
import page.FNFLoansPage;
import page.HomePage;
import page.LoginPage;
import page.MailHomePage;
import page.MailLoginPage;
import page.PageFactory;

public class sanityTest_027_FNF_SendInformationByEmail extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		mailPageUrl = "https://accounts.google.com";
		usernameEmail = "minhdam06@gmail.com";
		passwordEmail = "dam123!@#!@#";
		invalidEmail = "abc!@#@gmail.abc";
		emailSubject = "precise";
		emailMessage = "pres information";
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		status = "Under Review";
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
	}

	@Test(groups = { "regression" }, description = "Send Information 01 - Send information by email at Borrower page")
	public void SendInformation_01_Lender_BorrowersPage() {

		log.info("SendInformation_01 - Step 01. Logout and login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("SendInformation_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
		preciseRESHomePageUrl = loginPage.getCurrentUrl(driver);
		
		log.info("SendInformation_01 - Step 03. Get first Borrower name");
		emailContent = applicantsPage.getCellContentByCellID(driver, "DataCell FieldTypeVarchar datacell_BorrowerId");
		
		log.info("SendInformation_01 - Step 04. Click on Send this information to Email button");
		applicantsPage.clickSendInformationToEmail(driver);
		
		log.info("SendInformation_01 - Step 05. Click on Send this information to Email button");
		verifyTrue(applicantsPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_01 - Step 06. Open Email url");
		mailLoginPage = applicantsPage.openMailLink(driver, mailPageUrl);
		mailLoginPage.loginToGmail(usernameEmail, passwordEmail);

		log.info("SendInformation_01 - Step 07. Click Submit button");
		mailHomePage = mailLoginPage.clickSubmitButton(driver, ipClient);

		log.info("SendInformation_01 - Step 08. Click 'Google Apps' title");
		mailHomePage.clickGoogleAppsTitle();

		log.info("SendInformation_01 - Step 09. Click 'Gmail' application");
		mailHomePage.clickGmailApplication();

		log.info("VP. Gmail home page is displayed");
		verifyTrue(mailHomePage.isGmailHomePageDisplay());

		log.info("SendInformation_01 - Step 10. Click 'Inbox' title");
		mailHomePage.clickInboxTitle();

		log.info("SendInformation_01 - Step 11. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail("List of Borrowers");
		
		log.info("SendInformation_01 - Step 12. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_01 - Step 13. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_01 - Step 14. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent().trim(), emailContent.trim().toLowerCase());
		
		log.info("SendInformation_01 - Step 15. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_01 - Step 16. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		log.info("SendInformation_01 - Step 17. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESHomePageUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 02 - Send information by email at Loans page")
	public void SendInformation_02_Lender_LoansPage() {
	
		log.info("SendInformation_02 - Step 02: Go to 'Loans' page");
		loanPage = applicantsPage.openFNFLoansPage(driver, ipClient);
		preciseRESHomePageUrl = loanPage.getCurrentUrl(driver);
		
		log.info("SendInformation_02 - Step 03. Get first Loan name");
		emailContent = loanPage.getCellContentByCellID(driver, "DataCell FieldTypeHTML datacell_OpportunityName");
		
		log.info("SendInformation_02 - Step 04. Click on Send this information to Email button");
		loanPage.clickSendInformationToEmail(driver);
		
		log.info("SendInformation_02 - Step 05. Click on Send this information to Email button");
		verifyTrue(loanPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_04 - Step 05. Open Email url");
		mailLoginPage = loanPage.openMailLink(driver, "https://mail.google.com");

		log.info("SendInformation_02 - Step 11. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail("List of Loans");
		
		log.info("SendInformation_02 - Step 12. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_02 - Step 13. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_02 - Step 14. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent().trim(), emailContent.trim().toLowerCase());
		
		log.info("SendInformation_02 - Step 15. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_02 - Step 16. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		log.info("SendInformation_02 - Step 17. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESHomePageUrl);
	}
	
//
	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Loans Properties Documents page")
	public void SendInformation_03_Lender_LoansDocumentsTab() {
		
		log.info("SendInformation_03 - Step 01. Click Property Document Tab");
		documentsPage = loanPage.openDocumentsPageFNF(driver, ipClient);
		preciseRESHomePageUrl = documentsPage.getCurrentUrl(driver);
		
		log.info("SendInformation_03 - Step 02. Get first address");
		emailContent = documentsPage.getCellContentByCellID(driver, "DataCell FieldTypeHTML datacell_OpportunityName");
		
		log.info("SendInformation_03 - Step 03. Click on Send this information to Email button");
		loanPage.clickSendInformationToEmail(driver);
		
		log.info("SendInformation_03 - Step 04. Click on Send this information to Email button");
		verifyTrue(documentsPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_03 - Step 05. Open Email url");
		mailLoginPage = documentsPage.openMailLink(driver, "https://mail.google.com");

		log.info("SendInformation_03 - Step 06. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail("Loan Documents");
		
		log.info("SendInformation_03 - Step 07. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_03 - Step 08. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_03 - Step 09. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent.toLowerCase());
		
		log.info("SendInformation_03 - Step 10. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_03 - Step 11. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();

		log.info("SendInformation_03 - Step 12. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESHomePageUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Loans Property Document Tab")
	public void SendInformation_04_Lender_PropertyDocumentsTab() {
		
		log.info("SendInformation_04 - Step 01. Click Property Document Tab");
		documentsPage.openPropertyDocumentsTab();
		
		log.info("SendInformation_04 - Step 02: Input 'Document status'");
		documentsPage.selectDocumentStatus(status);
		
		log.info("SendInformation_04 - Step 03: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton();
		
		log.info("SendInformation_04 - Step 02. Get first address");
		emailContent = documentsPage.getCellContentByCellID(driver, "DataCell FieldTypeSQLServerIdentity datacell_Document");
		
		log.info("SendInformation_04 - Step 03. Click on Send this information to Email button");
		documentsPage.clickSendInformationToEmail(driver);
		
		log.info("SendInformation_04 - Step 04. Click on Send this information to Email button");
		verifyTrue(documentsPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_04 - Step 05. Open Email url");
		mailLoginPage = documentsPage.openMailLink(driver, "https://mail.google.com");

		log.info("SendInformation_04 - Step 06. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail("Properties Documents");
		
		log.info("SendInformation_04 - Step 07. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_04 - Step 08. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_04 - Step 09. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_04 - Step 10. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_04 - Step 11. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		log.info("SendInformation_04 - Step 12. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESHomePageUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 02 - Send information by email at Loans page")
	public void SendInformation_05_Borrower_LoansPage() {
		
		log.info("SendInformation_05 - Step 01. Logout and login with Borrower");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);
	
		log.info("SendInformation_05 - Step 02: Go to 'Loans' page");
		loanPage = applicantsPage.openFNFLoansPage(driver, ipClient);
		preciseRESHomePageUrl = loanPage.getCurrentUrl(driver);
		
		log.info("SendInformation_05 - Step 03. Get first Loan name");
		emailContent = loanPage.getCellContentByCellID(driver, "DataCell FieldTypeHTML datacell_OpportunityName");
		
		log.info("SendInformation_05 - Step 04. Click on Send this information to Email button");
		loanPage.clickSendInformationToEmail(driver);
		
		log.info("SendInformation_05 - Step 05. Click on Send this information to Email button");
		verifyTrue(loanPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_05 - Step 05. Open Email url");
		mailLoginPage = loanPage.openMailLink(driver, "https://mail.google.com");

		log.info("SendInformation_05 - Step 11. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail(companyName+" List of Loans");
		
		log.info("SendInformation_05 - Step 12. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_05 - Step 13. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_05 - Step 14. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent().trim(), emailContent.trim().toLowerCase());
		
		log.info("SendInformation_05 - Step 15. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_05 - Step 16. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		log.info("SendInformation_05 - Step 17. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESHomePageUrl);
	}
	

	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Loans Properties Documents page")
	public void SendInformation_06_Borrower_LoansDocumentsTab() {
		
		log.info("SendInformation_06 - Step 01. Click Property Document Tab");
		documentsPage = loanPage.openDocumentsPageFNF(driver, ipClient);
		preciseRESHomePageUrl = documentsPage.getCurrentUrl(driver);
		
		log.info("SendInformation_06 - Step 02. Get first address");
		emailContent = documentsPage.getCellContentByCellID(driver, "DataCell FieldTypeHTML datacell_OpportunityName");
		
		log.info("SendInformation_06 - Step 03. Click on Send this information to Email button");
		loanPage.clickSendInformationToEmail(driver);
		
		log.info("SendInformation_06 - Step 04. Click on Send this information to Email button");
		verifyTrue(documentsPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_06 - Step 05. Open Email url");
		mailLoginPage = documentsPage.openMailLink(driver, "https://mail.google.com");

		log.info("SendInformation_06 - Step 06. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail(companyName+" Loan Documents");
		
		log.info("SendInformation_06 - Step 07. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_06 - Step 08. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_06 - Step 09. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent.toLowerCase());
		
		log.info("SendInformation_06 - Step 10. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_06 - Step 11. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();

		log.info("SendInformation_06 - Step 12. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESHomePageUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Loans Property Document Tab")
	public void SendInformation_07_Borrower_PropertyDocumentsTab() {
		
		log.info("SendInformation_07 - Step 01. Click Property Document Tab");
		documentsPage.openPropertyDocumentsTab();
		
		log.info("SearchPropertyDocuments_07 - Step 02: Input 'Document status'");
		documentsPage.selectDocumentStatus("Under Review");
		
		log.info("SearchPropertyDocuments_07 - Step 03: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton();
		
		log.info("SendInformation_07 - Step 02. Get first address");
		emailContent = documentsPage.getCellContentByCellID(driver, "DataCell FieldTypeSQLServerIdentity datacell_Document");
		
		log.info("SendInformation_07 - Step 03. Click on Send this information to Email button");
		documentsPage.clickSendInformationToEmail(driver);
		
		log.info("SendInformation_07 - Step 04. Click on Send this information to Email button");
		verifyTrue(documentsPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_07 - Step 05. Open Email url");
		mailLoginPage = documentsPage.openMailLink(driver, "https://mail.google.com");

		log.info("SendInformation_07 - Step 06. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail(companyName+" Properties Documents");
		
		log.info("SendInformation_07 - Step 07. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_07 - Step 08. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_07 - Step 09. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_07 - Step 10. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_07 - Step 11. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
	}
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFDocumentPage documentsPage;
	private MailLoginPage mailLoginPage;
	private MailHomePage mailHomePage;
	private String preciseRESHomePageUrl;
	private String mailPageUrl, usernameEmail, passwordEmail;
	private String invalidEmail, emailSubject, emailMessage, emailContent;
	private FNFLoansPage loanPage;
	private FNFBorrowersPage applicantsPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername, FNFBorrowerPassword;
	private String status;
	private String companyName, accountNumber;
}