package sanityTest_PreCondition;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.AdminPage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_000_CheckAuditLoanB2R extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameAdmin = Constant.USERNAME_ADMIN;
		passwordAdmin = Constant.PASSWORD_ADMIN;
	}
	
	@Test(groups = { "regression" },description = "CheckManagingPlaceholders - B2R - Check Audit Loan for B2R")
	public void CheckAuditLoanB2R()
	{
		log.info("CheckManagingPlaceholders_01 - Step 01. Login with Admin");
		adminPage = loginPage.loginWithAdmin(usernameAdmin, passwordAdmin, false);
		
		log.info("CheckManagingPlaceholders_01 - Step 02. Open Managing Placeholders tab");
		adminPage.openAuditLoanTab();
		
		log.info("CheckManagingPlaceholders_01 - Step 03. Open Section Types tab");
		adminPage.openManageAuditProfiles();
		
		log.info("CheckManagingPlaceholders_01 - Step 04. Open Property Manager placeholders");
		adminPage.openProfilesName("AuditLoanB2R (Dont Touch)");
		
		//Property Management
		
		log.info("VP: Property Management is displayed");
		verifyTrue(adminPage.isSectionsTypeDisplayed("Property Management"));
		
		log.info("CheckManagingPlaceholders_01 - Step 05. Open Property Manager general documents type dropdown box");
		adminPage.openGeneralDocumentTypeDropdownBox("Property Management");
		
		log.info("VP: Property Managerment general documents types are displayed correctly");
		verifyTrue(adminPage.isGenaralDocumentsTypeDisplayed("L31 - Management Company Questionnaire"));
		verifyTrue(adminPage.isGenaralDocumentsTypeDisplayed("L32 - Management Agreement"));
		verifyTrue(adminPage.isGenaralDocumentsTypeDisplayed("L33 - Management Company Certificate of Formation"));
		verifyTrue(adminPage.isGenaralDocumentsTypeDisplayed("L34 - Management Company Certificate of Good Standing"));
		
		//B2R UW / Credit
		
		log.info("VP: B2R UW / Credit is displayed");
		verifyTrue(adminPage.isSectionsTypeDisplayed("B2R UW / Credit"));
		
		log.info("CheckManagingPlaceholders_01 - Step 05. Open B2R UW / Credit general documents type dropdown box");
		adminPage.openGeneralDocumentTypeDropdownBox("B2R UW / Credit");
		
		log.info("VP: B2R UW / Credit general documents types are displayed correctly");
		verifyTrue(adminPage.isGenaralDocumentsTypeDisplayed("L41 - Credit Memo/Approval"));
		verifyTrue(adminPage.isGenaralDocumentsTypeDisplayed("L42 - Final Rockport UW Model"));
		
		//Borrower (SPE)
		
		log.info("VP: Borrower (SPE) is displayed");
		verifyTrue(adminPage.isSectionsTypeDisplayed("Borrower (SPE)"));
		
		log.info("CheckManagingPlaceholders_01 - Step 05. Open Borrower (SPE) general documents type dropdown box");
		adminPage.openGeneralDocumentTypeDropdownBox("Borrower (SPE)");
		
		log.info("VP: Borrower (SPE) general documents types are displayed correctly");
		verifyTrue(adminPage.isGenaralDocumentsTypeDisplayed("L1 - Loan Application"));
		verifyTrue(adminPage.isGenaralDocumentsTypeDisplayed("L3 - Background Search"));
		
		//Guarantor
		
		log.info("VP: Guarantor is displayed");
		verifyTrue(adminPage.isSectionsTypeDisplayed("Guarantor"));
		
		log.info("CheckManagingPlaceholders_01 - Step 05. Open Guarantor general documents type dropdown box");
		adminPage.openGeneralDocumentTypeDropdownBox("Guarantor");
		
		log.info("VP: Guarantor general documents types are displayed correctly");
		verifyTrue(adminPage.isGenaralDocumentsTypeDisplayed("L17 - Personal Financial Statement"));
		verifyTrue(adminPage.isGenaralDocumentsTypeDisplayed("L18 - PFS Certification"));
		verifyTrue(adminPage.isGenaralDocumentsTypeDisplayed("L19 - Proof of Liquidity Statements"));
		verifyTrue(adminPage.isGenaralDocumentsTypeDisplayed("L20 - Real Estate Resume"));
		
		//Property
		
		log.info("VP: Property documents types are displayed correctly");
		verifyTrue(adminPage.isGenaralDocumentsTypeDisplayed("P1 - Appraisal/BPO (with Market Rent Data)"));
		verifyTrue(adminPage.isGenaralDocumentsTypeDisplayed("P2 - Lease Agreement"));
	}
	
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private AdminPage adminPage;
	private String usernameAdmin, passwordAdmin;
}