package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import page.UserLoginsPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_002_ListOfUsersPage extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameUser = Constant.USERNAME_ADMIN;
		passwordUser = Constant.PASSWORD_ADMIN;
	}
	
	@Test(groups = { "regression" },description = "Search User with loan application is 'Colony'")
	public void ListOfUsers_01_SearchUserWithColonyLoanApplication()
	{
		log.info("ListOfUsers_01 - Step 01. Login with Admin");
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("ListOfUsers_01 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("ListOfUsers_01 - Step 03. Search loan application 'Colony'");
		userLoginsPage.selectLoanApplicationForSearchUser("Colony");
		
		log.info("ListOfUsers_01 - Step 04. Click Search button");
		userLoginsPage.clickSearchUserButton();
		
		log.info("VP: All user of Colony Loan Application display on table");
		verifyTrue(userLoginsPage.isAllUserForLoanApplicationDisplay("Colony"));
	}
	
	@Test(groups = { "regression" },description = "Search user with loan application is 'B2R'")
	public void ListOfUsers_02_SearchUserWithB2RLoanApplication()
	{
		log.info("ListOfUsers_02 - Step 01. Login with Admin");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("ListOfUsers_02 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("ListOfUsers_02 - Step 03. Search loan application 'B2R'");
		userLoginsPage.selectLoanApplicationForSearchUser("B2R");
		
		log.info("ListOfUsers_02 - Step 04. Click Search button");
		userLoginsPage.clickSearchUserButton();
		
		log.info("VP: All user of B2R Loan Application display on table");
		verifyTrue(userLoginsPage.isAllUserForLoanApplicationDisplay("B2R"));
	}
	
	@Test(groups = { "regression" },description = "Search user with loan application is 'FirstKey'")
	public void ListOfUsers_03_SearchUserWithFirstKeyLoanApplication()
	{
		log.info("ListOfUsers_03 - Step 01. Login with Admin");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("ListOfUsers_03 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("ListOfUsers_03 - Step 03. Search loan application 'FirstKey'");
		userLoginsPage.selectLoanApplicationForSearchUser("FirstKey");
		
		log.info("ListOfUsers_03 - Step 04. Click Search button");
		userLoginsPage.clickSearchUserButton();
		
		log.info("VP: All user of FirstKey Loan Application display on table");
		verifyTrue(userLoginsPage.isAllUserForLoanApplicationDisplay("FirstKey"));
	}
	
	@Test(groups = { "regression" },description = "Search user with loan application is 'Genesis'")
	public void ListOfUsers_04_SearchUserWithGenesisLoanApplication()
	{
		log.info("ListOfUsers_04 - Step 01. Login with Admin");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("ListOfUsers_04 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("ListOfUsers_04 - Step 03. Search loan application 'Genesis'");
		userLoginsPage.selectLoanApplicationForSearchUser("Genesis");
		
		log.info("ListOfUsers_04 - Step 04. Click Search button");
		userLoginsPage.clickSearchUserButton();
		
		log.info("VP: All user of Genesis Loan Application display on table");
		verifyTrue(userLoginsPage.isAllUserForLoanApplicationDisplay("Genesis"));
	}
	
	@Test(groups = { "regression" },description = "Search user with valid last name")
	public void ListOfUsers_05_SearchUserWithValidLastname()
	{
		log.info("ListOfUsers_05 - Step 01. Login with Admin");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("ListOfUsers_05 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("ListOfUsers_05 - Step 03. Search user with last name 'udi'");
		userLoginsPage.enterLastnameForSearchUser("udi");
		
		log.info("ListOfUsers_05 - Step 04. Click Search button");
		userLoginsPage.clickSearchUserButton();
		
		log.info("VP: All user of last name contain 'udi' display on table");
		verifyTrue(userLoginsPage.isAllUserForLastNameDisplay("udi"));
	}
	
	@Test(groups = { "regression" },description = "Search user with valid first name")
	public void ListOfUsers_06_SearchUserWithValidFirstname()
	{
		log.info("ListOfUsers_06 - Step 01. Login with Admin");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("ListOfUsers_06 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("ListOfUsers_06 - Step 03. Search user with first name 'udi'");
		userLoginsPage.enterFirstnameForSearchUser("udi");
		
		log.info("ListOfUsers_06 - Step 04. Click Search button");
		userLoginsPage.clickSearchUserButton();
		
		log.info("VP: All user of first name contain 'udi' display on table");
		verifyTrue(userLoginsPage.isAllUserForFirstNameDisplay("udi"));
	}
	
	@Test(groups = { "regression" },description = "Search user with valid login id")
	public void ListOfUsers_07_SearchUserWithValidLoginId()
	{
		log.info("ListOfUsers_07 - Step 01. Login with Admin");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("ListOfUsers_07 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("ListOfUsers_07 - Step 03. Search user with login id 'udi'");
		userLoginsPage.enterLoginIdForSearchUser("udi");
		
		log.info("ListOfUsers_07 - Step 04. Click Search button");
		userLoginsPage.clickSearchUserButton();
		
		log.info("VP: All user of login id contain 'udi' display on table");
		verifyTrue(userLoginsPage.isAllUserForLoginIdDisplay("udi"));
	}
	
	@Test(groups = { "regression" },description = "Search user with role is 'Lender'")
	public void ListOfUsers_08_SearchUserWithLenderRole()
	{
		log.info("ListOfUsers_08 - Step 01. Login with Admin");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("ListOfUsers_08 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("ListOfUsers_08 - Step 03. Search user with lender role");
		userLoginsPage.selectRoleForSearchUser("Lender");
		
		log.info("ListOfUsers_08 - Step 04. Click Search button");
		userLoginsPage.clickSearchUserButton();
		
		log.info("VP: All user of Lender role display on table");
		verifyTrue(userLoginsPage.isAllUserForRoleDisplay("Lender"));
	}
	
	@Test(groups = { "regression" },description = "Search user with role is 'Borrower'")
	public void ListOfUsers_09_SearchUserWithBorrowerRole()
	{
		log.info("ListOfUsers_09 - Step 01. Login with Admin");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("ListOfUsers_09 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("ListOfUsers_09 - Step 03. Search user with borrower role");
		userLoginsPage.selectRoleForSearchUser("Borrower");
		
		log.info("ListOfUsers_09 - Step 04. Click Search button");
		userLoginsPage.clickSearchUserButton();
		
		log.info("VP: All user of Borrower role display on table");
		verifyTrue(userLoginsPage.isAllUserForRoleDisplay("Borrower"));
	}
	
	@Test(groups = { "regression" },description = "Search user with role is 'Document Submitter'")
	public void ListOfUsers_10_SearchUserWithDocumentSubmitterRole()
	{
		log.info("ListOfUsers_10 - Step 01. Login with Admin");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("ListOfUsers_10 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("ListOfUsers_10 - Step 03. Search user with Document Submitter role");
		userLoginsPage.selectRoleForSearchUser("Document Submitter");
		
		log.info("ListOfUsers_10 - Step 04. Click Search button");
		userLoginsPage.clickSearchUserButton();
		
		log.info("VP: All user of Document Submitter role display on table");
		verifyTrue(userLoginsPage.isAllUserForRoleDisplay("Document Submitter"));
	}
	
	@Test(groups = { "regression" },description = "Search user with role is 'Broker'")
	public void ListOfUsers_11_SearchUserWithBrokerRole()
	{
		log.info("ListOfUsers_11 - Step 01. Login with Admin");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("ListOfUsers_11 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("ListOfUsers_11 - Step 03. Search user with Broker role");
		userLoginsPage.selectRoleForSearchUser("Broker");
		
		log.info("ListOfUsers_11 - Step 04. Click Search button");
		userLoginsPage.clickSearchUserButton();
		
		log.info("VP: All user of Broker role display on table");
		verifyTrue(userLoginsPage.isAllUserForRoleDisplay("Broker"));
	}
	
	@Test(groups = { "regression" },description = "Search user with role is 'Reviewer'")
	public void ListOfUsers_12_SearchUserWithReviewerRole()
	{
		log.info("ListOfUsers_12 - Step 01. Login with Admin");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("ListOfUsers_12 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("ListOfUsers_12 - Step 03. Search user with Reviewer role");
		userLoginsPage.selectRoleForSearchUser("Reviewer");
		
		log.info("ListOfUsers_12 - Step 04. Click Search button");
		userLoginsPage.clickSearchUserButton();
		
		log.info("VP: All user of Reviewer role display on table");
		verifyTrue(userLoginsPage.isAllUserForRoleDisplay("Reviewer"));
	}
	
	@Test(groups = { "regression" },description = "Search user with role is 'Member'")
	public void ListOfUsers_13_SearchUserWithMemberRole()
	{
		log.info("ListOfUsers_13 - Step 01. Login with Admin");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("ListOfUsers_13 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("ListOfUsers_13 - Step 03. Search user with Member role");
		userLoginsPage.selectRoleForSearchUser("Member");
		
		log.info("ListOfUsers_13 - Step 04. Click Search button");
		userLoginsPage.clickSearchUserButton();
		
		log.info("VP: All user of Member role display on table");
		verifyTrue(userLoginsPage.isAllUserForRoleDisplay("Member"));
	}
	
	@Test(groups = { "regression" },description = "Search user with Active status is 'Yes'")
	public void ListOfUsers_14_SearchUserWithYesActiveStatus()
	{
		log.info("ListOfUsers_14 - Step 01. Login with Admin");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("ListOfUsers_14 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("ListOfUsers_14 - Step 03. Search user with yes active status");
		userLoginsPage.selectYesActiveStatusForSearchUser();
		
		log.info("ListOfUsers_14 - Step 04. Click Search button");
		userLoginsPage.clickSearchUserButton();
		
		log.info("VP: All user of Yes Active status display on table");
		verifyTrue(userLoginsPage.isAllUserForActiveStatusDisplay("Yes"));
	}
	
	@Test(groups = { "regression" },description = "Search user with Active status is 'No'")
	public void ListOfUsers_15_SearchUserWithNoActiveStatus()
	{
		log.info("ListOfUsers_15 - Step 01. Login with Admin");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("ListOfUsers_15 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("ListOfUsers_15 - Step 03. Search user with no active status");
		userLoginsPage.selectNoActiveStatusForSearchUser();
		
		log.info("ListOfUsers_15 - Step 04. Click Search button");
		userLoginsPage.clickSearchUserButton();
		
		log.info("VP: All user of No Active status display on table");
		verifyTrue(userLoginsPage.isAllUserForActiveStatusDisplay("No"));
	}
	
	@Test(groups = { "regression" },description = "Search user with Consolidation status is 'Yes'")
	public void ListOfUsers_16_SearchUserWithYesConsolidationStatus()
	{
		log.info("ListOfUsers_16 - Step 01. Login with Admin");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("ListOfUsers_16 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("ListOfUsers_16 - Step 03. Search user with yes consolidation status");
		userLoginsPage.selectYesConsolidationStatusForSearchUser();
		
		log.info("ListOfUsers_16 - Step 04. Click Search button");
		userLoginsPage.clickSearchUserButton();
		
		log.info("VP: All user of Yes Consolidation status display on table");
		verifyTrue(userLoginsPage.isAllUserForConsolidationStatusDisplay("Yes"));
	}
	
	@Test(groups = { "regression" },description = "Search user with Consolidation status is 'No'")
	public void ListOfUsers_17_SearchUserWithNoConsolidationStatus()
	{
		log.info("ListOfUsers_17 - Step 01. Login with Admin");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("ListOfUsers_17 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("ListOfUsers_17 - Step 03. Search user with no consolidation status");
		userLoginsPage.selectNoConsolidationStatusForSearchUser();
		
		log.info("ListOfUsers_17 - Step 04. Click Search button");
		userLoginsPage.clickSearchUserButton();
		
		log.info("VP: All user of No Consolidation status display on table");
		verifyTrue(userLoginsPage.isAllUserForConsolidationStatusDisplay("No"));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UserLoginsPage userLoginsPage;
	private String usernameUser, passwordUser;
}