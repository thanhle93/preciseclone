package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import common.AbstractTest;
import common.Common;
import common.Constant;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;

public class sanityTest_015_STO_AddNewLesseeDocument extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + getUniqueNumber();
		documentFileName01 = "datatest1.pdf";
		documentFileName02 = "datatest2.pdf";
		year = Common.getCommon().getCurrentYearOfWeek();
		submitVia = "Store Automation";
		documentSectionSearch = "Applicant";
		documentSection = "Corporate Entity Documents - Applicant";
		documentType = "Applicant 01";
	}

	@Test(groups = { "regression" }, description = "Document 01 - Add New Lessee Documents")
	public void Document_01_AddNewLesseedocuments() {

		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("Precondition - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("Document_01 - Step 01: Open Lessee tab");
		transactionPage.openLesseeTab();

		log.info("Document_01 - Step 02. Click 'New Lessee Documents' button");
		transactionPage.clickNewTransactionDocumentLesseeButton();

		log.info("Document_01 - Step 03. Select Section value");
		transactionPage.selectTransactionDocumentLesseeSection(documentSection);

		log.info("Document_01 - Step 04. Select Document Type");
		transactionPage.selectTransactionDocumentLesseeDocumentType(documentType);

		log.info("Document_01 - Step 05. Upload Document file name 01");
		transactionPage.uploadDocumentFileName(documentFileName01);

		log.info("Document_01 - Step 06. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("Document_01 - Step 07. Click 'Document List' button");
		transactionPage.clickDocumentListButtonAtDocumentTypeDetails();

		log.info("Document_01 - Step 08. Select 'Section/ Document Type' search");
		transactionPage.searchSectionAndDocumentType(documentSectionSearch, documentType);

		log.info("VP: Document type is created successfully");
		verifyTrue(transactionPage.isDocumentUploadedToDocumentType(documentType));
	}

	@Test(groups = { "regression" }, description = "Document 02 - Make sure you can see both the old and new document")
	public void Document_02_OpenBothTheOldDocumentAndNewDocument() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("Document_02 - Step 01: Open Lessee tab");
		transactionPage.openLesseeTab();
		
		log.info("Document_02 - Step 02. Select 'Section/ Document Type' search");
		transactionPage.searchSectionAndDocumentType(documentSectionSearch, documentType);
		
		log.info("Document_02 - Step 03: Open Document Type detail");
		transactionPage.openDocumentDetail(documentType);
		
		log.info("Document_02 - Step 04. Upload document file name 02");
		transactionPage.uploadDocumentFileName(documentFileName02);

		log.info("Document_02 - Step 05. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("VP: Document file name 01 is uploaded successfully");
		verifyTrue(transactionPage.isDocumentHistoryLoadedToDocumentType(documentFileName01, submitVia, "/" + year));

		log.info("VP: Document file name 02 is uploaded successfully");
		verifyTrue(transactionPage.isDocumentHistoryLoadedToDocumentType(documentFileName02, submitVia, "/" + year));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private int year;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName,  submitVia;
	private String documentFileName01, documentFileName02;
	private String documentSection, documentType, documentSectionSearch;
}