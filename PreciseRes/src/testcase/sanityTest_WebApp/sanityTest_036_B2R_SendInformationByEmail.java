package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Constant;
import page.DocumentsPage;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.MailHomePage;
import page.MailLoginPage;
import page.PageFactory;
import page.PropertiesPage;
import page.UsersPage;

public class sanityTest_036_B2R_SendInformationByEmail extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower";
		borrowerName = "UdiTeamBorrower";
		address1 = "37412 OAKHILL ST";
		documentType1 = "P2 - Lease Agreement";
		documentFileName = "datatest.pdf";
		propertiesFileNameRequired = "B2R Required.xlsx";
		documentSection1 = "General Loan Documents - Property Management";
		documentType6 = "L32 - Management Agreement";
		documentSection2 = "Corporate Entity Documents - Borrower (SPE)";
		documentType7 = "L8 - Borrower Org Chart";
		mailPageUrl = "https://accounts.google.com";
		usernameEmail = "minhdam06@gmail.com";
		passwordEmail = "dam123!@#!@#";
		invalidEmail = "abc!@#@gmail.abc";
		emailSubject = "precise";
		emailMessage = "pres information";
		
	}

	@Test(groups = { "regression" }, description = "Send Information 01 - Send information by email at Users page")
	public void SendInformation_01_UserPage() {

		log.info("SendInformation_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		preciseRESHomePageUrl = loginPage.getCurrentUrl(driver);
		
		log.info("SendInformation_01 - Step 02. Go to User Page");
		usersPage = homePage.openUsersPage(driver, ipClient);
		
		log.info("SendInformation_01 - Step 03. Get first username");
		emailContent = usersPage.getUserInformationB2RCAF();
		
		log.info("SendInformation_01 - Step 04. Click on Send this information to Email button");
		usersPage.clickSendInformationToEmail();
		
		log.info("SendInformation_01 - Step 05. Click on Send this information to Email button");
		verifyTrue(usersPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_01 - Step 06. Open Email url");
		mailLoginPage = usersPage.openMailLink(driver, mailPageUrl);
		mailLoginPage.loginToGmail(usernameEmail, passwordEmail);

		log.info("SendInformation_01 - Step 07. Click Submit button");
		mailHomePage = mailLoginPage.clickSubmitButton(driver, ipClient);

		log.info("SendInformation_01 - Step 08. Click 'Google Apps' title");
		mailHomePage.clickGoogleAppsTitle();

		log.info("SendInformation_01 - Step 09. Click 'Gmail' application");
		mailHomePage.clickGmailApplication();

		log.info("VP. Gmail home page is displayed");
		verifyTrue(mailHomePage.isGmailHomePageDisplay());

		log.info("SendInformation_01 - Step 10. Click 'Inbox' title");
		mailHomePage.clickInboxTitle();

		log.info("SendInformation_01 - Step 11. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail();
		
		log.info("SendInformation_01 - Step 12. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_01 - Step 13. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_01 - Step 14. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_01 - Step 15. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_01 - Step 16. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		log.info("SendInformation_01 - Step 17. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESHomePageUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Loans Property tab")
	public void SendInformation_02_LoansPropertiesTab() {
		
		log.info("SendInformation_02 - Pre-condition 01. Go to Loans Page");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("SendInformation_02 - Pre-condition 02. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, borrowerName, loanStatus, loanName);

		log.info("SendInformation_02 - Pre-condition 03. Open 'Properties' tab");
		loansPage.openPropertiesTab();

		log.info("SendInformation_02 - Pre-condition 04. Load test file");
		loansPage.uploadFileProperties(propertiesFileNameRequired);
		
		log.info("SendInformation_02 - Pre-condition 05. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("SendInformation_02 - Pre-condition 06. Add document through the 'New' button");
		propertiesPage.clickNewDocumentButton();
		propertiesPage.selectDocumentType(documentType1);
		propertiesPage.uploadDocumentFile(documentFileName);
		propertiesPage.uncheckPrivateCheckbox();
		propertiesPage.clickSaveButton();
		propertiesPage.clickGoToPropertyButton();
		
		log.info("VP: Document is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, documentFileName));

		log.info("SendInformation_02 - Pre-condition 07. Open 'General Loan Documents' page");
		documentsPage = propertiesPage.openGeneralDocumentTab();

		log.info("SendInformation_02 - Pre-condition 08. New 'General Loan Document' file");
		documentsPage.clickNewButton();
		documentsPage.selectGeneralDocumentSection(documentSection1);
		documentsPage.selectGeneralDocumentType(documentType6);
		documentsPage.uploadDocumentFile(documentFileName);
		documentsPage.clickSaveButton();
				
		log.info("SendInformation_02 - Step 09. Open 'Corporate Entity Documents' page");
		documentsPage.openCorporateEntityDocumentsTab();
		
		log.info("SendInformation_02 - Step 10. New 'Corporate Entity Documents' file");
		documentsPage.clickNewButton();
		documentsPage.selectGeneralDocumentSection(documentSection2);
		documentsPage.selectGeneralDocumentType(documentType7);
		documentsPage.uploadDocumentFile(documentFileName);
		documentsPage.clickSaveButton();

		log.info("SendInformation_02 - Step 11. Click Document List button");
		loansPage = documentsPage.clickOnPropertyTab();
		preciseRESUrl = loansPage.getCurrentUrl(driver);
		
		log.info("SendInformation_02 - Step 12. Get first address");
		emailContent = loansPage.getAddressProperties();
		
		log.info("SendInformation_02 - Step 13. Click on Send this information to Email button");
		loansPage.clickSendInformationToEmail();
		
		log.info("SendInformation_02 - Step 14. Click on Send this information to Email button");
		verifyTrue(propertiesPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_02 - Step 15. Open Email url");
		mailLoginPage = propertiesPage.openMailLink(driver, "https://mail.google.com");

		log.info("SendInformation_02 - Step 16. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail();
		
		log.info("SendInformation_02 - Step 17. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_02 - Step 18. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_02 - Step 19. Verify Email content");
		verifyEquals(mailHomePage.getEmailContentForCollumn2(), emailContent);
		
		log.info("SendInformation_02 - Step 20. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_02 - Step 21. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();

		log.info("SendInformation_02 - Step 22. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESUrl);
	}

	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Loans Properties Documents page")
	public void SendInformation_03_LoansPropertyDocumentsTab() {
		
		log.info("SendInformation_03 - Step 01. Click Property Document Tab");
		loansPage.openPropertyDocumentsTab();
		
		log.info("SendInformation_03 - Step 02. Get first address");
		emailContent = loansPage.getDocumentNumber();
		
		log.info("SendInformation_03 - Step 03. Click on Send this information to Email button");
		loansPage.clickSendInformationToEmail();
		
		log.info("SendInformation_03 - Step 04. Click on Send this information to Email button");
		verifyTrue(propertiesPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_03 - Step 05. Open Email url");
		mailLoginPage = propertiesPage.openMailLink(driver, "https://mail.google.com");

		log.info("SendInformation_03 - Step 06. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail();
		
		log.info("SendInformation_03 - Step 07. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_03 - Step 08. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_03 - Step 09. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_03 - Step 10. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_03 - Step 11. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();

		log.info("SendInformation_03 - Step 12. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Loans General Document Tab")
	public void SendInformation_04_GeneralDocumentsTab() {
		
		log.info("SendInformation_04 - Step 01. Click General Document Tab");
		documentsPage = loansPage.openGeneralDocumentTab();
		
		log.info("SendInformation_04 - Step 02. Get first address");
		emailContent = documentsPage.getDocumentNumber();
		
		log.info("SendInformation_04 - Step 03. Click on Send this information to Email button");
		documentsPage.clickSendInformationToEmail();
		
		log.info("SendInformation_04 - Step 04. Click on Send this information to Email button");
		verifyTrue(propertiesPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_04 - Step 05. Open Email url");
		mailLoginPage = propertiesPage.openMailLink(driver, "https://mail.google.com");

		log.info("SendInformation_04 - Step 06. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail();
		
		log.info("SendInformation_04 - Step 07. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_04 - Step 08. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_04 - Step 09. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_04 - Step 10. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_04 - Step 11. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();

		log.info("SendInformation_04 - Step 12. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Loans Corporate Entity Documents Tab")
	public void SendInformation_05_CorporateEntityDocumentsTab() {
		
		log.info("SendInformation_05 - Step 01. Click Corporate Entity Documents Tab");
		documentsPage = loansPage.openCorporateEntityDocumentsTab();
		
		log.info("SendInformation_05 - Step 02. Get first address");
		emailContent = documentsPage.getDocumentNumber();
		
		log.info("SendInformation_05 - Step 03. Click on Send this information to Email button");
		documentsPage.clickSendInformationToEmail();
		
		log.info("SendInformation_05 - Step 04. Click on Send this information to Email button");
		verifyTrue(propertiesPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_05 - Step 05. Open Email url");
		mailLoginPage = propertiesPage.openMailLink(driver, "https://mail.google.com");

		log.info("SendInformation_05 - Step 06. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail();
		
		log.info("SendInformation_05 - Step 07. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_05 - Step 08. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_05 - Step 09. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_05 - Step 10. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_05 - Step 11. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();

		log.info("SendInformation_05 - Step 12. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Loans Loaded Data Tapes Tab")
	public void SendInformation_06_LoadedDataTapesTab() {
		
		log.info("SendInformation_06 - Step 01. Click Loaded Data Tapes Tab");
		documentsPage = loansPage.openLoadDataTapesTab();
		
		log.info("SendInformation_06 - Step 02. Get first address");
		emailContent = documentsPage.getDataTapeName();
		
		log.info("SendInformation_06 - Step 03. Click on Send this information to Email button");
		documentsPage.clickSendInformationToEmail();
		
		log.info("SendInformation_06 - Step 04. Click on Send this information to Email button");
		verifyTrue(propertiesPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_06 - Step 05. Open Email url");
		mailLoginPage = propertiesPage.openMailLink(driver, "https://mail.google.com");

		log.info("SendInformation_06 - Step 06. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail();
		
		log.info("SendInformation_06 - Step 07. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_06 - Step 08. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_06 - Step 09. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_06 - Step 10. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_06 - Step 11. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();

		log.info("SendInformation_06 - Step 12. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESHomePageUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 02 - Send information by email at Properties page")
	public void SendInformation_07_PropertiesPage() {
		
		log.info("SendInformation_07 - Step 01. Go to Properties Page");
		propertiesPage = homePage.openPropertiesPage(driver, ipClient);
		
		log.info("SendInformation_07 - Step 02. Search by Address");
		propertiesPage.searchAddress("101 Main St");
		
		log.info("SendInformation_07 - Step 03. Get first address");
		emailContent = propertiesPage.getAddressProperties();
		
		log.info("SendInformation_07 - Step 03. Click on Send this information to Email button");
		propertiesPage.clickSendInformationToEmail();
		
		log.info("SendInformation_07 - Step 04. Click on Send this information to Email button");
		verifyTrue(propertiesPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_07 - Step 05. Open Email url");
		mailLoginPage = propertiesPage.openMailLink(driver, "https://mail.google.com");

		log.info("SendInformation_07 - Step 06. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail();
		
		log.info("SendInformation_07 - Step 07. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_07 - Step 08. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_07 - Step 09. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_07 - Step 10. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_07 - Step 11. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		log.info("SendInformation_07 - Step 11. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Loans page")
	public void SendInformation_08_LoansPage() {
		
		log.info("SendInformation_08 - Step 01. Go to Loans Page");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		log.info("SendInformation_08 - Step 02. Search Loan by name");
		loansPage.searchLoanByName(loanName);
		
		log.info("SendInformation_08 - Step 02. Get first legal name");
		emailContent = loansPage.getLegalName();
		
		log.info("SendInformation_08 - Step 03. Click on Send this information to Email button");
		loansPage.clickSendInformationToEmail();
		
		log.info("SendInformation_08 - Step 04. Click on Send this information to Email button");
		verifyTrue(loansPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_08 - Step 05. Open Email url");
		mailLoginPage = loansPage.openMailLink(driver, "https://mail.google.com");

		log.info("SendInformation_08 - Step 06. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail();
		
		log.info("SendInformation_08 - Step 07. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_08 - Step 08. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_08 - Step 09. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_08 - Step 10. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_08 - Step 11. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		log.info("SendInformation_08 - Step 12. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Documents page")
	public void SendInformation_09_DocumentsPage() {
		
		log.info("SendInformation_09 - Step 01. Go to Documents Page");
		documentsPage = homePage.openDocumentsPage(driver, ipClient);
		
		log.info("SendInformation_09 - Step 02. Search for Document type");
		documentsPage.searchDocumentType("L8 - Borrower Org Chart");
		
		log.info("SendInformation_09 - Step 02. Get first Document number");
		emailContent = documentsPage.getDocumentNumber();
		
		log.info("SendInformation_09 - Step 03. Click on Send this information to Email button");
		documentsPage.clickSendInformationToEmail();
		
		log.info("SendInformation_09 - Step 04. Click on Send this information to Email button");
		verifyTrue(documentsPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_09 - Step 05. Open Email url");
		mailLoginPage = documentsPage.openMailLink(driver, "https://mail.google.com");

		log.info("SendInformation_09 - Step 06. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail();
		
		log.info("SendInformation_09 - Step 07. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_09 - Step 08. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_09 - Step 09. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_09 - Step 10. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_09 - Step 11. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		log.info("SendInformation_09 - Step 12. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESUrl);
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UsersPage usersPage;
	private PropertiesPage propertiesPage;
	private LoansPage loansPage;
	private DocumentsPage documentsPage;
	private MailLoginPage mailLoginPage;
	private MailHomePage mailHomePage;
	private String preciseRESUrl, preciseRESHomePageUrl;
	private String mailPageUrl, usernameEmail, passwordEmail;
	private String invalidEmail, emailSubject, emailMessage, emailContent;
	private String usernameLender, passwordLender, address1, documentFileName;
	private String loanName, accountNumber, applicantName, loanStatus;
	private String documentType1, documentType6, documentType7;
	private String borrowerName, propertiesFileNameRequired, documentSection1, documentSection2;
}