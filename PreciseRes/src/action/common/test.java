package common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Start");
//		if(first("2","3","4","5")) System.out.println("true");
//		System.out.println(convertDate("06/01/2016"));
//		String Str = new String("  1. Financials");
//
//	      System.out.print("Return Value :" );
//	      System.out.println(Str.startsWith("1. Financials") );
		
//		System.out.print(convertDate("yyyy/mm/dd", "m/d/yyyy", "2016-04-29"));
//		System.out.print(addDateTimeByMonth("04/20/2015", 5));
		System.out.print(calculateDifferentBetweenDates("12/02/2014", "10/25/2017", "Date", true));
	}
	
	public static String convertDate(String oldDateType, String newDateType, String date)
	{
		String newDateString;

		SimpleDateFormat sdf = new SimpleDateFormat(oldDateType);
		Date d = new Date();
		try {
			d = sdf.parse(date.replace("-", "/"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sdf.applyPattern(newDateType);
		newDateString = sdf.format(d);
		return newDateString.toString();
	}
//	public static String convertDate(String date){
//		String[] dateParts = date.split("/");
//		int month = Integer.parseInt(dateParts[0]);
//		int day = Integer.parseInt(dateParts[1]);
//		return Integer.toString(month)+"/"+Integer.toString(day)+"/"+dateParts[2];
//	}
//	
	 public static boolean  first (String a, String... values) {
		 int visible = 0;
		 String[] str = {"3","4", "5"};
			 		for (String s:values){
			 			visible=0;
			 			for(int i=0;i<str.length;i++)
					 	{
					 		if(str[i].equals(s))
					 		{
					 			System.out.println(s);
					 			visible=1;	
					 			break;
					 		}
					 	}
			 			if(visible==0) return false;
			 		}
				return true;
		}
	     
	 public static String addDateTimeByDate(String inputString, int value){
		 SimpleDateFormat myFormat = new SimpleDateFormat("M/d/yyyy");

		 try {
		     Date date = myFormat.parse(inputString);
		     Calendar cal = Calendar.getInstance();
		     cal.setTime(date);
		     cal.add(Calendar.DATE, value );
		     return myFormat.format(cal.getTime()).toString(); 
		 } catch (Exception e) {
		     e.printStackTrace();
		 }
		 return "";
	 }
	 
	 public static String addDateTimeByMonth(String inputString, int value){
		 SimpleDateFormat myFormat = new SimpleDateFormat("M/d/yyyy");

		 try {
		     Date date = myFormat.parse(inputString);
		     Calendar cal = Calendar.getInstance();
		     cal.setTime(date);
		     cal.add(Calendar.MONTH, value );
		     return myFormat.format(cal.getTime()).toString(); 
		 } catch (Exception e) {
		     e.printStackTrace();
		 }
		 return "";
	 }
	 
	 public static String calculateDifferentBetweenDates(String date1, String date2, String timeType, Boolean acceptPossitive){
		 SimpleDateFormat myFormat = new SimpleDateFormat("M/d/yyyy");
		 try {
			 Date firstDate = myFormat.parse(date1);
			 Date secondDate = myFormat.parse(date2);
			 Boolean isFirstDateSmallerThanSecondDate = true;
			 if(firstDate == secondDate) return "0";
			 if(firstDate.after(secondDate)){
				 Date tempDate;
				 tempDate = firstDate;
				 firstDate = secondDate;
				 secondDate = tempDate;
				 isFirstDateSmallerThanSecondDate = false;
			 }
			 Calendar cal1 = Calendar.getInstance();
			 Calendar cal2 = Calendar.getInstance();
		     cal1.setTime(firstDate);
		     cal2.setTime(secondDate);
		     int daysBetween = 0; 
		     switch(timeType){
		     case "Date": 	while (cal1.before(cal2)) {  
		     					cal1.add(Calendar.DATE, 1);  
		     					daysBetween++;  
		     				} 
							break;
		     case "Month": 	while (cal1.before(cal2)) {  
								cal1.add(Calendar.MONTH, 1);  
								daysBetween++; 
		     				} 
							break;
		     case "Year": 	while (cal1.before(cal2)) {  
								cal1.add(Calendar.YEAR, 1);  
								daysBetween++;  
							} 
		     				break;

		     default: System.out.println("Time type isn't exist: "+timeType);
		     }
		     if(acceptPossitive&&isFirstDateSmallerThanSecondDate) 
		     return Integer.toString(-daysBetween);
		     else return Integer.toString(daysBetween);
		 } catch (Exception e) {
		     e.printStackTrace();
		 }
		 return "";
	 }

}
