package page;

import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import common.Common;
import PreciseRes.Interfaces;

public class PropertiesPage extends AbstractPage{
	public PropertiesPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}
	
	/**
	 * check Properties page display
	 * @param N/A
	 */
	public boolean isPropertiesPageDisplay() {
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTIES_PAGE_SECTION, timeWait);
		return isControlDisplayed(driver, Interfaces.PropertiesPage.PROPERTIES_PAGE_SECTION);
	}
	
	/**
	 * open Document Type Detail
	 */
	public void openDocumentTypeDetail(String docType) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_DOC_TYPE_ON_TABLE, docType, timeWait);
		click(driver, Interfaces.PropertiesPage.DYNAMIC_DOC_TYPE_ON_TABLE, docType);
		sleep();
	}
	
	/**
	 * select Document Type
	 */
	public void selectDocumentType(String documentType) {
		waitForControl(driver, Interfaces.PropertiesPage.DOCUMENT_TYPE_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.PropertiesPage.DOCUMENT_TYPE_COMBOBOX, documentType);
		sleep(1);
	}
	
	/**
	 * upload Document File
	 */
	public void uploadDocumentFile(String fileName) {
		waitForControl(driver, Interfaces.PropertiesPage.CHOOSE_FILE_BUTTON, timeWait);
		doubleClick(driver, Interfaces.PropertiesPage.CHOOSE_FILE_BUTTON);
		sleep(1);
		Common.getCommon().openFileForUpload(driver, fileName);
		sleep();
	}
	
	/**
	 * click Save Button [NEW DOCUMENT]
	 */
	public void clickSaveButton() {
		waitForControl(driver, Interfaces.PropertiesPage.SAVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
          sleep(10);
     }
		sleep(5);
		isRecordSavedMessageDisplays();
	}
	
	/**
	 * click Save Button [NEW DOCUMENT]
	 */
	public void clickSaveButtonValidDataType() {
		waitForControl(driver, Interfaces.PropertiesPage.SAVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
          sleep(10);
     }
		sleep(5);
		isBasicDetailMessageTitleDisplay("Please correct errors marked below");
	}
	
	/**
	 * click GoTo ProPerty Button
	 */
	public void clickGoToPropertyButton() {
		waitForControl(driver, Interfaces.PropertiesPage.GOTO_PROPERTY_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('GotoLAProp').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(3);
       }
		sleep(2);
	}
	
	/**
	 * click On Document List Button
	 */
	public void clickOnDocumentListButton() {
		waitForControl(driver, Interfaces.LoansPage.DOCUMENT_LIST_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.DOCUMENT_LIST_BUTTON);
		executeJavaScript(driver, "document.getElementById('GotoLAList').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(10);
       }
		sleep(1);
	}

	/**
	 * click On Make Document Active Button
	 */
	public void clickMakeDocumentActiveButton() {
		waitForControl(driver, Interfaces.PropertiesPage.MAKE_DOCUMENT_ACTIVE_BUTTON, timeWait);
		click(driver, Interfaces.PropertiesPage.MAKE_DOCUMENT_ACTIVE_BUTTON);
		executeJavaScript(driver, "document.getElementById('MakeDocumentActive').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(10);
       }
		sleep(1);
	}
	
	/**
	 * search Active
	 */
	public void searchActive() {
		waitForControl(driver,Interfaces.DocumentsPage.ACTIVE_RADIO_BUTTON, timeWait);
		click(driver, Interfaces.DocumentsPage.ACTIVE_RADIO_BUTTON);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(10);
       }
		sleep();
	}
	
	/**
	 * get Document ID In [General Loan Document - Corporate Entity Documents] Tab
	 */
	public String getDocumentID() {
		waitForControl(driver, Interfaces.DocumentsPage.GET_DOCUMENT_ID, timeWait);
		String documentID = getText(driver, Interfaces.DocumentsPage.GET_DOCUMENT_ID);
		return documentID;
	}
	
	public void searchDocumentID(String documentID) {
		waitForControl(driver, Interfaces.DocumentsPage.DOCUMENT_ID_TEXTBOX,	documentID, timeWait);
		type(driver, Interfaces.DocumentsPage.DOCUMENT_ID_TEXTBOX, documentID);
		sleep(1);
		click(driver, Interfaces.DocumentsPage.ACTIVE_RADIO_YES_BUTTON);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(10);
       }
		sleep(1);
	}
	
	/**
	 * check Document Loaded To Document Type
	 */
	public boolean isDocumentLoadedToDocumentType(String docType, String fileName) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL,docType, fileName, timeWait);
		return isControlDisplayed(driver, Interfaces.PropertiesPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL,docType, fileName);
	}
	
	/**
	 * check Private Checkbox Checked
	 */
	public boolean isPrivateCheckboxChecked() {
		waitForControl(driver, Interfaces.PropertiesPage.PRIVATE_CHECKBOX, timeWait);
		return isCheckboxChecked(driver, Interfaces.PropertiesPage.PRIVATE_CHECKBOX);
	}
	
	/**
	 * check Document Type Display On Property Detail
	 */
	public boolean isDocumentTypeDisplayOnPropertyDetail(String docType) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_DOC_TYPE_ON_PROPERTY_DETAIL, docType, timeWait);
		return isControlDisplayed(driver, Interfaces.PropertiesPage.DYNAMIC_DOC_TYPE_ON_PROPERTY_DETAIL, docType);
	}
	
	/**
	 * check Uploaded Document Opened
	 */
	public boolean isUploadedDocumentOpened(String fileName) {
		sleep();
		String currentHandle = driver.getWindowHandle();
		openWindowHandles.push(driver.getWindowHandle());
		Set<String> newHandles = driver.getWindowHandles();
		newHandles.removeAll(openWindowHandles);
		String handle = newHandles.iterator().next();
		driver.switchTo().window(handle);
		String url = getCurrentUrl(driver);
		boolean exists = url.contains(fileName);
		driver.close();
		driver.switchTo().window(currentHandle);
		
		return exists;
	}
	
	/**
	 * click New Document Button
	 */
	public void clickNewDocumentButton() {
		waitForControl(driver, Interfaces.PropertiesPage.NEW_DOCUMENT_BUTTON, timeWait);
		WebElement element = driver.findElement(By.id("PropertyLoanApplicationDetails_R1_Comments"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
		click(driver, Interfaces.PropertiesPage.NEW_DOCUMENT_BUTTON);
		sleep(2);
		if(driver.toString().toLowerCase().contains("internetexplorer")){
			sleep(6);
		}
	}
	
	/**
	 * open Document Of Document Type On Property Detail
	 */
	public void openDocumentOfDocumentTypeOnPropertyDetail(String documentType, String documentName) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL,documentType, documentName, timeWait);
		click(driver, Interfaces.PropertiesPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL,documentType, documentName);
		sleep(3);
	}
	
	/**
	 * uncheck Private Checkbox
	 */
	public void uncheckPrivateCheckbox() {
		waitForControl(driver, Interfaces.PropertiesPage.PRIVATE_CHECKBOX, timeWait);
		uncheckTheCheckbox(driver, Interfaces.PropertiesPage.PRIVATE_CHECKBOX);
		sleep(1);
	}
	
	/**
	 * check Private Checkbox
	 */
	public void checkPrivateCheckbox() {
		waitForControl(driver, Interfaces.PropertiesPage.PRIVATE_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.PropertiesPage.PRIVATE_CHECKBOX);
		sleep();
		if(isControlDisplayed(driver, Interfaces.PropertiesPage.YES_CONFIRM_BUTTON)){
			waitForControl(driver, Interfaces.PropertiesPage.YES_CONFIRM_BUTTON, timeWait);
			click(driver, Interfaces.PropertiesPage.YES_CONFIRM_BUTTON);
			sleep();
		}
	}
	
	/**
	 * uncheck Active Checkbox
	 */
	public void uncheckActiveCheckbox() {
		waitForControl(driver, Interfaces.PropertiesPage.ACTIVE_CHECKBOX, timeWait);
		uncheckTheCheckbox(driver, Interfaces.PropertiesPage.ACTIVE_CHECKBOX);
		sleep(1);
	}
	
	/**
	 * click On Mark Property Inactive Button
	 */
	public void clickOnMarkPropertyInactiveButton() {
		waitForControl(driver, Interfaces.PropertiesPage.MARK_PROPERTY_INACTIVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('MakeInactive').click();");
		executeJavaScript(driver, "document.getElementById('CBRMakeInactive').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(10);
       }
		sleep();
	}
	
	/**
	 * click On Mark Property Active Button
	 */
	public void clickOnMarkPropertyActiveButton() {
		waitForControl(driver, Interfaces.PropertiesPage.MARK_PROPERTY_ACTIVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('MakeActive').click();");
		executeJavaScript(driver, "document.getElementById('CBRMakeActive').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(10);
       }
		sleep();
	}
	
	/**
	 * click On Mark Inactive Property Button
	 */
	public void clickOnMarkInactivePropertyButton() {
		waitForControl(driver, Interfaces.PropertiesPage.MARK_INACTIVE_PROPERTY_BUTTON, timeWait);
		click(driver, Interfaces.PropertiesPage.MARK_INACTIVE_PROPERTY_BUTTON);
		sleep();
	}
	
	/**
	 * click On Mark Active Property Button
	 */
	public void clickOnMarkActivePropertyButton() {
		waitForControl(driver, Interfaces.PropertiesPage.MARK_ACTIVE_PROPERTY_BUTTON, timeWait);
		click(driver, Interfaces.PropertiesPage.MARK_ACTIVE_PROPERTY_BUTTON);
		sleep();
	}
	
	/**
	 * click On Property List Button
	 */
	public LoansPage clickOnPropertyListButton() {
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTY_LIST_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('ListLAProperties').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(10);
       }
		sleep();
		return PageFactory.getLoansPage(driver, ipClient);
	}
	
	/**
	 * Switch driver to iFrame
	 * @return driver
	 */
	public WebDriver switchToMakeInactiveFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.PropertiesPage.MAKE_INACTIVE_FRAME, timeWait);
		sleep(5);
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.PropertiesPage.MAKE_INACTIVE_FRAME)));
		return driver;
	}
	
	/**
	 * Switch driver to iFrame
	 * @return driver
	 */
	public WebDriver switchToMakeActiveFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.PropertiesPage.MAKE_ACTIVE_FRAME, timeWait);
		sleep(5);
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.PropertiesPage.MAKE_ACTIVE_FRAME)));
		return driver;
	}
	
	/**
	 * delete Property Document
	 */
	public void deletePropertyDocument(String documentType, String documentFileName) {
		WebElement element = driver.findElement(By.id("PropertyLoanApplicationDetails_R1_Comments"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_DELETE_DOCUMENT_TYPE_CHECKBOX,	documentType, documentFileName, timeWait);
		checkTheCheckbox(driver, Interfaces.PropertiesPage.DYNAMIC_DELETE_DOCUMENT_TYPE_CHECKBOX, documentType, documentFileName);
		sleep(2);
		clickSaveButton();
	}
	
	/**
	 * check Applicant Exist In Dropdown List On Properties Page
	 */
	public boolean isApplicantExistInDropdownListOnPropertiesPage(String applicantName) {
		boolean returnValue = false;
		waitForControl(driver, Interfaces.PropertiesPage.SEARCH_PROPERTY_APPLICANT_COMBOBOX, timeWait);
		int realNumber = countElement(driver, Interfaces.PropertiesPage.SEARCH_PROPERTY_APPLICANT_COMBOBOX_OPTION);
		if(realNumber==0){
			return false;
		}
		List<WebElement> elements = getElements(driver, Interfaces.PropertiesPage.SEARCH_PROPERTY_APPLICANT_COMBOBOX_OPTION);
		for(WebElement element : elements) {
			String text = element.getText();
			if(text.contains(applicantName)){
				returnValue = true;
				break;
			}
		}
		return returnValue;	
	}
	
	/**
	 * check Applicant Exist In Dropdown List On Properties Page
	 */
	public boolean isLoanExistInDropdownListOnPropertiesPage(String loanNam) {
		boolean returnValue = false;
		waitForControl(driver, Interfaces.PropertiesPage.SEARCH_PROPERTY_LOAN_COMBOBOX, timeWait);
		int realNumber = countElement(driver, Interfaces.PropertiesPage.SEARCH_PROPERTY_LOAN_COMBOBOX_OPTION);
		if(realNumber==0){
			return false;
		}
		List<WebElement> elements = getElements(driver, Interfaces.PropertiesPage.SEARCH_PROPERTY_LOAN_COMBOBOX_OPTION);
		for(WebElement element : elements) {
			String text = element.getText();
			if(text.contains(loanNam)){
				returnValue = true;
				break;
			}
		}
		return returnValue;	
	}
	
	/**
	 * update Property information
	 */
	public void updatePropertyInfor(String newCity, String newState, String newZip, String newPropertyType) {
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTY_CITY_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.PROPERTY_CITY_TEXTBOX, newCity);
		sleep(1);
		selectItemCombobox(driver, Interfaces.PropertiesPage.PROPERTY_STATE_COMBOBOX, newState);
		sleep(1);
		type(driver, Interfaces.PropertiesPage.PROPERTY_ZIP_TEXTBOX, newZip);
		sleep(1);
		selectItemCombobox(driver, Interfaces.PropertiesPage.PROPERTY_TYPE_COMBOBOX, newPropertyType);
		sleep(1);
		clickSaveButton();
	}
	
	/**
	 * select Parent Property Item
	 */
	public void selectParentPropertyItem(String parent) {
		waitForControl(driver, Interfaces.PropertiesPage.PARENT_PROPERTY_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.PropertiesPage.PARENT_PROPERTY_COMBOBOX, parent);
		sleep(1);
	}
	
	/**
	 * select Section 8 Combobox
	 */
	public void selectSection8(String section8) {
		waitForControl(driver, Interfaces.PropertiesPage.SECTION_8_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.PropertiesPage.SECTION_8_COMBOBOX, section8);
		sleep(2);
	}
	
	/**
	 * Enter Document Type
	 */
	public void enterDocumentType(String documentType) {
		waitForControl(driver, Interfaces.PropertiesPage.OTHER_DOCUMENT_TYPE_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.OTHER_DOCUMENT_TYPE_TEXTBOX, documentType);
		sleep(1);
	}
	
	/**
	 * check 'Document Type' On Properties Detail Loaded
	 */
	public boolean isDocumentTypeOnPropertiesDetailLoaded(String documentType) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_DOCTYPE_ON_PROPERTY_DETAIL,
				documentType, 5);
		return isControlDisplayed(driver, Interfaces.PropertiesPage.DYNAMIC_DOCTYPE_ON_PROPERTY_DETAIL,
				documentType);
	}
	
	/**
	 * upload Dynamic Document File by Place Holder
	 */
	public void uploadDynamicDocumentFileByPlaceHolder(String documentType, String fileName) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_CHOOSE_FILE_BUTTON_PLACE_HOLDER, documentType, timeWait);
		doubleClick(driver, Interfaces.PropertiesPage.DYNAMIC_CHOOSE_FILE_BUTTON_PLACE_HOLDER, documentType);
//		if(driver.toString().toLowerCase().contains("internetexplorerdriver"))
//		{
//			keyPressing("space");
//		}
		sleep(1);
		Common.getCommon().openFileForUpload(driver, fileName);
		sleep(1);
	}
	
	/**
	 * open Property Documents Tab
	 */
	public void openPropertyDocumentsTab() {
		waitForControl(driver, Interfaces.LoansPage.PROPERTY_DOCUMENTS_TAB, timeWait);
		executeJavaScript(driver, "document.getElementById('DocumentTB').click();");
		executeJavaScript(driver, "document.getElementById('CBRDocumentTB').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(5);
     }
//		click(driver, Interfaces.LoansPage.PROPERTY_DOCUMENTS_TAB);
		sleep();
	}
	
	/**
	 * click 'Check All' link
	 */
	public void clickReviewSelectedButton() {
		waitForControl(driver, Interfaces.LoansPage.CHECK_ALL_LINK,	timeWait);
		click(driver, Interfaces.LoansPage.CHECK_ALL_LINK);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('RevSel').click();");
		executeJavaScript(driver, "document.getElementById('RevSel2').click();");
		sleep(5);
	}
	
	/**
	 * check 'Review Selected' Document Loaded To Document Type
	 */
	public boolean isReviewSelectedDocumentLoadedToDocumentType(String docType, String reviewedBy) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_REVIEW_SELECTED_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL,
				docType, reviewedBy, timeWait);
		return isControlDisplayed(driver, Interfaces.PropertiesPage.DYNAMIC_REVIEW_SELECTED_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL,
				docType, reviewedBy);
	}
	
	/**
	 * check 'Activated' Document Loaded
	 */
	public boolean isActivatedDocumentLoadedToDocumentType(String docType, String active) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_ACTIVATED_DOCUMENT_FOR_DOCTYPE,	docType, active, timeWait);
		return isControlDisplayed(driver, Interfaces.PropertiesPage.DYNAMIC_ACTIVATED_DOCUMENT_FOR_DOCTYPE,	docType, active);
	}
	
	/**
	 * check 'Document Type' checkbox
	 */
	public void isSelectedDocumentTypeCheckbox(String documentType, String activeStatus) {
		waitForControl(driver, Interfaces.PropertiesPage.SELECTED_DOCUMENT_TYPE_CHECKBOX, documentType, activeStatus, timeWait);
		checkTheCheckbox(driver, Interfaces.PropertiesPage.SELECTED_DOCUMENT_TYPE_CHECKBOX, documentType, activeStatus);
		sleep(1);
	}
	
	/**
	 * get Number Of Document In Property Documents Tab
	 */
	public int getNumberOfDocumentInPropertyDocumentsTab() {
		waitForControl(driver, Interfaces.LoansPage.TOTAL_DOCUMENT_LABEL,
				timeWait);
		String text = getText(driver, Interfaces.LoansPage.TOTAL_DOCUMENT_LABEL);
		String[] subString = text.split(" ");
		int result = Integer.parseInt(subString[subString.length - 1]);
		return result;
	}
	
	/**
	 * Enter 'HOA'
	 */
	public void enterHOA(String hoaFee) {
		waitForControl(driver, Interfaces.PropertiesPage.HOA_FEE_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.HOA_FEE_TEXTBOX, hoaFee);
		sleep(1);
	}
	
	/**
	 * check 'Date Created' field not display
	 */
	public boolean isDateCreatedNotDisplay() {
		waitForControl(driver, Interfaces.PropertiesPage.DATE_CREATED_TEXTBOX,
				timeWait);
		return isControlDisplayed(driver, Interfaces.PropertiesPage.DATE_CREATED_TEXTBOX);
	}
	
	/**
	 * open General Document Tab
	 */
	public DocumentsPage openGeneralDocumentTab() {
		waitForControl(driver, Interfaces.LoansPage.GENERAL_LOANS_DOCUMENT_TAB,
				timeWait);
		executeJavaScript(driver,"document.getElementById('GeneralLoanDocsTB').click();");
		executeJavaScript(driver,"document.getElementById('CBRGeneralLoanDocsTB').click();");
		sleep();
		return PageFactory.getDocumentsPage(driver, ipClient);
	}
	
	/**
	 * input ZipCode
	 */
	public void inputZipCode(String zipCode) {
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTIES_ZIP_CODE_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.PROPERTIES_ZIP_CODE_TEXTBOX, zipCode);
		sleep(1);
	}
	
	/**
	 * check 'Zip code' Saved
	 */
	public boolean isZipCodeSaved(String zipCode) {
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTIES_ZIP_CODE_TEXTBOX, timeWait);
		String realZipCode = getAttributeValue(driver, Interfaces.PropertiesPage.PROPERTIES_ZIP_CODE_TEXTBOX, "value");
		return realZipCode.contains(zipCode);
	}
	
	/**
	 * check Record Saved Message Displays
	 * 
	 * @return
	 */
	public boolean isRecordSavedMessageDisplays() {
		waitForControl(driver, Interfaces.PropertiesPage.RECORD_SAVED_MESSAGE, timeWait);
		return isControlDisplayed(driver,Interfaces.PropertiesPage.RECORD_SAVED_MESSAGE);
	}
	
	/**
	 * click New Document Button
	 */
	public void inputCounty(String county) {
		waitForControl(driver, Interfaces.PropertiesPage.COUNTY_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.COUNTY_TEXTBOX, county);
		sleep();
	}
	
	/**
	 * Check Document History loaded to Document File, Submit Via, Submitted On
	 */
	public boolean isDocumentHistoryLoadedToDocumentType(String documentFile, String submitVia, String submitOn) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_HISTORY_DOCUMENT_LOADED_FOR_DOCTYPE, documentFile, submitVia, submitOn, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_HISTORY_DOCUMENT_LOADED_FOR_DOCTYPE, documentFile, submitVia, submitOn);
	}
	
	/**
	 * click Send this information to email
	 */
	public void clickSendInformationToEmail() {
		waitForControl(driver, Interfaces.PropertiesPage.SEND_INFORMATION_TO_EMAIL, timeWait);
		click(driver, Interfaces.PropertiesPage.SEND_INFORMATION_TO_EMAIL);
		sleep(15);
	}
	
	/**
	 * Get user information
	 */
	public String getAddressProperties() {
		waitForControl(driver, Interfaces.PropertiesPage.FIRST_ADDRESS, timeWait);
		return getText(driver, Interfaces.PropertiesPage.FIRST_ADDRESS).trim().toLowerCase();
	}
	
	/**
	 * search property address
	 */
	public void searchAddress(String address) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, address);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search4').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		sleep();
	}
	
	/**
	 * Check Loan changed date displays
	 */
	
	public Boolean isLoanDateChangeDisplayedCorrectly(){
		waitForControl(driver, Interfaces.PropertiesPage.DOCUMENT_TYPE_LINK, timeWait);
		int numberOfDocumentTypes = countElement(driver, Interfaces.PropertiesPage.DOCUMENT_TYPE_LINK);
		for(int i =1; i<= numberOfDocumentTypes; i++){
			if(!getText(driver, Interfaces.PropertiesPage.DYNAMIC_LAST_CHANGED_DATE_TEXT_BY_NUMBERICAL, Integer.toString(i)).equals(""))
				if(!isControlDisplayed(driver, Interfaces.PropertiesPage.DYNAMIC_FILE_UPLOADED_LINK, Integer.toString(i))) 
					return false;
		}
		return true;
	}
	
	/**
	 * check Load data message title display
	 * 
	 * @param N/A
	 */
	public boolean isBasicDetailMessageTitleDisplay(String messageContent) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_BASIC_DETAIL_MESSAGE_TITLE, messageContent, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_BASIC_DETAIL_MESSAGE_TITLE, messageContent);
	}
	
	/**
	 * check Dynamic Alert message title display
	 * 
	 * @param id
	 * @param messageContent
	 * @return
	 */
	public boolean isDynamicAlertMessageTitleDisplay(String id, String messageContent) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_FIELD_ALERT_MESSAGE_TITLE, id, messageContent, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_FIELD_ALERT_MESSAGE_TITLE, id, messageContent);
	}
	
	/**
	 * Input Address
	 */
	public void inputAddress(String address) {
		waitForControl(driver, Interfaces.TransactionPage.ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.ADDRESS_TEXTBOX, address);
		sleep();
	}

	/**
	 * input Property Name 
	 */
	public void inputPropertyName(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTY_NAME, timeWait);
		type(driver, Interfaces.PropertiesPage.PROPERTY_NAME, value);
		sleep();
	}
	
	/**
	 * input BD 
	 */
	public void inputBD(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.BED_ROOM, timeWait);
		type(driver, Interfaces.PropertiesPage.BED_ROOM, value);
		sleep();
	}
	
	/**
	 * input BA 
	 */
	public void inputBA (String value) {
		waitForControl(driver, Interfaces.PropertiesPage.BATH, timeWait);
		type(driver, Interfaces.PropertiesPage.BATH, value);
		sleep();
	}
	
	/**
	 * input SF 
	 */
	public void inputSF (String value) {
		waitForControl(driver, Interfaces.PropertiesPage.SQFT, timeWait);
		type(driver, Interfaces.PropertiesPage.SQFT, value);
		sleep();
	}
	
	/**
	 * input Year Built 
	 */
	public void inputYearBuilt(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.YEAR_BUILT, timeWait);
		type(driver, Interfaces.PropertiesPage.YEAR_BUILT, value);
		sleep();
	}
	
	/**
	 * input County 
	 */
	public void county(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.COUNTY, timeWait);
		type(driver, Interfaces.PropertiesPage.COUNTY, value);
		sleep();
	}
	
	/**
	 * input Acquisition Date 
	 */
	public void inputAcquisitionDate (String value) {
		waitForControl(driver, Interfaces.PropertiesPage.ACQUISITION_DATE, timeWait);
		type(driver, Interfaces.PropertiesPage.ACQUISITION_DATE, value);
		sleep();
	}
	
	/**
	 * input Rehab Completion Date
	 */
	public void inputRehabCompletionDate(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.RENOVATION_DATE, timeWait);
		type(driver, Interfaces.PropertiesPage.RENOVATION_DATE, value);
		sleep();
	}
	
	/**
	 * input Lease End Date  
	 */
	public void inputLeaseEndDate (String value) {
		waitForControl(driver, Interfaces.PropertiesPage.LEASE_EXPIRE, timeWait);
		type(driver, Interfaces.PropertiesPage.LEASE_EXPIRE, value);
		sleep();
	}
	
	/**
	 * input CapEx Reserves 
	 */
	public void inputCapExReserves(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.CAPEX_RESERVES, timeWait);
		type(driver, Interfaces.PropertiesPage.CAPEX_RESERVES, value);
		sleep();
	}
	
	/**
	 * input Insurance 
	 */
	public void inputInsurance(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.INSURANCE, timeWait);
		type(driver, Interfaces.PropertiesPage.INSURANCE, value);
		sleep();
	}
	
	/**
	 * input Repair Cost 
	 */
	public void inputRepairCost (String value) {
		waitForControl(driver, Interfaces.PropertiesPage.REPAIR_COST, timeWait);
		type(driver, Interfaces.PropertiesPage.REPAIR_COST, value);
		sleep();
	}
	
	/**
	 * input Taxes 
	 */
	public void inputTaxes(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.TAXES_AMOUNT, timeWait);
		type(driver, Interfaces.PropertiesPage.TAXES_AMOUNT, value);
		sleep();
	}
	
	/**
	 * input Management Fee 
	 */
	public void inputManagementFee(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.MANAGEMENT_FEE, timeWait);
		type(driver, Interfaces.PropertiesPage.MANAGEMENT_FEE, value);
		sleep();
	}
	
	/**
	 * input HOA Fee 
	 */
	public void inputHOAFee(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.HOA, timeWait);
		type(driver, Interfaces.PropertiesPage.HOA, value);
		sleep();
	}
	
	/**
	 * input Vacancy Factor
	 */
	public void inputVacancyFactor(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.VACANCY_FACTOR, timeWait);
		type(driver, Interfaces.PropertiesPage.VACANCY_FACTOR, value);
		sleep();
	}
	
	/**
	 * input Vacancy Cost
	 */
	public void inputVacancyCost(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.VACANCY_COST, timeWait);
		type(driver, Interfaces.PropertiesPage.VACANCY_COST, value);
		sleep();
	}
	
	/**
	 * input Comment
	 */
	public void inputComment(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.COMMENTS, timeWait);
		type(driver, Interfaces.PropertiesPage.COMMENTS, value);
		sleep();
	}
	
	/**
	 * input Verified Rent 
	 */
	public void inputVerifiedRent (String value) {
		waitForControl(driver, Interfaces.PropertiesPage.VERIFIED_RENT, timeWait);
		type(driver, Interfaces.PropertiesPage.VERIFIED_RENT, value);
		sleep();
	}
	
	/**
	 * input Verified Tax 
	 */
	public void inputVerifiedTax (String value) {
		waitForControl(driver, Interfaces.PropertiesPage.VERIFIED_TAX, timeWait);
		type(driver, Interfaces.PropertiesPage.VERIFIED_TAX, value);
		sleep();
	}
	
	/**
	 * input Verified Acquisition Price 
	 */
	public void inputVerifiedAcquisitionPrice (String value) {
		waitForControl(driver, Interfaces.PropertiesPage.VERIFIED_ACQUISITION_PRICE, timeWait);
		type(driver, Interfaces.PropertiesPage.VERIFIED_ACQUISITION_PRICE, value);
		sleep();
	}
	
	/**
	 * input Verified BPO 
	 */
	public void inputVerifiedBPO (String value) {
		waitForControl(driver, Interfaces.PropertiesPage.VERIFIED_BPO, timeWait);
		type(driver, Interfaces.PropertiesPage.VERIFIED_BPO, value);
		sleep();
	}
	
	/**
	 * input Rent 
	 */
	public void inputRent(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.RENT, timeWait);
		type(driver, Interfaces.PropertiesPage.RENT, value);
		sleep();
	}
	
	/**
	 * input Owner Paid Utilities  
	 */
	public void inputOwnerPaidUtilities (String value) {
		waitForControl(driver, Interfaces.PropertiesPage.OWNER_PAID_UTILITIES, timeWait);
		type(driver, Interfaces.PropertiesPage.OWNER_PAID_UTILITIES, value);
		sleep();
	}
	
	/**
	 * input Security Deposit  
	 */
	public void inputSecurityDeposit(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.SECURITY_DEPOSIT_AMOUNT, timeWait);
		type(driver, Interfaces.PropertiesPage.SECURITY_DEPOSIT_AMOUNT, value);
		sleep();
	}
	
	/**
	 * input Lease Start Date 
	 */
	public void inputLeaseStartDate(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.LEASE_START_DATE, timeWait);
		type(driver, Interfaces.PropertiesPage.LEASE_START_DATE, value);
		sleep();
	}
	
	/**
	 * input Borrower Opinion of Curr. Value 
	 */
	public void inputBorrowerOpinionOfCurrValue(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.BORROWER_OPINION_OF_CURR_VALUE, timeWait);
		type(driver, Interfaces.PropertiesPage.BORROWER_OPINION_OF_CURR_VALUE, value);
		sleep();
	}
	
	/**
	 * input Acquisition Price  
	 */
	public void inputAcquisitionPrice (String value) {
		waitForControl(driver, Interfaces.PropertiesPage.ACQUISITION_PRICE, timeWait);
		type(driver, Interfaces.PropertiesPage.ACQUISITION_PRICE, value);
		sleep();
	}
	
	/**
	 * input Flood Zone Name 
	 */
	public void inputFloodZoneName(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.FLOOD_ZONE, timeWait);
		type(driver, Interfaces.PropertiesPage.FLOOD_ZONE, value);
		sleep();
	}
	
	/**
	 * input Zip 
	 */
	public void inputZip(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTY_ZIP_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.PROPERTY_ZIP_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input City
	 */
	public void inputCity(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTY_CITY_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.PROPERTY_CITY_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input unit
	 */
	public void inputUnit(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.UNITS, timeWait);
		type(driver, Interfaces.PropertiesPage.UNITS, value);
		sleep();
	}
	
	/**
	 * select Property type
	 */
	public void selectPropertyTypeComboBox(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTY_TYPE_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.PropertiesPage.PROPERTY_TYPE_COMBOBOX, value);
	}
	
	/**
	 * select Property state
	 */
	public void selectPropertyStateComboBox(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTY_STATE_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.PropertiesPage.PROPERTY_STATE_COMBOBOX, value);
	}
	
	/**
	 * select Partial flood
	 */
	public void selectPartialFloodComboBox(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.PARTIAL_FLOOD_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.PropertiesPage.PARTIAL_FLOOD_DROPDOWN, value);
	}
	
	/**
	 * select Current leased
	 */
	public void selectCurrentLeasedComboBox(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.CURRENT_LEASED_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.PropertiesPage.CURRENT_LEASED_DROPDOWN, value);
	}
	
	/**
	 * select Flood zone
	 */
	public void selectFloodZoneComboBox(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.FLOOD_ZONE_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.PropertiesPage.FLOOD_ZONE_DROPDOWN, value);
	}
	
	/**
	 * select Air Conditioning
	 */
	public void selectAirConditioningComboBox(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.AIR_CONDITIONING, timeWait);
		selectItemCombobox(driver, Interfaces.PropertiesPage.AIR_CONDITIONING, value);
	}
	
	//check result
	
	/**
	 * is Address displayed correctly
	 */
	public Boolean isAddressDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.TransactionPage.ADDRESS_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.TransactionPage.ADDRESS_TEXTBOX, "value").contains(value);
	}

	/**
	 * is Property Name displayed correctly
	 */
	public Boolean isPropertyNameDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTY_NAME, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.PROPERTY_NAME, "value").contains(value);
	}
	
	/**
	 * is BD displayed correctly
	 */
	public Boolean isBDDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.BED_ROOM, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.BED_ROOM, "value").contains(value);
	}
	
	/**
	 * is BA displayed correctly
	 */
	public Boolean isBADisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.BATH, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.BATH, "value").contains(value);
	}
	
	/**
	 * is SF displayed correctly
	 */
	public Boolean isSFDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.SQFT, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.SQFT, "value").contains(value);
	}
	
	/**
	 * is Year Built displayed correctly
	 */
	public Boolean isYearBuiltDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.YEAR_BUILT, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.YEAR_BUILT, "value").contains(value);
	}
	
	/**
	 * is County displayed correctly
	 */
	public Boolean isCountyDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.COUNTY, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.COUNTY, "value").contains(value);
	}
	
	/**
	 * is Acquisition Date displayed correctly
	 */
	public Boolean isAcquisitionDateDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.ACQUISITION_DATE, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.ACQUISITION_DATE, "value").contains(value);
	}
	
	/**
	 * is Rehab Completion Date displayed correctly
	 */
	public Boolean isRehabCompletionDateDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.RENOVATION_DATE, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.RENOVATION_DATE, "value").contains(value);
	}
	
	/**
	 * is Lease End Date  displayed correctly
	 */
	public Boolean isLeaseEndDateDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.LEASE_EXPIRE, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.LEASE_EXPIRE, "value").contains(value);
	}
	
	/**
	 * is CapEx Reserves displayed correctly
	 */
	public Boolean isCapExReservesDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.CAPEX_RESERVES, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.CAPEX_RESERVES, "value").contains(value);
	}
	
	/**
	 * is Insurance displayed correctly
	 */
	public Boolean isInsuranceDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.INSURANCE, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.INSURANCE, "value").contains(value);
	}
	
	/**
	 * is Repair Cost displayed correctly
	 */
	public Boolean isRepairCostDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.REPAIR_COST, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.REPAIR_COST, "value").contains(value);
	}
	
	/**
	 * is Taxes displayed correctly
	 */
	public Boolean isTaxesDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.TAXES_AMOUNT, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.TAXES_AMOUNT, "value").contains(value);
	}
	
	/**
	 * is Management Fee displayed correctly
	 */
	public Boolean isManagementFeeDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.MANAGEMENT_FEE, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.MANAGEMENT_FEE, "value").contains(value);
	}
	
	/**
	 * is HOA Fee displayed correctly
	 */
	public Boolean isHOAFeeDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.HOA, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.HOA, "value").contains(value);
	}
	
	/**
	 * is Vacancy Factor displayed correctly
	 */
	public Boolean isVacancyFactorDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.VACANCY_FACTOR, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.VACANCY_FACTOR, "value").contains(value);
	}
	
	/**
	 * is Vacancy Cost displayed correctly
	 */
	public Boolean isVacancyCostDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.VACANCY_COST, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.VACANCY_COST, "value").contains(value);
	}
	
	/**
	 * is Comment displayed correctly
	 */
	public Boolean isCommentDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.COMMENTS, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.COMMENTS, "value").contains(value);
	}
	
	/**
	 * is Verified Rent displayed correctly
	 */
	public Boolean isVerifiedRentDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.VERIFIED_RENT, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.VERIFIED_RENT, "value").contains(value);
	}
	
	/**
	 * is Verified Tax displayed correctly
	 */
	public Boolean isVerifiedTaxDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.VERIFIED_TAX, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.VERIFIED_TAX, "value").contains(value);
	}
	
	/**
	 * is Verified Acquisition Price displayed correctly
	 */
	public Boolean isVerifiedAcquisitionPriceDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.VERIFIED_ACQUISITION_PRICE, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.VERIFIED_ACQUISITION_PRICE, "value").contains(value);
	}
	
	/**
	 * is Verified BPO displayed correctly
	 */
	public Boolean isVerifiedBPODisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.VERIFIED_BPO, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.VERIFIED_BPO, "value").contains(value);
	}
	
	/**
	 * is Rent displayed correctly
	 */
	public Boolean isRentDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.RENT, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.RENT, "value").contains(value);
	}
	
	/**
	 * is Owner Paid Utilities displayed correctly 
	 */
	public Boolean isOwnerPaidUtilitiesDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.OWNER_PAID_UTILITIES, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.OWNER_PAID_UTILITIES, "value").contains(value);
	}
	
	/**
	 * is Security Deposit  displayed correctly
	 */
	public Boolean isSecurityDepositDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.SECURITY_DEPOSIT_AMOUNT, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.SECURITY_DEPOSIT_AMOUNT, "value").contains(value);
	}
	
	/**
	 * is Lease Start Date displayed correctly
	 */
	public Boolean isLeaseStartDateDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.LEASE_START_DATE, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.LEASE_START_DATE, "value").contains(value);
	}
	
	/**
	 * is Borrower Opinion of Curr. Value displayed correctly 
	 */
	public Boolean isBorrowerOpinionOfCurrValueDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.BORROWER_OPINION_OF_CURR_VALUE, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.BORROWER_OPINION_OF_CURR_VALUE, "value").contains(value);
	}
	
	/**
	 * is Acquisition Price displayed correctly 
	 */
	public Boolean isAcquisitionPriceDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.ACQUISITION_PRICE, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.ACQUISITION_PRICE, "value").contains(value);
	}
	
	/**
	 * is Flood Zone Name displayed correctly
	 */
	public Boolean isFloodZoneNameDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.FLOOD_ZONE, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.FLOOD_ZONE, "value").contains(value);
	}
	
	/**
	 * is Zip displayed correctly
	 */
	public Boolean isZipDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTY_ZIP_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.PROPERTY_ZIP_TEXTBOX, "value").contains(value);
	}
	
	/**
	 * is City displayed correctly
	 */
	public Boolean isCityDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTY_CITY_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.PROPERTY_CITY_TEXTBOX, "value").contains(value);
	}
	
	/**
	 * is unit displayed correctly
	 */
	public Boolean isUnitDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.UNITS, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.UNITS, "value").contains(value);
	}
	
	/**
	 * is Property return getAttributeValue displayed correctly
	 */
	public Boolean isPropertyTypeComboBoxDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTY_TYPE_COMBOBOX, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.PropertiesPage.PROPERTY_TYPE_COMBOBOX).contains(value);
	}
	
	/**
	 * is Property state displayed correctly
	 */
	public Boolean isPropertyStateComboBoxDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTY_STATE_COMBOBOX, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.PropertiesPage.PROPERTY_STATE_COMBOBOX).contains(value);
	}
	
	/**
	 * is Partial flood displayed correctly
	 */
	public Boolean isPartialFloodComboBoxDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.PARTIAL_FLOOD_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.PropertiesPage.PARTIAL_FLOOD_DROPDOWN).contains(value);
	}
	
	/**
	 * is Current leased displayed correctly
	 */
	public Boolean isCurrentLeasedComboBoxDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.CURRENT_LEASED_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.PropertiesPage.CURRENT_LEASED_DROPDOWN).contains(value);
	}
	
	/**
	 * is Flood zonedisplayed correctly
	 */
	public Boolean isFloodZoneComboBoxDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.FLOOD_ZONE_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.PropertiesPage.FLOOD_ZONE_DROPDOWN).contains(value);
	}
	
	/**
	 * is Air Conditioning displayed correctly
	 */
	public Boolean isAirConditioningComboBoxDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.AIR_CONDITIONING, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.PropertiesPage.AIR_CONDITIONING).contains(value);
	}
	
	/**
	 * is Section 8 Combobox saved 
	 */
	public Boolean isSection8ComboBoxDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.SECTION_8_COMBOBOX, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.PropertiesPage.SECTION_8_COMBOBOX).contains(value);
	}
	
	/**
	 * input Transaction Costs  
	 */
	public void inputTransactionCosts(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.TRANSACTION_COSTS_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.TRANSACTION_COSTS_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input Special Assesments 
	 */
	public void inputSpecialAssesments(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.SPECIAL_ASSESMENTS_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.SPECIAL_ASSESMENTS_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input Other Expenses 
	 */
	public void inputOtherExpenses(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.OTHER_EXPENSES_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.OTHER_EXPENSES_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input Lease Up Marketing 
	 */
	public void inputLeaseUpMarketing(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.LEASE_UP_MARKETING_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.LEASE_UP_MARKETING_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input Zip 
	 */
	public void inputCreditLoss(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.CREDIT_LOSS_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.CREDIT_LOSS_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input Rehab Costs 
	 */
	public void inputRehabCosts(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.REHAB_COST_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.REHAB_COST_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input Landscaping Expense 
	 */
	public void inputLandscapingExpense(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.LANDSCAPING_EXPENSE_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.LANDSCAPING_EXPENSE_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input Vacancy Repairs Maintenance 
	 */
	public void inputVacancyRepairsMaintenance(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.VACANCY_REPAIRS_MAINTENANCE_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.VACANCY_REPAIRS_MAINTENANCE_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input Other Reserves 
	 */
	public void inputOtherReserves(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.OTHER_RESERVES_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.OTHER_RESERVES_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input APN 
	 */
	public void inputAPN(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.APN_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.APN_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input Total Basis 
	 */
	public void inputTotalBasis(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.TOTAL_BASIS_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.TOTAL_BASIS_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input MSA 
	 */
	public void inputMSA(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.MSA_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.MSA_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * select Pool
	 */
	public void selectPoolComboBox(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.POOL_TEXTBOX, timeWait);
		selectItemCombobox(driver, Interfaces.PropertiesPage.POOL_TEXTBOX, value);
	}
	
	//Check result
	
	/**
	 * is Transaction Costs displayed correctly
	 */
	public Boolean isTransactionCostsDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.TRANSACTION_COSTS_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.TRANSACTION_COSTS_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Special Assesments displayed correctly 
	 */
	public Boolean isSpecialAssesmentsDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.SPECIAL_ASSESMENTS_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.SPECIAL_ASSESMENTS_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Other Expenses displayed correctly 
	 */
	public Boolean isOtherExpensesDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.OTHER_EXPENSES_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.OTHER_EXPENSES_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Lease Up Marketing displayed correctly 
	 */
	public Boolean isLeaseUpMarketingDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.LEASE_UP_MARKETING_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.LEASE_UP_MARKETING_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Credit loss  displayed correctly 
	 */
	public Boolean isCreditLossDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.CREDIT_LOSS_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.CREDIT_LOSS_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Rehab Costs displayed correctly 
	 */
	public Boolean isRehabCostsDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.REHAB_COST_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.REHAB_COST_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Landscaping Expense displayed correctly 
	 */
	public Boolean isLandscapingExpenseDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.LANDSCAPING_EXPENSE_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.LANDSCAPING_EXPENSE_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Vacancy Repairs Maintenance displayed correctly 
	 */
	public Boolean isVacancyRepairsMaintenanceDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.VACANCY_REPAIRS_MAINTENANCE_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.VACANCY_REPAIRS_MAINTENANCE_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Other Reserves displayed correctly 
	 */
	public Boolean isOtherReservesDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.OTHER_RESERVES_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.OTHER_RESERVES_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is APN displayed correctly 
	 */
	public Boolean isAPNDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.APN_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.APN_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Total Basis displayed correctly 
	 */
	public Boolean isTotalBasisDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.TOTAL_BASIS_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.TOTAL_BASIS_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is MSA displayed correctly 
	 */
	public Boolean isMSADisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.MSA_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.PropertiesPage.MSA_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Pool displayed correctly
	 */
	public Boolean isPoolComboBoxDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.PropertiesPage.POOL_TEXTBOX, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.PropertiesPage.POOL_TEXTBOX).contains(value);
	}
	
	/**
	 * Get text in Property search
	 * 
	 * @param text
	 * @return
	 */
	public String getTextPropertiesSearch(String text) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_TEXT_PROPERTY_INFORMATION_SEARCH, text, timeWait);
		String itemName = getText(driver, Interfaces.PropertiesPage.DYNAMIC_TEXT_PROPERTY_INFORMATION_SEARCH, text);
		return itemName;
	}
	
	/**
	 * Get text in Property search
	 * 
	 * @param text
	 * @return
	 */
	public String getTextPropertiesSearch(String text, int i) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_TEXT_PROPERTY_INFORMATION_SEARCH, text, timeWait);
		String itemName = getText(driver, Interfaces.PropertiesPage.DYNAMIC_TEXT_PROPERTY_INFORMATION_SEARCH, text);
		if(i==1) return convertDate(itemName);
		else return itemName.replace("$", "").replace(".00", "");
	}
	
	public Boolean isAllValueDisplayOnSearchPropertiesTableB2R(String propertySearchAddress, String city, String state, String zip, String applicant, String loanIndex, String dateModified, String assetID,
			String active, String documents, String reviewed){
		waitForControl(driver, Interfaces.PropertiesPage.NO_PROPERTIES_FOUND_ALERT, 5);
		if(isControlDisplayed(driver, Interfaces.PropertiesPage.NO_PROPERTIES_FOUND_ALERT)) return false;
		if(!propertySearchAddress.contains(getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_AddressLine1"))) return false;
		if(!city.contains(getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_City"))) return false;
		if(!state.contains(getTextPropertiesSearch("DataCell FieldTypeState datacell_State"))) return false;
		if(!zip.contains(getTextPropertiesSearch("DataCell FieldTypeZipCode datacell_Zip"))) return false;
		if(!applicant.contains(getTextPropertiesSearch("DataCell FieldTypeForeignKey datacell_Borrower"))) return false;
		if(!loanIndex.contains(getTextPropertiesSearch("DataCell FieldTypeForeignKey datacell_LoanApplication"))) return false;
		if(!dateModified.contains(getTextPropertiesSearch("DataCell FieldTypeDate datacell_LastModifiedDate"))) return false;
		if(!assetID.contains(getTextPropertiesSearch("DataCell FieldTypeInteger datacell_LoanPropertyID"))) return false;
		if(!active.contains(getTextPropertiesSearch("DataCell FieldTypeBoolean datacell_IsActive"))) return false;
		if(!documents.contains(getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_DocumentsNew"))) return false;
		if(!reviewed.contains(getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_ReviewsNew"))) return false;
		return true;
	}
	
	public Boolean isAllValueDisplayOnSearchDuplicatedPropertiesTableB2R(String propertySearchAddress, String city, String state, String zip, String applicant, String loanIndex,
			String active){
		waitForControl(driver, Interfaces.PropertiesPage.NO_PROPERTIES_FOUND_ALERT, 5);
		if(isControlDisplayed(driver, Interfaces.PropertiesPage.NO_PROPERTIES_FOUND_ALERT)) return false;
		if(!propertySearchAddress.contains(getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_AddressLine1"))) return false;
		if(!city.contains(getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_City"))) return false;
		if(!state.contains(getTextPropertiesSearch("DataCell FieldTypeState datacell_State"))) return false;
		if(!zip.contains(getTextPropertiesSearch("DataCell FieldTypeZipCode datacell_Zip"))) return false;
		if(!applicant.contains(getTextPropertiesSearch("DataCell FieldTypeForeignKey datacell_Borrower"))) return false;
		if(!getTextPropertiesSearch("DataCell FieldTypeForeignKey datacell_LoanApplication").contains(loanIndex)) return false;
		if(!active.contains(getTextPropertiesSearch("DataCell FieldTypeBoolean datacell_IsActive"))) return false;
		return true;
	}
	
	public Boolean isAllValueDisplayOnSearchPropertiesTableCAF(String propertySearchAddress, String city, String state, String zip, String applicant, String loanIndex,
			String active, String documents, String reviewed){
		waitForControl(driver, Interfaces.PropertiesPage.NO_PROPERTIES_FOUND_ALERT, 5);
		if(isControlDisplayed(driver, Interfaces.PropertiesPage.NO_PROPERTIES_FOUND_ALERT)) return false;
		if(!propertySearchAddress.contains(getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_AddressLine1"))) return false;
		if(!city.contains(getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_City"))) return false;
		if(!state.contains(getTextPropertiesSearch("DataCell FieldTypeState datacell_State"))) return false;
		if(!zip.contains(getTextPropertiesSearch("DataCell FieldTypeZipCode datacell_Zip"))) return false;
		if(!applicant.contains(getTextPropertiesSearch("DataCell FieldTypeForeignKey datacell_Borrower"))) return false;
		if(!loanIndex.contains(getTextPropertiesSearch("DataCell FieldTypeForeignKey datacell_LoanApplication"))) return false;
		if(!active.contains(getTextPropertiesSearch("DataCell FieldTypeBoolean datacell_IsActive"))) return false;
		if(!documents.contains(getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_DocumentsNew"))) return false;
		if(!reviewed.contains(getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_ReviewsNew"))) return false;
		return true;
	}
	
	/**
	 * select Active radio button
	 */
	public void selectActiveRadioButton(String status) {
		waitForControl(driver,	Interfaces.PropertiesPage.SEARCH_BY_CITY_TEXTFIELD, timeWait);
		if(status.contains("Yes")) 
		click(driver, Interfaces.PropertiesPage.DYNAMIC_SEARCH_BY_ACTIVE_RADIO_BUTTON, "1");
		else if(status.contains("No")) click(driver, Interfaces.PropertiesPage.DYNAMIC_SEARCH_BY_ACTIVE_RADIO_BUTTON, "0");
		else click(driver, Interfaces.PropertiesPage.DYNAMIC_SEARCH_BY_ACTIVE_RADIO_BUTTON, "");
	}
	
	/**
	 * select Missing documents radio button
	 */
	public void selectMissingDocumentsRadioButton(String status) {
		waitForControl(driver,	Interfaces.PropertiesPage.SEARCH_BY_CITY_TEXTFIELD, timeWait);
		if(status.contains("Yes")) 
		click(driver, Interfaces.PropertiesPage.DYNAMIC_SEARCH_BY_MISSING_DOCUMENTS_RADIO_BUTTON, "1");
		else if(status.contains("No")) click(driver, Interfaces.PropertiesPage.DYNAMIC_SEARCH_BY_MISSING_DOCUMENTS_RADIO_BUTTON, "0");
		else click(driver, Interfaces.PropertiesPage.DYNAMIC_SEARCH_BY_MISSING_DOCUMENTS_RADIO_BUTTON, "");
	}
	
	/**
	 * select Reviewed documents radio button
	 */
	public void selectReviewedDocumentsRadioButton(String status) {
		waitForControl(driver,	Interfaces.PropertiesPage.SEARCH_BY_CITY_TEXTFIELD, timeWait);
		if(status.contains("Yes")) 
		click(driver, Interfaces.PropertiesPage.DYNAMIC_SEARCH_BY_REVIEWED_RADIO_BUTTON, "1");
		else if(status.contains("No")) click(driver, Interfaces.PropertiesPage.DYNAMIC_SEARCH_BY_REVIEWED_RADIO_BUTTON, "0");
		else click(driver, Interfaces.PropertiesPage.DYNAMIC_SEARCH_BY_REVIEWED_RADIO_BUTTON, "");
	}
	
	/**
	 * search property City
	 */
	public void searchByCity(String address, String value) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, address);
		type(driver, Interfaces.PropertiesPage.SEARCH_BY_CITY_TEXTFIELD, value);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search4').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		sleep();
	}
	
	/**
	 * search property City
	 */
	public void searchByZip(String address, String value) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, address);
		type(driver, Interfaces.PropertiesPage.SEARCH_BY_ZIP_TEXTFIELD, value);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search4').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		sleep();
	}
	
	/**
	 * search property Date modified
	 */
	public void searchByDateModified(String address, String value) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, address);
		type(driver, Interfaces.PropertiesPage.SEARCH_BY_DATE_MODIFIED_TEXTFIELD, value);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search4').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		sleep();
	}
	
	/**
	 * search property Asset ID
	 */
	public void searchByAssetID(String address, String value) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, address);
		type(driver, Interfaces.PropertiesPage.SEARCH_BY_ASSET_ID_TEXTFIELD, value);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search4').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		sleep();
	}
	
	/**
	 * search property State
	 */
	public void searchByState(String address, String value) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, address);
		click(driver, Interfaces.PropertiesPage.SEARCH_BY_STATE_DROPDOWN);
		sleep(1);
		click(driver, Interfaces.PropertiesPage.DYNAMIC_SEARCH_BY_STATE_DROPDOWN_OPTION, value);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search4').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		sleep();
	}
	
	/**
	 * search property Applicant
	 */
	public void searchByApplicant(String address, String value) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, address);
		selectItemCombobox(driver,	Interfaces.PropertiesPage.SEARCH_BY_APPLICANT_DROPDOWN, value);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search4').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		sleep();
	}
	
	/**
	 * search property Loan #
	 */
	public void searchByLoanApplication(String address, String value) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, address);
		selectItemCombobox(driver,	Interfaces.PropertiesPage.SEARCH_BY_LOAN_APPLICATION_DROPDOWN, value);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search4').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		sleep();
	}
	
	public void deleteAllPlaceholders(){
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTIES_PLACEHOLDER_CHECKBOX, timeWait);
		selectAllElementsInList(driver, Interfaces.PropertiesPage.PROPERTIES_PLACEHOLDER_CHECKBOX);
		clickSaveButton();
	}
	
	/**
	 * open Property Detail
	 */
	public void openPropertyDetail(String address) {
		searchAddress(address);
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_PROPERTIES_ADDRESS_LINK, address, timeWait);
		click(driver, Interfaces.PropertiesPage.DYNAMIC_PROPERTIES_ADDRESS_LINK, address);
		sleep(3);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}
	
	/**
	 * open Property Detail
	 */
	public int countNumberOfProperties(String address) {
		sleep(5);
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_PROPERTIES_ADDRESS_LINK, address, timeWait);
		return countElement(driver, Interfaces.PropertiesPage.DYNAMIC_PROPERTIES_ADDRESS_LINK, address);
	}
	
	/**
	 * open Property Detail
	 */
	public void openPropertyDocumentType(String documentsType) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_PROPERTY_DOCUMENT_TYPE, documentsType, timeWait);
		click(driver, Interfaces.PropertiesPage.DYNAMIC_PROPERTY_DOCUMENT_TYPE, documentsType);
		sleep(3);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}
	
	/**
	 * open Property Detail
	 */
	public void clickOnMarkAsReviewedButton() {
		waitForControl(driver, Interfaces.PropertiesPage.MARK_AS_REVIEWED_BUTTON, timeWait);
		click(driver, Interfaces.PropertiesPage.MARK_AS_REVIEWED_BUTTON);
		sleep(3);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}
	
	/**
	 * open Property Detail
	 */
	public void clickSearchDuplicatedAddressesButton() {
		waitForControl(driver, Interfaces.PropertiesPage.SEARCH_DUPLICATED_ADRESSES, timeWait);
		executeClick(driver, Interfaces.PropertiesPage.SEARCH_DUPLICATED_ADRESSES);
		sleep(10);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(15);
		}
	}
	
	/**
	 * get Uploaded method
	 * 
	 * @return method
	 */
	public String getUploadMethod(){
		waitForControl(driver, Interfaces.PropertiesPage.UPLOADED_METHOD, timeWait);
		return getText(driver, Interfaces.PropertiesPage.UPLOADED_METHOD);
	}
	
	/**
	 * get Added on time
	 * 
	 * @return time
	 */
	public String getAddedOnTime(){
		waitForControl(driver, Interfaces.PropertiesPage.ADDED_ON_TIME, timeWait);
		return getText(driver, Interfaces.PropertiesPage.ADDED_ON_TIME);
	}
	
	/**
	 * get Reviewed by
	 * 
	 * @return username
	 */
	public String getReviewedByUser(){
		waitForControl(driver, Interfaces.PropertiesPage.REVIEWED_BY, timeWait);
		return getText(driver, Interfaces.PropertiesPage.REVIEWED_BY);
	}
	
	/**
	 * get Reviewed time
	 * 
	 * @return time
	 */
	public String getReviewedTime(){
		waitForControl(driver, Interfaces.PropertiesPage.REVIEWED_ON_TIME, timeWait);
		return getText(driver, Interfaces.PropertiesPage.REVIEWED_ON_TIME);
	}
	
	private WebDriver driver;
	private String ipClient; 
	private final Stack<String> openWindowHandles = new Stack<String>();
}