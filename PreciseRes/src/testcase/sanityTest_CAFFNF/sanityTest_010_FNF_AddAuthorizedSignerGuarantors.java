package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFLoansPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_010_FNF_AddAuthorizedSignerGuarantors extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		loanName = "Edit Loan Auto " + getUniqueNumber();
		underwriter = "CAF FNF Lender";
		statusApplicant = "Underwriting";
		fullNameGuarantor = "Guarantor " + getUniqueNumber();
		percentageOwned = "99.90";
//		documentTypeGuarantor1 = "Bank Statements � Current Month";
//		documentTypeGuarantor2 = "Bank Statements � Last Month";
		locClosingDate = "04/12/2014";
	}

	@Test(groups = { "regression" }, description = "Applicant 15 - Login as Lender - Add to 'Authorized Signers / Guarantors' a signer with Holding Type - 'Guarantor'")
	public void AddGuarantors_01_Lender_AddGuarantor() {
		log.info("AddGuarantors_01 - Step 01. Login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);

		log.info("AddGuarantors_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("AddGuarantors_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("AddGuarantors_01 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("AddGuarantors_01 - Step 05: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("AddGuarantors_01 - Step 06: Input loan name");
		loanPage.inputRequiredData(loanName, locClosingDate);
		
		log.info("AddGuarantors_01 - Step 07: Click Save button");
		loanPage.clickOnSaveButton();

		log.info("AddGuarantors_01 - Step 08: Click on 'New' button");
		loanPage.clickOnNewGuarantorButton();

		log.info("AddGuarantors_01 - Step 09: Input correct 'Guarantor' information");
		loanPage.inputGuarantorInfo(fullNameGuarantor, percentageOwned);

		log.info("AddGuarantors_01 - Step 10: Click on 'Save' button");
		loanPage.clickOnSaveButton();

		log.info("AddGuarantors_01 - Step 11: Click on 'Back' button");
		loanPage.clickOnBackButton();

		log.info("VP: Verify that 'Full Name, Percentage Owned' is added successfully");
		verifyTrue(loanPage.isGuarantorSaved(fullNameGuarantor, percentageOwned));
		
		log.info("AddGuarantors_01 - Step 12: Click on 'Send to Initial Review' button");
		loanPage.clickOnSendToInitialReviewButton();
		
		log.info("AddGuarantors_01 - Step 13: Click on 'Send to Term Sheet Sent' button");
		loanPage.clickOnSendToTermSheetSentButton();
		
		log.info("AddGuarantors_01 - Step 14: Select 'CAF FNF Lender' value in Underwriter combobox");
		loanPage.selectUnderwriterCombobox(underwriter);
		
		log.info("AddGuarantors_01 - Step 15: Click on 'Send to Underwriting' button");
		loanPage.clickOnSendToUnderwritingButton();
		
		log.info("VP: Record Saved message displays");
		verifyTrue(loanPage.isRecordSavedMessageDisplays());
		
		log.info("VP: Verify that 'Underwriting' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull(statusApplicant));
	}

	@Test(groups = { "regression" }, description = "Applicant 16 - Login as Borower and check signer with 'Guarantor' are created")
	public void AddGuarantors_02_Borower_GuarantorAreCreated() {

		log.info("AddGuarantors_02 - Step 01: Login with Applicant");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);

		log.info("AddGuarantors_02 - Step 02: Open 'Loan Name'");
		loanPage = applicantsPage.openLoanNameInOpportunityTable(loanName);
		
		log.info("VP: Verify that 'Full Name, Percentage Owned' is added successfully");
		verifyTrue(loanPage.isGuarantorSaved(fullNameGuarantor, percentageOwned));
	}
	
//	@Test(groups = { "regression" }, description = "Applicant 17 - Login as Applicant - Check the right place holders for Guarantor are created")
//	public void AddGuarantors_03_Borrower_PlaceHolderForGuarantorAreCreated() {
//
//		log.info("VP: Placeholder displays with content 'Bank Statements � Current Month'");
//		verifyTrue(loanPage.isSavedNameMessageDisplayWithText(fullNameGuarantor + " - " + documentTypeGuarantor1));
//		
//		log.info("VP: Placeholder displays with content 'Bank Statements � Last Month'");
//		verifyTrue(loanPage.isSavedNameMessageDisplayWithText(fullNameGuarantor + " - " + documentTypeGuarantor2));
//	}
	
//	@Test(groups = { "regression" }, description = "Applicant 18 - Login as Lender - Make sure that Lender can see the document type are added when borrower add a Signer with Holding Type - 'Guarantor'")
//	public void AddGuarantors_04_Lender_PlaceHolderForGuarantorAreCreated() {
//
//		log.info("AddGuarantors_04 - Step 01. Logout and login with Lender");
//		loginPage = logout(driver, ipClient);
//		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
//	
//		log.info("AddGuarantors_04 - Step 02: Go to 'Applicant' page");
//		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
//	
//		log.info("AddGuarantors_04 - Step 03: Input 'Applicant Name' and click on 'Search' button");
//		applicantsPage.clickOnSearchButton(companyName);
//	
//		log.info("AddGuarantors_04 - Step 04: Click 'Applicant Name'");
//		applicantsPage.clickOnApplicantName();
//		
//		log.info("AddGuarantors_04 - Step 05: Open 'Loan Name'");
//		loanPage = applicantsPage.openLoanNameInOpportunityTable(loanName);
//		
//		log.info("VP: Placeholder displays with content 'Bank Statements � Current Month'");
//		verifyTrue(loanPage.isSavedNameMessageDisplayWithText(fullNameGuarantor + " - " + documentTypeGuarantor1));
//		
//		log.info("VP: Placeholder displays with content 'Bank Statements � Last Month'");
//		verifyTrue(loanPage.isSavedNameMessageDisplayWithText(fullNameGuarantor + " - " + documentTypeGuarantor2));
//	}
	
	@Test(groups = { "regression" }, description = "Applicant 19 - Login as Lender - Delete Guarantor successfully")
	public void AddGuarantors_05_Lender_DeleteGuarantor() {
		
		log.info("AddGuarantors_05 - Step 01. Logout and login with Lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("AddGuarantors_05 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("AddGuarantors_05 - Step 02: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("AddGuarantors_05 - Step 03: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("AddGuarantors_05 - Step 04: Open 'Loan Name'");
		loanPage = applicantsPage.openLoanNameInOpportunityTable(loanName);
		
		log.info("AddGuarantors_05 - Step 05: Delete Guarantor");
		loanPage.deleteGuarator(fullNameGuarantor);
		
		log.info("AddGuarantors_01 - Step 06: Click on 'Save' button");
		loanPage.clickOnSaveButton();
		
		log.info("VP: Record Saved message displays");
		verifyTrue(loanPage.isRecordSavedMessageDisplays());
		
		log.info("VP: Verify that Guarantor is deleted successfully");
		verifyFalse(loanPage.isGuarantorSaved(fullNameGuarantor, percentageOwned));
		
//		log.info("VP: Placeholder with content 'Bank Statements � Current Month' is deleted successfully");
//		verifyFalse(loanPage.isSavedNameMessageDisplayWithText(fullNameGuarantor + " - " + documentTypeGuarantor1));
//		
//		log.info("VP: Placeholder with content 'Bank Statements � Last Month' is deleted successfully");
//		verifyFalse(loanPage.isSavedNameMessageDisplayWithText(fullNameGuarantor + " - " + documentTypeGuarantor2));
	}
	
	@Test(groups = { "regression" }, description = "Applicant 20 - Login as Borower and check signer with 'Guarantor' are deleted")
	public void AddGuarantors_06_Borower_GuarantorAreDeleted() {

		log.info("AddGuarantors_06 - Step 01: Login with Applicant");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);

		log.info("AddGuarantors_06 - Step 02: Open 'Loan Name'");
		loanPage = applicantsPage.openLoanNameInOpportunityTable(loanName);
		
		log.info("VP: Verify that Guarantor is deleted successfully");
		verifyFalse(loanPage.isGuarantorSaved(fullNameGuarantor, percentageOwned));
		
//		log.info("VP: Placeholder with content 'Bank Statements � Current Month' is deleted successfully");
//		verifyFalse(loanPage.isSavedNameMessageDisplayWithText(fullNameGuarantor + " - " + documentTypeGuarantor1));
//		
//		log.info("VP: Placeholder with content 'Bank Statements � Last Month' is deleted successfully");
//		verifyFalse(loanPage.isSavedNameMessageDisplayWithText(fullNameGuarantor + " - " + documentTypeGuarantor2));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private FNFLoansPage loanPage;
	private String fullNameGuarantor, percentageOwned;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername,	FNFBorrowerPassword;
//	private String documentTypeGuarantor1, documentTypeGuarantor2;
	private String companyName, accountNumber, loanName, underwriter, statusApplicant, locClosingDate;
	}