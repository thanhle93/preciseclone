package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_005_B2R_CheckLoanInfoAfterUploadDataTape extends
		AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower New";
		propertiesFileName = "B2RDataTape_multi-unit.xlsx";
		parentAddress1 = "Oakhill";
		parentAddress2 = "Rosemarie";
		propertyAddress = "10105 Spring ST";
		propertyNumberProtab = 7;
		propertyNumberProtab2 = 6;
		city = "Palmdale";
		state = "CA";
		zip = "93551";
		parent1 = "Rosemarie";
		documentFileName = "datatest.pdf";
		documentType = "P3 - Purchase Contract/HUD-1 Settlement Stmt";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}

	@Test(groups = { "regression" }, description = "Loan41 - Check that parents are created properly")
	public void PropertyInfo_01_PropertyParents() {
		log.info("PropertyInfo_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender,false);

		log.info("PropertyInfo_01 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("PropertyInfo_01 - Step 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus,	loanName);
		
		log.info("PropertyInfo_01 - Step 04. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("PropertyInfo_01 - Step 05. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("VP: Check that parents are created properly");
		verifyTrue(loansPage.isParentsPropertyDisplayCorrectly(parentAddress1));
		verifyTrue(loansPage.isParentsPropertyDisplayCorrectly(parentAddress2));
	}

	@Test(groups = { "regression" }, description = "Loan42 - Add A Property To Parent")
	public void PropertyInfo_02_AddAPropertyToParent() {
		log.info("PropertyInfo_02 - Step 01. Open a property");
		propertiesPage = loansPage.openPropertyDetail(propertyAddress);

		log.info("PropertyInfo_02 - Step 02. Select property parent in Parent property dropdown");
		propertiesPage.selectParentPropertyItem(parent1);

		log.info("PropertyInfo_02 - Step 03. Click Save button");
		propertiesPage.clickSaveButton();

		log.info("PropertyInfo_02 - Step 04. Click Property List button");
		loansPage = propertiesPage.clickOnPropertyListButton();
		loansPage.searchAddress(propertyAddress);

		log.info("VP: Make sure the property is added as a unit to the parent");
		verifyTrue(loansPage.isPropertyAddedToParent(propertyAddress, parent1));
	}

	@Test(groups = { "regression" }, description = "Loan43 - Remove A Property From Parent")
	public void PropertyInfo_03_RemoveAPropertyFromParent() {
		log.info("PropertyInfo_03 - Step 01. Open a property");
		propertiesPage = loansPage.openPropertyDetail(propertyAddress);

		log.info("PropertyInfo_03 - Step 02. Select blank in Parent property dropdown");
		propertiesPage.selectParentPropertyItem("");

		log.info("PropertyInfo_03 - Step 03. Click Save button");
		propertiesPage.clickSaveButton();

		log.info("PropertyInfo_03 - Step 04. Click Property List button");
		loansPage = propertiesPage.clickOnPropertyListButton();
		loansPage.searchAddress(propertyAddress);

		log.info("VP: Make sure the unit is removed from the parent and became a regular property again");
		verifyFalse(loansPage.isPropertyAddedToParent(propertyAddress, parent1));
	}

	@Test(groups = { "regression" }, description = "Loan44 - Check value Property# in Loan Basic Detail tab")
	public void PropertyInfo_04_BasicDetaisPropertyField() {
		log.info("PropertyInfo_04 - Step 01. Open Basic detail page");
		loansPage.openBasicDetailTab();

		log.info("VP: The value in Property # filed equal the value of property number in property tab");
		verifyTrue(loansPage.isNumberOfPropertyDisplayCorrect(propertyNumberProtab));
	}

	@Test(groups = { "regression" }, description = "Loan51 - Add a document to a property")
	public void PropertyInfo_05_AddADocumentToProperty() {
		log.info("PropertyInfo_05 - Step 01. Open a property detail");
		loansPage.openPropertiesTab();
		propertiesPage = loansPage.openPropertyDetail(propertyAddress);

		log.info("PropertyInfo_05 - Step 02. Open Document type detail");
		propertiesPage.openDocumentTypeDetail(documentType);

		log.info("PropertyInfo_05 - Step 03. Select document type");
		propertiesPage.selectDocumentType(documentType);

		log.info("PropertyInfo_05 - Step 04. Uncheck private checkbox");
		propertiesPage.uncheckPrivateCheckbox();

		log.info("PropertyInfo_05 - Step 05. Upload document file");
		propertiesPage.uploadDocumentFile(documentFileName);

		log.info("PropertyInfo_05 - Step 06. Click save button");
		propertiesPage.clickSaveButton();

		log.info("PropertyInfo_05 - Step 07. Click Go To Property button");
		propertiesPage.clickGoToPropertyButton();

		log.info("VP: Document is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType,documentFileName));
	}

	@Test(groups = { "regression" }, description = "Loan52 - make inactive property")
	public void PropertyInfo_06_MakeInactiveProperty() {
		log.info("PropertyInfo_06 - Step 01. Click Mark Property inactive");
		propertiesPage.clickOnMarkPropertyInactiveButton();
		propertiesPage.switchToMakeInactiveFrame(driver);

		log.info("PropertyInfo_06 - Step 02. On popup, click Mark inactive Property");
		propertiesPage.clickOnMarkInactivePropertyButton();

		log.info("PropertyInfo_06 - Step 03. Click Property list");
		propertiesPage.switchToTopWindowFrame(driver);
		loansPage = propertiesPage.clickOnPropertyListButton();

		log.info("PropertyInfo_06 - Step 04. Search property address");
		loansPage.searchAddress(propertyAddress);

		log.info("VP: Verify  that the property is not there");
		verifyFalse(loansPage.isPropertyDisplay(propertyAddress, city, state,	zip));
	}

	@Test(groups = { "regression" }, description = "Loan53 - Check property number in bacsic tab after inactive")
	public void PropertyInfo_07_PropertyNumberOnBacsicDetailAfterInactive() {
		log.info("PropertyInfo_07 - Step 01. Open Basic detail page");
		loansPage.openBasicDetailTab();

		log.info("VP: The value in Property # filed equal the value of property number in property tab");
		verifyTrue(loansPage.isNumberOfPropertyDisplayCorrect(propertyNumberProtab2));
	}

	@Test(groups = { "regression" }, description = "Loan55 - Check property number in dashboard after inactive")
	public void PropertyInfo_08_PropertyNumberOnDashboardAfterInactive() {
		log.info("PropertyInfo_08 - Step 01. Go to dashboard page");
		homePage = loansPage.openHomePage(driver, ipClient);
		
		log.info("PropertyInfo_08 - Step 02. Select Loan Status combobox");
		homePage.selectDefaultLoanStatus(loanStatus);

		log.info("VP: The value in Property number on dashboard displays correctly");
		verifyTrue(homePage.isNumberPropertyOfLoanDisplayDashboard(loanName,propertyNumberProtab2 + ""));
	}

	@Test(groups = { "regression" }, description = "Loan57 - the property of inactive property doesn't appear in Document property tab")
	public void PropertyInfo_09_CheckPropertyDocumentAfterInactiveProperty() {
		log.info("PropertyInfo_09 - Step 01. Go loan page");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("PropertyInfo_09 - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("PropertyInfo_09 - Step 03. Open loan item");
		loansPage.openLoansDetailPage(loanName);

		log.info("PropertyInfo_09 - Step 04. Open Property Document tab");
		loansPage.openPropertyDocumentsTab();

		log.info("VP: Verify you can't find the document you loaded before to the property that you made inactive");
		verifyFalse(loansPage.isPropertyDocumentDisplay(propertyAddress, documentType));
	}

	@Test(groups = { "regression" }, description = "Loan56 - the property doesn't appear in the loan in Uploader")
	public void PropertyInfo_10_CheckPropertyDisplayInUploader() {

		log.info("PropertyInfo_10 - Step 01. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient,	uploaderPageUrl);

		log.info("PropertyInfo_10 - Step 02. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameLender,passwordLender);

		log.info("PropertyInfo_10 - Step 03. Select loans item");
		uploaderPage.clickChangeGroupedView();
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);

		log.info("PropertyInfo_10 - Step 04. Open Property folder");
		uploaderPage.clickPropertiesFolder();

		log.info("VP: Verify that the property doesn't appear in the loan");
		verifyFalse(uploaderPage.isPropertyDisplaysInLoan(propertyAddress));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private UploaderLoginPage uploaderLoginPage;
	private UploaderPage uploaderPage;

	private String usernameLender, passwordLender, parentAddress1;
	private String loanName, accountNumber, applicantName, propertiesFileName, loanStatus;
	private int propertyNumberProtab, propertyNumberProtab2;
	private String propertyAddress, uploaderPageUrl,parentAddress2, city, state, zip;
	private String documentType, parent1, documentFileName;
}