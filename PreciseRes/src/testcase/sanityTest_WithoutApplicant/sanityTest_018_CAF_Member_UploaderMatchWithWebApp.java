package sanityTest_WithoutApplicant;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_018_CAF_Member_UploaderMatchWithWebApp extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameMember = "auto-CAF-member2014";
		passwordMember = "change";
		loanName = "UploaderUdiTesting" + getUniqueNumber();
		loanStatus = Constant.LOAN_STATUS_CAF;
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
		propertiesFileName = "CAF Data Tape.xlsx";
		fileNameDocument1 = "datatest.pdf";
		documentType1 = "Lease Agreement";
		property1Name = "504 W Euclid";
		fileNameDocument2 = "Uploader_testing_document.txt";
		documentType2 = "Evidence of Insurance";
	}

	@Test(groups = { "regression" }, description = "UploaderMember01 - Add file then check if uploader matches the Web app")
	public void Uploader_01_AddFileAndCheckMatch() {
		log.info("Precondition 01. Login with Member");
		homePage = loginPage.loginAsLender(usernameMember, passwordMember, false);

		log.info("Precondition 02. Create new loans and get loans' id");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.createNewLoan(loanName, "", "", loanStatus);

		log.info("Precondition 03. Upload data tape");
		loansPage.openPropertiesTab();
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("Precondition 04. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);

		log.info("Precondition 05. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameMember, passwordMember);

		log.info("Uploader_01 - Step 01. Select loans item");
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);

		log.info("Uploader_01 - Step 02. Open Property folder");
		uploaderPage.clickPropertiesFolder();

		log.info("Uploader_01 - Step 03. Open Property item");
		uploaderPage.selectPropertyFolder(property1Name);

		log.info("Uploader_01 - Step 04. Upload document for a document type");
		uploaderPage.clickOnSelectNewFileIconByType(documentType1);
		uploaderPage.uploadFileOnUploader(fileNameDocument1);

		log.info("Uploader_01 - Step 05. Wait for upload complete");
		uploaderPage.waitForUploadCompleteOnUploader(fileNameDocument1);

		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(fileNameDocument1));

		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, documentType1));

		log.info("Uploader_01 - Step 06. Open web app");
		uploaderLoginPage = uploaderPage.logoutUploader();
		loginPage = uploaderLoginPage.openPreciseResUrlByBorrower(driver, ipClient, getPreciseResUrl());

		log.info("Uploader_01 - Step 07. Login with Member");
		homePage = loginPage.loginAsLender(usernameMember, passwordMember, false);

		log.info("Uploader_01 - Step 08. Open Loans detail");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		loansPage.openLoansDetailPage(loanName);

		log.info("Uploader_01 - Step 09. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Uploader_01 - Step 10. Open property detail");
		propertiesPage = loansPage.openPropertyDetail(property1Name);

		log.info("VP: Uploader matches the Web app");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, fileNameDocument1));
	}

	@Test(groups = { "regression" }, description = "UploaderMember02 - Update document type of file then check if uploader matches the Web app")
	public void Uploader_02_UpdateDocumentTypeAndCheckMatch() {
		log.info("Uploader_02 - Step 01. Click on logged user name on the top right corner");
		loansPage = propertiesPage.openLoansPage(driver, ipClient);

		log.info("Uploader_02 - Step 02. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);

		log.info("Uploader_02 - Step 03. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameMember, passwordMember);

		log.info("Uploader_02 - Step 04. Select loans item");
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);

		log.info("Uploader_02 - Step 05. Open Property folder");
		uploaderPage.clickPropertiesFolder();

		log.info("Uploader_02 - Step 06. Open Property item");
		uploaderPage.selectPropertyFolder(property1Name);

		log.info("Uploader_02 - Step 07. Update document type for document");
		uploaderPage.clickOnEditDocumentIcon(fileNameDocument1);

		log.info("Uploader_02 - Step 08. Select new document type");
		uploaderPage.selectDocumentType(documentType2);

		log.info("Uploader_02 - Step 09. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.clickYesButton();

		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, documentType2));

		log.info("Uploader_02 - Step 10. Open web app");
		uploaderLoginPage = uploaderPage.logoutUploader();
		loginPage = uploaderLoginPage.openPreciseResUrlByBorrower(driver, ipClient, getPreciseResUrl());

		log.info("Uploader_02 - Step 11. Login with Member");
		homePage = loginPage.loginAsLender(usernameMember, passwordMember, false);

		log.info("Uploader_02 - Step 12. Open Loans detail");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		loansPage.openLoansDetailPage(loanName);

		log.info("Uploader_02 - Step 13. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Uploader_02 - Step 14. Open property detail");
		propertiesPage = loansPage.openPropertyDetail(property1Name);

		log.info("VP: uploader matches the Web app");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType2, fileNameDocument1));
	}

	@Test(groups = { "regression" }, description = "UploaderMember03 - Upload new document type of file then check if uploader matches the Web app")
	public void Uploader_03_UploadNewFileForDocumentTypeAndCheckMatch() {
		log.info("Uploader_03 - Step 01. Click on logged user name on the top right corner");
		loansPage = propertiesPage.openLoansPage(driver, ipClient);

		log.info("Uploader_03 - Step 02. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);

		log.info("Uploader_03 - Step 03. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameMember, passwordMember);

		log.info("Uploader_03 - Step 04. Select loans item");
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);

		log.info("Uploader_03 - Step 05. Open Property folder");
		uploaderPage.clickPropertiesFolder();

		log.info("Uploader_03 - Step 06. Open Property item");
		uploaderPage.selectPropertyFolder(property1Name);

		log.info("Uploader_03 - Step 07. Update document type for document");
		uploaderPage.clickOnSelectNewFileIconByType(documentType2);
		uploaderPage.uploadFileOnUploader(fileNameDocument2);

		log.info("Uploader_03 - Step 08. Wait for upload complete");
		uploaderPage.waitForUploadCompleteOnUploader(fileNameDocument2);

		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(fileNameDocument2));

		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument2, documentType2));

		log.info("Uploader_03 - Step 09. Open web app");
		uploaderLoginPage = uploaderPage.logoutUploader();
		loginPage = uploaderLoginPage.openPreciseResUrlByBorrower(driver, ipClient, getPreciseResUrl());

		log.info("Uploader_03 - Step 10. Login with Member");
		homePage = loginPage.loginAsLender(usernameMember, passwordMember, false);

		log.info("Uploader_03 - Step 11. Open Loans detail");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		loansPage.openLoansDetailPage(loanName);

		log.info("Uploader_03 - Step 12. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Uploader_03 - Step 13. Open property detail");
		propertiesPage = loansPage.openPropertyDetail(property1Name);

		log.info("VP: uploader matches the Web app");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType2, fileNameDocument2));
	}

	@Test(groups = { "regression" }, description = "UploaderMember04 - DeleteDocumentAndCheckMatch")
	public void Uploader_04_DeleteDocumentAndCheckMatch() {
		log.info("Uploader_04 - Step 01. Click on logged user name on the top right corner");
		loansPage = propertiesPage.openLoansPage(driver, ipClient);

		log.info("Uploader_04 - Step 02. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);

		log.info("Uploader_04 - Step 03. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameMember, passwordMember);

		log.info("Uploader_04 - Step 04. Select loans item");
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);

		log.info("Uploader_04 - Step 05. Open Property folder");
		uploaderPage.clickPropertiesFolder();

		log.info("Uploader_04 - Step 06. Open Property item");
		uploaderPage.selectPropertyFolder(property1Name);

		log.info("Uploader_04 - Step 07. Delete document");
		uploaderPage.clickOnDeleteDocumentIcon(fileNameDocument2);

		log.info("Uploader_04 - Step 08. Click Yes button");
		uploaderPage.clickYesButton();

		log.info("VP: The document is uploaded successfully");
		verifyFalse(uploaderPage.isDocumentDisplay(fileNameDocument2));

		log.info("Uploader_04 - Step 09. Open web app");
		uploaderLoginPage = uploaderPage.logoutUploader();
		loginPage = uploaderLoginPage.openPreciseResUrlByBorrower(driver, ipClient, getPreciseResUrl());

		log.info("Uploader_04 - Step 10. Login with Member");
		homePage = loginPage.loginAsLender(usernameMember, passwordMember, false);

		log.info("Uploader_04 - Step 11. Open Loans detail");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		loansPage.openLoansDetailPage(loanName);

		log.info("Uploader_04 - Step 12. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Uploader_04 - Step 13. Open property detail");
		propertiesPage = loansPage.openPropertyDetail(property1Name);

		log.info("VP: uploader matches the Web app");
		verifyFalse(propertiesPage.isDocumentLoadedToDocumentType(documentType2, fileNameDocument2));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private String usernameMember, passwordMember, loanStatus;
	private String loanName, uploaderPageUrl, propertiesFileName, fileNameDocument2;
	private String property1Name, fileNameDocument1, documentType1, documentType2;
}