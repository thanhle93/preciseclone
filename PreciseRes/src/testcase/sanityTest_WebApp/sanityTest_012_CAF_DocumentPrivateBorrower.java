package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.DocumentsPage;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import common.AbstractTest;
import common.Common;
import common.Constant;

public class sanityTest_012_CAF_DocumentPrivateBorrower extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		applicantUsername = Constant.USERNAME_CAF_BORROWER;
		applicantPassword = Constant.PASSWORD_CAF_BORROWER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_CAF;
		applicantName = "Udi Dorner";
		propertiesFileName = "CAF Required.xlsx";
		address1 = "504 W Euclid";
		documentType1 = "Lease Agreement";
		borrowerName = "UdiTeamBorrower";
		documentSection1 = "General Loan Documents - General";
		documentSection2 = "Corporate Entity Documents - Borrower";
		documentType2 = "Org Chart";
		documentType3 = "Operating Agreement";
		fileName01 = "datatest.pdf";
		fileName02 = "datatest2.pdf";
		year = Common.getCommon().getCurrentYearOfWeek();
		submitVia = "Dorner";
	}

	@Test(groups = { "regression" }, description = "Document11 - Make 3 documents as private: [Property document - General loan document - Corporate entity document]")
	public void DocumentTest_11_MakeThreeDocumentsAsPrivateBorrower() {

		log.info("DocumentTest_11 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("DocumentTest_11 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_11 - Step 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, borrowerName, loanStatus);

		log.info("DocumentTest_11 - Step 05. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("DocumentTest_11 - Step 06. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("DocumentTest_11 - Step 07. Log out and login with applicant");
		loginPage = logout(driver, ipClient);
		loansPage = loginPage.loginAsBorrower(applicantUsername, applicantPassword, false);

		log.info("DocumentTest_11 - Step 08. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_11 - Step 09. Open detail loan item");
		loansPage.openLoansDetailPage(loanName);

		// Properties
		log.info("DocumentTest_11 - Step 09. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("DocumentTest_11 - Step 10. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("DocumentTest_11 - Step 11. Click New document button");
		propertiesPage.clickNewDocumentButton();

		log.info("DocumentTest_11 - Step 12. Select document type");
		propertiesPage.selectDocumentType(documentType1);

		log.info("DocumentTest_11 - Step 13. Check private checkbox");
		propertiesPage.checkPrivateCheckbox();

		log.info("DocumentTest_11 - Step 14. Upload document file 01");
		propertiesPage.uploadDocumentFile(fileName01);

		log.info("DocumentTest_11 - Step 15. Click Save button");
		propertiesPage.clickSaveButton();
		
		log.info("DocumentTest_11 - Step 16. Upload document file 02");
		propertiesPage.uploadDocumentFile(fileName02);

		log.info("DocumentTest_11 - Step 17. Click Save button");
		propertiesPage.clickSaveButton();
		
		log.info("VP: Document file name 01 is uploaded successfully");
		verifyTrue(propertiesPage.isDocumentHistoryLoadedToDocumentType(fileName01, submitVia, "/" + year));

		log.info("VP: Document file name 02 is uploaded successfully");
		verifyTrue(propertiesPage.isDocumentHistoryLoadedToDocumentType(fileName02, submitVia, "/" + year));
		
		// General
		log.info("DocumentTest_11 - Step 18. Open General Loan Documents tab");
		documentsPage = propertiesPage.openGeneralDocumentTab();

		log.info("DocumentTest_11 - Step 19. Click new button");
		documentsPage.clickNewButton();

		log.info("DocumentTest_11 - Step 20. Select section value");
		documentsPage.selectGeneralDocumentSection(documentSection1);

		log.info("DocumentTest_11 - Step 21. Select document type");
		documentsPage.selectGeneralDocumentType(documentType2);

		log.info("DocumentTest_11 - Step 22. Upload document file 01");
		documentsPage.uploadDocumentFile(fileName01);

		log.info("DocumentTest_11 - Step 23. Check private checkbox");
		documentsPage.checkPrivateCheckbox();

		log.info("DocumentTest_11 - Step 24. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("DocumentTest_11 - Step 25. Upload document file 02");
		documentsPage.uploadDocumentFile(fileName02);

		log.info("DocumentTest_11 - Step 26. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("VP: Document file name 01 is uploaded successfully");
		verifyTrue(documentsPage.isDocumentHistoryLoadedToDocumentType(fileName01, submitVia, "/" + year));

		log.info("VP: Document file name 02 is uploaded successfully");
		verifyTrue(documentsPage.isDocumentHistoryLoadedToDocumentType(fileName02, submitVia, "/" + year));

		// Corporate
		log.info("DocumentTest_11 - Step 27. Open Corporate Entity Documents tab");
		documentsPage.openCorporateEntityDocumentsTab();

		log.info("DocumentTest_11 - Step 28. Click new button");
		documentsPage.clickNewButton();

		log.info("DocumentTest_11 - Step 29. Select section value");
		documentsPage.selectGeneralDocumentSection(documentSection2);

		log.info("DocumentTest_11 - Step 30. Select document type");
		documentsPage.selectGeneralDocumentType(documentType3);

		log.info("DocumentTest_11 - Step 31. Upload document file 01");
		documentsPage.uploadDocumentFile(fileName01);

		log.info("DocumentTest_11 - Step 32. Check private checkbox");
		documentsPage.checkPrivateCheckbox();

		log.info("DocumentTest_11 - Step 33. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("DocumentTest_11 - Step 34. Upload document file 02");
		documentsPage.uploadDocumentFile(fileName02);

		log.info("DocumentTest_11 - Step 35. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("VP: Document file name 01 is uploaded successfully");
		verifyTrue(documentsPage.isDocumentHistoryLoadedToDocumentType(fileName01, submitVia, "/" + year));

		log.info("VP: Document file name 02 is uploaded successfully");
		verifyTrue(documentsPage.isDocumentHistoryLoadedToDocumentType(fileName02, submitVia, "/" + year));
	}

	@Test(groups = { "regression" }, description = "Document12 - Make sure that the borrower can see the 3 documents added above")
	public void DocumentTest_12_MakeSureBorrowerCanSeeTheDocumentBorrower() {

		log.info("DocumentTest_12 - Step 01. Open Loan tab");
		loansPage = documentsPage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_12 - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_12 - Step 03. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_12 - Step 04. Open Property tab");
		loansPage.openPropertiesTab();

		log.info("DocumentTest_12 - Step 05. Open property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("VP: Borrower can see the private document from Borrower");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, fileName02));

		log.info("DocumentTest_12 - Step 06. Open General Loan Documents tab");
		documentsPage = propertiesPage.openGeneralDocumentTab();

		log.info("DocumentTest_12 - Step 07. Click Document Type search");
		documentsPage.searchDocumentType(documentType2);

		log.info("VP: Document is loaded to loan item");
		verifyTrue(documentsPage.isDocumentUploadedToDocumentType(documentType2));

		log.info("DocumentTest_12 - Step 08. Open Corporate Entity Documents tab");
		documentsPage.openCorporateEntityDocumentsTab();

		log.info("DocumentTest_12 - Step 09. Click Document Type search");
		documentsPage.searchDocumentType(documentType3);

		log.info("VP: Document is loaded to loan item");
		verifyTrue(documentsPage.isDocumentUploadedToDocumentType(documentType3));
	}

	@Test(groups = { "regression" }, description = "Document14 - Make sure that the lender can not see the 3 documents private of Borrower")
	public void DocumentTest_14_MakeSureLenderCanNotSeeTheDocumentPrivateBorrower() {

		log.info("DocumentTest_14 - Step 01. Login with Lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("DocumentTest_14 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_14 - Step 03. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_14 - Step 04. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_14 - Step 05. Open Property tab");
		loansPage.openPropertiesTab();

		log.info("DocumentTest_14 - Step 06. Open property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("VP: Lender can not see the private document from Borrower");
		verifyFalse(propertiesPage.isDocumentLoadedToDocumentType(documentType1, fileName02));

		log.info("DocumentTest_14 - Step 07. Open General Loan Documents tab");
		documentsPage = propertiesPage.openGeneralDocumentTab();

		log.info("DocumentTest_14 - Step 08. Click Document Type search");
		documentsPage.searchDocumentType(documentType2);

		log.info("VP: Document is not loaded to loan item");
		verifyFalse(documentsPage.isDocumentUploadedToDocumentType(documentType2));

		log.info("DocumentTest_14 - Step 09. Open Corporate Entity Documents tab");
		documentsPage.openCorporateEntityDocumentsTab();

		log.info("DocumentTest_14 - Step 10. Click Document Type search");
		documentsPage.searchDocumentType(documentType3);

		log.info("VP: Document is not loaded to loan item");
		verifyFalse(documentsPage.isDocumentUploadedToDocumentType(documentType3));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private DocumentsPage documentsPage;
	private int year;
	private String usernameLender, passwordLender, address1, fileName01, fileName02, submitVia;
	private String loanName, accountNumber, applicantName, propertiesFileName, loanStatus;
	private String applicantUsername, applicantPassword, documentSection1, documentSection2, documentType1, documentType2, documentType3, borrowerName;
}