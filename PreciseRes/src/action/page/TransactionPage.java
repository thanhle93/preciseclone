package page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import PreciseRes.Interfaces;
import common.Common;

public class TransactionPage extends AbstractPage {
	public TransactionPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}

	/**
	 * click New Transaction button
	 */
	public void clickNewTransactionButton() {
		waitForControl(driver, Interfaces.TransactionPage.NEW_TRANSACTION_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('New').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * click Load Transaction button
	 */
	public void clickLoadTransactionButton() {
		waitForControl(driver, Interfaces.TransactionPage.LOAD_TRANSACTION_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('LoadLease').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * upload Transaction XML
	 */
	public void uploadTransactionXML(String xmlFileName) {
		waitForControl(driver, Interfaces.TransactionPage.TRANSACTION_CHOOSE_FILE_BUTTON, timeWait);
		doubleClick(driver, Interfaces.TransactionPage.TRANSACTION_CHOOSE_FILE_BUTTON);
		sleep(2);
		Common.getCommon().openFileForUpload(driver, xmlFileName);
		sleep(2);
		click(driver, Interfaces.TransactionPage.LOAD_BUTTON);
		sleep(5);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
	}

	/**
	 * check Load data message title display
	 * 
	 * @param N/A
	 */
	public boolean isBasicDetailMessageTitleDisplay(String messageContent) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_BASIC_DETAIL_MESSAGE_TITLE, messageContent, timeWait);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_BASIC_DETAIL_MESSAGE_TITLE, messageContent);
	}

	/**
	 * click Save Button
	 */
	public void clickSaveButton() {
		waitForControl(driver, Interfaces.TransactionPage.SAVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}
	
	/**
	 * click Make Inactive Button
	 */
	public void clickMakeInactiveButton() {
		waitForControl(driver, Interfaces.TransactionPage.MAKE_INACTIVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('SetInactive').click();");
		sleep();
		acceptJavascriptAlert(driver);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * click Make Active Button [Transaction Page - Property/ Property Documents Tab]
	 */
	public void clickMakeActiveButtonAtTransactionPropertyTab() {
		waitForControl(driver, Interfaces.TransactionPage.MAKE_ACTIVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('SetActive').click();");
		sleep();
		acceptJavascriptAlert(driver);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * click Make Active Button [Transaction Document - Lessee] tab
	 */
	public void clickMakeActiveButtonAtTransactionLesseeTab() {
		waitForControl(driver, Interfaces.TransactionPage.MAKE_ACTIVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('MakeDocumentActive').click();");
		sleep();
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * Select Lessee combobox
	 */
	public void selectLessee(String lessee) {
		waitForControl(driver, Interfaces.TransactionPage.LESSEE_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.LESSEE_DROPDOWN_LIST, lessee);
		sleep();
	}

	/**
	 * Check 'Lessee' saved successfully
	 */
	public boolean isLesseeSavedSuccessfully(String lessee) {
		waitForControl(driver, Interfaces.TransactionPage.LESSEE_DROPDOWN_LIST, timeWait);
		String realLessee = getItemSelectedCombobox(driver, Interfaces.TransactionPage.LESSEE_DROPDOWN_LIST);
		return realLessee.contains(lessee);
	}

	/**
	 * Select Workflow Status combobox
	 */
	public void selectWorkflowStatus(String workflowStatus) {
		waitForControl(driver, Interfaces.TransactionPage.WORKFLOW_STATUS_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.WORKFLOW_STATUS_DROPDOWN_LIST, workflowStatus);
		sleep();
	}

	/**
	 * Check 'Workflow Status' saved successfully
	 */
	public boolean isWorkflowStatusSavedSuccessfully(String workflowStatus) {
		waitForControl(driver, Interfaces.TransactionPage.WORKFLOW_STATUS_DROPDOWN_LIST, timeWait);
		String realWorkflowStatus = getItemSelectedCombobox(driver, Interfaces.TransactionPage.WORKFLOW_STATUS_DROPDOWN_LIST);
		return realWorkflowStatus.contains(workflowStatus);
	}

	/**
	 * Input Client ID
	 */
	public void inputClientID(String clientID) {
		waitForControl(driver, Interfaces.TransactionPage.CLIENT_ID_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.CLIENT_ID_TEXTBOX, clientID);
		sleep();
	}

	/**
	 * Check 'Client ID' saved successfully
	 */
	public boolean isClientIDSavedSuccessfully(String clientID) {
		waitForControl(driver, Interfaces.TransactionPage.CLIENT_ID_TEXTBOX, timeWait);
		String realClientID = getAttributeValue(driver, Interfaces.TransactionPage.CLIENT_ID_TEXTBOX, "value");
		return realClientID.contains(clientID);
	}

	/**
	 * Select Product Type
	 */
	public void selectProductType(String productType) {
		waitForControl(driver, Interfaces.TransactionPage.PRODUCT_TYPE_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.PRODUCT_TYPE_DROPDOWN_LIST, productType);
		sleep();
	}

	/**
	 * Check 'Product Type' saved successfully
	 */
	public boolean isProductTypeSavedSuccessfully(String productType) {
		waitForControl(driver, Interfaces.TransactionPage.PRODUCT_TYPE_DROPDOWN_LIST, timeWait);
		String realProductType = getItemSelectedCombobox(driver, Interfaces.TransactionPage.PRODUCT_TYPE_DROPDOWN_LIST);
		return realProductType.contains(productType);
	}

	/**
	 * Input Industry Type
	 */
	public void selectIndustryType(String industryType) {
		waitForControl(driver, Interfaces.TransactionPage.INDUSTRY_TYPE_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.INDUSTRY_TYPE_DROPDOWN_LIST, industryType);
		waitForControl(driver, Interfaces.TransactionPage.YES_BUTTON, timeWait);
		click(driver, Interfaces.TransactionPage.YES_BUTTON);
		sleep(3);
	}

	/**
	 * Check 'Industry Type' saved successfully
	 */
	public boolean isIndustryTypeSavedSuccessfully(String industryType) {
		waitForControl(driver, Interfaces.TransactionPage.INDUSTRY_TYPE_DROPDOWN_LIST, timeWait);
		String realProductType = getItemSelectedCombobox(driver, Interfaces.TransactionPage.INDUSTRY_TYPE_DROPDOWN_LIST);
		return realProductType.contains(industryType);
	}

	/**
	 * Input Portfolio Company
	 */
	public void inputPortfolioCompany(String portfolioCompany) {
		waitForControl(driver, Interfaces.TransactionPage.PORTFOLIO_COMPANY_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.PORTFOLIO_COMPANY_TEXTBOX, portfolioCompany);
		sleep();
	}

	/**
	 * Check 'Portfolio Company' saved successfully
	 */
	public boolean isPortfolioCompanySavedSuccessfully(String portfolioCompany) {
		waitForControl(driver, Interfaces.TransactionPage.PORTFOLIO_COMPANY_TEXTBOX, timeWait);
		String realPortfolioCompany = getAttributeValue(driver, Interfaces.TransactionPage.PORTFOLIO_COMPANY_TEXTBOX, "value");
		return realPortfolioCompany.contains(portfolioCompany);
	}

	/**
	 * Input Prorated Days
	 */
	public void inputProratedDays(String proratedDays) {
		waitForControl(driver, Interfaces.TransactionPage.PRORATED_DAYS_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.PRORATED_DAYS_TEXTBOX, proratedDays);
		sleep();
	}

	/**
	 * Check 'Prorated Days' saved successfully
	 */
	public boolean isProratedDaysSavedSuccessfully(String proratedDays) {
		waitForControl(driver, Interfaces.TransactionPage.PRORATED_DAYS_TEXTBOX, timeWait);
		String realProratedDays = getAttributeValue(driver, Interfaces.TransactionPage.PRORATED_DAYS_TEXTBOX, "value");
		return realProratedDays.contains(proratedDays);
	}

	/**
	 * Input Lease Expiration Date
	 */
	public void inputLeaseExpirationDate(String leaseExpiration) {
		waitForControl(driver, Interfaces.TransactionPage.LEASE_EXPIRATION_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.LEASE_EXPIRATION_DATE_TEXTBOX, leaseExpiration);
		sleep();
	}

	/**
	 * Check 'Lease Expiration Date' saved successfully
	 */
	public boolean isLeaseExpirationDateSavedSuccessfully(String leaseExpiration) {
		waitForControl(driver, Interfaces.TransactionPage.LEASE_EXPIRATION_DATE_TEXTBOX, timeWait);
		String realLeaseExpirationDate = getAttributeValue(driver, Interfaces.TransactionPage.LEASE_EXPIRATION_DATE_TEXTBOX, "value");
		return realLeaseExpirationDate.contains(leaseExpiration);
	}
	
	/**
	 * Get 'Lease Expiration Date'
	 */
	public String  getLeaseExpirationDate(){
		waitForControl(driver, Interfaces.TransactionPage.LEASE_EXPIRATION_DATE_TEXTBOX, timeWait);
		String realLeaseExpirationDate = getAttributeValue(driver, Interfaces.TransactionPage.LEASE_EXPIRATION_DATE_TEXTBOX, "value");
		return realLeaseExpirationDate;
	}

	/**
	 * Input Security Deposit Amount
	 */
	public void inputSercurityDepositAmount(String sercurityDepositAmount) {
		waitForControl(driver, Interfaces.TransactionPage.SERCURITY_DEPOSIT_AMOUNT_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SERCURITY_DEPOSIT_AMOUNT_TEXTBOX, sercurityDepositAmount);
		sleep();
	}

	/**
	 * Check 'Security Deposit Amount' saved successfully
	 */
	public boolean isSercurityDepositAmountSavedSuccessfully(String sercurityDepositAmount) {
		waitForControl(driver, Interfaces.TransactionPage.SERCURITY_DEPOSIT_AMOUNT_TEXTBOX, timeWait);
		String realSercurityDepositAmount = getAttributeValue(driver, Interfaces.TransactionPage.SERCURITY_DEPOSIT_AMOUNT_TEXTBOX, "value");
		return realSercurityDepositAmount.contains(sercurityDepositAmount);
	}

	/**
	 * select ACH Payment
	 */
	public void selectACHPayment() {
		waitForControl(driver, Interfaces.TransactionPage.ACH_PAYMENT_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.ACH_PAYMENT_CHECKBOX);
		sleep();
	}

	/**
	 * Input Original Commencement Date
	 */
	public void inputOriginalCommencementDate(String originalCommencementDate) {
		waitForControl(driver, Interfaces.TransactionPage.ORIGINAL_COMMENCE_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.ORIGINAL_COMMENCE_DATE_TEXTBOX, originalCommencementDate);
		sleep();
	}

	/**
	 * Check 'Original Commencement Date' saved successfully
	 */
	public boolean isOriginalCommencementDateSavedSuccessfully(String originalCommencementDate) {
		waitForControl(driver, Interfaces.TransactionPage.ORIGINAL_COMMENCE_DATE_TEXTBOX, timeWait);
		String realOriginalCommencementDate = getAttributeValue(driver, Interfaces.TransactionPage.ORIGINAL_COMMENCE_DATE_TEXTBOX, "value");
		return realOriginalCommencementDate.contains(originalCommencementDate);
	}
	
	/**
	 * Check 'Original Commencement Date' saved successfully
	 */
	public String getOriginalCommencementDate() {
		waitForControl(driver, Interfaces.TransactionPage.ORIGINAL_COMMENCE_DATE_TEXTBOX, timeWait);
		String realOriginalCommencementDate = getAttributeValue(driver, Interfaces.TransactionPage.ORIGINAL_COMMENCE_DATE_TEXTBOX, "value");
		return realOriginalCommencementDate;
	}

	/**
	 * select Corporate Financial Statements Required
	 */
	public void selectCorporateFinancialStatementsRequired() {
		waitForControl(driver, Interfaces.TransactionPage.CORPORATE_FINANCIAL_STATEMENT_REQUIRED_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.CORPORATE_FINANCIAL_STATEMENT_REQUIRED_CHECKBOX);
		sleep();
	}

	/**
	 * select Unit Financial Statements Required
	 */
	public void selectUnitFinancialStatementsRequired() {
		waitForControl(driver, Interfaces.TransactionPage.UNIT_FINANCIAL_STATEMENT_REQUIRED_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.UNIT_FINANCIAL_STATEMENT_REQUIRED_CHECKBOX);
		sleep();
	}

	/**
	 * select Unit Sales Only Required
	 */
	public void selectUnitSalesOnlyRequired() {
		waitForControl(driver, Interfaces.TransactionPage.UNIT_SALE_ONLY_REQUIRED_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.UNIT_SALE_ONLY_REQUIRED_CHECKBOX);
		sleep();
	}

	/**
	 * select Contingent Rent
	 */
	public void selectContingentRent() {
		waitForControl(driver, Interfaces.TransactionPage.CONTINGENT_RENT_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.CONTINGENT_RENT_CHECKBOX);
		sleep();
	}

	/**
	 * Input Contingent Rent Description
	 */
	public void inputContingentRentDescription(String contingentRentDescription) {
		waitForControl(driver, Interfaces.TransactionPage.CONTINGENT_RENT_DESCRIPTION_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.CONTINGENT_RENT_DESCRIPTION_TEXTBOX, contingentRentDescription);
		sleep();
	}

	/**
	 * Check 'Contingent Rent Description' saved successfully
	 */
	public boolean isContingentRentDescriptionSavedSuccessfully(String contingentRentDescription) {
		waitForControl(driver, Interfaces.TransactionPage.CONTINGENT_RENT_DESCRIPTION_TEXTBOX, timeWait);
		String realContingentRentDescription = getAttributeValue(driver, Interfaces.TransactionPage.CONTINGENT_RENT_DESCRIPTION_TEXTBOX, "value");
		return realContingentRentDescription.contains(contingentRentDescription);
	}

	/**
	 * Select Breakover
	 */
	public void selectBreakover(String breakover) {
		waitForControl(driver, Interfaces.TransactionPage.BREAKOVER_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.BREAKOVER_DROPDOWN_LIST, breakover);
		sleep();
	}

	/**
	 * Check 'Breakover' saved successfully
	 */
	public boolean isBreakoverSavedSuccessfully(String breakover) {
		waitForControl(driver, Interfaces.TransactionPage.BREAKOVER_DROPDOWN_LIST, timeWait);
		String realBreakover = getItemSelectedCombobox(driver, Interfaces.TransactionPage.BREAKOVER_DROPDOWN_LIST);
		return realBreakover.contains(breakover);
	}

	/**
	 * check Renewal Option
	 */
	public void checkRenewalOption() {
		waitForControl(driver, Interfaces.TransactionPage.RENEWAL_OPTION_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.RENEWAL_OPTION_CHECKBOX);
		sleep();
	}
	
	/**
	 * uncheck Renewal Option
	 */
	public void uncheckRenewalOption() {
		waitForControl(driver, Interfaces.TransactionPage.RENEWAL_OPTION_CHECKBOX, timeWait);
		uncheckTheCheckbox(driver, Interfaces.TransactionPage.RENEWAL_OPTION_CHECKBOX);
		sleep();
	}

	/**
	 * select Cross-Defaulted
	 */
	public void selectCrossDefaulted() {
		waitForControl(driver, Interfaces.TransactionPage.CROSS_DEFAULTED_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.CROSS_DEFAULTED_CHECKBOX);
		sleep();
	}

	/**
	 * select Escrow Rights
	 */
	public void selectEscrowRights() {
		waitForControl(driver, Interfaces.TransactionPage.ESCROW_RIGHTS_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.ESCROW_RIGHTS_CHECKBOX);
		sleep();
	}

	/**
	 * select STORE Preferred Contract Terms
	 */
	public void selectStorePreferredContracTerms() {
		waitForControl(driver, Interfaces.TransactionPage.STORE_PREFERRED_CONTRACTS_TERMS, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.STORE_PREFERRED_CONTRACTS_TERMS);
		sleep();
	}

	/**
	 * Input Salesforce Opportunity ID
	 */
	public void inputSalesforceOpportunityID(String salesforceOpportunityID) {
		waitForControl(driver, Interfaces.TransactionPage.SALEFORCE_OPPORTUNITY_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SALEFORCE_OPPORTUNITY_TEXTBOX, salesforceOpportunityID);
		sleep();
	}

	/**
	 * Check 'Salesforce Opportunity ID' saved successfully
	 */
	public boolean isSalesforceOpportunityIDSavedSuccessfully(String salesforceOpportunityID) {
		waitForControl(driver, Interfaces.TransactionPage.SALEFORCE_OPPORTUNITY_TEXTBOX, timeWait);
		String realSalesforceOpportunityID = getAttributeValue(driver, Interfaces.TransactionPage.SALEFORCE_OPPORTUNITY_TEXTBOX, "value");
		return realSalesforceOpportunityID.contains(salesforceOpportunityID);
	}

	/**
	 * Input Deal ID
	 */
	public void inputDealID(String dealID) {
		waitForControl(driver, Interfaces.TransactionPage.DEAL_ID_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.DEAL_ID_TEXTBOX, dealID);
		sleep();
	}

	/**
	 * Check 'DealID' saved successfully
	 */
	public boolean isDealIDSavedSuccessfully(String dealID) {
		waitForControl(driver, Interfaces.TransactionPage.DEAL_ID_TEXTBOX, timeWait);
		String realDealID = getAttributeValue(driver, Interfaces.TransactionPage.DEAL_ID_TEXTBOX, "value");
		return realDealID.contains(dealID);
	}

	/**
	 * select Existing Lease Prior to Purchase
	 */
	public void selectExistingLeasePriorToPurchase() {
		waitForControl(driver, Interfaces.TransactionPage.EXISTING_LEASE_PRIOR_TO_PURCHASE_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.EXISTING_LEASE_PRIOR_TO_PURCHASE_CHECKBOX);
		sleep();
	}

	/**
	 * Input Company name
	 */
	public void inputCompanyName(String company) {
		waitForControl(driver, Interfaces.TransactionPage.COMPANY_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.COMPANY_TEXTBOX, company);
		sleep();
	}

	/**
	 * Check 'Company name' saved successfully
	 */
	public boolean isCompanyNameSavedSuccessfully(String company) {
		waitForControl(driver, Interfaces.TransactionPage.COMPANY_TEXTBOX, timeWait);
		String realCompanyName = getAttributeValue(driver, Interfaces.TransactionPage.COMPANY_TEXTBOX, "value");
		return realCompanyName.contains(company);
	}

	/**
	 * Input Store Entity
	 */
	public void inputStoreEntity(String storeEntity) {
		waitForControl(driver, Interfaces.TransactionPage.STORE_ENTITY_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.STORE_ENTITY_TEXTBOX, storeEntity);
		sleep();
	}

	/**
	 * Check 'Store Entity' saved successfully
	 */
	public boolean isStoreEntitySavedSuccessfully(String storeEntity) {
		waitForControl(driver, Interfaces.TransactionPage.STORE_ENTITY_TEXTBOX, timeWait);
		String realStoreEntity = getAttributeValue(driver, Interfaces.TransactionPage.STORE_ENTITY_TEXTBOX, "value");
		return realStoreEntity.contains(storeEntity);
	}

	/**
	 * Input Price
	 */
	public void inputPrice(String price) {
		waitForControl(driver, Interfaces.TransactionPage.PRICE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.PRICE_TEXTBOX, price);
		sleep();
	}

	/**
	 * Check 'Price' saved successfully
	 */
	public boolean isPriceSavedSuccessfully(String price) {
		waitForControl(driver, Interfaces.TransactionPage.PRICE_TEXTBOX, timeWait);
		String realPrice = getAttributeValue(driver, Interfaces.TransactionPage.PRICE_TEXTBOX, "value");
		return realPrice.contains(price);
	}

	/**
	 * Input First Payment Date
	 */
	public void inputFirstPaymentDate(String firstPayment) {
		waitForControl(driver, Interfaces.TransactionPage.FIRST_PAYMENT_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.FIRST_PAYMENT_DATE_TEXTBOX, firstPayment);
		sleep();
	}

	/**
	 * Check 'First Payment Date' saved successfully
	 */
	public boolean isFirstPaymentDateSavedSuccessfully(String firstPayment) {
		waitForControl(driver, Interfaces.TransactionPage.FIRST_PAYMENT_DATE_TEXTBOX, timeWait);
		String realFirstPaymentDate = getAttributeValue(driver, Interfaces.TransactionPage.FIRST_PAYMENT_DATE_TEXTBOX, "value");
		return realFirstPaymentDate.contains(firstPayment);
	}

	/**
	 * Input Term
	 */
	public void inputTerm(String term) {
		waitForControl(driver, Interfaces.TransactionPage.TERM_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.TERM_TEXTBOX, term);
		sleep();
	}

	/**
	 * Check 'Term' saved successfully
	 */
	public boolean isTermSavedSuccessfully(String term) {
		waitForControl(driver, Interfaces.TransactionPage.TERM_TEXTBOX, timeWait);
		String realTerm = getAttributeValue(driver, Interfaces.TransactionPage.TERM_TEXTBOX, "value");
		return realTerm.contains(term);
	}

	/**
	 * Input Cap Rate
	 */
	public void inputCapRate(String capRate) {
		waitForControl(driver, Interfaces.TransactionPage.CAP_RATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.CAP_RATE_TEXTBOX, capRate);
		sleep();
	}

	/**
	 * Check 'Cap Rate' saved successfully
	 */
	public boolean isCapRateSavedSuccessfully(String capRate) {
		waitForControl(driver, Interfaces.TransactionPage.CAP_RATE_TEXTBOX, timeWait);
		String realCapRate = getAttributeValue(driver, Interfaces.TransactionPage.CAP_RATE_TEXTBOX, "value");
		return realCapRate.contains(capRate);
	}

	/**
	 * Input Initial Rent
	 */
	public void inputInitialRent(String initialRent) {
		waitForControl(driver, Interfaces.TransactionPage.INITIAL_RENT_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.INITIAL_RENT_TEXTBOX, initialRent);
		sleep();
	}

	/**
	 * Check 'Initial Rent' saved successfully
	 */
	public boolean isInitialRentSavedSuccessfully(String initialRent) {
		waitForControl(driver, Interfaces.TransactionPage.INITIAL_RENT_TEXTBOX, timeWait);
		String realInitialRent = getAttributeValue(driver, Interfaces.TransactionPage.INITIAL_RENT_TEXTBOX, "value");
		return realInitialRent.contains(initialRent);
	}

	/**
	 * Input Contract Commencement Date
	 */
	public void inputContractCommencementDate(String contractCommencementDate) {
		waitForControl(driver, Interfaces.TransactionPage.CONTRACT_COMMENCEMENT_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.CONTRACT_COMMENCEMENT_DATE_TEXTBOX, contractCommencementDate);
		click(driver, Interfaces.TransactionPage.INITIAL_RENT_TEXTBOX);
		sleep();
	}

	/**
	 * Check 'Contract Commencement Date' saved successfully
	 */
	public boolean isContractCommencementDateSavedSuccessfully(String contractCommencementDate) {
		waitForControl(driver, Interfaces.TransactionPage.CONTRACT_COMMENCEMENT_DATE_TEXTBOX, timeWait);
		String realContractCommencementDate = getAttributeValue(driver, Interfaces.TransactionPage.CONTRACT_COMMENCEMENT_DATE_TEXTBOX, "value");
		return realContractCommencementDate.contains(contractCommencementDate);
	}

	/**
	 * Select Corporate Level Frequency
	 */
	public void selectCorporateLevelFrequency(String corporateLevelFrequency) {
		waitForControl(driver, Interfaces.TransactionPage.CORPORATE_LEVEL_FREQUENCY_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.CORPORATE_LEVEL_FREQUENCY_DROPDOWN_LIST, corporateLevelFrequency);
		sleep();
	}

	/**
	 * Check 'Corporate Level Frequency' saved successfully
	 */
	public boolean isCorporateLevelFrequencySavedSuccessfully(String corporateLevelFrequency) {
		waitForControl(driver, Interfaces.TransactionPage.CORPORATE_LEVEL_FREQUENCY_DROPDOWN_LIST, timeWait);
		String realCorporateLevelFrequency = getItemSelectedCombobox(driver, Interfaces.TransactionPage.CORPORATE_LEVEL_FREQUENCY_DROPDOWN_LIST);
		return realCorporateLevelFrequency.contains(corporateLevelFrequency);
	}

	/**
	 * select Corporate is Unit Proxy
	 */
	public void selectCorporateUnitProxy() {
		waitForControl(driver, Interfaces.TransactionPage.CORPORATE_UNIT_PROXY_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.CORPORATE_UNIT_PROXY_CHECKBOX);
		sleep();
	}

	/**
	 * Select Unit Level Financials Frequency
	 */
	public void selectUnitLevelFinancialsFrequency(String unitLevelFinancialsFrequency) {
		waitForControl(driver, Interfaces.TransactionPage.UNIT_LEVEL_FINANCIALS_FREQUENCY_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.UNIT_LEVEL_FINANCIALS_FREQUENCY_DROPDOWN_LIST, unitLevelFinancialsFrequency);
		sleep();
	}

	/**
	 * Check 'Unit Level Financials Frequency' saved successfully
	 */
	public boolean isUnitLevelFinancialsFrequencySavedSuccessfully(String unitLevelFinancialsFrequency) {
		waitForControl(driver, Interfaces.TransactionPage.UNIT_LEVEL_FINANCIALS_FREQUENCY_DROPDOWN_LIST, timeWait);
		String realUnitLevelFinancialsFrequency = getItemSelectedCombobox(driver, Interfaces.TransactionPage.UNIT_LEVEL_FINANCIALS_FREQUENCY_DROPDOWN_LIST);
		return realUnitLevelFinancialsFrequency.contains(unitLevelFinancialsFrequency);
	}

	/**
	 * Input Period End
	 */
	public void inputPeriodEnd(String periodEnd) {
		waitForControl(driver, Interfaces.TransactionPage.PERIOD_END_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.PERIOD_END_TEXTBOX, periodEnd);
		sleep();
	}

	/**
	 * Check 'Period End' saved successfully
	 */
	public boolean isPeriodEndSavedSuccessfully(String contractCommencementDate) {
		waitForControl(driver, Interfaces.TransactionPage.PERIOD_END_TEXTBOX, timeWait);
		String realPeriodEnd = getAttributeValue(driver, Interfaces.TransactionPage.PERIOD_END_TEXTBOX, "value");
		return realPeriodEnd.contains(contractCommencementDate);
	}

	/**
	 * Input Days to Report
	 */
	public void inputDaysToReport(String daysToReport) {
		waitForControl(driver, Interfaces.TransactionPage.DAYS_TO_REPORT_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.DAYS_TO_REPORT_TEXTBOX, daysToReport);
		sleep();
	}

	/**
	 * Check 'Days to Report' saved successfully
	 */
	public boolean isDaysToReportSavedSuccessfully(String daysToReport) {
		waitForControl(driver, Interfaces.TransactionPage.DAYS_TO_REPORT_TEXTBOX, timeWait);
		String realDaysToReport = getAttributeValue(driver, Interfaces.TransactionPage.DAYS_TO_REPORT_TEXTBOX, "value");
		return realDaysToReport.contains(daysToReport);
	}

	/**
	 * select Franchise Agreement Required
	 */
	public void selectFranchiseAgreementRequired() {
		waitForControl(driver, Interfaces.TransactionPage.FRANCHISE_AGREEMENT_REQUIRED_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.FRANCHISE_AGREEMENT_REQUIRED_CHECKBOX);
		sleep();
	}

	/**
	 * select Guarantor
	 */
	public void selectGuarantor() {
		waitForControl(driver, Interfaces.TransactionPage.GUARANTOR_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.GUARANTOR_CHECKBOX);
		sleep();
	}

	/**
	 * Input Escrow Description
	 */
	public void inputEscrowDescription(String escrowDescription) {
		waitForControl(driver, Interfaces.TransactionPage.ESCROW_DESCRIPTION_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.ESCROW_DESCRIPTION_TEXTBOX, escrowDescription);
		sleep();
	}

	/**
	 * Check 'Escrow Description' saved successfully
	 */
	public boolean isEscrowDescriptionSavedSuccessfully(String escrowDescription) {
		waitForControl(driver, Interfaces.TransactionPage.ESCROW_DESCRIPTION_TEXTBOX, timeWait);
		String realEscrowDescription = getAttributeValue(driver, Interfaces.TransactionPage.ESCROW_DESCRIPTION_TEXTBOX, "value");
		return realEscrowDescription.contains(escrowDescription);
	}

	/**
	 * check Dynamic Alert message title display
	 * 
	 * @param id
	 * @param messageContent
	 * @return
	 */
	public boolean isDynamicAlertMessageTitleDisplay(String id, String messageContent) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_FIELD_ALERT_MESSAGE_TITLE, id, messageContent, 10);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_FIELD_ALERT_MESSAGE_TITLE, id, messageContent);
	}

	/**
	 * search Transaction by Company name
	 */
	public void searchTransactionByCompanyName(String companyName) {
		waitForControl(driver, Interfaces.TransactionPage.SEARCH_COMPANY_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SEARCH_COMPANY_NAME_TEXTBOX, companyName);
		sleep();
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * search Transaction by ClientID
	 */
	public void searchTransactionByClientID(String clientID) {
		waitForControl(driver, Interfaces.TransactionPage.SEARCH_CLIENT_ID_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SEARCH_CLIENT_ID_TEXTBOX, clientID);
		sleep();
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * search Transaction by Active/Inactive
	 */
	public void searchTransactionByActiveOrInactive(String clientID, String status) {
		waitForControl(driver, Interfaces.TransactionPage.SEARCH_CLIENT_ID_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SEARCH_CLIENT_ID_TEXTBOX, clientID);
		sleep(1);
		click(driver, Interfaces.TransactionPage.DYNAMIC_ACTIVE_RADIO_BUTTON, status);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * Check Transaction checkbox in Transaction table
	 */
	public void isSelectedTransactionCheckbox(String companyName) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_TRANSACTION_CHECKBOX_IN_TRANSACTION_TABLE, companyName, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.DYNAMIC_TRANSACTION_CHECKBOX_IN_TRANSACTION_TABLE, companyName);
		sleep();
	}

	/**
	 * open Transaction detail page
	 */
	public void openTransactionDetailPage(String companyName) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_TRANSACTION_ITEM, companyName, timeWait);
		click(driver, Interfaces.TransactionPage.DYNAMIC_TRANSACTION_ITEM, companyName);
		sleep(1);
	}

	/**
	 * Check Transaction display on search Transaction page
	 */
	public boolean isTransactionDisplayOnSearchTransactionPage(String companyName, String clientID, String active) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_SEARCH_TRANSACTION_ITEM, companyName, clientID, active, timeWait);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_SEARCH_TRANSACTION_ITEM, companyName, clientID, active);
	}

	/**
	 * Check Property number of Transaction display on search Transaction page
	 */
	public boolean isPropertyNumberDisplayOnSearchTransactionPage(String companyName, String propertyNumber) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_PROPERTY_NUMBER_ON_SEARCH_TRANSACTION_ITEM, companyName, propertyNumber, timeWait);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_PROPERTY_NUMBER_ON_SEARCH_TRANSACTION_ITEM, companyName, propertyNumber);
	}

	/**
	 * click Download CSV image
	 */
	public void clickOnDownloadCSVImage() {
		waitForControl(driver, Interfaces.TransactionPage.DOWNLOAD_CSV_IMAGE, timeWait);
		click(driver, Interfaces.TransactionPage.DOWNLOAD_CSV_IMAGE);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
		sleep(5);
	}

	/**
	 * click Transaction Reports button
	 */
	public void clickOnTransactionReportsButton() {
		waitForControl(driver, Interfaces.TransactionPage.TRANSACTION_REPORTS_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('LeaseReports').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
		sleep();
	}

	/**
	 * click New External Entities button
	 */
	public void clickNewExternalEntitiesButton() {
		waitForControl(driver, Interfaces.TransactionPage.INTERNAL_ENTITIES_NEW_BUTTON, timeWait);
		click(driver, Interfaces.TransactionPage.INTERNAL_ENTITIES_NEW_BUTTON);
		sleep();
	}

	/**
	 * Open Date click Create New Entity Button
	 */
	public void clickNewEntityButton() {
		waitForControl(driver, Interfaces.TransactionPage.CREATE_NEW_ENTITY_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('NewEntity').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * click Create New Login Button
	 */
	public void clickNewLoginButton() {
		waitForControl(driver, Interfaces.TransactionPage.CREATE_NEW_LOGIN_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('NewLogin').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * Select Entity Type
	 */
	public void selectEntityType(String entityType) {
		waitForControl(driver, Interfaces.TransactionPage.ENTITY_TYPE_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.ENTITY_TYPE_DROPDOWN_LIST, entityType);
		sleep();
	}

	/**
	 * Select Entity Classification
	 */
	public void selectEntityClassification(String entityClassification) {
		waitForControl(driver, Interfaces.TransactionPage.ENTITY_CLASSIFICATION_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.ENTITY_CLASSIFICATION_DROPDOWN_LIST, entityClassification);
		sleep();
	}

	/**
	 * Input External Entities info to create
	 */
	public void inputExternalEntitiesInfoToCreate(String organizationID, String firstName, String lastName, String companyName, String email, String address, String city,
			String state, String zipCode) {
		waitForControl(driver, Interfaces.TransactionPage.ENTITY_ORGANIZATION_ID_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.ENTITY_ORGANIZATION_ID_TEXTBOX, organizationID);
		sleep(1);
		type(driver, Interfaces.TransactionPage.EXTERNAL_ENTITY_FIRSTNAME_TEXTBOX, firstName);
		sleep(1);
		type(driver, Interfaces.TransactionPage.EXTERNAL_ENTITY_LASTNAME_TEXTBOX, lastName);
		sleep(1);
		type(driver, Interfaces.TransactionPage.EXTERNAL_ENTITY_COMPANY_TEXTBOX, companyName);
		sleep(1);
		type(driver, Interfaces.TransactionPage.EXTERNAL_ENTITY_EMAIL_TEXTBOX, email);
		sleep(1);
		type(driver, Interfaces.TransactionPage.EXTERNAL_ENTITY_ADDRESS_TEXTBOX, address);
		sleep(1);
		type(driver, Interfaces.TransactionPage.EXTERNAL_ENTITY_CITY_TEXTBOX, city);
		sleep(1);
		selectItemCombobox(driver, Interfaces.TransactionPage.EXTERNAL_ENTITY_STATE_DROPDOWN_LIST, state);
		sleep(1);
		type(driver, Interfaces.TransactionPage.EXTERNAL_ENTITY_ZIP_TEXTBOX, zipCode);
		sleep(1);
	}

	/**
	 * Check New External Entities saved
	 */
	public boolean isNewExternalEntitiesSaved(String entitiesName) {
		waitForControl(driver, Interfaces.TransactionPage.ENTITY_NAME_DROPDOWN_LIST, timeWait);
		sleep();
		String username = getItemSelectedCombobox(driver, Interfaces.TransactionPage.ENTITY_NAME_DROPDOWN_LIST);
		String fullname = getAttributeValue(driver, Interfaces.TransactionPage.EXTERNAL_ENTITY_LASTNAME_TEXTBOX, "value");
		return username.contains(entitiesName) && fullname.contains(entitiesName);
	}

	/**
	 * click Back Button
	 */
	public void clickBackExternalEntitiesButton() {
		waitForControl(driver, Interfaces.TransactionPage.BACK_EXTERNAL_ENTITIES_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Back').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(7);
		}
		sleep(3);
	}

	/**
	 * check New Entities display on External Entities table
	 */
	public boolean isNewEntitiesDisplayOnExternalEntitiesTable(String entitiesName, String entityType) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_ENTITIES_NAME_ON_TABLE, entitiesName, entityType, timeWait);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_ENTITIES_NAME_ON_TABLE, entitiesName, entityType);
	}

	/**
	 * Delete Entity in External Entity table
	 */
	public void deleteEntityInTable(String entity) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_ENTITY_CHECKBOX_IN_EXTERNAl_ENTITIES_TABLE, entity, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.DYNAMIC_ENTITY_CHECKBOX_IN_EXTERNAl_ENTITIES_TABLE, entity);
		sleep();
	}

	/**
	 * Open Basic Details tab
	 */
	public void openBasicDetailsTab() {
		waitForControl(driver, Interfaces.TransactionPage.BASIC_DETAILS_TAB_TITLE, timeWait);
		executeJavaScript(driver, "document.getElementById('ActionLoanApplicationBasicDetails').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
		sleep();
	}

	/**
	 * Open Lease Details tab
	 */
	public void openLeaseDetailsTab() {
		waitForControl(driver, Interfaces.TransactionPage.LEASE_DETAILS_TAB_TITLE, timeWait);
		executeJavaScript(driver, "document.getElementById('ActionLoanApplicationLeaseDetails').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
		sleep();
	}
	
	/**
	 * Open Property tab
	 */
	public void openPropertyTab() {
		waitForControl(driver, Interfaces.TransactionPage.PROPERTY_TAB_TITLE, timeWait);
		executeJavaScript(driver, "document.getElementById('ActionLoanApplicationProperty').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
		sleep();
	}

	/**
	 * Open Property Documents tab
	 */
	public void openPropertyDocumentsTab() {
		waitForControl(driver, Interfaces.TransactionPage.PROPERTY_DOCUMENTS_TAB_TITLE, timeWait);
		executeJavaScript(driver, "document.getElementById('ActionLoanApplicationDocuments').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
		sleep();
	}

	/**
	 * Open Transaction Documents tab
	 */
	public void openTransactionDocumentsTab() {
		waitForControl(driver, Interfaces.TransactionPage.TRANSACTION_DOCUMENTS_TAB_TITLE, timeWait);
		executeJavaScript(driver, "document.getElementById('ActionLeaseGeneralDocumentsSearchMenu').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
		sleep();
	}

	/**
	 * Open Lessee tab
	 */
	public void openLesseeTab() {
		waitForControl(driver, Interfaces.TransactionPage.LESSEE_TAB_TITLE, timeWait);
		executeJavaScript(driver, "document.getElementById('ActionLeaseEntitiesDocumentsSearchMenu').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
		sleep();
	}

	/**
	 * Select Payment Frequency combobox
	 */
	public void selectPaymentFrequency(String paymentFrequency) {
		waitForControl(driver, Interfaces.TransactionPage.PAYMENT_FREQUENCY_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.PAYMENT_FREQUENCY_DROPDOWN_LIST, paymentFrequency);
		sleep();
	}

	/**
	 * Check 'Payment Frequency' saved successfully
	 */
	public boolean isPaymentFrequencySavedSuccessfully(String paymentFrequency) {
		waitForControl(driver, Interfaces.TransactionPage.PAYMENT_FREQUENCY_DROPDOWN_LIST, timeWait);
		String realPaymentFrequency = getItemSelectedCombobox(driver, Interfaces.TransactionPage.PAYMENT_FREQUENCY_DROPDOWN_LIST);
		return realPaymentFrequency.contains(paymentFrequency);
	}

	/**
	 * Select Increase Type combobox
	 */
	public void selectIncreaseType(String increaseType) {
		waitForControl(driver, Interfaces.TransactionPage.INCREASE_TYPE_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.INCREASE_TYPE_DROPDOWN_LIST, increaseType);
		sleep();
	}

	/**
	 * Check 'Increase Type' saved successfully
	 */
	public boolean isIncreaseTypeSavedSuccessfully(String increaseType) {
		waitForControl(driver, Interfaces.TransactionPage.INCREASE_TYPE_DROPDOWN_LIST, timeWait);
		String realIncreaseType = getItemSelectedCombobox(driver, Interfaces.TransactionPage.INCREASE_TYPE_DROPDOWN_LIST);
		return realIncreaseType.contains(increaseType);
	}

	/**
	 * Input Next Increase Date
	 */
	public void inputNextIncreaseDate(String nextIncreaseDate) {
		waitForControl(driver, Interfaces.TransactionPage.NEXT_INCREASE_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.NEXT_INCREASE_DATE_TEXTBOX, nextIncreaseDate);
		sleep();
	}

	/**
	 * Check 'Next Increase Date' saved successfully
	 */
	public boolean isNextIncreaseDateSavedSuccessfully(String nextIncreaseDate) {
		waitForControl(driver, Interfaces.TransactionPage.NEXT_INCREASE_DATE_TEXTBOX, timeWait);
		String realNextIncreaseDate = getAttributeValue(driver, Interfaces.TransactionPage.NEXT_INCREASE_DATE_TEXTBOX, "value");
		return realNextIncreaseDate.contains(nextIncreaseDate);
	}

	/**
	 * Input Increase Frequency
	 */
	public void inputIncreaseFrequency(String increaseFrequency) {
		waitForControl(driver, Interfaces.TransactionPage.INCREASE_FREQUENCY_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.INCREASE_FREQUENCY_TEXTBOX, increaseFrequency);
		sleep(2);
	}

	/**
	 * Check 'Increase Frequency' saved successfully
	 */
	public boolean isIncreaseFrequencySavedSuccessfully(String increaseFrequency) {
		sleep();
		waitForControl(driver, Interfaces.TransactionPage.INCREASE_FREQUENCY_TEXTBOX, timeWait);
		String realIncreaseFrequency = getAttributeValue(driver, Interfaces.TransactionPage.INCREASE_FREQUENCY_TEXTBOX, "value");
		return realIncreaseFrequency.contains(increaseFrequency);
	}

	/**
	 * Input Increase Rate
	 */
	public void inputIncreaseRate(String increaseRate) {
		waitForControl(driver, Interfaces.TransactionPage.INCREASE_RATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.INCREASE_RATE_TEXTBOX, increaseRate);
		sleep();
	}

	/**
	 * Check 'Increase Rate' saved successfully
	 */
	public boolean isIncreaseRateSavedSuccessfully(String increaseRate) {
		waitForControl(driver, Interfaces.TransactionPage.INCREASE_RATE_TEXTBOX, timeWait);
		String realIncreaseRate = getAttributeValue(driver, Interfaces.TransactionPage.INCREASE_RATE_TEXTBOX, "value");
		return realIncreaseRate.contains(increaseRate);
	}

	/**
	 * Input Leverage Factor
	 */
	public void inputLeverageFactor(String leverageFactor) {
		waitForControl(driver, Interfaces.TransactionPage.LEVERAGE_FACTOR_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.LEVERAGE_FACTOR_TEXTBOX, leverageFactor);
		sleep();
	}

	/**
	 * Check 'Leverage Factor' saved successfully
	 */
	public boolean isLeverageFactorSavedSuccessfully(String leverageFactor) {
		waitForControl(driver, Interfaces.TransactionPage.LEVERAGE_FACTOR_TEXTBOX, timeWait);
		String realLeverageFactor = getAttributeValue(driver, Interfaces.TransactionPage.LEVERAGE_FACTOR_TEXTBOX, "value");
		return realLeverageFactor.contains(leverageFactor);
	}

	/**
	 * Input Adjustment Description
	 */
	public void inputAdjustmentDescription(String adjustmentDescription) {
		waitForControl(driver, Interfaces.TransactionPage.ADJUSTMENT_DESCRIPTION_TEXTAREA, timeWait);
		type(driver, Interfaces.TransactionPage.ADJUSTMENT_DESCRIPTION_TEXTAREA, adjustmentDescription);
		sleep();
	}

	/**
	 * Check 'Adjustment Description' saved successfully
	 */
	public boolean isAdjustmentDescriptionSavedSuccessfully(String adjustmentDescription) {
		waitForControl(driver, Interfaces.TransactionPage.ADJUSTMENT_DESCRIPTION_TEXTAREA, timeWait);
		String realAdjustmentDescription = getText(driver, Interfaces.TransactionPage.ADJUSTMENT_DESCRIPTION_TEXTAREA);
		return realAdjustmentDescription.contains(adjustmentDescription);
	}

	/**
	 * Input Straight Line Average Rent Payment
	 */
	public void inputStraightLineAverageRentPayment(String straightLineAverageRentPayment) {
		waitForControl(driver, Interfaces.TransactionPage.STRAIGHT_LINE_AVERAGE_RENT_PAYMENT_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.STRAIGHT_LINE_AVERAGE_RENT_PAYMENT_TEXTBOX, straightLineAverageRentPayment);
		sleep();
	}

	/**
	 * Check 'Straight Line Average Rent Payment' saved successfully
	 */
	public boolean isStraightLineAverageRentPaymentSavedSuccessfully(String straightLineAverageRentPayment) {
		waitForControl(driver, Interfaces.TransactionPage.STRAIGHT_LINE_AVERAGE_RENT_PAYMENT_TEXTBOX, timeWait);
		String realStraightLineAverageRentPayment = getAttributeValue(driver, Interfaces.TransactionPage.STRAIGHT_LINE_AVERAGE_RENT_PAYMENT_TEXTBOX, "value");
		return realStraightLineAverageRentPayment.contains(straightLineAverageRentPayment);
	}

	/**
	 * select Rent Reset
	 */
	public void selectRentReset() {
		waitForControl(driver, Interfaces.TransactionPage.RENT_RESET_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.RENT_RESET_CHECKBOX);
		sleep();
	}

	/**
	 * Input Next Reset Date
	 */
	public void inputNextResetDate(String nextResetDate) {
		waitForControl(driver, Interfaces.TransactionPage.NEXT_RESET_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.NEXT_RESET_DATE_TEXTBOX, nextResetDate);
		sleep();
	}

	/**
	 * Check 'Next Reset Date' saved successfully
	 */
	public boolean isNextResetDateSavedSuccessfully(String nextResetDate) {
		waitForControl(driver, Interfaces.TransactionPage.NEXT_RESET_DATE_TEXTBOX, timeWait);
		String realNextResetDate = getAttributeValue(driver, Interfaces.TransactionPage.NEXT_RESET_DATE_TEXTBOX, "value");
		return realNextResetDate.contains(nextResetDate);
	}

	/**
	 * Input Reset Frequency
	 */
	public void inputResetFrequency(String resetFrequency) {
		waitForControl(driver, Interfaces.TransactionPage.RESET_FREQUENCY_TEXTAREA, timeWait);
		type(driver, Interfaces.TransactionPage.RESET_FREQUENCY_TEXTAREA, resetFrequency);
		sleep();
	}

	/**
	 * Check 'Reset Frequency' saved successfully
	 */
	public boolean isResetFrequencySavedSuccessfully(String resetFrequency) {
		waitForControl(driver, Interfaces.TransactionPage.RESET_FREQUENCY_TEXTAREA, timeWait);
		String realResetFrequency = getText(driver, Interfaces.TransactionPage.RESET_FREQUENCY_TEXTAREA);
		return realResetFrequency.contains(resetFrequency);
	}

	/**
	 * select Tenant Purchase Option
	 */
	public void selectTenantPurchaseOption() {
		waitForControl(driver, Interfaces.TransactionPage.TENANT_PURCHASE_OPTION_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.TENANT_PURCHASE_OPTION_CHECKBOX);
		sleep();
	}

	/**
	 * Select Purchase Option Type combobox
	 */
	public void selectPurchaseOptionType(String purchaseOptionType) {
		waitForControl(driver, Interfaces.TransactionPage.PURCHASE_OPTION_TYPE_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.PURCHASE_OPTION_TYPE_DROPDOWN_LIST, purchaseOptionType);
		sleep();
	}

	/**
	 * Check 'Purchase Option Type' saved successfully
	 */
	public boolean isPurchaseOptionTypeSavedSuccessfully(String purchaseOptionType) {
		waitForControl(driver, Interfaces.TransactionPage.PURCHASE_OPTION_TYPE_DROPDOWN_LIST, timeWait);
		String realPurchaseOptionType = getItemSelectedCombobox(driver, Interfaces.TransactionPage.PURCHASE_OPTION_TYPE_DROPDOWN_LIST);
		return realPurchaseOptionType.contains(purchaseOptionType);
	}

	/**
	 * Input Purchase Option Timing
	 */
	public void inputPurchaseOptionTiming(String purchaseOptionTiming) {
		waitForControl(driver, Interfaces.TransactionPage.PURCHASE_OPTION_TIMING_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.PURCHASE_OPTION_TIMING_TEXTBOX, purchaseOptionTiming);
		sleep();
	}

	/**
	 * Check 'Purchase Option Timing' saved successfully
	 */
	public boolean isPurchaseOptionTimingSavedSuccessfully(String purchaseOptionTiming) {
		waitForControl(driver, Interfaces.TransactionPage.PURCHASE_OPTION_TIMING_TEXTBOX, timeWait);
		String realPurchaseOptionTiming = getAttributeValue(driver, Interfaces.TransactionPage.PURCHASE_OPTION_TIMING_TEXTBOX, "value");
		return realPurchaseOptionTiming.contains(purchaseOptionTiming);
	}

	/**
	 * Input Purchase Option Terms
	 */
	public void inputPurchaseOptionTerms(String purchaseOptionTerms) {
		waitForControl(driver, Interfaces.TransactionPage.PURCHASE_OPTION_TERMS_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.PURCHASE_OPTION_TERMS_TEXTBOX, purchaseOptionTerms);
		sleep();
	}

	/**
	 * Check 'Purchase Option Terms' saved successfully
	 */
	public boolean isPurchaseOptionTermsSavedSuccessfully(String purchaseOptionTerms) {
		waitForControl(driver, Interfaces.TransactionPage.PURCHASE_OPTION_TERMS_TEXTBOX, timeWait);
		String realPurchaseOptionTerms = getAttributeValue(driver, Interfaces.TransactionPage.PURCHASE_OPTION_TERMS_TEXTBOX, "value");
		return realPurchaseOptionTerms.contains(purchaseOptionTerms);
	}

	/**
	 * Input Date First Exercisable
	 */
	public void inputDateFirstExercisable(String dateFirstExercisable) {
		waitForControl(driver, Interfaces.TransactionPage.DATE_FIRST_EXERCISABLE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.DATE_FIRST_EXERCISABLE_TEXTBOX, dateFirstExercisable);
		sleep();
	}

	/**
	 * Check 'Date First Exercisable' saved successfully
	 */
	public boolean isDateFirstExercisableSavedSuccessfully(String dateFirstExercisable) {
		waitForControl(driver, Interfaces.TransactionPage.DATE_FIRST_EXERCISABLE_TEXTBOX, timeWait);
		String realDateFirstExercisable = getAttributeValue(driver, Interfaces.TransactionPage.DATE_FIRST_EXERCISABLE_TEXTBOX, "value");
		return realDateFirstExercisable.contains(dateFirstExercisable);
	}

	/**
	 * Select TripleNet
	 */
	public void selectTripleNet() {
		waitForControl(driver, Interfaces.TransactionPage.TRIPLENET_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.TRIPLENET_CHECKBOX);
		sleep();
	}

	/**
	 * Unselect TripleNet
	 */
	public void unSelectTripleNet() {
		waitForControl(driver, Interfaces.TransactionPage.TRIPLENET_CHECKBOX, timeWait);
		uncheckTheCheckbox(driver, Interfaces.TransactionPage.TRIPLENET_CHECKBOX);
		sleep();
	}

	/**
	 * Select Tenant subsitution/transfer rights
	 */
	public void selectTenantSubsitutionTransferRights() {
		waitForControl(driver, Interfaces.TransactionPage.TENANT_SUBSITUTION_TRANSFER_RIGHTS_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.TENANT_SUBSITUTION_TRANSFER_RIGHTS_CHECKBOX);
		sleep();
	}

	/**
	 * Select Collect Lease Sales Tax
	 */
	public void selectCollectLeaseSalesTax() {
		waitForControl(driver, Interfaces.TransactionPage.COLLECT_LEASE_SALES_TAX_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.COLLECT_LEASE_SALES_TAX_CHECKBOX);
		sleep();
	}

	/**
	 * Select Lease Personal Property Lien
	 */
	public void selectLeasePersonalPropertyLien() {
		waitForControl(driver, Interfaces.TransactionPage.LEASE_PERSONAL_PROPERTY_LIEN_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.LEASE_PERSONAL_PROPERTY_LIEN_CHECKBOX);
		sleep();
	}

	/**
	 * Select NN Type
	 */
	public void selectNNType(String nnType) {
		waitForControl(driver, Interfaces.TransactionPage.NN_TYPE_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.NN_TYPE_DROPDOWN_LIST, nnType);
		sleep();
	}

	/**
	 * Check 'NN Type' saved successfully
	 */
	public boolean isNNTypeSavedSuccessfully(String nnType) {
		waitForControl(driver, Interfaces.TransactionPage.NN_TYPE_DROPDOWN_LIST, timeWait);
		String realNNType = getItemSelectedCombobox(driver, Interfaces.TransactionPage.NN_TYPE_DROPDOWN_LIST);
		return realNNType.contains(nnType);
	}

	/**
	 * Input Description of Landlord Obligations
	 */
	public void inputDescriptionOfLandlordObligations(String descriptionOfLandlordObligations) {
		waitForControl(driver, Interfaces.TransactionPage.DESCRIPTION_OF_LANDLORD_OBLIGATIONS_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.DESCRIPTION_OF_LANDLORD_OBLIGATIONS_TEXTBOX, descriptionOfLandlordObligations);
		sleep();
	}

	/**
	 * Check 'Description of Landlord Obligations' saved successfully
	 */
	public boolean isDescriptionOfLandlordObligationsSavedSuccessfully(String descriptionOfLandlordObligations) {
		waitForControl(driver, Interfaces.TransactionPage.DESCRIPTION_OF_LANDLORD_OBLIGATIONS_TEXTBOX, timeWait);
		String realDescriptionOfLandlordObligations = getAttributeValue(driver, Interfaces.TransactionPage.DESCRIPTION_OF_LANDLORD_OBLIGATIONS_TEXTBOX, "value");
		return realDescriptionOfLandlordObligations.contains(descriptionOfLandlordObligations);
	}

	/**
	 * Select Lease Liquor Sold
	 */
	public void selectLeaseLiquorSold() {
		waitForControl(driver, Interfaces.TransactionPage.LEASE_LIQUOR_SOLD_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.LEASE_LIQUOR_SOLD_CHECKBOX);
		sleep();
	}

	/**
	 * Click Add Property button
	 */
	public void clickAddPropertyButton() {
		waitForControl(driver, Interfaces.TransactionPage.ADD_PROPERTY_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('AddProperties').click();");
		sleep();
	}

	/**
	 * Upload File Properties
	 */
	public void uploadFileProperties(String propertiesFileName) {
		waitForControl(driver, Interfaces.TransactionPage.PROPERTY_CHOOSE_FILE_BUTTON, timeWait);
		doubleClick(driver, Interfaces.TransactionPage.PROPERTY_CHOOSE_FILE_BUTTON);
		sleep(2);
		Common.getCommon().openFileForUpload(driver, propertiesFileName);
		sleep(2);
		clickSaveLoadPropertyButton();
		sleep(2);
		waitForControl(driver, Interfaces.TransactionPage.FIND_PROPERTIES_LOAN_APPLICANT_SEARCH_SECTION, timeWait);
	}

	/**
	 * Click Save Load Property Button
	 */
	public void clickSaveLoadPropertyButton() {
		waitForControl(driver, Interfaces.TransactionPage.SAVE_LOAD_PROPERTY_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('SaveFile').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(20);
		}
		sleep(5);
	}

	/**
	 * Get Number Of Property In Properties Tab
	 */
	public int getNumberOfPropertyInPropertiesTab() {
		waitForControl(driver, Interfaces.TransactionPage.TOTAL_PROPERTY_LABEL, timeWait);
		String text = getText(driver, Interfaces.TransactionPage.TOTAL_PROPERTY_LABEL);
		String[] subString = text.split(" ");
		int result = Integer.parseInt(subString[subString.length - 1]);
		return result;
	}

	/**
	 * Get number of Address In Properties Tab
	 */
	public int getNumberOfAddressInPropertiesTab(String address) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_ADDRESS_NUMBER_ON_SEARCH_TRANSACTION_ITEM, address, timeWait);
		String text = getText(driver, Interfaces.TransactionPage.DYNAMIC_ADDRESS_NUMBER_ON_SEARCH_TRANSACTION_ITEM, address);
		String[] subString = text.split(" ");
		int result = Integer.parseInt(subString[subString.length - 1]);
		return result;
	}

	/**
	 * Check Property Info Correctly
	 */
	public boolean isPropertyInfoCorrectly(String pID, String address, String city, String state, String zipCode) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_PROPERTY_INFOR, pID, address, city, state, zipCode, timeWait);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_PROPERTY_INFOR, pID, address, city, state, zipCode);
	}

	/**
	 * Search P#
	 */
	public void searchPropertyPNumber(String pNumber) {
		waitForControl(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_P_NUMBER_OR_DOCUMENT_NUMBER_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_P_NUMBER_OR_DOCUMENT_NUMBER_TEXTBOX, pNumber);
		sleep();
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep(3);
	}

	/**
	 * Search Property ID
	 */
	public void searchPropertyPropertyID(String propertyID) {
		waitForControl(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_ID_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_ID_TEXTBOX, propertyID);
		sleep();
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep(3);
	}

	/**
	 * Search Property address
	 */
	public void searchPropertyAddress(String address) {
		waitForControl(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, address);
		sleep();
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep(3);
	}

	/**
	 * Search City
	 */
	public void searchPropertyCity(String city) {
		waitForControl(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_CITY_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_CITY_TEXTBOX, city);
		sleep();
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep(3);
	}

	/**
	 * Search State
	 */
	public void searchPropertyState(String state) {
		waitForControl(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_STATE_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_STATE_DROPDOWN_LIST, state);
		sleep();
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep(3);
	}

	/**
	 * Search Zip code
	 */
	public void searchPropertyZipCode(String zipCode) {
		waitForControl(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_ZIP_CODE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_ZIP_CODE_TEXTBOX, zipCode);
		sleep();
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep(3);
	}

	/**
	 * Search by Active/Inactive
	 */
	public void searchByActiveOrInactive(String pNumber, String status) {
		waitForControl(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_P_NUMBER_OR_DOCUMENT_NUMBER_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_P_NUMBER_OR_DOCUMENT_NUMBER_TEXTBOX, pNumber);
		sleep(1);
		click(driver, Interfaces.TransactionPage.DYNAMIC_ACTIVE_RADIO_BUTTON, status);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * Check Property checkbox in List Properties table
	 */
	public void isSelectedPropertyCheckbox(String address) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_PROPERTY_CHECKBOX_IN_LIST_PROPERTIES_TABLE, address, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.DYNAMIC_PROPERTY_CHECKBOX_IN_LIST_PROPERTIES_TABLE, address);
		sleep();
	}

	/**
	 * Open Property Detail
	 */
	public void openPropertyDetail(String address) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_ADDRESS_PROPERTY, address, timeWait);
		click(driver, Interfaces.TransactionPage.DYNAMIC_ADDRESS_PROPERTY, address);
		sleep(3);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * Get value of P#
	 */
	public String getValuePNumber() {
		waitForControl(driver, Interfaces.TransactionPage.P_NUMBER_TITLE, timeWait);
		String pNumber = getText(driver, Interfaces.TransactionPage.P_NUMBER_TITLE);
		return pNumber;
	}

	/**
	 * Get value of Active
	 */
	public String getValueActive() {
		waitForControl(driver, Interfaces.TransactionPage.ACTIVE_TITLE, timeWait);
		String active = getText(driver, Interfaces.TransactionPage.ACTIVE_TITLE);
		return active;
	}

	/**
	 * Get value of Property ID
	 */
	public String getValuePropertyID() {
		waitForControl(driver, Interfaces.TransactionPage.PROPERTY_ID_TITLE, timeWait);
		String propertyID = getText(driver, Interfaces.TransactionPage.PROPERTY_ID_TITLE);
		return propertyID;
	}

	/**
	 * Input Property Name
	 */
	public void inputPropertyName(String propertyName) {
		waitForControl(driver, Interfaces.TransactionPage.PROPERTY_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.PROPERTY_NAME_TEXTBOX, propertyName);
		sleep();
	}

	/**
	 * Check Property Name saved successfully
	 */
	public boolean isPropertyNameSavedSuccessfully(String propertyName) {
		waitForControl(driver, Interfaces.TransactionPage.PROPERTY_NAME_TEXTBOX, timeWait);
		String realPropertyName = getAttributeValue(driver, Interfaces.TransactionPage.PROPERTY_NAME_TEXTBOX, "value");
		return realPropertyName.contains(propertyName);
	}

	/**
	 * Input Contract ID
	 */
	public void inputContractID(String contractID) {
		waitForControl(driver, Interfaces.TransactionPage.CONTRACT_ID_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.CONTRACT_ID_TEXTBOX, contractID);
		sleep();
	}

	/**
	 * Check Contract ID saved successfully
	 */
	public boolean isContractIDSavedSuccessfully(String contractID) {
		waitForControl(driver, Interfaces.TransactionPage.CONTRACT_ID_TEXTBOX, timeWait);
		String realContractID = getAttributeValue(driver, Interfaces.TransactionPage.CONTRACT_ID_TEXTBOX, "value");
		return realContractID.contains(contractID);
	}

	/**
	 * Input Unit Number
	 */
	public void inputUnitNumber(String unitNumber) {
		waitForControl(driver, Interfaces.TransactionPage.UNIT_NUMBER_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.UNIT_NUMBER_TEXTBOX, unitNumber);
		sleep();
	}

	/**
	 * Check Unit Number saved successfully
	 */
	public boolean isUnitNumberSavedSuccessfully(String unitNumber) {
		waitForControl(driver, Interfaces.TransactionPage.UNIT_NUMBER_TEXTBOX, timeWait);
		String realUnitNumber = getAttributeValue(driver, Interfaces.TransactionPage.UNIT_NUMBER_TEXTBOX, "value");
		return realUnitNumber.contains(unitNumber);
	}

	/**
	 * Input APN
	 */
	public void inputAPN(String aPN) {
		waitForControl(driver, Interfaces.TransactionPage.APN_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.APN_TEXTBOX, aPN);
		sleep();
	}

	/**
	 * Check APN saved successfully
	 */
	public boolean isAPNSavedSuccessfully(String aPN) {
		waitForControl(driver, Interfaces.TransactionPage.APN_TEXTBOX, timeWait);
		String realAPN = getAttributeValue(driver, Interfaces.TransactionPage.APN_TEXTBOX, "value");
		return realAPN.contains(aPN);
	}

	/**
	 * Input # of Units
	 */
	public void inputUnit(String ofUnit) {
		waitForControl(driver, Interfaces.TransactionPage.OF_UNIT_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.OF_UNIT_TEXTBOX, ofUnit);
		sleep();
	}

	/**
	 * Check # of Units saved successfully
	 */
	public boolean isUnitSavedSuccessfully(String ofUnit) {
		waitForControl(driver, Interfaces.TransactionPage.OF_UNIT_TEXTBOX, timeWait);
		String realUnit = getAttributeValue(driver, Interfaces.TransactionPage.OF_UNIT_TEXTBOX, "value");
		return realUnit.contains(ofUnit);
	}

	/**
	 * Input Parent Property
	 */
	public void inputParentProperty(String parentProperty) {
		waitForControl(driver, Interfaces.TransactionPage.PARENT_PROPERTY_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.PARENT_PROPERTY_TEXTBOX, parentProperty);
		sleep();
	}

	/**
	 * Check Parent Property saved successfully
	 */
	public boolean isParentPropertySavedSuccessfully(String parentProperty) {
		waitForControl(driver, Interfaces.TransactionPage.PARENT_PROPERTY_TEXTBOX, timeWait);
		String realParentProperty = getAttributeValue(driver, Interfaces.TransactionPage.PARENT_PROPERTY_TEXTBOX, "value");
		return realParentProperty.contains(parentProperty);
	}

	/**
	 * Input Seller
	 */
	public void inputSeller(String seller) {
		waitForControl(driver, Interfaces.TransactionPage.SELLER_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SELLER_TEXTBOX, seller);
		sleep();
	}

	/**
	 * Check Seller saved successfully
	 */
	public boolean isSellerSavedSuccessfully(String seller) {
		waitForControl(driver, Interfaces.TransactionPage.SELLER_TEXTBOX, timeWait);
		String realSeller = getAttributeValue(driver, Interfaces.TransactionPage.SELLER_TEXTBOX, "value");
		return realSeller.contains(seller);
	}

	/**
	 * Input Address
	 */
	public void inputAddress(String address) {
		waitForControl(driver, Interfaces.TransactionPage.ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.ADDRESS_TEXTBOX, address);
		sleep();
	}

	/**
	 * Check Address saved successfully
	 */
	public boolean isAddressSavedSuccessfully(String address) {
		waitForControl(driver, Interfaces.TransactionPage.ADDRESS_TEXTBOX, timeWait);
		String realAddress = getAttributeValue(driver, Interfaces.TransactionPage.ADDRESS_TEXTBOX, "value");
		return realAddress.contains(address);
	}

	/**
	 * Input Suite No.
	 */
	public void inputSuiteNumber(String suiteNumber) {
		waitForControl(driver, Interfaces.TransactionPage.SUITE_NUMBER_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SUITE_NUMBER_TEXTBOX, suiteNumber);
		sleep();
	}

	/**
	 * Check Suite No. saved successfully
	 */
	public boolean isSuiteNumberSavedSuccessfully(String suiteNumber) {
		waitForControl(driver, Interfaces.TransactionPage.SUITE_NUMBER_TEXTBOX, timeWait);
		String realSuiteNumber = getAttributeValue(driver, Interfaces.TransactionPage.SUITE_NUMBER_TEXTBOX, "value");
		return realSuiteNumber.contains(suiteNumber);
	}

	/**
	 * Input City
	 */
	public void inputCity(String city) {
		waitForControl(driver, Interfaces.TransactionPage.CITY_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.CITY_TEXTBOX, city);
		sleep();
	}

	/**
	 * Check City saved successfully
	 */
	public boolean isCitySavedSuccessfully(String city) {
		waitForControl(driver, Interfaces.TransactionPage.CITY_TEXTBOX, timeWait);
		String realCity = getAttributeValue(driver, Interfaces.TransactionPage.CITY_TEXTBOX, "value");
		return realCity.contains(city);
	}

	/**
	 * Select State
	 */
	public void selectState(String state) {
		waitForControl(driver, Interfaces.TransactionPage.STATE_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.STATE_DROPDOWN_LIST, state);
		sleep();
	}

	/**
	 * Check State saved successfully
	 */
	public boolean isStateSavedSuccessfully(String state) {
		waitForControl(driver, Interfaces.TransactionPage.STATE_DROPDOWN_LIST, timeWait);
		String realState = getItemSelectedCombobox(driver, Interfaces.TransactionPage.STATE_DROPDOWN_LIST);
		return realState.contains(state);
	}

	/**
	 * Input Zip Code
	 */
	public void inputZipCode(String zipCode) {
		waitForControl(driver, Interfaces.TransactionPage.ZIP_CODE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.ZIP_CODE_TEXTBOX, zipCode);
		sleep();
	}

	/**
	 * Check Zip Code saved successfully
	 */
	public boolean isZipCodeSavedSuccessfully(String zipCode) {
		waitForControl(driver, Interfaces.TransactionPage.ZIP_CODE_TEXTBOX, timeWait);
		String realZipCode = getAttributeValue(driver, Interfaces.TransactionPage.ZIP_CODE_TEXTBOX, "value");
		return realZipCode.contains(zipCode);
	}

	/**
	 * Input County
	 */
	public void inputCounty(String county) {
		waitForControl(driver, Interfaces.TransactionPage.COUNTY_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.COUNTY_TEXTBOX, county);
		sleep();
	}

	/**
	 * Check County saved successfully
	 */
	public boolean isCountySavedSuccessfully(String county) {
		waitForControl(driver, Interfaces.TransactionPage.COUNTY_TEXTBOX, timeWait);
		String realCounty = getAttributeValue(driver, Interfaces.TransactionPage.COUNTY_TEXTBOX, "value");
		return realCounty.contains(county);
	}

	/**
	 * Input MSA
	 */
	public void inputMSA(String mSA) {
		waitForControl(driver, Interfaces.TransactionPage.MSA_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.MSA_TEXTBOX, mSA);
		sleep();
	}

	/**
	 * Check MSA saved successfully
	 */
	public boolean isMSASavedSuccessfully(String mSA) {
		waitForControl(driver, Interfaces.TransactionPage.MSA_TEXTBOX, timeWait);
		String realMSA = getAttributeValue(driver, Interfaces.TransactionPage.MSA_TEXTBOX, "value");
		return realMSA.contains(mSA);
	}

	/**
	 * Input Contact Name
	 */
	public void inputContactName(String contactName) {
		waitForControl(driver, Interfaces.TransactionPage.CONTACT_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.CONTACT_NAME_TEXTBOX, contactName);
		sleep();
	}

	/**
	 * Check Contact Name saved successfully
	 */
	public boolean isContactNameSavedSuccessfully(String contactName) {
		waitForControl(driver, Interfaces.TransactionPage.CONTACT_NAME_TEXTBOX, timeWait);
		String realContactName = getAttributeValue(driver, Interfaces.TransactionPage.CONTACT_NAME_TEXTBOX, "value");
		return realContactName.contains(contactName);
	}

	/**
	 * Input Open Date
	 */
	public void inputOpenDate(String openDate) {
		waitForControl(driver, Interfaces.TransactionPage.OPEN_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.OPEN_DATE_TEXTBOX, openDate);
		sleep();
	}

	/**
	 * Check Open Date saved successfully
	 */
	public boolean isOpenDateSavedSuccessfully(String openDate) {
		waitForControl(driver, Interfaces.TransactionPage.OPEN_DATE_TEXTBOX, timeWait);
		String realOpenDate = getAttributeValue(driver, Interfaces.TransactionPage.OPEN_DATE_TEXTBOX, "value");
		return realOpenDate.contains(openDate);
	}

	/**
	 * Select Collateral Type
	 */
	public void selectCollateralType(String collateralType) {
		waitForControl(driver, Interfaces.TransactionPage.COLLATERAL_TYPE_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.COLLATERAL_TYPE_DROPDOWN_LIST, collateralType);
		sleep();
	}

	/**
	 * Check Collateral Type saved successfully
	 */
	public boolean isCollateralTypeSavedSuccessfully(String collateralType) {
		waitForControl(driver, Interfaces.TransactionPage.COLLATERAL_TYPE_DROPDOWN_LIST, timeWait);
		String realCollateralType = getItemSelectedCombobox(driver, Interfaces.TransactionPage.COLLATERAL_TYPE_DROPDOWN_LIST);
		return realCollateralType.contains(collateralType);
	}

	/**
	 * Input Acquisition Price
	 */
	public void inputAcquisitionPrice(String acquisitionPrice) {
		waitForControl(driver, Interfaces.TransactionPage.ACQ_PRICE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.ACQ_PRICE_TEXTBOX, acquisitionPrice);
		sleep();
	}

	/**
	 * Check Acquisition Price saved successfully
	 */
	public boolean isAcquisitionPriceSavedSuccessfully(String acquisitionPrice) {
		waitForControl(driver, Interfaces.TransactionPage.ACQ_PRICE_TEXTBOX, timeWait);
		String realAcquisitionPrice = getAttributeValue(driver, Interfaces.TransactionPage.ACQ_PRICE_TEXTBOX, "value");
		return realAcquisitionPrice.contains(acquisitionPrice);
	}

	/**
	 * Input Verified Acquisition Price
	 */
	public void inputVerifiedAcquisitionPrice(String verifiedAcquisitionPrice) {
		waitForControl(driver, Interfaces.TransactionPage.VERIFIED_ACQ_PRICE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.VERIFIED_ACQ_PRICE_TEXTBOX, verifiedAcquisitionPrice);
		sleep();
	}

	/**
	 * Check Verified Acquisition Price saved successfully
	 */
	public boolean isVerifiedAcquisitionPriceSavedSuccessfully(String verifiedAcquisitionPrice) {
		waitForControl(driver, Interfaces.TransactionPage.VERIFIED_ACQ_PRICE_TEXTBOX, timeWait);
		String realVerifiedAcquisitionPrice = getAttributeValue(driver, Interfaces.TransactionPage.VERIFIED_ACQ_PRICE_TEXTBOX, "value");
		return realVerifiedAcquisitionPrice.contains(verifiedAcquisitionPrice);
	}

	/**
	 * Select Currently Leased
	 */
	public void selectCurrentlyLeased(String currentlyLeased) {
		waitForControl(driver, Interfaces.TransactionPage.LEASE_STATUS_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.LEASE_STATUS_DROPDOWN_LIST, currentlyLeased);
		sleep();
	}

	/**
	 * Check Currently Leased saved successfully
	 */
	public boolean isCurrentlyLeasedSavedSuccessfully(String currentlyLeased) {
		waitForControl(driver, Interfaces.TransactionPage.LEASE_STATUS_DROPDOWN_LIST, timeWait);
		String realCurrentlyLeased = getItemSelectedCombobox(driver, Interfaces.TransactionPage.LEASE_STATUS_DROPDOWN_LIST);
		return realCurrentlyLeased.contains(currentlyLeased);
	}

	/**
	 * Input Transaction Costs
	 */
	public void inputTransactionCosts(String transactionCosts) {
		waitForControl(driver, Interfaces.TransactionPage.TRANSACTION_COST_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.TRANSACTION_COST_TEXTBOX, transactionCosts);
		sleep();
	}

	/**
	 * Check Transaction Costs saved successfully
	 */
	public boolean isTransactionCostsSavedSuccessfully(String transactionCosts) {
		waitForControl(driver, Interfaces.TransactionPage.TRANSACTION_COST_TEXTBOX, timeWait);
		String realTransactionCosts = getAttributeValue(driver, Interfaces.TransactionPage.TRANSACTION_COST_TEXTBOX, "value");
		return realTransactionCosts.contains(transactionCosts);
	}

	/**
	 * Input Lease End Date
	 */
	public void inputLeaseEndDate(String transactionCosts) {
		waitForControl(driver, Interfaces.TransactionPage.LEASE_END_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.LEASE_END_DATE_TEXTBOX, transactionCosts);
		sleep();
	}

	/**
	 * Check Lease End Date saved successfully
	 */
	public boolean isLeaseEndDateSavedSuccessfully(String transactionCosts) {
		waitForControl(driver, Interfaces.TransactionPage.LEASE_END_DATE_TEXTBOX, timeWait);
		String realLeaseEndDate = getAttributeValue(driver, Interfaces.TransactionPage.LEASE_END_DATE_TEXTBOX, "value");
		return realLeaseEndDate.contains(transactionCosts);
	}

	/**
	 * Input Occupancy
	 */
	public void inputOccupancy(String occupancy) {
		waitForControl(driver, Interfaces.TransactionPage.OCCUPANCY_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.OCCUPANCY_TEXTBOX, occupancy);
		sleep();
	}

	/**
	 * Check Occupancy saved successfully
	 */
	public boolean isOccupancySavedSuccessfully(String occupancy) {
		waitForControl(driver, Interfaces.TransactionPage.OCCUPANCY_TEXTBOX, timeWait);
		String realOccupancy = getAttributeValue(driver, Interfaces.TransactionPage.OCCUPANCY_TEXTBOX, "value");
		return realOccupancy.contains(occupancy);
	}

	/**
	 * Input Escrow
	 */
	public void inputEscrow(String escrow) {
		waitForControl(driver, Interfaces.TransactionPage.ESCROW_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.ESCROW_TEXTBOX, escrow);
		sleep();
	}

	/**
	 * Check Escrow saved successfully
	 */
	public boolean isEscrowSavedSuccessfully(String escrow) {
		waitForControl(driver, Interfaces.TransactionPage.ESCROW_TEXTBOX, timeWait);
		String realEscrow = getAttributeValue(driver, Interfaces.TransactionPage.ESCROW_TEXTBOX, "value");
		return realEscrow.contains(escrow);
	}

	/**
	 * Input Year Built
	 */
	public void inputYearBuilt(String yearBuilt) {
		waitForControl(driver, Interfaces.TransactionPage.YEAR_BUILT_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.YEAR_BUILT_TEXTBOX, yearBuilt);
		sleep();
	}

	/**
	 * Check Year Built saved successfully
	 */
	public boolean isYearBuiltSavedSuccessfully(String yearBuilt) {
		waitForControl(driver, Interfaces.TransactionPage.YEAR_BUILT_TEXTBOX, timeWait);
		String realYearBuilt = getAttributeValue(driver, Interfaces.TransactionPage.YEAR_BUILT_TEXTBOX, "value");
		return realYearBuilt.contains(yearBuilt);
	}

	/**
	 * Input Year Renovated
	 */
	public void inputYearRenovated(String yearRenovated) {
		waitForControl(driver, Interfaces.TransactionPage.YEAR_RENOVATED_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.YEAR_RENOVATED_TEXTBOX, yearRenovated);
		sleep();
	}

	/**
	 * Check Year Renovated saved successfully
	 */
	public boolean isYearRenovatedSavedSuccessfully(String yearRenovated) {
		waitForControl(driver, Interfaces.TransactionPage.YEAR_RENOVATED_TEXTBOX, timeWait);
		String realYearRenovated = getAttributeValue(driver, Interfaces.TransactionPage.YEAR_RENOVATED_TEXTBOX, "value");
		return realYearRenovated.contains(yearRenovated);
	}

	/**
	 * Input Number of Stories
	 */
	public void inputNumberOfStories(String numberOfStories) {
		waitForControl(driver, Interfaces.TransactionPage.NUMBER_OF_STORIES_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.NUMBER_OF_STORIES_TEXTBOX, numberOfStories);
		sleep();
	}

	/**
	 * Check Number of Stories saved successfully
	 */
	public boolean isNumberOfStoriesSavedSuccessfully(String numberOfStories) {
		waitForControl(driver, Interfaces.TransactionPage.NUMBER_OF_STORIES_TEXTBOX, timeWait);
		String realNumberOfStories = getAttributeValue(driver, Interfaces.TransactionPage.NUMBER_OF_STORIES_TEXTBOX, "value");
		return realNumberOfStories.contains(numberOfStories);
	}

	/**
	 * Select Building Construction Type combobox
	 */
	public void selectBuildingConstructionType(String buildingConstructionType) {
		waitForControl(driver, Interfaces.TransactionPage.BUILDING_CONSTRUCTION_TYPE_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.BUILDING_CONSTRUCTION_TYPE_DROPDOWN_LIST, buildingConstructionType);
		sleep();
	}

	/**
	 * Check 'Building Construction Type' saved successfully
	 */
	public boolean isBuildingConstructionTypeSavedSuccessfully(String buildingConstructionType) {
		waitForControl(driver, Interfaces.TransactionPage.BUILDING_CONSTRUCTION_TYPE_DROPDOWN_LIST, timeWait);
		String realBuildingConstructionType = getItemSelectedCombobox(driver, Interfaces.TransactionPage.BUILDING_CONSTRUCTION_TYPE_DROPDOWN_LIST);
		return realBuildingConstructionType.contains(buildingConstructionType);
	}

	/**
	 * Input Appraisal Date
	 */
	public void inputAppraisalDate(String appraisalDate) {
		waitForControl(driver, Interfaces.TransactionPage.APPRAISAL_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.APPRAISAL_DATE_TEXTBOX, appraisalDate);
		sleep();
	}

	/**
	 * Check Appraisal Date saved successfully
	 */
	public boolean isAppraisalDateSavedSuccessfully(String appraisalDate) {
		waitForControl(driver, Interfaces.TransactionPage.APPRAISAL_DATE_TEXTBOX, timeWait);
		String realAppraisalDate = getAttributeValue(driver, Interfaces.TransactionPage.APPRAISAL_DATE_TEXTBOX, "value");
		return realAppraisalDate.contains(appraisalDate);
	}

	/**
	 * Input Appraisal Draft Delivery Date
	 */
	public void inputAppraisalDraftDeliveryDate(String appraisalDraftDeliveryDate) {
		waitForControl(driver, Interfaces.TransactionPage.APPRAISAL_DRAFT_DELIVERY_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.APPRAISAL_DRAFT_DELIVERY_DATE_TEXTBOX, appraisalDraftDeliveryDate);
		sleep();
	}

	/**
	 * Check Appraisal Draft Delivery Date saved successfully
	 */
	public boolean isAppraisalDraftDeliveryDateSavedSuccessfully(String appraisalDraftDeliveryDate) {
		waitForControl(driver, Interfaces.TransactionPage.APPRAISAL_DRAFT_DELIVERY_DATE_TEXTBOX, timeWait);
		String realAppraisalDraftDeliveryDate = getAttributeValue(driver, Interfaces.TransactionPage.APPRAISAL_DRAFT_DELIVERY_DATE_TEXTBOX, "value");
		return realAppraisalDraftDeliveryDate.contains(appraisalDraftDeliveryDate);
	}

	/**
	 * Input Appraisal Final Delivery Date
	 */
	public void inputAppraisalFinalDeliveryDate(String appraisalFinalDeliveryDate) {
		waitForControl(driver, Interfaces.TransactionPage.APPRAISAL_FINAL_DELIVERY_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.APPRAISAL_FINAL_DELIVERY_DATE_TEXTBOX, appraisalFinalDeliveryDate);
		sleep();
	}

	/**
	 * Check Appraisal Final Delivery Date saved successfully
	 */
	public boolean isAppraisalFinalDeliveryDateSavedSuccessfully(String appraisalFinalDeliveryDate) {
		waitForControl(driver, Interfaces.TransactionPage.APPRAISAL_FINAL_DELIVERY_DATE_TEXTBOX, timeWait);
		String realAppraisalFinalDeliveryDate = getAttributeValue(driver, Interfaces.TransactionPage.APPRAISAL_FINAL_DELIVERY_DATE_TEXTBOX, "value");
		return realAppraisalFinalDeliveryDate.contains(appraisalFinalDeliveryDate);
	}

	/**
	 * select MAI Appraisal
	 */
	public void selectMAIAppraisal() {
		waitForControl(driver, Interfaces.TransactionPage.MAI_APPRAISAL_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.MAI_APPRAISAL_CHECKBOX);
		sleep();
	}

	/**
	 * Input Appraised Value
	 */
	public void inputAppraisedValue(String appraisedValue) {
		waitForControl(driver, Interfaces.TransactionPage.APPRAISAL_VALUE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.APPRAISAL_VALUE_TEXTBOX, appraisedValue);
		click(driver, Interfaces.TransactionPage.RAW_VALUE_TEXTBOX);
		sleep();
	}

	/**
	 * Check Appraised Value saved successfully
	 */
	public boolean isAppraisedValueSavedSuccessfully(String appraisedValue) {
		waitForControl(driver, Interfaces.TransactionPage.APPRAISAL_VALUE_TEXTBOX, timeWait);
		String realAppraisedValue = getAttributeValue(driver, Interfaces.TransactionPage.APPRAISAL_VALUE_TEXTBOX, "value");
		return realAppraisedValue.contains(appraisedValue);
	}

	/**
	 * Input Raw Land Value
	 */
	public void inputRawLandValue(String rawLandValue) {
		waitForControl(driver, Interfaces.TransactionPage.RAW_VALUE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.RAW_VALUE_TEXTBOX, rawLandValue);
		sleep();
	}

	/**
	 * Check Raw Land Value saved successfully
	 */
	public boolean isRawLandValueSavedSuccessfully(String rawLandValue) {
		waitForControl(driver, Interfaces.TransactionPage.RAW_VALUE_TEXTBOX, timeWait);
		String realRawLandValue = getAttributeValue(driver, Interfaces.TransactionPage.RAW_VALUE_TEXTBOX, "value");
		return realRawLandValue.contains(rawLandValue);
	}

	/**
	 * Input Cost Approach Value
	 */
	public void inputCostApproachValue(String costApproachValue) {
		waitForControl(driver, Interfaces.TransactionPage.COST_APPROACH_VALUE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.COST_APPROACH_VALUE_TEXTBOX, costApproachValue);
		sleep();
	}

	/**
	 * Check Cost Approach Value saved successfully
	 */
	public boolean isCostApproachValueSavedSuccessfully(String costApproachValue) {
		waitForControl(driver, Interfaces.TransactionPage.COST_APPROACH_VALUE_TEXTBOX, timeWait);
		String realCostApproachValue = getAttributeValue(driver, Interfaces.TransactionPage.COST_APPROACH_VALUE_TEXTBOX, "value");
		return realCostApproachValue.contains(costApproachValue);
	}

	/**
	 * Input Sales Comparison Value
	 */
	public void inputSalesComparisonValue(String salesComparisonValue) {
		waitForControl(driver, Interfaces.TransactionPage.SALES_COMPARISON_VALUE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SALES_COMPARISON_VALUE_TEXTBOX, salesComparisonValue);
		sleep();
	}

	/**
	 * Check Sales Comparison Value saved successfully
	 */
	public boolean isSalesComparisonValueSavedSuccessfully(String salesComparisonValue) {
		waitForControl(driver, Interfaces.TransactionPage.SALES_COMPARISON_VALUE_TEXTBOX, timeWait);
		String realSalesComparisonValue = getAttributeValue(driver, Interfaces.TransactionPage.SALES_COMPARISON_VALUE_TEXTBOX, "value");
		return realSalesComparisonValue.contains(salesComparisonValue);
	}

	/**
	 * Input Income Value
	 */
	public void inputIncomeValue(String incomeValue) {
		waitForControl(driver, Interfaces.TransactionPage.INCOME_VALUE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.INCOME_VALUE_TEXTBOX, incomeValue);
		sleep();
	}

	/**
	 * Check Income Value saved successfully
	 */
	public boolean isIncomeValueSavedSuccessfully(String incomeValue) {
		waitForControl(driver, Interfaces.TransactionPage.INCOME_VALUE_TEXTBOX, timeWait);
		String realIncomeValue = getAttributeValue(driver, Interfaces.TransactionPage.INCOME_VALUE_TEXTBOX, "value");
		return realIncomeValue.contains(incomeValue);
	}

	/**
	 * Input Replacement Cost New
	 */
	public void inputReplacementCostNew(String replacementCostNew) {
		waitForControl(driver, Interfaces.TransactionPage.REPLACEMENT_COST_NEW_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.REPLACEMENT_COST_NEW_TEXTBOX, replacementCostNew);
		sleep();
	}

	/**
	 * Check Replacement Cost New saved successfully
	 */
	public boolean isReplacementCostNewSavedSuccessfully(String replacementCostNew) {
		waitForControl(driver, Interfaces.TransactionPage.REPLACEMENT_COST_NEW_TEXTBOX, timeWait);
		String realReplacementCostNew = getAttributeValue(driver, Interfaces.TransactionPage.REPLACEMENT_COST_NEW_TEXTBOX, "value");
		return realReplacementCostNew.contains(replacementCostNew);
	}

	/**
	 * Input Appraisal Firm
	 */
	public void inputAppraisalFirm(String appraisalFirm) {
		waitForControl(driver, Interfaces.TransactionPage.APPRAISAL_FIRM_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.APPRAISAL_FIRM_TEXTBOX, appraisalFirm);
		sleep();
	}

	/**
	 * Check Appraisal Firm saved successfully
	 */
	public boolean isAppraisalFirmSavedSuccessfully(String appraisalFirm) {
		waitForControl(driver, Interfaces.TransactionPage.APPRAISAL_FIRM_TEXTBOX, timeWait);
		String realAppraisalFirm = getAttributeValue(driver, Interfaces.TransactionPage.APPRAISAL_FIRM_TEXTBOX, "value");
		return realAppraisalFirm.contains(appraisalFirm);
	}

	/**
	 * Select Appraisal Type combobox
	 */
	public void selectAppraisalType(String appraisalType) {
		waitForControl(driver, Interfaces.TransactionPage.APPRAISAL_TYPE_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.APPRAISAL_TYPE_DROPDOWN_LIST, appraisalType);
		sleep();
	}

	/**
	 * Check 'Appraisal Type' saved successfully
	 */
	public boolean isAppraisalTypeSavedSuccessfully(String appraisalType) {
		waitForControl(driver, Interfaces.TransactionPage.APPRAISAL_TYPE_DROPDOWN_LIST, timeWait);
		String realAppraisalType = getItemSelectedCombobox(driver, Interfaces.TransactionPage.APPRAISAL_TYPE_DROPDOWN_LIST);
		return realAppraisalType.contains(appraisalType);
	}

	/**
	 * select Current Appraisal
	 */
	public void selectCurrentAppraisal() {
		waitForControl(driver, Interfaces.TransactionPage.CURRENT_APPPRAISAL_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.CURRENT_APPPRAISAL_CHECKBOX);
		sleep();
	}

	/**
	 * Get value of Allocation Rate
	 */
	public String getValueAllocationRate() {
		sleep(2);
		waitForControl(driver, Interfaces.TransactionPage.ALLOCATION_RATE_STATUS_TITLE, timeWait);
		String allocationRateStatus = getText(driver, Interfaces.TransactionPage.ALLOCATION_RATE_STATUS_TITLE);
		return allocationRateStatus;
	}

	/**
	 * Input Sales Tax State Rate
	 */
	public void inputSalesTaxStateRate(String salesTaxStateRate) {
		waitForControl(driver, Interfaces.TransactionPage.SALES_TAX_STATE_RATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SALES_TAX_STATE_RATE_TEXTBOX, salesTaxStateRate);
		sleep();
	}

	/**
	 * Check Sales Tax State Rate saved successfully
	 */
	public boolean isSalesTaxStateRateSavedSuccessfully(String salesTaxStateRate) {
		waitForControl(driver, Interfaces.TransactionPage.SALES_TAX_STATE_RATE_TEXTBOX, timeWait);
		String realSalesTaxStateRate = getAttributeValue(driver, Interfaces.TransactionPage.SALES_TAX_STATE_RATE_TEXTBOX, "value");
		return realSalesTaxStateRate.contains(salesTaxStateRate);
	}

	/**
	 * Input Sales Tax County Rate
	 */
	public void inputSalesTaxCountyRate(String salesTaxCountyRate) {
		waitForControl(driver, Interfaces.TransactionPage.SALES_TAX_COUNTY_RATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SALES_TAX_COUNTY_RATE_TEXTBOX, salesTaxCountyRate);
		sleep();
	}

	/**
	 * Check Sales Tax County Rate saved successfully
	 */
	public boolean isSalesTaxCountyRateSavedSuccessfully(String salesTaxCountyRate) {
		waitForControl(driver, Interfaces.TransactionPage.SALES_TAX_COUNTY_RATE_TEXTBOX, timeWait);
		String realSalesTaxCountyRate = getAttributeValue(driver, Interfaces.TransactionPage.SALES_TAX_COUNTY_RATE_TEXTBOX, "value");
		return realSalesTaxCountyRate.contains(salesTaxCountyRate);
	}

	/**
	 * Input Sales Tax City Rate
	 */
	public void inputSalesTaxCityRate(String salesTaxCityRate) {
		waitForControl(driver, Interfaces.TransactionPage.SALES_TAX_CITY_RATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SALES_TAX_CITY_RATE_TEXTBOX, salesTaxCityRate);
		sleep();
	}

	/**
	 * Check Sales Tax City Rate saved successfully
	 */
	public boolean isSalesTaxCityRateSavedSuccessfully(String salesTaxCityRate) {
		waitForControl(driver, Interfaces.TransactionPage.SALES_TAX_CITY_RATE_TEXTBOX, timeWait);
		String realSalesTaxCityRate = getAttributeValue(driver, Interfaces.TransactionPage.SALES_TAX_CITY_RATE_TEXTBOX, "value");
		return realSalesTaxCityRate.contains(salesTaxCityRate);
	}

	/**
	 * Get value of Pre-Paid Rent
	 */
	public String getValuePrePaidRent() {
		sleep(3);
		waitForControl(driver, Interfaces.TransactionPage.PRE_PAID_RENT_TITLE, timeWait);
		String prePaidRent = getText(driver, Interfaces.TransactionPage.PRE_PAID_RENT_TITLE);
		return prePaidRent;
	}

	/**
	 * Input Pre-Paid Sales Tax
	 */
	public void inputPrePaidSalesTax(String prePaidSalesTax) {
		waitForControl(driver, Interfaces.TransactionPage.PRE_PAID_SALES_TAX_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.PRE_PAID_SALES_TAX_TEXTBOX, prePaidSalesTax);
		sleep();
	}

	/**
	 * Check Pre-Paid Sales Tax saved successfully
	 */
	public boolean isPrePaidSalesTaxSavedSuccessfully(String prePaidSalesTax) {
		waitForControl(driver, Interfaces.TransactionPage.PRE_PAID_SALES_TAX_TEXTBOX, timeWait);
		String realPrePaidSalesTax = getAttributeValue(driver, Interfaces.TransactionPage.PRE_PAID_SALES_TAX_TEXTBOX, "value");
		return realPrePaidSalesTax.contains(prePaidSalesTax);
	}

	/**
	 * select Subleased
	 */
	public void selectSubleased() {
		waitForControl(driver, Interfaces.TransactionPage.SUBLEASED_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.SUBLEASED_CHECKBOX);
		sleep();
	}

	/**
	 * Input Subtenant
	 */
	public void inputSubtenant(String subtenant) {
		waitForControl(driver, Interfaces.TransactionPage.SUBTENANT_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SUBTENANT_TEXTBOX, subtenant);
		sleep();
	}

	/**
	 * Check Subtenant saved successfully
	 */
	public boolean isSubtenantSavedSuccessfully(String subtenant) {
		waitForControl(driver, Interfaces.TransactionPage.SUBTENANT_TEXTBOX, timeWait);
		String realSubtenant = getAttributeValue(driver, Interfaces.TransactionPage.SUBTENANT_TEXTBOX, "value");
		return realSubtenant.contains(subtenant);
	}

	/**
	 * select Ground Lease Obligation
	 */
	public void selectGroundLeaseObligation() {
		waitForControl(driver, Interfaces.TransactionPage.GROUND_LEASE_OBLIGATION_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.GROUND_LEASE_OBLIGATION_CHECKBOX);
		sleep();
	}

	/**
	 * Input Ground Lease Monthly Rent
	 */
	public void inputGroundLeaseMonthlyRent(String groundLeaseMonthlyRent) {
		waitForControl(driver, Interfaces.TransactionPage.GROUND_LEASE_MONTHLY_RENT_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.GROUND_LEASE_MONTHLY_RENT_TEXTBOX, groundLeaseMonthlyRent);
		sleep();
	}

	/**
	 * Check Ground Lease Monthly Rent saved successfully
	 */
	public boolean isGroundLeaseMonthlyRentSavedSuccessfully(String groundLeaseMonthlyRent) {
		waitForControl(driver, Interfaces.TransactionPage.GROUND_LEASE_MONTHLY_RENT_TEXTBOX, timeWait);
		String realGroundLeaseMonthlyRent = getAttributeValue(driver, Interfaces.TransactionPage.GROUND_LEASE_MONTHLY_RENT_TEXTBOX, "value");
		return realGroundLeaseMonthlyRent.contains(groundLeaseMonthlyRent);
	}

	/**
	 * select Property Liquor Sold
	 */
	public void selectPropertyLiquorSold() {
		waitForControl(driver, Interfaces.TransactionPage.PROPERTY_LIQUOR_SOLD_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.PROPERTY_LIQUOR_SOLD_CHECKBOX);
		sleep();
	}

	/**
	 * select Property Personal Property Lien
	 */
	public void selectPropertyPersonalPropertyLiens() {
		waitForControl(driver, Interfaces.TransactionPage.PROPERTY_PERSONAL_PROPERTY_LIEN_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.PROPERTY_PERSONAL_PROPERTY_LIEN_CHECKBOX);
		sleep();
	}

	/**
	 * select CAM
	 */
	public void selectCAM() {
		waitForControl(driver, Interfaces.TransactionPage.CAM_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.CAM_CHECKBOX);
		sleep();
	}

	/**
	 * select Lien Subordinated
	 */
	public void selectLienSubordinated() {
		waitForControl(driver, Interfaces.TransactionPage.LIEN_SUBORDINATED_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.LIEN_SUBORDINATED_CHECKBOX);
		sleep();
	}

	/**
	 * Input Subordinated To
	 */
	public void inputSubordinatedTo(String subordinatedTo) {
		waitForControl(driver, Interfaces.TransactionPage.SUBORDINATED_TO_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SUBORDINATED_TO_TEXTBOX, subordinatedTo);
		sleep();
	}

	/**
	 * Check Subordinated To saved successfully
	 */
	public boolean isSubordinatedToSavedSuccessfully(String subordinatedTo) {
		waitForControl(driver, Interfaces.TransactionPage.SUBORDINATED_TO_TEXTBOX, timeWait);
		String realSubordinatedTo = getAttributeValue(driver, Interfaces.TransactionPage.SUBORDINATED_TO_TEXTBOX, "value");
		return realSubordinatedTo.contains(subordinatedTo);
	}

	/**
	 * Input Subordination End Date
	 */
	public void inputSubordinationEndDate(String subordinationEndDate) {
		waitForControl(driver, Interfaces.TransactionPage.SUBORDINATED_END_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SUBORDINATED_END_DATE_TEXTBOX, subordinationEndDate);
		sleep();
	}

	/**
	 * Check Subordination End Date saved successfully
	 */
	public boolean isSubordinationEndDateSavedSuccessfully(String subordinationEndDate) {
		waitForControl(driver, Interfaces.TransactionPage.SUBORDINATED_END_DATE_TEXTBOX, timeWait);
		String realSubordinationEndDate = getAttributeValue(driver, Interfaces.TransactionPage.SUBORDINATED_END_DATE_TEXTBOX, "value");
		return realSubordinationEndDate.contains(subordinationEndDate);
	}

	/**
	 * Input Original Survey Date
	 */
	public void inputOriginalSurveyDate(String originalSurveyDate) {
		waitForControl(driver, Interfaces.TransactionPage.SUBORDINATED_SURVEY_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SUBORDINATED_SURVEY_DATE_TEXTBOX, originalSurveyDate);
		sleep();
	}

	/**
	 * Check Original Survey Date saved successfully
	 */
	public boolean isOriginalSurveyDateSavedSuccessfully(String originalSurveyDate) {
		waitForControl(driver, Interfaces.TransactionPage.SUBORDINATED_SURVEY_DATE_TEXTBOX, timeWait);
		String realOriginalSurveyDate = getAttributeValue(driver, Interfaces.TransactionPage.SUBORDINATED_SURVEY_DATE_TEXTBOX, "value");
		return realOriginalSurveyDate.contains(originalSurveyDate);
	}

	/**
	 * select Current Survey
	 */
	public void selectCurrentSurvey() {
		waitForControl(driver, Interfaces.TransactionPage.CURRENT_SURVEY_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.CURRENT_SURVEY_CHECKBOX);
		sleep();
	}

	/**
	 * Input Revision Date
	 */
	public void inputRevisionDate(String revisionDate) {
		waitForControl(driver, Interfaces.TransactionPage.REVISION_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.REVISION_DATE_TEXTBOX, revisionDate);
		sleep();
	}

	/**
	 * Check Revision Date saved successfully
	 */
	public boolean isRevisionDateSavedSuccessfully(String revisionDate) {
		waitForControl(driver, Interfaces.TransactionPage.REVISION_DATE_TEXTBOX, timeWait);
		String realRevisionDate = getAttributeValue(driver, Interfaces.TransactionPage.REVISION_DATE_TEXTBOX, "value");
		return realRevisionDate.contains(revisionDate);
	}

	/**
	 * select Final Revision
	 */
	public void selectFinalRevision() {
		waitForControl(driver, Interfaces.TransactionPage.FINAL_REVISION_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.FINAL_REVISION_CHECKBOX);
		sleep();
	}

	/**
	 * select Zoning Restriction
	 */
	public void selectZoningRestriction() {
		waitForControl(driver, Interfaces.TransactionPage.ZONING_RESTRICTION_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.ZONING_RESTRICTION_CHECKBOX);
		sleep();
	}

	/**
	 * Input Zoning Restriction Description
	 */
	public void inputZoningRestrictionDescription(String revisionDate) {
		waitForControl(driver, Interfaces.TransactionPage.ZONING_RESTRICTION_DESCRIPTION_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.ZONING_RESTRICTION_DESCRIPTION_TEXTBOX, revisionDate);
		sleep();
	}

	/**
	 * Check Zoning Restriction Description saved successfully
	 */
	public boolean isZoningRestrictionDescriptionSavedSuccessfully(String revisionDate) {
		waitForControl(driver, Interfaces.TransactionPage.ZONING_RESTRICTION_DESCRIPTION_TEXTBOX, timeWait);
		String realRevisionDate = getAttributeValue(driver, Interfaces.TransactionPage.ZONING_RESTRICTION_DESCRIPTION_TEXTBOX, "value");
		return realRevisionDate.contains(revisionDate);
	}

	/**
	 * Input Land Sq. Ft.
	 */
	public void inputLandSqFt(String landSqFt) {
		waitForControl(driver, Interfaces.TransactionPage.LAND_SQ_FT_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.LAND_SQ_FT_TEXTBOX, landSqFt);
		sleep();
	}

	/**
	 * Check Land Sq. Ft. saved successfully
	 */
	public boolean isLandSqFtSavedSuccessfully(String landSqFt) {
		waitForControl(driver, Interfaces.TransactionPage.LAND_SQ_FT_TEXTBOX, timeWait);
		String realLandSqFt = getAttributeValue(driver, Interfaces.TransactionPage.LAND_SQ_FT_TEXTBOX, "value");
		return realLandSqFt.contains(landSqFt);
	}

	/**
	 * Input Building Sq. Ft.
	 */
	public void inputBuildingSqFt(String buildingSqFt) {
		waitForControl(driver, Interfaces.TransactionPage.BUILDING_SQ_FT_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.BUILDING_SQ_FT_TEXTBOX, buildingSqFt);
		sleep();
	}

	/**
	 * Check Building Sq. Ft. saved successfully
	 */
	public boolean isBuildingSqFtSavedSuccessfully(String buildingSqFt) {
		waitForControl(driver, Interfaces.TransactionPage.BUILDING_SQ_FT_TEXTBOX, timeWait);
		String realBuildingSqFt = getAttributeValue(driver, Interfaces.TransactionPage.BUILDING_SQ_FT_TEXTBOX, "value");
		return realBuildingSqFt.contains(buildingSqFt);
	}

	/**
	 * select Is Sq. Ft. Final
	 */
	public void selectIsSqFtFinal() {
		waitForControl(driver, Interfaces.TransactionPage.IS_SQ_FT_FINAL_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.IS_SQ_FT_FINAL_CHECKBOX);
		sleep();
	}

	/**
	 * select Flood Zone
	 */
	public void selectFloodZone() {
		waitForControl(driver, Interfaces.TransactionPage.IS_FLOOD_ZONE_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.IS_FLOOD_ZONE_CHECKBOX);
		sleep();
	}

	/**
	 * select Earthquake Zone
	 */
	public void selectEarthquakeZone() {
		waitForControl(driver, Interfaces.TransactionPage.EARTHQUAKE_ZONE_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.EARTHQUAKE_ZONE_CHECKBOX);
		sleep();
	}

	/**
	 * select Wind Zone
	 */
	public void selectWindZone() {
		waitForControl(driver, Interfaces.TransactionPage.WIND_ZONE_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.WIND_ZONE_CHECKBOX);
		sleep();
	}

	/**
	 * Input Environmental Data Effective Date
	 */
	public void inputEnvironmentalDataEffectiveDate(String environmentalDataEffectiveDate) {
		waitForControl(driver, Interfaces.TransactionPage.ENVIRONMENTAL_EFFECTIVE_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.ENVIRONMENTAL_EFFECTIVE_DATE_TEXTBOX, environmentalDataEffectiveDate);
		sleep();
	}

	/**
	 * Check Environmental Data Effective Date saved successfully
	 */
	public boolean isEnvironmentalDataEffectiveDateSavedSuccessfully(String environmentalDataEffectiveDate) {
		waitForControl(driver, Interfaces.TransactionPage.ENVIRONMENTAL_EFFECTIVE_DATE_TEXTBOX, timeWait);
		String realEnvironmentalDataEffectiveDate = getAttributeValue(driver, Interfaces.TransactionPage.ENVIRONMENTAL_EFFECTIVE_DATE_TEXTBOX, "value");
		return realEnvironmentalDataEffectiveDate.contains(environmentalDataEffectiveDate);
	}

	/**
	 * select Phase I
	 */
	public void selectPhaseI() {
		waitForControl(driver, Interfaces.TransactionPage.PHASE_I_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.PHASE_I_CHECKBOX);
		sleep();
	}

	/**
	 * select Phase II
	 */
	public void selectPhaseII() {
		waitForControl(driver, Interfaces.TransactionPage.PHASE_II_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.PHASE_II_CHECKBOX);
		sleep();
	}

	/**
	 * Input Phase I Date
	 */
	public void inputPhaseIDate(String phaseIDate) {
		waitForControl(driver, Interfaces.TransactionPage.PHASE_I_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.PHASE_I_DATE_TEXTBOX, phaseIDate);
		sleep();
	}

	/**
	 * Check Phase I Date saved successfully
	 */
	public boolean isPhaseIDateSavedSuccessfully(String phaseIDate) {
		waitForControl(driver, Interfaces.TransactionPage.PHASE_I_DATE_TEXTBOX, timeWait);
		String realPhaseIDate = getAttributeValue(driver, Interfaces.TransactionPage.PHASE_I_DATE_TEXTBOX, "value");
		return realPhaseIDate.contains(phaseIDate);
	}

	/**
	 * Input Phase II Date
	 */
	public void inputPhaseIIDate(String phaseIIDate) {
		waitForControl(driver, Interfaces.TransactionPage.PHASE_II_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.PHASE_II_DATE_TEXTBOX, phaseIIDate);
		sleep();
	}

	/**
	 * Check Phase II Date saved successfully
	 */
	public boolean isPhaseIIDateSavedSuccessfully(String phaseIIDate) {
		waitForControl(driver, Interfaces.TransactionPage.PHASE_II_DATE_TEXTBOX, timeWait);
		String realPhaseIIDate = getAttributeValue(driver, Interfaces.TransactionPage.PHASE_II_DATE_TEXTBOX, "value");
		return realPhaseIIDate.contains(phaseIIDate);
	}

	/**
	 * Input Phase II Date Result
	 */
	public void inputPhaseIIDateResult(String phaseIIDateResult) {
		waitForControl(driver, Interfaces.TransactionPage.PHASE_II_RESULT_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.PHASE_II_RESULT_TEXTBOX, phaseIIDateResult);
		sleep();
	}

	/**
	 * Check Phase II Date Result saved successfully
	 */
	public boolean isPhaseIIDateResultSavedSuccessfully(String phaseIIDateResult) {
		waitForControl(driver, Interfaces.TransactionPage.PHASE_II_RESULT_TEXTBOX, timeWait);
		String realPhaseIIDateResult = getAttributeValue(driver, Interfaces.TransactionPage.PHASE_II_RESULT_TEXTBOX, "value");
		return realPhaseIIDateResult.contains(phaseIIDateResult);
	}

	/**
	 * select Flood Zone Insurance Required
	 */
	public void selectFloodZoneInsuranceRequiredI() {
		waitForControl(driver, Interfaces.TransactionPage.FLOOD_ZONE_INSURANCE_REQUIRED_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.FLOOD_ZONE_INSURANCE_REQUIRED_CHECKBOX);
		sleep();
	}

	/**
	 * select Earthquake Insurance Required
	 */
	public void selectEarthquakeInsuranceRequired() {
		waitForControl(driver, Interfaces.TransactionPage.EARTHQUAKE_INSURANCE_REQUIRED_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.EARTHQUAKE_INSURANCE_REQUIRED_CHECKBOX);
		sleep();
	}

	/**
	 * select Wind Insurance Required
	 */
	public void selectWindInsuranceRequired() {
		waitForControl(driver, Interfaces.TransactionPage.WIND_INSURANCE_REQUIRED_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.WIND_INSURANCE_REQUIRED_CHECKBOX);
		sleep();
	}

	/**
	 * select Self Insurance Allowed
	 */
	public void selectSelfInsuranceAllowed() {
		waitForControl(driver, Interfaces.TransactionPage.SELF_INSURANCE_ALLOWED_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.SELF_INSURANCE_ALLOWED_CHECKBOX);
		sleep();
	}

	/**
	 * select Business Interruption/Rental Value Insurance Required
	 */
	public void selectBusinessInterruptionRentalValueInsuranceRequired() {
		waitForControl(driver, Interfaces.TransactionPage.BUSINESS_INTERRUPTION_RENTAL_VALUE_INSURANCE_REQUIRED_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.BUSINESS_INTERRUPTION_RENTAL_VALUE_INSURANCE_REQUIRED_CHECKBOX);
		sleep();
	}

	/**
	 * select Environmental Insurance
	 */
	public void selectEnvironmentalInsurance() {
		waitForControl(driver, Interfaces.TransactionPage.ENVIRONMENTAL_INSURANCE_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.ENVIRONMENTAL_INSURANCE_CHECKBOX);
		sleep();
	}

	/**
	 * Input Environmental Insurance Policy Date
	 */
	public void inputEnvironmentalInsurancePolicyDate(String environmentalInsurancePolicyDate) {
		waitForControl(driver, Interfaces.TransactionPage.ENVIRONMENTAL_INSURANCE_POLICY_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.ENVIRONMENTAL_INSURANCE_POLICY_DATE_TEXTBOX, environmentalInsurancePolicyDate);
		sleep();
	}

	/**
	 * Check Phase II Date Result saved successfully
	 */
	public boolean isEnvironmentalInsurancePolicyDateSavedSuccessfully(String environmentalInsurancePolicyDate) {
		waitForControl(driver, Interfaces.TransactionPage.ENVIRONMENTAL_INSURANCE_POLICY_DATE_TEXTBOX, timeWait);
		String realEnvironmentalInsurancePolicyDate = getAttributeValue(driver, Interfaces.TransactionPage.ENVIRONMENTAL_INSURANCE_POLICY_DATE_TEXTBOX, "value");
		return realEnvironmentalInsurancePolicyDate.contains(environmentalInsurancePolicyDate);
	}

	/**
	 * Input Environmental Insurance Expiration Date
	 */
	public void inputEnvironmentalInsuranceExpirationDate(String environmentalInsuranceExpirationDate) {
		waitForControl(driver, Interfaces.TransactionPage.ENVIRONMENTAL_INSURANCE_EXPIRATION_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.ENVIRONMENTAL_INSURANCE_EXPIRATION_DATE_TEXTBOX, environmentalInsuranceExpirationDate);
		sleep();
	}

	/**
	 * Check Environmental Insurance Expiration Date saved successfully
	 */
	public boolean isEnvironmentalInsuranceExpirationDateSavedSuccessfully(String environmentalInsuranceExpirationDate) {
		waitForControl(driver, Interfaces.TransactionPage.ENVIRONMENTAL_INSURANCE_EXPIRATION_DATE_TEXTBOX, timeWait);
		String realEnvironmentalInsuranceExpirationDate = getAttributeValue(driver, Interfaces.TransactionPage.ENVIRONMENTAL_INSURANCE_EXPIRATION_DATE_TEXTBOX, "value");
		return realEnvironmentalInsuranceExpirationDate.contains(environmentalInsuranceExpirationDate);
	}

	/**
	 * Input Environmental Insurer
	 */
	public void inputEnvironmentalInsurer(String environmentalInsurer) {
		waitForControl(driver, Interfaces.TransactionPage.ENVIRONMENTAL_INSURER_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.ENVIRONMENTAL_INSURER_TEXTBOX, environmentalInsurer);
		sleep();
	}

	/**
	 * Check Environmental Insurer saved successfully
	 */
	public boolean isEnvironmentalInsurerSavedSuccessfully(String environmentalInsurer) {
		waitForControl(driver, Interfaces.TransactionPage.ENVIRONMENTAL_INSURER_TEXTBOX, timeWait);
		String realEnvironmentalInsurer = getAttributeValue(driver, Interfaces.TransactionPage.ENVIRONMENTAL_INSURER_TEXTBOX, "value");
		return realEnvironmentalInsurer.contains(environmentalInsurer);
	}

	/**
	 * select Future Modification Planned
	 */
	public void selectFutureModificationPlanned() {
		waitForControl(driver, Interfaces.TransactionPage.FUTURE_MODIFICATION_PLANNED_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.FUTURE_MODIFICATION_PLANNED_CHECKBOX);
		sleep();
	}

	/**
	 * Input Planned Modification Type
	 */
	public void inputPlannedModificationType(String plannedModificationType) {
		waitForControl(driver, Interfaces.TransactionPage.ENVIRONMENTAL_INSURER_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.ENVIRONMENTAL_INSURER_TEXTBOX, plannedModificationType);
		sleep();
	}

	/**
	 * Check Planned Modification Type saved successfully
	 */
	public boolean isPlannedModificationTypeSavedSuccessfully(String plannedModificationType) {
		waitForControl(driver, Interfaces.TransactionPage.PLANNED_MODIFICATION_TYPE_TEXTBOX, timeWait);
		String realPlannedModificationType = getAttributeValue(driver, Interfaces.TransactionPage.PLANNED_MODIFICATION_TYPE_TEXTBOX, "value");
		return realPlannedModificationType.contains(plannedModificationType);
	}

	/**
	 * Input Planned Modification Comments
	 */
	public void inputPlannedModificationComments(String plannedModificationComments) {
		waitForControl(driver, Interfaces.TransactionPage.PLANNED_MODIFICATION_COMMENTS_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.PLANNED_MODIFICATION_COMMENTS_TEXTBOX, plannedModificationComments);
		sleep();
	}

	/**
	 * Check Planned Modification Comments saved successfully
	 */
	public boolean isPlannedModificationCommentsSavedSuccessfully(String plannedModificationComments) {
		waitForControl(driver, Interfaces.TransactionPage.PLANNED_MODIFICATION_COMMENTS_TEXTBOX, timeWait);
		String realPlannedModificationComments = getText(driver, Interfaces.TransactionPage.PLANNED_MODIFICATION_COMMENTS_TEXTBOX);
		return realPlannedModificationComments.contains(plannedModificationComments);
	}

	/**
	 * Click Goto Properties Button At Property Details
	 */
	public void clickGotoPropertiesButtonAtPropertyDetails() {
		waitForControl(driver, Interfaces.TransactionPage.PROPERTY_DETAIL_GO_TO_PROPERTIES_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('GotoProperties').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * Click Goto Transactions Button At Property Details
	 */
	public void clickGotoTransactionButtonAtPropertyDetails() {
		waitForControl(driver, Interfaces.TransactionPage.GO_TO_TRANSACTION_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('GotoLease').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * Click New Property Documents Button
	 */
	public void clickNewPropertyDocumentsButton() {
		waitForControl(driver, Interfaces.TransactionPage.PROPERTY_DOCUMENTS_NEW_BUTTON, timeWait);
		WebElement element = driver.findElement(By.id("PropertyLoanApplicationDetails_R1_EnvironmentalInsuer"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
		click(driver, Interfaces.TransactionPage.PROPERTY_DOCUMENTS_NEW_BUTTON);
		sleep(2);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(6);
		}
	}

	/**
	 * select Document Type
	 */
	public void selectDocumentType(String documentType) {
		waitForControl(driver, Interfaces.TransactionPage.DOCUMENT_TYPE_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.DOCUMENT_TYPE_COMBOBOX, documentType);
		sleep();
	}

	/**
	 * Input Other Document Type
	 */
	public void inputOtherDocumentType(String documentType) {
		waitForControl(driver, Interfaces.TransactionPage.OTHER_DOCUMENT_TYPE_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.OTHER_DOCUMENT_TYPE_TEXTBOX, documentType);
		sleep();
	}

	/**
	 * Click Go To Property button At Document Type Details
	 */
	public void clickGoToPropertyButtonAtDocumentTypeDetails() {
		waitForControl(driver, Interfaces.TransactionPage.DOCUMENT_TYPE_DETAIL_GO_TO_PROPERTY_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('GotoLAProp').click();");
	}

	/**
	 * Click Go To Transaction button At Document Type Details
	 */
	public void clickGoToTransactionButtonAtDocumentTypeDetails() {
		waitForControl(driver, Interfaces.TransactionPage.DOCUMENT_TYPE_DETAIL_GO_TO_TRANSACTION_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('GotoLoan').click();");
	}

	/**
	 * Check Document Type created successfully [No document file name]
	 */
	public boolean isDocumentTypeCreatedSuccessfully(String docType) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_TYPE_ON_PROPERTY_DETAIL, docType, timeWait);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_TYPE_ON_PROPERTY_DETAIL, docType);
	}

	/**
	 * Check Document file name loaded to Document type
	 */
	public boolean isDocumentLoadedToDocumentType(String docType, String fileName) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL, docType, fileName, timeWait);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL, docType, fileName);
	}

	/**
	 * Delete Document Type [No file name]
	 */
	public void deleteDocumentTypeNotFileName(String documentType) {
		WebElement element = driver.findElement(By.id("PropertyLoanApplicationDetails_R1_EnvironmentalInsuer"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_DELETE_DOCUMENT_TYPE_NOT_FILENAME_CHECKBOX, documentType, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.DYNAMIC_DELETE_DOCUMENT_TYPE_NOT_FILENAME_CHECKBOX, documentType);
		sleep();
	}

	/**
	 * Upload Document File Name
	 */
	public void uploadDocumentFileName(String fileName) {
		waitForControl(driver, Interfaces.TransactionPage.CHOOSE_FILE_DOCUMENT_BUTTON, timeWait);
		doubleClick(driver, Interfaces.TransactionPage.CHOOSE_FILE_DOCUMENT_BUTTON);
		sleep();
		Common.getCommon().openFileForUpload(driver, fileName);
		sleep();
	}

	/**
	 * Upload dynamic Document file name by placeholder
	 */
	public void uploadDynamicDocumentFileByPlaceHolder(String documentType, String fileName) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_CHOOSE_FILE_BUTTON_PLACE_HOLDER, documentType, timeWait);
		doubleClick(driver, Interfaces.TransactionPage.DYNAMIC_CHOOSE_FILE_BUTTON_PLACE_HOLDER, documentType);
		sleep();
		Common.getCommon().openFileForUpload(driver, fileName);
		sleep();
	}

	/**
	 * Check Document History loaded to Document File, Submit Via, Submitted On
	 */
	public boolean isDocumentHistoryLoadedToDocumentType(String documentFile, String submitVia, String submitOn) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_HISTORY_DOCUMENT_LOADED_FOR_DOCTYPE, documentFile, submitVia, submitOn, timeWait);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_HISTORY_DOCUMENT_LOADED_FOR_DOCTYPE, documentFile, submitVia, submitOn);
	}

	/**
	 * Check Required Document loaded to Document column
	 */
	public boolean isRequiredDocumentLoadedToDocumentColumn(String address, String documentColumn) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_REQUIRED_DOCUMENT_FOR_DOCUMENT_COLUMN, address, documentColumn, timeWait);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_REQUIRED_DOCUMENT_FOR_DOCUMENT_COLUMN, address, documentColumn);
	}

	/**
	 * Check Property display on search Properties page
	 */
	public boolean isPropertyDisplayOnSearchPropertiesPage(String pNumber, String propertyID, String address, String city, String state, String zipCode, String activeStatus) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_SEARCH_PROPERTY_ITEM, pNumber, propertyID, address, city, state, zipCode, activeStatus, timeWait);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_SEARCH_PROPERTY_ITEM, pNumber, propertyID, address, city, state, zipCode, activeStatus);
	}

	/**
	 * Get Property ID of Address In Properties Tab
	 */
	public String getPropertyIDOfAddressInPropertiesTab(String address) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_GET_PROPERTY_ID_NUMBER, address, timeWait);
		String text = getText(driver, Interfaces.TransactionPage.DYNAMIC_GET_PROPERTY_ID_NUMBER, address);
		return text;
	}

	/**
	 * Click Export Data Tape button
	 */
	public void clickExportDatatapeButton() {
		waitForControl(driver, Interfaces.TransactionPage.EXPORT_DATATAPE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('ExportDataTape').click();");
		sleep();
	}

	/**
	 * Click Export Data Tape radio button
	 */
	public void clickExportAllPropertiesRadioButton(String value) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_EXPORT_DATA_TAPE_RADIO_BUTTON, value, timeWait);
		doubleClick(driver, Interfaces.TransactionPage.DYNAMIC_EXPORT_DATA_TAPE_RADIO_BUTTON, value);
		sleep(2);
		doubleClick(driver, Interfaces.TransactionPage.OK_EXPORT_DATATAPE_BUTTON);
		sleep(2);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
			keyPressing("alt_s");
			keyPressing("alt_s");
			keyPressing("alt_s");
		}
	}

	/**
	 * Search Property Documents with Document#
	 */
	public void searchPropertyDocumentsDocumentNumber(String documentNumber) {
		waitForControl(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_DOCUMENTS_DOCUMENT_NUMBER_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_DOCUMENTS_DOCUMENT_NUMBER_TEXTBOX, documentNumber);
		sleep();
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep(3);
	}

	/**
	 * Search Property Documents with Address
	 */
	public void searchPropertyDocumentsAddress(String address) {
		waitForControl(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_DOCUMENTS_ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_DOCUMENTS_ADDRESS_TEXTBOX, address);
		sleep();
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep(3);
	}

	/**
	 * Search Property Documents with Document Loaded
	 */
	public void searchPropertyDocumentsDocumentLoaded(String address, String documentLoaded) {
		waitForControl(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_DOCUMENTS_ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SEARCH_PROPERTY_DOCUMENTS_ADDRESS_TEXTBOX, address);
		sleep(1);
		selectItemCombobox(driver, Interfaces.TransactionPage.DOCUMENT_LOADED_DROPDOWN_LIST, documentLoaded);
		sleep();
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep(3);
	}


	/**
	 * Search [Transaction Documents - Lessee] with Document Loaded
	 */
	public void searchTransactionLesseeDocumentLoaded(String section, String documentLoaded) {
		waitForControl(driver, Interfaces.TransactionPage.SEARCH_SECTION_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.SEARCH_SECTION_COMBOBOX, section);
		sleep(1);
		selectItemCombobox(driver, Interfaces.TransactionPage.DOCUMENT_LOADED_DROPDOWN_LIST, documentLoaded);
		sleep();
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep(3);
	}
	
	/**
	 * Search Property Documents with Document Type
	 */
	public void searchPropertyDocumentType(String documentType) {
		waitForControl(driver, Interfaces.TransactionPage.SEARCH_DOCUMENT_TYPE_DROPDOWN, timeWait);
		click(driver, Interfaces.TransactionPage.SEARCH_DOCUMENT_TYPE_DROPDOWN);
		sleep(1);
		waitForControl(driver, Interfaces.TransactionPage.UNCHECK_ALL_LINK, timeWait);
		click(driver, Interfaces.TransactionPage.UNCHECK_ALL_LINK);
		sleep(1);
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_TYPE_CHECKBOX, documentType, timeWait);
		click(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_TYPE_CHECKBOX, documentType);
		sleep(1);
		click(driver, Interfaces.TransactionPage.SEARCH_DOCUMENT_TYPE_DROPDOWN);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(7);
		}
		sleep();
	}

	/**
	 * Search Section and Document Type [Transaction Documents - Lessee]
	 */
	public void searchSectionAndDocumentType(String documentSection, String documentType) {
		waitForControl(driver, Interfaces.TransactionPage.SEARCH_SECTION_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.TransactionPage.SEARCH_SECTION_COMBOBOX, documentSection);
		sleep();
		if (!documentType.equals("")) {
			waitForControl(driver, Interfaces.TransactionPage.SEARCH_DOCUMENT_TYPE_DROPDOWN, timeWait);
			click(driver, Interfaces.TransactionPage.SEARCH_DOCUMENT_TYPE_DROPDOWN);
			sleep(1);
			waitForControl(driver, Interfaces.TransactionPage.UNCHECK_ALL_LINK, timeWait);
			click(driver, Interfaces.TransactionPage.UNCHECK_ALL_LINK);
			sleep(1);
			waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_TYPE_CHECKBOX, documentType, timeWait);
			click(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_TYPE_CHECKBOX, documentType);
			sleep(1);
			click(driver, Interfaces.TransactionPage.SEARCH_DOCUMENT_TYPE_DROPDOWN);
			sleep(1);
		}
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(7);
		}
		sleep();
	}
	
	/**
	 * Search with Custom Type [Transaction Documents - Lessee]
	 */
	public void searchWithCustomType( String documentType) {
		waitForControl(driver, Interfaces.TransactionPage.SEARCH_TRANSACTION_LESSEE_CUSTOM_DOCUMENT_TEXTBOX, timeWait);
		type(driver, Interfaces.TransactionPage.SEARCH_TRANSACTION_LESSEE_CUSTOM_DOCUMENT_TEXTBOX, documentType);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(7);
		}
		sleep();
	}

	/**
	 * Get DocumentID In [Property Documents - Transaction Documents - Lessee]
	 * tab
	 */
	public String getDocumentID() {
		waitForControl(driver, Interfaces.TransactionPage.GET_DOCUMENT_ID, timeWait);
		String documentID = getText(driver, Interfaces.TransactionPage.GET_DOCUMENT_ID);
		return documentID;
	}

	/**
	 * Check Document type display on search [Property Documents - Transaction Documents - Lessee] tab with document loaded Yes
	 */
	public boolean isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(String documentID, String address, String documentType, String documentLoaded, String activeStatus) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_SEARCH_LOADED_YES_PROPERTY_TRANSACTION_LESSEE_TAB, documentID, address, documentType, documentLoaded, activeStatus,timeWait);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_SEARCH_LOADED_YES_PROPERTY_TRANSACTION_LESSEE_TAB, documentID, address, documentType, documentLoaded, activeStatus);
	}
	
	/**
	 * Check Document type display on search [Property Documents - Transaction Documents - Lessee] tab with document loaded No
	 */
	public boolean isDocumentTypeNoDisplayOnSearchPropertyTransactionLesseeTab(String address, String documentType, String documentLoaded, String activeStatus) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_SEARCH_LOADED_NO_PROPERTY_TRANSACTION_LESSEE_TAB, address,documentType, documentLoaded, activeStatus,timeWait);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_SEARCH_LOADED_NO_PROPERTY_TRANSACTION_LESSEE_TAB, address, documentType, documentLoaded,activeStatus);
	}

	/**
	 * click New [Transaction Documents - Lessee] button
	 */
	public void clickNewTransactionDocumentLesseeButton() {
		waitForControl(driver, Interfaces.TransactionPage.NEW_TRANSACTION_DOCUMENTS_LESSEE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('NewLeaseDoc').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}
	
	/**
	 * Select [Transaction Documents - Lessee] Section
	 */
	public void selectTransactionDocumentLesseeSection(String documentSection) {
		waitForControl(driver, Interfaces.TransactionPage.GENERAL_DOCUMENT_SECTION_CLICK, timeWait);
		click(driver, Interfaces.TransactionPage.GENERAL_DOCUMENT_SECTION_CLICK);
		sleep(2);
		waitForControl(driver, Interfaces.TransactionPage.GENERAL_DOCUMENT_SECTION_SELECT, documentSection, timeWait);
		click(driver, Interfaces.TransactionPage.GENERAL_DOCUMENT_SECTION_SELECT, documentSection);
		sleep(1);
	}

	/**
	 * Select [Transaction Documents - Lessee] Document Type
	 */
	public void selectTransactionDocumentLesseeDocumentType(String documentType) {
		waitForControl(driver, Interfaces.TransactionPage.GENERAL_DOCUMENT_TYPE, timeWait);
		sleep(1);
		selectItemCombobox(driver, Interfaces.TransactionPage.GENERAL_DOCUMENT_TYPE, documentType);
		sleep();
	}
	
	/**
	 * Click Document List button At Document Type Details
	 */
	public void clickDocumentListButtonAtDocumentTypeDetails() {
		waitForControl(driver, Interfaces.TransactionPage.DOCUMENT_TYPE_DETAIL_GO_TO_PROPERTY_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('GotoLAList').click();");
	}
	
	/**
	 * Check Document Uploaded To Document Type [Transaction Documents - Lessee]
	 */
	public boolean isDocumentUploadedToDocumentType(String documentType) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_LOADED_IN_TRANSACTION_LESSEE_TAB, documentType, timeWait);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_LOADED_IN_TRANSACTION_LESSEE_TAB, documentType);
	}
	

	/**
	 * Open Document detail [Transaction Documents - Lessee]
	 */
	public void openDocumentDetail(String documentType) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_OPEN_DOCUMENT_TYPE_DETAIL_TRANSACTION_LESSEE_TAB, documentType, timeWait);
		click(driver, Interfaces.TransactionPage.DYNAMIC_OPEN_DOCUMENT_TYPE_DETAIL_TRANSACTION_LESSEE_TAB, documentType);
		sleep();
	}
	
	/**
	 * Uncheck Private Checkbox
	 */
	public void uncheckPrivateCheckbox() {
		waitForControl(driver, Interfaces.TransactionPage.PRIVATE_CHECKBOX, timeWait);
		uncheckTheCheckbox(driver, Interfaces.TransactionPage.PRIVATE_CHECKBOX);
		sleep(1);
	}

	/**
	 * Check Private Checkbox
	 */
	public void checkPrivateCheckbox() {
		waitForControl(driver, Interfaces.TransactionPage.PRIVATE_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.PRIVATE_CHECKBOX);
		sleep(1);
		waitForControl(driver, Interfaces.TransactionPage.YES_CONFIRM_BUTTON, 5);
		if (isControlDisplayed(driver, Interfaces.TransactionPage.YES_CONFIRM_BUTTON)) {
			click(driver, Interfaces.TransactionPage.YES_CONFIRM_BUTTON);
			sleep();
		}
	}
	
	/**
	 * Uncheck Active Checkbox
	 */
	public void uncheckActiveCheckbox() {
		waitForControl(driver, Interfaces.TransactionPage.ACTIVE_CHECKBOX, timeWait);
		uncheckTheCheckbox(driver, Interfaces.TransactionPage.ACTIVE_CHECKBOX);
		sleep(1);
	}
	
	/**
	 * click Back Button
	 */
	public void clickBackButton() {
		waitForControl(driver, Interfaces.TransactionPage.BACK_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('BackSearch').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}
	
	/**
	 * Get Increase Rate
	 */
	public String getIncreaseRate() {
		waitForControl(driver, Interfaces.TransactionPage.INCREASE_RATE_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.TransactionPage.INCREASE_RATE_TEXTBOX, "value");
	}
	
	/**
	 * Get Leverage Factor
	 */
	public String getLeverageFactor() {
		waitForControl(driver, Interfaces.TransactionPage.LEVERAGE_FACTOR_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.TransactionPage.LEVERAGE_FACTOR_TEXTBOX, "value");
	}
	
	/**
	 * Get Increase Frequency
	 */
	public String getIncreaseFrequency() {
		waitForControl(driver, Interfaces.TransactionPage.INCREASE_FREQUENCY_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.TransactionPage.INCREASE_FREQUENCY_TEXTBOX, "value");
	}
	
	/**
	 * Get Adjustment Description
	 */
	public String getAdjustmentDescription() {
		waitForControl(driver, Interfaces.TransactionPage.ADJUSTMENT_DESCRIPTION_TEXTAREA, timeWait);
		return getAttributeValue(driver, Interfaces.TransactionPage.ADJUSTMENT_DESCRIPTION_TEXTAREA, "value");
	}
	
	/**
	 * check Error Message Displayed
	 * 
	 * @return
	 */
	public boolean isErrorMessageDisplayed(String messageContent) {
		waitForControl(driver,	Interfaces.ApplicantsPage.ERROR_MESSAGE_DISPLAYED,messageContent, timeWait);
		return isControlDisplayed(driver,Interfaces.ApplicantsPage.ERROR_MESSAGE_DISPLAYED,messageContent);
	}
	
	/**
	 * Click
	 * 
	 * @return
	 */
	public void clickOnAdjustment() {
		waitForControl(driver,	Interfaces.TransactionPage.ADJUSTMENT_DESCRIPTION_TEXTAREA, timeWait);
		 click(driver,Interfaces.TransactionPage.ADJUSTMENT_DESCRIPTION_TEXTAREA);
		
	}
	
	/**
	 * Input information for Adjustment Description
	 * 
	 * @return
	 */
	public void inputDataForAdjustmentDescription(String increaseRate,String leverageFactor,String increaseFrequency) {
		
		inputIncreaseFrequency(increaseFrequency);
		inputLeverageFactor(leverageFactor);
		inputIncreaseRate(increaseRate);
		clickOnAdjustment();
		clickSaveButton();
		
	}
	
	/**
	 * Get value of Allocation Rate
	 */
	public String getExpectedAllocatedRate(String appraisedValueCurrent, String AppraisedValue1, String AppraisedValue2, String AppraisedValue3, int decimalPlace) {
		float expectedAllocationRate = Float.parseFloat(appraisedValueCurrent)/(Float.parseFloat(AppraisedValue1)+ Float.parseFloat(AppraisedValue2)+ Float.parseFloat(AppraisedValue3))*100;
		sleep(10);
		return round(expectedAllocationRate,decimalPlace).toString();
	}
	
	/**
	 * Get Initial Rent
	 */
	public String getInitialRent() {
		waitForControl(driver, Interfaces.TransactionPage.INITIAL_RENT_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.TransactionPage.INITIAL_RENT_TEXTBOX, "value");
	}
	
	/**
	 * Get number of days in month
	 */
	public int getNumberOfDaysInMonth() {
		waitForControl(driver, Interfaces.TransactionPage.DATE_SELECT_BUTTON, timeWait);
		return countElement(driver, Interfaces.TransactionPage.DATE_SELECT_BUTTON);
	}
	
	/**
	 * Click
	 * 
	 * @return
	 */
	public void clickOnDateSelecterIcon() {
		waitForControl(driver,	Interfaces.TransactionPage.DATE_SELECTER_ICON, timeWait);
		click(driver,Interfaces.TransactionPage.DATE_SELECTER_ICON);
	}

	/**
	 * Get value of Pre-paid rent round Rate
	 */
	public String getExpectedPrePaidRent(String appraisedValueCurrent,String AppraisedValue1,String AppraisedValue2, String AppraisedValue3,int proratedDays,int numberOfDaysInMonth,String initialRent, int decimalPlace) {
		float expectedAllocationRate = Float.parseFloat(appraisedValueCurrent)/(Float.parseFloat(AppraisedValue1)+ Float.parseFloat(AppraisedValue2)+ Float.parseFloat(AppraisedValue3));
		float expectedPrePaidRent = expectedAllocationRate* Float.parseFloat(initialRent.replace(",","")) * proratedDays / numberOfDaysInMonth;
		sleep(5);
		return round(expectedPrePaidRent,decimalPlace).toString();
	}
	
	/**
	 * Get value of Pre-paid rent not round
	 */
	public float getExpectedPrePaidRentWithoutRound(String appraisedValueCurrent,String AppraisedValue1,String AppraisedValue2, String AppraisedValue3,int proratedDays,int numberOfDaysInMonth,String initialRent) {
		float expectedAllocationRate = Float.parseFloat(appraisedValueCurrent)/(Float.parseFloat(AppraisedValue1)+ Float.parseFloat(AppraisedValue2)+ Float.parseFloat(AppraisedValue3));
		float expectedPrePaidRent = expectedAllocationRate* Float.parseFloat(initialRent.replace(",","")) * proratedDays / numberOfDaysInMonth;
		return expectedPrePaidRent;
	}
	
	/**
	 * Check Loan changed date displays
	 */
	
	public Boolean isDateOfLastChangeDisplayedCorrectly(){
		waitForControl(driver, Interfaces.TransactionPage.DOCUMENT_TYPES_LINK, timeWait);
		int numberOfDocumentTypes = countElement(driver, Interfaces.TransactionPage.DOCUMENT_TYPES_LINK);
		for(int i =1; i<= numberOfDocumentTypes; i++){
			if (!getText(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_LAST_CHANGED_BY_STATUS, Integer.toString(i)).equals(""))
				if(!isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_FILE_LINK, Integer.toString(i)))
					return false;
		}
		return true;
	}
	
	/**
	 * Check Loan changed date displays
	 */
	
	public Boolean isLastChangeByDisplayedCorrectly(){
		waitForControl(driver, Interfaces.TransactionPage.DOCUMENT_TYPES_NAME, timeWait);
		int numberOfDocumentTypes = countElement(driver, Interfaces.TransactionPage.DOCUMENT_TYPES_NAME);
		for(int i =1; i<= numberOfDocumentTypes; i++){
			if (!getText(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_LAST_CHANGED_BY_STATUS_TRANSACTION, Integer.toString(i)).equals(""))
				if(getText(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_LOADED_FILE_STATUS, Integer.toString(i)).equals("No"))
					return false;
		}
		return true;
	}
	
	/**
	 * check Renewal Option selected
	 */
	public Boolean isRenewalOptionSelected() {
		waitForControl(driver, Interfaces.TransactionPage.RENEWAL_OPTION_CHECKBOX, timeWait);
		return isControlSelected(driver, Interfaces.TransactionPage.RENEWAL_OPTION_CHECKBOX);
	}
	
	/**
	 * check TripleNet selected
	 */
	public Boolean isTripleNetSelected() {
		waitForControl(driver, Interfaces.TransactionPage.TRIPLENET_CHECKBOX, timeWait);
		return isControlSelected(driver, Interfaces.TransactionPage.TRIPLENET_CHECKBOX);
	}
	
	/**
	 * Calculate Initial Payment
	 */
	public String calculateInitialPayment(String priceValue) {
		float price = Float.parseFloat(priceValue) ;
		float initialPayment = price/12;
		return round(initialPayment,2).toString();
	}
	
	/**
	 * Calculate Prorated Days
	 */
	public int calculateProratedDays(int daysInMonth) {
		waitForControl(driver, Interfaces.TransactionPage.CONTRACT_COMMENCEMENT_DATE_TEXTBOX, timeWait);
		String date = getAttributeValue(driver, Interfaces.TransactionPage.CONTRACT_COMMENCEMENT_DATE_TEXTBOX, "value");
		String[] dateSplit = date.split("/");
		int currentDay = Integer.parseInt(dateSplit[1]);
		return daysInMonth - currentDay +1;
	}
	
	/**
	 * Get 'Prorated Days' 
	 */
	public int getProratedDays() {
		waitForControl(driver, Interfaces.TransactionPage.PRORATED_DAYS_TEXTBOX, timeWait);
		String realProratedDays = getAttributeValue(driver, Interfaces.TransactionPage.PRORATED_DAYS_TEXTBOX, "value");
		return Integer.parseInt(realProratedDays);
	}
	
	/**
	 * Calculate 1st Payment Day
	 */
	public String calculate1stPaymentDay(int daysInMonth) {
		waitForControl(driver, Interfaces.TransactionPage.CONTRACT_COMMENCEMENT_DATE_TEXTBOX, timeWait);
		String date = getAttributeValue(driver, Interfaces.TransactionPage.CONTRACT_COMMENCEMENT_DATE_TEXTBOX, "value");
		String[] dateSplit = date.split("/");
		int paymentMonth = Integer.parseInt(dateSplit[0])+1;
		if(paymentMonth>12) paymentMonth-=12;
		int paymentYear = Integer.parseInt(dateSplit[2]);
		return Integer.toString(paymentMonth) + "/1/"+ Integer.toString(paymentYear);
	}
	
	/**
	 * Get '1st Payment Day' 
	 */
	public String get1stPaymentDay() {
		waitForControl(driver, Interfaces.TransactionPage.FIRST_PAYMENT_DATE_TEXTBOX, timeWait);
		String realProratedDays = getAttributeValue(driver, Interfaces.TransactionPage.FIRST_PAYMENT_DATE_TEXTBOX, "value");
		return realProratedDays;
	}
	
	/**
	 * check Unit Sales Only Required is selected
	 */
	public Boolean isUnitSalesOnlySelected() {
		waitForControl(driver, Interfaces.TransactionPage.UNIT_SALE_ONLY_REQUIRED_CHECKBOX, timeWait);
		return isControlSelected(driver, Interfaces.TransactionPage.UNIT_SALE_ONLY_REQUIRED_CHECKBOX);
	}
	
	/**
	 * check Unit Financial Statements Required is selected
	 */
	public Boolean isUnitFinancialStatementsRequiredSelected() {
		waitForControl(driver, Interfaces.TransactionPage.UNIT_FINANCIAL_STATEMENT_REQUIRED_CHECKBOX, timeWait);
		return isControlSelected(driver, Interfaces.TransactionPage.UNIT_FINANCIAL_STATEMENT_REQUIRED_CHECKBOX);
	}
	
	/**
	 * Calculate Next Increase Date
	 */
	public String calculateNextIncreaseDate(String date) {
		int increaseFreq = Integer.parseInt(getIncreaseFrequency()); 
		String[] dateSplit = date.split("/");
		int paymentMonth = Integer.parseInt(dateSplit[0])+1;
		if(paymentMonth>12) paymentMonth-=12;
		int paymentYear = Integer.parseInt(dateSplit[2])+increaseFreq/12;
		String abc = String.format("%02d", paymentMonth);
		return String.format("%02d", paymentMonth) + "/01/"+ Integer.toString(paymentYear);
	}
	
	/**
	 * Calculate Lease Expiration Date
	 */
	public String calculateLeaseExpirationDate(int numberOfDaysInMonth) {
		int term = getTerm();
		String date = getAttributeValue(driver, Interfaces.TransactionPage.CONTRACT_COMMENCEMENT_DATE_TEXTBOX, "value");
		String[] dateSplit = date.split("/");
		int paymentMonth = Integer.parseInt(dateSplit[0]);
		int paymentYear = Integer.parseInt(dateSplit[2])+term;
		return Integer.toString(paymentMonth) + "/"+numberOfDaysInMonth+"/"+ Integer.toString(paymentYear);
	}
	
	/**
	 * Get 'Term' text
	 */
	public int getTerm() {
		waitForControl(driver, Interfaces.TransactionPage.TERM_TEXTBOX, timeWait);
		String realTerm = getAttributeValue(driver, Interfaces.TransactionPage.TERM_TEXTBOX, "value");
		return Integer.parseInt(realTerm);
	}
	
	/**
	 * Get 'Contract Commencement Date ' text
	 */
	public String getContractCommencementDate () {
		waitForControl(driver, Interfaces.TransactionPage.CONTRACT_COMMENCEMENT_DATE_TEXTBOX, timeWait);
		String realTerm = getAttributeValue(driver, Interfaces.TransactionPage.CONTRACT_COMMENCEMENT_DATE_TEXTBOX, "value");
		return realTerm;
	}
	
	/**
	 * Open Users tab
	 */
	public UsersPage openUsersPage() {
		waitForControl(driver, Interfaces.TransactionPage.USERS_TAB_TITLE, timeWait);
		click(driver, Interfaces.TransactionPage.USERS_TAB_TITLE);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
		sleep();
		return PageFactory.getUsersPage(driver, ipClient);
	}
	
	/**
	 * click Bid request Button
	 */
	public void clickBidRequestButton() {
		waitForControl(driver, Interfaces.TransactionPage.BID_REQUEST_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('BidRequest').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}
	
	/**
	 * click Provider selector Button
	 */
	public void clickOnProviderSelector(String providerType) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_SELECT_PROVIDER_BUTTON, providerType, timeWait);
		click(driver, Interfaces.TransactionPage.DYNAMIC_SELECT_PROVIDER_BUTTON, providerType);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}
	
	/**
	 * check Provider name checkbox
	 */
	public void clickOnProviderCheckbox(String providerName) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_PROVIDER_CHECKBOX, providerName, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.DYNAMIC_PROVIDER_CHECKBOX, providerName);
	}
	
	/**
	 * click on Provider selector Ok button
	 */
	public void clickOnProviderSelectorOkButton() {
		waitForControl(driver, Interfaces.TransactionPage.SELECT_PROVIDER_OK_BUTTON, timeWait);
		click(driver, Interfaces.TransactionPage.SELECT_PROVIDER_OK_BUTTON);
	}
	
	/**
	 * open Third Pary Bids Tab
	 */
	public void openThirdParyBidsTab() {
		waitForControl(driver, Interfaces.TransactionPage.THIRD_PARTY_BIDS_TITLE, timeWait);
		executeJavaScript(driver, "document.getElementById('BidsLoanApplicationSearchMenu').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}
	
	/**
	 * open bid type link
	 */
	public void openBidTypeLink(String providerType) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_BID_TYPE_LINK, providerType, timeWait);
		click(driver, Interfaces.TransactionPage.DYNAMIC_BID_TYPE_LINK, providerType);
	}
	
	/**
	 * check Providers display correctly
	 */
	public Boolean isProviderDisplayCorrectly(String providerName, String status) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_PROVIDER_NAME_LABEL, providerName, status, timeWait);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_PROVIDER_NAME_LABEL, providerName, status);
	}
	
	/**
	 * Go back to third party bids
	 */
	public void goBackToThirdPartyBids() {
		waitForControl(driver, Interfaces.TransactionPage.BACK_TO_THIRD_PARTY_BIDS_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('BackThirdPart').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}
	
	/**
	 * Switch driver to iFrame
	 * @return driver
	 */
	public WebDriver switchToThirdPartyBidInstructionFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.TransactionPage.THIRD_PARTY_BIDS_IFRAME, timeWait);
		sleep(5);
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.TransactionPage.THIRD_PARTY_BIDS_IFRAME)));
		return driver;
	}
	
	/**
	 * click on Choose winner button
	 */
	public void clickOnChooseWinnerButton() {
		waitForControl(driver, Interfaces.TransactionPage.CHOOSE_WINNER_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('ChooseWinner').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}
	
	/**
	 * click on Save instruction button
	 */
	public void clickOnSaveInstructionButton() {
		waitForControl(driver, Interfaces.TransactionPage.SAVE_WINNWER_BUTTON, timeWait);
		click(driver, Interfaces.TransactionPage.SAVE_WINNWER_BUTTON);
	}
	
	/**
	 * is Save instruction button displayed
	 */
	public Boolean isChooseWinnerButtonDisplay() {
		waitForControl(driver, Interfaces.TransactionPage.CHOOSE_WINNER_BUTTON, 10);
		return isControlDisplayed(driver, Interfaces.TransactionPage.CHOOSE_WINNER_BUTTON);
	}
	
	/**
	 * open Provider Detail Page
	 */
	public void openProviderDetailPage(String providerName) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_WINNER_PROVIDER_LINK, providerName, timeWait);
		click(driver, Interfaces.TransactionPage.DYNAMIC_WINNER_PROVIDER_LINK, providerName);
	}
	
	/**
	 * check Document name displays correctly
	 */
	public Boolean isDocumentNameDisplayedCorrectly(String documentName) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_NAME_LINK, documentName, timeWait);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_NAME_LINK, documentName);
	}
	
	/**
	 * Calculate 1st Payment Day
	 */
	public String calculateEarliestBurnoffDate () {
		waitForControl(driver, Interfaces.TransactionPage.CONTRACT_COMMENCEMENT_DATE_TEXTBOX, timeWait);
		String date = getAttributeValue(driver, Interfaces.TransactionPage.CONTRACT_COMMENCEMENT_DATE_TEXTBOX, "value");
		String[] dateSplit = date.split("/");
		int number = Integer.parseInt(getAttributeValue(driver, Interfaces.TransactionPage.GUARANTY_BURNOFF_ANNIVERSARY, "value"));
		int burnoffYear = Integer.parseInt(dateSplit[2]) + number;
		String burnoffDay =dateSplit[1], burnoffMonth = dateSplit[0];
		if(Integer.parseInt(dateSplit[1]) <10) burnoffDay = "0"+dateSplit[1];
		if(Integer.parseInt(dateSplit[0]) <10) burnoffMonth = "0"+dateSplit[0];
		return  burnoffMonth+ "/"+ burnoffDay +"/"+ Integer.toString(burnoffYear);
	}
	
	/**
	 * get Earliest Burnoff Date
	 */
	public String getEarliestBurnoffDate(){
		waitForControl(driver, Interfaces.TransactionPage.EARLIEST_BURNOFF_DATE, timeWait);
		return getText(driver, Interfaces.TransactionPage.EARLIEST_BURNOFF_DATE);
	}
	
	/**
	 * check Bid Request button display for all statuses
	 * 
	 * @param N/A
	 */
	public boolean isBidRequestButtonDisplayedForAllStatuses() {
		waitForControl(driver, Interfaces.TransactionPage.WORKFLOW_STATUS_DROPDOWN_LIST, timeWait);
		int numberOfOptions = countElement(driver, Interfaces.TransactionPage.WORKFLOW_STATUS_DROPDOWN_LIST_OPTION);
		for(int i=0; i< numberOfOptions; i++){
			selectItemComboboxByIndex(driver, Interfaces.TransactionPage.WORKFLOW_STATUS_DROPDOWN_LIST, i);
			clickSaveButton();
			if(!isControlDisplayed(driver, Interfaces.TransactionPage.BID_REQUEST_BUTTON)){
				System.out.println(getItemSelectedCombobox(driver, Interfaces.TransactionPage.WORKFLOW_STATUS_DROPDOWN_LIST));
				return false;
			}
		}
		return true;
	}
	
	/**
	 * check Document type displays correctly
	 */
	public Boolean isDocumentTypeDisplayedCorrectly(String documentType) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_TYPE_NAME, documentType, 10);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_DOCUMENT_TYPE_NAME, documentType);
	}
	
	/**
	 * Check Transaction display on search Transaction page
	 */
	public boolean isTransactionDisplayOnSearchTransactionPage(String companyName, String lessee) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_SEARCH_TRANSACTION_LESSEE_LINK, companyName, lessee, timeWait);
		return isControlDisplayed(driver, Interfaces.TransactionPage.DYNAMIC_SEARCH_TRANSACTION_LESSEE_LINK, companyName, lessee);
	}
	
	/**
	 * Select Lessee combobox
	 */
	public String getLesseeName() {
		waitForControl(driver, Interfaces.TransactionPage.LESSEE_DROPDOWN_LIST, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.TransactionPage.LESSEE_DROPDOWN_LIST);
	}
	
	/**
	 * Select Lessee combobox
	 */
	public void chooseLessee(String lesseeName) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_EXTERNAL_ENTITY_CHECKBOX, lesseeName, timeWait);
		checkTheCheckbox(driver, Interfaces.TransactionPage.DYNAMIC_EXTERNAL_ENTITY_CHECKBOX, lesseeName);
	}
	
	/**
	 * Input External Entities info to create new login
	 */
	public void inputExternalEntitiesInfoToCreateNewLogin(String firstName, String lastName, String email) {
		waitForControl(driver, Interfaces.TransactionPage.EXTERNAL_ENTITY_LOGIN_FIRST_NAME_TEXTBOX, timeWait);
		sleep(1);
		type(driver, Interfaces.TransactionPage.EXTERNAL_ENTITY_LOGIN_FIRST_NAME_TEXTBOX, firstName);
		sleep(1);
		type(driver, Interfaces.TransactionPage.EXTERNAL_ENTITY_LOGIN_LAST_NAME_TEXTBOX, lastName);
		sleep(1);
		type(driver, Interfaces.TransactionPage.EXTERNAL_ENTITY_LOGIN_EMAIL_TEXTBOX, email);
		sleep(1);
	}
	
	/**
	 * click Send this information to email
	 */
	public void clickSendInformationToEmail() {
		waitForControl(driver, Interfaces.UsersPage.SEND_INFORMATION_TO_EMAIL, timeWait);
		click(driver, Interfaces.UsersPage.SEND_INFORMATION_TO_EMAIL);
		sleep();
	}
	
	/**
	 * Get user information store
	 */
	public String getRecordInformationStore() {
		waitForControl(driver, Interfaces.TransactionPage.FIRST_TRANSACTION, timeWait);
		return getText(driver, Interfaces.TransactionPage.FIRST_TRANSACTION).trim().toLowerCase();
	}
	
	/**
	 * Get user information store
	 */
	public String getPropertiesNumber() {
		waitForControl(driver, Interfaces.TransactionPage.FIRST_PROPERTY_NUMBER, timeWait);
		return getText(driver, Interfaces.TransactionPage.FIRST_PROPERTY_NUMBER).trim().toLowerCase();
	}
	
	/**
	 * input Number of Renewal Options
	 */
	public void inputNumberOfRenewalOptions(String value) {
		waitForControl(driver, Interfaces.TransactionPage.NUMBER_OF_RENEWAL_OPTIONS, timeWait);
		type(driver, Interfaces.TransactionPage.NUMBER_OF_RENEWAL_OPTIONS, value);
	}
	
	/**
	 * input Renewal Option Year
	 */
	public void inputRenewalOptionYears(String value) {
		waitForControl(driver, Interfaces.TransactionPage.RENEWAL_OPTION_YEARS, timeWait);
		type(driver, Interfaces.TransactionPage.RENEWAL_OPTION_YEARS, value);
	}
	
	/**
	 * check Number of Renewal Option displays correctly
	 */
	public Boolean isNumberOfRenewalOptionsDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.TransactionPage.NUMBER_OF_RENEWAL_OPTIONS, timeWait);
		return getAttributeValue(driver, Interfaces.TransactionPage.NUMBER_OF_RENEWAL_OPTIONS, "value").contains(value);
	}
	
	/**
	 * check Renewal Option Year displays correctly
	 */
	public Boolean isRenewalOptionYearsDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.TransactionPage.RENEWAL_OPTION_YEARS, timeWait);
		return getAttributeValue(driver, Interfaces.TransactionPage.RENEWAL_OPTION_YEARS, "value").contains(value);
	}
	
	/**
	 * check Renewal Option Start date displays correctly
	 */
	public Boolean isRenewalOptionStartDateDisplayedCorrectly(String index, String value) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_RENEWAL_OPTION_START_DATE_READ_ONLY_TEXTFIELD, index, timeWait);
		String startDate = getAttributeValue(driver, Interfaces.TransactionPage.DYNAMIC_RENEWAL_OPTION_START_DATE_READ_ONLY_TEXTFIELD, index, "value");
		return startDate.contains(value);
	}
	
	/**
	 * check Renewal Option Option Close date displays correctly
	 */
	public Boolean isRenewalOptionOptionCloseDateDisplayedCorrectly(String index, String value) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_RENEWAL_OPTION_OPTION_CLOSE_DATE_TEXTFIELD, index, timeWait);
		return getAttributeValue(driver, Interfaces.TransactionPage.DYNAMIC_RENEWAL_OPTION_OPTION_CLOSE_DATE_TEXTFIELD, index, "value").contains(value);
	}
	
	/**
	 * check Renewal Option Months displays correctly
	 */
	public Boolean isRenewalOptionMonthsDisplayedCorrectly(String index, String value) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_RENEWAL_OPTION_MONTHS_TEXTFIELD, index, timeWait);
		return getAttributeValue(driver, Interfaces.TransactionPage.DYNAMIC_RENEWAL_OPTION_MONTHS_TEXTFIELD, index, "value").contains(value);
	}
	
	/**
	 * check Renewal Option Status displays correctly
	 */
	public Boolean isRenewalOptionStatusDisplayedCorrectly(String index, String value) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_RENEWAL_OPTION_STATUS_TEXTFIELD, index, timeWait);
		return getText(driver, Interfaces.TransactionPage.DYNAMIC_RENEWAL_OPTION_STATUS_TEXTFIELD, index).contains(value);
	}
	
	/**
	 * check Increase at Renewal Only selected
	 */
	public Boolean isIncreaseAtRenewalOnlySelected(String index) {
		waitForControl(driver, Interfaces.TransactionPage.DYNAMIC_RENEWAL_OPTION_OPTION_INCREASE_AT_RENEWAL_ONLY_CHECKBOX, index, timeWait);
		return isControlSelected(driver, Interfaces.TransactionPage.DYNAMIC_RENEWAL_OPTION_OPTION_INCREASE_AT_RENEWAL_ONLY_CHECKBOX, index);
	}
	
	/**
	 * check Number of Renewal Option record displays correctly
	 */
	public Boolean isNumberOfRenewalOptionsRecordDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.TransactionPage.RENEWAL_OPTION_RECORDS, timeWait);
		return Integer.toString(countElement(driver, Interfaces.TransactionPage.RENEWAL_OPTION_RECORDS)).equals(value);
	}
	
	
	
	public Boolean isRenewalOptionsChildTableGenerateCorrectly(String numberOfRenewalOptions, String renewalOptionYears, String leaseExpirationDate){
		waitForControl(driver, Interfaces.TransactionPage.RENEWAL_OPTION_RECORDS, timeWait);
		int numberOfRecords = Integer.parseInt(numberOfRenewalOptions);
		int months = Integer.parseInt(renewalOptionYears) * 12;
		String expectedStartDate, expectedCloseDate, expectedMonths, expectedStatus;
		expectedStartDate ="";
		expectedCloseDate ="";
		expectedMonths = Integer.toString(months);
		for(int i = 1; i<=numberOfRecords; i++){
			System.out.println("Checking for record number "+ i+"...");
			if(i==1){
				String j = Integer.toString(i);
				expectedStartDate = addDateTimeByDate(leaseExpirationDate, 1);
				expectedCloseDate = addDateTimeByDate(expectedStartDate, -121);
				expectedStatus = "ACT"; 
				if(!isRenewalOptionStartDateDisplayedCorrectly(j,expectedStartDate)) return false;
				if(!isRenewalOptionOptionCloseDateDisplayedCorrectly(j,expectedCloseDate)) return false;
				if(!isRenewalOptionStartDateDisplayedCorrectly(j,expectedStartDate)) return false;
				if(!isRenewalOptionMonthsDisplayedCorrectly(j,expectedMonths)) return false;
				if(!isRenewalOptionStatusDisplayedCorrectly(j,expectedStatus)) return false;
				if(isIncreaseAtRenewalOnlySelected(j)) return false;
			}
			else{
				String j = Integer.toString(i);
				expectedStartDate = addDateTimeByMonth(expectedStartDate, months);
				expectedCloseDate = addDateTimeByDate(expectedStartDate, -121);
				expectedStatus = "FTR"; 
				if(!isRenewalOptionStartDateDisplayedCorrectly(j,expectedStartDate)) return false;
				if(!isRenewalOptionOptionCloseDateDisplayedCorrectly(j,expectedCloseDate)) return false;
				if(!isRenewalOptionStartDateDisplayedCorrectly(j,expectedStartDate)) return false;
				if(!isRenewalOptionMonthsDisplayedCorrectly(j,expectedMonths)) return false;
				if(!isRenewalOptionStatusDisplayedCorrectly(j,expectedStatus)) return false;
				if(isIncreaseAtRenewalOnlySelected(j)) return false;
				}
			
		}
		return true;
	}
	
	/**
	 * Click on Generate button
	 */
	public void clickOnGenerateButton(){
		waitForControl(driver, Interfaces.TransactionPage.GENERATE_BUTTON, timeWait);
		click(driver, Interfaces.TransactionPage.GENERATE_BUTTON);
		sleep(4);
	}
	
	/**
	 * Chek Guarantor imported correctly
	 * @param fileName
	 * @return
	 */
	public boolean isGuarantorMappingFromXmlFileCorrectly(String fileName){
		String tagName = "Variable";
		String attributeName ="Name";
		Boolean isRepeated = Boolean.valueOf(getDataFromXmlFile(fileName, tagName, attributeName, "Multiple Guarantors"));
		List<String> listGuarantorName = new ArrayList<String>();
		listGuarantorName = getGuarantorListFromXmlFile(fileName, tagName, attributeName, isRepeated, "Guarantor Name");
		int numberOfGuarantor = Integer.parseInt(getDataFromXmlFile(fileName, tagName, attributeName, "Number of Guarantors"));
		int actualNumberOfGuarantors = listGuarantorName.size();
		if(isRepeated){
			if(numberOfGuarantor>actualNumberOfGuarantors) numberOfGuarantor = actualNumberOfGuarantors;
			for(int i=0;i< numberOfGuarantor; i++){
				System.out.println("Checking for Guarantor: "+listGuarantorName.get(i));
				if(!isNewEntitiesDisplayOnExternalEntitiesTable(listGuarantorName.get(i), "Guarantor")) return false;
			}
		}
		else{
			System.out.println("Checking for Guarantor: "+listGuarantorName.get(0));
			if(!isNewEntitiesDisplayOnExternalEntitiesTable(listGuarantorName.get(0), "Guarantor")) return false;
		}
		return true;
	}
	
	/**
	 * Chek Increse type imported correctly
	 * @param fileName
	 * @return
	 */
	public boolean isIncreaseTypeMappingFromXmlFileCorrectly(String fileName){
		String tagName = "Variable";
		String attributeName ="Name";
		Boolean cpiMultiplier = Boolean.valueOf(getDataFromXmlFile(fileName, tagName, attributeName, "CPI Multiplier"));
		if(cpiMultiplier){
			if(isIncreaseTypeSavedSuccessfully("CPIL")) return true;
		}
		else if(isIncreaseTypeSavedSuccessfully("")) return true;
		return false;
	}
	
	/**
	 * Chek Increse type imported correctly
	 * @param fileName
	 * @return
	 */
	public boolean isOriginalCommencementDateMappingFromXmlFileCorrectly(String fileName, String previousCommencement){
		String tagName = "Variable";
		String attributeName ="Name";
		String isLeasePriorExist = getDataFromXmlFile(fileName, tagName, attributeName, "Opportunity:Existing_Lease_Prior_to_Purchase:WriteBack");
		if(isLeasePriorExist.equals("False") || isLeasePriorExist.equals("No")){
//			System.out.println(convertDateFormat("yyyy/mm/dd", "m/d/yyyy", getDataFromXmlFile(fileName, tagName, attributeName, "Lease Date")));
			if(isOriginalCommencementDateSavedSuccessfully(convertDateFormat("yyyy/mm/dd", "m/d/yyyy", getDataFromXmlFile(fileName, tagName, attributeName, "Lease Date")))) return true;
		}
		else if(isOriginalCommencementDateSavedSuccessfully(previousCommencement)) return true;
		return false;
	}
	
	/**
	 * upload Transaction XML
	 */
	public void uploadTransactionXMLTest(String xmlFileName) {
		waitForControl(driver, Interfaces.TransactionPage.TRANSACTION_CHOOSE_FILE_BUTTON, timeWait);
		doubleClick(driver, Interfaces.TransactionPage.TRANSACTION_CHOOSE_FILE_BUTTON);
		sleep(2);
		Common.getCommon().uploadFileSikuli(xmlFileName);
		sleep(2);
		click(driver, Interfaces.TransactionPage.LOAD_BUTTON);
		sleep(5);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
	}
	
	private WebDriver driver;
	private String ipClient;
}
