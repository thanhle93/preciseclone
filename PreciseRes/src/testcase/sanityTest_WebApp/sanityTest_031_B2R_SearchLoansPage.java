package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Constant;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;

public class sanityTest_031_B2R_SearchLoansPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		applicantName = "Consolidation Demo Borrower New";
		loanStatus = Constant.LOAN_STATUS_B2R;
		newLoanStatus = "Rent Roll Received";
		propertiesFileName = "B2R Required.xlsx";
		finalLLC = "100";
		active = "Yes";
		inActive = "No";
	}

	@Test(groups = { "regression" }, description = "SearchLoansPage 01 - Search by Legal Name")
	public void SearchLoansPage_01_SearchByLegalName() {
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Precondition 02. Open Loans page");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 03. Create New loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);

		log.info("Precondition 04. Input data to 'Final Borrower LLC, Loan Application Status' fields");
		loansPage.inputFinalBorrowerAndFundedStatus(finalLLC, newLoanStatus);

		log.info("Precondition 05. Update Midland ID");
		loansPage.inputMidlandID(accountNumber);

		log.info("Precondition 06. Click Save button");
		loansPage.clickSaveButton();
		loansPage.isRecordSavedMessageDisplays();

		log.info("Precondition 07. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Precondition 08. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("Precondition 09. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_01 - Step 01. Search by Legal Name");
		loansPage.searchLoanByName(loanName);

		numberProperties = loansPage.getTextLoanSearch("DataCell FieldTypeInteger datacell_TotProp");
		totalEstimatedValue = loansPage.getTextLoanSearch("DataCell FieldTypeCurrency datacell_TotVal");
		loanStatusChangeDate = loansPage.getTextLoanSearch("DataCell FieldTypeDate datacell_StatusChangeDate");
		dateCreated = loansPage.getTextLoanSearch("DataCell FieldTypeDateTime datacell_CreateDate");

		log.info("VP: Legal Name display in Loans search table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableB2R(loanName, loanName, finalLLC, accountNumber, applicantName, numberProperties, totalEstimatedValue,
				loanStatusChangeDate, newLoanStatus, dateCreated, active));
	}

	@Test(groups = { "regression" }, description = "SearchLoansPage 02 - Search by Loan ID")
	public void SearchLoansPage_02_SearchByLoanID() {
		log.info("SearchLoansPage_02 - Step 01. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_02 - Step 02. Search by Loan ID");
		loansPage.searchLoanByLoanID(loanName);

		log.info("VP: Loan ID display in Loans search table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableB2R(loanName, loanName, finalLLC, accountNumber, applicantName, numberProperties, totalEstimatedValue,
				loanStatusChangeDate, newLoanStatus, dateCreated, active));
	}

	@Test(groups = { "regression" }, description = "SearchLoansPage 03 - Search by Final Borrower LLC")
	public void SearchLoansPage_03_SearchByFinalBorrowerLLC() {

		log.info("SearchLoansPage_03 - Step 01. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_03 - Step 02. Search by Final Borrower LLC");
		loansPage.searchLoanByFinalBorrowerLLC(loanName, finalLLC);

		log.info("VP: Final Borrower LLC display in Loans search table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableB2R(loanName, loanName, finalLLC, accountNumber, applicantName, numberProperties, totalEstimatedValue,
				loanStatusChangeDate, newLoanStatus, dateCreated, active));
	}

	@Test(groups = { "regression" }, description = "SearchLoansPage 04 - Search by Midland ID")
	public void SearchLoansPage_04_SearchByMidlandID() {

		log.info("SearchLoansPage_04 - Step 01. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_04 - Step 02. Search by Midland ID");
		loansPage.searchLoanByMidlandID(loanName, accountNumber);

		log.info("VP: Midland ID display in Loans search table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableB2R(loanName, loanName, finalLLC, accountNumber, applicantName, numberProperties, totalEstimatedValue,
				loanStatusChangeDate, newLoanStatus, dateCreated, active));
	}

	@Test(groups = { "regression" }, description = "SearchLoansPage 05 - Search by Applicant")
	public void SearchLoansPage_05_SearchByApplicant() {

		log.info("SearchLoansPage_05 - Step 01. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_05 - Step 02. Search by Applicant");
		loansPage.searchLoanByApplicant(loanName, applicantName);

		log.info("VP: Applicant display in Loans search table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableB2R(loanName, loanName, finalLLC, accountNumber, applicantName, numberProperties, totalEstimatedValue,
				loanStatusChangeDate, newLoanStatus, dateCreated, active));
	}

	@Test(groups = { "regression" }, description = "SearchLoansPage 06 - Search by Loan Status Change Date")
	public void SearchLoansPage_06_SearchByLoanStatusChangeDate() {

		log.info("SearchLoansPage_06 - Step 01. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_06 - Step 02. Search by Loan Status Change Date");
		loansPage.searchLoanByLoanStatusChangeDate(loanName, loanStatusChangeDate);

		log.info("VP: Loan Status Change Date display in Loans search table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableB2R(loanName, loanName, finalLLC, accountNumber, applicantName, numberProperties, totalEstimatedValue,
				loanStatusChangeDate, newLoanStatus, dateCreated, active));
	}

	@Test(groups = { "regression" }, description = "SearchLoansPage 07 - Search by Loan Application Status")
	public void SearchLoansPage_07_SearchByLoanApplicationStatus() {

		log.info("SearchLoansPage_07 - Step 01. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_07 - Step 02. Search by Loan Application Status");
		loansPage.searchLoanByLoanApplicationStatusB2R(loanName, newLoanStatus);

		log.info("VP: Loan Application Status display in Loans search table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableB2R(loanName, loanName, finalLLC, accountNumber, applicantName, numberProperties, totalEstimatedValue,
				loanStatusChangeDate, newLoanStatus, dateCreated, active));
	}

	@Test(groups = { "regression" }, description = "SearchLoansPage 08 - Search by Date Created")
	public void SearchLoansPage_08_SearchByDateCreated() {

		log.info("SearchLoansPage_08 - Step 01. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_08 - Step 02. Search by Date Created");
		loansPage.searchLoanByDateCreated(loanName, dateCreated);

		log.info("VP: Date Created display in Loans search table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableB2R(loanName, loanName, finalLLC, accountNumber, applicantName, numberProperties, totalEstimatedValue,
				loanStatusChangeDate, newLoanStatus, dateCreated, active));
	}

	@Test(groups = { "regression" }, description = "SearchLoansPage 09 - Search by Loan Active status")
	public void SearchLoansPage_09_SearchByLoanActiveStatus() {

		log.info("SearchLoansPage_09 - Step 01. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_09 - Step 02. Search by Legal Name");
		loansPage.searchLoanByName(loanName);

		log.info("SearchLoansPage_09 - Step 03: Select Loan checkbox in List Loan Applicants table");
		loansPage.isSelectedPropertyLoanCheckbox(loanName);

		log.info("SearchLoansPage_09 - Step 04: Click 'Make Loan Inactive' button");
		loansPage.clickMakeInactiveButtonAtLoanTab();

		log.info("VP: The Loan not displayed in table");
		verifyFalse(loansPage.isAllValueDisplayOnSearchLoansTableB2R(loanName, loanName, finalLLC, accountNumber, applicantName, numberProperties, totalEstimatedValue, loanStatusChangeDate, newLoanStatus, dateCreated, active));

		log.info("SearchLoansPage_09 - Step 05. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);
		
		log.info("SearchLoansPage_09 - Step 06. Search Loan by Inactive status");
		loansPage.searchByActiveOrInactive(loanName, "No");

		log.info("VP: The Loan inactive displayed in table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableB2R(loanName, loanName, finalLLC, accountNumber, applicantName, "0", "", loanStatusChangeDate, newLoanStatus, dateCreated, inActive));
		
		log.info("SearchLoansPage_09 - Step 07. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_09 - Step 08. Search Loan by Either status");
		loansPage.searchByActiveOrInactive(loanName, "Either");

		log.info("VP: The Loan inactive displayed in table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableB2R(loanName, loanName, finalLLC, accountNumber, applicantName, "0", "", loanStatusChangeDate, newLoanStatus, dateCreated, inActive));

		log.info("SearchLoansPage_09 - Step 09: Select Loan checkbox in List Loan Applicants table");
		loansPage.isSelectedPropertyLoanCheckbox(loanName);

		log.info("SearchLoansPage_09 - Step 10: Click 'Make Loan Active' button");
		loansPage.clickMakeActiveButtonAtLoanTab();

		log.info("SearchLoansPage_09 - Step 11. Search Loan by Active status");
		loansPage.searchByActiveOrInactive(loanName, "Yes");

		log.info("VP: The loan displayed in table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableB2R(loanName, loanName, finalLLC, accountNumber, applicantName, numberProperties, totalEstimatedValue,
				loanStatusChangeDate, newLoanStatus, dateCreated, active));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender, loanName, loanStatus, newLoanStatus, propertiesFileName, finalLLC;
	private String accountNumber, applicantName, numberProperties, totalEstimatedValue, loanStatusChangeDate, dateCreated, active, inActive;
}