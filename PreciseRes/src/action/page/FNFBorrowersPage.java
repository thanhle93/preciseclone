package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import PreciseRes.Interfaces;
import common.Common;

public class FNFBorrowersPage extends AbstractPage {
	public FNFBorrowersPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}

	/**
	 * click On 'New Applicant' button
	 */
	public void clickOnNewApplicantButton() {
		waitForControl(driver, Interfaces.ApplicantsPage.NEW_APPLICANTS_BUTTON,	timeWait);
		click(driver, Interfaces.ApplicantsPage.NEW_APPLICANTS_BUTTON);
		sleep();
	}
	
	/**
	 * click On 'New Opportunities' button
	 */
	public void clickOnNewOpportunitiesButton() {
		waitForControl(driver, Interfaces.ApplicantsPage.NEW_OPPORTUNITIES_BUTTON,	timeWait);
		click(driver, Interfaces.ApplicantsPage.NEW_OPPORTUNITIES_BUTTON);
		sleep();
	}
	
	/**
	 * input New Applicant Info [CAF-FNF]
	 */
	public void inputNewApplicantInfo(String companyName, String primaryContactFirstName, String primaryContactLastName, String email) {
		waitForControl(driver, Interfaces.ApplicantsPage.COMPANY_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.ApplicantsPage.COMPANY_NAME_TEXTBOX,	companyName);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.PRIMARY_CONTACT_FIRST_NAME_TEXTBOX, primaryContactFirstName);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.PRIMARY_CONTACT_LAST_NAME_TEXTBOX, primaryContactLastName);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.EMAIL_TEXTBOX, email);
		sleep(1);
	}
	
	/**
	 * select Product Type
	 */
	public void selectProductType(String productType) {
		waitForControl(driver, Interfaces.ApplicantsPage.PRODUCT_TYPE_COMBOBOX,	timeWait);
		selectItemCombobox(driver,	Interfaces.ApplicantsPage.PRODUCT_TYPE_COMBOBOX, productType);
	}

	/**
	 * click On 'Invite New Applicant' button
	 */
	public void clickOnInviteNewApplicantButton() {
		waitForControl(driver,
				Interfaces.ApplicantsPage.INVITE_NEW_APPLICANT_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('SaveNewApplicant').click();");
		sleep(1);
	}

	/**
	 * check Detail Message Email
	 */
	public boolean isDetailMessageEmailDisplay(String detailMessageEmail) {
		waitForControl(driver,
				Interfaces.ApplicantsPage.EMAIL_MESSAGE_WITH_TEXT,
				detailMessageEmail, timeWait);
		return isControlDisplayed(driver,
				Interfaces.ApplicantsPage.EMAIL_MESSAGE_WITH_TEXT,
				detailMessageEmail);
	}

	/**
	 * get value 'Company Name' field
	 */
	public String getValueCompanyName() {
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_APPLICANT_COMPANY_NAME_TEXTBOX, timeWait);
		String companyName = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_APPLICANT_COMPANY_NAME_TEXTBOX, "value");
		return companyName;
	}
	
	/**
	 * get value 'Company Name' field (Read-only)
	 */
	public String getValueCompanyNameReadOnly() {
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_APPLICANT_COMPANY_NAME_READ_ONLY_TEXTBOX, timeWait);
		String companyName = getText(driver, Interfaces.ApplicantsPage.FNF_APPLICANT_COMPANY_NAME_READ_ONLY_TEXTBOX);
		return companyName;
	}
	
	/**
	 * change Company/ Borrower Name, Address of Entity/Borrower, Website
	 */
	public void inputValidCompanyNameAddressSiteCity(String companyName,	String street, String city, String companysite,
			String borrowerFirstName,	String borrowerLastName, String borrowerStreet, String borrowerCity) {
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_APPLICANT_COMPANY_NAME_TEXTBOX, companyName, timeWait);
		type(driver,	Interfaces.ApplicantsPage.FNF_APPLICANT_COMPANY_NAME_TEXTBOX, companyName);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.FNF_ADDRESS_TEXTBOX, street);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.FNF_CITY_TEXTBOX, city);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.FNF_COMPANY_SITE_TEXTBOX, companysite);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.FNF_BORROWER_FIRST_NAME_TEXTBOX, borrowerFirstName);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.FNF_BORROWER_LAST_NAME_TEXTBOX, borrowerLastName);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.FNF_BORROWER_ADRESS_TEXTBOX, borrowerStreet);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.FNF_BORROWER_CITY_TEXTBOX, borrowerCity);
		sleep(1);
	}
	
	/**
	 * change Company/Borrower State, ProductType, Corporate Structure, Originator
	 * State of Incorporation/Formation, Party in a lawsuit, Pending litigation Involving, Citizen
	 */
	public void inputValidAllComboboxFields(String state, String corporateStructure, String stateOfIncorporation, String borrowerState) {
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_STATE_COMBOBOX, state, timeWait);
		selectItemCombobox(driver,	Interfaces.ApplicantsPage.FNF_STATE_COMBOBOX, state);
		sleep(1);
		selectCorporateStructure(corporateStructure);
		sleep(1);
		selectItemCombobox(driver, Interfaces.ApplicantsPage.FNF_STATE_OF_INCORPORATION_FORMATION_COMBOBOX, stateOfIncorporation);
		sleep(1);
		selectItemCombobox(driver, Interfaces.ApplicantsPage.FNF_BORROWER_STATE_COMBOBOX, borrowerState);
		sleep(1);
	}

	/**
	 * input Phone, Fax, primaryPhone, primaryMobile, primaryFax
	 */
	public void inputPhoneCellFax(String borrowerPhone, String borrowerFax, String primaryPhone, String primaryMobile, String primaryFax) {
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_PHONE_TEXTBOX,borrowerPhone, timeWait);
		type(driver, Interfaces.ApplicantsPage.FNF_PHONE_TEXTBOX, borrowerPhone);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.FNF_FAX_TEXTBOX, borrowerFax);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.FNF_PRIMARY_PHONE_TEXTBOX, primaryPhone);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.FNF_PRIMARY_MOBILE_TEXTBOX, primaryMobile);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.FNF_PRIMARY_FAX_TEXTBOX, primaryFax);
		sleep(1);
	}
	
	/**
	 * check ''Phone, Fax, BorrowerPhone/ Cell/ Fax' Saved
	 */
	public boolean isPhoneCellFaxSaved(String phone, String fax, String borrowerPhone, String borrowerMobile,String borrowerFax) {
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_PHONE_TEXTBOX, timeWait);
		String realPhone = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_PHONE_TEXTBOX, "value");
		String realFax = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_FAX_TEXTBOX, "value");
		String realBorrPhone = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_PRIMARY_PHONE_TEXTBOX, "value");
		String realBorrMobile = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_PRIMARY_MOBILE_TEXTBOX, "value");
		String realBorrFax = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_PRIMARY_FAX_TEXTBOX, "value");
		return realPhone.contains(phone) && realFax.contains(fax) && realBorrPhone.contains(borrowerPhone) && realBorrMobile.contains(borrowerMobile) && realBorrFax.contains(borrowerFax);
	}

	/**
	 * input Email, ZipCode, borrowerEmail, borrowerZipCode
	 */
	public void inputEmailZipCode(String zipCode,String borrowerEmail, String borrowerZipCode) {
		waitForControl(driver,Interfaces.ApplicantsPage.FNF_ZIP_TEXTBOX, zipCode,timeWait);
		type(driver, Interfaces.ApplicantsPage.FNF_ZIP_TEXTBOX, zipCode);
		sleep(1);
		type(driver,Interfaces.ApplicantsPage.FNF_BORROWER_EMAIL_CONTACT_TEXTBOX,borrowerEmail);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.FNF_BORROWER_ZIP_TEXTBOX,borrowerZipCode);
		sleep(1);
	}

	/**
	 * input Net Income
	 */
	public void inputCurrency(String netIncome) {
		waitForControl(driver,Interfaces.ApplicantsPage.FNF_NET_INCOME_TEXTBOX,	timeWait);
		type(driver, Interfaces.ApplicantsPage.FNF_NET_INCOME_TEXTBOX,netIncome);
		sleep(1);
	}

	/**
	 * input Date of Formation, Date of Fiancials, taxID
	 */
	public void inputDate(String dateOfFormation, String dateOfFiancials) {
		waitForControl(driver,	Interfaces.ApplicantsPage.FNF_DATE_OF_FORMATION_TEXTBOX,	dateOfFormation, timeWait);
		type(driver, Interfaces.ApplicantsPage.FNF_DATE_OF_FORMATION_TEXTBOX,dateOfFormation);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.FNF_DATE_OF_FIANCIALS_TEXTBOX,dateOfFiancials);
		sleep(1);
	}

	/**
	 * input TaxID, Term, Rate (%), Years of relevant Expirence
	 */
	public void inputNumber(String taxID, String term, String ratePecent,String yearOfRelevantExp) {
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_TAX_ID_TEXTBOX,	taxID, timeWait);
		type(driver, Interfaces.ApplicantsPage.FNF_TAX_ID_TEXTBOX, taxID);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.FNF_TERM_TEXTBOX, term);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.FNF_RATE_TEXTBOX, ratePecent);
		sleep(1);
		type(driver,Interfaces.ApplicantsPage.FNF_BORROWER_YEARS_RELEVANT_EXPERIENCE_TEXTBOX, yearOfRelevantExp);
		sleep(1);
	}

	/**
	 * check 'Company/ Borrower Name, Address of Entity/ Borrower, Site' Saved
	 */
	public boolean isCompanyNameAddressSiteCitySaved(String companyName, String street, String city, String companysite,
			String borrowerFirstName,	String borrowerLastName, String borrowerStreet, String borrowerCity) {
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_APPLICANT_COMPANY_NAME_TEXTBOX, timeWait);
		String realCompany = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_APPLICANT_COMPANY_NAME_TEXTBOX, "value");
		String realStreet = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_ADDRESS_TEXTBOX, "value");
		String realCity = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_CITY_TEXTBOX, "value");
		String realCompanySite = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_COMPANY_SITE_TEXTBOX, "value");
		String realBorrFirstName = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_BORROWER_FIRST_NAME_TEXTBOX, "value");
		String realBorrLastName = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_BORROWER_LAST_NAME_TEXTBOX, "value");
		String realBorrStreet = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_BORROWER_ADRESS_TEXTBOX, "value");
		String realBorrCity = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_BORROWER_CITY_TEXTBOX, "value");
		return realCompany.contains(companyName) && realStreet.contains(street) && realCity.contains(city) && realCompanySite.contains(companysite)
		&& realBorrFirstName.contains(borrowerFirstName) && realBorrLastName.contains(borrowerLastName) && realBorrStreet.contains(borrowerStreet) && realBorrCity.contains(borrowerCity);
	}
	
	/**
	 * check 'Company/ Borrower Name, Address of Entity/ Borrower, Site' Fields Set Read-Only
	 */
	public boolean isCompanyNameAddressSiteCityReadOnly(String companyName, String street, String city, String companysite, String borrowerFirstName,	String borrowerLastName, String borrowerStreet, String borrowerCity) {
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_APPLICANT_COMPANY_NAME_TEXTBOX_READ_ONLY, timeWait);
		String realCompany = getText(driver, Interfaces.ApplicantsPage.FNF_APPLICANT_COMPANY_NAME_TEXTBOX_READ_ONLY);
		String realStreet = getText(driver, Interfaces.ApplicantsPage.FNF_ADDRESS_TEXTBOX_READ_ONLY);
		String realCity = getText(driver, Interfaces.ApplicantsPage.FNF_CITY_TEXTBOX_READ_ONLY);
		String realCompanySite = getText(driver, Interfaces.ApplicantsPage.FNF_COMPANY_SITE_TEXTBOX_READ_ONLY);
		String realBorrFirstName = getText(driver, Interfaces.ApplicantsPage.FNF_BORROWER_FIRST_NAME_TEXTBOX_READ_ONLY);
		String realBorrLastName = getText(driver, Interfaces.ApplicantsPage.FNF_BORROWER_LAST_NAME_TEXTBOX_READ_ONLY);
		String realBorrStreet = getText(driver, Interfaces.ApplicantsPage.FNF_BORROWER_ADRESS_TEXTBOX_READ_ONLY);
		String realBorrCity = getText(driver, Interfaces.ApplicantsPage.FNF_BORROWER_CITY_TEXTBOX_READ_ONLY);
		return realCompany.contains(companyName) && realStreet.contains(street) && realCity.contains(city) && realCompanySite.contains(companysite) 
		&& realBorrFirstName.contains(borrowerFirstName) && realBorrLastName.contains(borrowerLastName) && realBorrStreet.contains(borrowerStreet) && realBorrCity.contains(borrowerCity);
	}
	
	/**
	 * check 'Company/ Borrower State, Product Type, Corporate Structure, State of Incorporation/Formation' Saved
	 */
	public boolean isAllComboboxFieldsSaved(String state, String corporateStructure, String stateOfIncorporation,String borrowerState) {
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_STATE_COMBOBOX, timeWait);
		String realState = getItemSelectedCombobox(driver, Interfaces.ApplicantsPage.FNF_STATE_COMBOBOX);
		String realCorporateStructure = getItemSelectedCombobox(driver, Interfaces.ApplicantsPage.FNF_CORPORATE_STRUCTURE_COMBOBOX);
		String realIncorporationFormation = getItemSelectedCombobox(driver, Interfaces.ApplicantsPage.FNF_STATE_OF_INCORPORATION_FORMATION_COMBOBOX);
		String realBorrState = getItemSelectedCombobox(driver, Interfaces.ApplicantsPage.FNF_BORROWER_STATE_COMBOBOX);
		return realState.contains(state) && realCorporateStructure.contains(corporateStructure) && realIncorporationFormation.contains(stateOfIncorporation) 
				&& realBorrState.contains(borrowerState);
	}
	

	/**
	 * check 'Email, Zip code, Borrower Email/ Zip code' Saved
	 */
	public boolean isEmailZipCodeSaved(String zipCode, String borrowerEmail, String borrowerZipCode) {
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_ZIP_TEXTBOX, timeWait);
		String realZipCode = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_ZIP_TEXTBOX, "value");
		String realBorrEmail = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_BORROWER_EMAIL_CONTACT_TEXTBOX, "value");
		String realBorrZipCode = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_BORROWER_ZIP_TEXTBOX, "value");
		return realZipCode.contains(zipCode) && realBorrEmail.contains(borrowerEmail) && realBorrZipCode.contains(borrowerZipCode);
	}
	
	/**
	 * check 'Net Income' Saved
	 */
	public boolean isNetIncomeSaved(String netIncome) {
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_NET_INCOME_TEXTBOX, timeWait);
		String realNetIncome= getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_NET_INCOME_TEXTBOX, "value");
		return realNetIncome.contains(netIncome);
	}
	
	/**
	 * check 'Date of Formation, Date of Fiancials' Saved
	 */
	public boolean isDateSaved(String dateOfFormation, String dateOfFiancials) {
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_DATE_OF_FORMATION_TEXTBOX, timeWait);
		String realDateOfFormation= getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_DATE_OF_FORMATION_TEXTBOX, "value");
		String realDateOfFiancial = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_DATE_OF_FIANCIALS_TEXTBOX, "value");
		return realDateOfFormation.contains(dateOfFormation) && realDateOfFiancial.contains(dateOfFiancials);
	}
	
	/**
	 * check 'TaxID, Term, Rate(%), Years of Relevant Experience ' Saved
	 */
	public boolean isNumberSaved(String taxID,String term, String ratePercent, String borrowerYearRelevantExp) {
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_TAX_ID_TEXTBOX, timeWait);
		String realTaxID= getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_TAX_ID_TEXTBOX, "value");
		String realTerm = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_TERM_TEXTBOX, "value");
		String realRatePercent = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_RATE_TEXTBOX, "value");
		String reaBorrYearRelevantExpt = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_BORROWER_YEARS_RELEVANT_EXPERIENCE_TEXTBOX, "value");
		return realTaxID.contains(taxID) && realTerm.contains(term) && realRatePercent.contains(ratePercent) 
		&& reaBorrYearRelevantExpt.contains(borrowerYearRelevantExp);
	}
	
	/**
	 * check Borrower/ Primary Contact created from Salesforce
	 */
	public boolean isBorrowerPrimaryContactSaved(String companyName, String lastName, String email) {
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_APPLICANT_COMPANY_NAME_TEXTBOX, timeWait);
		String realCompany = getAttributeValue(driver, Interfaces.ApplicantsPage.FNF_APPLICANT_COMPANY_NAME_TEXTBOX, "value");
		String realLastname = getAttributeValue(driver, Interfaces.ApplicantsPage.APPLICANT_CONTACT_DETAIL_LASTNAME, "value");
		String realEmail = getAttributeValue(driver, Interfaces.ApplicantsPage.BORROWER_EMAIL_CONTACT_TEXTBOX, "value");
		return realCompany.contains(companyName) && realLastname.contains(lastName) && realEmail.contains(email);
	}
	
	/**
	 * check Loans display in Opportunity table
	 * @return loanName, productType, loanOwner, approvedLineAmount
	 */
	public boolean isLoansDisplayInOpportunityTable(String loanName, String productType, String loanOwner, String approvedLineAmount) {
		waitForControl(driver, Interfaces.ApplicantsPage.DYNAMIC_LOAN_DISPLAY_IN_OPPOTUNITY_TABLE, loanName, productType, loanOwner, approvedLineAmount, timeWait);
		return isControlDisplayed(driver, Interfaces.ApplicantsPage.DYNAMIC_LOAN_DISPLAY_IN_OPPOTUNITY_TABLE, loanName, productType, loanOwner, approvedLineAmount);
	}
	
	/**
	 * click On 'Save' button
	 */
	public void clickOnSaveButton() {
		waitForControl(driver, Interfaces.ApplicantsPage.SAVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		sleep(2);
	}

	/**
	 * check Record Saved Message Displays
	 * @return recordMessage
	 */
	public boolean isRecordSavedMessageDisplays() {
		waitForControl(driver, Interfaces.ApplicantsPage.RECORD_SAVED_MESSAGE,	timeWait);
		return isControlDisplayed(driver, Interfaces.ApplicantsPage.RECORD_SAVED_MESSAGE);
	}

	/**
	 * check Error Message Displayed
	 * 
	 * @return
	 */
	public boolean isErrorMessageDisplayed(String messageContent) {
		waitForControl(driver,	Interfaces.ApplicantsPage.ERROR_MESSAGE_DISPLAYED,messageContent, timeWait);
		return isControlDisplayed(driver,Interfaces.ApplicantsPage.ERROR_MESSAGE_DISPLAYED,messageContent);
	}
	
	/**
	 * check Info Message Displayed
	 * 
	 * @return
	 */
	public boolean isInfoMessageDisplayed(String messageContent) {
		waitForControl(driver,	Interfaces.ApplicantsPage.INFO_MESSAGE_DISPLAYED,messageContent, timeWait);
		return isControlDisplayed(driver,Interfaces.ApplicantsPage.INFO_MESSAGE_DISPLAYED,messageContent);
	}

	/**
	 * select Corporate Structure
	 */
	public void selectCorporateStructure(String corporateStructure) {
		waitForControl(driver,Interfaces.ApplicantsPage.FNF_CORPORATE_STRUCTURE_COMBOBOX,	timeWait);
		selectItemCombobox(driver,Interfaces.ApplicantsPage.FNF_CORPORATE_STRUCTURE_COMBOBOX,corporateStructure);
	}

	/**
	 * upload Document File
	 */
	public void uploadDocumentFile(String fileName) {
		if (driver.toString().toLowerCase().contains("internetexplorerdriver")) {
			keyPressing("space");
		}
		sleep(1);
		Common.getCommon().openFileForUpload(driver, fileName);
		sleep();
	}

	/**
	 * get number items of Administration Page
	 */
	public int getNumberOfItemInListOfUsers() {
		int numberOfItem = 0;
		waitForControl(driver, Interfaces.ApplicantsPage.ALL_ITEMS_OF_DOCUMENTS, timeWait);
		numberOfItem = countElement(driver,Interfaces.ApplicantsPage.ALL_ITEMS_OF_DOCUMENTS);
		return numberOfItem;
	}

	/**
	 * click On 'New' button
	 */
	public void clickOnNewButton() {
		waitForControl(driver, Interfaces.ApplicantsPage.NEW_BUTTON, timeWait);
		click(driver, Interfaces.ApplicantsPage.NEW_BUTTON);
		sleep();
	}

	/**
	 * click On 'Submit to Lender for Review' button
	 */
	public void clickOnSubmitLenderReviewButton() {
		waitForControl(driver,	Interfaces.ApplicantsPage.SUBMIT_LENDER_REVIEW_BUTTON, timeWait);
		sleep(1);
		executeJavaScript(driver,	"document.getElementById('SubmitToReview').click();");
		sleep(1);
		keyPressing("enter");
		sleep();
	}

	/**
	 * check Error Message Display With Text
	 */
	public boolean isErrorMessageDisplayWithText(String messageContent) {
		waitForControl(driver, Interfaces.ApplicantsPage.ERROR_MESSAGE_DISPLAY,	messageContent, 5);
		return isControlDisplayed(driver,	Interfaces.ApplicantsPage.ERROR_MESSAGE_DISPLAY, messageContent);
	}

	/**
	 * click On 'Search' button
	 */
	public void clickOnSearchButton(String applicantName) {
		waitForControl(driver, Interfaces.ApplicantsPage.BORROWER_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.ApplicantsPage.BORROWER_NAME_TEXTBOX, applicantName);
		sleep(1);
		waitForControl(driver, Interfaces.ApplicantsPage.SEARCH_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(1);
	}
	
	/**
	 * click On 'Search' button
	 */
	public void clickOnSearchButton() {
		waitForControl(driver, Interfaces.ApplicantsPage.SEARCH_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(1);
	}

	/**
	 * check Loans Display With Correct Info
	 */
	public boolean isApplicantDisplayWithCorrectInfo(String companyName,
			String primaryContact) {
		waitForControl(driver,	Interfaces.ApplicantsPage.FNF_APPLICANT_COMPANY_NAME_TEXTBOX,timeWait);
		String realLoanName = getAttributeValue(driver,Interfaces.ApplicantsPage.FNF_APPLICANT_COMPANY_NAME_TEXTBOX,"value");
		String realStatus = getItemSelectedCombobox(driver,Interfaces.ApplicantsPage.FNF_BORROWER_LAST_NAME_TEXTBOX);
		return realLoanName.contains(companyName)&& realStatus.contains(primaryContact);
	}

	/**
	 * click On 'Applicant Name' Field
	 */
	public void clickOnBorrowerSummary() {
		waitForControl(driver, Interfaces.ApplicantsPage.BORROWER_SUMMARY_TAB,5);
		click(driver, Interfaces.ApplicantsPage.BORROWER_SUMMARY_TAB);
		sleep();
	}

	/**
	 * click 'Applicant Name'
	 */
	public void clickOnApplicantName() {
		waitForControl(driver, Interfaces.ApplicantsPage.BORROWER_NAME_ITEM,timeWait);
		click(driver, Interfaces.ApplicantsPage.BORROWER_NAME_ITEM);
		sleep(1);
	}

	/**
	 * check Detail Message Email
	 */
	public boolean isSavedSearchNameDisplay2(String detailMessageEmail) {
		waitForControl(driver,	Interfaces.ApplicantsPage.DYNAMIC_DETAIL_MESSAGE_EMAIL,detailMessageEmail, timeWait);
		return isControlDisplayed(driver,Interfaces.ApplicantsPage.DYNAMIC_DETAIL_MESSAGE_EMAIL,detailMessageEmail);
	}
	
	/**
	 * click Open 'Signer' link
	 */
	public void clickOpenSignerLink(String signer) {
		waitForControl(driver, Interfaces.ApplicantsPage.GNS_OPEN_SIGNER_LINK, signer, timeWait);
		click(driver, Interfaces.ApplicantsPage.GNS_OPEN_SIGNER_LINK, signer);
		sleep();
	}
	
	
	
	/********* FNFApplicantsPage ***********/
	/**
	 * click 'Reject' Applicant button
	 */
	public void clickRejectApplicantButton() {
		waitForControl(driver, Interfaces.FNFApplicantsPage.REJECT_APPLICANT_BUTTON,timeWait);
		click(driver, Interfaces.FNFApplicantsPage.REJECT_APPLICANT_BUTTON);
		sleep(1);
	}
	
	/**
	 * switch to 'Reject' frame
	 */
	public WebDriver switchToRejectFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.FNFApplicantsPage.REJECT_APPLICANT_FRAME, timeWait);
		sleep();
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.FNFApplicantsPage.REJECT_APPLICANT_FRAME)));
		return driver;
	}
	
	/**
	 * select Reject Type 
	 */
	public void selectRejectType(String rejectType) {
		waitForControl(driver, Interfaces.FNFApplicantsPage.REJECT_TYPE_COMBOBOX,	timeWait);
		selectItemCombobox(driver, Interfaces.FNFApplicantsPage.REJECT_TYPE_COMBOBOX, rejectType);
		sleep(2);
	}

	/**
	 * select Reject Reason
	 */
	public void selectRejectReason(String rejectReason) {
		waitForControl(driver, Interfaces.FNFApplicantsPage.REJECT_REASON_COMBOX,	timeWait);
		selectItemCombobox(driver, Interfaces.FNFApplicantsPage.REJECT_REASON_COMBOX, rejectReason);
		sleep(1);
	}
	
	/**
	 * input Reject Comment
	 */
	public void inputRejectComment(String rejectComment) {
		waitForControl(driver, Interfaces.FNFApplicantsPage.REJECT_COMMENT_TEXTAREA, timeWait);
		type(driver, Interfaces.FNFApplicantsPage.REJECT_COMMENT_TEXTAREA, rejectComment);
		sleep(1);
	}
	
	/**
	 * check 'Reject Reason' Display
	 */
	public boolean isRejectReasonDisplay(String rejectReason) {
		waitForControl(driver, Interfaces.FNFApplicantsPage.LIST_OF_REJECT_REASON_COMBOX, rejectReason, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFApplicantsPage.LIST_OF_REJECT_REASON_COMBOX, rejectReason);
	}
	
	/**
	 * click 'Cancel' button
	 */
	public void clickCancelRejectButton() {
		waitForControl(driver, Interfaces.FNFApplicantsPage.CANCEL_REJECT_BUTTON,timeWait);
		click(driver, Interfaces.FNFApplicantsPage.CANCEL_REJECT_BUTTON);
		sleep(1);
	}
	
	/**
	 * click 'Save' button
	 */
	public void clickSaveRejectButton() {
		waitForControl(driver, Interfaces.FNFApplicantsPage.SAVE_REJECT_BUTTON,timeWait);
		click(driver, Interfaces.FNFApplicantsPage.SAVE_REJECT_BUTTON);
		sleep(1);
	}
	
	/**
	 * check 'Reject Type, Reject Reason, Reject By, Reject Comment' Fields Set Read-Only
	 */
	public boolean isRejectApplicantSaveAndReadOnly(String rejectType, String rejectReason, String rejectBy, String rejectComment) {
		waitForControl(driver, Interfaces.FNFApplicantsPage.FNF_APPLICANT_REJECT_TYPE_READ_ONLY_COMBOBOX, timeWait);
		String realRejectType = getText(driver, Interfaces.FNFApplicantsPage.FNF_APPLICANT_REJECT_TYPE_READ_ONLY_COMBOBOX);
		String realRejectReason = getText(driver, Interfaces.FNFApplicantsPage.FNF_APPLICANT_REJECT_REASON_READ_ONLY_COMBOBOX);
		String realRejectBy = getText(driver, Interfaces.FNFApplicantsPage.FNF_APPLICANT_REJECT_BY_READ_ONLY_COMBOBOX);
		String realComment = getText(driver, Interfaces.FNFApplicantsPage.FNF_APPLICANT_REJECT_COMMENT_READ_ONLY_TEXTAREA);
		return realRejectType.contains(rejectType) && realRejectReason.contains(rejectReason) && realRejectBy.contains(rejectBy) && realComment.contains(rejectComment);
	}
	
	/**
	 * open Loan Name in Opportunity table
	 */
	public FNFLoansPage openLoanNameInOpportunityTable(String loanName) {
		waitForControl(driver, Interfaces.ApplicantsPage.LOAN_NAME_IN_OPPORTUNITY_TABLE, loanName, timeWait);
		click(driver, Interfaces.ApplicantsPage.LOAN_NAME_IN_OPPORTUNITY_TABLE, loanName);
		sleep();
		return PageFactory.getFNFLoansPage(driver, ipClient);
	}
	
	/**
	 *is Loan Name displayed Opportunity table
	 */
	public Boolean isLoanDisplayedInOpportunityTable(String loanName) {
		waitForControl(driver, Interfaces.ApplicantsPage.LOAN_NAME_IN_OPPORTUNITY_TABLE, loanName, 10);
		return isControlDisplayed(driver, Interfaces.ApplicantsPage.LOAN_NAME_IN_OPPORTUNITY_TABLE, loanName);
	}
	
	/**
	 * click New Loan Button
	 */
	public FNFLoansPage clickNewLoanButton() {
		waitForControl(driver, Interfaces.ApplicantsPage.NEW_LOAN_BUTTON, timeWait);
		click(driver, Interfaces.ApplicantsPage.NEW_LOAN_BUTTON);
		sleep(2);
		if(driver.toString().toLowerCase().contains("internetexplorer")){
			sleep(6);
		}
		return PageFactory.getFNFLoansPage(driver, ipClient);
	}
	
	public void inputFirstNameAndPhoneNumberTextBoxApplicant(String name, String phoneNumber){
		waitForControl(driver, Interfaces.ApplicantsPage.PRIMARY_FIRST_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.ApplicantsPage.PRIMARY_FIRST_NAME_TEXTBOX, name);
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_PHONE_TEXTBOX, timeWait);
		type(driver, Interfaces.ApplicantsPage.FNF_PHONE_TEXTBOX, phoneNumber);
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_PRIMARY_PHONE_TEXTBOX, timeWait);
		type(driver, Interfaces.ApplicantsPage.FNF_PRIMARY_PHONE_TEXTBOX, phoneNumber);
	}
	
	/**
	 * input Applicant Contact Info
	 */
	public void setApplicantNameToDefault(String firstName, String lastName) {
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_BORROWER_FIRST_NAME_TEXTBOX, timeWait);
		if(getText(driver, Interfaces.ApplicantsPage.FNF_BORROWER_FIRST_NAME_TEXTBOX).contains(firstName) && getText(driver, Interfaces.ApplicantsPage.FNF_BORROWER_FIRST_NAME_TEXTBOX).contains(lastName)) return;
		type(driver, Interfaces.ApplicantsPage.FNF_BORROWER_FIRST_NAME_TEXTBOX,	firstName);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.FNF_BORROWER_LAST_NAME_TEXTBOX, lastName);
		sleep(1);
		clickOnSaveButton();
	}
	
	/**
	 * input Applicant Name
	 */
	public void inputApplicantName(String applicantName) {
		waitForControl(driver,	Interfaces.ApplicantsPage.BORROWER_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.ApplicantsPage.BORROWER_NAME_TEXTBOX,	applicantName);
		sleep(1);
	}
	
	/**
	 * input Email
	 */
	public void inputEmail(String email) {
		waitForControl(driver,	Interfaces.ApplicantsPage.FNF_BORROWER_SEARCH_EMAIL_TEXTBOX, timeWait);
		type(driver, Interfaces.ApplicantsPage.FNF_BORROWER_SEARCH_EMAIL_TEXTBOX,	email);
		sleep(1);
	}
	
	/**
	 * input Date Invited
	 */
	public void inputDateInvited(String dateInvited) {
		waitForControl(driver,	Interfaces.ApplicantsPage.FNF_BORROWER_SEARCH_DATE_INVITED_TEXTBOX, timeWait);
		type(driver, Interfaces.ApplicantsPage.FNF_BORROWER_SEARCH_DATE_INVITED_TEXTBOX, dateInvited);
		sleep(1);
	}
	
	/**
	 * input phone numbers
	 */
	public void inputPhoneNumbers(String phone) {
		waitForControl(driver,	Interfaces.ApplicantsPage.FNF_BORROWER_SEARCH_PHONE_TEXTBOX, timeWait);
		type(driver, Interfaces.ApplicantsPage.FNF_BORROWER_SEARCH_PHONE_TEXTBOX, phone);
		sleep(1);
	}
	
	/**
	 * select Active radio button
	 */
	public void selectActiveRadioButton(String status) {
		waitForControl(driver,	Interfaces.ApplicantsPage.BORROWER_NAME_TEXTBOX, timeWait);
		if(status.contains("Yes")) 
		click(driver, Interfaces.ApplicantsPage.DYNAMIC_FNF_BORROWER_SEARCH_ACTIVE_RADIO_BUTTON, "1");
		else if(status.contains("No")) click(driver, Interfaces.ApplicantsPage.DYNAMIC_FNF_BORROWER_SEARCH_ACTIVE_RADIO_BUTTON, "0");
		else click(driver, Interfaces.ApplicantsPage.DYNAMIC_FNF_BORROWER_SEARCH_ACTIVE_RADIO_BUTTON, "");
	}
	
	/**
	 * get Field index
	 */
	public String getIndexOfField(String fieldName){
		waitForControl(driver,	Interfaces.ApplicantsPage.FNF_BORROWER_SEARCH_HEADER, timeWait);
		int numbersOfField = countElement(driver, Interfaces.ApplicantsPage.FNF_BORROWER_SEARCH_HEADER);
		for(int i=2;i<=numbersOfField; i++){
			String index = Integer.toString(i);
			if(getText(driver,	Interfaces.ApplicantsPage.DYNAMIC_FNF_BORROWER_SEARCH_HEADER, index).contains(fieldName)) return index;
		}
		return null;
	}
	
	/**
	 * get Field text
	 */
	public String getRecordValue(String fieldName){
		String index = getIndexOfField(fieldName);
		waitForControl(driver,	Interfaces.ApplicantsPage.DYNAMIC_FNF_BORROWER_SEARCH_RESULT, index, timeWait);
		return getText(driver,	Interfaces.ApplicantsPage.DYNAMIC_FNF_BORROWER_SEARCH_RESULT, index);
	}
	
	/**
	 * check record displayed
	 */
	public Boolean isRecordValueDisplayedCorrectly(String fieldName, String value){
		String index = getIndexOfField(fieldName);
		waitForControl(driver,	Interfaces.ApplicantsPage.DYNAMIC_FNF_BORROWER_SEARCH_RESULT, index, timeWait);
		return getText(driver,	Interfaces.ApplicantsPage.DYNAMIC_FNF_BORROWER_SEARCH_RESULT, index).contains(value);
	}
	
	/**
	 * check record displayed
	 */
	public Boolean isAllRecordValuesDisplayedCorrectly(String companyName, String email, String dateInvited, String phone, String status){
		sleep(2);
		return (isRecordValueDisplayedCorrectly("Company Name",companyName) 
				&& isRecordValueDisplayedCorrectly("Email",email) 
				&& isRecordValueDisplayedCorrectly("Phone",phone) 
				&& isRecordValueDisplayedCorrectly("Date Invited",dateInvited) 
				&& isRecordValueDisplayedCorrectly("Active",status));
	}
	
	/**
	 * click on check all link
	 */
	public void clickOnCheckAllLink() {
		waitForControl(driver,	Interfaces.ApplicantsPage.CHECK_ALL_LINK, timeWait);
		click(driver, Interfaces.ApplicantsPage.CHECK_ALL_LINK);
		sleep(1);
	}
	
	/**
	 * click Make Inactive Button
	 */
	public void clickMakeInactiveButton() {
		waitForControl(driver, Interfaces.ApplicantsPage.MAKE_INACTIVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('SetInactive').click();");
		sleep();
		acceptJavascriptAlert(driver);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * click Make Active Button 
	 */
	public void clickMakeActiveButton() {
		waitForControl(driver, Interfaces.ApplicantsPage.MAKE_ACTIVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('SetActive').click();");
		sleep();
		acceptJavascriptAlert(driver);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}
	
	private WebDriver driver;
	private String ipClient;
}