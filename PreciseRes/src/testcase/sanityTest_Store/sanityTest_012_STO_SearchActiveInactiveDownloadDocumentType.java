package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Constant;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;

public class sanityTest_012_STO_SearchActiveInactiveDownloadDocumentType extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		accountNumber = getUniqueNumber();
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + accountNumber;
		propertiesFileName = "StoreDataTape.xlsx";
		csvExtension = "List of Documents.csv";
		newDocumentType = "New Document Type";
		documentType = "Title Commitment";
		documentFileName = "datatest1.pdf";
		address = "1020 Bonforte Blvd";
		active = "Yes";
		inActive = "No";
		either = "Either";
		loadedYes = "Yes";
		loadedNo = "No";
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 01 - Search by Document #")
	public void SearchActiveInactive_01_SearchByDocumentNumber() {

		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("Precondition - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("Precondition - Step 07: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("Precondition - Step 08: Click 'Add Properties' button");
		transactionPage.clickAddPropertyButton();

		log.info("Precondition - Step 09: Load 'Property Excel Template' file");
		transactionPage.uploadFileProperties(propertiesFileName);

		log.info("Precondition - Step 10. Search with Address name'");
		transactionPage.searchPropertyAddress(address);

		log.info("Precondition - Step 11. Open a property detail");
		transactionPage.openPropertyDetail(address);

		log.info("Precondition - Step 12. Click 'New Property Documents' button");
		transactionPage.clickNewPropertyDocumentsButton();

		log.info("Precondition - Step 13. Input to 'Other Document Type' textbox");
		transactionPage.inputOtherDocumentType(newDocumentType);

		log.info("Precondition - Step 14. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("Document_01 - Step 15. Click 'Go to Property' button");
		transactionPage.clickGoToPropertyButtonAtDocumentTypeDetails();

		log.info("Precondition - Step 16. Click 'New Property Documents' button");
		transactionPage.clickNewPropertyDocumentsButton();

		log.info("Document_05 - Step 17. Select document type");
		transactionPage.selectDocumentType(documentType);

		log.info("Document_05 - Step 18. Upload document file name");
		transactionPage.uploadDocumentFileName(documentFileName);

		log.info("Document_05 - Step 19. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("Document_05 - Step 20. Click 'Go to Property' button");
		transactionPage.clickGoToPropertyButtonAtDocumentTypeDetails();

		log.info("SearchActiveInactive_01 - Step 01: Open Property Documents tab");
		transactionPage.openPropertyDocumentsTab();

		log.info("SearchActiveInactive_01 - Step 02. Get value 'Document #'");
		documentID = transactionPage.getDocumentID();

		log.info("SearchActiveInactive_01 - Step 03. Search with Document #");
		transactionPage.searchPropertyDocumentsDocumentNumber(documentID);

		log.info("VP: The Documents with Document # display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, address, documentType, loadedYes, active));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 02 - Search by Property Address ")
	public void SearchActiveInactive_02_SearchByPropertyAddress() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_02 - Step 01: Open Property Documents tab");
		transactionPage.openPropertyDocumentsTab();

		log.info("SearchActiveInactive_02 - Step 02. Search with Address");
		transactionPage.searchPropertyDocumentsAddress(address);

		log.info("VP: The Documents with Address display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, address, documentType, loadedYes, active));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 03 - Search by Document Type")
	public void SearchActiveInactive_03_SearchByDocumentType() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_03 - Step 01: Open Property Documents tab");
		transactionPage.openPropertyDocumentsTab();

		log.info("SearchActiveInactive_03 - Step 02. Search with Document Type");
		transactionPage.searchPropertyDocumentType(documentType);

		log.info("VP: The Documents with Document Type display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, address, documentType, loadedYes, active));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 04 - Search by Document Loaded")
	public void SearchActiveInactive_04_SearchByDocumentLoaded() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_04 - Step 01: Open Property Documents tab");
		transactionPage.openPropertyDocumentsTab();

		log.info("SearchActiveInactive_04 - Step 02. Search with Document Loaded is Yes");
		transactionPage.searchPropertyDocumentsDocumentLoaded(address, loadedYes);

		log.info("VP: The Documents with Document Loaded is Yes display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, address, documentType, loadedYes, active));

		log.info("SearchActiveInactive_04 - Step 03. Search with Document Loaded is No");
		transactionPage.searchPropertyDocumentsDocumentLoaded(address, loadedNo);

		log.info("VP: The Documents with Document Loaded is No display in table");
		verifyTrue(transactionPage.isDocumentTypeNoDisplayOnSearchPropertyTransactionLesseeTab(address, newDocumentType, loadedNo, active));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 05 - Search by Document active status")
	public void SearchActiveInactive_05_SearchByActiveStatus() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_05 - Step 01: Open Property Documents tab");
		transactionPage.openPropertyDocumentsTab();

		log.info("SearchActiveInactive_05 - Step 01: Search with Active status");
		transactionPage.searchByActiveOrInactive(documentID, active);

		log.info("VP: The Documents with Document Active display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, address, documentType, loadedYes, active));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 06 - Search by Document either status")
	public void SearchActiveInactive_06_SearchByEitherStatus() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_06 - Step 01: Open Property Documents tab");
		transactionPage.openPropertyDocumentsTab();

		log.info("SearchActiveInactive_06 - Step 02: Search with Either status");
		transactionPage.searchByActiveOrInactive(documentID, either);

		log.info("VP: The Documents with Document Active display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, address, documentType, loadedYes, active));

		log.info("SearchActiveInactive_06 - Step 03: Select Document Type checkbox in List Documents table");
		transactionPage.isSelectedPropertyCheckbox(address);

		log.info("SearchActiveInactive_06 - Step 04: Click 'Make Inactive' button");
		transactionPage.clickMakeInactiveButton();

		log.info("SearchActiveInactive_06 - Step 05: Search with Either status");
		transactionPage.searchByActiveOrInactive(documentID, either);

		log.info("VP: The Documents with Document Inactive display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, address, documentType, loadedYes, inActive));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 07 - Search by Document inactive status")
	public void SearchActiveInactive_07_SearchByInactiveStatus() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_07 - Step 01: Open Property Documents tab");
		transactionPage.openPropertyDocumentsTab();

		log.info("SearchActiveInactive_07 - Step 02: Search with Inactive status");
		transactionPage.searchByActiveOrInactive(documentID, inActive);

		log.info("VP: The Documents with Document Inactive display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, address, documentType, loadedYes, inActive));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 08 - Make a Document is Active")
	public void SearchActiveInactive_08_MakeADocumentIsActive() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_08 - Step 01: Open Property Documents tab");
		transactionPage.openPropertyDocumentsTab();

		log.info("SearchActiveInactive_08 - Step 02: Search with Inactive status");
		transactionPage.searchByActiveOrInactive(documentID, inActive);

		log.info("VP: The Documents with Document Inactive display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, address, documentType, loadedYes, inActive));

		log.info("SearchActiveInactive_08 - Step 03: Select Document checkbox in List Documents table");
		transactionPage.isSelectedPropertyCheckbox(address);

		log.info("SearchActiveInactive_08 - Step 04: Click 'Make Active' button");
		transactionPage.clickMakeActiveButtonAtTransactionPropertyTab();

		log.info("SearchActiveInactive_08 - Step 05: Search with Active status");
		transactionPage.searchByActiveOrInactive(documentID, active);

		log.info("VP: The Documents with Document Active display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, address, documentType, loadedYes, active));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 09 - Make a Document is Inactive")
	public void SearchActiveInactive_09_MakeADocumentIsInactive() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("Precondition - Step 04: Open Property tab");
		transactionPage.openPropertyTab();
		propertyNumberBefore = transactionPage.getNumberOfAddressInPropertiesTab(address);

		log.info("SearchActiveInactive_09 - Step 01: Open Property Documents tab");
		transactionPage.openPropertyDocumentsTab();

		log.info("SearchActiveInactive_09 - Step 02: Search Document by Active status");
		transactionPage.searchByActiveOrInactive(documentID, active);

		log.info("VP: The Documents with Document Active display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, address, documentType, loadedYes, active));

		log.info("SearchActiveInactive_09 - Step 03: Select Document checkbox in List Documents table");
		transactionPage.isSelectedPropertyCheckbox(address);

		log.info("SearchActiveInactive_09 - Step 04: Click 'Make Inactive' button");
		transactionPage.clickMakeInactiveButton();

		log.info("SearchActiveInactive_09 - Step 05: Search Document by Inactive status");
		transactionPage.searchByActiveOrInactive(documentID, inActive);

		log.info("VP: The Documents with Document Inactive display in table");
		verifyTrue(transactionPage.isDocumentTypeYesDisplayOnSearchPropertyTransactionLesseeTab(documentID, address, documentType, loadedYes, inActive));
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 10 - Download list Document (Excel file that is created)")
	public void SearchActiveInactive_10_DownloadListDocumentCSV() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_10 - Step 01: Open Property Documents tab");
		transactionPage.openPropertyDocumentsTab();

		log.info("SearchActiveInactive_10 - Step 02: Click 'Download CSV' image");
		transactionPage.deleteAllFileInFolder();
		transactionPage.clickOnDownloadCSVImage();

		log.info("SearchActiveInactive_10 - Step 03. Wait for file downloaded complete");
		transactionPage.waitForDownloadFileFullnameCompleted(csvExtension);

		log.info("VP: File number in folder equal 1");
		countFile = transactionPage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("SearchActiveInactive_10 - Step 04. Delete the downloaded file");
		transactionPage.deleteContainsFileName(csvExtension);
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 11 - Verify the Document for the Property is decreased by one")
	public void SearchActiveInactive_11_DocumentOfPropertyDecreasedByOne() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("SearchActiveInactive_11 - Step 01: Open Property tab");
		transactionPage.openPropertyTab();
		propertyNumberAfter = transactionPage.getNumberOfAddressInPropertiesTab(address);

		log.info("VP: Required Documents for the Property is decreased by one");
		propertyNumberBefore = propertyNumberBefore - 1;
		verifyEquals(propertyNumberBefore, propertyNumberAfter);
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private int propertyNumberBefore, propertyNumberAfter, countFile;
	private String storeLenderUsername, storeLenderPassword, propertiesFileName, address;
	private String xmlFileName, companyName, accountNumber, active, inActive, either;
	private String csvExtension, loadedYes, loadedNo;
	private String newDocumentType, documentType, documentFileName, documentID;
}