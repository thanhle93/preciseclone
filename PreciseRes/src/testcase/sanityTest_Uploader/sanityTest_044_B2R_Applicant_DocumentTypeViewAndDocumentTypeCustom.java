package sanityTest_Uploader;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_044_B2R_Applicant_DocumentTypeViewAndDocumentTypeCustom extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameMember = "auto-B2R-member2014";
		passwordMember = "change";
		usernameApplicant = Constant.USERNAME_B2R_BORROWER;
		passwordApplicant = Constant.PASSWORD_B2R_BORROWER;
		loanName = "UploaderUdiTesting " + getUniqueNumber();
		applicantName = "Consolidation Demo Borrower";
		loanStatus = Constant.LOAN_STATUS_B2R;
		propertiesFileName = "B2R Data Tape Keyword.xlsx";
		documentType1 = "P3 - Purchase Contract/HUD-1 Settlement Stmt";
		documentType2 = "P5 - Zoning Compliance Letter";
		property1Name = "37412 OAKHILL ST";
		fileNameDocument1 = "datatest 1.pdf";
		fileNameDocument2 = "datatest 2.pdf";
		documentTypeCustom1 = "Custom Type 1" + getUniqueNumber();
		documentTypeCustom2 = "Custom Type 2" + getUniqueNumber();
		documentTypeCustom3 = "Custom Type 3" + getUniqueNumber();
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}
	
	@Test(groups = { "regression" },description = "UploaderLender25 - Then go to a Folder and change document type of a placeholder")
	public void Uploader_18_ChangeDocumentTypeOfPlaceholder()
	{
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameMember, passwordMember,	false);

		log.info("Precondition 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);

		log.info("Precondition 04. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Precondition 05. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("Precondition 06. Click on logged user name on the top right corner");
//	    loansPage.clickOnLoggedUserName( driver );
//	    uploaderPageUrl = loansPage .getUploaderPageUrl( driver);
//		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
		
		log.info("Precondition 07. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("Precondition 08. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameApplicant, passwordApplicant);
		
		log.info("Precondition 09. Click on 'Keywords' button");
		uploaderPage.clickOnKeywordsButton();
		
		log.info("Precondition 10. Add new Keywords");
		uploaderPage.enterNewKeyword(documentTypeCustom1);
		uploaderPage.enterNewKeyword(documentTypeCustom2);
		uploaderPage.enterNewKeyword(documentTypeCustom3);
				
		log.info("Precondition 11. Click Ok button");
		uploaderPage.clickOkButtonOnKeywords();
		
		log.info("Uploader_18 - Step 01. Select loans item");
		uploaderPage.clickChangeGroupedView();
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("Uploader_18 - Step 02. Open Property folder");
		uploaderPage.clickPropertiesFolder();
				
		log.info("Uploader_18 - Step 03. Open Property item");
		uploaderPage.selectPropertyFolder(property1Name);
		
		log.info("Uploader_18 - Step 04. Upload document for a document type 1");
		uploaderPage.clickOnSelectNewFileIconByType(documentType1);
		uploaderPage.uploadFileOnUploader(fileNameDocument1);
		
		log.info("Uploader_18 - Step 05. Wait for upload complete");
		uploaderPage.waitForUploadCompleteOnUploader(fileNameDocument1);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(fileNameDocument1));
		
		log.info("VP: The document type 1 display correctly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, documentType1));
		
		log.info("Uploader_18 - Step 06. Upload document for a document type 2");
		uploaderPage.clickOnSelectNewFileIconByType(documentType2);
		uploaderPage.uploadFileOnUploader(fileNameDocument2);
		
		log.info("Uploader_18 - Step 07. Wait for upload complete");
		uploaderPage.waitForUploadCompleteOnUploader(fileNameDocument2);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(fileNameDocument2));
		
		log.info("VP: The document type display correctly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument2, documentType2));
		
		log.info("Uploader_18 - Step 08. Select 'Document Type' radio button");
		uploaderPage.selectRadioButtonForDocumentType();
		
		log.info("Uploader_18 - Step 09. Open 'Document Type' item");
		uploaderPage.selectPropertyFolder(documentType1);
		
		log.info("Uploader_18 - Step 10. Update document type for document");
		uploaderPage.clickOnEditDocumentIcon(fileNameDocument1);
		
		log.info("Uploader_18 - Step 11. Select new document type");
		uploaderPage.selectDocumentType(documentTypeCustom1);
		
		log.info("Uploader_18 - Step 12. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.clickYesButton();
		
		log.info("VP: Document is removed from 'Document Type 1' to 'Document Type 2'");
		if(uploaderPage.isPropertyFolderDisplayed(documentType1)){
			uploaderPage.selectPropertyFolder(documentType1);
			verifyFalse(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, property1Name));
		}
		uploaderPage.selectPropertyFolder(documentTypeCustom1);
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, property1Name));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender25 - Change document type with Custom document type")
	public void Uploader_19_ChangeDocumentTypeWithCustomDocumentType()
	{
		log.info("Uploader_19 - Step 01. Select 'Address' radio button");
		uploaderPage.selectRadioButtonForAddress();
		
		log.info("Uploader_19 - Step 02. Open Property item");
		uploaderPage.selectPropertyFolder(property1Name);
		
		log.info("Uploader_19 - Step 03. Update document type for document");
		uploaderPage.clickOnEditDocumentIcon(fileNameDocument2);
		
		log.info("Uploader_19 - Step 04. Select new document type");
		uploaderPage.selectDocumentType(documentTypeCustom2);
		
		log.info("Uploader_19 - Step 05. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.clickYesButton();
		
		log.info("VP: The document type display correctly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument2, documentTypeCustom2));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender26 - Multi change document type with Custom document type")
	public void Uploader_20_MultiChangeDocumentTypeWithCustomDocumentType()
	{
		log.info("Uploader_20 - Step 01. Open Property item");
		uploaderPage.selectPropertyFolder(property1Name);
		
		log.info("Uploader_20 - Step 02. Select document");
		uploaderPage.selectCheckboxForDocumentName(documentTypeCustom1);
		uploaderPage.selectCheckboxForDocumentName(documentTypeCustom2);
		
		log.info("Uploader_20 - Step 03. Click on change document type link");
		uploaderPage.selectChangeDocumentTypeLink();
		
		log.info("Uploader_20 - Step 04. Select new document type");
		uploaderPage.selectDocumentTypeOnEditDocumentType(documentTypeCustom3);
		
		log.info("Uploader_20 - Step 05. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.clickYesButton();
		
		log.info("VP: These document is changed type");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, documentTypeCustom3));
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument2, documentTypeCustom3));
		verifyFalse(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, documentTypeCustom1));
		verifyFalse(uploaderPage.isDocumentTypeDisplay(fileNameDocument2, documentTypeCustom2));
		
		log.info("Uploader_20 - Step 06. Delete new Keywords 01");
		uploaderPage.clickOnKeywordsButton();
		uploaderPage.deleteNewKeyword(documentTypeCustom1);
		uploaderPage.clickOkButtonOnKeywords();
		
		log.info("Uploader_20 - Step 07. Delete new Keywords 02");
		uploaderPage.clickOnKeywordsButton();
		uploaderPage.deleteNewKeyword(documentTypeCustom2);
		uploaderPage.clickOkButtonOnKeywords();
		
		log.info("Uploader_20 - Step 08. Delete new Keywords 03");
		uploaderPage.clickOnKeywordsButton();
		uploaderPage.deleteNewKeyword(documentTypeCustom3);
		uploaderPage.clickOkButtonOnKeywords();
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private LoansPage loansPage;
	private String usernameMember, passwordMember, applicantName, loanStatus;
	private String usernameApplicant, passwordApplicant;
	private String loanName, uploaderPageUrl, propertiesFileName, fileNameDocument1, fileNameDocument2;
	private String property1Name, documentType1, documentType2, documentTypeCustom3, documentTypeCustom1, documentTypeCustom2;
}