package page;

import java.util.Set;
import java.util.Stack;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import common.Common;
import PreciseRes.Interfaces;

public class FNFPropertiesPage extends AbstractPage{
	public FNFPropertiesPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}
	
	/**
	 * click New Properties Button
	 */
	public void clickNewPropertyButton() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.NEW_PROPERTIES_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('New').click();");
		sleep(1);
	}
	
	/**
	 * click New Properties Button
	 */
	public void clickNewPropertyButtonPropertyTab() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.ADD_PROPERTIES_BUTTON, timeWait);
		executeClick(driver, Interfaces.FNFPropertiesPage.ADD_PROPERTIES_BUTTON);
		sleep(1);
	}
	
	/**
	 * input Loan, Applicant, Address, City, State, Zip Code
	 */
	public void inputRequiredInformationProperties(String companyName, String loanName, String address, String city,String state, String zipCode, String date, String number, String price, String propertyType, String transactionType, String titleCompany, String escrowAgent, String insuranceCompany) {
		waitForControl(driver, Interfaces.FNFPropertiesPage.APPLICANT_COMBOBOX, 10);
		if (!companyName.equals("")&& !isControlDisplayed(driver, Interfaces.FNFPropertiesPage.APPLICANT_READ_ONLY_COMBOBOX)) {
			selectItemCombobox(driver, Interfaces.FNFPropertiesPage.APPLICANT_COMBOBOX, companyName);
		}
		sleep(1);
		if (!loanName.equals("") && !isControlDisplayed(driver, Interfaces.FNFPropertiesPage.LOAN_READ_ONLY_LINK)) {
			selectItemCombobox(driver, Interfaces.FNFPropertiesPage.LOAN_COMBOBOX, loanName);
		}
		
		////
		if (!propertyType.equals("")) {
			selectItemCombobox(driver, Interfaces.FNFPropertiesPage.PROPERTY_TYPE_DROPDOWN, propertyType);
		}
		sleep(1);
		if (!transactionType.equals("")) {
			selectItemCombobox(driver, Interfaces.FNFPropertiesPage.TRANSACTION_TYPE_DROPDOWN, transactionType);
		}
		
		click(driver, Interfaces.FNFPropertiesPage.TITLE_COMPANY_DROPDOWN);
		if(!isControlDisplayed(driver, Interfaces.FNFPropertiesPage.DYNAMIC_OPTION_ITEM,titleCompany)){
			click(driver, Interfaces.FNFPropertiesPage.NEW_TITLE_COMPANY_LINK);
			
			switchToCreateTitleCompanyFrame(driver);
			waitForControl(driver, Interfaces.FNFPropertiesPage.NEW_COMPANY_NAME_TEXTBOX, timeWait);
			type(driver, Interfaces.FNFPropertiesPage.NEW_COMPANY_NAME_TEXTBOX, titleCompany);
			type(driver, Interfaces.FNFPropertiesPage.CONTACT_LAST_NAME_TEXTBOX, titleCompany);
			type(driver, Interfaces.FNFPropertiesPage.NEW_COMPANY_EMAIL_TEXTBOX, "minhdam06@gmail.com");
			click(driver, Interfaces.FNFPropertiesPage.SAVE_COMPANY_BUTTON);
			sleep();
			switchToTopWindowFrame(driver);
			switchToBorrowerNotificationFrame(driver);
			waitForControl(driver, Interfaces.FNFPropertiesPage.CANCEL_SEND_EMAIL_BUTTON, timeWait);
			click(driver, Interfaces.FNFPropertiesPage.CANCEL_SEND_EMAIL_BUTTON);
			switchToTopWindowFrame(driver);
			sleep();
		}
		
		if (!titleCompany.equals("")) {
			selectItemCombobox(driver, Interfaces.FNFPropertiesPage.TITLE_COMPANY_DROPDOWN, titleCompany);
		}
		sleep(1);
		
		//Create new escrow Agent
		click(driver, Interfaces.FNFPropertiesPage.ESCROW_AGENT_DROPDOWN);
		if(!isControlDisplayed(driver, Interfaces.FNFPropertiesPage.DYNAMIC_OPTION_ITEM,escrowAgent)){
			click(driver, Interfaces.FNFPropertiesPage.NEW_ESCROW_LINK);
			switchToCreateEscrowFrame(driver);
			waitForControl(driver, Interfaces.FNFPropertiesPage.NEW_COMPANY_NAME_TEXTBOX, timeWait);
			type(driver, Interfaces.FNFPropertiesPage.NEW_COMPANY_NAME_TEXTBOX, escrowAgent);
			type(driver, Interfaces.FNFPropertiesPage.CONTACT_LAST_NAME_TEXTBOX, escrowAgent);
			type(driver, Interfaces.FNFPropertiesPage.NEW_COMPANY_EMAIL_TEXTBOX, "minhdam06@gmail.com");
			click(driver, Interfaces.FNFPropertiesPage.SAVE_COMPANY_BUTTON);
			sleep();
			switchToTopWindowFrame(driver);
			switchToBorrowerNotificationFrame(driver);
			click(driver, Interfaces.FNFPropertiesPage.CANCEL_SEND_EMAIL_BUTTON);
			switchToTopWindowFrame(driver);
			sleep();
		}
		
		if (!escrowAgent.equals("")) {
			selectItemCombobox(driver, Interfaces.FNFPropertiesPage.ESCROW_AGENT_DROPDOWN, escrowAgent);
		}
		sleep(1);
		
		//Create new insuarance company
		click(driver, Interfaces.FNFPropertiesPage.INSURANCE_COMPANY_DROPDOWN);
		if(!isControlDisplayed(driver, Interfaces.FNFPropertiesPage.DYNAMIC_OPTION_ITEM, insuranceCompany)){
			click(driver, Interfaces.FNFPropertiesPage.NEW_INSURANCE_COMPANY_LINK);
			switchToCreateInsuranceCompanyFrame(driver);
			waitForControl(driver, Interfaces.FNFPropertiesPage.NEW_COMPANY_NAME_TEXTBOX, timeWait);
			type(driver, Interfaces.FNFPropertiesPage.NEW_COMPANY_NAME_TEXTBOX, insuranceCompany);
			type(driver, Interfaces.FNFPropertiesPage.CONTACT_LAST_NAME_TEXTBOX, insuranceCompany);
			type(driver, Interfaces.FNFPropertiesPage.NEW_COMPANY_EMAIL_TEXTBOX, "minhdam06@gmail.com");
			click(driver, Interfaces.FNFPropertiesPage.SAVE_COMPANY_BUTTON);
			sleep();
			switchToTopWindowFrame(driver);
			switchToBorrowerNotificationFrame(driver);
			click(driver, Interfaces.FNFPropertiesPage.CANCEL_SEND_EMAIL_BUTTON);
			switchToTopWindowFrame(driver);
			sleep();
		}
		
		if (!insuranceCompany.equals("")) {
			selectItemCombobox(driver, Interfaces.FNFPropertiesPage.INSURANCE_COMPANY_DROPDOWN, insuranceCompany);
		}
		
		sleep(1);
		type(driver, Interfaces.FNFPropertiesPage.ADDRESS_TEXTBOX, address);
		sleep(1);
		type(driver, Interfaces.FNFPropertiesPage.CITY_TEXTBOX, city);
		sleep(1);
		selectItemCombobox(driver, Interfaces.FNFPropertiesPage.STATE_COMBOBOX, state);
		sleep(1);
		type(driver, Interfaces.FNFPropertiesPage.ZIP_TEXTBOX, zipCode);
		sleep(1);
		//////
		type(driver, Interfaces.FNFPropertiesPage.ASSESSORS_PARCEL_NUMBER_TEXTBOX, number);
		sleep(1);
		type(driver, Interfaces.FNFPropertiesPage.BED_RROOM_TEXTBOX, number);
		sleep(1);
		type(driver, Interfaces.FNFPropertiesPage.BATH_ROOM_TEXTBOX, number);
		sleep(1);
		type(driver, Interfaces.FNFPropertiesPage.SQUARE_FOOTAGE_TEXTBOX, number);
		sleep(1);
		type(driver, Interfaces.FNFPropertiesPage.NUMBER_OF_UNITS_TEXTBOX, number);
		sleep(1);
		type(driver, Interfaces.FNFPropertiesPage.PURCHASE_PRICE_TEXTBOX, price);
		sleep(1);
		type(driver, Interfaces.FNFPropertiesPage.PURCHASE_DATE_TEXTBOX, date);
		sleep(1);
		type(driver, Interfaces.FNFPropertiesPage.REQUESTED_FUNDING_DATE_TEXTBOX, date);
		sleep(1);
		
	}
	
	/**
	 * click On 'Save' button
	 */
	public void clickOnSaveButton() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.SAVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		sleep(1);
	}
	
	/**
	 * check Record Saved Message Displays
	 * 
	 * @return
	 */
	public boolean isRecordSavedMessageDisplays() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.RECORD_SAVED_MESSAGE, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFPropertiesPage.RECORD_SAVED_MESSAGE);
	}
	
	/**
	 * check 'Lender, Applicant, Address, City, State, Zip Code' Saved [Applicant]
	 */
	public boolean isApplicantPropertiesRequiredInformationSaved(String loanName, String applicant, String address, String city, String state, String zipCode,String date, String number, String price, String propertyType, String transactionType, String titleCompany, String escrowAgent, String insuranceCompany) {
		String floatNumber = number+".00";
		waitForControl(driver, Interfaces.FNFPropertiesPage.LOAN_READ_ONLY_LINK, 10);
		String realLoan;
		if(isControlDisplayed(driver, Interfaces.FNFPropertiesPage.LOAN_READ_ONLY_LINK))
		realLoan = getText(driver, Interfaces.FNFPropertiesPage.LOAN_READ_ONLY_LINK);
		else realLoan = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.LOAN_COMBOBOX, "value");
		String realApplicant = getText(driver, Interfaces.FNFPropertiesPage.APPLICANT_READ_ONLY_COMBOBOX);
		String realAddress = getAttributeValue(driver, Interfaces.FNFPropertiesPage.ADDRESS_TEXTBOX, "value");
		String realCity = getAttributeValue(driver, Interfaces.FNFPropertiesPage.CITY_TEXTBOX, "value");
		String realState = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.STATE_COMBOBOX, "value");
		String realZipCode = getAttributeValue(driver, Interfaces.FNFPropertiesPage.ZIP_TEXTBOX, "value");
		//
		String realPropertyType = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.PROPERTY_TYPE_DROPDOWN, "value");
		String realTransactionType = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.TRANSACTION_TYPE_DROPDOWN, "value");
		String realTitleCompany = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.TITLE_COMPANY_DROPDOWN, "value");
		String realEscrowAgent = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.ESCROW_AGENT_DROPDOWN, "value");
		String realInsuranceCompany = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.INSURANCE_COMPANY_DROPDOWN, "value");
		String realAssessorsParcelNumber = getAttributeValue(driver, Interfaces.FNFPropertiesPage.ASSESSORS_PARCEL_NUMBER_TEXTBOX, "value");
		String realBedrooms = getAttributeValue(driver, Interfaces.FNFPropertiesPage.BED_RROOM_TEXTBOX, "value");
		String realBathrooms = getAttributeValue(driver, Interfaces.FNFPropertiesPage.BATH_ROOM_TEXTBOX, "value");
		String realSquareFootage = getAttributeValue(driver, Interfaces.FNFPropertiesPage.SQUARE_FOOTAGE_TEXTBOX, "value");
		String realNumberofUnits = getAttributeValue(driver, Interfaces.FNFPropertiesPage.NUMBER_OF_UNITS_TEXTBOX, "value");
		String realPurchasePrice = getAttributeValue(driver, Interfaces.FNFPropertiesPage.PURCHASE_PRICE_TEXTBOX, "value");
		String realPurchaseDate = getAttributeValue(driver, Interfaces.FNFPropertiesPage.PURCHASE_DATE_TEXTBOX, "value");
		String realRequestedFundingDate = getAttributeValue(driver, Interfaces.FNFPropertiesPage.REQUESTED_FUNDING_DATE_TEXTBOX, "value");
		
		return realLoan.contains(loanName) && realApplicant.contains(applicant) && realAddress.contains(address) 
		&& realCity.contains(city) && realState.contains(state) && realZipCode.contains(zipCode) && realPropertyType.contains(propertyType) && realTransactionType.contains(transactionType) 
		&& realTitleCompany.contains(titleCompany) && realEscrowAgent.contains(escrowAgent) && realInsuranceCompany.contains(insuranceCompany) && realAssessorsParcelNumber.contains(number) && realBedrooms.contains(number) 
		&& realBathrooms.contains(number) && realSquareFootage.contains(number) && realNumberofUnits.contains(number) && realPurchasePrice.contains(price) && realPurchaseDate.contains(date) 
		&& realRequestedFundingDate.contains(date);
	}
	
	public boolean isApplicantPropertiesInformationUploaded(String loanName, String applicant, String address, String city, String state, String zipCode,String date, String number, String price, String propertyType, String transactionType, String titleCompany, String escrowAgent) {
		String floatNumber = number+".00";
		waitForControl(driver, Interfaces.FNFPropertiesPage.LOAN_READ_ONLY_LINK, 10);
		String realLoan;
		if(isControlDisplayed(driver, Interfaces.FNFPropertiesPage.LOAN_READ_ONLY_LINK))
		realLoan = getText(driver, Interfaces.FNFPropertiesPage.LOAN_READ_ONLY_LINK);
		else realLoan = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.LOAN_COMBOBOX, "value");
		String realApplicant = getText(driver, Interfaces.FNFPropertiesPage.APPLICANT_READ_ONLY_COMBOBOX);
		String realAddress = getAttributeValue(driver, Interfaces.FNFPropertiesPage.ADDRESS_TEXTBOX, "value");
		String realCity = getAttributeValue(driver, Interfaces.FNFPropertiesPage.CITY_TEXTBOX, "value");
		String realState = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.STATE_COMBOBOX, "value");
		String realZipCode = getAttributeValue(driver, Interfaces.FNFPropertiesPage.ZIP_TEXTBOX, "value");
		//
		String realPropertyType = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.PROPERTY_TYPE_DROPDOWN, "value");
		String realTransactionType = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.TRANSACTION_TYPE_DROPDOWN, "value");
		String realTitleCompany = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.TITLE_COMPANY_DROPDOWN, "value");
		String realEscrowAgent = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.ESCROW_AGENT_DROPDOWN, "value");
		String realBedrooms = getAttributeValue(driver, Interfaces.FNFPropertiesPage.BED_RROOM_TEXTBOX, "value");
		String realBathrooms = getAttributeValue(driver, Interfaces.FNFPropertiesPage.BATH_ROOM_TEXTBOX, "value");
		String realSquareFootage = getAttributeValue(driver, Interfaces.FNFPropertiesPage.SQUARE_FOOTAGE_TEXTBOX, "value");
		String realNumberofUnits = getAttributeValue(driver, Interfaces.FNFPropertiesPage.NUMBER_OF_UNITS_TEXTBOX, "value");
		String realPurchasePrice = getAttributeValue(driver, Interfaces.FNFPropertiesPage.PURCHASE_PRICE_TEXTBOX, "value");
		String realPurchaseDate = getAttributeValue(driver, Interfaces.FNFPropertiesPage.PURCHASE_DATE_TEXTBOX, "value");
		String realRequestedFundingDate = getAttributeValue(driver, Interfaces.FNFPropertiesPage.REQUESTED_FUNDING_DATE_TEXTBOX, "value");
		
		return realLoan.contains(loanName) && realApplicant.contains(applicant) && realAddress.contains(address) 
		&& realCity.contains(city) && realState.contains(state) && realZipCode.contains(zipCode) && realPropertyType.contains(propertyType) && realTransactionType.contains(transactionType) 
		&& realTitleCompany.contains(titleCompany) && realEscrowAgent.contains(escrowAgent) && realBedrooms.contains(number) 
		&& realBathrooms.contains(number) && realSquareFootage.contains(number) && realNumberofUnits.contains(number) && realPurchasePrice.contains(price) && realPurchaseDate.contains(date) 
		&& realRequestedFundingDate.contains(date);
	}
	
	

	/**
	 * check Detail Message Display With Text
	 */
	public boolean isSavedNameMessageDisplayWithText(String messageContent) {
		waitForControl(driver, Interfaces.FNFPropertiesPage.SAVE_NAME_MESSAGE_DISPLAY_WITH_TEXT,
				messageContent, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFPropertiesPage.SAVE_NAME_MESSAGE_DISPLAY_WITH_TEXT,
				messageContent);
	}

	/**
	 * click On 'Back To List' button
	 */
	public void clickOnBackToListButton() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.BACK_TO_LIST_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('ListProperties').click();");
		executeJavaScript(driver, "document.getElementById('GotoLoan').click();");
		sleep(1);
	}
	
	/**
	 * search Property address
	 */
	public void searchAddress(String address) {
		waitForControl(driver,	Interfaces.FNFPropertiesPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.FNFPropertiesPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX,address);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep();
	}
	
	/**
	 * open Property Detail
	 */
	public FNFPropertiesPage openPropertyDetail(String address) {
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_ADDRESS_PROPERTY,address, timeWait);
		click(driver, Interfaces.FNFPropertiesPage.DYNAMIC_ADDRESS_PROPERTY, address);
		sleep();
		return PageFactory.getFNFPropertiesPage(driver, ipClient);
	}

	/**
	 * upload Document File
	 */
	public void uploadDocumentFile(String fileName) {
		waitForControl(driver, Interfaces.PropertiesPage.CHOOSE_FILE_BUTTON, timeWait);
		click(driver, Interfaces.PropertiesPage.CHOOSE_FILE_BUTTON);
		if(driver.toString().toLowerCase().contains("internetexplorerdriver"))
		{
			keyPressing("space");
		}
		sleep(1);
		Common.getCommon().openFileForUpload(driver, fileName);
		sleep();
	}
	
	/**
	 * click Save Button
	 */
	public void clickSaveButton() {
		waitForControl(driver, Interfaces.PropertiesPage.SAVE_BUTTON, timeWait);
//		driver.findElement(By.id("Save")).sendKeys(Keys.ENTER);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		sleep(1);
	}
	
	
	/**
	 * search Active
	 */
	public void searchActive() {
		waitForControl(driver,Interfaces.DocumentsPage.ACTIVE_RADIO_BUTTON, timeWait);
		click(driver, Interfaces.DocumentsPage.ACTIVE_RADIO_BUTTON);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep();
	}
	
	/**
	 * get Document ID In [General Loan Document - Corporate Entity Documents] Tab
	 */
	public String getDocumentID() {
		waitForControl(driver, Interfaces.DocumentsPage.GET_DOCUMENT_ID, timeWait);
		String documentID = getText(driver, Interfaces.DocumentsPage.GET_DOCUMENT_ID);
		return documentID;
	}
	
	public void searchDocumentID(String documentID) {
		waitForControl(driver, Interfaces.DocumentsPage.DOCUMENT_ID_TEXTBOX,	documentID, timeWait);
		type(driver, Interfaces.DocumentsPage.DOCUMENT_ID_TEXTBOX, documentID);
		sleep(1);
		click(driver, Interfaces.DocumentsPage.ACTIVE_RADIO_YES_BUTTON);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(1);
	}
	
	/**
	 * check Document Loaded To Document Type
	 */
	public boolean isDocumentLoadedToDocumentType(String docType, String fileName) {
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL,docType, fileName, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL,docType, fileName);
	}
	
	/**
	 * check Private Checkbox Checked
	 */
	public boolean isPrivateCheckboxChecked() {
		waitForControl(driver, Interfaces.PropertiesPage.PRIVATE_CHECKBOX, timeWait);
		return isCheckboxChecked(driver, Interfaces.PropertiesPage.PRIVATE_CHECKBOX);
	}
	
	/**
	 * check Document Type Display On Property Detail
	 */
	public boolean isDocumentTypeDisplayOnPropertyDetail(String docType) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_DOC_TYPE_ON_PROPERTY_DETAIL, docType, 5);
		return isControlDisplayed(driver, Interfaces.PropertiesPage.DYNAMIC_DOC_TYPE_ON_PROPERTY_DETAIL, docType);
	}
	
	/**
	 * check Uploaded Document Opened
	 */
	public boolean isUploadedDocumentOpened(String fileName) {
		sleep();
		String currentHandle = driver.getWindowHandle();
		openWindowHandles.push(driver.getWindowHandle());
		Set<String> newHandles = driver.getWindowHandles();
		newHandles.removeAll(openWindowHandles);
		String handle = newHandles.iterator().next();
		driver.switchTo().window(handle);
		String url = getCurrentUrl(driver);
		boolean exists = url.contains(fileName);
		driver.close();
		driver.switchTo().window(currentHandle);
		return exists;
	}
	
	/**
	 * open Document Of Document Type On Property Detail
	 */
	public void openDocumentOfDocumentTypeOnPropertyDetail(String documentType, String documentName) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL,documentType, documentName, timeWait);
		click(driver, Interfaces.PropertiesPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL,	documentType, documentName);
		sleep(3);
	}
	
	/**
	 * update Property information
	 */
	public void updatePropertyInfor(String newCity, String newState, String newZip, String newPropertyType) {
		waitForControl(driver, Interfaces.PropertiesPage.PROPERTY_CITY_TEXTBOX, timeWait);
		type(driver, Interfaces.PropertiesPage.PROPERTY_CITY_TEXTBOX, newCity);
		sleep(1);
		selectItemCombobox(driver, Interfaces.PropertiesPage.PROPERTY_STATE_COMBOBOX, newState);
		sleep(1);
		type(driver, Interfaces.PropertiesPage.PROPERTY_ZIP_TEXTBOX, newZip);
		sleep(1);
		selectItemCombobox(driver, Interfaces.PropertiesPage.PROPERTY_TYPE_COMBOBOX, newPropertyType);
		sleep(1);
		clickSaveButton();
	}
	
	/**
	 * check 'Document Type' On Properties Detail Loaded
	 */
	public boolean isDocumentTypeOnPropertiesDetailLoaded(String documentType) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_DOCTYPE_ON_PROPERTY_DETAIL,
				documentType, 5);
		return isControlDisplayed(driver, Interfaces.PropertiesPage.DYNAMIC_DOCTYPE_ON_PROPERTY_DETAIL,
				documentType);
	}
	
	/**
	 * upload Dynamic Document File by Place Holder
	 */
	public void uploadDynamicDocumentFileByPlaceHolder(String documentType, String fileName) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_CHOOSE_FILE_BUTTON_PLACE_HOLDER, documentType, timeWait);
		click(driver, Interfaces.PropertiesPage.DYNAMIC_CHOOSE_FILE_BUTTON_PLACE_HOLDER, documentType);
		if(driver.toString().toLowerCase().contains("internetexplorerdriver"))
		{
			keyPressing("space");
		}
		sleep(1);
		Common.getCommon().openFileForUpload(driver, fileName);
		sleep();
	}
	
	/**
	 * open Property Documents Tab
	 */
	public void openPropertyDocumentsTab() {
		waitForControl(driver, Interfaces.LoansPage.PROPERTY_DOCUMENTS_TAB,timeWait);
		click(driver, Interfaces.LoansPage.PROPERTY_DOCUMENTS_TAB);
		sleep();
	}
	
	/**
	 * Switch driver to iFrame
	 * @return driver
	 */
	public WebDriver switchToCreateTitleCompanyFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.FNFPropertiesPage.CREATE_TITLE_COMPANY_IFRAME, timeWait);
		sleep(5);
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.FNFPropertiesPage.CREATE_TITLE_COMPANY_IFRAME)));
		return driver;
	}
	
	/**
	 * Switch driver to iFrame
	 * @return driver
	 */
	public WebDriver switchToCreateEscrowFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.FNFPropertiesPage.CREATE_ESCROW_IFRAME, timeWait);
		sleep(5);
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.FNFPropertiesPage.CREATE_ESCROW_IFRAME)));
		return driver;
	}
	
	/**
	 * Switch driver to iFrame
	 * @return driver
	 */
	public WebDriver switchToCreateInsuranceCompanyFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.FNFPropertiesPage.CREATE_INSURANCE_COMPANY_IFRAME, timeWait);
		sleep(5);
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.FNFPropertiesPage.CREATE_INSURANCE_COMPANY_IFRAME)));
		return driver;
	}
	
	/**
	 * Switch driver to iFrame
	 * @return driver
	 */
	public WebDriver switchToBorrowerNotificationFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.FNFPropertiesPage.BORROWER_NOTIFICATION_IFRAME, timeWait);
		sleep(5);
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.FNFPropertiesPage.BORROWER_NOTIFICATION_IFRAME)));
		return driver;
	}
	
	/**
	 * click On 'New' Documents button
	 */
	public FNFDocumentPage clickOnNewDocumentsButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.DOCUMENTS_NEW_BUTTON,timeWait);
		click(driver, Interfaces.FNFLoansPage.DOCUMENTS_NEW_BUTTON);
		sleep();
		return PageFactory.getFNFDocumentsPage(driver, ipClient);
	}
	
	/** 
	 * Open Placeholders
	 */
	public void openPlaceholders(String name){
		waitForControl(driver, Interfaces.FNFDocumentsPage.DYNAMIC_PLACEHOLDER, name, timeWait);
		click(driver, Interfaces.FNFDocumentsPage.DYNAMIC_PLACEHOLDER, name);
	}
	
	/**
	 * check 'Status' is changed successful
	 */
	public boolean isStatusChangedSuccessfull(String statusApplicant) {
		waitForControl(driver, Interfaces.FNFPropertiesPage.PROPERTY_STATUS, timeWait);
		String realStatus = getText(driver, Interfaces.FNFPropertiesPage.PROPERTY_STATUS);
		return realStatus.contains(statusApplicant);
	}
	
	/**
	 * click On 'Back to Due Diligence' button
	 */
	public void clickOnBackToDueDiligenceButton() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.FNF_BACK_TO_DUE_DILIGENCE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('MovetoPreviousStatus').click();");
		sleep(1);
	}
	
	/**
	 * click On 'Send to Pending' button
	 */
	public void clickOnSendToPendingButton() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.FNF_SEND_TO_PENDING_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('MovetoNextStatus').click();");
		sleep(1);
	}
	
	/**
	 * click On 'Back to Pending' button
	 */
	public void clickOnBackToPendingButton() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.FNF_BACK_TO_PENDING_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('MovetoPreviousStatus').click();");
		sleep(1);
	}
	
	/**
	 * click On 'Send to Active' button
	 */
	public void clickOnSendToActiveButton() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.FNF_SEND_TO_ACTIVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('MovetoNextStatus').click();");
		sleep(1);
	}
	
	/**
	 * click On 'Re-open' button
	 */
	public void clickOnReopenButton() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.REOPEN_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('ReOpen').click();");
		sleep(1);
	}
	
	/**
	 * check 'On Hold' button displays
	 */
	public Boolean isPutOnHoldButtonDisplayed() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.PUT_ON_HOLD_BUTTON, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFPropertiesPage.PUT_ON_HOLD_BUTTON);
	}
	
	/**
	 * click On 'On Hold' button
	 */
	public void clickOnPutOnHoldButton() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.PUT_ON_HOLD_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('PutOnHold').click();");
		sleep(1);
	}
	
	/**
	 * check 'Rescind' button displays
	 */
	public Boolean isRescindButtonDisplayed() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.RESCIND_BUTTON, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFPropertiesPage.RESCIND_BUTTON);
	}
	
	/**
	 * click On 'Rescind' button
	 */
	public void clickOnRescindButton() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.RESCIND_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Rescind').click();");
		sleep(1);
	}
	
	/**
	 * check 'Cancel' button displays
	 */
	public Boolean isCancelButtonDisplayed() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.CANCEL_BUTTON, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFPropertiesPage.PUT_ON_HOLD_BUTTON);
	}
	
	/**
	 * click On 'Cancel' button
	 */
	public void clickOnCancelButton() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.CANCEL_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Cancel').click();");
		sleep(1);
	}
	
	/**
	 * select Insurance Approved
	 */
	public void selectInsuranceApproved(String value) {
		waitForControl(driver, Interfaces.FNFPropertiesPage.INSURANCE_APPROVED_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.FNFPropertiesPage.INSURANCE_APPROVED_DROPDOWN, value);
		sleep(1);
	}
	
	/**
	 * check Error Message Displayed
	 * 
	 * @return
	 */
	public boolean isErrorMessageDisplayed(String messageContent) {
		waitForControl(driver,	Interfaces.ApplicantsPage.ERROR_MESSAGE_DISPLAYED,messageContent, timeWait);
		return isControlDisplayed(driver,Interfaces.ApplicantsPage.ERROR_MESSAGE_DISPLAYED,messageContent);
	}
	
	/**
	 * check 'State and Property type' default data [Applicant]
	 */
	public boolean isDropdownDefaultValueDisplaysCorrectly(String state, String propertyType, String transactionType) {
		waitForControl(driver, Interfaces.FNFPropertiesPage.STATE_COMBOBOX, 10);
		String realState = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.STATE_COMBOBOX, "value");
		String realPropertyType = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.PROPERTY_TYPE_DROPDOWN, "value");
		String realTransactionType = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.TRANSACTION_TYPE_DROPDOWN, "value");
		return realState.contains(state) && realPropertyType.contains(propertyType)&& realTransactionType.contains(transactionType);
	}
	
	/**
	 * check loaded Document files displayed correctly
	 */
	public boolean isDocumentLoadedToDocumentType(String fileName) {
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENT_FILE_IN_DOCUMENT_FOLDER, fileName,  30);
		return isControlDisplayed(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENT_FILE_IN_DOCUMENT_FOLDER, fileName);
	}
	
	/**
	 * open Document Folder
	 */
	public FNFDocumentPage openDocumentsFolder(String documentFolder) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.DYNAMIC_DOCUMENTS_FOLDER, documentFolder, timeWait);
		click(driver, Interfaces.FNFDocumentsPage.DYNAMIC_DOCUMENTS_FOLDER, documentFolder);
		sleep(1);
		return PageFactory.getFNFDocumentsPage(driver, ipClient);
	}
	
	/**
	 * Select borrower
	 * @param borrower
	 */
	public void selectBorrower(String borrower){
		waitForControl(driver, Interfaces.FNFPropertiesPage.BORROWER_DROPDOWN, 10);
		selectItemCombobox(driver, Interfaces.FNFPropertiesPage.BORROWER_DROPDOWN, borrower);
	}
	
	/**
	 * upload File Properties
	 */
	public void uploadFileProperties(String propertiesFileName) {
		waitForControl(driver, Interfaces.LoansPage.UPLOAD_FILE_BUTTON,
				timeWait);
		executeClick(driver, Interfaces.LoansPage.UPLOAD_FILE_BUTTON);
		waitForControl(driver, Interfaces.LoansPage.CHOOSE_FILE_BUTTON,
				timeWait);
		click(driver, Interfaces.LoansPage.CHOOSE_FILE_BUTTON);
		if (driver.toString().toLowerCase().contains("internetexplorerdriver")) {
			keyPressing("space");
		}
		sleep(1);
		Common.getCommon().openFileForUpload(driver, propertiesFileName);
		sleep();
		clickSaveLoadPropertyButton();
		sleep();
	}
	
	/**
	 * click Save Load Property Button
	 */
	public void clickSaveLoadPropertyButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_LOAD_PROPERTY_BUTTON,
				timeWait);
		click(driver, Interfaces.LoansPage.SAVE_LOAD_PROPERTY_BUTTON);
		sleep();
	}
	
	/**
	 * Check Load data message title display
	 * 
	 * @param N/A
	 */
	public boolean isBasicDetailMessageTitleDisplay(String messageContent) {
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_BASIC_DETAIL_MESSAGE_TITLE, messageContent, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFPropertiesPage.DYNAMIC_BASIC_DETAIL_MESSAGE_TITLE, messageContent);
	}
	
	/**
	 * click Go to Loan Properties
	 */
	public void clickGoBackToLoanProperties() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.BACK_TO_LOAN_PROPERTIES, timeWait);
		executeClick(driver, Interfaces.FNFPropertiesPage.BACK_TO_LOAN_PROPERTIES);
		sleep();
	}
	
	/**
	 * click edit Title company
	 */
	public void clickEditTitleCompanyVendor() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.EDIT_TITLE_COMPANY_LINK, timeWait);
		executeClick(driver, Interfaces.FNFPropertiesPage.EDIT_TITLE_COMPANY_LINK);
		sleep();
	}
	
	/**
	 * click edit Escrow company
	 */
	public void clickEditEscrowCompanyVendor() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.EDIT_ESCROW_AGENT_LINK, timeWait);
		executeClick(driver, Interfaces.FNFPropertiesPage.EDIT_ESCROW_AGENT_LINK);
		sleep();
	}
	
	/**
	 * Check Vendor information displays correctly
	 * 
	 * @param N/A
	 */
	public boolean isVendorInformationCorrect(String vendorName, String contactName, String phoneNumber, String global, String suffix) {
		waitForControl(driver, Interfaces.FNFPropertiesPage.COMPANY_NAME_VENDOR_TEXTFIELD, timeWait);
		String firstName, lastName;
		
		if(contactName.contains(" ")){
			String name[] = contactName.trim().split(" ");
			firstName = name[0];
			lastName = name[1];
			
			String actualFirstName = getAttributeValue(driver, Interfaces.FNFPropertiesPage.VENDOR_FIRST_NAME_TEXTFIELD, "value");
			String actualLastName = getAttributeValue(driver, Interfaces.FNFPropertiesPage.VENDOR_LAST_NAME_TEXTFIELD, "value");
			String actualvendorName = getAttributeValue(driver, Interfaces.FNFPropertiesPage.COMPANY_NAME_VENDOR_TEXTFIELD, "value");
			String actualphoneNumber = getAttributeValue(driver, Interfaces.FNFPropertiesPage.VENDOR_PHONE_TEXTFIELD, "value");
			String actualglobal = getText(driver, Interfaces.FNFPropertiesPage.SELECTED_GLOBAL_OPTION);
			String actualSuffix = "0";
			if(isControlDisplayed(driver, Interfaces.FNFPropertiesPage.SUFFIX_LABLE))
			actualSuffix = getText(driver, Interfaces.FNFPropertiesPage.SUFFIX_LABLE);
			
			if(!firstName.equals(actualFirstName)){
				System.out.println("Expected: "+firstName+ " Actual: "+  actualFirstName);
				return false;
			}
			if(!lastName.equals(actualLastName)){
				System.out.println("Expected: "+lastName+ " Actual: "+  actualLastName);
				return false;
			}
			if(!vendorName.equals(actualvendorName)){
				System.out.println("Expected: "+vendorName+ " Actual: "+  actualvendorName);
				return false;
			}
			if(!phoneNumber.equals(actualphoneNumber)){
				System.out.println("Expected: "+phoneNumber+ " Actual: "+  actualphoneNumber);
				return false;
			}
			if(!suffix.equals(actualSuffix)){
				System.out.println("Expected: "+suffix+ " Actual: "+  actualSuffix);
				return false;
			}
			if(!global.equals(actualglobal)){
				System.out.println("Expected: "+global+ " Actual: "+  actualglobal);
				return false;
			}
			return true;
		}
		
		else{
		String actualLastName = getAttributeValue(driver, Interfaces.FNFPropertiesPage.VENDOR_LAST_NAME_TEXTFIELD, "value");
		String actualvendorName = getAttributeValue(driver, Interfaces.FNFPropertiesPage.COMPANY_NAME_VENDOR_TEXTFIELD, "value");
		String actualphoneNumber = getAttributeValue(driver, Interfaces.FNFPropertiesPage.VENDOR_PHONE_TEXTFIELD, "value");
		String actualglobal = getText(driver, Interfaces.FNFPropertiesPage.SELECTED_GLOBAL_OPTION);
		String actualSuffix = "0";
		if(isControlDisplayed(driver, Interfaces.FNFPropertiesPage.SUFFIX_LABLE))
		actualSuffix = getText(driver, Interfaces.FNFPropertiesPage.SUFFIX_LABLE);
		
		if(!contactName.equals(actualLastName)){
			System.out.println("Expected: "+contactName+ " Actual: "+  actualLastName);
			return false;
		}
		if(!vendorName.equals(actualvendorName)){
			System.out.println("Expected: "+vendorName+ " Actual: "+  actualvendorName);
			return false;
		}
		if(!phoneNumber.equals(actualphoneNumber)){
			System.out.println("Expected: "+phoneNumber+ " Actual: "+  actualphoneNumber);
			return false;
		}
		if(!suffix.equals(actualSuffix)){
			System.out.println("Expected: "+suffix+ " Actual: "+  actualSuffix);
			return false;
		}
		if(!global.equals(actualglobal)){
			System.out.println("Expected: "+global+ " Actual: "+  actualglobal);
			return false;
		}
		return true;
		}
	}
	
	/**
	 * click 'Back to Applicant' button
	 * @return FNF Loan Page
	 */
	public FNFPropertiesPage clickBackToProperty() {
		waitForControl(driver, Interfaces.FNFDocumentsPage.BACK_TO_PROPERTY_BUTTON,	timeWait);
		executeJavaScript(driver, "document.getElementById('BackToProperty').click();");
		sleep(1);
		return PageFactory.getFNFPropertiesPage(driver, ipClient);
	}
	
	/**
	 * check add property button displayed
	 */
	public boolean isAddPropertyButtonDisplayed() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.ADD_PROPERTIES_BUTTON, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFPropertiesPage.ADD_PROPERTIES_BUTTON);
	}
	
	/**
	 * check import property button displayed
	 */
	public boolean isImportPropertiesButtonDisplayed() {
		waitForControl(driver, Interfaces.FNFPropertiesPage.IMPORT_PROPERTIES_BUTTON, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFPropertiesPage.IMPORT_PROPERTIES_BUTTON);
	}
	
	/**
	 * input value to textfield
	 */
	public void inputTextfieldByName(String textfieldName, String value){
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_TEXTFIELD_BY_NAME, textfieldName, timeWait);
		type(driver, Interfaces.FNFPropertiesPage.DYNAMIC_TEXTFIELD_BY_NAME, textfieldName, value);
	}
	
	/**
	 * input value to textfield
	 */
	public String getTextfieldValueByName(String textfieldName){
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_TEXTFIELD_BY_NAME, textfieldName, timeWait);
		return getAttributeValue(driver, Interfaces.FNFPropertiesPage.DYNAMIC_TEXTFIELD_BY_NAME, textfieldName, "value");
	}
	
	/**
	 * input value to textarea
	 */
	public void inputTextareaByName(String textareaName, String value){
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_TEXTAREA_BY_NAME, textareaName, timeWait);
		type(driver, Interfaces.FNFPropertiesPage.DYNAMIC_TEXTAREA_BY_NAME, textareaName, value);
	}
	
	/**
	 * select item from dropdown
	 */
	public void selectItemFromDropdown(String dropdownName, String value){
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_DROPDOWN_BY_NAME, dropdownName, timeWait);
		selectItemCombobox(driver, Interfaces.FNFPropertiesPage.DYNAMIC_DROPDOWN_BY_NAME, dropdownName, value);
	}
	
	/**
	 * input value to textfield
	 */
	public boolean isTextfieldByNameDisplayed(String textfieldName){
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_TEXTFIELD_BY_NAME, textfieldName, 10);
		return isControlDisplayed(driver, Interfaces.FNFPropertiesPage.DYNAMIC_TEXTFIELD_BY_NAME, textfieldName);
	}
	
	/**
	 * check textfield displays correctly
	 */
	public boolean isTextfieldDisplayedCorrectly(String textfieldName, String value){
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_TEXTFIELD_BY_NAME, textfieldName, timeWait);
		return getAttributeValue(driver, Interfaces.FNFPropertiesPage.DYNAMIC_TEXTFIELD_BY_NAME, textfieldName, "value").contains(value);
	}
	
	/**
	 * check textarea displays correctly
	 */
	public boolean isTextareaDisplayedCorrectly(String textareaName, String value){
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_TEXTAREA_BY_NAME, textareaName, timeWait);
		return getText(driver, Interfaces.FNFPropertiesPage.DYNAMIC_TEXTAREA_BY_NAME, textareaName).contains(value);
	}
	
	/**
	 * check textfield is read only
	 */
	public boolean isTextfieldReadOnly(String textfieldName){
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_LABEL_BY_NAME, textfieldName, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFPropertiesPage.DYNAMIC_LABEL_BY_NAME, textfieldName) && !isControlDisplayed(driver, Interfaces.FNFPropertiesPage.DYNAMIC_TEXTFIELD_BY_NAME, textfieldName);
	}
	
	/**
	 * check textfield is read only
	 */
	public boolean isReadOnlyTextfieldDisplayCorrectly(String textfieldName, String value){
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_READONLY_TEXTFIELD, textfieldName, timeWait);
		if(value.equals("")) return getText(driver, Interfaces.FNFPropertiesPage.DYNAMIC_READONLY_TEXTFIELD, textfieldName).trim().equals("");
		String result = getText(driver, Interfaces.FNFPropertiesPage.DYNAMIC_READONLY_TEXTFIELD, textfieldName);
		return result.contains(value);
	}
	
	/**
	 * check textfield is read only
	 */
	public String getTextReadOnlyTextfield(String textfieldName){
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_READONLY_TEXTFIELD, textfieldName, timeWait);
		return getText(driver, Interfaces.FNFPropertiesPage.DYNAMIC_READONLY_TEXTFIELD, textfieldName);
	}
	
	/**
	 * check dropdown displays correctly
	 */
	public boolean isDropdownDisplayedCorrectly(String textfieldName, String value){
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_DROPDOWN_BY_NAME, textfieldName, timeWait);
		String realValue = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.DYNAMIC_DROPDOWN_BY_NAME, textfieldName);
		return realValue.contains(value);
	}
	
	/**
	 * input addtional value to textfield for lender
	 */
	public void inputOptinalInformationsForLender(String valuationType, String includeRehabCosts, String price, String commonText, String date){
		inputTextfieldByName("Approved Completed Rehab Costs", price);
		inputTextfieldByName("Estimated Completed Rehab Costs", price);
		inputTextfieldByName("Borrower Estimated Market Value", price);
		inputTextfieldByName("BPO/Appraisal Firm", commonText);
		inputTextfieldByName("BPO/Appraisal Order Date", date);
		inputTextfieldByName("BPO/Appraisal Fee $", price);
		inputTextareaByName("Lender Comments", commonText);
		selectItemFromDropdown("Valuation Type", valuationType);
		selectItemFromDropdown("Include Rehab Costs (Y/N)", includeRehabCosts);
	}
	
	/**
	 * is addtional value for lender saved
	 */
	public boolean isOptinalInformationsForLenderSaved(String valuationType, String includeRehabCosts, String price, String commonText, String date){
		return isTextfieldDisplayedCorrectly("Approved Completed Rehab Costs", price)
		&& isTextfieldDisplayedCorrectly("Estimated Completed Rehab Costs", price)
		&& isTextfieldDisplayedCorrectly("Borrower Estimated Market Value", price)
		&& isTextfieldDisplayedCorrectly("BPO/Appraisal Firm", commonText)
		&& isTextfieldDisplayedCorrectly("BPO/Appraisal Order Date", date)
		&& isTextfieldDisplayedCorrectly("BPO/Appraisal Fee $", price)
		&& isTextareaDisplayedCorrectly("Lender Comments", commonText)
		&& isDropdownDisplayedCorrectly("Valuation Type", valuationType)
		&& isDropdownDisplayedCorrectly("Include Rehab Costs (Y/N)", includeRehabCosts);
	}
	
	/**
	 * input addtional value to textfield for borrower
	 */
	public void inputOptinalInformationsForBorrower(String includeRehabCosts, String price, String commonText){
		inputTextfieldByName("Estimated Completed Rehab Costs", price);
		inputTextfieldByName("Borrower Estimated Market Value", price);
		inputTextareaByName("Borrower Comments", commonText);
		selectItemFromDropdown("Include Rehab Costs (Y/N)", includeRehabCosts);
	}
	
	/**
	 * is addtional value for borrower saved
	 */
	public boolean isOptinalInformationsForBorrowerSaved(String includeRehabCosts, String price, String commonText){
		return isTextfieldDisplayedCorrectly("Estimated Completed Rehab Costs", price)
		&& isTextfieldDisplayedCorrectly("Borrower Estimated Market Value", price)
		&& isTextareaDisplayedCorrectly("Borrower Comments", commonText)
		&& isDropdownDisplayedCorrectly("Include Rehab Costs (Y/N)", includeRehabCosts);
	}
	
	/**
	 * check Document Loaded To Document Type
	 */
	public boolean isDocumentFolderNameDisplayedCorrectly(String documentType, String numberOfDocumentsLoaded) {
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENTS_FOLDER_NAME, documentType, numberOfDocumentsLoaded,  timeWait);
		return isControlDisplayed(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENTS_FOLDER_NAME, documentType, numberOfDocumentsLoaded);
	}
	
	/**
	 * Calculate Hold time
	 * @return days
	 */
	public String calculateHoldTime(String fundingDate){
		String currentDate = getCurrentDate(driver, "mm/dd/yyyy");
		if(fundingDate.equals("")) return "";
		System.out.println(minusTwoDates(currentDate, fundingDate, "Date", true));
		return minusTwoDates(currentDate, fundingDate, "Date", true);
	}
	
	/**
	 * Calculate Turn time
	 * @return days
	 */
	public String calculateTurnTime(String fundingDate){
		String payoffRequestDate = getTextfieldValueByName("Payoff Received Date");
		if(payoffRequestDate.equals("") || fundingDate.equals("")) return "";
		return minusTwoDates(payoffRequestDate, fundingDate, "Date", true);
	}
	
	/**
	 * Input Funding date
	 * @param date
	 */
	public void inputFundingDate(String date){
		waitForControl(driver, Interfaces.FNFPropertiesPage.FUNDIND_DATE_TEXTFIELD, timeWait);
		type(driver, Interfaces.FNFPropertiesPage.FUNDIND_DATE_TEXTFIELD, date);
	}
	
	public Boolean isApprovedAdvanceAmountCalculatedCorrectly(String maxLTC, String maxLTV){
		if(getTextfieldValueByName("BPO/Appraisal Value").equals(""))
			return isTextfieldDisplayedCorrectly("Approved Advance Amount","");
		else {
			if(!getTextfieldValueByName("Override Advance Amount").equals("")){
			return isTextfieldDisplayedCorrectly("Approved Advance Amount",getTextfieldValueByName("Override Advance Amount"));
			}
			else{
				String totalCostBasic = getTextReadOnlyTextfield("Total Costs Basis");
				float case1 = Float.parseFloat(maxLTC) * Float.parseFloat(totalCostBasic.replace("$", "").replace(",", "."));
				float case2 = Float.parseFloat(maxLTV) * Float.parseFloat(getTextfieldValueByName("BPO/Appraisal Value").replace("$", "").replace(",", "."));
				if(case1 >= case2) return isTextfieldDisplayedCorrectly("Approved Advance Amount",Float.toString(case2));
				else return isTextfieldDisplayedCorrectly("Approved Advance Amount",Float.toString(case1));
			}
		}
		
	}
	
	private WebDriver driver;
	private String ipClient; 
	private final Stack<String> openWindowHandles = new Stack<String>();
}