package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import PreciseRes.Interfaces;

public class ApplicantsPage extends AbstractPage {
	public ApplicantsPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
	}

	/**
	 * check User Display On Contact Detail Table
	 */
	public boolean isUserDisplayOnContactDetailTable(String userName) {
		waitForControl(driver,	Interfaces.ApplicantsPage.DYNAMIC_CONTACT_IN_TABLE, userName,timeWait);
		return isControlDisplayed(driver,Interfaces.ApplicantsPage.DYNAMIC_CONTACT_IN_TABLE, userName);
	}

	/**
	 * open User name detail page
	 */
	public void openUserNameDetailPage(String userName) {
		waitForControl(driver,Interfaces.ApplicantsPage.DYNAMIC_CONTACT_IN_TABLE,userName, timeWait);
		click(driver, Interfaces.ApplicantsPage.DYNAMIC_CONTACT_IN_TABLE,userName);
		sleep();
	}
	
	/**
	 * check Applicant Detail Page Saved
	 */
	public boolean isApplicantDetailPageSaved() {
		waitForControl(driver,	Interfaces.ApplicantsPage.APPLICANT_DETAIL_SAVED_MESSAGE,	timeWait);
		return isControlDisplayed(driver,Interfaces.ApplicantsPage.APPLICANT_DETAIL_SAVED_MESSAGE);
	}
	
	/**
	 * check Resend Welcome Email message
	 */
	public boolean isResendWelcomeEmailMessage(String email) {
		waitForControl(driver,	Interfaces.ApplicantsPage.RESEND_WELCOME_EMAIL_MESSAGE, email,  timeWait);
		return isControlDisplayed(driver,Interfaces.ApplicantsPage.RESEND_WELCOME_EMAIL_MESSAGE, email);
	}

	/**
	 * click Save Applicant Detail
	 */
	public void clickSaveApplicantDetail() {
		waitForControl(driver,	Interfaces.ApplicantsPage.SAVE_APPLICANT_DETAIL_BUTTON,timeWait);
		click(driver, Interfaces.ApplicantsPage.SAVE_APPLICANT_DETAIL_BUTTON);
		sleep();
	}

	/**
	 * search Applicant By Name
	 */
	public void searchApplicantByName(String applicantName) {
		waitForControl(driver, Interfaces.ApplicantsPage.USER_LEGAL_NAME_TEXTBOX,timeWait);
		type(driver, Interfaces.ApplicantsPage.USER_LEGAL_NAME_TEXTBOX, applicantName);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(2);
	}

	/**
	 * open Applicants Detail Page
	 */
	public void openApplicantsDetailPage(String applicantName) {
		waitForControl(driver,Interfaces.ApplicantsPage.DYNAMIC_APPLICANT_ITEM,applicantName, timeWait);
		click(driver, Interfaces.ApplicantsPage.DYNAMIC_APPLICANT_ITEM,applicantName);
		sleep();
	}

	/**
	 * click New Contact Detail
	 */
	public void clickNewContactDetail() {
		waitForControl(driver,Interfaces.ApplicantsPage.NEW_CONTACT_DETAIL_BUTTON, timeWait);
		click(driver, Interfaces.ApplicantsPage.NEW_CONTACT_DETAIL_BUTTON);
		sleep();
	}

	/**
	 * input Applicant Contact Info
	 */
	public void inputApplicantContactInfo(String firstName, String lastName,String emailContact) {
		waitForControl(driver,Interfaces.ApplicantsPage.BORROWER_FIRST_NAME_TEXTBOX,	timeWait);
		type(driver, Interfaces.ApplicantsPage.BORROWER_FIRST_NAME_TEXTBOX,firstName);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.BORROWER_LAST_NAME_TEXTBOX,lastName);
		sleep(1);
		type(driver,Interfaces.ApplicantsPage.BORROWER_EMAIL_CONTACT_TEXTBOX,emailContact);
		sleep(1);
	}

	/**
	 * click Save Applicant Contact
	 */
	public void clickSaveApplicantContact() {
		waitForControl(driver,Interfaces.ApplicantsPage.SAVE_APPLICANT_CONTACT_BUTTON,timeWait);
		click(driver, Interfaces.ApplicantsPage.SAVE_APPLICANT_CONTACT_BUTTON);
		sleep();
	}

	/**
	 * check Message Email Sent Display
	 */
	public boolean isMessageEmailSentDisplay(String email) {
		waitForControl(driver,	Interfaces.ApplicantsPage.DYNAMIC_EMAIL_SENT_MESSAGE, email,	timeWait);
		return isControlDisplayed(driver,Interfaces.ApplicantsPage.DYNAMIC_EMAIL_SENT_MESSAGE, email);
	}

	/**
	 * switch to applicant contact frame
	 */
	public WebDriver switchToApplicantContactFrame(WebDriver driver) {
		waitForControl(driver,	Interfaces.ApplicantsPage.APPLICANT_CONTACT_FRAME, timeWait);
		sleep();
		driver = driver	.switchTo().frame(driver.findElement(By.xpath(Interfaces.ApplicantsPage.APPLICANT_CONTACT_FRAME)));
		return driver;
	}

	/**
	 * check Applicant Name Detail Correctly
	 */
	public boolean isApplicantNameDetailCorrectly(String applicantName) {
		waitForControl(driver,Interfaces.ApplicantsPage.APPLICANT_DETAIL_NAME_TEXTBOX,timeWait);
		String realName = getAttributeValue(driver,	Interfaces.ApplicantsPage.APPLICANT_DETAIL_NAME_TEXTBOX,	"value");
		return realName.contains(applicantName);
	}

	/**
	 * check Account Number Detail Correctly
	 */
	public boolean isAccountNumberDetailCorrectly(String accountNumber) {
		waitForControl(
				driver,
				Interfaces.ApplicantsPage.APPLICANT_DETAIL_ACCOUNT_NUMBER_TEXTBOX,
				timeWait);
		String realName = getAttributeValue(
				driver,
				Interfaces.ApplicantsPage.APPLICANT_DETAIL_ACCOUNT_NUMBER_TEXTBOX,
				"value");
		return realName.contains(accountNumber);
	}

	/**
	 * open Contact Detail Page
	 */
	public void openContactDetailPage(String userName) {
		waitForControl(driver,
				Interfaces.ApplicantsPage.DYNAMIC_CONTACT_DETAIL, userName,
				timeWait);
		click(driver, Interfaces.ApplicantsPage.DYNAMIC_CONTACT_DETAIL,
				userName);
		sleep();
	}

	/**
	 * open Login Record Applicant Contact Detail
	 */
	public void openLoginRecordApplicantContactDetail() {
		waitForControl(driver,
				Interfaces.ApplicantsPage.LOGIN_RECORD_APPLIANT_DETAIL,
				timeWait);
		click(driver, Interfaces.ApplicantsPage.LOGIN_RECORD_APPLIANT_DETAIL);
		sleep();
	}

	/**
	 * check Last Name User Detail Display
	 */
	public boolean isLastNameUserDetailDisplay(String userName) {
		waitForControl(driver,
				Interfaces.ApplicantsPage.LAST_NAME_USER_DETAIL_TEXTBOX,
				timeWait);
		String realName = getAttributeValue(driver,
				Interfaces.ApplicantsPage.LAST_NAME_USER_DETAIL_TEXTBOX,
				"value");
		return realName.contains(userName);
	}

	/**
	 * check Email User Detail Display
	 */
	public boolean isEmailUserDetailDisplay(String email) {
		waitForControl(driver,
				Interfaces.ApplicantsPage.EMAIL_USER_DETAIL_TEXTBOX, timeWait);
		String realEmail = getAttributeValue(driver,
				Interfaces.ApplicantsPage.EMAIL_USER_DETAIL_TEXTBOX, "value");
		return realEmail.contains(email);
	}

	/**
	 * get Login Id In User Detail
	 */
	public String getLoginIdInUserDetail() {
		waitForControl(driver,
				Interfaces.ApplicantsPage.LOGIN_ID_USER_DETAIL_TEXTBOX,
				timeWait);
		String loginId = getAttributeValue(driver,
				Interfaces.ApplicantsPage.LOGIN_ID_USER_DETAIL_TEXTBOX, "value");
		return loginId;
	}

	/**
	 * click New Applicant Button
	 */
	public void clickNewApplicantButton() {
		waitForControl(driver, Interfaces.ApplicantsPage.NEW_APPLICANT_BUTTON,timeWait);
		executeJavaScript(driver, "document.getElementById('New').click();");
		executeJavaScript(driver, "document.getElementById('New2').click();");
		sleep(1);
	}

	/**
	 * input Applicant Info
	 */
	public void inputApplicantInfo(String accountNumber, String applicantName) {
		waitForControl(driver,	Interfaces.ApplicantsPage.NEW_APPLICANT_ACCOUNT_NUMBER_TEXTBOX,timeWait);
		type(driver,Interfaces.ApplicantsPage.NEW_APPLICANT_ACCOUNT_NUMBER_TEXTBOX,accountNumber);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.NEW_APPLICANT_NAME_TEXTBOX,applicantName);
		sleep(1);
	}

	/**
	 * click Next Button
	 */
	public void clickNextButton() {
		waitForControl(driver, Interfaces.ApplicantsPage.NEXT_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		sleep();
	}

	/**
	 * click Done Button
	 */
	public void clickDoneButton() {
		waitForControl(driver, Interfaces.ApplicantsPage.DONE_BUTTON, timeWait);
		click(driver, Interfaces.ApplicantsPage.DONE_BUTTON);
		sleep();
	}

	/**
	 * check Last Name Applicant Contact Detail Correctly
	 */
	public boolean isLastNameApplicantContactDetailCorrectly(String lastName) {
		waitForControl(driver,
				Interfaces.ApplicantsPage.APPLICANT_CONTACT_DETAIL_LASTNAME,
				timeWait);
		String realLastName = getAttributeValue(driver,
				Interfaces.ApplicantsPage.APPLICANT_CONTACT_DETAIL_LASTNAME,
				"value");
		return realLastName.contains(lastName);
	}

	/**
	 * check First Name Applicant Contact Detail Correctly
	 */
	public boolean isFirstNameApplicantContactDetailCorrectly(String firstName) {
		waitForControl(driver,
				Interfaces.ApplicantsPage.APPLICANT_CONTACT_DETAIL_FIRSTNAME,
				timeWait);
		String realFirstName = getAttributeValue(driver,
				Interfaces.ApplicantsPage.APPLICANT_CONTACT_DETAIL_FIRSTNAME,
				"value");
		return realFirstName.contains(firstName);
	}

	/**
	 * check Email Applicant Contact Detail Correctly
	 */
	public boolean isEmailApplicantContactDetailCorrectly(String email) {
		waitForControl(
				driver,
				Interfaces.ApplicantsPage.DYNAMIC_APPLICANT_CONTACT_DETAIL_EMAIL,
				email, timeWait);
		return isControlDisplayed(
				driver,
				Interfaces.ApplicantsPage.DYNAMIC_APPLICANT_CONTACT_DETAIL_EMAIL,
				email);
	}

	/**
	 * check Login ID display on user Detail page
	 */
	public boolean isChooseLoginListDisplay() {
		waitForControl(driver, Interfaces.ApplicantsPage.CHOOSE_LOGIN_COMBOBOX,
				timeWait);
		click(driver, Interfaces.ApplicantsPage.CHOOSE_LOGIN_COMBOBOX);
		sleep();
		return isControlDisplayed(driver,
				Interfaces.ApplicantsPage.CHOOSE_LOGIN_OPTION);
	}

	/**
	 * input Applicant Contact Info
	 */
	public void inputApplicantContactInfo(String firstName, String lastName,
			String emailContact, String loginName) {
		waitForControl(driver,
				Interfaces.ApplicantsPage.FNF_BORROWER_FIRST_NAME_TEXTBOX,
				timeWait);
		type(driver, Interfaces.ApplicantsPage.FNF_BORROWER_FIRST_NAME_TEXTBOX,
				firstName);
		sleep(1);
		type(driver, Interfaces.ApplicantsPage.FNF_BORROWER_LAST_NAME_TEXTBOX,
				lastName);
		sleep(1);
		type(driver,
				Interfaces.ApplicantsPage.FNF_BORROWER_EMAIL_CONTACT_TEXTBOX,
				emailContact);
		sleep(1);
		selectItemCombobox(driver,
				Interfaces.ApplicantsPage.CHOOSE_LOGIN_COMBOBOX, loginName);
		sleep(1);
	}
	
	/**
	 * check the name of Applicant Contact Detail Display
	 */
	public boolean isApplicantContactDetailDisplay(String firstName,String lastName) {
		String applicantContactDetailName = firstName + " " + lastName;
		waitForControl(driver,Interfaces.ApplicantsPage.DYNAMIC_APPLICANT_CONTACT_DETAIL_LINK,
				applicantContactDetailName, timeWait);
		return isControlDisplayed(driver,Interfaces.ApplicantsPage.DYNAMIC_APPLICANT_CONTACT_DETAIL_LINK,
				applicantContactDetailName);
	}

	/**
	 * click on link of Applicant Contact Detail name
	 */
	public void clickApplicantContactDetailLink(String applicationName) {
		waitForControl(
				driver,
				Interfaces.ApplicantsPage.DYNAMIC_APPLICANT_CONTACT_DETAIL_LINK,
				applicationName, timeWait);
		click(driver,
				Interfaces.ApplicantsPage.DYNAMIC_APPLICANT_CONTACT_DETAIL_LINK,
				applicationName);
	}

	/**
	 * check the name of Applicant Contact Detail Display
	 */
	public boolean isExistingLoginDisplay(String loginName) {
		waitForControl(driver,
				Interfaces.ApplicantsPage.DYNAMIC_LOGIN_SELECTED_ITEM,
				loginName, timeWait);
		return isControlDisplayed(driver,
				Interfaces.ApplicantsPage.DYNAMIC_LOGIN_SELECTED_ITEM,
				loginName);
	}

	/**
	 * click on User Detail icon of existing login to open user Details page
	 */
	public void openUserDetailsOfExistingLogin() {
		waitForControl(driver, Interfaces.ApplicantsPage.EXISTING_LOGIN_ICON,
				timeWait);
		click(driver, Interfaces.ApplicantsPage.EXISTING_LOGIN_ICON);
	}

	/**
	 * check Login ID display on user Detail page
	 */
	public boolean isNameAndIdOfExistingUserDisplay(String firstNameLogin,
			String laseNameLogin) {

		boolean bool = false;
		boolean bool1 = false;
		boolean bool2 = false;

		waitForControl(driver,
				Interfaces.ApplicantsPage.DYNAMIC_LAST_NAME_TEXTBOX,
				firstNameLogin, timeWait);
		bool1 = isControlDisplayed(driver,
				Interfaces.ApplicantsPage.DYNAMIC_LAST_NAME_TEXTBOX,
				firstNameLogin);

		waitForControl(driver,
				Interfaces.ApplicantsPage.DYNAMIC_LOGIN_ID_TEXTBOX,
				laseNameLogin, timeWait);
		bool2 = isControlDisplayed(driver,
				Interfaces.ApplicantsPage.DYNAMIC_LOGIN_ID_TEXTBOX,
				laseNameLogin);

		if (bool1 == true && bool2 == true) {
			bool = true;
		}
		return bool;
	}

	/**
	 * check Applicant Display On Search Page
	 */
	public boolean isApplicantDisplayOnSearchPage(String applicantName) {
		waitForControl(driver,	Interfaces.ApplicantsPage.DYNAMIC_APPLICANT_ITEM, applicantName, timeWait);
		return isControlDisplayed(driver,Interfaces.ApplicantsPage.DYNAMIC_APPLICANT_ITEM, applicantName);
	}

	/**
	 * click New Applicant Link
	 */
	public void clickNewApplicantLink() {
		waitForControl(driver, Interfaces.ApplicantsPage.NEW_APPLICANT_LINK,
				timeWait);
		click(driver, Interfaces.ApplicantsPage.NEW_APPLICANT_LINK);
		sleep();
	}
	
	/**
	 * check Users page display
	 * @param N/A
	 */
	public boolean isUsersPageDisplay() {
		waitForControl(driver, Interfaces.ApplicantsPage.USERS_PAGE_SECTION, timeWait);
		return isControlDisplayed(driver, Interfaces.ApplicantsPage.USERS_PAGE_SECTION);
	}
	
	/**
	 * click Re-Send Welcome Email button
	 */
	public void clickResendWelcomeEmailButton() {
		waitForControl(driver, Interfaces.ApplicantsPage.RESEND_WELCOME_EMAIL_BUTTON,timeWait);
		executeJavaScript(driver, "document.getElementById('ReSendWelcomeEmail').click();");
		sleep(2);
	}
	
	private WebDriver driver;
}