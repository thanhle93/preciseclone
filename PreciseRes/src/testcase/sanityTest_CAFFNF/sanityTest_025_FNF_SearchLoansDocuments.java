package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFDocumentPage;
import page.FNFLoansPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_025_FNF_SearchLoansDocuments extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		loanName = "Edit Loan Auto " + getUniqueNumber();
		validDate = "2/12/2014";
		documentFolder = "Entity Documents";
		documentFileName = "datatest1.pdf";
		text = "comments";
	}


	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search borrower by Company Name")
	public void SearchLoansDocuments_01_Lender_SearchByLoanName() {

		log.info("SearchLoansDocuments_01 - Step 01. Logout and login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("SearchLoansDocuments_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("SearchLoansDocuments_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("SearchLoansDocuments_01 - Step 04: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("SearchLoansDocuments_01 - Step 05: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
	
		log.info("SearchLoansDocuments_01 - Step 06: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("SearchLoansDocuments_01 - Step 07: Input loan name");
		loanPage.inputRequiredData(loanName, validDate);
		
		log.info("SearchLoansDocuments_01 - Step 08: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("SearchLoansDocuments_01 - Step 09. Open document folder");
		documentsPage = loanPage.openDocumentsFolder(documentFolder);
		
		log.info("SearchLoansDocuments_01 - Step 10. Upload document file name 01");
		documentsPage.uploadDocumentFileForFolderByPlaceHolder(documentFileName);
		
		log.info("SearchLoansDocuments_01 - Step 11. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("SearchLoansDocuments_01 - Step 12. Input comment");
		documentsPage.inputComments(documentFileName, text);
		
		log.info("SearchLoansDocuments_01 - Step 13. Click Save button");
		documentsPage.clickSaveButton();
	
		log.info("SearchLoansDocuments_01 - Step 14: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openLoanDocumentsTab();
		
		log.info("SearchLoansDocuments_01 - Step 15: Input 'Loan Name' and click on 'Search' button");
		documentsPage.clickOnSearchLoansButton(loanName);
		
		log.info("SearchLoansDocuments_01 - Step 16: Get information for checking in CSV file");
		documentFolder = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_DisplayName");
		documentFileName = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeHTML datacell_DocumentFile");
		lastUpoadDate = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeDateTime datacell_DocumentChangedDate");
		lastUploadBy = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_DocumentChangedByName");
		lastComments = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeLongVarchar datacell_ApproveReason");
		
		log.info("VP: Loans Documents information displayed correctly");
		verifyTrue(documentsPage.isAllLoansDocumentsRecordValuesDisplayedCorrectly(loanName, documentFolder, documentFileName, lastUpoadDate, lastUploadBy, lastComments));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search documents by Upload date")
	public void SearchLoansDocuments_02_Lender_SearchByUploadDate() {

		log.info("SearchLoansDocuments_02 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openLoanDocumentsTab();
	
		log.info("SearchLoansDocuments_02 - Step 02: Input 'Upload date'");
		documentsPage.inputUploadDate(lastUpoadDate);
		
		log.info("SearchLoansDocuments_02 - Step 03: Input 'Loan Name' and click on 'Search' button");
		documentsPage.clickOnSearchLoansButton(loanName);
	
		log.info("VP: Loans Documents information displayed correctly");
		verifyTrue(documentsPage.isAllLoansDocumentsRecordValuesDisplayedCorrectly(loanName, documentFolder, documentFileName, lastUpoadDate, lastUploadBy, lastComments));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search borrower by Upload by")
	public void SearchLoansDocuments_03_Lender_SearchByUploadBy() {

		log.info("SearchLoansDocuments_03 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openLoanDocumentsTab();
	
		log.info("SearchLoansDocuments_03 - Step 02: Input 'Upload by'");
		documentsPage.inputUploadBy(lastUploadBy);
		
		log.info("SearchLoansDocuments_03 - Step 03: Input 'Loan Name' and click on 'Search' button");
		documentsPage.clickOnSearchLoansButton(loanName);
		
		log.info("VP: Loans Documents information displayed correctly");
		verifyTrue(documentsPage.isAllLoansDocumentsRecordValuesDisplayedCorrectly(loanName, documentFolder, documentFileName, lastUpoadDate, lastUploadBy, lastComments));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search borrower by Active status")
	public void SearchLoansDocuments_04_Lender_SearchByStatus() {

		log.info("SearchLoansDocuments_04 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openLoanDocumentsTab();
	
		log.info("SearchLoansDocuments_04 - Step 02: Select 'Status' ");
		documentsPage.selectActiveRadioButton("Yes");
	
		log.info("SearchLoansDocuments_04 - Step 03: Input 'Loan Name' and click on 'Search' button");
		documentsPage.clickOnSearchLoansButton(loanName);
		
		log.info("VP: Loans Documents information displayed correctly");
		verifyTrue(documentsPage.isAllLoansDocumentsRecordValuesDisplayedCorrectly(loanName, documentFolder, documentFileName, lastUpoadDate, lastUploadBy, lastComments));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search borrower by Company Name")
	public void SearchLoansDocuments_05_Borrower_SearchByLoanName() {

		log.info("SearchLoansDocuments_05 - Step 01. Login with Borrower");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);
	
		log.info("SearchLoansDocuments_05 - Step 02: Open Documents Page and open Loan Documents tab");
		documentsPage = applicantsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openLoanDocumentsTab();
		
		log.info("SearchLoansDocuments_05 - Step 03: Input 'Loan Name' and click on 'Search' button");
		documentsPage.clickOnSearchLoansButton(loanName);
		
		log.info("VP: Loans Documents information displayed correctly");
		verifyTrue(documentsPage.isAllLoansDocumentsRecordValuesDisplayedCorrectly(loanName, documentFolder, documentFileName, lastUpoadDate, lastUploadBy, lastComments));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search documents by Upload date")
	public void SearchLoansDocuments_06_Borrower_SearchByUploadDate() {

		log.info("SearchLoansDocuments_06 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openLoanDocumentsTab();
	
		log.info("SearchLoansDocuments_06 - Step 02: Input 'Upload date'");
		documentsPage.inputUploadDate(lastUpoadDate);
		
		log.info("SearchLoansDocuments_06 - Step 03: Input 'Loan Name' and click on 'Search' button");
		documentsPage.clickOnSearchLoansButton(loanName);
	
		log.info("VP: Loans Documents information displayed correctly");
		verifyTrue(documentsPage.isAllLoansDocumentsRecordValuesDisplayedCorrectly(loanName, documentFolder, documentFileName, lastUpoadDate, lastUploadBy, lastComments));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search borrower by Upload by")
	public void SearchLoansDocuments_07_Borrower_SearchByUploadBy() {

		log.info("SearchLoansDocuments_07 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openLoanDocumentsTab();
	
		log.info("SearchLoansDocuments_07 - Step 02: Input 'Upload by'");
		documentsPage.inputUploadBy(lastUploadBy);
		
		log.info("SearchLoansDocuments_07 - Step 03: Input 'Loan Name' and click on 'Search' button");
		documentsPage.clickOnSearchLoansButton(loanName);
		
		log.info("VP: Loans Documents information displayed correctly");
		verifyTrue(documentsPage.isAllLoansDocumentsRecordValuesDisplayedCorrectly(loanName, documentFolder, documentFileName, lastUpoadDate, lastUploadBy, lastComments));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search borrower by Active status")
	public void SearchLoansDocuments_08_Borrower_SearchByStatus() {

		log.info("SearchLoansDocuments_08 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openLoanDocumentsTab();
	
		log.info("SearchLoansDocuments_08 - Step 02: Select 'Status' ");
		documentsPage.selectActiveRadioButton("Yes");
	
		log.info("SearchLoansDocuments_08 - Step 03: Input 'Loan Name' and click on 'Search' button");
		documentsPage.clickOnSearchLoansButton(loanName);
		
		log.info("VP: Loans Documents information displayed correctly");
		verifyTrue(documentsPage.isAllLoansDocumentsRecordValuesDisplayedCorrectly(loanName, documentFolder, documentFileName, lastUpoadDate, lastUploadBy, lastComments));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private FNFLoansPage loanPage;
	private FNFDocumentPage documentsPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername, FNFBorrowerPassword;
	private String companyName, accountNumber;
	private String validDate;
	private String documentFolder;
	private String documentFileName;
	private String lastUpoadDate, lastUploadBy, lastComments;
	private String loanName, text;
	}