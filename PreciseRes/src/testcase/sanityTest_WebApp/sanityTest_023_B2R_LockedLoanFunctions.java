package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.DocumentsPage;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.MailHomePage;
import page.MailLoginPage;
import page.PageFactory;
import page.PropertiesPage;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_023_B2R_LockedLoanFunctions extends	AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		usernameAudit = Constant.USERNAME_B2R_AUDIT;
		passwordAudit = Constant.PASSWORD_B2R_AUDIT;
		accountNumber = getUniqueNumber();
		loanName = "Audit-Locked-Loan-B2R " + accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower";
		borrowerName = "UdiTeamBorrower";
		guarantorName = "UdiTeamGuarantor";
		propertiesFileName = "B2RDataTape_multi-unit.xlsx";
		propertyGroup = "Oakhill";
		address1 = "37225 OAKHILL ST - suite 15";
		documentType1 = "P1 - Appraisal/BPO (with Market Rent Data)";
		documentType2 = "P2 - Lease Agreement";

		documentSectionSearch1 = "Property Management";
		documentType3 = "L31 - Management Company Questionnaire";
		documentType4 = "L32 - Management Agreement";
		documentType5 = "L33 - Management Company Certificate of Formation";
		documentType6 = "L34 - Management Company Certificate of Good Standing";
		
		documentSectionSearch2 = "B2R UW / Credit";
		documentType7 = "L41 - Credit Memo/Approval";
		documentType8 = "L42 - Final Rockport UW Model";
		
		documentSectionSearch3 = "Guarantor";
		documentType9 = "L17 - Personal Financial Statement";
		documentType10 = "L18 - PFS Certification";
		documentType11 = "L19 - Proof of Liquidity Statements";
		documentType12 = "L20 - Real Estate Resume";
		
		documentSectionSearch4 = "Borrower (SPE)";
		documentType13 = "L1 - Loan Application";
		documentType14 = "L3 - Background Search";

		documentFileName = "datatest.pdf";
		auditProfile = "AuditLoanB2R (Dont Touch)";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
		finalLLC = "100";
		postClosingStatus = "Post Closing";
		fundedStatusMessage = "Please fill in Final Borrower LLC field in order to proceed to Post Closing status";
		lockedLoanSuccess = "Modifications are not allowed at this time as loan is locked.  Please unlock loan if you wish to make changes.";
		mailPageUrl = "https://accounts.google.com";
		usernameEmail = "minhdam06@gmail.com";
		passwordEmail = "dam123!@#!@#";
		zipDownload = ".zip";
	}

	@Test(groups = { "regression" }, description = "Loan73 - Loan locked successfully")
	public void LockedLoan_01_LoanLockedSuccessfully() {

		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Precondition 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 03. Create new loan");
		loansPage.createNewLoanAudit(loanName, loanName, loanStatus, applicantName, borrowerName, guarantorName);
		
		log.info("VP: Applicant displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(applicantName));
		
		log.info("VP: Borrower displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(borrowerName));
		
		log.info("VP: Gurantor displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(guarantorName));

		log.info("VP: Loan Application Entities table have 3 items");
		verifyTrue(loansPage.isItemOfLoanApplicantEntitiesOnBasicDetailTab(3));
		
		log.info("Precondition 04. Open 'Properties' tab");
		loansPage.openPropertiesTab();

		log.info("Precondition 05. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("Precondition 06. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("Precondition 07. Add document 1 by clicking the place holder");
		propertiesPage.uploadDynamicDocumentFileByPlaceHolder(documentType1, documentFileName);
		
		log.info("Precondition 08. Click 'Save' button");
		propertiesPage.clickSaveButton();
		
		log.info("VP: Document file is loaded to document type 1");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, documentFileName));
		
		log.info("VP: Document Type 2 display on Property Detail");
		verifyTrue(propertiesPage.isDocumentTypeDisplayOnPropertyDetail(documentType2));
		
		log.info("Precondition 09. Open 'General Loan Documents' page");
		documentsPage = propertiesPage.openGeneralDocumentTab();
		
		log.info("Precondition 10. Click Document Type search");
		documentsPage.searchDocumentType(documentType3);
		
		log.info("Precondition 11. Open a General document detail");
		documentsPage.openGeneralDocumentDetail(documentType3);
		
		log.info("Precondition 12. Upload document file to document type");
		documentsPage.uploadDocumentFile(documentFileName);
		documentsPage.clickSaveButton();
		
		log.info("Precondition 13. Go to Document list");
		documentsPage.clickDocumentListButton();
		 
		log.info("Precondition 14. Search 'Section' by 'Property Management'");
		documentsPage.searchSectionAndUncheckDocumentType(documentSectionSearch1);

		log.info("VP: Document file is loaded to document type 3");
		verifyTrue(documentsPage.isDocumentUploadedToDocumentType(documentType3));
		
		log.info("VP: Document Type 4 display on General detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType4));
		
		log.info("VP: Document Type 5 display on General detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType5));
		
		log.info("VP: Document Type 6 display on General detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType6));
		
		log.info("Precondition 15. Search 'Section' by 'B2R UW / Credit'");
		documentsPage.searchSectionAndUncheckDocumentType(documentSectionSearch2);
		
		log.info("VP: Document Type 7 display on General detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType7));
		
		log.info("VP: Document Type 8 display on General detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType8));
		
		log.info("Precondition 16. Open 'Corporate Entity Documents' page");
		documentsPage.openCorporateEntityDocumentsTab();
		
		log.info("Precondition 17. Click Document Type search");
		documentsPage.searchDocumentType(documentType9);
		
		log.info("Precondition 18. Open a General document detail");
		documentsPage.openGeneralDocumentDetail(documentType9);
		
		log.info("Precondition 19. Upload document file to document type");
		documentsPage.uploadDocumentFile(documentFileName);
		documentsPage.clickSaveButton();
		
		log.info("Precondition 20. Go to Document list");
		documentsPage.clickDocumentListButton();
		 
		log.info("Precondition 21. Search 'Section' by 'Guarantor'");
		documentsPage.searchSectionAndUncheckDocumentType(documentSectionSearch3);
		
		log.info("VP: Document file is loaded to document type 9");
		verifyTrue(documentsPage.isDocumentUploadedToDocumentType(documentType9));
		
		log.info("VP: Document Type 10 display on Corporate detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType10));
		
		log.info("VP: Document Type 11 display on Corporate detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType11));
		
		log.info("VP: Document Type 12 display on Corporate detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType12));
		
		log.info("Precondition 22. Search 'Section' by 'Borrower (SPE)'");
		documentsPage.searchSectionAndUncheckDocumentType(documentSectionSearch4);
		
		log.info("VP: Document Type 13 display on Corporate detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType13));
		
		log.info("VP: Document Type 14 display on Corporate detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType14));
		
		log.info("LockedLoan_01 - Step 01. Open 'Basic Detail' tab");
		loansPage = documentsPage.openBasicDetailTab();
		
		log.info("LockedLoan_01 - Step 02. Select Loan Status is 'Post Closing'");
		loansPage.selectLoanStatusBasicDetail(postClosingStatus);
		
		log.info("LockedLoan_01 - Step 03. Click Save button");
		loansPage.clickSaveButton();
		
		log.info("VP: Error message display with info: 'Please fill in Final Borrower LLC field in order to proceed to Post Closing status'");
		verifyTrue(loansPage.isSaveFundedStatusErrorMessageDisplay(fundedStatusMessage));
		
		log.info("FundedStatus_01 - Step 06. Input data to 'Final Borrower LLC, Loan Application Status' fields");
		loansPage.inputFinalBorrowerAndFundedStatus(finalLLC, postClosingStatus);
		
		log.info("FundedStatus_01 - Step 07. Click Save button");
		loansPage.clickSaveButton();
		
		log.info("VP: Record Saved message displays");
		verifyTrue(loansPage.isRecordSavedMessageDisplays());
		
		log.info("VP: Locked loan saved successfully message display");
		verifyTrue(loansPage.isLockedLoanSavedSuccessfullyMessageDisplay(lockedLoanSuccess));
		
		log.info("VP: Verify that 'Loan Name, Final Borrower LLC, Loan Application Status' is set read-only successfully");
		loansPage.isCompanyNameAddressSiteCityReadOnly(loanName, finalLLC, postClosingStatus);
	}
	
	@Test(groups = { "regression" }, description = "Loan74 - Download all documents from Loan locked")
	public void LockedLoan_02_DownloadAllDocumentsFromLoanLocked() {
		
		log.info("LockedLoan_02 - Step 03. Open Loan tab");
		loansPage.openLoansPage(driver, ipClient);
		
		log.info("LockedLoan_02 - Step 04. Open Loan detail");
		loansPage.searchLoanByName(loanName);
		loansPage.openLoansDetailPage(loanName);
		preciseRESUrl = loansPage.getCurrentUrl(driver);

		log.info("LockedLoan_02 - Step 05. Click 'Download All Document' button");
		loansPage.clickDownloadAllDocumentsButton();

		log.info("LockedLoan_02 - Step 06. Open Email url");
		mailLoginPage = loansPage.openMailLink(driver, mailPageUrl);

		log.info("LockedLoan_02 - Step 07. Input Usename and Password");
		mailLoginPage.loginToGmail(usernameEmail, passwordEmail);

		log.info("LockedLoan_02 - Step 08. Click Submit button");
		mailHomePage = mailLoginPage.clickSubmitButton(driver, ipClient);

		log.info("LockedLoan_02 - Step 09. Click 'Google Apps' title");
		mailHomePage.clickGoogleAppsTitle();

		log.info("LockedLoan_02 - Step 10. Click 'Gmail' application");
		mailHomePage.clickGmailApplication();

		log.info("VP. Gmail home page is displayed");
		verifyTrue(mailHomePage.isGmailHomePageDisplay());

		log.info("LockedLoan_02 - Step 11. Click 'Inbox' title");
		mailHomePage.clickInboxTitle();

		log.info("LockedLoan_02 - Step 12. Click 'Subject first email'");
		mailHomePage.clickSubjectFirstEmail();

		log.info("VP. Link hyperlink download is displayed");
		verifyTrue(mailHomePage.isLinkHyperlinkDisplay());

		log.info("LockedLoan_02 - Step 13. Click 'Link' hyperlink");
		mailHomePage.deleteAllFileInFolder();
		mailHomePage.clickLinkHyperlink();

		log.info("LockedLoan_02 - Step 14. Wait for file downloaded complete");
		mailHomePage.waitForDownloadFileContainsNameCompleted(zipDownload);

		log.info("VP: File number in folder equal 1");
		countFile = mailHomePage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("LockedLoan_02 - Step 15. Delete the downloaded file");
		mailHomePage.deleteContainsFileName(zipDownload);

		log.info("LockedLoan_02 - Step 16. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("LockedLoan_02 - Step 17. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		while(mailHomePage.isLinkHyperlinkDisplay()==true){
			mailHomePage.clickDataTooltipMoreDropdown();
			mailHomePage.clickDeleteThisMessageTitle();
		}
	}
	
	@Test(groups = { "regression" }, description = "Loan75 - Create audit copy from lender loan basic details (check 'Copy empty placeholders')")
	public void LockedLoan_03_CheckCopyEmptyPlaceholder() {
		
		log.info("LockedLoan_03 - Step 01. Open Loan detail");
		loansPage.openLink(driver, preciseRESUrl);
		
		log.info("LockedLoan_03 - Step 02. Click to 'Create Audit Copy' button");
		loansPage.clickCreateAuditCopyButton();
		
		log.info("LockedLoan_03 - Step 03. Select your profile is 'AuditLoanB2R (Dont Touch)'");
		loansPage.selectProfileAudit(auditProfile);
		
		log.info("LockedLoan_03 - Step 04. Check 'Copy Empty Placeholder' checkbox and Click 'OK' button");
		loansPage.checkCopyEmptyPlaceholderCheckbox();
		loansPage.clickOkButton();
		
		log.info("VP: Audit duplicating message displays");
		verifyTrue(loansPage.isAuditDuplicatingMessageDisplays());
		
		log.info("LockedLoan_03 - Step 05. Login with Audit lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameAudit, passwordAudit, false);
		
		log.info("LockedLoan_03 - Step 06. Open Loan page");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		log.info("LockedLoan_03 - Step 07. Open Loan detail");
		loansPage.searchLoanByName(loanName);
		loansPage.openLoansDetailPage(loanName);
		
		log.info("VP: Applicant displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(applicantName));
		
		log.info("VP: Borrower displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(borrowerName));
		
		log.info("VP: Gurantor displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(guarantorName));
		
		log.info("VP: Loan Application Entities table have 3 items");
		verifyTrue(loansPage.isItemOfLoanApplicantEntitiesOnBasicDetailTab(3));
		
		log.info("LockedLoan_03 - Step 08. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("LockedLoan_03 - Step 09. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);
		
		log.info("VP: Document file is loaded to document type 1");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, documentFileName));
		
		log.info("VP: Document Type 2 display on Property Detail");
		verifyTrue(propertiesPage.isDocumentTypeDisplayOnPropertyDetail(documentType2));
		
		log.info("LockedLoan_03 - Step 10. Open 'General Loan Documents' page");
		documentsPage = propertiesPage.openGeneralDocumentTab();
		
		log.info("LockedLoan_03 - Step 11. Search 'Section' by 'Property Management'");
		documentsPage.searchSectionAndUncheckDocumentType(documentSectionSearch1);

		log.info("VP: Document file is loaded to document type 3");
		verifyTrue(documentsPage.isDocumentUploadedToDocumentType(documentType3));
		
		log.info("VP: Document Type 4 display on General detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType4));
		
		log.info("VP: Document Type 5 display on General detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType5));
		
		log.info("VP: Document Type 6 display on General detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType6));
		
		log.info("LockedLoan_03 - Step 12. Search 'Section' by 'B2R UW / Credit'");
		documentsPage.searchSectionAndUncheckDocumentType(documentSectionSearch2);
		
		log.info("VP: Document Type 7 display on General detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType7));
		
		log.info("VP: Document Type 8 not display on General detail");
		verifyFalse(documentsPage.isNoDocumentUploadedToDocumentType(documentType8));
		
		log.info("LockedLoan_03 - Step 13. Open 'Corporate Entity Documents' page");
		documentsPage.openCorporateEntityDocumentsTab();
		
		log.info("LockedLoan_03 - Step 14. Search 'Section' by 'Guarantor'");
		documentsPage.searchSectionAndUncheckDocumentType(documentSectionSearch3);
		
		log.info("VP: Document file is loaded to document type 9");
		verifyTrue(documentsPage.isDocumentUploadedToDocumentType(documentType9));
		
		log.info("VP: Document Type 10 display on Corporate detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType10));
		
		log.info("VP: Document Type 11 display on Corporate detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType11));
		
		log.info("VP: Document Type 12 display on Corporate detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType12));
		
		log.info("LockedLoan_03 - Step 15. Search 'Section' by 'Borrower (SPE)'");
		documentsPage.searchSectionAndUncheckDocumentType(documentSectionSearch4);
		
		log.info("VP: Document Type 13 display on Corporate detail");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType13));
		
		log.info("VP: Document Type 14 not display on Corporate detail");
		verifyFalse(documentsPage.isNoDocumentUploadedToDocumentType(documentType14));
		
		log.info("LockedLoan_03 - Step 16. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("LockedLoan_03 - Step 17. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameAudit, passwordAudit);
		
		log.info("LockedLoan_03 - Step 18. Select loans item");
		uploaderPage.clickChangeGroupedView();
		uploaderPage.selectDefaultLoanStatus(postClosingStatus);
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("LockedLoan_03 - Step 19. Select folder General Loan Documents");
		uploaderPage.selectSubFolderLevel2("General Loan Documents");
		
		log.info("VP: There is a folder in General Loan Documents folder with 'Property Management' name");
		verifyTrue(uploaderPage.isSubFolderLevel3Display(documentSectionSearch1));
		
		log.info("LockedLoan_03 - Step 20. Select sub-folder by 'Property Management'");
		uploaderPage.selectSubFolderLevel3(documentSectionSearch1);
		
		log.info("VP: Document file is loaded to document type 3");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(documentFileName, documentType3));
		
		log.info("VP: Document Type 4 display on General detail");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType4));
		
		log.info("VP: Document Type 5 display on General detail");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType5));
		
		log.info("VP: Document Type 6 display on General detail");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType6));
		
		log.info("LockedLoan_03 - Step 21. Select sub-folder by 'B2R UW/ Credit'");
		uploaderPage.selectSubFolderLevel3(documentSectionSearch2);
		
		log.info("VP: Document Type 7 display on General detail");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType7));
		
		log.info("VP: Document Type 8 not display on General detail");
		verifyFalse(uploaderPage.isDocumentDisplay(documentType8));
		
		log.info("LockedLoan_03 - Step 22. Select folder Corporate Entity Documents");
		uploaderPage.selectSubFolderLevel2("Corporate Entity Documents");
		
		log.info("VP: There is a folder in Corporate Entity Documents folder with 'Guarantor' name");
		verifyTrue(uploaderPage.isSubFolderLevel3Display(documentSectionSearch3));
		
		log.info("LockedLoan_03 - Step 23. Select sub-folder 'Guarantor'");
		uploaderPage.selectSubFolderLevel3(documentSectionSearch3);
		
		log.info("VP: Document file is loaded to document type 9");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(documentFileName, documentType9));
		
		log.info("VP: Document Type 10 display on Corporate detail");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType10));
		
		log.info("VP: Document Type 11 display on Corporate detail");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType11));
		
		log.info("VP: Document Type 12 display on Corporate detail");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType12));;
		
		log.info("LockedLoan_03 - Step 24. Select sub-folder 'Borrower (SPE)'");
		uploaderPage.selectSubFolderLevel3(documentSectionSearch4);
		
		log.info("VP: Document Type 13 display on Corporate detail");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType13));
		
		log.info("VP: Document Type 14 not display on Corporate detail");
		verifyFalse(uploaderPage.isDocumentDisplay(documentType14));
		
		log.info("LockedLoan_03 - Step 25. Open Property folder");
		uploaderPage.clickPropertiesFolder();
		
		log.info("LockedLoan_03 - Step 26. Expand property group");
		uploaderPage.expandPropertyGroup(propertyGroup);
		
		log.info("LockedLoan_03 - Step 27. Select '37225 OAKHILL ST - suite 15' property");
		uploaderPage.selectSubPropertyFolder(address1);
		
		log.info("VP: Document file is loaded to document type 1");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(documentType1, documentFileName));
		
		log.info("VP: Document Type 2 display on Property Detail");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType2));
	}
	
	@Test(groups = { "regression" }, description = "Loan76 - Create audit copy from lender loan basic details (Uncheck 'Copy empty placeholders')")
	public void LockedLoan_04_UncheckCopyEmptyPlaceholder() {
		log.info("Precondition 01. Open web app");
		uploaderLoginPage = uploaderPage.logoutUploader();
		loginPage = uploaderLoginPage.openPreciseResUrlByBorrower(driver, ipClient, getPreciseResUrl());
		
		log.info("Precondition 02. Login with Lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender,	false);
		
		log.info("Precondition 03. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);
				
		log.info("LockedLoan_04 - Step 01. Open loan detail");
		loansPage.searchLoanByName(loanName);
		loansPage.openLoansDetailPage(loanName);
		
		log.info("LockedLoan_04 - Step 02. Click to 'Create Audit Copy' button");
		loansPage.clickCreateAuditCopyButton();
		
		log.info("LockedLoan_04 - Step 03. Select your profile is 'AuditLoanB2R (Dont Touch)'");
		loansPage.selectProfileAudit(auditProfile);
		
		log.info("LockedLoan_04 - Step 04. Uncheck 'Copy Empty Placeholder' checkbox and Click 'OK' button");
		loansPage.unCheckCopyEmptyPlaceholderCheckbox();
		loansPage.clickOkButton();
		
		log.info("VP: Audit duplicating message displays");
		verifyTrue(loansPage.isAuditDuplicatingMessageDisplays());
		
		log.info("LockedLoan_04 - Step 05. Login with Audit lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameAudit, passwordAudit, false);
		
		log.info("LockedLoan_04 - Step 06. Open Loan page");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		log.info("LockedLoan_04 - Step 07. Open Loan Audit detail page");
		loansPage.searchLoanByName(loanName);
		loansPage.openLoansDetailPage(loanName);
		
		log.info("VP: Applicant displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(applicantName));
		
		log.info("VP: Borrower displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(borrowerName));
		
		log.info("VP: Gurantor displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(guarantorName));
		
		log.info("VP: Loan Application Entities table have 3 items");
		verifyTrue(loansPage.isItemOfLoanApplicantEntitiesOnBasicDetailTab(3));

		log.info("LockedLoan_04 - Step 08. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("LockedLoan_04 - Step 09. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);
		
		log.info("VP: Document file is loaded to document type 1");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, documentFileName));
		
		log.info("VP: Document Type 2 not display on Property Detail");
		verifyFalse(propertiesPage.isDocumentTypeDisplayOnPropertyDetail(documentType2));
		
		log.info("LockedLoan_04 - Step 10. Open 'General Loan Documents' page");
		documentsPage = propertiesPage.openGeneralDocumentTab();
		
		log.info("LockedLoan_04 - Step 11. Search 'Section' by 'Property Management'");
		documentsPage.searchSectionAndUncheckDocumentType(documentSectionSearch1);

		log.info("VP: Document file is loaded to document type 3");
		verifyTrue(documentsPage.isDocumentUploadedToDocumentType(documentType3));
		
		log.info("VP: Document Type 4 not display on General detail");
		verifyFalse(documentsPage.isNoDocumentUploadedToDocumentType(documentType4));
		
		log.info("LockedLoan_04 - Step 12. Search 'Section' by 'B2R UW / Credit'");
		documentsPage.searchSectionAndUncheckDocumentType("");
		
		log.info("VP: Document Type 7 not display on General detail");
		verifyFalse(documentsPage.isNoDocumentUploadedToDocumentType(documentType7));
		
		log.info("LockedLoan_04 - Step 13. Open 'Corporate Entity Documents' page");
		documentsPage.openCorporateEntityDocumentsTab();
		
		log.info("LockedLoan_04 - Step 14. Search 'Section' by 'Guarantor'");
		documentsPage.searchSectionAndUncheckDocumentType(documentSectionSearch3);
		
		log.info("VP: Document file is loaded to document type 9");
		verifyTrue(documentsPage.isDocumentUploadedToDocumentType(documentType9));
		
		log.info("VP: Document Type 10 not display on Corporate detail");
		verifyFalse(documentsPage.isNoDocumentUploadedToDocumentType(documentType10));
		
		log.info("LockedLoan_04 - Step 15. Search 'Section' by 'Borrower (SPE)'");
		documentsPage.searchSectionAndUncheckDocumentType("");
		
		log.info("VP: Document Type 13 not display on Corporate detail");
		verifyFalse(documentsPage.isNoDocumentUploadedToDocumentType(documentType13));
		
		log.info("Precondition 04. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("Precondition 05. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameAudit, passwordAudit);
		
		log.info("LockedLoan_04 - Step 16. Select loans item");
		uploaderPage.clickChangeGroupedView();
		uploaderPage.selectDefaultLoanStatus(postClosingStatus);
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("LockedLoan_04 - Step 17. Select folder General Loan Documents");
		uploaderPage.selectSubFolderLevel2("General Loan Documents");
		
		log.info("VP: There is a folder in General Loan Documents folder with 'Property Management' name");
		verifyTrue(uploaderPage.isSubFolderLevel3Display(documentSectionSearch1));
		
		log.info("LockedLoan_04 - Step 18. Select sub-folder by 'Property Management'");
		uploaderPage.selectSubFolderLevel3(documentSectionSearch1);
		
		log.info("VP: Document file is loaded to document type 3");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(documentFileName, documentType3));
		
		log.info("VP: Document Type 4 not display on General detail");
		verifyFalse(uploaderPage.isDocumentDisplay(documentType4));
		
		log.info("LockedLoan_04 - Step 19. Select folder Corporate Entity Documents");
		uploaderPage.selectSubFolderLevel2("Corporate Entity Documents");
		
		log.info("VP: There is a folder in Corporate Entity Documents folder with 'Guarantor' name");
		verifyTrue(uploaderPage.isSubFolderLevel3Display(documentSectionSearch3));
		
		log.info("LockedLoan_04 - Step 20. Select sub-folder 'Guarantor'");
		uploaderPage.selectSubFolderLevel3(documentSectionSearch3);
		
		log.info("VP: Document file is loaded to document type 9");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(documentFileName, documentType9));
		
		log.info("VP: Document Type 10 not display on Corporate detail");
		verifyFalse(uploaderPage.isDocumentDisplay(documentType10));
		
		log.info("LockedLoan_04 - Step 21. Open Property folder");
		uploaderPage.clickPropertiesFolder();
		
		log.info("LockedLoan_04 - Step 22. Expand property group");
		uploaderPage.expandPropertyGroup(propertyGroup);
		
		log.info("LockedLoan_04 - Step 23. Select '37225 OAKHILL ST - suite 15' property");
		uploaderPage.selectSubPropertyFolder(address1);
		
		log.info("VP: Document file is loaded to document type 1");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(documentType1, documentFileName));
		
		log.info("VP: Document Type 2 not display on Property Detail");
		verifyFalse(uploaderPage.isDocumentDisplay(documentType2));
	}
		
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private DocumentsPage documentsPage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private MailHomePage mailHomePage;
	private MailLoginPage mailLoginPage;
	private int countFile;
	private String usernameEmail, passwordEmail, uploaderPageUrl, mailPageUrl;
	private String usernameLender, passwordLender, usernameAudit, passwordAudit, loanStatus, zipDownload;
	private String loanName, accountNumber, applicantName, borrowerName, guarantorName, lockedLoanSuccess;
	private String propertiesFileName, address1, auditProfile, postClosingStatus, fundedStatusMessage, finalLLC, preciseRESUrl;
	private String documentType1, documentType2, documentType3, documentType4, documentType5, documentType6, documentType7, documentType8;
	private String documentType9, documentType10, documentType11, documentType12, documentType13, documentType14, propertyGroup;
	private String documentFileName, documentSectionSearch1, documentSectionSearch2, documentSectionSearch3, documentSectionSearch4;
}