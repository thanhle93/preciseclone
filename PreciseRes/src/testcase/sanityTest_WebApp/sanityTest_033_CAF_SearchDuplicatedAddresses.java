package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Constant;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;

public class sanityTest_033_CAF_SearchDuplicatedAddresses extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_CAF;
		applicantName = "CAF Demo Applicant";
		propertiesFileName = "CAF Required.xlsx";
		propertyAddress = "504 W Euclid";
		propertySearchAddress = "504 W Euclid " +accountNumber;
	}

	@Test(groups = { "regression" }, description = "SearchDuplicatedAddressPage 01 - Search by Property Address")
	public void SearchDuplicatedAddressPage_01_SearchByPropertyAddress() {
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Precondition 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		//First loan and first property

		log.info("Precondition 03. Create first Loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus);

		log.info("Precondition 04. Input Loan Amount Requested");
		loansPage.inputLoanAmountRequested(Integer.toString(1000));
		loansPage.inputFloor("10");
		loansPage.selectBridgeBorrower("Yes");

		log.info("Precondition 06. Click Save button");
		loansPage.clickSaveButton();
		
		log.info("Precondition 07. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Precondition 08. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("Precondition 08 - Step 01. Open Property Details page");
		propertiesPage = loansPage.openPropertyDetail(propertyAddress);
		
		log.info("Precondition 09 - Step 02. Input Address for searching");
		propertiesPage.inputAddress(propertySearchAddress);

		log.info("Precondition 10 - Step 03. Click 'Save' button");
		propertiesPage.clickSaveButton();
		
		log.info("Precondition 10 - Step 03. Open Loan page");
		loansPage = propertiesPage.openLoansPage(driver, ipClient);
		
		//Second loan and second property
		
		log.info("Precondition 11. Create second loan");
		loansPage.createNewLoan(loanName+"second", applicantName, "", loanStatus);

		log.info("Precondition 04. Input Loan Amount Requested");
		loansPage.inputLoanAmountRequested(Integer.toString(1000));
		loansPage.inputFloor("10");
		loansPage.selectBridgeBorrower("Yes");

		log.info("Precondition 14. Click Save button");
		loansPage.clickSaveButton();
		
		log.info("Precondition 15. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Precondition 16. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("Precondition 17 - Step 01. Open Property Details page");
		propertiesPage = loansPage.openPropertyDetail(propertyAddress);
		
		log.info("Precondition 18 - Step 02. Input Address for searching");
		propertiesPage.inputAddress(propertySearchAddress);

		log.info("Precondition 19 - Step 03. Click 'Save' button");
		propertiesPage.clickSaveButton();
		
		log.info("SearchDuplicatedAddressPage_01 - Step 01. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchDuplicatedAddressPage_01 - Step 02. Open Properties Page");
		propertiesPage.clickSearchDuplicatedAddressesButton();

		log.info("SearchDuplicatedAddressPage_01 - Step 03. Search by Loan name");
		propertiesPage.searchAddress(propertySearchAddress);
		
		log.info("VP: Two Properties are displayed");
		verifyEquals(propertiesPage.countNumberOfProperties(propertySearchAddress),2);

		city = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_City");
		state = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeState datacell_State");
		zip = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeZipCode datacell_Zip");
		applicant = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeForeignKey datacell_Borrower");
		loanIndex = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeForeignKey datacell_LoanApplication");
		active = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeBoolean datacell_IsActive");

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchDuplicatedPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex,
				active));
	}

	@Test(groups = { "regression" }, description = "SearchDuplicatedAddressPage 02 - Search by City")
	public void SearchDuplicatedAddressPage_02_SearchByCity() {
		
		log.info("SearchDuplicatedAddressPage_02 - Step 01. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchDuplicatedAddressPage_01 - Step 02. Open Properties Page");
		propertiesPage.clickSearchDuplicatedAddressesButton();
		
		log.info("SearchDuplicatedAddressPage_02 - Step 03. Search by City");
		propertiesPage.searchByCity(propertySearchAddress, city);
		
		log.info("VP: Two Properties are displayed");
		verifyTrue(propertiesPage.countNumberOfProperties(propertySearchAddress)==2);

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchDuplicatedPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex,
				active));
	}
	
	@Test(groups = { "regression" }, description = "SearchDuplicatedAddressPage 03 - Search by State")
	public void SearchDuplicatedAddressPage_03_SearchByState() {
		
		log.info("SearchDuplicatedAddressPage_03 - Step 01. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchDuplicatedAddressPage_03 - Step 02. Open Properties Page");
		propertiesPage.clickSearchDuplicatedAddressesButton();
		
		log.info("SearchDuplicatedAddressPage_03 - Step 03. Search by State");
		propertiesPage.searchByState(propertySearchAddress, state);
		
		log.info("VP: Two Properties are displayed");
		verifyTrue(propertiesPage.countNumberOfProperties(propertySearchAddress)==2);

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchDuplicatedPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex,
				active));
	}
	
	@Test(groups = { "regression" }, description = "SearchDuplicatedAddressPage 04 - Search by Zip")
	public void SearchDuplicatedAddressPage_04_SearchByZip() {
		
		log.info("SearchDuplicatedAddressPage_04 - Step 01. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchDuplicatedAddressPage_04 - Step 02. Open Properties Page");
		propertiesPage.clickSearchDuplicatedAddressesButton();
		
		log.info("SearchDuplicatedAddressPage_04 - Step 03. Search by Zip");
		propertiesPage.searchByZip(propertySearchAddress, zip);
		
		log.info("VP: Two Properties are displayed");
		verifyTrue(propertiesPage.countNumberOfProperties(propertySearchAddress)==2);

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchDuplicatedPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex,
				active));
	}
	
	@Test(groups = { "regression" }, description = "SearchDuplicatedAddressPage 05 - Search by Applicant")
	public void SearchDuplicatedAddressPage_05_SearchByApplicant() {
		
		log.info("SearchDuplicatedAddressPage_05 - Step 01. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchDuplicatedAddressPage_05 - Step 02. Open Properties Page");
		propertiesPage.clickSearchDuplicatedAddressesButton();
		
		log.info("SearchDuplicatedAddressPage_05 - Step 03. Search by Applicant name");
		propertiesPage.searchByApplicant(propertySearchAddress, applicant);
		
		log.info("VP: Two Properties are displayed");
		verifyTrue(propertiesPage.countNumberOfProperties(propertySearchAddress)==2);

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchDuplicatedPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex,
				active));
	}
	
	@Test(groups = { "regression" }, description = "SearchDuplicatedAddressPage 06 - Search by Loan Application")
	public void SearchDuplicatedAddressPage_06_SearchByLoanIndex() {
		
		log.info("SearchDuplicatedAddressPage_06 - Step 01. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchDuplicatedAddressPage_06 - Step 04. Open Properties Page");
		propertiesPage.clickSearchDuplicatedAddressesButton();
		
		log.info("SearchDuplicatedAddressPage_06 - Step 02. Search by Loan Application");
		propertiesPage.searchByLoanApplication(propertySearchAddress, loanIndex);
		
		log.info("VP: Two Properties are displayed");
		verifyTrue(propertiesPage.countNumberOfProperties(propertySearchAddress)==1);

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchDuplicatedPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex,
				active));
		
		log.info("SearchDuplicatedAddressPage_06 - Step 02. Search by Loan Application");
		propertiesPage.searchByLoanApplication(propertySearchAddress, loanIndex+"second");
		
		log.info("VP: Two Properties are displayed");
		verifyTrue(propertiesPage.countNumberOfProperties(propertySearchAddress)==1);

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchDuplicatedPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex+"second",
				active));
	}
	
	@Test(groups = { "regression" }, description = "SearchDuplicatedAddressPage 07 - Search by Active")
	public void SearchDuplicatedAddressPage_07_SearchByActive() {
		
		log.info("SearchDuplicatedAddressPage_07 - Step 01. Open Properties Page");
		propertiesPage.openPropertiesPage(driver, ipClient);
		
		log.info("SearchDuplicatedAddressPage_07 - Step 04. Open Properties Page");
		propertiesPage.clickSearchDuplicatedAddressesButton();
		
		log.info("SearchDuplicatedAddressPage_07 - Step 02. Search by Active status");
		propertiesPage.selectActiveRadioButton("Yes");
		propertiesPage.searchAddress(propertySearchAddress);
		
		log.info("VP: Two Properties are displayed");
		verifyTrue(propertiesPage.countNumberOfProperties(propertySearchAddress)==2);

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(propertiesPage.isAllValueDisplayOnSearchDuplicatedPropertiesTableB2R(propertySearchAddress, city, state, zip, applicant, loanIndex,
				active));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private String usernameLender, passwordLender;
	private String loanName, accountNumber, applicantName, propertiesFileName, loanStatus, propertyAddress, propertySearchAddress;
	private String city, state, zip, applicant, loanIndex, active;
}