package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFDocumentPage;
import page.FNFLoansPage;
import page.FNFPropertiesPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_016_FNF_LastChangedDate extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		loanName = "Edit Loan Auto " + getUniqueNumber();
//		documentType1 = "Borrower Application";
//		documentType2 = "Promissory Note";
		locClosingDate = "04/12/2014";
		addressApplicant = "CAF-FNF Applicant Address " + getUniqueNumber();
		addressLender = "CAF-FNF Lender Address " + getUniqueNumber();
		city = "Fresno";
		state = "CA - California";
		zip = "95542";
		date = "04/12/2016";
		number = "30";
		price = "$40.00";
		propertyType ="SFR";
		transactionType ="Refinance";
		titleCompany= "testing company global";
		escrowAgent = "testing escrow global";
		insuranceCompany = "testing insurance global";
		documentFolder1 = "Entity Documents";
		documentFolder2 = "Financials";
	}

	@Test(groups = { "regression" }, description = "Applicant - Login as Lender - Make sure Last changed date is not displayed if documents haven't loaded")
	public void LastChangedDate_01_Lender_LoanDetailsPage() {

		log.info("DocumentHistorycal_01 - Step 01. Login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);

		log.info("DocumentHistorycal_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("DocumentHistorycal_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("DocumentHistorycal_01 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("DocumentHistorycal_01 - Step 05: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("DocumentHistorycal_01 - Step 06: Input loan name");
		loanPage.inputRequiredData(loanName, locClosingDate);
		
		log.info("DocumentHistorycal_01 - Step 07: Click Save button");
		loanPage.clickOnSaveButton();

		log.info("DocumentHistorycal_01 - Step 09. Open document folder");
		documentsPage = loanPage.openDocumentsFolder(documentFolder1);
		
		log.info("VP: Date of Last change is empty");
		verifyFalse(documentsPage.isDateOfLastChangeDisplayed());
		
		log.info("VP: Status changed by is empty");
		verifyFalse(documentsPage.isStatusChangedByTextDisplayed());
		
	}

	@Test(groups = { "regression" }, description = "Applicant 30 - Login as Applicant - Make sure Last changed date is not displayed if documents haven't loaded")
	public void LastChangedDate_02_Borrower_LoanDetailsPage() {

		log.info("DocumentHistorycal_02 - Step 01: Login with Applicant");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);
		applicantsPage.setApplicantNameToDefault("CAF FNF", "Applicant");

		log.info("DocumentHistorycal_02 - Step 02: Open 'Loan Name'");
		loanPage = applicantsPage.openLoanNameInOpportunityTable(loanName);
		
		log.info("DocumentHistorycal_01 - Step 09. Open document folder");
		documentsPage = loanPage.openDocumentsFolder(documentFolder1);
		
		log.info("VP: Date of Last change is empty");
		verifyFalse(documentsPage.isDateOfLastChangeDisplayed());
		
		log.info("VP: Status changed by is empty");
		verifyFalse(documentsPage.isStatusChangedByTextDisplayed());
	}
	
	@Test(groups = { "regression" }, description = " Login as Lender - Make sure Last changed date is not displayed if documents haven't loaded")
	public void LastChangedDate_03_Lender_PropertiesPage() {

		log.info("Properties_02 - Step 01. Logout and login with Lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("Properties_02 - Step 02: Go to 'Properties' page");
		propertiesPage = homePage.openPropertiesPageFNF(driver, ipClient);
		
		log.info("Properties_02 - Step 03: Click on 'New' button");
		propertiesPage.clickNewPropertyButton();

		log.info("Properties_02 - Step 04: Input valid  all required information");
		propertiesPage.inputRequiredInformationProperties(companyName, loanName, addressLender, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany);
		
		log.info("Properties_02 - Step 05: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();

		log.info("VP: Record Saved message displays");
		verifyTrue(propertiesPage.isRecordSavedMessageDisplays());
		
		log.info("DocumentHistorycal_01 - Step 09. Open document folder");
		documentsPage = loanPage.openDocumentsFolder(documentFolder2);
		
		log.info("VP: Date of Last change is empty");
		verifyFalse(documentsPage.isDateOfLastChangeDisplayed());
		
		log.info("VP: Status changed by is empty");
		verifyFalse(documentsPage.isStatusChangedByTextDisplayed());
	}
	
	@Test(groups = { "regression" }, description = "Login as Applicant - Make sure Last changed date is not displayed if documents haven't loaded")
	public void LastChangedDate_04_Borrower_PropertiesPage() {

		log.info("Properties_02 - Step 01. Logout and login with Borrower");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);
		
		log.info("Properties_01 - Step 02: Go to 'Properties' page");
		propertiesPage = applicantsPage.openPropertiesPageFNF(driver, ipClient);
		
		log.info("Properties_01 - Step 03: Click on 'New' button");
		propertiesPage.clickNewPropertyButton();

		log.info("Properties_01 - Step 04: Input valid all required information");
		propertiesPage.inputRequiredInformationProperties("", loanName, addressApplicant, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany);
		
		log.info("Properties_01 - Step 05: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();

		log.info("VP: Record Saved message displays");
		verifyTrue(propertiesPage.isRecordSavedMessageDisplays());
		
		log.info("DocumentHistorycal_01 - Step 09. Open document folder");
		documentsPage = loanPage.openDocumentsFolder(documentFolder2);
		
		log.info("VP: Date of Last change is empty");
		verifyFalse(documentsPage.isDateOfLastChangeDisplayed());
		
		log.info("VP: Status changed by is empty");
		verifyFalse(documentsPage.isStatusChangedByTextDisplayed());
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private FNFLoansPage loanPage;
	private FNFDocumentPage documentsPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername,	FNFBorrowerPassword;
//	private String documentType1, documentType2;
	private String companyName, accountNumber, loanName, locClosingDate;
	private FNFPropertiesPage propertiesPage;
	private String addressApplicant, addressLender, city, state, zip, date, number, price, documentFolder1, documentFolder2;
	private String propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany;
	}