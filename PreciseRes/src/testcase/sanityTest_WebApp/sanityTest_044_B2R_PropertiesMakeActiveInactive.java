package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Constant;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;

public class sanityTest_044_B2R_PropertiesMakeActiveInactive extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower New";
		propertiesFileName = "B2RDataTape_multi-unit.xlsx";
		parentPropertyAddress = "Oakhill";
		subPropertyAddress = "37225 OAKHILL ST - suite 15";
		standAlonePropertyAddress = "10105 Spring ST";
	}

	@Test(groups = { "regression" }, description = "PropertiesMakeActiveInactive 01 - Check Added Property Active")
	public void PropertiesMakeActiveInactive_01_CheckAddedPropertyActive() {
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Precondition 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);

		log.info("Precondition 04. Change status of Loan");
		loansPage.selectLoanStatusBasicDetail("Term Sheet Sent");

		log.info("Precondition 05. Input Loan Amount Requested");
		loansPage.inputLoanAmountRequested("1000");

		log.info("Precondition 06. Click Save button");
		loansPage.clickSaveButton();

		log.info("Precondition 07. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Precondition 08. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("VP. Check Parent property is displayed correctly");
		verifyTrue(loansPage.isPropertyDisplayedCorrectly("Parent", parentPropertyAddress, "Yes"));
		
		log.info("VP. Check Sub property is displayed correctly");
		verifyTrue(loansPage.isPropertyDisplayedCorrectly("1", subPropertyAddress, "Yes"));
		
		log.info("VP. Check Stand-alone property is displayed correctly");
		verifyTrue(loansPage.isPropertyDisplayedCorrectly("", standAlonePropertyAddress, "Yes"));
	}

	@Test(groups = { "regression" }, description = "PropertiesMakeActiveInactive 02 - Check make propery inactive in Properties tab")
	public void PropertiesMakeActiveInactive_02_CheckMakePropertyInactivePropertyTab() {
		
		log.info("PropertiesMakeActiveInactive_02 - Step 01. Select Parent, sub and stand-alone properties");
		loansPage.selectPropertyCheckbox("Parent", parentPropertyAddress);
		loansPage.selectPropertyCheckbox("1", subPropertyAddress);
		loansPage.selectPropertyCheckbox("", standAlonePropertyAddress);
		
		log.info("PropertiesMakeActiveInactive_02 - Step 02: Click 'Make Inactive' button");
		loansPage.clickMakeInactiveButtonAtPropertyTab();

		log.info("VP. Check Parent property is displayed correctly");
		verifyFalse(loansPage.isPropertyDisplayedCorrectly("Parent", parentPropertyAddress, "Yes"));
		
		log.info("VP. Check Sub property is displayed correctly");
		verifyFalse(loansPage.isPropertyDisplayedCorrectly("1", subPropertyAddress, "Yes"));
		
		log.info("VP. Check Stand-alone property is displayed correctly");
		verifyFalse(loansPage.isPropertyDisplayedCorrectly("", standAlonePropertyAddress, "Yes"));
		
		log.info("PropertiesMakeActiveInactive_02 - Step 03. Click on Download CSV button");
		loansPage.deleteAllFileInFolder();
		loansPage.clickOnDownloadCsvFileButton(driver);

		log.info("PropertiesMakeActiveInactive_02 - Step 04. Wait for file downloaded complete");
		loansPage.waitForDownloadFileFullnameCompleted("csv");

		log.info("VP. Check Inactive Properties aren't displayed in CSV file");
		verifyFalse(loansPage.isCSVFileExportedCorrectly(loansPage.getFileNameInDirectory(), parentPropertyAddress, "Address"));
		verifyFalse(loansPage.isCSVFileExportedCorrectly(loansPage.getFileNameInDirectory(), subPropertyAddress, "Address"));
		verifyFalse(loansPage.isCSVFileExportedCorrectly(loansPage.getFileNameInDirectory(), standAlonePropertyAddress, "Address"));
		
		log.info("PropertiesMakeActiveInactive_02 - Step 05. Delete the downloaded file");
		loansPage.deleteContainsFileName("csv");
		
		log.info("PropertiesMakeActiveInactive_02 - Step 06. Search Property by Inactive status");
		loansPage.searchByActiveOrInactive("", "No");

		log.info("VP. Check Parent property is displayed correctly");
		verifyTrue(loansPage.isPropertyDisplayedCorrectly("Parent", parentPropertyAddress, "No"));
		
		log.info("VP. Check Sub property is displayed correctly");
		verifyTrue(loansPage.isPropertyDisplayedCorrectly("1", subPropertyAddress, "No"));
		
		log.info("VP. Check Stand-alone property is displayed correctly");
		verifyTrue(loansPage.isPropertyDisplayedCorrectly("", standAlonePropertyAddress, "No"));
		
		log.info("PropertiesMakeActiveInactive_02 - Step 07. Select Parent, sub and stand-alone properties");
		loansPage.selectPropertyCheckbox("Parent", parentPropertyAddress);
		loansPage.selectPropertyCheckbox("1", subPropertyAddress);
		loansPage.selectPropertyCheckbox("", standAlonePropertyAddress);
		
		log.info("PropertiesMakeActiveInactive_02 - Step 08: Click 'Make Active' button");
		loansPage.clickMakeActiveButtonAtPropertyTab();
		
		log.info("VP. Check Parent property is displayed correctly");
		verifyFalse(loansPage.isPropertyDisplayedCorrectly("Parent", parentPropertyAddress, "No"));
		
		log.info("VP. Check Sub property is displayed correctly");
		verifyFalse(loansPage.isPropertyDisplayedCorrectly("1", subPropertyAddress, "No"));
		
		log.info("VP. Check Stand-alone property is displayed correctly");
		verifyFalse(loansPage.isPropertyDisplayedCorrectly("", standAlonePropertyAddress, "No"));
		
		log.info("PropertiesMakeActiveInactive_02 - Step 09. Search Property by Active status");
		loansPage.searchByActiveOrInactive("", "Yes");
		
		log.info("VP. Check Parent property is displayed correctly");
		verifyTrue(loansPage.isPropertyDisplayedCorrectly("Parent", parentPropertyAddress, "Yes"));
		
		log.info("VP. Check Sub property is displayed correctly");
		verifyTrue(loansPage.isPropertyDisplayedCorrectly("1", subPropertyAddress, "Yes"));
		
		log.info("VP. Check Stand-alone property is displayed correctly");
		verifyTrue(loansPage.isPropertyDisplayedCorrectly("", standAlonePropertyAddress, "Yes"));
	}
	
	@Test(groups = { "regression" }, description = "PropertiesMakeActiveInactive 02 - Check make propery inactive in Properties details page")
	public void PropertiesMakeActiveInactive_03_CheckMakeSubPropertyInactivePropertyDetailsPage() {
		
		log.info("PropertiesMakeActiveInactive_03 - Step 01. Open sub property details page");
		propertiesPage = loansPage.openPropertyDetail(subPropertyAddress);
		
		log.info("PropertiesMakeActiveInactive_03 - Step 02. Click Mark Property inactive");
		propertiesPage.clickOnMarkPropertyInactiveButton();
		propertiesPage.switchToMakeInactiveFrame(driver);

		log.info("PropertiesMakeActiveInactive_03 - Step 03. On popup, click Mark inactive Property");
		propertiesPage.clickOnMarkInactivePropertyButton();

		log.info("PropertiesMakeActiveInactive_03 - Step 04. Click Property list");
		propertiesPage.switchToTopWindowFrame(driver);
		loansPage = propertiesPage.clickOnPropertyListButton();

		log.info("VP. Check Sub property is displayed correctly");
		verifyFalse(loansPage.isPropertyDisplayedCorrectly("1", subPropertyAddress, "No"));
	
		log.info("DataTape_04 - Step 01. Search Property by Inactive status");
		loansPage.searchByActiveOrInactive("", "No");
		
		log.info("VP. Check Sub property is displayed correctly");
		verifyTrue(loansPage.isPropertyDisplayedCorrectly("1", subPropertyAddress, "No"));
	
		log.info("PropertiesMakeActiveInactive_03 - Step 05. Open sub property details page");
		propertiesPage = loansPage.openPropertyDetail(subPropertyAddress);
		
		log.info("PropertiesMakeActiveInactive_03 - Step 06. Click Mark Property inactive");
		propertiesPage.clickOnMarkPropertyActiveButton();
		propertiesPage.switchToMakeActiveFrame(driver);

		log.info("PropertiesMakeActiveInactive_03 - Step 07. On popup, click Mark inactive Property");
		propertiesPage.clickOnMarkActivePropertyButton();

		log.info("PropertiesMakeActiveInactive_03 - Step 08. Click Property list");
		propertiesPage.switchToTopWindowFrame(driver);
		loansPage = propertiesPage.clickOnPropertyListButton();

		log.info("VP. Check Sub property is displayed correctly");
		verifyTrue(loansPage.isPropertyDisplayedCorrectly("1", subPropertyAddress, "Yes"));
	}
	
	@Test(groups = { "regression" }, description = "PropertiesMakeActiveInactive 02 - Check make stand alone propery inactive in Properties details page")
	public void PropertiesMakeActiveInactive_04_CheckMakeStandAlonePropertyInactivePropertyDetailsPage() {
		
		log.info("PropertiesMakeActiveInactive_04 - Step 01. Open stand Alone property details page");
		propertiesPage = loansPage.openPropertyDetail(standAlonePropertyAddress);
		
		log.info("PropertiesMakeActiveInactive_04 - Step 01. Click Mark Property inactive");
		propertiesPage.clickOnMarkPropertyInactiveButton();
		propertiesPage.switchToMakeInactiveFrame(driver);

		log.info("PropertiesMakeActiveInactive_04 - Step 02. On popup, click Mark inactive Property");
		propertiesPage.clickOnMarkInactivePropertyButton();

		log.info("PropertiesMakeActiveInactive_04 - Step 03. Click Property list");
		propertiesPage.switchToTopWindowFrame(driver);
		loansPage = propertiesPage.clickOnPropertyListButton();

		log.info("VP. Check Sub property is displayed correctly");
		verifyFalse(loansPage.isPropertyDisplayedCorrectly("", standAlonePropertyAddress, "No"));
	
		log.info("DataTape_04 - Step 01. Search Property by Inactive status");
		loansPage.searchByActiveOrInactive("", "No");
		
		log.info("VP. Check Sub property is displayed correctly");
		verifyTrue(loansPage.isPropertyDisplayedCorrectly("", standAlonePropertyAddress, "No"));
	
		log.info("PropertiesMakeActiveInactive_03 - Step 04. Open standAlone property details page");
		propertiesPage = loansPage.openPropertyDetail(standAlonePropertyAddress);
		
		log.info("PropertiesMakeActiveInactive_04 - Step 05. Click Mark Property inactive");
		propertiesPage.clickOnMarkPropertyActiveButton();
		propertiesPage.switchToMakeActiveFrame(driver);

		log.info("PropertiesMakeActiveInactive_04 - Step 06. On popup, click Mark inactive Property");
		propertiesPage.clickOnMarkActivePropertyButton();

		log.info("PropertiesMakeActiveInactive_04 - Step 07. Click Property list");
		propertiesPage.switchToTopWindowFrame(driver);
		loansPage = propertiesPage.clickOnPropertyListButton();

		log.info("VP. Check Sub property is displayed correctly");
		verifyTrue(loansPage.isPropertyDisplayedCorrectly("", standAlonePropertyAddress, "Yes"));
	}
	

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private String usernameLender, passwordLender;
	private String loanName, accountNumber, applicantName, propertiesFileName, loanStatus, parentPropertyAddress, subPropertyAddress;
	private String standAlonePropertyAddress;
}