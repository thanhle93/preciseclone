package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFDocumentPage;
import page.FNFLoansPage;
import page.FNFPropertiesPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_026_FNF_SearchPropertyDocuments extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		loanName = "Edit Loan Auto " + getUniqueNumber();
		validDate = "2/12/2014";
		documentFolder = "Financials";
		documentFileName = "datatest2.pdf";
		text = "comments";
		addressApplicant = "CAF-FNF Applicant Address " + getUniqueNumber();
		city = "Fresno";
		state = "CA - California";
		zip = "95542";
		date = "04/12/2016";
		number = "30";
		price = "$40.00";
		propertyType ="SFR";
		transactionType ="Refinance";
		titleCompany= "testing company global";
		escrowAgent = "testing escrow global";
		insuranceCompany = "testing insurance global";
	}


	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search borrower by Company Name")
	public void SearchPropertyDocuments_01_Lender_SearchByLoanName() {
	
		log.info("SearchPropertyDocuments_01 - Step 01. Logout and login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
		lenderName = homePage.getCurrentUserName(driver);
		
		log.info("SearchPropertyDocuments_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("SearchPropertyDocuments_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("SearchPropertyDocuments_01 - Step 05: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
	
		log.info("SearchPropertyDocuments_01 - Step 06: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("SearchPropertyDocuments_01 - Step 07: Input loan name");
		loanPage.inputRequiredData(loanName, validDate);
		
		log.info("SearchPropertyDocuments_01 - Step 08: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("SearchPropertyDocuments_01 - Step 01: Go to 'Properties' page");
		propertiesPage = loanPage.openPropertiesPageFNF(driver, ipClient);
		
		log.info("SearchPropertyDocuments_01 - Step 02: Click on 'New' button");
		propertiesPage.clickNewPropertyButton();
		
		log.info("SearchPropertyDocuments_01 - Step 03: Input valid all required information");
		propertiesPage.inputRequiredInformationProperties("", loanName, addressApplicant, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany);
		propertiesPage.selectBorrower(companyName);
		
		log.info("SearchPropertyDocuments_01 - Step 04: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();
		
		log.info("SearchPropertyDocuments_01 - Step 05. Open document folder");
		documentsPage = propertiesPage.openDocumentsFolder(documentFolder);
		
		log.info("SearchPropertyDocuments_01 - Step 06. Upload document file name 01");
		documentsPage.uploadDocumentFileForFolderByPlaceHolder(documentFileName);
		
		log.info("SearchPropertyDocuments_01 - Step 07. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("SearchPropertyDocuments_01 - Step 08. Input comment");
		documentsPage.inputComments(documentFileName, text);
		
		log.info("SearchPropertyDocuments_01 - Step 09. Click Save button");
		documentsPage.clickSaveButton();
	
		log.info("SearchPropertyDocuments_01 - Step 10: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
		
		log.info("SearchPropertyDocuments_01 - Step 11: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);
		
		log.info("SearchPropertyDocuments_01 - Step 12: Get information for checking in CSV file");
		documentFolder = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_DisplayName");
		documentsIndex = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeSQLServerIdentity datacell_Document");
		status = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_Status");
		lastUpoadDate = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeDateTime datacell_DocumentChangedDate");
		lastUploadBy = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_DocumentChangedByName");
		lastComments = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeLongVarchar datacell_ApproveReason");
		active = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeBoolean datacell_IsActive");
		addressApplicant = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_PropertyAddress");

		log.info("VP: Property Documents information displayed correctly");
		verifyTrue(documentsPage.isAllPropertyDocumentsRecordValuesDisplayedCorrectly(documentsIndex, documentFolder, lastUpoadDate, lastUploadBy, lastComments, status, active, addressApplicant));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search documents by Document index")
	public void SearchPropertyDocuments_02_Lender_SearchByDocumentIndex() {

		log.info("SearchPropertyDocuments_02 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
	
		log.info("SearchPropertyDocuments_02 - Step 02: Input 'Document #'");
		documentsPage.inputDocumentIndex(documentsIndex);
		
		log.info("SearchPropertyDocuments_02 - Step 03: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);
	
		log.info("VP: Property Documents information displayed correctly");
		verifyTrue(documentsPage.isAllPropertyDocumentsRecordValuesDisplayedCorrectly(documentsIndex, documentFolder, lastUpoadDate, lastUploadBy, lastComments, status, active, addressApplicant));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search documents by Document folder name")
	public void SearchPropertyDocuments_03_Lender_SearchByDocumentFolderName() {

		log.info("SearchPropertyDocuments_03 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
	
		log.info("SearchPropertyDocuments_03 - Step 02: Input 'Document folder name'");
		documentsPage.inputDocumentFolderName(documentFolder);
		
		log.info("SearchPropertyDocuments_03 - Step 03: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);
	
		log.info("VP: Property Documents information displayed correctly");
		verifyTrue(documentsPage.isAllPropertyDocumentsRecordValuesDisplayedCorrectly(documentsIndex, documentFolder, lastUpoadDate, lastUploadBy, lastComments, status, active, addressApplicant));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search documents by Borrower name")
	public void SearchPropertyDocuments_04_Lender_SearchByBorrowerName() {

		log.info("SearchPropertyDocuments_04 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
	
		log.info("SearchPropertyDocuments_04 - Step 02: Input 'Borrower name'");
		documentsPage.inputBorrowerName(companyName);
		
		log.info("SearchPropertyDocuments_04 - Step 03: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);
	
		log.info("VP: Property Documents information displayed correctly");
		verifyTrue(documentsPage.isAllPropertyDocumentsRecordValuesDisplayedCorrectly(documentsIndex, documentFolder, lastUpoadDate, lastUploadBy, lastComments, status, active, addressApplicant));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search documents by Document status")
	public void SearchPropertyDocuments_05_Lender_SearchByStatus() {

		log.info("SearchPropertyDocuments_05 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
	
		log.info("SearchPropertyDocuments_05 - Step 02: Input 'Document status'");
		documentsPage.selectDocumentStatus(status);
		
		log.info("SearchPropertyDocuments_05 - Step 03: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);
	
		log.info("VP: Property Documents information displayed correctly");
		verifyTrue(documentsPage.isAllPropertyDocumentsRecordValuesDisplayedCorrectly(documentsIndex, documentFolder, lastUpoadDate, lastUploadBy, lastComments, status, active, addressApplicant));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search documents by Last modified by")
	public void SearchPropertyDocuments_06_Lender_SearchByLastModifiedBy() {

		log.info("SearchPropertyDocuments_06 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
	
		log.info("SearchPropertyDocuments_06 - Step 02: Input 'Last modified date'");
		documentsPage.inputLastModifiedDate(lastUpoadDate);
		
		log.info("SearchPropertyDocuments_06 - Step 03: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);
	
		log.info("VP: Property Documents information displayed correctly");
		verifyTrue(documentsPage.isAllPropertyDocumentsRecordValuesDisplayedCorrectly(documentsIndex, documentFolder, lastUpoadDate, lastUploadBy, lastComments, status, active, addressApplicant));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search documents by Modified by")
	public void SearchPropertyDocuments_07_Lender_SearchByModifiedBy() {

		log.info("SearchPropertyDocuments_07 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
	
		log.info("SearchPropertyDocuments_07 - Step 02: Input 'Modified By'");
		documentsPage.inputModifiedBy(lastUploadBy);
		
		log.info("SearchPropertyDocuments_07 - Step 03: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);
	
		log.info("VP: Property Documents information displayed correctly");
		verifyTrue(documentsPage.isAllPropertyDocumentsRecordValuesDisplayedCorrectly(documentsIndex, documentFolder, lastUpoadDate, lastUploadBy, lastComments, status, active, addressApplicant));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search documents by Active")
	public void SearchPropertyDocuments_08_Lender_SearchByActive() {

		log.info("SearchPropertyDocuments_08 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
	
		log.info("SearchPropertyDocuments_08 - Step 02: Input 'Active status'");
		documentsPage.selectActiveRadioButton(active);
		
		log.info("SearchPropertyDocuments_08 - Step 03: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);
	
		log.info("VP: Property Documents information displayed correctly");
		verifyTrue(documentsPage.isAllPropertyDocumentsRecordValuesDisplayedCorrectly(documentsIndex, documentFolder, lastUpoadDate, lastUploadBy, lastComments, status, active, addressApplicant));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search borrower by Company Name")
	public void SearchPropertyDocuments_09_Borrower_SearchByLoanName() {
	
		log.info("SearchPropertyDocuments_09 - Step 01. Logout and login with Borrower");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);

		log.info("SearchPropertyDocuments_09 - Step 10: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
		
		log.info("SearchPropertyDocuments_09 - Step 11: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);

		log.info("VP: Property Documents information displayed correctly");
		verifyTrue(documentsPage.isAllPropertyDocumentsRecordValuesDisplayedCorrectly(documentsIndex, documentFolder, lastUpoadDate, lastUploadBy, lastComments, status, active, addressApplicant));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search documents by Document index")
	public void SearchPropertyDocuments_10_Borrower_SearchByDocumentIndex() {

		log.info("SearchPropertyDocuments_10 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
	
		log.info("SearchPropertyDocuments_10 - Step 02: Input 'Document #'");
		documentsPage.inputDocumentIndex(documentsIndex);
		
		log.info("SearchPropertyDocuments_10 - Step 03: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);
	
		log.info("VP: Property Documents information displayed correctly");
		verifyTrue(documentsPage.isAllPropertyDocumentsRecordValuesDisplayedCorrectly(documentsIndex, documentFolder, lastUpoadDate, lastUploadBy, lastComments, status, active, addressApplicant));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search documents by Document folder name")
	public void SearchPropertyDocuments_11_Borrower_SearchByDocumentFolderName() {

		log.info("SearchPropertyDocuments_11 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
	
		log.info("SearchPropertyDocuments_11 - Step 02: Input 'Document folder name'");
		documentsPage.inputDocumentFolderName(documentFolder);
		
		log.info("SearchPropertyDocuments_11 - Step 03: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);
	
		log.info("VP: Property Documents information displayed correctly");
		verifyTrue(documentsPage.isAllPropertyDocumentsRecordValuesDisplayedCorrectly(documentsIndex, documentFolder, lastUpoadDate, lastUploadBy, lastComments, status, active, addressApplicant));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search documents by Lender name")
	public void SearchPropertyDocuments_12_Borrower_SearchByLenderName() {

		log.info("SearchPropertyDocuments_12 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
	
		log.info("SearchPropertyDocuments_12 - Step 02: Input 'Lender name'");
		documentsPage.inputLenderName(lenderName);
		
		log.info("SearchPropertyDocuments_12 - Step 03: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);
	
		log.info("VP: Property Documents information displayed correctly");
		verifyTrue(documentsPage.isAllPropertyDocumentsRecordValuesDisplayedCorrectly(documentsIndex, documentFolder, lastUpoadDate, lastUploadBy, lastComments, status, active, addressApplicant));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search documents by Document status")
	public void SearchPropertyDocuments_13_Borrower_SearchByStatus() {

		log.info("SearchPropertyDocuments_13 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
	
		log.info("SearchPropertyDocuments_13 - Step 02: Input 'Document status'");
		documentsPage.selectDocumentStatus(status);
		
		log.info("SearchPropertyDocuments_13 - Step 03: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);
	
		log.info("VP: Property Documents information displayed correctly");
		verifyTrue(documentsPage.isAllPropertyDocumentsRecordValuesDisplayedCorrectly(documentsIndex, documentFolder, lastUpoadDate, lastUploadBy, lastComments, status, active, addressApplicant));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search documents by Last modified by")
	public void SearchPropertyDocuments_14_Borrower_SearchByLastModifiedBy() {

		log.info("SearchPropertyDocuments_14 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
	
		log.info("SearchPropertyDocuments_14 - Step 02: Input 'Last modified date'");
		documentsPage.inputLastModifiedDate(lastUpoadDate);
		
		log.info("SearchPropertyDocuments_14 - Step 03: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);
	
		log.info("VP: Property Documents information displayed correctly");
		verifyTrue(documentsPage.isAllPropertyDocumentsRecordValuesDisplayedCorrectly(documentsIndex, documentFolder, lastUpoadDate, lastUploadBy, lastComments, status, active, addressApplicant));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search documents by Modified by")
	public void SearchPropertyDocuments_15_Borrower_SearchByModifiedBy() {

		log.info("SearchPropertyDocuments_15 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
	
		log.info("SearchPropertyDocuments_15 - Step 02: Input 'Modified By'");
		documentsPage.inputModifiedBy(lastUploadBy);
		
		log.info("SearchPropertyDocuments_15 - Step 03: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);
	
		log.info("VP: Property Documents information displayed correctly");
		verifyTrue(documentsPage.isAllPropertyDocumentsRecordValuesDisplayedCorrectly(documentsIndex, documentFolder, lastUpoadDate, lastUploadBy, lastComments, status, active, addressApplicant));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search documents by Active")
	public void SearchPropertyDocuments_16_Borrower_SearchByActive() {

		log.info("SearchPropertyDocuments_16 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
	
		log.info("SearchPropertyDocuments_16 - Step 02: Input 'Active status'");
		documentsPage.selectActiveRadioButton(active);
		
		log.info("SearchPropertyDocuments_16 - Step 03: Input 'Property Address' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);
	
		log.info("VP: Property Documents information displayed correctly");
		verifyTrue(documentsPage.isAllPropertyDocumentsRecordValuesDisplayedCorrectly(documentsIndex, documentFolder, lastUpoadDate, lastUploadBy, lastComments, status, active, addressApplicant));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private FNFLoansPage loanPage;
	private FNFDocumentPage documentsPage;
	private FNFPropertiesPage propertiesPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername, FNFBorrowerPassword;	
	private String companyName, accountNumber;
	private String status;
	private String validDate;
	private String documentFolder;
	private String documentFileName;
	private String lastUpoadDate, lastUploadBy, lastComments;
	private String addressApplicant, city, state, zip, date, number, price;
	private String propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany;
	private String documentsIndex, active;
	private String loanName, text, lenderName;
	}