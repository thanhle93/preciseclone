package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Constant;
import page.FNFBorrowersPage;
import page.FNFLoansPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;

public class sanityTest_015_STO_EditNameWithSpecialCharacters extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		fnfLenderUsername = Constant.USERNAME_FNF_LENDER;
		fnfLenderPassword = Constant.PASSWORD_FNF_LENDER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		specialCharacter = "-�-,.!@#$%^&*()[]{}/?<>";
		companyName = "Edit_Borrower " + accountNumber;
		loanName = "Speacial" + (accountNumber + 1);
		locClosingDate = "04/12/2014";
		
		address = "1020 Bonforte Blvd -�-,.!@#$%^&*()[]{}/?<>";
		propertiesFileName = "StoreDataTape_SpecialCharacters.xlsx";
		documentType = "Document Type" + specialCharacter;
		documentFileName = "datatest.pdf";
		xmlFileName = "Transaction New.xml";
		documentSection = "Lease General Documents - Lease Documents";
	}
	
	@Test(groups = { "regression" },description = "SpecialCharacters01 - Create new a Transaction with special characters")
	public void SpecialCharacters_01_CreateNewLoanWithSpecialCharacters()
	{
		log.info("SpecialCharacters_01 - Step 01: Login with valid username/ password");
		homePage = loginPage.loginAsLender(fnfLenderUsername, fnfLenderPassword, false);
		
		log.info("SpecialCharacters_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("SpecialCharacters_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("SpecialCharacters_01 - Step 04: Open 'Applicant Name' detail");
		applicantsPage.clickOnApplicantName();
		
		log.info("SpecialCharacters_01 - Step 05: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("SpecialCharacters_01 - Step 06: Input Loan name");
		loanPage.inputRequiredData(loanName, locClosingDate);
		
		log.info("SpecialCharacters_01 - Step 07: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("SpecialCharacters_01 - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();
		
		log.info("SpecialCharacters_01 - Step 03: Click 'Load Transaction' button");
		transactionPage.clickLoadTransactionButton();
		
		log.info("SpecialCharacters_01 - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);
		
		log.info("VP: 'Load data has been done' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Load data has been done"));
		
		log.info("SpecialCharacters_01 - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("SpecialCharacters_01 - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: Verify that 'Company Name' is saved successfully");
		verifyTrue(transactionPage.isCompanyNameSavedSuccessfully(companyName));
	}
	
	@Test(groups = { "regression" },description = "SpecialCharacters02 - Upload data tape with special characters")
	public void SpecialCharacters_02_UploadDatatapeWithSpecialCharacters()
	{
		log.info("SpecialCharacters_02 - Step 01. Open Properties tab");
		transactionPage.openPropertyTab();

		log.info("SpecialCharacters_02 - Step 02: Click 'Add Properties' button");
		transactionPage.clickAddPropertyButton();
		
		log.info("SpecialCharacters_02 - Step 03: Load 'Property Excel Template' file");
		transactionPage.uploadFileProperties(propertiesFileName);
		
		log.info("SpecialCharacters_02 - Step 04. Search with P# is 'P0001686'");
		transactionPage.searchPropertyPNumber("P0001686");
		
		log.info("SpecialCharacters_02 - Step 05. Open a property detail");
		transactionPage.openPropertyDetail(address);
		
		log.info("VP: Value of Address  field is '1020 Bonforte Blvd -�-,.!@#$%^&*()[]{}/?<>'");
		verifyTrue(transactionPage.isAddressSavedSuccessfully("1020 Bonforte Blvd -�-,.!@#$%^&*()[]{}/?<>"));                                      
	}
	
	@Test(groups = { "regression" },description = "SpecialCharacters03 - Add a document with special characters to a property")
	public void SpecialCharacters_03_AddNewPropertyTypeWithSpecialCharacters()
	{
		log.info("SpecialCharacters_03 - Step 01. Click 'New Property Documents' button");
		transactionPage.clickNewPropertyDocumentsButton();

		log.info("SpecialCharacters_03 - Step 02. Input to 'Other Document Type' textbox");
		transactionPage.inputOtherDocumentType(documentType);
		
		log.info("SpecialCharacters_03 - Step 03. Upload document file name");
		transactionPage.uploadDocumentFileName(documentFileName);

		log.info("SpecialCharacters_03 - Step 04. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("SpecialCharacters_03 - Step 05. Click 'Go to Property' button");
		transactionPage.clickGoToPropertyButtonAtDocumentTypeDetails();

		log.info("VP: New Document Type is created successfully");
		verifyTrue(transactionPage.isDocumentLoadedToDocumentType(documentType, documentFileName));
	}
		
	@Test(groups = { "regression" }, description = "Document 01 - Add New Transaction Document with special characters")
	public void SpecialCharacters_04_AddNewTransactionDocumentWithSpecialCharacters() {

		log.info("SpecialCharacters_04 - Step 01: Open Transaction Documents tab");
		transactionPage.openTransactionDocumentsTab();

		log.info("SpecialCharacters_04 - Step 02. Click 'New Transaction Documents' button");
		transactionPage.clickNewTransactionDocumentLesseeButton();

		log.info("SpecialCharacters_04 - Step 03. Select Section value");
		transactionPage.selectTransactionDocumentLesseeSection(documentSection);

		log.info("SpecialCharacters_04 - Step 04. Input to 'Other Document Type' textbox");
		transactionPage.inputOtherDocumentType(documentType);

		log.info("SpecialCharacters_04 - Step 05. Upload Document file name");
		transactionPage.uploadDocumentFileName(documentFileName);

		log.info("SpecialCharacters_04 - Step 06. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("SpecialCharacters_04 - Step 07. Click 'Document List' button");
		transactionPage.clickDocumentListButtonAtDocumentTypeDetails();

		log.info("SpecialCharacters_04 - Step 08. Search with 'Custom Type'");
		transactionPage.searchWithCustomType(documentType);

		log.info("VP: New Document type is created successfully");
		verifyTrue(transactionPage.isDocumentUploadedToDocumentType(documentType));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private FNFLoansPage loanPage;
	private String fnfLenderUsername, fnfLenderPassword, loanName;
	private TransactionPage transactionPage;
	private String specialCharacter, xmlFileName, address, documentType, documentFileName;
	private String companyName, accountNumber, propertiesFileName, documentSection, locClosingDate;
}