package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFDocumentPage;
import page.FNFLoansPage;
import page.FNFPropertiesPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_024_FNF_DownloadCSVList extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		loanName = "Edit Loan Auto " + getUniqueNumber();
		underwriter = "CAF FNF Lender";
		validDate = "2/12/2014";
		validNumber ="10";
		originator = "CAF FNF Lender";
		documentFolder1 = "Entity Documents";
		documentFolder2 = "Financials";
		documentFileName1 = "datatest1.pdf";
		documentFileName2 = "datatest2.pdf";
		text = "comments";
		addressApplicant = "CAF-FNF Applicant Address " + getUniqueNumber();
		city = "Fresno";
		state = "CA - California";
		zip = "95542";
		date = "04/12/2016";
		number = "30";
		price = "$40.00";
		propertyType ="SFR";
		transactionType ="Refinance";
		titleCompany= "testing company global";
		escrowAgent = "testing escrow global";
		insuranceCompany = "testing insurance global";
	}


	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Download CSV list Borrower page")
	public void DownloadCSVList_01_Lender_BorrowerPage() {

		log.info("DownloadCSVList_01 - Step 01. Logout and login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("DownloadCSVList_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("DownloadCSVList_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("DownloadCSVList_01 - Step 04: Get all record values");
		email = applicantsPage.getRecordValue("Email");
		phone = applicantsPage.getRecordValue("Phone");
		dateInvited = applicantsPage.convertDate(applicantsPage.getRecordValue("Date Invited"));
		status = applicantsPage.getRecordValue("Active");
		
		log.info("DownloadCSVList_01 - Step 05: Go to 'Applicant' page");
		applicantsPage.openBorrowersPage(driver, ipClient);
		
		log.info("DownloadCSVList_03 - Step 06. Click on Download CSV button");
		applicantsPage.deleteAllFileInFolder();
		applicantsPage.clickOnDownloadCsvFileButton(driver);

		log.info("DownloadCSVList_03 - Step 07. Wait for file downloaded complete");
		applicantsPage.waitForDownloadFileFullnameCompleted("csv");

		log.info("VP. Records are displayed correctly in CSV file");
		verifyTrue(applicantsPage.isRecordsExportedCorrectly(applicantsPage.getFileNameInDirectory(), companyName, "Company Name", applicantsPage.getNumberOfRecords(driver), email, phone, status, dateInvited));
		
		log.info("DownloadCSVList_03 - Step 08. Delete the downloaded file");
		applicantsPage.deleteContainsFileName("csv");
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Download CSV list Loan Page")
	public void DownloadCSVList_02_Lender_LoansPage() {

		log.info("DownloadCSVList_02 - Step 01: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("DownloadCSVList_02 - Step 02: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("DownloadCSVList_02 - Step 03: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("DownloadCSVList_02 - Step 04: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("DownloadCSVList_02 - Step 05: Input loan name");
		loanPage.inputRequiredData(loanName, validDate);
		
		log.info("DownloadCSVList_02 - Step 06: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("DownloadCSVList_02 - Step 07: Input data for loan searching");
		loanPage.inputDataForLoanSearching(originator, underwriter, validNumber, validDate);
		
		log.info("DownloadCSVList_02 - Step 08: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("DownloadCSVList_02 - Step 09: Click Back to loan list button");
		loanPage.clickBackToListButton();
		
		log.info("DownloadCSVList_03 - Step 10. Click on Download CSV button");
		loanPage.deleteAllFileInFolder();
		loanPage.clickOnDownloadCsvFileButton(driver);

		log.info("DownloadCSVList_03 - Step 11. Wait for file downloaded complete");
		loanPage.waitForDownloadFileFullnameCompleted("csv");

		log.info("VP. Records are displayed correctly in CSV file");
		verifyTrue(loanPage.isRecordsExportedCorrectly(loanPage.getFileNameInDirectory(), loanName, "Loan Name", loanPage.getNumberOfRecords(driver), originator, underwriter, validNumber, loanPage.convertDate(validDate), loanPage.convertNumberToPercent(validNumber)));
		
		log.info("DownloadCSVList_03 - Step 12. Delete the downloaded file");
		loanPage.deleteContainsFileName("csv");
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Download CSV list Loan Page")
	public void DownloadCSVList_03_Lender_LoansDocumentsTab() {

		log.info("DownloadCSVList_03 - Step 01: Go to Loan detail page");
		loanPage.clickOnSearchButton(loanName);
		loanPage.openLoansDetailPage(loanName);
		
		log.info("DownloadCSVList_03 - Step 02. Open document folder");
		documentsPage = loanPage.openDocumentsFolder(documentFolder1);
		
		log.info("DownloadCSVList_03 - Step 03. Upload document file name 01");
		documentsPage.uploadDocumentFileForFolderByPlaceHolder(documentFileName1);
		
		log.info("DownloadCSVList_03 - Step 04. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("DownloadCSVList_03 - Step 05. Input comment");
		documentsPage.inputComments(documentFileName1, text);
		
		log.info("DownloadCSVList_03 - Step 06. Click Save button");
		documentsPage.clickSaveButton();
	
		log.info("DownloadCSVList_03 - Step 07: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openLoanDocumentsTab();
		
		log.info("DownloadCSVList_03 - Step 08: Input 'Loan Name' and click on 'Search' button");
		documentsPage.clickOnSearchLoansButton(loanName);
		
		log.info("DownloadCSVList_03 - Step 09: Get information for checking in CSV file");
		documentFolder1 = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_DisplayName");
		documentFileName1 = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeHTML datacell_DocumentFile");
		lastUpoadDate1 = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeDateTime datacell_DocumentChangedDate",1);
		lastUploadBy1 = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_DocumentChangedByName");
		lastComments1 = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeLongVarchar datacell_ApproveReason");
		
		log.info("DownloadCSVList_03 - Step 10: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openLoanDocumentsTab();
		
		log.info("DownloadCSVList_03 - Step 11. Click on Download CSV button");
		documentsPage.deleteAllFileInFolder();
		documentsPage.clickOnDownloadCsvFileButton(driver);

		log.info("DownloadCSVList_03 - Step 12. Wait for file downloaded complete");
		documentsPage.waitForDownloadFileFullnameCompleted("csv");

		log.info("VP. Records are displayed correctly in CSV file");
		verifyTrue(documentsPage.isRecordsExportedCorrectly(loanPage.getFileNameInDirectory(), loanName, "Loan Name", loanPage.getNumberOfRecords(driver), documentFolder1, documentFileName1, lastUpoadDate1, lastUploadBy1, lastComments1));
		
		log.info("DownloadCSVList_03 - Step 13. Delete the downloaded file");
		documentsPage.deleteContainsFileName("csv");
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Download CSV list Loan Page")
	public void DownloadCSVList_04_Lender_PropertyDocumentsTab() {

		log.info("DownloadCSVList_04 - Step 01: Go to 'Properties' page");
		propertiesPage = applicantsPage.openPropertiesPageFNF(driver, ipClient);
		
		log.info("DownloadCSVList_04 - Step 02: Click on 'New' button");
		propertiesPage.clickNewPropertyButton();
		
		log.info("DownloadCSVList_04 - Step 03: Input valid all required information");
		propertiesPage.inputRequiredInformationProperties("", loanName, addressApplicant, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany);
		propertiesPage.selectBorrower(companyName);
		
		log.info("DownloadCSVList_04 - Step 04: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();
		
		log.info("DownloadCSVList_04 - Step 05. Open document folder");
		documentsPage = propertiesPage.openDocumentsFolder(documentFolder2);
		
		log.info("DownloadCSVList_04 - Step 06. Upload document file name 01");
		documentsPage.uploadDocumentFileForFolderByPlaceHolder(documentFileName2);
		
		log.info("DownloadCSVList_04 - Step 07. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("DownloadCSVList_04 - Step 08. Input comment");
		documentsPage.inputComments(documentFileName2, text);
		
		log.info("DownloadCSVList_04 - Step 09. Click Save button");
		documentsPage.clickSaveButton();
	
		log.info("DownloadCSVList_04 - Step 10: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
		
		log.info("DownloadCSVList_04 - Step 11: Input 'Loan Name' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);
		
		log.info("DownloadCSVList_04 - Step 12: Get information for checking in CSV file");
		documentFolder2 = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_DisplayName");
		documentsIndex = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeSQLServerIdentity datacell_Document");
		status = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_Status");
		lastUpoadDate2 = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeDateTime datacell_DocumentChangedDate",1);
		lastUploadBy2 = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_DocumentChangedByName");
		lastComments2 = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeLongVarchar datacell_ApproveReason");
		active = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeBoolean datacell_IsActive");
		addressApplicant = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_PropertyAddress");
		
		log.info("DownloadCSVList_04 - Step 13. Click on Download CSV button");
		documentsPage.deleteAllFileInFolder();
		documentsPage.clickOnDownloadCsvFileButton(driver);

		log.info("DownloadCSVList_04 - Step 14. Wait for file downloaded complete");
		documentsPage.waitForDownloadFileFullnameCompleted("csv");

		log.info("VP. Records are displayed correctly in CSV file");
		verifyTrue(documentsPage.isRecordsExportedCorrectly(loanPage.getFileNameInDirectory(), documentsIndex, "Document #", loanPage.getNumberOfRecords(driver), documentFolder2, status, lastUpoadDate2, lastUploadBy2, lastComments2, active, addressApplicant));
		
		log.info("DownloadCSVList_04 - Step 15. Delete the downloaded file");
		documentsPage.deleteContainsFileName("csv");
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Download CSV list Loan Page")
	public void DownloadCSVList_05_Borrower_LoansPage() {

		log.info("DownloadCSVList_05 - Step 01. Login with Borrower");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);

		log.info("DownloadCSVList_05 - Step 02. Login with Borrower");
		loanPage= applicantsPage.openFNFLoansPage(driver, ipClient);
		
		log.info("DownloadCSVList_03 - Step 03. Click on Download CSV button");
		loanPage.deleteAllFileInFolder();
		loanPage.clickOnDownloadCsvFileButton(driver);

		log.info("DownloadCSVList_03 - Step 04. Wait for file downloaded complete");
		loanPage.waitForDownloadFileFullnameCompleted("csv");

		log.info("VP. Records are displayed correctly in CSV file");
		verifyTrue(loanPage.isRecordsExportedCorrectly(loanPage.getFileNameInDirectory(), loanName, "Loan Name", loanPage.getNumberOfRecords(driver), originator, underwriter, validNumber, loanPage.convertDate(validDate), loanPage.convertNumberToPercent(validNumber)));
		
		log.info("DownloadCSVList_03 - Step 05. Delete the downloaded file");
		loanPage.deleteContainsFileName("csv");
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Download CSV list Loan Page")
	public void DownloadCSVList_06_Borrower_LoansDocumentsTab() {

		log.info("DownloadCSVList_06 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage = loanPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openLoanDocumentsTab();
		
		log.info("DownloadCSVList_06 - Step 02. Click on Download CSV button");
		documentsPage.deleteAllFileInFolder();
		documentsPage.clickOnDownloadCsvFileButton(driver);

		log.info("DownloadCSVList_06 - Step 03. Wait for file downloaded complete");
		documentsPage.waitForDownloadFileFullnameCompleted("csv");

		log.info("VP. Records are displayed correctly in CSV file");
		verifyTrue(documentsPage.isRecordsExportedCorrectly(loanPage.getFileNameInDirectory(), loanName, "Loan Name", loanPage.getNumberOfRecords(driver), documentFolder1, documentFileName1, lastUpoadDate1, lastUploadBy1, lastComments1));
		
		log.info("DownloadCSVList_06 - Step 04. Delete the downloaded file");
		documentsPage.deleteContainsFileName("csv");
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Download CSV list Loan Page")
	public void DownloadCSVList_07_Borrower_PropertyDocumentsTab() {
	
		log.info("DownloadCSVList_07 - Step 01: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
		
		log.info("DownloadCSVList_07 - Step 02: Input 'Loan Name' and click on 'Search' button");
		documentsPage.clickOnSearchPropertyButton(addressApplicant);

		log.info("DownloadCSVList_07 - Step 03. Click on Download CSV button");
		documentsPage.deleteAllFileInFolder();
		documentsPage.clickOnDownloadCsvFileButton(driver);

		log.info("DownloadCSVList_07 - Step 04. Wait for file downloaded complete");
		documentsPage.waitForDownloadFileFullnameCompleted("csv");

		log.info("VP. Records are displayed correctly in CSV file");
		verifyTrue(documentsPage.isRecordsExportedCorrectly(loanPage.getFileNameInDirectory(), documentsIndex, "Document #", loanPage.getNumberOfRecords(driver), documentFolder2, status, lastUpoadDate2, lastUploadBy2, lastComments2, active));
		
		log.info("DownloadCSVList_07 - Step 05. Delete the downloaded file");
		documentsPage.deleteContainsFileName("csv");
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFLoansPage loanPage;
	private FNFBorrowersPage applicantsPage;
	private FNFDocumentPage documentsPage;
	private FNFPropertiesPage propertiesPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername, FNFBorrowerPassword;
	private String companyName, accountNumber;
	private String email, phone, dateInvited, status;
	private String loanName, underwriter, originator, text;
	private String validDate, validNumber;
	private String documentFolder1, documentFolder2;
	private String documentFileName1, documentFileName2;
	private String lastUpoadDate1, lastUploadBy1, lastComments1;
	private String lastUpoadDate2, lastUploadBy2, lastComments2;
	private String addressApplicant, city, state, zip, date, number, price;
	private String propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany;
	private String documentsIndex, active;
	}