package page;

import java.util.Set;
import java.util.Stack;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import PreciseRes.Interfaces;
import common.Common;

/**
 * @author Administrator
 */
public class FNFDocumentPage extends AbstractPage {
	public FNFDocumentPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
	}
	
	/**
	 * select Document Type
	 */
	public void selectDocumentType(String documentType) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.DOCUMENT_TYPE_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.FNFDocumentsPage.DOCUMENT_TYPE_COMBOBOX, documentType);
		sleep(1);
	}

	/**
	 * upload Document File
	 */
	public void uploadDocumentFile(String fileName) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.CHOOSE_FILE_BUTTON, timeWait);
		doubleClick(driver, Interfaces.FNFDocumentsPage.CHOOSE_FILE_BUTTON);
		sleep(1);
		Common.getCommon().openFileForUpload(driver, fileName);
		sleep();
	}
	
	/**
	 * upload Document File
	 */
	public void uploadDocumentFile(String fileName1, String fileName2, String fileName3) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.CHOOSE_FILE_BUTTON, timeWait);
		doubleClick(driver, Interfaces.FNFDocumentsPage.CHOOSE_FILE_BUTTON);
		sleep(1);
		Common.getCommon().openFileForUpload(driver, fileName1, fileName2, fileName3);
		sleep();
	}
	
	/**
	 * click Save Button
	 */
	public void clickSaveButton() {
		waitForControl(driver, Interfaces.FNFDocumentsPage.SAVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
          sleep(10);
     }
		sleep(5);
		isRecordSavedMessageDisplays();
	}
	
	/**
	 * check Record Saved Message Displays
	 */
	public boolean isRecordSavedMessageDisplays() {
		waitForControl(driver, Interfaces.FNFDocumentsPage.RECORD_SAVED_MESSAGE, timeWait);
		return isControlDisplayed(driver,Interfaces.FNFDocumentsPage.RECORD_SAVED_MESSAGE);
	}
	
	/**
	 * check Document History Loaded To Document File, Submit Via, Submitted On, Previous Status, Status
	 */
	public boolean isDocumentHistoryLoadedToDocumentType(String documentFile, String submitVia, String submitOn, String previousStatus, String status) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.HISTORY_DYNAMIC_DOCUMENT_LOADED_FOR_DOCTYPE, documentFile, submitVia, submitOn, previousStatus, status, timeWait);
		Boolean result;
		result = isControlDisplayed(driver, Interfaces.FNFDocumentsPage.HISTORY_DYNAMIC_DOCUMENT_LOADED_FOR_DOCTYPE, documentFile, submitVia, submitOn, previousStatus, status);
		return result;
	}
	
	/**
	 * check Document History Loaded To No Document File, Submit Via, Submitted On, Previous Status, Status
	 */
	public boolean isNoDocumentHistoryLoadedToDocumentType(String submitVia, String submitOn, String previousStatus, String status) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.HISTORY_DYNAMIC_NO_DOCUMENT_LOADED_FOR_DOCTYPE, submitVia, submitOn, previousStatus, status, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFDocumentsPage.HISTORY_DYNAMIC_NO_DOCUMENT_LOADED_FOR_DOCTYPE, submitVia, submitOn, previousStatus, status);
	}
	
	/**
	 * click 'Document File Name' detail
	 */
	public void clickDocumentFileNameDetail(String submitVia, String documentFile) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.DYNAMIC_DOCUMENT_FILE_NAME_DETAIL_PLACE_HOLDER, submitVia, documentFile, timeWait);
		click(driver, Interfaces.FNFDocumentsPage.DYNAMIC_DOCUMENT_FILE_NAME_DETAIL_PLACE_HOLDER, submitVia, documentFile);
		sleep();
	}
	
	/**
	 * check Document Viewable
	 */
	public boolean isDocumentViewable(String fileName) {
		sleep(3);
		String currentHandle = driver.getWindowHandle();
		openWindowHandles.push(driver.getWindowHandle());
		Set<String> newHandles = driver.getWindowHandles();
		newHandles.removeAll(openWindowHandles);
		String handle = newHandles.iterator().next();
		driver.switchTo().window(handle);
		String url = getCurrentUrl(driver);
		boolean exists = url.replace("%20"," ").contains(fileName);
		driver.close();
		driver.switchTo().window(currentHandle);
		return exists;
	}
	
	/**
	 * click 'Back to Applicant' button
	 * @return FNF Loan Page
	 */
	public FNFLoansPage clickBackToLoanButton() {
		waitForControl(driver, Interfaces.FNFDocumentsPage.BACK_TO_LOAN_BUTTON,	timeWait);
		executeJavaScript(driver, "document.getElementById('BackToOpportunity').click();");
		sleep(1);
		return PageFactory.getFNFLoansPage(driver, ipClient);
	}
	
	/** 
	 * Open Placeholders
	 */
	public void openPlaceholders(String name){
		waitForControl(driver, Interfaces.FNFDocumentsPage.DYNAMIC_PLACEHOLDER, name, timeWait);
		click(driver, Interfaces.FNFDocumentsPage.DYNAMIC_PLACEHOLDER, name);
	}
	
	/**
	 * change Document File
	 */
	public void changeDocumentFile(String fileName1, String fileName2) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.DYNAMIC_CHANGE_FILE_BUTTON, fileName1, timeWait);
		doubleClick(driver, Interfaces.FNFDocumentsPage.DYNAMIC_CHANGE_FILE_BUTTON, fileName1);
		sleep(1);
		Common.getCommon().openFileForUpload(driver, fileName2);
		sleep();
	}
	
	/** 
	 * Open history popup box
	 */
	public void openHistoryPopupBox(String name){
		waitForControl(driver, Interfaces.FNFDocumentsPage.HISTORY_BUTTON, name, timeWait);
		click(driver, Interfaces.FNFDocumentsPage.HISTORY_BUTTON, name);
	}
	
	/** 
	 * Close history popup box
	 */
	public void closeHistoryPopupBox(){
		sleep();
		keyPressing("esc");
//		waitForControl(driver, Interfaces.FNFDocumentsPage.CLOSE_HISTORY_POPUP_BUTTON, timeWait);
//		click(driver, Interfaces.FNFDocumentsPage.CLOSE_HISTORY_POPUP_BUTTON);
	}
	
	/**
	 * Switch driver to iFrame
	 * @return driver
	 */
	public WebDriver switchToHistoryPopupFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.HISTORY_IFRAME, timeWait);
		sleep(5);
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.FNFDocumentsPage.HISTORY_IFRAME)));
		return driver;
	}
	
	/**
	 * save Document type Button
	 */
	public FNFLoansPage clickSaveDocumentType() {
		waitForControl(driver, Interfaces.FNFDocumentsPage.SAVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
          sleep(10);
     }
		sleep(5);
		isRecordSavedMessageDisplays();
		return PageFactory.getFNFLoansPage(driver, ipClient);
	}
	
	/**
	 * save Document type Button
	 */
	public Boolean isDateOfLastChangeDisplayed(){
		waitForControl(driver, Interfaces.FNFDocumentsPage.DATE_OF_LAST_CHANGE_TEXT, timeWait);
		return !getText(driver, Interfaces.FNFDocumentsPage.DATE_OF_LAST_CHANGE_TEXT).equals("");
	}
	
	/**
	 * save Document type Button
	 */
	public Boolean isStatusChangedByTextDisplayed(){
		waitForControl(driver, Interfaces.FNFDocumentsPage.STATUS_CHANGED_BY_TEXT, timeWait);
		return !getText(driver, Interfaces.FNFDocumentsPage.STATUS_CHANGED_BY_TEXT).equals(" ");
	}
	
	/**
	 * click 'Back to Applicant' button
	 * @return FNF Loan Page
	 */
	public FNFPropertiesPage clickBackToProperty() {
		waitForControl(driver, Interfaces.FNFDocumentsPage.BACK_TO_PROPERTY_BUTTON,	timeWait);
		executeJavaScript(driver, "document.getElementById('BackToProperty').click();");
		sleep(1);
		return PageFactory.getFNFPropertiesPage(driver, ipClient);
	}
	
	/**
	 * upload Document File
	 */
	public void uploadDocumentFileForFolderByPlaceHolder(String fileName) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.CHOOSE_FILE_FOR_FOLDERS_BUTTON, timeWait);
		doubleClick(driver, Interfaces.FNFDocumentsPage.CHOOSE_FILE_FOR_FOLDERS_BUTTON);
		sleep(1);
		Common.getCommon().openFileForUpload(driver, fileName);
		sleep();
	}
	
	/**
	 * upload Document File
	 */
	public void uploadDocumentFileForFolder(String fileName1, String fileName2, String fileName3) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.CHOOSE_MULTI_FILES_FOR_FOLDERS_BUTTON, timeWait);
		doubleClick(driver, Interfaces.FNFDocumentsPage.CHOOSE_MULTI_FILES_FOR_FOLDERS_BUTTON);
		sleep(1);
		Common.getCommon().openFileForUpload(driver, fileName1, fileName2, fileName3);
		sleep();
	}
	
	/**
	 * check loaded Document files displayed correctly
	 */
	public boolean isDocumentLoadedToDocumentType(String fileName) {
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENT_FILE_IN_DOCUMENT_FOLDER, fileName,  timeWait);
		return isControlDisplayed(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENT_FILE_IN_DOCUMENT_FOLDER, fileName);
	}
	
	/**
	 * input comment
	 */
	public void inputComments(String fileName, String text) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.DYNAMIC_COMMENT_TEXTAREA, fileName, timeWait);
		type(driver, Interfaces.FNFDocumentsPage.DYNAMIC_COMMENT_TEXTAREA, fileName, text);
	}
	
	/**
	 * open Loan documents tab
	 */
	public void openLoanDocumentsTab() {
		waitForControl(driver, Interfaces.FNFDocumentsPage.LOAN_DOCUMENTS_TAB, timeWait);
		executeClick(driver, Interfaces.FNFDocumentsPage.LOAN_DOCUMENTS_TAB);
	}
	
	/**
	 * open Property documents tab
	 */
	public void openPropertyDocumentsTab() {
		waitForControl(driver, Interfaces.FNFDocumentsPage.PROPERTY_DOCUMENTS_TAB, timeWait);
		executeClick(driver, Interfaces.FNFDocumentsPage.PROPERTY_DOCUMENTS_TAB);
	}
	
	/**
	 * click On 'Search'Loans button
	 */
	public void clickOnSearchLoansButton(String loanName) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.SEARCH_LOANS_NAME_TEXTFIELD, timeWait);
		type(driver, Interfaces.FNFDocumentsPage.SEARCH_LOANS_NAME_TEXTFIELD, loanName);
		sleep(1);
		waitForControl(driver, Interfaces.FNFDocumentsPage.SEARCH_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('DocumentSearchOpportunity').click();");
		sleep(1);
	}
	
	/**
	 * click On 'Search' Property button
	 */
	public void clickOnSearchPropertyButton(String propertyAddress) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.SEARCH_PROPERTY_ADDRESS_TEXTFIELD, timeWait);
		type(driver, Interfaces.FNFDocumentsPage.SEARCH_PROPERTY_ADDRESS_TEXTFIELD, propertyAddress);
		sleep(1);
		waitForControl(driver, Interfaces.FNFDocumentsPage.SEARCH_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('DocumentSearchOpportunity').click();");
		sleep(4);
	}
	
	/**
	 * click On 'Search' Property button
	 */
	public void clickOnSearchPropertyButton() {
		waitForControl(driver, Interfaces.FNFDocumentsPage.SEARCH_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('DocumentSearchOpportunity').click();");
		sleep(5);
	}
	
	
	/**
	 * get Field index
	 */
	public String getIndexOfField(String fieldName){
		waitForControl(driver,	Interfaces.FNFDocumentsPage.FNF_DOCUMENTS_SEARCH_HEADER, timeWait);
		int numbersOfField = countElement(driver, Interfaces.FNFDocumentsPage.FNF_DOCUMENTS_SEARCH_HEADER);
		for(int i=2;i<=numbersOfField; i++){
			String index = Integer.toString(i);
			if(getText(driver,	Interfaces.FNFDocumentsPage.DYNAMIC_FNF_DOCUMENTS_SEARCH_HEADER, index).equals(fieldName)) return index;
		}
		return null;
	}
	
	/**
	 * get Field text
	 */
	public String getRecordValue(String fieldName){
		String index = getIndexOfField(fieldName);
		waitForControl(driver,	Interfaces.FNFDocumentsPage.DYNAMIC_FNF_DOCUMENTS_SEARCH_RESULT, index, timeWait);
		return getText(driver,	Interfaces.FNFDocumentsPage.DYNAMIC_FNF_DOCUMENTS_SEARCH_RESULT, index);
	}
	
	/**
	 * check record displayed
	 */
	public Boolean isRecordValueDisplayedCorrectly(String fieldName, String value){
		String index = getIndexOfField(fieldName);
		waitForControl(driver,	Interfaces.FNFDocumentsPage.DYNAMIC_FNF_DOCUMENTS_SEARCH_RESULT, index, 10);
		if (!isControlDisplayed(driver,	Interfaces.FNFDocumentsPage.DYNAMIC_FNF_DOCUMENTS_SEARCH_RESULT, index)) return false;
		return getText(driver,	Interfaces.FNFDocumentsPage.DYNAMIC_FNF_DOCUMENTS_SEARCH_RESULT, index).trim().contains(value.trim());
	}
	
	/**
	 * check record displayed
	 */
	public Boolean isAllLoansDocumentsRecordValuesDisplayedCorrectly(String loanName, String documentFolder, String documentFileName, String lastUpoadDate, String lastUploadBy, String lastComments){
		sleep(2);
		return (isRecordValueDisplayedCorrectly("Loan Name",loanName) 
				&& isRecordValueDisplayedCorrectly("Document Folder",documentFolder) 
				&& isRecordValueDisplayedCorrectly("Document Name",documentFileName) 
				&& isRecordValueDisplayedCorrectly("Last Upload Date",lastUpoadDate) 
				&& isRecordValueDisplayedCorrectly("Last Upload By",lastUploadBy)
				&& isRecordValueDisplayedCorrectly("Last Comment",lastComments));
	}
	
	/**
	 * check record displayed
	 */
	public Boolean isAllPropertyDocumentsRecordValuesDisplayedCorrectly(String documentsIndex, String documentFolder, String lastUpoadDate, String lastUploadBy, String lastComments, String status, String active, String addressApplicant){
		sleep(2);
		return 	(isRecordValueDisplayedCorrectly("Document #",documentsIndex) 
				&& isRecordValueDisplayedCorrectly("Document",documentFolder) 
				&& isRecordValueDisplayedCorrectly("Last Modified On",lastUpoadDate) 
				&& isRecordValueDisplayedCorrectly("Modified By",lastUploadBy)
				&& isRecordValueDisplayedCorrectly("Comment",lastComments)
				&& isRecordValueDisplayedCorrectly("Status", status) 
				&& isRecordValueDisplayedCorrectly("Active", active)
				&& isRecordValueDisplayedCorrectly("Property", addressApplicant));
	}
	
	/**
	 * click On 'Search'Loans button
	 */
	public void inputUploadDate(String value) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.SEARCH_UPLOAD_DATE_TEXTFIELD, timeWait);
		type(driver, Interfaces.FNFDocumentsPage.SEARCH_UPLOAD_DATE_TEXTFIELD, value);
	}
	
	/**
	 * click On 'Search'Loans button
	 */
	public void inputUploadBy(String value) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.SEARCH_UPLOAD_BY_TEXTFIELD, timeWait);
		type(driver, Interfaces.FNFDocumentsPage.SEARCH_UPLOAD_BY_TEXTFIELD, value);
	}
	
	/**
	 * select Active radio button
	 */
	public void selectActiveRadioButton(String status) {
		waitForControl(driver,	Interfaces.FNFDocumentsPage.SEARCH_UPLOAD_BY_TEXTFIELD, timeWait);
		if(status.contains("Yes")) 
		click(driver, Interfaces.ApplicantsPage.DYNAMIC_FNF_BORROWER_SEARCH_ACTIVE_RADIO_BUTTON, "1");
		else if(status.contains("No")) click(driver, Interfaces.ApplicantsPage.DYNAMIC_FNF_BORROWER_SEARCH_ACTIVE_RADIO_BUTTON, "0");
		else click(driver, Interfaces.ApplicantsPage.DYNAMIC_FNF_BORROWER_SEARCH_ACTIVE_RADIO_BUTTON, "");
	}
	
	/**
	 * Input Document index
	 */
	public void inputDocumentIndex(String value) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.SEARCH_PROPERTY_DOCUMENT_INDEX_TEXTFIELD, timeWait);
		type(driver, Interfaces.FNFDocumentsPage.SEARCH_PROPERTY_DOCUMENT_INDEX_TEXTFIELD, value);
	}
	
	/**
	 * Input Document folder name
	 */
	public void inputDocumentFolderName(String value) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.SEARCH_PROPERTY_DOCUMENT_FOLDER_TEXTFIELD, timeWait);
		type(driver, Interfaces.FNFDocumentsPage.SEARCH_PROPERTY_DOCUMENT_FOLDER_TEXTFIELD, value);
	}
	
	/**
	 * Input Borrower name
	 */
	public void inputBorrowerName(String value) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.SEARCH_PROPERTY_DOCUMENT_BORROWER_TEXTFIELD, timeWait);
		type(driver, Interfaces.FNFDocumentsPage.SEARCH_PROPERTY_DOCUMENT_BORROWER_TEXTFIELD, value);
	}
	
	/**
	 * Input Lender name
	 */
	public void inputLenderName(String value) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.SEARCH_PROPERTY_DOCUMENT_LENDER_TEXTFIELD, timeWait);
		type(driver, Interfaces.FNFDocumentsPage.SEARCH_PROPERTY_DOCUMENT_LENDER_TEXTFIELD, value);
	}
	
	/**
	 * Input Last modified date
	 */
	public void inputLastModifiedDate(String value) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.SEARCH_PROPERTY_DOCUMENT_LAST_MODIFIED_TEXTFIELD, timeWait);
		type(driver, Interfaces.FNFDocumentsPage.SEARCH_PROPERTY_DOCUMENT_LAST_MODIFIED_TEXTFIELD, value);
	}
	
	/**
	 * Input Modified by
	 */
	public void inputModifiedBy(String value) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.SEARCH_PROPERTY_DOCUMENT_MODIFIED_BY_TEXTFIELD, timeWait);
		type(driver, Interfaces.FNFDocumentsPage.SEARCH_PROPERTY_DOCUMENT_MODIFIED_BY_TEXTFIELD, value);
	}
	
	/**
	 * select Document status
	 */
	public void selectDocumentStatus(String documentStatus) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.SEARCH_PROPERTY_DOCUMENT_STATUS_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.FNFDocumentsPage.SEARCH_PROPERTY_DOCUMENT_STATUS_DROPDOWN, documentStatus);
		sleep(1);
	}
	
	/**
	 * click on check all link
	 */
	public void clickOnCheckAllLink() {
		waitForControl(driver,	Interfaces.ApplicantsPage.CHECK_ALL_LINK, timeWait);
		click(driver, Interfaces.ApplicantsPage.CHECK_ALL_LINK);
		sleep(1);
	}
	
	/**
	 * click Make Inactive Button
	 */
	public void clickMakeInactiveButton() {
		waitForControl(driver, Interfaces.ApplicantsPage.MAKE_INACTIVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('SetInactive').click();");
		sleep();
		acceptJavascriptAlert(driver);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * click Make Active Button 
	 */
	public void clickMakeActiveButton() {
		waitForControl(driver, Interfaces.ApplicantsPage.MAKE_ACTIVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('SetActive').click();");
		sleep();
		acceptJavascriptAlert(driver);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}
	
	private WebDriver driver;
	private String ipClient;
	private final Stack<String> openWindowHandles = new Stack<String>();
}