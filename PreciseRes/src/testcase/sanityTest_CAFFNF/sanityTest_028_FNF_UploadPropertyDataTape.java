package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFLoansPage;
import page.FNFPropertiesPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;

import common.AbstractTest;
import common.Constant;

public class sanityTest_028_FNF_UploadPropertyDataTape extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		loanName = "Edit Loan Auto " + getUniqueNumber();
		companyName = "Edit_Borrower " + accountNumber;
		city = "Los Angeles";
		state = "CA - California";
		zip = "90210";
		date = "10/20/2001";
		number = "4";
		price = "$85,000.00";
		propertyType ="SFR";
		transactionType ="Purchase";
		titleCompany= "Title Company Test "+getUniqueNumber();
		escrowAgent = "Escrow Company Test "+getUniqueNumber();
		isGlobal = "No";
		fileName = "CAFFNF Data tape.xlsx";
		
		propertyAddress1 = "101 Sunny Lane";
		propertyAddress2 = "102 Sunny Lane";
		propertyAddress3 = "103 Sunny Lane";
		propertyAddress4 = "104 Sunny Lane";
		propertyAddress5 = "105 Sunny Lane";
		propertyAddress6 = "106 Sunny Lane";
		
		titleContactName1 = "FirstName1 LastName1";
		titleContactName2 = "FirstName2 LastName2";
		titleContactName3 = "FirstName3 LastName3";
		titleContactName4 = "FirstName3 LastName3";
		titleContactName5 = "FirstName3 LastName3";
		titleContactName6 = "LastName3";
		
		titlePhoneNumber1 = "Title Contact Phone 1";
		titlePhoneNumber2 = "Title Contact Phone 2";
		titlePhoneNumber3 = "Title Contact Phone 2";
		titlePhoneNumber4 = "Title Contact Phone 3";
		titlePhoneNumber5 = "Title Contact Phone 3";
		titlePhoneNumber6 = "Title Contact Phone 3";
		
		escrowContactName1 = "FirstName1 LastName1";
		escrowContactName2 = "FirstName2 LastName2";
		escrowContactName3 = "FirstName3 LastName3";
		escrowContactName4 = "FirstName3 LastName3";
		escrowContactName5 = "FirstName3 LastName3";
		escrowContactName6 = "LastName3";
		
		escrowPhoneNumber1 = "Escrow Contact Phone 1";
		escrowPhoneNumber2 = "Escrow Contact Phone 2";
		escrowPhoneNumber3 = "Escrow Contact Phone 2";
		escrowPhoneNumber4 = "Escrow Contact Phone 3";
		escrowPhoneNumber5 = "Escrow Contact Phone 3";
		escrowPhoneNumber6 = "Escrow Contact Phone 3";
		
	}

	@Test(groups = { "regression" }, description = "Lender - Login as Lender - Check data tape Uploaded correctly")
	public void UploadPropertyDataTape_01_Lender_CheckDataTapeUploadedCorrectly() {

		log.info("Precondition - Step 01. Logout and login with Lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("Precondition - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("Precondition - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("Precondition - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
	
		log.info("Precondition - Step 05: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("Precondition - Step 06: Input loan name");
		loanPage.inputRequiredData(loanName, date);
		
		log.info("Precondition - Step 07: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("UploadPropertyDataTape_01 - Step 01: Go to 'Properties' page");
		propertiesPage = loanPage.openPropertiesTab();
		
		log.info("VP: Verify that 'Add Properties Button' displayed");
		verifyTrue(propertiesPage.isAddPropertyButtonDisplayed());
		
		log.info("VP: Verify that 'Import Properties Button' displayed");
		verifyTrue(propertiesPage.isImportPropertiesButtonDisplayed());
		
		log.info("VP: Verify that 'Make Property Active' button displayed");
		verifyTrue(propertiesPage.isMenuButtonDisplayed(driver, "Make Property Active"));
		
		log.info("VP: Verify that 'Make Property Inactive' button displayed");
		verifyTrue(propertiesPage.isMenuButtonDisplayed(driver, "Make Property Inactive"));
		
		log.info("UploadPropertyDataTape_01 - Step 02: Upload property data tape");
		propertiesPage.editDataTapeFile(fileName, "Data Tape", "Title Company Test", titleCompany);
		propertiesPage.editDataTapeFile(fileName, "Data Tape", "Escrow Company Test", escrowAgent);
		propertiesPage.uploadFileProperties(fileName);
		
		log.info("VP: Basic detail message title display correctly");
		verifyTrue(propertiesPage.isBasicDetailMessageTitleDisplay("Import has been done"));
		
		log.info("UploadPropertyDataTape_01 - Step 01: Go to 'Properties' page");
		propertiesPage.openPropertyDetail(propertyAddress1);
		
		log.info("VP: Verify that all required information is uploaded successfully");
		verifyTrue(propertiesPage.isApplicantPropertiesInformationUploaded(loanName, companyName, propertyAddress1, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent));
	}
	
	@Test(groups = { "regression" }, description = "Lender - Login as Lender - Check Vendor created correctly")
	public void UploadPropertyDataTape_02_Lender_CheckVendorCreateNew() {

		log.info("UploadPropertyDataTape_02 - Step 01: Go to 'Title company Vendor details' page");
		propertiesPage.clickEditTitleCompanyVendor();
		
		log.info("VP: Verify that title company vendor created correctly");
		verifyTrue(propertiesPage.isVendorInformationCorrect(titleCompany, titleContactName1, titlePhoneNumber1, isGlobal, "0"));
		
		log.info("UploadPropertyDataTape_02 - Step 02: Go back to Property details");
		propertiesPage.clickBackToProperty();
		
		log.info("UploadPropertyDataTape_02 - Step 03: Go to 'Title company Vendor details' page");
		propertiesPage.clickEditEscrowCompanyVendor();
		
		log.info("VP: Verify that Escrow Agent vendor created correctly");
		verifyTrue(propertiesPage.isVendorInformationCorrect(escrowAgent, escrowContactName1, escrowPhoneNumber1, isGlobal, "0"));
	
		log.info("UploadPropertyDataTape_02 - Step 04: Go back to Property details");
		propertiesPage.clickBackToProperty();
	}
	
	@Test(groups = { "regression" }, description = "Lender - Login as Lender - Check Vendor editted correctly")
	public void UploadPropertyDataTape_03_Lender_CheckVendorEdittedCorrectly() {

		log.info("UploadPropertyDataTape_03 - Step 01: Go to Loan Properties list");
		propertiesPage.clickGoBackToLoanProperties();
		
		//Check Vendor editted corecttly when update with Different contact name and phone, same company name
		
		log.info("UploadPropertyDataTape_03 - Step 02: Go to 'Properties' page");
		propertiesPage.openPropertyDetail(propertyAddress2);
		
		log.info("VP: Verify that all required information is uploaded successfully");
		verifyTrue(propertiesPage.isApplicantPropertiesInformationUploaded(loanName, companyName, propertyAddress2, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent));
	
		log.info("UploadPropertyDataTape_03 - Step 03: Go to 'Title company Vendor details' page");
		propertiesPage.clickEditTitleCompanyVendor();
		
		log.info("VP: Verify that title company vendor created correctly");
		verifyTrue(propertiesPage.isVendorInformationCorrect(titleCompany, titleContactName2, titlePhoneNumber2, isGlobal, "1"));
		
		log.info("UploadPropertyDataTape_03 - Step 04: Go back to Property details");
		propertiesPage.clickBackToProperty();
		
		log.info("UploadPropertyDataTape_03 - Step 05: Go to 'Title company Vendor details' page");
		propertiesPage.clickEditEscrowCompanyVendor();
		
		log.info("VP: Verify that title company vendor created correctly");
		verifyTrue(propertiesPage.isVendorInformationCorrect(escrowAgent, escrowContactName2, escrowPhoneNumber2, isGlobal, "1"));
	
		log.info("UploadPropertyDataTape_03 - Step 06: Go back to Property details");
		propertiesPage.clickBackToProperty();
		
		log.info("UploadPropertyDataTape_03 - Step 07: Go to Loan Properties list");
		propertiesPage.clickGoBackToLoanProperties();
		
		//Check Vendor editted corecttly when update with Different contact name,  same company name and phone
		
		log.info("UploadPropertyDataTape_03 - Step 08: Go to 'Properties' page");
		propertiesPage.openPropertyDetail(propertyAddress3);
		
		log.info("VP: Verify that all required information is uploaded successfully");
		verifyTrue(propertiesPage.isApplicantPropertiesInformationUploaded(loanName, companyName, propertyAddress3, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent));
	
		log.info("UploadPropertyDataTape_03 - Step 09: Go to 'Title company Vendor details' page");
		propertiesPage.clickEditTitleCompanyVendor();
		
		log.info("VP: Verify that title company vendor created correctly");
		verifyTrue(propertiesPage.isVendorInformationCorrect(titleCompany, titleContactName3, titlePhoneNumber3, isGlobal, "2"));
		
		log.info("UploadPropertyDataTape_03 - Step 10: Go back to Property details");
		propertiesPage.clickBackToProperty();
		
		log.info("UploadPropertyDataTape_03 - Step 11: Go to 'Title company Vendor details' page");
		propertiesPage.clickEditEscrowCompanyVendor();
		
		log.info("VP: Verify that title company vendor created correctly");
		verifyTrue(propertiesPage.isVendorInformationCorrect(escrowAgent, escrowContactName3, escrowPhoneNumber3, isGlobal, "2"));
	
		log.info("UploadPropertyDataTape_03 - Step 12: Go back to Property details");
		propertiesPage.clickBackToProperty();
		
		log.info("UploadPropertyDataTape_03 - Step 13: Go to Loan Properties list");
		propertiesPage.clickGoBackToLoanProperties();
		
		//Check Vendor editted corecttly when update with Different phone,  same company name and contact name
		
		log.info("UploadPropertyDataTape_03 - Step 14: Go to 'Properties' page");
		propertiesPage.openPropertyDetail(propertyAddress4);
		
		log.info("VP: Verify that all required information is uploaded successfully");
		verifyTrue(propertiesPage.isApplicantPropertiesInformationUploaded(loanName, companyName, propertyAddress4, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent));
	
		log.info("UploadPropertyDataTape_03 - Step 15: Go to 'Title company Vendor details' page");
		propertiesPage.clickEditTitleCompanyVendor();
		
		log.info("VP: Verify that title company vendor created correctly");
		verifyTrue(propertiesPage.isVendorInformationCorrect(titleCompany, titleContactName4, titlePhoneNumber4, isGlobal, "3"));
		
		log.info("UploadPropertyDataTape_03 - Step 16: Go back to Property details");
		propertiesPage.clickBackToProperty();
		
		log.info("UploadPropertyDataTape_03 - Step 17: Go to 'Title company Vendor details' page");
		propertiesPage.clickEditEscrowCompanyVendor();
		
		log.info("VP: Verify that title company vendor created correctly");
		verifyTrue(propertiesPage.isVendorInformationCorrect(escrowAgent, escrowContactName4, escrowPhoneNumber4, isGlobal, "3"));
	
		log.info("UploadPropertyDataTape_03 - Step 18: Go back to Property details");
		propertiesPage.clickBackToProperty();
		
		log.info("UploadPropertyDataTape_03 - Step 19: Go to Loan Properties list");
		propertiesPage.clickGoBackToLoanProperties();
		
		//Check Vendor editted corecttly when update with Same company name, contact name and phone
		
		log.info("UploadPropertyDataTape_03 - Step 20: Go to 'Properties' page");
		propertiesPage.openPropertyDetail(propertyAddress5);
		
		log.info("VP: Verify that all required information is uploaded successfully");
		verifyTrue(propertiesPage.isApplicantPropertiesInformationUploaded(loanName, companyName, propertyAddress5, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent));
	
		log.info("UploadPropertyDataTape_03 - Step 21: Go to 'Title company Vendor details' page");
		propertiesPage.clickEditTitleCompanyVendor();
		
		log.info("VP: Verify that title company vendor created correctly");
		verifyTrue(propertiesPage.isVendorInformationCorrect(titleCompany, titleContactName5, titlePhoneNumber5, isGlobal, "3"));
		
		log.info("UploadPropertyDataTape_03 - Step 22: Go back to Property details");
		propertiesPage.clickBackToProperty();
		
		log.info("UploadPropertyDataTape_03 - Step 23: Go to 'Title company Vendor details' page");
		propertiesPage.clickEditEscrowCompanyVendor();
		
		log.info("VP: Verify that title company vendor created correctly");
		verifyTrue(propertiesPage.isVendorInformationCorrect(escrowAgent, escrowContactName5, escrowPhoneNumber5, isGlobal, "3"));
	
		log.info("UploadPropertyDataTape_03 - Step 24: Go back to Property details");
		propertiesPage.clickBackToProperty();
		
		log.info("UploadPropertyDataTape_03 - Step 01: Go to Loan Properties list");
		propertiesPage.clickGoBackToLoanProperties();
		
		//Check Vendor editted corecttly when update with Contact name only have Last Name
		
		log.info("UploadPropertyDataTape_03 - Step 25: Go to 'Properties' page");
		propertiesPage.openPropertyDetail(propertyAddress6);
		
		log.info("VP: Verify that all required information is uploaded successfully");
		verifyTrue(propertiesPage.isApplicantPropertiesInformationUploaded(loanName, companyName, propertyAddress6, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent));
	
		log.info("UploadPropertyDataTape_03 - Step 26: Go to 'Title company Vendor details' page");
		propertiesPage.clickEditTitleCompanyVendor();
		
		log.info("VP: Verify that title company vendor created correctly");
		verifyTrue(propertiesPage.isVendorInformationCorrect(titleCompany, titleContactName6, titlePhoneNumber6, isGlobal, "4"));
		
		log.info("UploadPropertyDataTape_03 - Step 27: Go back to Property details");
		propertiesPage.clickBackToProperty();
		
		log.info("UploadPropertyDataTape_03 - Step 28: Go to 'Title company Vendor details' page");
		propertiesPage.clickEditEscrowCompanyVendor();
		
		log.info("VP: Verify that title company vendor created correctly");
		verifyTrue(propertiesPage.isVendorInformationCorrect(escrowAgent, escrowContactName6, escrowPhoneNumber6, isGlobal, "4"));
	
		log.info("UploadPropertyDataTape_03 - Step 29: Go back to Property details");
		propertiesPage.clickBackToProperty();
		
		log.info("UploadPropertyDataTape_03 - Step 30: Go to Loan Properties list");
		propertiesPage.clickGoBackToLoanProperties();
		
	}
	
	
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFPropertiesPage propertiesPage;
	private FNFBorrowersPage applicantsPage;
	private FNFLoansPage loanPage;
	private String FNFLenderUsername, FNFLenderPassword;
	private String city, state, zip, companyName, date, number, price;
	private String propertyType, transactionType, titleCompany, escrowAgent;
	private String accountNumber, loanName, fileName, isGlobal;
	private String propertyAddress1, propertyAddress2, propertyAddress3, propertyAddress4, propertyAddress5, propertyAddress6;
	private String titleContactName1, titleContactName2, titleContactName3, titleContactName4, titleContactName5, titleContactName6;
	private String titlePhoneNumber1, titlePhoneNumber2, titlePhoneNumber3, titlePhoneNumber4, titlePhoneNumber5, titlePhoneNumber6;
	private String escrowContactName1, escrowContactName2, escrowContactName3, escrowContactName4, escrowContactName5, escrowContactName6;
	private String escrowPhoneNumber1, escrowPhoneNumber2, escrowPhoneNumber3, escrowPhoneNumber4, escrowPhoneNumber5, escrowPhoneNumber6;
}