package page;

import org.openqa.selenium.WebDriver;

import PreciseRes.Interfaces;

public class MailLoginPage extends AbstractPage {

	public MailLoginPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
	}
	
	/**
	 * Login to Gmail
	 * 
	 * @param username
	 * @param password
	 */
	public void loginToGmail(String username, String password) {
		waitForControl(driver, Interfaces.MailLoginPage.EMAIL_TEXTBOX, timeWait);
		type(driver, Interfaces.MailLoginPage.EMAIL_TEXTBOX, username);
		sleep();
		click(driver, Interfaces.MailLoginPage.NEXT_BUTTON);
		sleep();
		type(driver, Interfaces.MailLoginPage.PASSWORD_TEXTBOX, password);
		sleep();
		uncheckTheCheckbox(driver, Interfaces.MailLoginPage.STAY_SIGNED_IN_CHECKBOX);
		sleep();
	}
	
	/**
	 * Click Submit button
	 * @return MailHomePage
	 */
	public MailHomePage clickSubmitButton(WebDriver driver, String ipClient)
	{
		waitForControl(driver, Interfaces.MailLoginPage.SIGN_IN_BUTTON, timeWait);
		click(driver,Interfaces.MailLoginPage.SIGN_IN_BUTTON);
		sleep();
		return PageFactory.getMailHomePage(driver, ipClient);
	}
	
	/**
	 * click Skip button
	 */
	public void clickSkipButton(){
		waitForControl(driver, Interfaces.MailLoginPage.SKIP_BUTTON, timeWait);
		click(driver,Interfaces.MailLoginPage.SKIP_BUTTON);
		sleep(5);
	}

	private WebDriver driver;
}