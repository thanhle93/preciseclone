package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFLoansPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_031_FNF_CalculationLoanDetailsPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		loanName = "Edit Loan Auto " + getUniqueNumber();
		underwriter = "CAF FNF Lender";
		locClosingDate = "2/12/2014";
		validNumber ="10";
		originator = "CAF FNF Lender";
	}

	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Checking fields are read-only")
	public void CalculationLoanDetailsPage_01_Lender_CheckingReadOnlyField() {
		log.info("CalculationLoanDetailsPage_01 - Step 01. Login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);

		log.info("CalculationLoanDetailsPage_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("CalculationLoanDetailsPage_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("CalculationLoanDetailsPage_01 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("CalculationLoanDetailsPage_01 - Step 05: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("CalculationLoanDetailsPage_01 - Step 06: Input loan name");
		loanPage.inputRequiredData(loanName, locClosingDate);
		
		log.info("CalculationLoanDetailsPage_01 - Step 07: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("VP: Current Close Date textfield is read-only");
		verifyTrue(loanPage.isTextfieldReadOnly("Current Close Date"));
		
		log.info("VP: Original Line Maturity Date textfield is read-only");
		verifyTrue(loanPage.isTextfieldReadOnly("Original Line Maturity Date"));
		
		log.info("VP: Current Close Date textfield is read-only");
		verifyTrue(loanPage.isTextfieldReadOnly("Current Line Maturity Date"));
		
		log.info("VP: Current Close Date textfield is read-only");
		verifyTrue(loanPage.isTextfieldReadOnly("Line Mod Date"));
		
		log.info("VP: Current Close Date textfield is read-only");
		verifyTrue(loanPage.isTextfieldReadOnly("Most Recent Line Mod Date"));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Calculate for date field")
	public void CalculationLoanDetailsPage_02_Lender_CalculateForDateFields() {

		log.info("VP: Current Close Date textfield take value from Loc Closing date");
		verifyTrue(loanPage.isTextfieldDisplayedCorrectly("Current Close Date", locClosingDate));
	
		log.info("CalculationLoanDetailsPage_02 - Step 01: Select Borrower and click on Search button");
		loanPage.selectItemFromDropdown("Term", "12 months");
		
		log.info("CalculationLoanDetailsPage_02 - Step 02: Calculation for Original Line Mature Date");
		originalLineMaturityDate= loanPage.addDateTimeByMonth(locClosingDate, 12);
		
		log.info("CalculationLoanDetailsPage_01 - Step 03: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("VP: Original Line Maturity Date textfield is displayed correctly");
		verifyTrue(loanPage.isReadOnlyTextfieldDisplayCorrectly("Original Line Maturity Date", originalLineMaturityDate));
		
		log.info("VP: Current Line Maturity Date Close Date textfield take value from Original Line Mature Date");
		verifyTrue(loanPage.isReadOnlyTextfieldDisplayCorrectly("Current Line Maturity Date", originalLineMaturityDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Checking fields are read-only")
	public void CalculationLoanDetailsPage_03_Borrower_CheckingReadOnlyField() {

		log.info("CalculationLoanDetailsPage_03 - Step 01. Logout and login with Borrower");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);
	
		log.info("CalculationLoanDetailsPage_03 - Step 02: Open 'Loan Name'");
		loanPage = applicantsPage.openLoanNameInOpportunityTable(loanName);

		log.info("VP: Current Close Date textfield is read-only");
		verifyTrue(loanPage.isTextfieldReadOnly("Current Close Date"));
		
		log.info("VP: Original Line Maturity Date textfield is read-only");
		verifyTrue(loanPage.isTextfieldReadOnly("Original Line Maturity Date"));
		
		log.info("VP: Current Close Date textfield is read-only");
		verifyTrue(loanPage.isTextfieldReadOnly("Current Line Maturity Date"));
		
		log.info("VP: Current Close Date textfield is read-only");
		verifyTrue(loanPage.isTextfieldReadOnly("Line Mod Date"));
		
		log.info("VP: Current Close Date textfield is read-only");
		verifyTrue(loanPage.isTextfieldReadOnly("Most Recent Line Mod Date"));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Calculate for date field")
	public void CalculationLoanDetailsPage_03_Borrower_CalculateForDateFields() {

		log.info("VP: Current Close Date textfield take value from Loc Closing date");
		verifyTrue(loanPage.isTextfieldDisplayedCorrectly("Current Close Date", locClosingDate));
	
		log.info("CalculationLoanDetailsPage_03 - Step 01: Select Borrower and click on Search button");
		loanPage.selectItemFromDropdown("Term", "12 months");
		
		log.info("CalculationLoanDetailsPage_03 - Step 02: Calculation for Original Line Mature Date");
		originalLineMaturityDate= loanPage.addDateTimeByMonth(locClosingDate, 12);
		
		log.info("CalculationLoanDetailsPage_03 - Step 03: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("VP: Original Line Maturity Date textfield is displayed correctly");
		verifyTrue(loanPage.isReadOnlyTextfieldDisplayCorrectly("Original Line Maturity Date", originalLineMaturityDate));
		
		log.info("VP: Current Line Maturity Date Close Date textfield take value from Original Line Mature Date");
		verifyTrue(loanPage.isReadOnlyTextfieldDisplayCorrectly("Current Line Maturity Date", originalLineMaturityDate));
	}
	
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private FNFLoansPage loanPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername, FNFBorrowerPassword;
	private String accountNumber, loanName, underwriter, originator, status, productType;
	private String companyName;
	private String locClosingDate, validNumber, current_Close_Date, line_Mod_Date, mostRecentLineModDate, originalLineMaturityDate, currentLineMaturityDate ;
	
	}