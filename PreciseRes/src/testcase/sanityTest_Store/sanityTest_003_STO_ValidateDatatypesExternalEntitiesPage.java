package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_003_STO_ValidateDatatypesExternalEntitiesPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + getUniqueNumber();
	}

	@Test(groups = { "regression" }, description = "Validate Entities 01 - Validate data types in External Entities (Entity Type = Lessee)")
	public void ValidateEntities_01_EntityTypeIsLessee() {

		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("Precondition - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("ValidateEntities_01 - Step 01: Click 'New External Entities' button");
		transactionPage.clickNewExternalEntitiesButton();

		log.info("ValidateEntities_01 - Step 02: Select 'Lessee' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Lessee");

		log.info("ValidateEntities_01 - Step 03: Select 'Individual' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Individual");

		log.info("ValidateEntities_01 - Step 04: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_01 - Step 05: Select 'Corporation' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Corporation");

		log.info("ValidateEntities_01 - Step 06: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_01 - Step 07: Select 'Limited Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Partnership");

		log.info("ValidateEntities_01 - Step 08: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_01 - Step 09: Select 'General Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("General Partnership");

		log.info("ValidateEntities_01 - Step 10: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_01 - Step 11: Select 'Limited Liability Company' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Liability Company");

		log.info("ValidateEntities_01 - Step 12: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));
	}

	@Test(groups = { "regression" }, description = "Validate Entities 02 - Validate data types in External Entities (Entity Type = Guarantor)")
	public void ValidateEntities_02_EntityTypeIsGuarantor() {

		log.info("ValidateEntities_02 - Step 01: Select 'Guarantor' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Guarantor");

		log.info("ValidateEntities_02 - Step 02: Select 'Individual' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Individual");

		log.info("ValidateEntities_02 - Step 03: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message isn't displayed at 'Email' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_02 - Step 04: Select 'Corporation' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Corporation");

		log.info("ValidateEntities_02 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message isn't displayed at 'Email' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));
		
		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_02 - Step 06: Select 'Limited Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Partnership");

		log.info("ValidateEntities_02 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message isn't displayed at 'Email' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));
		
		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_02 - Step 08: Select 'General Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("General Partnership");

		log.info("ValidateEntities_02 - Step 09: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message isn't displayed at 'Email' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));
		
		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_02 - Step 10: Select 'Limited Liability Company' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Liability Company");

		log.info("ValidateEntities_02 - Step 11: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message isn't displayed at 'Email' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));
		
		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));
	}

	@Test(groups = { "regression" }, description = "Validate Entities 03 - Validate data types in External Entities (Entity Type = Borrower)")
	public void ValidateEntities_03_EntityTypeIsBorrower() {

		log.info("ValidateEntities_03 - Step 01: Select 'Borrower' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Borrower");

		log.info("ValidateEntities_03 - Step 02: Select 'Individual' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Individual");

		log.info("ValidateEntities_03 - Step 03: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_03 - Step 04: Select 'Corporation' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Corporation");

		log.info("ValidateEntities_03 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_03 - Step 06: Select 'Limited Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Partnership");

		log.info("ValidateEntities_03 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_03 - Step 08: Select 'General Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("General Partnership");

		log.info("ValidateEntities_03 - Step 09: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_03 - Step 10: Select 'Limited Liability Company' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Liability Company");

		log.info("ValidateEntities_03 - Step 11: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));
	}

	@Test(groups = { "regression" }, description = "Validate Entities 04 - Validate data types in External Entities (Entity Type = Sponsor)")
	public void ValidateEntities_04_EntityTypeIsSponsor() {

		log.info("ValidateEntities_04 - Step 01: Select 'Sponsor' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Sponsor");

		log.info("ValidateEntities_04 - Step 02: Select 'Individual' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Individual");

		log.info("ValidateEntities_04 - Step 03: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_04 - Step 04: Select 'Corporation' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Corporation");

		log.info("ValidateEntities_04 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_04 - Step 06: Select 'Limited Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Partnership");

		log.info("ValidateEntities_04 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_04 - Step 08: Select 'General Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("General Partnership");

		log.info("ValidateEntities_04 - Step 09: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_04 - Step 10: Select 'Limited Liability Company' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Liability Company");

		log.info("ValidateEntities_04 - Step 11: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));
	}

	@Test(groups = { "regression" }, description = "Validate Entities 05 - Validate data types in External Entities (Entity Type = Pledgor)")
	public void ValidateEntities_05_EntityTypeIsPledgor() {

		log.info("ValidateEntities_05 - Step 01: Select 'Pledgor' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Pledgor");

		log.info("ValidateEntities_05 - Step 02: Select 'Individual' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Individual");

		log.info("ValidateEntities_05 - Step 03: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_05 - Step 04: Select 'Corporation' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Corporation");

		log.info("ValidateEntities_05 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_05 - Step 06: Select 'Limited Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Partnership");

		log.info("ValidateEntities_05 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_05 - Step 08: Select 'General Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("General Partnership");

		log.info("ValidateEntities_05 - Step 09: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_05 - Step 10: Select 'Limited Liability Company' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Liability Company");

		log.info("ValidateEntities_05 - Step 11: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));
	}

	@Test(groups = { "regression" }, description = "Validate Entities 06 - Validate data types in External Entities (Entity Type = Diligence Provider)")
	public void ValidateEntities_06_EntityTypeIsDiligenceProvider() {

		log.info("ValidateEntities_06 - Step 01: Select 'Diligence Provider' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Diligence Provider");

		log.info("ValidateEntities_06 - Step 02: Select 'Individual' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Individual");

		log.info("ValidateEntities_06 - Step 03: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_06 - Step 04: Select 'Corporation' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Corporation");

		log.info("ValidateEntities_06 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_06 - Step 06: Select 'Limited Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Partnership");

		log.info("ValidateEntities_06 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_06 - Step 08: Select 'General Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("General Partnership");

		log.info("ValidateEntities_06 - Step 09: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_06 - Step 10: Select 'Limited Liability Company' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Liability Company");

		log.info("ValidateEntities_04 - Step 11: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));
	}

	@Test(groups = { "regression" }, description = "Validate Entities 07 - Validate data types in External Entities (Entity Type = Franchisor)")
	public void ValidateEntities_07_EntityTypeIsFranchisor() {

		log.info("ValidateEntities_07 - Step 01: Select 'Franchisor' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Franchisor");

		log.info("ValidateEntities_07 - Step 02: Select 'Individual' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Individual");

		log.info("ValidateEntities_07 - Step 03: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_07 - Step 04: Select 'Corporation' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Corporation");

		log.info("ValidateEntities_07 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_07 - Step 06: Select 'Limited Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Partnership");

		log.info("ValidateEntities_07 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_07 - Step 08: Select 'General Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("General Partnership");

		log.info("ValidateEntities_07 - Step 09: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_07 - Step 10: Select 'Limited Liability Company' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Liability Company");

		log.info("ValidateEntities_07 - Step 11: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));
	}

	@Test(groups = { "regression" }, description = "Validate Entities 08 - Validate data types in External Entities (Entity Type = Seller)")
	public void ValidateEntities_08_EntityTypeIsSeller() {

		log.info("ValidateEntities_08 - Step 01: Select 'Seller' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Seller");

		log.info("ValidateEntities_08 - Step 02: Select 'Individual' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Individual");

		log.info("ValidateEntities_08 - Step 03: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_08 - Step 04: Select 'Corporation' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Corporation");

		log.info("ValidateEntities_08 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_08 - Step 06: Select 'Limited Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Partnership");

		log.info("ValidateEntities_08 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_08 - Step 08: Select 'General Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("General Partnership");

		log.info("ValidateEntities_08 - Step 09: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_08 - Step 10: Select 'Limited Liability Company' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Liability Company");

		log.info("ValidateEntities_08 - Step 11: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));
	}

	@Test(groups = { "regression" }, description = "Validate Entities 09 - Validate data types in External Entities (Entity Type = Survey Provider)")
	public void ValidateEntities_09_EntityTypeIsSurveyProvider() {

		log.info("ValidateEntities_09 - Step 01: Select 'Survey Provider' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Survey Provider");

		log.info("ValidateEntities_09 - Step 02: Select 'Individual' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Individual");

		log.info("ValidateEntities_09 - Step 03: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_09 - Step 04: Select 'Corporation' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Corporation");

		log.info("ValidateEntities_09 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_09 - Step 06: Select 'Limited Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Partnership");

		log.info("ValidateEntities_09 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_09 - Step 08: Select 'General Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("General Partnership");

		log.info("ValidateEntities_09 - Step 09: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_09 - Step 10: Select 'Limited Liability Company' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Liability Company");

		log.info("ValidateEntities_09 - Step 11: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));
	}

	@Test(groups = { "regression" }, description = "Validate Entities 10 - Validate data types in External Entities (Entity Type = Phase I Provider)")
	public void ValidateEntities_10_EntityTypeIsPhaseIProvider() {

		log.info("ValidateEntities_10 - Step 01: Select 'Phase I Provider' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Phase I Provider");

		log.info("ValidateEntities_10 - Step 02: Select 'Individual' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Individual");

		log.info("ValidateEntities_10 - Step 03: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_10 - Step 04: Select 'Corporation' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Corporation");

		log.info("ValidateEntities_10 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_10 - Step 06: Select 'Limited Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Partnership");

		log.info("ValidateEntities_10 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_10 - Step 08: Select 'General Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("General Partnership");

		log.info("ValidateEntities_10 - Step 09: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_10 - Step 10: Select 'Limited Liability Company' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Liability Company");

		log.info("ValidateEntities_10 - Step 11: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));
	}

	@Test(groups = { "regression" }, description = "Validate Entities 11 - Validate data types in External Entities (Entity Type = PCA Provider)")
	public void ValidateEntities_11_EntityTypeIsPCAProvider() {

		log.info("ValidateEntities_11 - Step 01: Select 'PCA Provider' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("PCA Provider");

		log.info("ValidateEntities_11 - Step 02: Select 'Individual' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Individual");

		log.info("ValidateEntities_11 - Step 03: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_11 - Step 04: Select 'Corporation' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Corporation");

		log.info("ValidateEntities_11 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_11 - Step 06: Select 'Limited Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Partnership");

		log.info("ValidateEntities_11 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_11 - Step 08: Select 'General Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("General Partnership");

		log.info("ValidateEntities_11 - Step 09: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_11 - Step 10: Select 'Limited Liability Company' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Liability Company");

		log.info("ValidateEntities_11 - Step 11: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));
	}

	@Test(groups = { "regression" }, description = "Validate Entities 12 - Validate data types in External Entities (Entity Type = Title Provider)")
	public void ValidateEntities_12_EntityTypeIsTitleProvider() {

		log.info("ValidateEntities_12 - Step 01: Select 'Title Provider' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Title Provider");

		log.info("ValidateEntities_12 - Step 02: Select 'Individual' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Individual");

		log.info("ValidateEntities_12 - Step 03: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_12 - Step 04: Select 'Corporation' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Corporation");

		log.info("ValidateEntities_12 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_12 - Step 06: Select 'Limited Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Partnership");

		log.info("ValidateEntities_12 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_12 - Step 08: Select 'General Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("General Partnership");

		log.info("ValidateEntities_12 - Step 09: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));

		log.info("ValidateEntities_12 - Step 10: Select 'Limited Liability Company' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Liability Company");

		log.info("ValidateEntities_12 - Step 11: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Last Name is required' message is displayed at 'Last Name' field");
		verifyFalse(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extLastName", "Last Name is required"));

		log.info("VP: 'Email is required' message is displayed at 'Email' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extEmail", "Email is required"));

		log.info("VP: 'Company is required' message is not displayed at 'Company' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_NewExternalEntity_R1_extCorporateInfo", "Company is required"));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName;
}