package sanityTest_Uploader;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_049_B2R_Lender_DragAndDropDocument extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		loanName = "UploaderUdiTesting"+getUniqueNumber();
		applicantName = "Consolidation Demo Borrower New";
		loanStatus = Constant.LOAN_STATUS_B2R;
		propertiesFileName = "B2R Data Tape.xlsx";
		sourcePropertyName = "37412 OAKHILL ST";
		destinationPropertyName = "12983 E 29TH ST";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
		fileNameDocument1 = "datatest.pdf";
		sourceDocumentType = "P5 - Zoning Compliance Letter";
		destinationDocumentType = "P4 - Final Property Condition/Environmental) Report";
		destinationFolderLevel2 = "Uncategorized Property Documents";
		destinationFolderLevel3 = "Operational";
		sourceImage = "sourceDocuments.png";
		destinationPlaceholderImage = "destinationPlaceholder.png";
		destinationPropertyImage = "destinationProperty.png";
		destinationFolderLevel2Image = "destinationFolderLevel1.png";
		destinationFolderLevel3Image = "destinationFolderLevel3.png";
	}
	
	@Test(groups = { "regression" },description = "DragAndDropDocument_01 - Drag Documents and drop to placeholder")
	public void DragAndDropDocument_01_DragAndDropDocumentFromPlaceholderToPlaceholder()
	{
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("Precondition 02. Create new loans and get loans' id");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);
		
		log.info("Precondition 03. Upload data tape");
		loansPage.openPropertiesTab();
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("Precondition 05. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("Precondition 06. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameLender, passwordLender);
		
		log.info("DragAndDropDocument_01 - Step 01. Select loans item");
		uploaderPage.clickChangeGroupedView();
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("DragAndDropDocument_01 - Step 02. Open Property folder");
		uploaderPage.clickPropertiesFolder();
		
		log.info("DragAndDropDocument_01 - Step 03. Open Property item");
		uploaderPage.selectPropertyFolder(sourcePropertyName);
		
		log.info("DragAndDropDocument_01 - Step 03. Get number of documents in here");
		numberOfDocumentsExpected = uploaderPage.getNumberOfDocuments() - 1;
		
		log.info("DragAndDropDocument_01 - Step 04. Upload document for a document type");
		uploaderPage.clickOnSelectNewFileIconByType(sourceDocumentType);
		uploaderPage.uploadFileOnUploader(fileNameDocument1);
		
		log.info("DragAndDropDocument_01 - Step 05. Wait for upload complete");
		uploaderPage.waitForUploadCompleteOnUploader(fileNameDocument1);
		
		log.info("VP: The source document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(fileNameDocument1));
		
		log.info("VP: The source document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, sourceDocumentType));
		
		log.info("VP: The destination document type is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(destinationDocumentType));
		
		log.info("DragAndDropDocument_01 - Step 05. Drag uploaded file to another placeholder in properties list");
		uploaderPage.dragDropFile(sourceImage, destinationPlaceholderImage);
		
		log.info("DragAndDropDocument_01 - Step 06. Click on Yes button ");
		uploaderPage.clickYesButton();
		
		log.info("VP: The old placeholder is deleted");
		verifyFalse(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, sourceDocumentType));
		
		log.info("VP: Documents is moved to new placeholder");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, destinationDocumentType));
	
		log.info("VP. Check all documents are moved to here");
		verifyEquals(uploaderPage.getNumberOfDocuments(), numberOfDocumentsExpected);
	}
	
	@Test(groups = { "regression" },description = "DragAndDropDocument_02 - Drag and drop Documents to UncategorizedFolder Folder")
	public void DragAndDropDocument_02_DragAndDropDocumentToUncategorizedFolder()
	{

		log.info("DragAndDropDocument_02 - Step 01. Open Uncategorized Property Documents folder");
		uploaderPage.selectSubFolderLevel2("Uncategorized Property Documents");
		
		log.info("DragAndDropDocument_02 - Step 02. Get number of documents in here");
		numberOfDocumentsExpected = uploaderPage.getNumberOfDocuments() + 1;
		
		log.info("DragAndDropDocument_02 - Step 03. Open Property item");
		uploaderPage.selectPropertyFolder(sourcePropertyName);
		uploaderPage.observePlaceholder(destinationDocumentType);
		
		log.info("DragAndDropDocument_02 - Step 04. Drag destination file to Uncategorized Folder");
		uploaderPage.dragDropFile(sourceImage, destinationFolderLevel2Image);
		
		log.info("DragAndDropDocument_02 - Step 05. Click on Yes button ");
		uploaderPage.clickYesButton();
		
		log.info("VP: The old placeholder is deleted");
		verifyFalse(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, destinationDocumentType));
		
		log.info("DragAndDropDocument_02 - Step 06. Open Uncategorized Property Documents folder");
		uploaderPage.selectSubFolderLevel2(destinationFolderLevel2);
		
		log.info("VP: Documents is moved to new placeholder");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, destinationDocumentType));
	
		log.info("VP. Check all documents are moved to here");
		verifyEquals(uploaderPage.getNumberOfDocuments(), numberOfDocumentsExpected);
	}
	
	@Test(groups = { "regression" },description = "DragAndDropDocument_02 - Drag and drop Documents to Property folder")
	public void DragAndDropDocument_03_DragAndDropDocumentToPropertyFolder()
	{
		log.info("DragAndDropDocument_03 - Step 01. Open Property item");
		uploaderPage.selectPropertyFolder(destinationPropertyName);
		
		log.info("DragAndDropDocument_03 - Step 02. Get number of documents in here");
		numberOfDocumentsExpected = uploaderPage.getNumberOfDocuments();
		
		log.info("DragAndDropDocument_03 - Step 03. Open Uncategorized Property Documents folder");
		uploaderPage.selectSubFolderLevel2("Uncategorized Property Documents");
		
		log.info("DragAndDropDocument_03 - Step 04. Drag destination file to Uncategorized Folder");
		uploaderPage.dragDropFile(sourceImage, destinationPropertyImage);
		
		log.info("DragAndDropDocument_03 - Step 05. Click on Yes button ");
		uploaderPage.clickYesButton();
		
		log.info("VP: The old placeholder is deleted");
		verifyFalse(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, destinationDocumentType));
		
		log.info("DragAndDropDocument_03 - Step 06. Open Property item");
		uploaderPage.selectPropertyFolder(destinationPropertyName);
		
		log.info("VP: Documents is moved to new placeholder");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, destinationDocumentType));
	
		log.info("VP. Check all documents are moved to here");
		verifyEquals(uploaderPage.getNumberOfDocuments(), numberOfDocumentsExpected);
	}
	
	@Test(groups = { "regression" },description = "DragAndDropDocument_02 - Drag and drop Documents to Level 3 folder")
	public void DragAndDropDocument_04_DragAndDropDocumentToLevel3Folder()
	{
		log.info("DragAndDropDocument_04 - Step 01. Open General Loan Documents ");
		uploaderPage.selectSubFolderLevel2("General Loan Documents");
		
		log.info("DragAndDropDocument_04 - Step 02. Open 'Insurance' documents");
		uploaderPage.selectSubFolderLevel3(destinationFolderLevel3);
		
		log.info("DragAndDropDocument_04 - Step 03. Get number of documents in here");
		numberOfDocumentsExpected = uploaderPage.getNumberOfDocuments() + 1;
		
		log.info("DragAndDropDocument_04 - Step 04. Open Property item");
		uploaderPage.selectPropertyFolder(destinationPropertyName);
		uploaderPage.observePlaceholder(destinationDocumentType);
		uploaderPage.observeFolderLevel2("General Loan Documents");
		
		log.info("DragAndDropDocument_04 - Step 05. Drag destination file to Uncategorized Folder");
		uploaderPage.dragDropFile(sourceImage, destinationFolderLevel3Image);
		
		log.info("DragAndDropDocument_04 - Step 06. Click on Yes button ");
		uploaderPage.clickYesButton();
		
		log.info("VP: The old placeholder is deleted");
		verifyFalse(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, destinationDocumentType));
		
		log.info("DragAndDropDocument_04 - Step 07. Open 'Insurance' documents");
		uploaderPage.selectSubFolderLevel3(destinationFolderLevel3);
		
		log.info("VP: Documents is moved to new placeholder");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, destinationDocumentType));
	
		log.info("VP. Check all documents are moved to here");
		verifyEquals(uploaderPage.getNumberOfDocuments(), numberOfDocumentsExpected);
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender, applicantName, loanStatus;
	private String loanName, uploaderPageUrl, propertiesFileName;
	private String sourcePropertyName, destinationPropertyName, fileNameDocument1, sourceDocumentType, destinationDocumentType, destinationFolderLevel2, destinationFolderLevel3;
	private int numberOfDocumentsExpected;
	private String sourceImage, destinationPlaceholderImage, destinationPropertyImage, destinationFolderLevel2Image, destinationFolderLevel3Image;
}