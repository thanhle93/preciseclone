package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_028_STO_RenewalOptionsChildTable extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileName = "Transaction Renewal Options.xml";
		numberOfRenewalOptions = "4";
		renewalOptionYears = "5";
	}

	@Test(groups = { "regression" }, description = "RenewalOptionsChildTable - Check Renewal Option Created Correctly")
	public void RenewalOptionsChildTable_01_CheckRenewalOptionCreatedCorrectly() {
		
		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("VP: 'Renewal Option' checkbox is checked");
		verifyTrue(transactionPage.isRenewalOptionSelected());
		
		log.info("VP: 'Number of Renewal Options' is displayed correctly");
		verifyTrue(transactionPage.isNumberOfRenewalOptionsDisplayedCorrectly(numberOfRenewalOptions));
		
		log.info("VP: 'Renewal Option' checkbox is checked");
		verifyTrue(transactionPage.isRenewalOptionSelected());
		
		log.info("VP: 'Renewal Option Years' is displayed correctly");
		verifyTrue(transactionPage.isRenewalOptionYearsDisplayedCorrectly(renewalOptionYears));
		
		log.info("VP: 'Number of Renewal Options record' is displayed correctly");
		verifyTrue(transactionPage.isNumberOfRenewalOptionsRecordDisplayedCorrectly(numberOfRenewalOptions));
	}
	
	@Test(groups = { "regression" }, description = "RenewalOptionsChildTable - Check Calculation Renewal Option records")
	public void RenewalOptionsChildTable_02_CalculationRenewalOptionRecords() {
		
		log.info("RenewalOptionsChildTable_02 - Step 01: Get Lease Expiration date");
		leaseExpirationDate= transactionPage.getLeaseExpirationDate();
		
		log.info("VP: Check Renewal Option records generate correctly");
		verifyTrue(transactionPage.isRenewalOptionsChildTableGenerateCorrectly(numberOfRenewalOptions, renewalOptionYears, leaseExpirationDate));
	}
	
	@Test(groups = { "regression" }, description = "RenewalOptionsChildTable - Check Generate Renewal Option records")
	public void RenewalOptionsChildTable_03_GenerateRenewalOptionRecords() {
		
		log.info("RenewalOptionsChildTable_03 - Step 01: Uncheck Renewal Option checkbox");
		transactionPage.uncheckRenewalOption();

		log.info("RenewalOptionsChildTable_03 - Step 02: Click on Generate button");
		transactionPage.clickOnGenerateButton();

		log.info("RenewalOptionsChildTable_03 - Step 03: Accept confirm message to Generate Renewal option table");
		transactionPage.acceptJavascriptAlert(driver);

		log.info("VP: 'Record saved' message displayed correctly");
		verifyTrue(transactionPage.isErrorDisplayedCorrectly(driver, "Record saved"));

		log.info("RenewalOptionsChildTable_03 - Step 04: Change Renewal Option Years and Number of Renewal options to 0");
		transactionPage.inputRenewalOptionYears("0");
		transactionPage.inputNumberOfRenewalOptions("0");
		
		log.info("RenewalOptionsChildTable_03 - Step 05: Click on Generate button");
		transactionPage.clickOnGenerateButton();

		log.info("RenewalOptionsChildTable_03 - Step 06: Accept confirm message to Generate Renewal option table");
		transactionPage.acceptJavascriptAlert(driver);

		log.info("VP: 'Record saved' message displayed correctly");
		verifyTrue(transactionPage.isErrorDisplayedCorrectly(driver, "Record saved"));
		
		log.info("RenewalOptionsChildTable_03 - Step 07: Check Renewal Option checkbox");
		transactionPage.checkRenewalOption();

		log.info("RenewalOptionsChildTable_03 - Step 08: Click on Generate button");
		transactionPage.clickOnGenerateButton();
		
		log.info("RenewalOptionsChildTable_03 - Step 09: Accept confirm message to Generate Renewal option table");
		transactionPage.acceptJavascriptAlert(driver);
		
		log.info("VP: 'Number of Renewal Options is required. Renewal Option Years is required.' message displayed correctly");
		verifyTrue(transactionPage.isErrorDisplayedCorrectly(driver, "Number of Renewal Options is required. Renewal Option Years is required."));

		log.info("RenewalOptionsChildTable_03 - Step 10: Change Renewal Option Years and Number of Renewal options to a number greater than 0");
		numberOfRenewalOptions = "5";
		renewalOptionYears = "7";		
		transactionPage.inputRenewalOptionYears(renewalOptionYears);
		transactionPage.inputNumberOfRenewalOptions(numberOfRenewalOptions);
		
		log.info("RenewalOptionsChildTable_03 - Step 11: Click on Generate button");
		transactionPage.clickOnGenerateButton();

		log.info("RenewalOptionsChildTable_03 - Step 12: Accept confirm message to Generate Renewal option table");
		transactionPage.acceptJavascriptAlert(driver);
		
		log.info("VP: Check Renewal Option records generate correctly");
		verifyTrue(transactionPage.isRenewalOptionsChildTableGenerateCorrectly(numberOfRenewalOptions, renewalOptionYears, leaseExpirationDate));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName;
	private String leaseExpirationDate, numberOfRenewalOptions, renewalOptionYears;
}