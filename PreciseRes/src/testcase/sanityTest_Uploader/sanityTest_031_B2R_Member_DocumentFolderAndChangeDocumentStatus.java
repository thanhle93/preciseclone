package sanityTest_Uploader;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_031_B2R_Member_DocumentFolderAndChangeDocumentStatus extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameMember = "auto-B2R-member2014";
		passwordMember = "change";
		loanName = "UploaderUdiTesting"+getUniqueNumber();
		applicantName = "Consolidation Demo Borrower New";
		loanStatus = Constant.LOAN_STATUS_B2R;
		propertiesFileName = "B2R Data Tape.xlsx";
		fileNameDocument1 = "datatest.pdf";
		documentType1 = "P5 - Zoning Compliance Letter";
		property1Name = "37412 OAKHILL ST";
		borrowerName = "UdiTeamBorrower";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}
	
	@Test(groups = { "regression" },description = "UploaderMember05 - Corporate entities document folder")
	public void Uploader_05_CorporateEntitiesDocumentFolder()
	{
		log.info("Uploader_05 - Step 01. Login with member");
		homePage = loginPage.loginAsLender(usernameMember, passwordMember, false);
		
		log.info("Uploader_05 - Step 02. Create new loans and get loans' id");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.createNewLoan(loanName, applicantName, borrowerName, loanStatus, loanName);
		
		log.info("VP: Applicant displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(applicantName));
		
		log.info("VP: Borrower displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(borrowerName));
		
		log.info("Uploader_05 - Step 03. Click on logged user name on the top right corner");
//		loansPage.clickOnLoggedUserName(driver);
//		uploaderPageUrl = loansPage.getUploaderPageUrl(driver);
//		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
		
		log.info("Uploader_05 - Step 04. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("Uploader_05 - Step 05. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameMember, passwordMember);
		
		log.info("Uploader_05 - Step 06. Select loans item");
		uploaderPage.clickChangeGroupedView();
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("Uploader_05 - Step 07. Select folder Corporate Entity Documents");
		uploaderPage.selectSubFolderLevel2("Corporate Entity Documents");
		
		log.info("VP: There is a folder in Corporate Entity Documents folder with applicant name");
		verifyTrue(uploaderPage.isSubFolderLevel3Display("Applicant - "+applicantName));
		
		log.info("VP: There is a folder in Corporate Entity Documents folder with borrower name");
		verifyTrue(uploaderPage.isSubFolderLevel3Display("Borrower (SPE) - "+borrowerName));
	}
	
	@Test(groups = { "regression" },description = "UploaderMember06 - Cannot see user in Corporate entities document folder after deleting")
	public void Uploader_06_CannotSeeUserInCorporateEntitiesDocumentFolderAfterDelete()
	{
		log.info("Uploader_06 - Step 01. Open web app");
		uploaderLoginPage = uploaderPage.logoutUploader();
		loginPage = uploaderLoginPage.openPreciseResUrlByBorrower(driver, ipClient, getPreciseResUrl());
		
		log.info("Uploader_06 - Step 02. Login with Member");
		homePage = loginPage.loginAsLender(usernameMember, passwordMember, false);
		
		log.info("Uploader_06 - Step 03. Open Loans detail");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		loansPage.openLoansDetailPage(loanName);
		
		log.info("Uploader_06 - Step 04. Open Basic Detail tab");
		log.info("Uploader_06 - Step 05. Delete entity in table");
		loansPage.deleteEntityInTable(borrowerName);
		
		log.info("Uploader_06 - Step 06. Click on logged user name on the top right corner");
//		loansPage.clickOnLoggedUserName(driver);
//		uploaderPageUrl = loansPage.getUploaderPageUrl(driver);
//		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
		
		log.info("Uploader_06 - Step 07. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("Uploader_06 - Step 08. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameMember, passwordMember);
		
		log.info("Uploader_06 - Step 09. Select loans item");
		uploaderPage.clickChangeGroupedView();
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("Uploader_06 - Step 10. Select folder Corporate Entity Documents");
		uploaderPage.selectSubFolderLevel2("Corporate Entity Documents");
		
		log.info("VP: There is a folder in Corporate Entity Documents folder with applicant name");
		verifyTrue(uploaderPage.isSubFolderLevel3Display("Applicant - "+applicantName));
		
		log.info("VP: There is not a folder in Corporate Entity Documents folder with borrower name");
		verifyFalse(uploaderPage.isSubFolderLevel3Display("Borrower (SPE) - "+borrowerName));
	}
	
	@Test(groups = { "regression" },description = "UploaderMember07 - Upload public document and check match")
	public void Uploader_07_UploadPublicDocumentAndCheckMatch()
	{
		log.info("Uploader_07 - Step 01. Open web app");
		uploaderLoginPage = uploaderPage.logoutUploader();
		loginPage = uploaderLoginPage.openPreciseResUrlByBorrower(driver, ipClient, getPreciseResUrl());
		
		log.info("Uploader_07 - Step 02. Login with Member");
		homePage = loginPage.loginAsLender(usernameMember, passwordMember, false);
		
		log.info("Uploader_07 - Step 03. Open Loans detail");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		loansPage.openLoansDetailPage(loanName);
		
		log.info("Uploader_07 - Step 04. Open Properties tab");
		loansPage.openPropertiesTab();
		
		log.info("Uploader_07 - Step 05. Upload data tape");
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("Uploader_07 - Step 06. Open property detail");
		propertiesPage = loansPage.openPropertyDetail(property1Name);
		
		log.info("Uploader_07 - Step 07. Open Document type detail");
		propertiesPage.openDocumentTypeDetail(documentType1);
		
		log.info("Uploader_07 - Step 08. Select document type");
		propertiesPage.selectDocumentType(documentType1);
			
		log.info("Uploader_07 - Step 09. Upload document file");
		propertiesPage.uploadDocumentFile(fileNameDocument1);
		
		log.info("Uploader_07 - Step 10. Click save button");
		propertiesPage.clickSaveButton();
		
		log.info("Uploader_07 - Step 11. Click Go To Property button");
		propertiesPage.clickGoToPropertyButton();
		
		log.info("VP: Document is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, fileNameDocument1));
		
		log.info("Uploader_07 - Step 12. Click on logged user name on the top right corner");
//		loansPage = propertiesPage.openLoansPage(driver, ipClient);
//		loansPage.clickOnLoggedUserName(driver);
//		uploaderPageUrl = loansPage.getUploaderPageUrl(driver);
//		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
		
		log.info("Uploader_07 - Step 13. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("Uploader_07 - Step 14. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameMember, passwordMember);
		
		log.info("Uploader_07 - Step 15. Select loans item");
		uploaderPage.clickChangeGroupedView();
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("Uploader_07 - Step 16. Open Property folder");
		uploaderPage.clickPropertiesFolder();
		
		log.info("Uploader_07 - Step 17. Open Property item");
		uploaderPage.selectPropertyFolder(property1Name);
		
		log.info("VP: Make sure the file is updated in the uploader and unmarked with V as Lender private");
		verifyFalse(uploaderPage.isLenderPrivateCheckmarkForType(documentType1));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private String usernameMember, passwordMember, applicantName;
	private String loanName, uploaderPageUrl, propertiesFileName, loanStatus;
	private String property1Name, fileNameDocument1, documentType1, borrowerName;
}