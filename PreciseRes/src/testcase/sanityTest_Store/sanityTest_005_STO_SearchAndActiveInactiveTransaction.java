package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_005_STO_SearchAndActiveInactiveTransaction extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		accountNumber = getUniqueNumber();
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + accountNumber;
		active = "Yes";
		inActive = "No";
		either = "Either";
		companyName = "Testing QA " + accountNumber;
		firstName = "Testing ";
		email = "qatesting" + accountNumber + "@gmail.com";
		address = "Street " + accountNumber ;
		city = "Los Angeles";
		state = "CA - California";
		zipCode = "90001";
		lesseeName = "New Lessee " + accountNumber;
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 01 - Search by Company name")
	public void SearchActiveInactive_01_SearchByCompanyName() {
		
		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("Precondition - Step 06: Update ClientID");
		transactionPage.inputClientID(accountNumber);
		
		log.info("Precondition - Step 07: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("Precondition - Step 08: Get chose Lessee name");
		currentLesseeName = transactionPage.getLesseeName();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("SearchActiveInactive_01 - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("SearchActiveInactive_01 - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("VP: The Transaction with Company name display in table");
		verifyTrue(transactionPage.isTransactionDisplayOnSearchTransactionPage(companyName, accountNumber, active));
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 02 - Search by Client ID")
	public void SearchActiveInactive_02_SearchByClientID() {
		
		log.info("SearchActiveInactive_02 - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("SearchActiveInactive_02 - Step 02: Search Transaction by ClientID");
		transactionPage.searchTransactionByClientID(accountNumber);
		
		log.info("VP: The Transaction with ClientID display in table");
		verifyTrue(transactionPage.isTransactionDisplayOnSearchTransactionPage(companyName, accountNumber, active));
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 03 - Search by Transaction active status")
	public void SearchActiveInactive_03_SearchByActiveStatus() {
		
		log.info("SearchActiveInactive_03 - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("SearchActiveInactive_03 - Step 02: Search Transaction by Active status");
		transactionPage.searchTransactionByActiveOrInactive(accountNumber, active);
		
		log.info("VP: The Transaction with Active status display in table");
		verifyTrue(transactionPage.isTransactionDisplayOnSearchTransactionPage(companyName, accountNumber, active));
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 04 - Search by Transaction either status")
	public void SearchActiveInactive_04_SearchByEitherStatus() {
		
		log.info("SearchActiveInactive_04 - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("SearchActiveInactive_04 - Step 02: Search Transaction by Either status");
		transactionPage.searchTransactionByActiveOrInactive(accountNumber, either);
		
		log.info("VP: The Transaction with Active status display in table");
		verifyTrue(transactionPage.isTransactionDisplayOnSearchTransactionPage(companyName, accountNumber, active));
		
		log.info("SearchActiveInactive_04 - Step 03: Select Transaction checkbox in Transactions table");
		transactionPage.isSelectedTransactionCheckbox(companyName);
		
		log.info("SearchActiveInactive_04 - Step 04: Click 'Make Inactive' button");
		transactionPage.clickMakeInactiveButton();
		
		log.info("SearchActiveInactive_04 - Step 05: Search Transaction by Either status");
		transactionPage.searchTransactionByActiveOrInactive(accountNumber, either);
		
		log.info("VP: The Transaction with Inactive status display in table");
		verifyTrue(transactionPage.isTransactionDisplayOnSearchTransactionPage(companyName, accountNumber, inActive));
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 05 - Search by Transaction inactive status")
	public void SearchActiveInactive_05_SearchByInactiveStatus() {
		
		log.info("SearchActiveInactive_05 - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("SearchActiveInactive_05 - Step 02: Search Transaction by Inactive status");
		transactionPage.searchTransactionByActiveOrInactive(accountNumber, inActive);
		
		log.info("VP: The Transaction with Inactive status display in table");
		verifyTrue(transactionPage.isTransactionDisplayOnSearchTransactionPage(companyName, accountNumber, inActive));
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 06 - Make a Transaction is Active")
	public void SearchActiveInactive_06_MakeATransactionIsActive() {
		
		log.info("SearchActiveInactive_06 - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("SearchActiveInactive_06 - Step 02: Search Transaction by Inactive status");
		transactionPage.searchTransactionByActiveOrInactive(accountNumber, inActive);
		
		log.info("VP: The Transaction with Inactive status display in table");
		verifyTrue(transactionPage.isTransactionDisplayOnSearchTransactionPage(companyName, accountNumber, inActive));
		
		log.info("SearchActiveInactive_06 - Step 03: Select Transaction checkbox in Transactions table");
		transactionPage.isSelectedTransactionCheckbox(companyName);
		
		log.info("SearchActiveInactive_06 - Step 04: Click 'Make Active' button");
		transactionPage.clickMakeActiveButtonAtTransactionPropertyTab();
		
		log.info("SearchActiveInactive_06 - Step 05: Search Transaction by Active status");
		transactionPage.searchTransactionByActiveOrInactive(accountNumber, active);
		
		log.info("VP: The Transaction with Active status display in table");
		verifyTrue(transactionPage.isTransactionDisplayOnSearchTransactionPage(companyName, accountNumber, active));
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 07 - Make a Transaction is Inactive")
	public void SearchActiveInactive_07_MakeATransactionIsInactive() {
		
		log.info("SearchActiveInactive_07 - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("SearchActiveInactive_07 - Step 02: Search Transaction by Active status");
		transactionPage.searchTransactionByActiveOrInactive(accountNumber, active);
		
		log.info("VP: The Transaction with Active status display in table");
		verifyTrue(transactionPage.isTransactionDisplayOnSearchTransactionPage(companyName, accountNumber, active));
		
		log.info("SearchActiveInactive_07 - Step 03: Select Transaction checkbox in Transactions table");
		transactionPage.isSelectedTransactionCheckbox(companyName);
		
		log.info("SearchActiveInactive_07 - Step 04: Click 'Make Inactive' button");
		transactionPage.clickMakeInactiveButton();
		
		log.info("SearchActiveInactive_07 - Step 05: Search Transaction by Inactive status");
		transactionPage.searchTransactionByActiveOrInactive(accountNumber, inActive);
		
		log.info("VP: The Transaction with Inactive status display in table");
		verifyTrue(transactionPage.isTransactionDisplayOnSearchTransactionPage(companyName, accountNumber, inActive));
		
		log.info("SearchActiveInactive_06 - Step 06: Select Transaction checkbox in Transactions table");
		transactionPage.isSelectedTransactionCheckbox(companyName);
		
		log.info("SearchActiveInactive_06 - Step 07: Click 'Make Active' button");
		transactionPage.clickMakeActiveButtonAtTransactionPropertyTab();
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 08 - Check Lessee transaction link")
	public void SearchActiveInactive_08_LesseeTransactionLink() {
		
		//Transaction displayed with lessee name
		
		log.info("SearchActiveInactive_08 - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("SearchActiveInactive_08 - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("VP: The Transaction with Company name and Lessee name display in table ");
		verifyTrue(transactionPage.isTransactionDisplayOnSearchTransactionPage(companyName, currentLesseeName));
		
		//Create new Lessee and check Transaction displayed with new lessee name
		
		log.info("SearchActiveInactive_08 - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("SearchActiveInactive_08 - Step 04: Click 'New External Entities' button");
		transactionPage.clickNewExternalEntitiesButton();
		
		log.info("SearchActiveInactive_08 - Step 05: Select 'Lessee' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Lessee");
		
		log.info("SearchActiveInactive_08 - Step 06: Select 'Individual' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Individual");
		
		log.info("SearchActiveInactive_08 - Step 07. Input Lessee information");
		transactionPage.inputExternalEntitiesInfoToCreate(accountNumber, firstName, lesseeName, lesseeName, email, address, city, state, zipCode);
		
		log.info("SearchActiveInactive_08 - Step 08: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: New Lessee is saved");
		verifyTrue(transactionPage.isNewExternalEntitiesSaved(lesseeName));
		
		log.info("SearchActiveInactive_08 - Step 09. Click Back button on New External Entites");
		transactionPage.clickBackExternalEntitiesButton();	
		
		log.info("VP: The Lessee display on External Entities table");
		verifyTrue(transactionPage.isNewEntitiesDisplayOnExternalEntitiesTable(lesseeName, "Lessee"));
		
		log.info("SearchActiveInactive_08 - Step 10: Select another lessee Name");
		transactionPage.selectLessee(firstName+lesseeName);
		
		log.info("SearchActiveInactive_08 - Step 11: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("SearchActiveInactive_08 - Step 12: Check new lessee is save");
		verifyTrue(transactionPage.isLesseeSavedSuccessfully(firstName+lesseeName));
		
		log.info("SearchActiveInactive_08 - Step 13: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("SearchActiveInactive_08 - Step 14: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("VP: The Transaction with Company name and Lessee name display in table ");
		verifyTrue(transactionPage.isTransactionDisplayOnSearchTransactionPage(companyName, firstName+lesseeName));
		
		//Delete new Lessee and check Transaction displayed with old lessee name
		
		log.info("SearchActiveInactive_08 - Step 15: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("SearchActiveInactive_08 - Step 16: Delete Lessee");
		transactionPage.chooseLessee(lesseeName);
		transactionPage.clickSaveButton();
		
		log.info("SearchActiveInactive_08 - Step 17: Check new lessee is save");
		verifyTrue(transactionPage.isLesseeSavedSuccessfully(currentLesseeName));
		
		log.info("SearchActiveInactive_08 - Step 18: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("SearchActiveInactive_08 - Step 19: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("VP: The Transaction with Company name and Lessee name display in table ");
		verifyTrue(transactionPage.isTransactionDisplayOnSearchTransactionPage(companyName, currentLesseeName));
		
		//Delete all lessee and check Transaction displayes with 'No Lessee' name
		
		log.info("SearchActiveInactive_08 - Step 20: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("SearchActiveInactive_08 - Step 21: Delete Lessee");
		transactionPage.chooseLessee(currentLesseeName);
		transactionPage.clickSaveButton();
		
		log.info("SearchActiveInactive_08 - Step 22: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("SearchActiveInactive_08 - Step 23: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("VP: The Transaction with Company name and Lessee name display in table ");
		verifyTrue(transactionPage.isTransactionDisplayOnSearchTransactionPage(companyName, "No Lessee"));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName, accountNumber, active, inActive, either;
	private String firstName, lesseeName, currentLesseeName;
	private String address, city, state, zipCode, email;
}