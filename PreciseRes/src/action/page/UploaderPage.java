package page;

import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import PreciseRes.Interfaces;
import common.Common;

public class UploaderPage extends AbstractPage {
	public UploaderPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}

	/**
	 * search Loan Item
	 */
	public void searchLoanItem(String loanItem) {
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			waitForControl(driver, Interfaces.UploaderPage.SEARCH_LOAN_ITEM_IE, timeWait);
			click(driver, Interfaces.UploaderPage.SEARCH_LOAN_ITEM_IE);
			type(driver, Interfaces.UploaderPage.SEARCH_LOAN_ITEM_IE, loanItem);
			keyPressing("enter");
			sleep(5);
		} else {
			waitForControl(driver, Interfaces.UploaderPage.SEARCH_LOAN_ITEM, timeWait);
			type(driver, Interfaces.UploaderPage.SEARCH_LOAN_ITEM, loanItem);
			keyPressing("enter");
			sleep(2);
		}
	}

	/**
	 * select Loan Item
	 */
	public void selectLoanItem(String loanItem) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_LOAN_ITEM, loanItem, timeWait);
		click(driver, Interfaces.UploaderPage.DYNAMIC_LOAN_ITEM, loanItem);
		sleep(10);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(15);
		}
	}

	/**
	 * select Loan AuditItem
	 */
	public void selectLoanAuditItem(String loanItem) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_LOAN_AUDIT_ITEM, loanItem, timeWait);
		click(driver, Interfaces.UploaderPage.DYNAMIC_LOAN_AUDIT_ITEM, loanItem);
		sleep(10);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
	}

	/**
	 * select Sub Folder
	 */
	public void selectSubFolderLevel2(String subFolder) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_SUBFOLDER_LEVEL2, subFolder, timeWait);
		click(driver, Interfaces.UploaderPage.DYNAMIC_SUBFOLDER_LEVEL2, subFolder);
		sleep(2);
		if (!isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_SUBFOLDER_LEVEL2_IS_COLLAPSE, subFolder)) {
			click(driver, Interfaces.UploaderPage.DYNAMIC_SUBFOLDER_LEVEL2, subFolder);
			sleep(2);
		}
		sleep(3);
	}

	/**
	 * select Sub Folder
	 */
	public void selectSubFolderLevel3(String subFolder) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_SUBFOLDER_LEVEL3, subFolder, timeWait);
		doubleClick(driver, Interfaces.UploaderPage.DYNAMIC_SUBFOLDER_LEVEL3, subFolder);
		sleep(2);
	}

	/**
	 * click Add Another Documents
	 */
	public void clickAddAnotherDocuments() {
		waitForControl(driver, Interfaces.UploaderPage.ADD_ANOTHER_DOCUMENT_BUTTON, timeWait);
		click(driver, Interfaces.UploaderPage.ADD_ANOTHER_DOCUMENT_BUTTON);
		sleep(3);
		while (!isControlDisplayed(driver, Interfaces.UploaderPage.OK_BUTTON)) {
			click(driver, Interfaces.UploaderPage.ADD_ANOTHER_DOCUMENT_BUTTON);
			sleep(3);
		}
		clickLoadFileButton();
	}

	/**
	 * click Add Another No Documents Upload
	 */
	public void clickAddAnotherNoDocumentsUpload() {
		waitForControl(driver, Interfaces.UploaderPage.ADD_ANOTHER_DOCUMENT_BUTTON, timeWait);
		click(driver, Interfaces.UploaderPage.ADD_ANOTHER_DOCUMENT_BUTTON);
		sleep(3);
		while (!isControlDisplayed(driver, Interfaces.UploaderPage.OK_BUTTON)) {
			click(driver, Interfaces.UploaderPage.ADD_ANOTHER_DOCUMENT_BUTTON);
			sleep(3);
		}
	}
	
	/**
	 * click Load File Button
	 */
	public void clickLoadFileButton() {
		waitForControl(driver, Interfaces.UploaderPage.LOAD_FILE_BUTTON, timeWait);
		click(driver, Interfaces.UploaderPage.LOAD_FILE_BUTTON);
		sleep(5);
	}

	/**
	 * upload File on Uploader
	 */
	public void uploadFileOnUploader(String fileName) {
		Common.getCommon().openFileForUpload(driver, fileName);
		sleep(5);
	}

	/**
	 * wait For Upload Complete
	 */
	public void waitForUploadCompleteOnUploader(String fileName) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TITLE, fileName, timeWait * 5);
//		if(!isDocumentTypeDisplay(fileName, "Unknown")) {
//			clickAddAnotherDocuments();
//			uploadFileOnUploader(fileName);
//			waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TITLE, fileName, timeWait * 2);
//		}
	}

	/**
	 * wait For Add Other Document
	 */
	public void waitForAddOtherDocument(String fileName) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_ADD_OTHER_DOCUMENT_TITLE, fileName, timeWait * 5);
	}

	/**
	 * check Document Display
	 */
	public boolean isDocumentDisplay(String fileName) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TITLE, fileName, timeWait);
		return isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TITLE, fileName);
	}

	/**
	 * check Document Type Display
	 */
	public boolean isDocumentTypeDisplay(String fileName, String documentType) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE, fileName, documentType, 10);
		return isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE, fileName, documentType);
	}
	
	/**
	 * check Document Type Display
	 */
	public boolean isDocumentTypeDisplay(String documentType) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE_WITHOUT_DOCUMENT, documentType, 10);
		return isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE_WITHOUT_DOCUMENT, documentType);
	}

	/**
	 * click On Edit Document Icon
	 */
	public void clickOnEditDocumentIcon(String documentName) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_EDIT_ICON, documentName, timeWait);
		click(driver, Interfaces.UploaderPage.DYNAMIC_EDIT_ICON, documentName);
		sleep(4);
		// if(!isControlDisplayed(driver,
		// Interfaces.UploaderPage.DOCUMENT_NAME_TEXTBOX_EDIT_DOCUMENT)){
		// click(driver, Interfaces.UploaderPage.DYNAMIC_EDIT_ICON,
		// documentName);
		// sleep(2);
		// }
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep(5);
	}

	/**
	 * click On View Document Icon
	 */
	public void clickOnViewDocumentIcon(String documentName) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_VIEW_DOCUMENT_ICON, documentName, timeWait);
		doubleClick(driver, Interfaces.UploaderPage.DYNAMIC_VIEW_DOCUMENT_ICON, documentName);
		sleep();
	}

	/**
	 * select Document Type on edit document popup
	 */
	public void selectDocumentType(String documentType) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE_EDIT_DOCUMENT, documentType, timeWait);
		click(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE_EDIT_DOCUMENT, documentType);
		sleep(2);
		if (!isControlSelected(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE_EDIT_DOCUMENT, documentType)) {
			click(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE_EDIT_DOCUMENT, documentType);
		}
		sleep(3);
	}

	/**
	 * click Ok Button On Edit Document
	 */
	public void clickOkButtonOnEditDocument() {
		if (isControlDisplayed(driver, Interfaces.UploaderPage.CHANGE_LOCATION_WITH_LOAN_DOCUMENTS)) {
			clickChangeLocationWithLoanAndProperties();
			sleep(3);
		}
		waitForControl(driver, Interfaces.UploaderPage.OK_BUTTON, timeWait);
		click(driver, Interfaces.UploaderPage.OK_BUTTON);
		executeJavaScript(driver, "document.getElementByClassName('btn btn-primary').click();");
		sleep(2);
		while (isControlDisplayed(driver, Interfaces.UploaderPage.OK_BUTTON)) {
			click(driver, Interfaces.UploaderPage.OK_BUTTON);
			executeJavaScript(driver, "document.getElementByClassName('btn btn-primary').click();");
			sleep(1);
		}
		sleep(3);
	}

	/**
	 * click Change Location with Loan and Properties
	 */
	public void clickChangeLocationWithLoanAndProperties() {
		waitForControl(driver, Interfaces.UploaderPage.CHANGE_LOCATION_WITH_LOAN_DOCUMENTS, timeWait);
		click(driver, Interfaces.UploaderPage.CHANGE_LOCATION_WITH_LOAN_DOCUMENTS);
		sleep();
		waitForControl(driver, Interfaces.UploaderPage.CHANGE_LOCATION_WITH_PROPERTIES, timeWait);
		click(driver, Interfaces.UploaderPage.CHANGE_LOCATION_WITH_PROPERTIES);
		sleep();
	}

	/**
	 * check Document Viewable
	 */
	public boolean isDocumentViewable(String fileName) {
		sleep(3);
		String currentHandle = driver.getWindowHandle();
		openWindowHandles.push(driver.getWindowHandle());
		Set<String> newHandles = driver.getWindowHandles();
		newHandles.removeAll(openWindowHandles);
		String handle = newHandles.iterator().next();
		driver.switchTo().window(handle);
		String url = getCurrentUrl(driver);
		boolean exists = url.contains(fileName);
		driver.close();
		driver.switchTo().window(currentHandle);
		sleep(3);
		return exists;
	}

	/**
	 * click On Select New File Icon
	 */
	public void clickOnSelectNewFileIconByType(String documentType) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_ADD_NEW_DOCUMENT_BY_TYPE_ICON, documentType, timeWait);
		click(driver, Interfaces.UploaderPage.DYNAMIC_ADD_NEW_DOCUMENT_BY_TYPE_ICON, documentType);
		sleep(2);
	}

	/**
	 * click On Select New File Icon
	 */
	public void clickOnSelectNewFileIcon(String documentName) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_ADD_NEW_DOCUMENT_ICON, documentName, timeWait);
		click(driver, Interfaces.UploaderPage.DYNAMIC_ADD_NEW_DOCUMENT_ICON, documentName);
		sleep(2);
	}

	/**
	 * click On Keywords Button
	 */
	public void clickOnKeywordsButton() {
		waitForControl(driver, Interfaces.UploaderPage.KEYWORD_BUTTON, timeWait);
		click(driver, Interfaces.UploaderPage.KEYWORD_BUTTON);
		sleep(3);
		if (!isControlDisplayed(driver, Interfaces.UploaderPage.AUTOCATEGORIZATION_KEYWORDS_POPUP)) {
			click(driver, Interfaces.UploaderPage.KEYWORD_BUTTON);
			sleep(2);
		}
		sleep(2);
	}

	/**
	 * click On Reload Data Button
	 */
	public void clickOnReloadDataButton() {
		waitForControl(driver, Interfaces.UploaderPage.RELOAD_DATA_BUTTON, timeWait);
		doubleClick(driver, Interfaces.UploaderPage.RELOAD_DATA_BUTTON);
		sleep();
	}

	/**
	 * enter New Keyword
	 */
	public void enterNewKeyword(String keyword) {
		waitForControl(driver, Interfaces.UploaderPage.ADD_NEW_KEYWORD_TEXTBOX, keyword, timeWait);
		type(driver, Interfaces.UploaderPage.ADD_NEW_KEYWORD_TEXTBOX, keyword);
		click(driver, Interfaces.UploaderPage.ADD_NEW_KEYWORD_BUTTON);
		sleep(1);
	}

	/**
	 * check New Keyword Display on table
	 */
	public boolean isNewKeywordDisplayOnTable(String newKeyword) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_KEYWORD_DISPLAY_TABLE_CELL, newKeyword, timeWait);
		return isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_KEYWORD_DISPLAY_TABLE_CELL, newKeyword);
	}

	/**
	 * Edit New Keyword
	 */
	public void editNewKeyword(String keywordOld, String keywordNew) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_KEYWORD_DISPLAY_TABLE_CELL, keywordOld, timeWait);
		type(driver, Interfaces.UploaderPage.DYNAMIC_KEYWORD_DISPLAY_TABLE_CELL, keywordOld, keywordNew);
		sleep(1);
	}

	/**
	 * Delete New Keyword
	 */
	public void deleteNewKeyword(String keyword) {
		waitForControl(driver, Interfaces.UploaderPage.DELETE_NEW_KEYWORD_TABLE_CELL, keyword, timeWait);
		click(driver, Interfaces.UploaderPage.DELETE_NEW_KEYWORD_TABLE_CELL, keyword);
		sleep(1);
	}

	/**
	 * check Keyword Display On Edit Document
	 */
	public boolean isKeywordDisplayOnEditDocument(String keyword) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE_EDIT_DOCUMENT, keyword, timeWait);
		return isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE_EDIT_DOCUMENT, keyword);
	}

	/**
	 * click Ok Button On Keywords
	 */
	public void clickOkButtonOnKeywords() {
		waitForControl(driver, Interfaces.UploaderPage.OK_BUTTON, timeWait);
		click(driver, Interfaces.UploaderPage.OK_BUTTON);
		sleep(3);
		while (isControlDisplayed(driver, Interfaces.UploaderPage.OK_BUTTON)) {
			click(driver, Interfaces.UploaderPage.OK_BUTTON);
		}
		sleep(2);
	}

	/**
	 * select SubFolder On Edit Document
	 */
	public void selectSubFolderOnEditDocument(String subfolder) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_SUBFOLDER_L3_EDIT_DOCUMENT, subfolder, timeWait);
		doubleClick(driver, Interfaces.UploaderPage.DYNAMIC_SUBFOLDER_L3_EDIT_DOCUMENT, subfolder);
		sleep();
	}

	/**
	 * click Cancel Button On Edit Document
	 */
	public void clickCancelButtonOnEditDocument() {
		waitForControl(driver, Interfaces.UploaderPage.CANCEL_BUTTON, timeWait);
		click(driver, Interfaces.UploaderPage.CANCEL_BUTTON);
		sleep(1);
	}

	/**
	 * click On Delete Document Icon
	 */
	public void clickOnDeleteDocumentIcon(String documentName) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DELETE_ICON, documentName, timeWait);
		click(driver, Interfaces.UploaderPage.DYNAMIC_DELETE_ICON, documentName);
		sleep(3);
		if (!isControlDisplayed(driver, Interfaces.UploaderPage.YES_BUTTON)) {
			click(driver, Interfaces.UploaderPage.DYNAMIC_DELETE_ICON, documentName);
			sleep(3);
		}
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(3);
		}
		sleep(2);
	}

	/**
	 * click Yes Button
	 */
	public void clickYesButton() {
		waitForControl(driver, Interfaces.UploaderPage.YES_BUTTON, timeWait);
		click(driver, Interfaces.UploaderPage.YES_BUTTON);
		sleep(3);
		if (isControlDisplayed(driver, Interfaces.UploaderPage.YES_BUTTON)) {
			click(driver, Interfaces.UploaderPage.YES_BUTTON);
			sleep(3);
		}
		sleep(15);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(15);
		}
	}

	/**
	 * click No Button
	 */
	public void clickNoButton() {
		waitForControl(driver, Interfaces.UploaderPage.NO_BUTTON, timeWait);
		click(driver, Interfaces.UploaderPage.NO_BUTTON);
		sleep(3);
		while (isControlDisplayed(driver, Interfaces.UploaderPage.NO_BUTTON)) {
			click(driver, Interfaces.UploaderPage.NO_BUTTON);
			sleep(3);
		}
		sleep(3);
	}

	/**
	 * logout Uploader
	 */
	public UploaderLoginPage logoutUploader() {
		waitForControl(driver, Interfaces.UploaderPage.LOGOUT_BUTTON, timeWait);
		click(driver, Interfaces.UploaderPage.LOGOUT_BUTTON);
		sleep(3);
		return PageFactory.getUploaderLoginPage(driver, ipClient);
	}

	/**
	 * delete Document Precondition
	 */
	public void deleteDocumentPrecondition(String fileName) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TITLE, fileName, timeWait);
		if (isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TITLE, fileName)) {
			clickOnDeleteDocumentIcon(fileName);
			clickYesButton();
		}
	}

	/**
	 * check User Name Display On Uploader As Folder
	 */
	public boolean isUserNameDisplayOnUploaderAsFolder(String userName) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_SUBFOLDER_LEVEL3, userName, timeWait);
		return isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_SUBFOLDER_LEVEL3, userName);
	}

	/**
	 * check Document For Folder Page Display
	 */
	public boolean isDocumentForFolderPageDisplay(String folderName) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_LIST_TITLE, folderName, timeWait);
		return isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_LIST_TITLE, folderName);
	}

	/**
	 * click Auto categorize Documents Button
	 */
	public void clickAutocategorizeDocumentsButton() {
		waitForControl(driver, Interfaces.UploaderPage.AUTOCATEGORIZE_DOC_BUTTON, timeWait);
		click(driver, Interfaces.UploaderPage.AUTOCATEGORIZE_DOC_BUTTON);
		sleep(10);
		waitForControl(driver, Interfaces.UploaderPage.CATEGORIZATION_RESULT_TITLE, timeWait);
	}

	/**
	 * click Ok Auto categorization Result Button
	 */
	public void clickOkAutocategorizationResultButton() {
		waitForControl(driver, Interfaces.UploaderPage.OK_AUTOCATEGORIZATION_RESULT_BUTTON, timeWait);
		click(driver, Interfaces.UploaderPage.OK_AUTOCATEGORIZATION_RESULT_BUTTON);
		sleep(3);
		while (isControlDisplayed(driver, Interfaces.UploaderPage.OK_AUTOCATEGORIZATION_RESULT_BUTTON)) {
			click(driver, Interfaces.UploaderPage.OK_AUTOCATEGORIZATION_RESULT_BUTTON);
			sleep(3);
		}
	}

	/**
	 * select Property On Edit Document Popup
	 */
	public void selectPropertyOnEditDocumentPopup(String propertyName) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_PROPERTY_ON_EDIT_DOCUMENT, propertyName, timeWait);
		click(driver, Interfaces.UploaderPage.DYNAMIC_PROPERTY_ON_EDIT_DOCUMENT, propertyName);
		sleep(2);
	}

	/**
	 * click Properties Folder
	 */
	public void clickPropertiesFolder() {
		waitForControl(driver, Interfaces.UploaderPage.PROPERTY_FOLDER, timeWait);
		click(driver, Interfaces.UploaderPage.PROPERTY_FOLDER);
		sleep(10);
		if (isControlDisplayed(driver, Interfaces.UploaderPage.PROPERTY_FOLDER_NG_HIDE)) {
			click(driver, Interfaces.UploaderPage.PROPERTY_FOLDER);
			sleep(10);
		}
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
	}

	/**
	 * click Properties Folder - Audit Loan
	 */
	public void clickPropertiesFolderAuditLoan() {
		waitForControl(driver, Interfaces.UploaderPage.AUDIT_LOAN_PROPERTY_FOLDER, timeWait);
		click(driver, Interfaces.UploaderPage.AUDIT_LOAN_PROPERTY_FOLDER);
		sleep(10);
		if (isControlDisplayed(driver, Interfaces.UploaderPage.PROPERTY_FOLDER_NG_HIDE)) {
			click(driver, Interfaces.UploaderPage.AUDIT_LOAN_PROPERTY_FOLDER);
			sleep(10);
		}
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
	}

	/**
	 * select Property Folder
	 */
	public void selectPropertyFolder(String propertyName) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_PROPERTY_FOLDER, propertyName, timeWait);
		doubleClick(driver, Interfaces.UploaderPage.DYNAMIC_PROPERTY_FOLDER, propertyName);
		sleep(10);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
	}

	/**
	 * is Property Folder displayed
	 */
	public boolean isPropertyFolderDisplayed(String propertyName) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_PROPERTY_FOLDER, propertyName, timeWait);
		return isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_PROPERTY_FOLDER, propertyName);
	}

	/**
	 * select Checkbox For Document Type
	 */
	public void selectCheckboxForDocumentType(String documentType) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_CHECKBOX_FOR_DOC_TYPE, documentType, timeWait);
		checkTheCheckbox(driver, Interfaces.UploaderPage.DYNAMIC_CHECKBOX_FOR_DOC_TYPE, documentType);
		sleep(2);
	}

	/**
	 * select Radio Button For Document Type
	 */
	public void selectRadioButtonForDocumentType() {
		waitForControl(driver, Interfaces.UploaderPage.RADIO_BUTTON_FOR_DOC_TYPE, timeWait);
		checkTheCheckbox(driver, Interfaces.UploaderPage.RADIO_BUTTON_FOR_DOC_TYPE);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep(10);
	}

	/**
	 * select Radio Button For Address
	 */
	public void selectRadioButtonForAddress() {
		waitForControl(driver, Interfaces.UploaderPage.RADIO_BUTTON_FOR_ADDRESS, timeWait);
		checkTheCheckbox(driver, Interfaces.UploaderPage.RADIO_BUTTON_FOR_ADDRESS);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep(5);
	}

	/**
	 * click Make Borrower Only Link
	 */
	public void clickMakeBorrowerOnlyLink() {
		waitForControl(driver, Interfaces.UploaderPage.MAKE_BORROWER_ONLY_LINK, timeWait);
		click(driver, Interfaces.UploaderPage.MAKE_BORROWER_ONLY_LINK);
		sleep(3);
		while (!isControlDisplayed(driver, Interfaces.UploaderPage.YES_BUTTON)) {
			click(driver, Interfaces.UploaderPage.MAKE_BORROWER_ONLY_LINK);
			sleep(2);
		}
	}

	/**
	 * check Borrower Private Check mark For Type
	 */
	public boolean isBorrowerPrivateCheckmarkForType(String documentType) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_BORROWER_PRIVATE_CHECKBOX_FOR_TYPE, documentType, timeWait);
		String classShow = getAttributeValue(driver, Interfaces.UploaderPage.DYNAMIC_BORROWER_PRIVATE_CHECKBOX_FOR_TYPE, documentType, "class");
		return !classShow.equals("ng-hide");
	}

	/**
	 * check Lender Private Check mark For Type
	 */
	public boolean isLenderPrivateCheckmarkForType(String documentType) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_LENDER_PRIVATE_CHECKBOX_FOR_TYPE, documentType, timeWait);
		String classShow = getAttributeValue(driver, Interfaces.UploaderPage.DYNAMIC_LENDER_PRIVATE_CHECKBOX_FOR_TYPE, documentType, "class");
		return !classShow.equals("ng-hide");
	}

	/**
	 * check Select All Checkbox
	 */
	public void checkSelectAllCheckbox() {
		waitForControl(driver, Interfaces.UploaderPage.SELECT_ALL_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.UploaderPage.SELECT_ALL_CHECKBOX);
		sleep(1);
	}

	/**
	 * click Make Public Link
	 */
	public void clickMakePublicLink() {
		waitForControl(driver, Interfaces.UploaderPage.MAKE_PUBLIC_LINK, timeWait);
		click(driver, Interfaces.UploaderPage.MAKE_PUBLIC_LINK);
		sleep(3);
		while (!isControlDisplayed(driver, Interfaces.UploaderPage.YES_BUTTON)) {
			click(driver, Interfaces.UploaderPage.MAKE_PUBLIC_LINK);
			sleep(2);
		}
	}

	/**
	 * check Public Document Type Display
	 */
	public boolean isPublicDocumentTypeDisplay(String documentType) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_PUBLIC_DOCUMENT_TYPE, documentType, 5);
		return isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_PUBLIC_DOCUMENT_TYPE, documentType);
	}

	/**
	 * click Make Lender Only Link
	 */
	public void clickMakeLenderOnlyLink() {
		waitForControl(driver, Interfaces.UploaderPage.MAKE_LENDER_ONLY_LINK, timeWait);
		click(driver, Interfaces.UploaderPage.MAKE_LENDER_ONLY_LINK);
		sleep(3);
		while (!isControlDisplayed(driver, Interfaces.UploaderPage.YES_BUTTON)) {
			click(driver, Interfaces.UploaderPage.MAKE_LENDER_ONLY_LINK);
			sleep(2);
		}
	}

	/**
	 * check Borrower Private Check mark For Type
	 */
	public boolean isDocumenTypeWithFileNameDisplay(String documentType, String documentFile) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE_WITH_FILE, documentType, documentFile, timeWait);
		return isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE_WITH_FILE, documentType, documentFile);
	}

	/**
	 * check Sub Folder Level3 Display
	 */
	public boolean isSubFolderLevel3Display(String subFolder) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_SUBFOLDER_LEVEL3, subFolder, timeWait);
		return isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_SUBFOLDER_LEVEL3, subFolder);
	}

	/**
	 * type Document Name
	 */
	public void selectPropertyOnEditDocument(String property) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_PROPERTY_ON_DOCUMENT_EDIT, property, timeWait);
		doubleClick(driver, Interfaces.UploaderPage.DYNAMIC_PROPERTY_ON_DOCUMENT_EDIT, property);
		sleep(3);
	}

	/**
	 * select Checkbox For Document Name
	 */
	public void selectCheckboxForDocumentName(String documentName) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_CHECKBOX_FOR_DOC_NAME, documentName, timeWait);
		checkTheCheckbox(driver, Interfaces.UploaderPage.DYNAMIC_CHECKBOX_FOR_DOC_NAME, documentName);
		sleep(2);
	}

	/**
	 * select Change Document Type Link
	 */
	public void selectChangeDocumentTypeLink() {
		waitForControl(driver, Interfaces.UploaderPage.CHANGE_DOCUMENT_TYPE_LINK, timeWait);
		click(driver, Interfaces.UploaderPage.CHANGE_DOCUMENT_TYPE_LINK);
		sleep(2);
		while (!isControlDisplayed(driver, Interfaces.UploaderPage.OK_BUTTON)) {
			click(driver, Interfaces.UploaderPage.CHANGE_DOCUMENT_TYPE_LINK);
			sleep(2);
		}
	}

	/**
	 * select Document Type On Edit Document Type
	 */
	public void selectDocumentTypeOnEditDocumentType(String documentType) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE_EDIT_DOCUMENT_TYPE, documentType, timeWait);
		click(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE_EDIT_DOCUMENT_TYPE, documentType);
		sleep(2);
	}

	/**
	 * click Delete Link
	 */
	public void clickDeleteLink() {
		waitForControl(driver, Interfaces.UploaderPage.DELETE_DOCUMENT_LINK, timeWait);
		click(driver, Interfaces.UploaderPage.DELETE_DOCUMENT_LINK);
		sleep(3);
		while (!isControlDisplayed(driver, Interfaces.UploaderPage.YES_BUTTON)) {
			click(driver, Interfaces.UploaderPage.DELETE_DOCUMENT_LINK);
			sleep(2);
		}
		sleep(5);
	}

	/**
	 * check Property Displays In Loan
	 */
	public boolean isPropertyDisplaysInLoan(String property) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_PROPERTY_FOLDER, property, 5);
		return isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_PROPERTY_FOLDER, property);
	}

	/**
	 * expand Property Group
	 */
	public void expandPropertyGroup(String propertyGroup) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_EXPAND_PROPERTY_GROUP_PLUS_BUTTON, propertyGroup, timeWait);
		click(driver, Interfaces.UploaderPage.DYNAMIC_EXPAND_PROPERTY_GROUP_PLUS_BUTTON, propertyGroup);
		sleep(2);
		if (!isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_EXPAND_PROPERTY_GROUP_MINUS_BUTTON, propertyGroup)) {
			click(driver, Interfaces.UploaderPage.DYNAMIC_EXPAND_PROPERTY_GROUP_PLUS_BUTTON, propertyGroup);
			sleep(2);
		}
		sleep(2);
	}

	/**
	 * select Sub Property Folder
	 */
	public void selectSubPropertyFolder(String propertyName) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_SUB_PROPERTY_FOLDER, propertyName, timeWait);
		doubleClick(driver, Interfaces.UploaderPage.DYNAMIC_SUB_PROPERTY_FOLDER, propertyName);
		sleep(5);
	}

	/**
	 * click Add Placeholder to All Properties
	 */
	public void clickAddPlaceholderToAllProperties() {
		waitForControl(driver, Interfaces.UploaderPage.ADD_PLACEHOLDER_TO_ALL_PROPERTIES_BUTTON, timeWait);
		click(driver, Interfaces.UploaderPage.ADD_PLACEHOLDER_TO_ALL_PROPERTIES_BUTTON);
		sleep(5);
	}

	/**
	 * click Change Grouped View
	 */
	public void clickChangeGroupedView() {
		if (isControlDisplayed(driver, Interfaces.UploaderPage.CHANGE_GROUPED_VIEW_TITLE)) {
			waitForControl(driver, Interfaces.UploaderPage.CHANGE_GROUPED_VIEW_TITLE, timeWait);
			click(driver, Interfaces.UploaderPage.CHANGE_GROUPED_VIEW_TITLE);
			sleep(10);
			if (driver.toString().toLowerCase().contains("internetexplorer")) {
				sleep(10);
			}
		}
		isControlDisplayed(driver, Interfaces.UploaderPage.CHANGE_UNGROUPED_VIEW_TITLE);
	}

	/**
	 * click Missing Documents Summary button
	 */
	public void clickMissingDocumentsSummaryButton() {
		waitForControl(driver, Interfaces.UploaderPage.MISSING_DOCUMENTS_SUMMARY_BUTTON, timeWait);
		click(driver, Interfaces.UploaderPage.MISSING_DOCUMENTS_SUMMARY_BUTTON);
		sleep(5);
		  if(driver.toString().toLowerCase().contains("internetexplorer")){
			  sleep();
              keyPressing("alt_s");
              keyPressing("alt_s");
              keyPressing("alt_s");
              sleep(5);
       }
	}

	/**
	 * click Download Documents button
	 */
	public void clickDownloadDocumentsButton() {
		waitForControl(driver, Interfaces.UploaderPage.DOWNLOAD_DOCUMENTS_BUTTON, timeWait);
		executeClick(driver, Interfaces.UploaderPage.DOWNLOAD_DOCUMENTS_BUTTON);
		sleep(15);
//		if (driver.toString().toLowerCase().contains("internetexplorer")) {
//			sleep(10);
//		}
//		waitForControl(driver, Interfaces.UploaderPage.DOWNLOAD_DOCUMENTS_BUTTON, timeWait);
//		click(driver, Interfaces.UploaderPage.DOWNLOAD_DOCUMENTS_BUTTON);
//		executeJavaScript(driver, "document.getElementByClassName('btn btn-warning').click();");
//		sleep(15);
//		while (isControlDisplayed(driver, Interfaces.UploaderPage.OK_BUTTON)) {
//			click(driver, Interfaces.UploaderPage.OK_BUTTON);
//			executeJavaScript(driver, "document.getElementByClassName('btn btn-primary').click();");
//			sleep(1);
//		}
//		sleep(3);
		
	}

	/**
	 * check Document Type displays in Download documents
	 */
	public boolean isDocumentTypeDisplaysInDownloadDocuments(String documentType) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE_IN_DOWNLOAD_DOCUMENT, documentType, timeWait);
		return isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE_IN_DOWNLOAD_DOCUMENT, documentType);
	}
	
	/**
	 * check download Generating message display
	 */
	public boolean isDownloadGeneratingMessageDisplays() {
		waitForControl(driver, Interfaces.UploaderPage.DOWNLOAD_GENERATING_MESSAGE, timeWait);
		return isControlDisplayed(driver, Interfaces.UploaderPage.DOWNLOAD_GENERATING_MESSAGE);
	}
	
	/**
	 * click Ok Button At Download Document
	 */
	public void clickOkButtonOnDownloadDocument() {
		waitForControl(driver, Interfaces.UploaderPage.OK_BUTTON, timeWait);
		executeClick(driver, Interfaces.UploaderPage.OK_BUTTON);
		sleep();
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * select Default Loan Status
	 */
	public void selectDefaultLoanStatus(String loanStatus) {
		waitForControl(driver, Interfaces.UploaderPage.DEFAULT_LOAN_STATUS_COMBOBOX, loanStatus, timeWait);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		selectItemCombobox(driver, Interfaces.UploaderPage.DEFAULT_LOAN_STATUS_COMBOBOX, loanStatus);
		sleep(15);
	}

	/**
	 * click Save Default Status button
	 */
	public void clickSaveDefaultStatusbutton() {
		waitForControl(driver, Interfaces.UploaderPage.SAVE_DEFAULT_STATUS_BUTTON, timeWait);
		doubleClick(driver, Interfaces.UploaderPage.SAVE_DEFAULT_STATUS_BUTTON);
		sleep(15);
	}

	/**
	 * check Default Loan Status Selected Display On Uploader
	 */
	public boolean isDefaultLoanStatusSelectedDisplayOnUploader(String loanStatus) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DEFAULT_LOAN_STATUS_SELECTED_DASHBOARD, loanStatus, timeWait);
		return isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_DEFAULT_LOAN_STATUS_SELECTED_DASHBOARD, loanStatus);
	}

	/**
	 * type Document Name
	 */
	public void typeDocumentName(String documentName) {
		waitForControl(driver, Interfaces.UploaderPage.DOCUMENT_NAME_TEXTBOX_EDIT_DOCUMENT, timeWait);
		type(driver, Interfaces.UploaderPage.DOCUMENT_NAME_TEXTBOX_EDIT_DOCUMENT, documentName);
		sleep(2);
	}

	// /**
	// * Merge 'Unknown' type to an Empty placeholder
	// */
	// public void dragAndDropUnknownTypeToAnEmptyPlaceholder(String
	// documentName, String documentType) {
	// waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TITLE,
	// documentName, timeWait);
	// waitForControl(driver,
	// Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TITLE_TARGET_DROG, documentType,
	// timeWait);
	// dragAndDrop(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TITLE,
	// documentName, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TITLE_TARGET_DROG,
	// documentType);
	// sleep(5);
	// }

	/**
	 * Merge 'Unknown' type to an Empty placeholder
	 */
	public void dragAndDropUnknownTypeToAnEmptyPlaceholder() {
		// waitForControl(driver,
		// Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TITLE, timeWait);
		// waitForControl(driver,
		// Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TITLE_TARGET_DROG,
		// timeWait);
		dragAndDrop(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TITLE, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TITLE_TARGET_DROG);
		// sleep(5);
		WebElement From = driver.findElement(By.xpath("//*[@id='form1']/div[3]/div[2]/div[1]/table/tbody[2]/tr[3]/td[5]/span[1]"));
		WebElement To = driver.findElement(By.xpath("//*[@id='form1']/div[3]/div[2]/div[1]/table/tbody[2]/tr[2]/td[4]"));
		Actions builder = new Actions(driver);
		builder.clickAndHold(From).moveToElement(To).release(To).build().perform();
		sleep(5);

		builder.keyDown(Keys.CONTROL).click(From).click(To).keyUp(Keys.CONTROL);
		// Then get the action
		Action selectMultiple = builder.build();
		// And execute it
		selectMultiple.perform();
	}
	
//	public String getKeywordForAutoCast(String keywordName)
//	{
////		List<WebElement> elementList;
//		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_KEYWORD_FOR_AUTOCAST, keywordName, timeWait);
////		elementList = getElements(driver, Interfaces.UploaderPage.DYNAMIC_KEYWORD_FOR_AUTOCAST, keywordName);
////		for (WebElement element: elementList) {
////			System.out.println(element.getAttribute("value"));
////			if (element.getAttribute("value")!=""){ 
////				System.out.println(element.getAttribute("value"));
////				return element.getAttribute("value");
////			}
////		}
////		return "Not found";
//
//		return getAttributeValueOfList(driver, Interfaces.UploaderPage.DYNAMIC_KEYWORD_FOR_AUTOCAST, keywordName, "value");
//	}
	
	public String getKeywordForAutoCast(String keywordName)
	{
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_KEYWORD_FOR_AUTOCAST, keywordName, timeWait);
		return getAttributeValueOfList(driver, Interfaces.UploaderPage.DYNAMIC_KEYWORD_FOR_AUTOCAST, keywordName, "value");
	}
	
	/**
	 * Upload file to documents
	 */
	public void UploadFileToPropertyByNumbericOrder(int startNumber, int endNumber) {
		for(int i= startNumber; i<=endNumber; i++){
			String number = Integer.toString(i);
			waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_UPLOADFILE_BUTTON_FOR_NUMBERING, number, timeWait);
			click(driver, Interfaces.UploaderPage.DYNAMIC_UPLOADFILE_BUTTON_FOR_NUMBERING, number);
			sleep(5);
			uploadFileOnUploader("datatest.pdf");
			waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENTS_FILE_FOR_NUMBERING, number, "datatest.pdf", timeWait*5);
			if (!isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_NAMES, "datatest.pdf")){
				click(driver, Interfaces.UploaderPage.DYNAMIC_UPLOADFILE_BUTTON_FOR_NUMBERING, number);
				sleep(2);
				uploadFileOnUploader("datatest.pdf");
			}
			
			waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENTS_FILE_FOR_NUMBERING, number, "datatest.pdf", timeWait*5);
       }
		
	}
	
	/**
	 * is Move Documents Button enabled
	 */
	public Boolean isMoveDocumentsButtonDisabled(){
		waitForControl(driver, Interfaces.UploaderPage.MOVE_DOCUMENTS_LINK, timeWait);
		return getAttributeValue(driver, Interfaces.UploaderPage.MOVE_DOCUMENTS_LINK, "class").equals("disabled");
	}
	
	/**
	 * is Copy Documents Button enabled
	 */
	public Boolean isCopyDocumentsButtonDisabled(){
		waitForControl(driver, Interfaces.UploaderPage.COPY_DOCUMENTS_LINK, timeWait);
		return getAttributeValue(driver, Interfaces.UploaderPage.COPY_DOCUMENTS_LINK, "class").equals("disabled");
	}
	
	/**
	 * Click Move Documents Button
	 */
	public void clickMoveDocumentsButton(){
		waitForControl(driver, Interfaces.UploaderPage.MOVE_DOCUMENTS_LINK, timeWait);
		click(driver, Interfaces.UploaderPage.MOVE_DOCUMENTS_LINK);
		sleep();
	}
	
	/**
	 * Click Copy Documents Button 
	 */
	public void clickCopyDocumentsButton(){
		waitForControl(driver, Interfaces.UploaderPage.COPY_DOCUMENTS_LINK, timeWait);
		click(driver, Interfaces.UploaderPage.COPY_DOCUMENTS_LINK);
		sleep();
	}
	
	/**
	 * click Ok Button On Copy and Move Document
	 */
	public void clickOkButtonOnCopyAndMoveDocument() {
		waitForControl(driver, Interfaces.UploaderPage.OK_BUTTON, timeWait);
		click(driver, Interfaces.UploaderPage.OK_BUTTON);
		executeJavaScript(driver, "document.getElementByClassName('btn btn-primary').click();");
		sleep(2);
		while (isControlDisplayed(driver, Interfaces.UploaderPage.OK_BUTTON)) {
			click(driver, Interfaces.UploaderPage.OK_BUTTON);
			executeJavaScript(driver, "document.getElementByClassName('btn btn-primary').click();");
			sleep(1);
		}
		sleep(3);
	}
	
	/**
	 * get Number of Documents
	 */
	public int getNumberOfDocuments(){
		sleep(2);
		waitForControl(driver, Interfaces.UploaderPage.DOCUMENT_TYPES, 10);
		if(!isControlDisplayed(driver, Interfaces.UploaderPage.DOCUMENT_TYPES)) return 0;
		else return countElement(driver, Interfaces.UploaderPage.DOCUMENT_TYPES);
	}
	
	/**
	 * Check document files is displayed
	 */
	public Boolean checkDocumentFilesUploadedByNumbericOrder(int startNumber, int endNumber) {
		sleep();
		if(isControlDisplayed(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_NAMES, "datatest.pdf")) {
				if(countElement(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_NAMES,"datatest.pdf")==endNumber) 
				return true;
       }
		return false;
	}
	
	/**
	 * select Sub Folder for copy and move
	 */
	public void selectFolderForCopyAndMove(String subFolder) {
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_SUBFOLDER_LEVEL2, subFolder, timeWait);
		click(driver, Interfaces.UploaderPage.DYNAMIC_SUBFOLDER_LEVEL2, subFolder);
		sleep(2);
	}

	/**
	 * click Ok Button On Download Document
	 */
	public void clickOkButtonOnDownloadDocument2() {
		
		waitForControl(driver, Interfaces.UploaderPage.OK_BUTTON, timeWait);
		click(driver, Interfaces.UploaderPage.OK_BUTTON);
		executeJavaScript(driver, "document.getElementByClassName('btn btn-primary').click();");
		sleep(2);
		while (isControlDisplayed(driver, Interfaces.UploaderPage.OK_BUTTON)) {
			click(driver, Interfaces.UploaderPage.OK_BUTTON);
			executeJavaScript(driver, "document.getElementByClassName('btn btn-primary').click();");
			sleep(1);
		}
		sleep(3);
	}
	
	public void observePlaceholder(String documentType){
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE_WITHOUT_DOCUMENT, documentType, timeWait);
		observeControl(driver, Interfaces.UploaderPage.DYNAMIC_DOCUMENT_TYPE_WITHOUT_DOCUMENT, documentType);
	}
	
	public void observeFolderLevel2(String folderName){
		waitForControl(driver, Interfaces.UploaderPage.DYNAMIC_SUBFOLDER_LEVEL2, folderName, timeWait);
		observeControl(driver, Interfaces.UploaderPage.DYNAMIC_SUBFOLDER_LEVEL2, folderName);
	}
	
	private WebDriver driver;
	private String ipClient;
	private final Stack<String> openWindowHandles = new Stack<String>();
}