package sanityTest_Uploader;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_043_B2R_Applicant_MultiDocumentInUploader extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		usernameApplicant = Constant.USERNAME_B2R_BORROWER;
		passwordApplicant = Constant.PASSWORD_B2R_BORROWER;
		loanName = "UploaderUdiTesting"+getUniqueNumber();
		applicantName = "Consolidation Demo Borrower";
		loanStatus = Constant.LOAN_STATUS_B2R;
		propertiesFileName = "B2RDataTape_multi-unit.xlsx";
		bpoDocName = "10105 Spring ST- P2.pdf";
		evidenceInsuranceDocName = "10105 Spring ST- P3.pdf";
		bpoDocType = "P2 - Lease Agreement";
		evidenceInsuranceDocType = "P3 - Purchase Contract/HUD-1 Settlement Stmt";
		newDocumentType = "P5 - Zoning Compliance Letter";
		propertyGroup = "Oakhill";
		propertyName = "10105 Spring ST";
		propertyName2 = "37225 OAKHILL ST - suite 19";
		bpoDocNameMulti = "37225 OAKHILL ST - suite 19- P10.pdf";
		bpoDocTypeMulti = "P10 - Lead-Based Paint Disclosure";
		bpoDocNameParent = "Oakhill- P10.pdf";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}
	
	@Test(groups = { "regression" },description = "UploaderApplicant09 - AutoCategorizing document with keyword")
	public void Uploader_10_AutoCategorizingDocumentWithKeyword()
	{
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("Precondition 02. Create new loans and get loans' id");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);
		
		log.info("Precondition 03. Upload Property data");
		loansPage.openPropertiesTab();
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("Precondition 04. Click on logged user name on the top right corner");
//	    loansPage.clickOnLoggedUserName( driver );
//	    uploaderPageUrl = loansPage .getUploaderPageUrl( driver);
//		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
		
		log.info("Precondition 05. Go to Uploader");
		uploaderLoginPage = homePage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("Precondition 06. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameApplicant, passwordApplicant);
		
		log.info("Uploader_10 - Step 01. Select loans item");
		uploaderPage.clickChangeGroupedView();
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("Uploader_10 - Step 02. Select folder 'Uncategorized Property Documents'");
		uploaderPage.selectSubFolderLevel2("Uncategorized Property Documents");
		
		log.info("Uploader_10 - Step 03. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherDocuments();
		
		log.info("Uploader_10 - Step 04. Select file to upload");
		uploaderPage.uploadFileOnUploader(bpoDocName);
		
		log.info("Uploader_10 - Step 05. Click 'OK' Button");
		uploaderPage.waitForUploadCompleteOnUploader(bpoDocName);

		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(bpoDocName));
		
		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(bpoDocName, "Unknown"));
		
		log.info("Uploader_10 - Step 06. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherDocuments();
		
		log.info("Uploader_10 - Step 07. Select file to upload");
		uploaderPage.uploadFileOnUploader(evidenceInsuranceDocName);
		
		log.info("Uploader_10 - Step 08. Click 'OK' Button");
		uploaderPage.waitForUploadCompleteOnUploader(evidenceInsuranceDocName);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(evidenceInsuranceDocName));
		
		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(evidenceInsuranceDocName, "Unknown"));
		
		log.info("Uploader_10 - Step 09. Click 'Auto categorize documents' button");
		uploaderPage.clickAutocategorizeDocumentsButton();
		
		log.info("Uploader_10 - Step 10. Click 'OK Autocategorization result' button");
		uploaderPage.clickOkAutocategorizationResultButton();
		
		log.info("VP: Deeds document is categorized");
		verifyFalse(uploaderPage.isDocumentDisplay(bpoDocName));
				
		log.info("VP: Evidence of Insurance document is categorized");
		verifyFalse(uploaderPage.isDocumentDisplay(evidenceInsuranceDocName));
		
		log.info("Uploader_10 - Step 11. Click Property folder");
		uploaderPage.clickPropertiesFolder();
		
		log.info("Uploader_10 - Step 12. Select '10105 Spring ST' property");
		uploaderPage.selectPropertyFolder(propertyName);
		
		log.info("VP: These document is added to property");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(bpoDocName, bpoDocType));
		verifyTrue(uploaderPage.isDocumentTypeDisplay(evidenceInsuranceDocName, evidenceInsuranceDocType));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender14 - Attach a document through autocat to a parent")
	public void Uploader_11_AutoCategorizingDocumentToParent()
	{
		log.info("Uploader_11 - Step 01. Select folder 'Uncategorized Property Documents'");
		uploaderPage.selectSubFolderLevel2("Uncategorized Property Documents");
		
		log.info("Uploader_11 - Step 02. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherDocuments();
		
		log.info("Uploader_11 - Step 03. Select file to upload");
		uploaderPage.uploadFileOnUploader(bpoDocNameParent);
		
		log.info("Uploader_11 - Step 04. Click 'OK' Button");
		uploaderPage.waitForUploadCompleteOnUploader(bpoDocNameParent);
		
		if(!driver.toString().toLowerCase().contains("internetexplorer")){
			log.info("VP: The document is uploaded successfully");
			verifyTrue(uploaderPage.isDocumentDisplay(bpoDocNameParent));
		}

		log.info("VP: The document type display correctly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(bpoDocNameParent, "Unknown"));
		
		log.info("Uploader_11 - Step 05. Click 'Auto categorize documents' button");
		uploaderPage.clickAutocategorizeDocumentsButton();
		
		log.info("Uploader_11 - Step 06. Click 'OK Autocategorization result' button");
		uploaderPage.clickOkAutocategorizationResultButton();
		
		log.info("VP: Oakhill document is categorized");
		verifyFalse(uploaderPage.isDocumentDisplay(bpoDocNameParent));
		
		log.info("Uploader_11 - Step 07. Select 'Oakhill' property");
		uploaderPage.selectPropertyFolder(propertyGroup);
		
		log.info("VP: These document is added to property");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(bpoDocNameParent, bpoDocTypeMulti));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender15 - Attach a document through autocat to a unit in a multi-unit")
	public void Uploader_12_AutoCategorizingDocumentToMultiUnit()
	{
		log.info("Uploader_12 - Step 01. Select folder 'Uncategorized Property Documents'");
		uploaderPage.selectSubFolderLevel2("Uncategorized Property Documents");
		
		log.info("Uploader_12 - Step 02. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherDocuments();
		
		log.info("Uploader_12 - Step 03. Select file to upload");
		uploaderPage.uploadFileOnUploader(bpoDocNameMulti);
		
		log.info("Uploader_12 - Step 04. Click 'OK' Button");
		uploaderPage.waitForUploadCompleteOnUploader(bpoDocNameMulti);
		
		if(!driver.toString().toLowerCase().contains("internetexplorer")){
			log.info("VP: The document is uploaded successfully");
			verifyTrue(uploaderPage.isDocumentDisplay(bpoDocNameMulti));
		}

		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(bpoDocNameMulti, "Unknown"));
		
		log.info("Uploader_12 - Step 05. Click 'Auto categorize documents' button");
		uploaderPage.clickAutocategorizeDocumentsButton();
		
		log.info("Uploader_12 - Step 06. Click 'OK Autocategorization result' button");
		uploaderPage.clickOkAutocategorizationResultButton();
		
		log.info("VP: Lease Agreement document is categorized");
		verifyFalse(uploaderPage.isDocumentDisplay(bpoDocNameMulti));
		
		log.info("Uploader_12 - Step 07. Expand property group");
		uploaderPage.expandPropertyGroup(propertyGroup);
		
		log.info("Uploader_12 - Step 08. Select '37225 OAKHILL ST - suite 19' property");
		uploaderPage.selectSubPropertyFolder(propertyName2);
		
		log.info("VP: These document is added to property");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(bpoDocNameMulti, bpoDocTypeMulti));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender15 - Multi change document type")
	public void Uploader_13_MultiChangeDocumentType()
	{
		log.info("Uploader_13 - Step 01. Select document");
		uploaderPage.selectPropertyFolder(propertyName);
		uploaderPage.selectCheckboxForDocumentName(bpoDocName);
		uploaderPage.selectCheckboxForDocumentName(evidenceInsuranceDocName);
		
		log.info("Uploader_13 - Step 02. Click on change document type link");
		uploaderPage.selectChangeDocumentTypeLink();
		
		log.info("Uploader_13 - Step 03. Select new document type");
		uploaderPage.selectDocumentTypeOnEditDocumentType(newDocumentType);
		
		log.info("Uploader_13 - Step 04. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.clickYesButton();
		
		log.info("VP: These document is changed type");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(bpoDocName, newDocumentType));
		verifyTrue(uploaderPage.isDocumentTypeDisplay(evidenceInsuranceDocName, newDocumentType));
		verifyFalse(uploaderPage.isDocumentTypeDisplay(bpoDocName, bpoDocType));
		verifyFalse(uploaderPage.isDocumentTypeDisplay(evidenceInsuranceDocName, evidenceInsuranceDocType));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender23 - Change document type but then in the dialogue click 'no'")
	public void Uploader_14_ChangeDocumentTypeButThenClickNo()
	{
		log.info("Uploader_14 - Step 01. Select document");
		uploaderPage.selectPropertyFolder(propertyName);
		uploaderPage.selectCheckboxForDocumentName(bpoDocName);
		
		log.info("Uploader_14 - Step 02. Click on change document type link");
		uploaderPage.selectChangeDocumentTypeLink();
		
		log.info("Uploader_14 - Step 03. Select new document type");
		uploaderPage.selectDocumentTypeOnEditDocumentType(bpoDocType);
		
		log.info("Uploader_14 - Step 04. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.clickNoButton();
		
		log.info("VP: The document type is not changed");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(bpoDocName, newDocumentType));
		verifyFalse(uploaderPage.isDocumentTypeDisplay(bpoDocName, bpoDocType));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender, usernameApplicant, propertyGroup;
	private String loanName, uploaderPageUrl, propertiesFileName, newDocumentType, propertyName;
	private String bpoDocName, evidenceInsuranceDocName, bpoDocType, passwordApplicant, propertyName2, bpoDocNameParent;
	private String evidenceInsuranceDocType, loanStatus, applicantName, bpoDocNameMulti, bpoDocTypeMulti;
}