package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Constant;
import page.DocumentsPage;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import page.UsersPage;

public class sanityTest_045_B2R_ExportCSVFile extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower";
		borrowerName = "UdiTeamBorrower";
		address1 = "37412 OAKHILL ST";
		documentType1 = "P2 - Lease Agreement";
		documentFileName = "datatest.pdf";
		propertiesFileNameRequired = "B2R Required.xlsx";
		documentSection1 = "General Loan Documents - Operational";
		documentType6 = "L24 - Schedule of Capital improvements";
		documentSection2 = "Corporate Entity Documents - Applicant";
		documentType7 = "L2 - Executed Borrower Term Sheet";
		newLoanStatus = "Rent Roll Received";
		finalLLC = "100";
		
	}

	@Test(groups = { "regression" }, description = "Send Information 01 - Send information by email at Users page")
	public void ExportCSVFile_01_UserPage() {

		log.info("ExportCSVFile_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("ExportCSVFile_01 - Step 02. Go to User Page");
		usersPage = homePage.openUsersPage(driver, ipClient);
		
		log.info("ExportCSVFile_01 - Step 03. Get first user information");
		userName = usersPage.getCellContentByCellID(driver,"DataCell FieldTypeForeignKey datacell_Borrower");
		entityType = usersPage.getCellContentByCellID(driver,"DataCell FieldTypeForeignKey datacell_EntityType");
		loanIndex = usersPage.getCellContentByCellID(driver,"DataCell FieldTypeInteger datacell_NOfLoans");
		userLogin = usersPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_UserLogin");
		email = usersPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_Mail");
		city = usersPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_BorrowerCity");
		state = usersPage.getCellContentByCellID(driver,"DataCell FieldTypeState datacell_BorrowerState");
		
		log.info("ExportCSVFile_01 - Step 04. Click on Download CSV button");
		usersPage.deleteAllFileInFolder();
		usersPage.clickOnDownloadCsvFileButton(driver);

		log.info("ExportCSVFile_01 - Step 05. Wait for file downloaded complete");
		usersPage.waitForDownloadFileFullnameCompleted("csv");

		log.info("VP. Records are displayed correctly in CSV file");
		verifyTrue(usersPage.isRecordsExportedCorrectly(usersPage.getFileNameInDirectory(), userName, "User Legal Name", usersPage.getNumberOfRecords(driver), entityType, loanIndex, userLogin, email, city, state));
		
		log.info("ExportCSVFile_01 - Step 07. Delete the downloaded file");
		usersPage.deleteContainsFileName("csv");
	}
	
	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Loans Property tab")
	public void ExportCSVFile_02_LoansPropertiesTab() {
		
		log.info("Pre-condition 01. Go to Loans Page");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Pre-condition 02. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, borrowerName, loanStatus, loanName);
		
		log.info("Precondition 03. Input data to 'Final Borrower LLC, Loan Application Status' fields");
		loansPage.inputFinalBorrowerAndFundedStatus(finalLLC, newLoanStatus);

		log.info("Precondition 03. Update Midland ID");
		loansPage.inputMidlandID(accountNumber);
		
		log.info("Precondition 04. Click Save button");
		loansPage.clickSaveButton();
		loansPage.isRecordSavedMessageDisplays();

		log.info("Pre-condition 05. Open 'Properties' tab");
		loansPage.openPropertiesTab();

		log.info("Pre-condition 06. Load test file");
		loansPage.uploadFileProperties(propertiesFileNameRequired);
		
		log.info("Pre-condition 07. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("Pre-condition 08. Add document through the 'New' button");
		propertiesPage.clickNewDocumentButton();
		propertiesPage.selectDocumentType(documentType1);
		propertiesPage.uploadDocumentFile(documentFileName);
		propertiesPage.uncheckPrivateCheckbox();
		propertiesPage.clickSaveButton();
		propertiesPage.clickOnMarkAsReviewedButton();
		
		log.info("Pre-condition 08 - Step 01. Get all credentials for check export Documents");
		addedOnTime = propertiesPage.getAddedOnTime();
		username = propertiesPage.getReviewedByUser();
		submittedMethod = propertiesPage.getUploadMethod();
		reviewedTime = propertiesPage.getReviewedTime();
		
		propertiesPage.clickGoToPropertyButton();
		
		log.info("VP: Document is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, documentFileName));

		log.info("Pre-condition 09. Open 'General Loan Documents' page");
		documentsPage = propertiesPage.openGeneralDocumentTab();

		log.info("Pre-condition 10. New 'General Loan Document' file");
		documentsPage.clickNewButton();
		documentsPage.selectGeneralDocumentSection(documentSection1);
		documentsPage.selectGeneralDocumentType(documentType6);
		documentsPage.uploadDocumentFile(documentFileName);
		documentsPage.clickSaveButton();
		addedOnTimeGeneralLoanDocument = documentsPage.getAddedOnTime();
		documentsPage.clickOnMarkAsReviewedButton();
				
		log.info("Pre-condition 11. Open 'Corporate Entity Documents' page");
		documentsPage.openCorporateEntityDocumentsTab();
		
		log.info("Pre-condition 12. New 'Corporate Entity Documents' file");
		documentsPage.clickNewButton();
		documentsPage.selectGeneralDocumentSection(documentSection2);
		documentsPage.selectGeneralDocumentType(documentType7);
		documentsPage.uploadDocumentFile(documentFileName);
		documentsPage.clickSaveButton();
		addedOnTimeCorporateEntityDocument = documentsPage.getAddedOnTime();
		documentsPage.clickOnMarkAsReviewedButton();

		log.info("ExportCSVFile_02 - Step 01. Click Document List button");
		loansPage = documentsPage.clickOnPropertyTab();
		
		addressProperty = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_AddressLine1");
		cityProperty = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_City");
		stateProperty = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeState datacell_State");
		zipProperty = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeZipCode datacell_Zip");
		applicantProperty = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeForeignKey datacell_Borrower");
		dateModifiedProperty = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeDate datacell_LastModifiedDate", 1);
		assetIDProperty = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeInteger datacell_LoanPropertyID");
		activeProperty = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeBoolean datacell_IsActive");
		documentsProperty = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_DocumentsNew");
		reviewedProperty = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeVarchar datacell_ReviewsNew");
		propertyTypeProperty = propertiesPage.getTextPropertiesSearch("DataCell FieldTypeValidValues datacell_PropertyType");
		
		log.info("ExportCSVFile_02 - Step 02. Click on Download CSV button");
		loansPage.deleteAllFileInFolder();
		loansPage.clickOnDownloadCsvFileButton(driver);

		log.info("ExportCSVFile_02 - Step 03. Wait for file downloaded complete");
		loansPage.waitForDownloadFileFullnameCompleted("csv");

		log.info("VP. Records are displayed correctly in CSV file");
		verifyTrue(loansPage.isRecordsExportedCorrectly(loansPage.getFileNameInDirectory(), addressProperty, "Address", usersPage.getNumberOfRecords(driver), zipProperty, propertyTypeProperty, applicantProperty, dateModifiedProperty, assetIDProperty, documentsProperty, activeProperty, reviewedProperty, cityProperty, stateProperty));
		
		log.info("ExportCSVFile_02 - Step 04. Delete the downloaded file");
		loansPage.deleteContainsFileName("csv");
	}

	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Loans Properties Documents page")
	public void ExportCSVFile_03_LoansPropertyDocumentsTab() {
		
		log.info("ExportCSVFile_03 - Step 01. Click Property Document Tab");
		loansPage.openPropertyDocumentsTab();
		
		log.info("ExportCSVFile_03 - Step 02. Get first Documents information");
		documentIndexProperty = usersPage.getCellContentByCellID(driver,"DataCell FieldTypeSQLServerIdentity datacell_Document");
		documentLoaded = usersPage.getCellContentByCellID(driver,"DataCell FieldTypeValidValues datacell_IsLoaded");
		addedOn = usersPage.getCellContentByCellID(driver,"DataCell FieldTypeDateTime datacell_Attached", 1);
		reviewedBy = usersPage.getCellContentByCellID(driver,"DataCell FieldTypeForeignKey datacell_ReviewBy");
		reviewedOn = usersPage.getCellContentByCellID(driver,"DataCell FieldTypeDateTime datacell_LastReviewed", 1);
		active = usersPage.getCellContentByCellID(driver,"DataCell FieldTypeValidValues datacell_IsActive2");
		address = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_AddressLine");

		
		log.info("ExportCSVFile_03 - Step 03. Click on Download CSV button");
		loansPage.deleteAllFileInFolder();
		loansPage.clickOnDownloadCsvFileButton(driver);

		log.info("ExportCSVFile_03 - Step 04. Wait for file downloaded complete");
		loansPage.waitForDownloadFileFullnameCompleted("csv");

		log.info("VP. Records are displayed correctly in CSV file");
		verifyTrue(loansPage.isRecordsExportedCorrectly(loansPage.getFileNameInDirectory(), documentIndexProperty, "Document #", usersPage.getNumberOfRecords(driver), documentLoaded, addedOn, reviewedBy, reviewedOn, active, address));
		
		log.info("ExportCSVFile_03 - Step 05. Delete the downloaded file");
		loansPage.deleteContainsFileName("csv");
	}
	
	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Loans General Document Tab")
	public void ExportCSVFile_04_GeneralDocumentsTab() {
		
		log.info("ExportCSVFile_04 - Step 01. Click General Document Tab");
		documentsPage = loansPage.openGeneralDocumentTab();
		documentsPage.searchByAddedOnTime(addedOnTimeGeneralLoanDocument);
		
		log.info("ExportCSVFile_04 - Step 02. Get first Documents information");
		documentIndex = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeSQLServerIdentity datacell_Document");
		documentLoaded = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeValidValues datacell_IsLoaded");
		addedOn = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeDateTime datacell_Attached", 1);
		reviewedBy = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeForeignKey datacell_ReviewBy");
		reviewedOn = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeDateTime datacell_LastReviewed", 1);
		active = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeValidValues datacell_IsActive2");
		
		log.info("ExportCSVFile_04 - Step 03. Click on Download CSV button");
		documentsPage.deleteAllFileInFolder();
		documentsPage.clickOnDownloadCsvFileButton(driver);

		log.info("ExportCSVFile_04 - Step 04. Wait for file downloaded complete");
		documentsPage.waitForDownloadFileFullnameCompleted("csv");

		log.info("VP. Records are displayed correctly in CSV file");
		verifyTrue(documentsPage.isRecordsExportedCorrectly(loansPage.getFileNameInDirectory(), documentIndex, "Document #", usersPage.getNumberOfRecords(driver), documentLoaded, addedOn, reviewedBy, reviewedOn, active, documentSection1, documentType6));
		
		log.info("ExportCSVFile_04 - Step 05. Delete the downloaded file");
		documentsPage.deleteContainsFileName("csv");
		
	}
	
	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Loans Corporate Entity Documents Tab")
	public void ExportCSVFile_05_CorporateEntityDocumentsTab() {
		
		log.info("ExportCSVFile_05 - Step 01. Click Corporate Entity Documents Tab");
		documentsPage.openCorporateEntityDocumentsTab();
		documentsPage.searchByAddedOnTime(addedOnTimeCorporateEntityDocument);
		
		log.info("ExportCSVFile_05 - Step 02. Get first Documents information");
		documentIndex = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeSQLServerIdentity datacell_Document");
		documentLoaded = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeValidValues datacell_IsLoaded");
		addedOn = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeDateTime datacell_Attached", 1);
		reviewedBy = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeForeignKey datacell_ReviewBy");
		reviewedOn = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeDateTime datacell_LastReviewed", 1);
		active = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeValidValues datacell_IsActive2");
		section = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_ViewSection");

		log.info("ExportCSVFile_05 - Step 03. Click on Download CSV button");
		documentsPage.deleteAllFileInFolder();
		documentsPage.clickOnDownloadCsvFileButton(driver);

		log.info("ExportCSVFile_05 - Step 04. Wait for file downloaded complete");
		documentsPage.waitForDownloadFileFullnameCompleted("csv");

		log.info("VP. Records are displayed correctly in CSV file");
		verifyTrue(documentsPage.isRecordsExportedCorrectly(loansPage.getFileNameInDirectory(), documentIndex, "Document #", usersPage.getNumberOfRecords(driver), documentLoaded, addedOn, reviewedBy, reviewedOn, active, section, documentType7));
		
		log.info("ExportCSVFile_05 - Step 05. Delete the downloaded file");
		documentsPage.deleteContainsFileName("csv");
	}
	
	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Loans Loaded Data Tapes Tab")
	public void ExportCSVFile_06_LoadedDataTapesTab() {
		
		log.info("ExportCSVFile_06 - Step 01. Click Loaded Data Tapes Tab");
		documentsPage = loansPage.openLoadDataTapesTab();
		
		log.info("ExportCSVFile_06 - Step 03. Get first Loaded datatape information");
		addedNumber = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeInteger datacell_AddedPropertys");
		ignoredNumber = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeInteger datacell_UpdatedPropertys");
		updatedNumber = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeInteger datacell_IgnoredFileRecords");
		
		log.info("ExportCSVFile_06 - Step 04. Click on Download CSV button");
		documentsPage.deleteAllFileInFolder();
		documentsPage.clickOnDownloadCsvFileButton(driver);

		log.info("ExportCSVFile_06 - Step 05. Wait for file downloaded complete");
		documentsPage.waitForDownloadFileFullnameCompleted("csv");

		log.info("VP. Records are displayed correctly in CSV file");
		verifyTrue(documentsPage.isCSVFileExportedCorrectly(usersPage.getFileNameInDirectory(), addedNumber, "Added"));
		verifyTrue(documentsPage.isCSVFileExportedCorrectly(usersPage.getFileNameInDirectory(), ignoredNumber, "Updated"));
		verifyTrue(documentsPage.isCSVFileExportedCorrectly(usersPage.getFileNameInDirectory(), updatedNumber, "Ignored"));
		
		log.info("ExportCSVFile_06 - Step 06. Delete the downloaded file");
		documentsPage.deleteContainsFileName("csv");
	}
	
	@Test(groups = { "regression" }, description = "Send Information 02 - Send information by email at Properties page")
	public void ExportCSVFile_07_PropertiesPage() {
		
		log.info("ExportCSVFile_07 - Step 01. Go to Properties Page");
		propertiesPage = homePage.openPropertiesPage(driver, ipClient);
		
		log.info("ExportCSVFile_07 - Step 02. Click on Download CSV button");
		propertiesPage.deleteAllFileInFolder();
		propertiesPage.clickOnDownloadCsvFileButton(driver);

		log.info("ExportCSVFile_07 - Step 03. Wait for file downloaded complete");
		propertiesPage.waitForDownloadFileFullnameCompleted("csv");

		log.info("VP. Records are displayed correctly in CSV file");
		verifyTrue(propertiesPage.isRecordsExportedCorrectly(loansPage.getFileNameInDirectory(), assetIDProperty, "Asset ID", usersPage.getNumberOfRecords(driver), zipProperty, addressProperty, applicantProperty, dateModifiedProperty, assetIDProperty, documentsProperty, activeProperty, reviewedProperty, cityProperty, stateProperty));
		
		log.info("ExportCSVFile_07 - Step 04. Delete the downloaded file");
		propertiesPage.deleteContainsFileName("csv");
	}
	
	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Loans page")
	public void ExportCSVFile_08_LoansPage() {
		
		log.info("ExportCSVFile_08 - Step 01. Go to Loans Page");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		log.info("ExportCSVFile_08 - Step 02. Search by Legal Name");
		loansPage.searchLoanByName(loanName);
		
		numberProperties = loansPage.getTextLoanSearch("DataCell FieldTypeInteger datacell_TotProp");
		totalEstimatedValue = loansPage.getTextLoanSearch("DataCell FieldTypeCurrency datacell_TotVal",2);
		loanStatusChangeDate = loansPage.getTextLoanSearch("DataCell FieldTypeDate datacell_StatusChangeDate", 1);
		dateCreated = loansPage.getTextLoanSearch("DataCell FieldTypeDateTime datacell_CreateDate", 1);
		active = loansPage.getTextLoanSearch("DataCell FieldTypeBoolean datacell_IsActive");
		
		log.info("Precondition 09. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);
		
		log.info("ExportCSVFile_08 - Step 03. Click on Download CSV button");
		loansPage.deleteAllFileInFolder();
		loansPage.clickOnDownloadCsvFileButton(driver);

		log.info("ExportCSVFile_08 - Step 04. Wait for file downloaded complete");
		loansPage.waitForDownloadFileFullnameCompleted("csv");

		log.info("VP. Records are displayed correctly in CSV file");
		verifyTrue(loansPage.isRecordsExportedCorrectly(loansPage.getFileNameInDirectory(), loanName, "Legal Name", usersPage.getNumberOfRecords(driver), applicantName, finalLLC, newLoanStatus, accountNumber, numberProperties, totalEstimatedValue, loanStatusChangeDate, dateCreated, active));
		
		log.info("ExportCSVFile_08 - Step 05. Delete the downloaded file");
		loansPage.deleteContainsFileName("csv");
	}
	
	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Documents page")
	public void ExportCSVFile_09_DocumentsPage() {
		
		log.info("ExportCSVFile_09 - Step 01. Go to Documents Page");
		documentsPage = homePage.openDocumentsPage(driver, ipClient);
		
		log.info("ExportCSVFile_09 - Step 02. Search by Added on time");
		documentsPage.searchByAddedOnTime(addedOnTime);
		documentIndex = documentsPage.getTextDocumentsSearch("DataCell FieldTypeSQLServerIdentity datacell_Document");
		active = documentsPage.getTextDocumentsSearch("DataCell FieldTypeBoolean datacell_IsActive");
		address = documentsPage.getTextDocumentsSearch("DataCell FieldTypeVarchar datacell_PropDisp");
		
		log.info("ExportCSVFile_09 - Step 03. Click on Download CSV button");
		loansPage.deleteAllFileInFolder();
		loansPage.clickOnDownloadCsvFileButton(driver);

		log.info("ExportCSVFile_09 - Step 04. Wait for file downloaded complete");
		loansPage.waitForDownloadFileFullnameCompleted("csv");

		log.info("VP. Records are displayed correctly in CSV file");
		verifyTrue(loansPage.isRecordsExportedCorrectly(loansPage.getFileNameInDirectory(), documentIndex, "Document #", usersPage.getNumberOfRecords(driver), loansPage.convertDate(addedOnTime), username, submittedMethod, loansPage.convertDate(reviewedTime), address, active, documentType1));
		
		log.info("ExportCSVFile_09 - Step 05. Delete the downloaded file");
		loansPage.deleteContainsFileName("csv");
		
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UsersPage usersPage;
	private PropertiesPage propertiesPage;
	private LoansPage loansPage;
	private DocumentsPage documentsPage;
	private String newLoanStatus, finalLLC;
	private String numberProperties, totalEstimatedValue, loanStatusChangeDate, dateCreated;
	private String usernameLender, passwordLender, address1, documentFileName;
	private String loanName, accountNumber, applicantName, loanStatus;
	private String documentType1, documentType6, documentType7;
	private String borrowerName, propertiesFileNameRequired, documentSection1, documentSection2;
	private String userName, entityType, loanIndex, userLogin, email, city, state;
	private String address, active;
	private String addressProperty, zipProperty, applicantProperty, dateModifiedProperty, assetIDProperty, activeProperty, documentsProperty, reviewedProperty, propertyTypeProperty, cityProperty, stateProperty;
	private String documentIndex, documentLoaded, addedOn, reviewedOn, reviewedBy, section, documentIndexProperty;
	private String ignoredNumber, addedNumber, updatedNumber;
	private String addedOnTime, username, submittedMethod, reviewedTime, addedOnTimeGeneralLoanDocument, addedOnTimeCorporateEntityDocument;
}