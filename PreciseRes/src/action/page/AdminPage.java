package page;

import org.openqa.selenium.WebDriver;
import PreciseRes.Interfaces;

public class AdminPage extends AbstractPage{
	public AdminPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
	}
	
	/**
	 * click On Search button
	 */
	public void clickOnSearchButton() {
		waitForControl(driver, Interfaces.HomePage.SEARCH_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(10);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
	}
	
	/**
	 * select Loan Application for search user
	 */
	public void selectLoanApplicationForSearchPlaceholders(String loanApplication) {
		waitForControl(driver, Interfaces.UserLoginsPage.LOAN_APPLICATION_SEARCH_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.UserLoginsPage.LOAN_APPLICATION_SEARCH_COMBOBOX, loanApplication);
		sleep(1);
	}
	
	public void openManagingPlaceholdersTab() {
		waitForControl(driver, Interfaces.AdminPage.MANAGING_PLACEHOLDER_TAB, timeWait);
		click(driver, Interfaces.AdminPage.MANAGING_PLACEHOLDER_TAB);
	}
	
	public void openSectionTypesTab() {
		waitForControl(driver, Interfaces.AdminPage.SECTION_TYPES_TAB, timeWait);
		click(driver, Interfaces.AdminPage.SECTION_TYPES_TAB);
	}
	
	public void openPlaceholdersGroup(String groupName) {
		waitForControl(driver, Interfaces.AdminPage.DYNAMIC_PLACEHOLDER_GROUP, groupName, timeWait);
		click(driver, Interfaces.AdminPage.DYNAMIC_PLACEHOLDER_GROUP, groupName);
	}
	
	public Boolean isAddOnLoanCreatedChecked() {
		waitForControl(driver, Interfaces.AdminPage.ADD_ON_LOAN_CREATED_CHECKBOX, timeWait);
		return isCheckboxChecked(driver, Interfaces.AdminPage.ADD_ON_LOAN_CREATED_CHECKBOX);
	}
	
	public Boolean isDocumentsTypeDisplayed(String documentsType) {
		waitForControl(driver, Interfaces.AdminPage.ADD_ON_LOAN_CREATED_CHECKBOX, documentsType, timeWait);
		return isControlDisplayed(driver, Interfaces.AdminPage.ADD_ON_LOAN_CREATED_CHECKBOX, documentsType);
	}
	
	public void openAuditLoanTab() {
		waitForControl(driver, Interfaces.AdminPage.AUDIT_LOAN_TAB, timeWait);
		click(driver, Interfaces.AdminPage.AUDIT_LOAN_TAB);
	}
	
	public void openManageAuditProfiles() {
		waitForControl(driver, Interfaces.AdminPage.MANAGE_AUDIT_PROFILE, timeWait);
		click(driver, Interfaces.AdminPage.MANAGE_AUDIT_PROFILE);
	}
	
	public void openProfilesName(String profileName) {
		waitForControl(driver, Interfaces.AdminPage.DYNAMIC_PROFILE_NAMES, profileName, timeWait);
		click(driver, Interfaces.AdminPage.DYNAMIC_PROFILE_NAMES, profileName);
	}
	
	public Boolean isSectionsTypeDisplayed(String documentsType) {
		waitForControl(driver, Interfaces.AdminPage.DYNAMIC_AUDIT_GENERAL_DOCUMENTS_TYPE_DROPDOWN_BOX, documentsType, timeWait);
		return isControlDisplayed(driver, Interfaces.AdminPage.DYNAMIC_AUDIT_GENERAL_DOCUMENTS_TYPE_DROPDOWN_BOX, documentsType);
	}
	
	public void openGeneralDocumentTypeDropdownBox(String documentsType) {
		waitForControl(driver, Interfaces.AdminPage.DYNAMIC_AUDIT_GENERAL_DOCUMENTS_TYPE_DROPDOWN_BOX, documentsType, timeWait);
		click(driver, Interfaces.AdminPage.DYNAMIC_AUDIT_GENERAL_DOCUMENTS_TYPE_DROPDOWN_BOX, documentsType);
	}
	
	public Boolean isGenaralDocumentsTypeDisplayed(String documentsType) {
		waitForControl(driver, Interfaces.AdminPage.DYNAMIC_AUDIT_DOCUMENTS_TYPE_DROPDOWN_OPTION, documentsType, timeWait);
		return isControlDisplayed(driver, Interfaces.AdminPage.DYNAMIC_AUDIT_DOCUMENTS_TYPE_DROPDOWN_OPTION, documentsType);
	}
	
	public Boolean isPropertyDocumentsTypeDisplayed(String documentsType) {
		waitForControl(driver, Interfaces.AdminPage.DYNAMIC_PROPERTY_DOCUMENTS_TYPE_DROPDOWN_OPTION, documentsType, timeWait);
		return isControlDisplayed(driver, Interfaces.AdminPage.DYNAMIC_PROPERTY_DOCUMENTS_TYPE_DROPDOWN_OPTION, documentsType);
	}
	
	public void openSystemsValueTab() {
		waitForControl(driver, Interfaces.AdminPage.SYSTEMS_VALUE_TAB, timeWait);
		click(driver, Interfaces.AdminPage.SYSTEMS_VALUE_TAB);
	}
	
	public void searchSystemsValueName(String value) {
		waitForControl(driver, Interfaces.AdminPage.SEARCH_SYSTEMS_VALUE_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.AdminPage.SEARCH_SYSTEMS_VALUE_NAME_TEXTBOX, value);
		clickOnSearchButton();
	}
	
	public void openSystemsValueName(String value) {
		waitForControl(driver, Interfaces.AdminPage.SYSTEMS_VALUE_LINK,value, timeWait);
		click(driver, Interfaces.AdminPage.SYSTEMS_VALUE_LINK, value);
	}
	
	public void changeSystemsValue(String value) {
		waitForControl(driver, Interfaces.AdminPage.SYSTEMS_VALUE_AREABOX, timeWait);
		type(driver, Interfaces.AdminPage.SYSTEMS_VALUE_AREABOX, value);
		clickOnSaveButton();
	}
	
	/**
	 * click On Search button
	 */
	public void clickOnSaveButton() {
		waitForControl(driver, Interfaces.HomePage.SAVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		sleep(10);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
	}
	
	private WebDriver driver;
}
