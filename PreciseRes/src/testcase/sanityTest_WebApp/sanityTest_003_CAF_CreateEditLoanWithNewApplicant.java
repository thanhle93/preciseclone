package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.ApplicantsPage;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.MailHomePage;
import page.MailLoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_003_CAF_CreateEditLoanWithNewApplicant extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan"+accountNumber;
		loanStatus = Constant.LOAN_STATUS_CAF;
		applicantName = "uditeam-applicant"+accountNumber;
		addressNumber = "street"+accountNumber;
		addressDictrict = "dictrict"+accountNumber;
		city = "Los Angeles";
		state = "CA - California";
		zipCode = "90001";
		userLastName = "uditeam"+accountNumber;
		companyType = "General Partnership";
		loanEmail = "invalidEmail";
		userFirstName = "applicantFirstNameCAF";
		emailAdress = "minhdam06@gmail.com";
		usernameEmail = "minhdam06@gmail.com";
		passwordEmail = "dam123!@#!@#";
		mailPageUrl = "https://accounts.google.com";
	}
	
	@Test(groups = { "regression" },description = "Loan30 - Create new applicant when creating new loan")
	public void NewLoanApplicant_01_CreateNewApplicantWhileCreateNewLoan()
	{
		log.info("NewLoanApplicant_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("NewLoanApplicant_01 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		log.info("NewLoanApplicant_01 - Step 03. Click new loan button");
		loansPage.clickNewLoanButton();
		
		log.info("NewLoanApplicant_01 - Step 04. Input loan legal name");
		loansPage.inputLegalNameForLoan(loanName);
		
		log.info("NewLoanApplicant_01 - Step 05. Select Loan Application Status");
		loansPage.selectLoanStatus(loanStatus);
		
		log.info("NewLoanApplicant_01 - Step 06. Click new applicant for Loan");
		loansPage.clickNewApplicantLinkOnAddLoan();
		loansPage.switchToNewApplicantForLoanFrame(driver);
		
		log.info("NewLoanApplicant_01 - Step 07. Input loan applicant info");
		loansPage.inputApplicantInfoToCreateForLoans(accountNumber, applicantName, addressNumber,
				addressDictrict, city, state, zipCode);
		loansPage.inputNewLoginInfo(userFirstName, userLastName, emailAdress);
		
		log.info("NewLoanApplicant_01 - Step 08. Click save button in Create new Applicant popup");
		loansPage.clickSaveCreateNewApplicantButton();
		loansPage.switchToTopWindowFrame(driver);
		
		log.info("VP: Applicant name displays in applicant combobox in add loan application");
		verifyTrue(loansPage.isApplicantSavedInNewLoanForm(applicantName));
	}
	
	@Test(groups = { "regression" },description = "Loan31 - Check value source filed in Add Loan Applicant page")
	public void NewLoanApplicant_02_CheckSourceValueOnNewLoanForm()
	{	
		log.info("VP: Source combobox has 14 item");
		verifyTrue(loansPage.isItemOfSourceComboboxOnAddNewLoan(13));
		
		log.info("VP: Source combobox contains item '3rd Party Website'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnAddNewLoan("3rd Party Website"));
		
		log.info("VP: Source combobox contains item 'Bridge Borrower'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnAddNewLoan("Bridge Borrower"));
		
		log.info("VP: Source combobox contains item 'Broker'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnAddNewLoan("Broker"));
		
		log.info("VP: Source combobox contains item 'CAF Hotline'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnAddNewLoan("CAF Hotline"));
		
		log.info("VP: Source combobox contains item 'CAF Website'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnAddNewLoan("CAF Website"));
		
		log.info("VP: Source combobox contains item 'Conference'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnAddNewLoan("Conference"));
		
		log.info("VP: Source combobox contains item 'Correspondent'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnAddNewLoan("Correspondent"));
		
		log.info("VP: Source combobox contains item 'Direct'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnAddNewLoan("Direct"));
		
		log.info("VP: Source combobox contains item 'JV'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnAddNewLoan("JV"));
		
		log.info("VP: Source combobox contains item 'Mail Campaign'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnAddNewLoan("Mail Campaign"));
		
		log.info("VP: Source combobox contains item 'Proprietary'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnAddNewLoan("Proprietary"));
		
		log.info("VP: Source combobox contains item 'Referral'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnAddNewLoan("Referral"));
		
		log.info("VP: Source combobox contains item 'Rent Range'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnAddNewLoan("Rent Range"));
		
		log.info("VP: Source combobox contains item 'Repeat Borrower'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnAddNewLoan("Repeat Borrower"));
	}
		
	@Test(groups = { "regression" },description = "Loan32 - Create new loan successfully with new applicant")
	public void NewLoanApplicant_03_CreateNewLoanWithNewApplicant()
	{		
		log.info("NewLoanApplicant_03 - Step 01. Click save loan application button");
		loansPage.clickSaveLoanApplicationButton();
		
		log.info("VP: Loan item display with correct info");
		verifyTrue(loansPage.isLoansDisplayWithCorrectInfo(loanName, loanStatus));
		
		log.info("VP: Applicant displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(applicantName));
	}
	
	@Test(groups = { "regression" },description = "Loan33 - Check error message display when save loan with invalid email")
	public void NewLoanApplicant_04_EditAndSaveLoanWithInvalidEmail()
	{
		log.info("NewLoanApplicant_04 - Step 01. Edit loan info");
		loansPage.editLoanInfo(companyType, loanEmail);
		
		log.info("NewLoanApplicant_04 - Step 02. Click save loan basic detail");
		loansPage.clickSaveButton();
		
		log.info("VP: Error message about invalid email display");
		verifyTrue(loansPage.isErrorMessageAboutInvalidEmailDisplay());
	}
	
	@Test(groups = { "regression" },description = "Loan45 - Check value Source field in Loan Basic Detail tab")
	public void NewLoanApplicant_05_CheckSourceValueOnBasicDetailTab()
	{	
		log.info("VP: Source combobox has 14 item");
		verifyTrue(loansPage.isItemOfSourceComboboxOnBasicDetailTab(13));
		
		log.info("VP: Source combobox contains item '3rd Party Website'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnBasicDetailTab("3rd Party Website"));
		
		log.info("VP: Source combobox contains item 'Bridge Borrower'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnBasicDetailTab("Bridge Borrower"));
		
		log.info("VP: Source combobox contains item 'Broker'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnBasicDetailTab("Broker"));
		
		log.info("VP: Source combobox contains item 'CAF Hotline'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnBasicDetailTab("CAF Hotline"));
		
		log.info("VP: Source combobox contains item 'CAF Website'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnBasicDetailTab("CAF Website"));
		
		log.info("VP: Source combobox contains item 'Conference'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnBasicDetailTab("Conference"));
		
		log.info("VP: Source combobox contains item 'Correspondent'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnBasicDetailTab("Correspondent"));
		
		log.info("VP: Source combobox contains item 'Direct'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnBasicDetailTab("Direct"));
		
		log.info("VP: Source combobox contains item 'JV'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnBasicDetailTab("JV"));
		
		log.info("VP: Source combobox contains item 'Mail Campaign'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnBasicDetailTab("Mail Campaign"));
		
		log.info("VP: Source combobox contains item 'Proprietary'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnBasicDetailTab("Proprietary"));
		
		log.info("VP: Source combobox contains item 'Referral'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnBasicDetailTab("Referral"));
		
		log.info("VP: Source combobox contains item 'Rent Range'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnBasicDetailTab("Rent Range"));
		
		log.info("VP: Source combobox contains item 'Repeat Borrower'");
		verifyTrue(loansPage.isItemExistInSourceComboboxOnBasicDetailTab("Repeat Borrower"));
	}
	
	@Test(groups = { "regression" },description = "Loan48 - Check the lender see the loan in the dashboard")
	public void NewLoanApplicant_06_LenderSeeAddedLoanInDashboard()
	{	
		log.info("NewLoanApplicant_06 - Step 01. Go to Home page");
		homePage = loansPage.openHomePage(driver, ipClient);
		
		log.info("VP: Lender can see the added loan in dashboard");
		verifyTrue(homePage.isLoanDisplayOnDashboard(loanName));
	}
	
	@Test(groups = { "regression" },description = "Loan49 - Check the lender search the loan")
	public void NewLoanApplicant_07_LenderSearchLoanSuccessfully()
	{
		log.info("NewLoanApplicant_07 - Step 01. Go to Loan page");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		log.info("NewLoanApplicant_07 - Step 02. Search loan by name");
		loansPage.searchLoanByName(loanName);
		
		log.info("VP: The loan displays in table");
		verifyTrue(loansPage.isLoansDisplayOnSearchLoanPage(loanName));
	}
	
	@Test(groups = { "regression" },description = "Loan50 - Make Inactive Loan")
	public void NewLoanApplicant_08_MakeInactiveLoan()
	{
		log.info("NewLoanApplicant_08 - Step 01: Select Loan checkbox in List Loan Applicants table");
		loansPage.isSelectedPropertyLoanCheckbox(loanName);
		
		log.info("NewLoanApplicant_08 - Step 02: Click 'Make Loan Inactive' button");
		loansPage.clickMakeInactiveButtonAtLoanTab();
		
		log.info("VP: The loan not displayed in table");
		verifyFalse(loansPage.isLoansDisplayOnSearchLoanPage(loanName));
	}
	
	@Test(groups = { "regression" },description = "Loan51 - Make Active Loan")
	public void NewLoanApplicant_09_MakeActiveLoan()
	{
		log.info("NewLoanApplicant_09 - Step 01. Search Loan by Inactive status");
		loansPage.searchByActiveOrInactive(loanName, "No");

		log.info("NewLoanApplicant_09 - Step 02: Select Loan checkbox in List Loan Applicants table");
		loansPage.isSelectedPropertyLoanCheckbox(loanName);
		
		log.info("NewLoanApplicant_09 - Step 03: Click 'Make Loan Active' button");
		loansPage.clickMakeActiveButtonAtLoanTab();
		
		log.info("NewLoanApplicant_09 - Step 04. Search Loan by Active status");
		loansPage.searchByActiveOrInactive(loanName, "Yes");
		
		log.info("VP: The loan displayed in table");
		verifyTrue(loansPage.isLoansDisplayOnSearchLoanPage(loanName));
	}
	
	@Test(groups = { "regression" },description = "Loan52 - Re-send welcome email to applicant contact")
	public void NewLoanApplicant_10_ResendWelcomeEmailToApplicantContact()
	{
		log.info("NewLoanApplicant_10 - Step 01. Go to User page");
		applicantPage = loansPage.openApplicantsPage(driver, ipClient);
		
		log.info("NewLoanApplicant_10 - Step 02. Search applicant name");
		applicantPage.searchApplicantByName(applicantName);
		
		log.info("VP: The applicant name display in User page");
		verifyTrue(applicantPage.isApplicantDisplayOnSearchPage(applicantName));
		
		log.info("NewLoanApplicant_10 - Step 03. Open applicant name detail");
		applicantPage.openApplicantsDetailPage(applicantName);
		
		log.info("VP: The user display on Contact Detail table");
		verifyTrue(applicantPage.isUserDisplayOnContactDetailTable(userLastName));
		
		log.info("NewLoanApplicant_10 - Step 04. Open user name detail");
		applicantPage.openUserNameDetailPage(userLastName);
		
		log.info("NewLoanApplicant_10 - Step 05. Click Re-Send Welcome Email button");
		applicantPage.clickResendWelcomeEmailButton();
		
		log.info("VP: Resend Welcome Email message display");
		applicantPage.isResendWelcomeEmailMessage(usernameEmail);
		
		log.info("NewLoanApplicant_10 - Step 06. Open Email url");
		mailLoginPage = applicantPage.openMailLink(driver, mailPageUrl);
		
		log.info("NewLoanApplicant_10 - Step 07. Input Usename and Password");
		mailLoginPage.loginToGmail(usernameEmail, passwordEmail);	
		
		log.info("NewLoanApplicant_10 - Step 08. Click Submit button");
		mailHomePage = mailLoginPage.clickSubmitButton(driver, ipClient);
		
		log.info("NewLoanApplicant_10 - Step 09. Click 'Google Apps' title");
		mailHomePage.clickGoogleAppsTitle();
		
		log.info("NewLoanApplicant_10 - Step 10. Click 'Gmail' application");
		mailHomePage.clickGmailApplication();
		
		log.info("VP. Gmail home page is displayed");
		verifyTrue(mailHomePage.isGmailHomePageDisplay());
		
		log.info("NewLoanApplicant_10 - Step 11. Click 'Inbox' title");
		mailHomePage.clickInboxTitle();
		
		log.info("NewLoanApplicant_10 - Step 12. Click 'Welcome to B2R' email");
		mailHomePage.clickWelcomeEmailResend(userFirstName);
		
		log.info("VP. Firstname/ Lastname is displayed successfully");
		verifyTrue(mailHomePage.isLastNameDisplay(userLastName));
		
		log.info("NewLoanApplicant_10 - Step 13. Delete this email");
		mailHomePage.clickDataTooltipMoreDropdown();
		mailHomePage.clickDeleteThisMessageTitle();
		
//		while(mailHomePage.isLastNameDisplay(userLastName)==true){
//			mailHomePage.clickDataTooltipMoreDropdown();
//			mailHomePage.clickDeleteThisMessageTitle();
//		}
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private ApplicantsPage applicantPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private MailLoginPage mailLoginPage;
	private MailHomePage mailHomePage;
	private String usernameLender, passwordLender, addressDictrict, loanStatus;
	private String loanName, accountNumber, applicantName, addressNumber, userLastName;
	private String city, state, zipCode, emailAdress, companyType, loanEmail, userFirstName, mailPageUrl;
	private String usernameEmail, passwordEmail;
}