package page;

import org.openqa.selenium.WebDriver;

import PreciseRes.Interfaces;
import common.Common;

public class ReportsPage extends AbstractPage {
	public ReportsPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
	}

	/**
	 * open Reports Tab
	 */
	public void openReportsTab() {
		waitForControl(driver, Interfaces.ReportsPage.REPORTS_TAB, timeWait);
		executeJavaScript(driver, "document.getElementById('Reports').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * open Group Reports Tab
	 */
	public void openGroupReportsTab() {
		waitForControl(driver, Interfaces.ReportsPage.GROUP_REPORTS_TAB, timeWait);
		executeJavaScript(driver, "document.getElementById('GroupReports').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * open Reports Template Tab
	 */
	public void openReportsTemplateTab() {
		waitForControl(driver, Interfaces.ReportsPage.REPORTS_TEMPLATE_TAB, timeWait);
		executeJavaScript(driver, "document.getElementById('ReportTemplates').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * click Search button
	 */
	public void clickSearchButton() {
		waitForControl(driver, Interfaces.ReportsPage.SEARCH_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * click New button
	 */
	public void clickNewButton() {
		waitForControl(driver, Interfaces.ReportsPage.NEW_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('New').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * click Delete button
	 */
	public void clickDeleteButton() {
		waitForControl(driver, Interfaces.ReportsPage.DELETE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Delete').click();");
		acceptJavascriptAlert(driver);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * Input Report Name
	 */
	public void inputReportName(String reportName) {
		waitForControl(driver, Interfaces.ReportsPage.REPORT_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.ReportsPage.REPORT_NAME_TEXTBOX, reportName);
		sleep();
	}

	/**
	 * Select Reporting Level
	 */
	public void selectReportingLevel(String reportingLevel) {
		waitForControl(driver, Interfaces.ReportsPage.REPORTING_LEVEL_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.ReportsPage.REPORTING_LEVEL_DROPDOWN_LIST, reportingLevel);
		sleep();
	}

	/**
	 * Input File Name Template
	 */
	public void inputFileNameTemplate(String fileNameTemplate) {
		waitForControl(driver, Interfaces.ReportsPage.FILE_NAME_TEMPLATE_TEXTBOX, timeWait);
		type(driver, Interfaces.ReportsPage.FILE_NAME_TEMPLATE_TEXTBOX, fileNameTemplate);
		sleep();
	}

	/**
	 * Input File Template
	 */
	public void inputFileTemplate(String fileTemplate) {
		waitForControl(driver, Interfaces.ReportsPage.FILE_TEMPLATE_TEXTBOX, timeWait);
		type(driver, Interfaces.ReportsPage.FILE_TEMPLATE_TEXTBOX, fileTemplate);
		sleep();
	}

	// Reports tab
	/**
	 * Select Reporting Level at New tab
	 */
	public void selectReportingLevelNewTab(String reportingLevel) {
		waitForControl(driver, Interfaces.ReportsPage.REPORT_REPORTING_LEVEL_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.ReportsPage.REPORT_REPORTING_LEVEL_DROPDOWN_LIST, reportingLevel);
		sleep();
	}

	/**
	 * Select Reporting Template at New Report tab
	 */
	public void selectReportingTemplateAtReportTab(String reportingTemplate) {
		waitForControl(driver, Interfaces.ReportsPage.REPORT_TEMPLATE_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.ReportsPage.REPORT_TEMPLATE_DROPDOWN_LIST, reportingTemplate);
		sleep();
	}

	/**
	 * Select File Format at New Report tab
	 */
	public void selectFileFormatAtReportTab(String fileFormat) {
		waitForControl(driver, Interfaces.ReportsPage.REPORT_FILE_FORMATE_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.ReportsPage.REPORT_FILE_FORMATE_DROPDOWN_LIST, fileFormat);
		sleep();
	}

	/**
	 * Input Report name
	 */
	public void inputReportNameAtReportTab(String reportName) {
		waitForControl(driver, Interfaces.ReportsPage.REPORT_REPORTING_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.ReportsPage.REPORT_REPORTING_NAME_TEXTBOX, reportName);
		sleep();
	}

	/**
	 * Input File name template
	 */
	public void inputFileNameTemplateAtReportTab(String fileNamTemplate) {
		waitForControl(driver, Interfaces.ReportsPage.REPORT_FILE_NAME_TEMPLATE_TEXTBOX, timeWait);
		type(driver, Interfaces.ReportsPage.REPORT_FILE_NAME_TEMPLATE_TEXTBOX, fileNamTemplate);
		sleep();
	}

	/**
	 * Check Reports checkbox in List Reports table
	 */
	public void isSelectedReportCheckbox(String reportName) {
		waitForControl(driver, Interfaces.ReportsPage.DYNAMIC_REPORT_CHECKBOX_IN_LIST_REPORTS_TABLE, reportName, timeWait);
		checkTheCheckbox(driver, Interfaces.ReportsPage.DYNAMIC_REPORT_CHECKBOX_IN_LIST_REPORTS_TABLE, reportName);
		sleep();
	}

	// Report Template tab
	/**
	 * Select Reporting Level at New Report Template tab
	 */
	public void selectReportingLevelAtReportTemplateTab(String reportingLevel) {
		waitForControl(driver, Interfaces.ReportsPage.REPORT_TEMPLATE_REPORTING_LEVEL_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.ReportsPage.REPORT_TEMPLATE_REPORTING_LEVEL_DROPDOWN_LIST, reportingLevel);
		sleep();
	}

	/**
	 * Upload File Template
	 */
	public void uploadFileTemplate(String fileName) {
		waitForControl(driver, Interfaces.ReportsPage.REPORT_TEMPLATE_FILE_TEMPLATE_UPLOAD, timeWait);
		doubleClick(driver, Interfaces.ReportsPage.REPORT_TEMPLATE_FILE_TEMPLATE_UPLOAD);
		sleep();
		Common.getCommon().openFileForUpload(driver, fileName);
		sleep();
	}

	/**
	 * Click Save Button
	 */
	public void clickSaveButton() {
		waitForControl(driver, Interfaces.ReportsPage.SAVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep(7);
	}

	/**
	 * Click Back To List Button
	 */
	public void clickBackToListButton() {
		waitForControl(driver, Interfaces.ReportsPage.BACK_TO_LIST_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('BackToList').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * Check Load data message title display
	 * 
	 * @param N/A
	 */
	public boolean isBasicDetailMessageTitleDisplay(String messageContent) {
		waitForControl(driver, Interfaces.ReportsPage.DYNAMIC_BASIC_DETAIL_MESSAGE_TITLE, messageContent, timeWait);
		return isControlDisplayed(driver, Interfaces.ReportsPage.DYNAMIC_BASIC_DETAIL_MESSAGE_TITLE, messageContent);
	}

	/**
	 * Check Report display on List of Reports
	 */
	public boolean isReportDisplayonListReports(String reportName, String reportingLevel, String fileNameTemplate) {
		waitForControl(driver, Interfaces.ReportsPage.DYNAMIC_REPORT_IN_REPORTS_TAB, reportName, reportingLevel, fileNameTemplate, timeWait);
		return isControlDisplayed(driver, Interfaces.ReportsPage.DYNAMIC_REPORT_IN_REPORTS_TAB, reportName, reportingLevel, fileNameTemplate);
	}

	/**
	 * Check Group/ Report Templates display on List of Group Reports/ Report
	 * Template
	 */
	public boolean isGroupAndTemplateReportDisplayonListGroupAndTemplateReports(String reportName, String reportingLevel) {
		waitForControl(driver, Interfaces.ReportsPage.DYNAMIC_GROUP_REPORT_IN_GROUP_REPORTS_TAB, reportName, reportingLevel, timeWait);
		return isControlDisplayed(driver, Interfaces.ReportsPage.DYNAMIC_GROUP_REPORT_IN_GROUP_REPORTS_TAB, reportName, reportingLevel);
	}

	/**
	 * Check message display in table
	 * 
	 * @param N/A
	 */
	public boolean isMessageTitleDisplayInTable(String messageContent) {
		waitForControl(driver, Interfaces.ReportsPage.DYNAMIC_REPORTS_MESSAGE_TITLE, messageContent, timeWait);
		return isControlDisplayed(driver, Interfaces.ReportsPage.DYNAMIC_REPORTS_MESSAGE_TITLE, messageContent);
	}

	/**
	 * click Download CSV image
	 */
	public void clickOnDownloadCSVImage() {
		waitForControl(driver, Interfaces.TransactionPage.DOWNLOAD_CSV_IMAGE, timeWait);
		click(driver, Interfaces.TransactionPage.DOWNLOAD_CSV_IMAGE);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
		sleep(5);
	}

	/**
	 * select Report Name
	 */
	public void selectReportName(String reportName) {
		waitForControl(driver, Interfaces.ReportsPage.DYNAMIC_REPORTS_NAME_CHECKBOX, reportName, timeWait);
		checkTheCheckbox(driver, Interfaces.ReportsPage.DYNAMIC_REPORTS_NAME_CHECKBOX, reportName);
		sleep();
	}
	
	/**
	 * click Send this information to email
	 */
	public void clickSendInformationToEmail() {
		waitForControl(driver, Interfaces.UsersPage.SEND_INFORMATION_TO_EMAIL, timeWait);
		click(driver, Interfaces.UsersPage.SEND_INFORMATION_TO_EMAIL);
		sleep();
	}
	
	/**
	 * Get user information store
	 */
	public String getRecordInformationStore() {
		waitForControl(driver, Interfaces.UsersPage.FIRST_RECORD, timeWait);
		return getText(driver, Interfaces.UsersPage.FIRST_RECORD).trim().toLowerCase();
	}
	
	/**
	 * Get user information store
	 */
	public String getTemplateInformationStore() {
		waitForControl(driver, Interfaces.UsersPage.FIRST_TEMPLATE, timeWait);
		return getText(driver, Interfaces.UsersPage.FIRST_TEMPLATE).trim().toLowerCase();
	}

	private WebDriver driver;
}
