package sanityTest_WithoutApplicant;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import common.AbstractTest;
import common.Common;
import common.Constant;

public class sanityTest_002_CAF_UpdateDataTapeTestCase extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan"+accountNumber;
		loanStatus = Constant.LOAN_STATUS_CAF;
		propertiesFileName = "CAF Data Tape.xlsx";
		numberOfProperty = 25;
		address1 = "502 E Hugh";
		address2 = "318 E Broad";
		address3 = "314 E Althea";
		documentType01 = "Section 8 HAP Contract/ Lease";
		documentType02 = "Evidence of Insurance";
		fileName01 = "datatest1.pdf";
		fileName02 = "datatest2.pdf";
		year = Common.getCommon().getCurrentYearOfWeek();
		submitVia = "Demo Lender Level 1";
	}
	
	@Test(groups = { "regression" },description = "Datatape01 - Upload Datatape and check info")
	public void DataTape_01_UploadAndCheckInfoDatatape()
	{
		log.info("DataTape_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("DataTape_01 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		log.info("DataTape_01 - Step 03. Create new loan");
		loansPage.createNewLoan(loanName, "", "", loanStatus);
		
		log.info("DataTape_01 - Step 04. Open Properties tab");
		loansPage.openPropertiesTab();
		
		log.info("DataTape_01 - Step 05. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("DataTape_01 - Step 06. Get Property number");
		propertyNumberProtab = loansPage.getNumberOfPropertyInPropertiesTab();
		
		log.info("VP: Number of property is uploaded enough");
		verifyEquals(propertyNumberProtab, numberOfProperty);
		
		log.info("VP: Information of property 1 is correct");
		verifyTrue(loansPage.isPropertyInfoCorrectly("807 E Elm", "Tampa", "FL", "33604", "SFR"));
		
		log.info("VP: Information of property 2 is correct");
		verifyTrue(loansPage.isPropertyInfoCorrectly("1520 E Caracas", "Tampa", "FL", "33610", "SFR"));
		
		log.info("VP: Information of property 3 is correct");
		verifyTrue(loansPage.isPropertyInfoCorrectly("502 E Hugh", "Tampa", "FL", "33603", "SFR"));
		
		log.info("VP: Information of property 4 is correct");
		verifyTrue(loansPage.isPropertyInfoCorrectly("110 E Emily", "Tampa", "FL", "33603", "SFR"));
		
		log.info("VP: Information of property 5 is correct");
		verifyTrue(loansPage.isPropertyInfoCorrectly("1913 E Emma", "Tampa", "FL", "33610", "SFR"));
		
		log.info("VP: Information of property 6 is correct");
		verifyTrue(loansPage.isPropertyInfoCorrectly("1701 E Powhatan", "Tampa", "FL", "33610", "SFR"));
		
		log.info("VP: Information of property 7 is correct");
		verifyTrue(loansPage.isPropertyInfoCorrectly("318 E Broad", "Tampa", "FL", "33604", "SFR"));
		
		log.info("VP: Information of property 8 is correct");
		verifyTrue(loansPage.isPropertyInfoCorrectly("2103 E Howell", "Tampa", "FL", "33610", "SFR"));
		
		log.info("VP: Information of property 9 is correct");
		verifyTrue(loansPage.isPropertyInfoCorrectly("110 W Adalee", "Tampa", "FL", "33603", "SFR"));
		
		log.info("VP: Information of property 10 is correct");
		verifyTrue(loansPage.isPropertyInfoCorrectly("1302 E Flora", "Tampa", "FL", "33604", "SFR"));
		
		log.info("VP: Information of property 11 is correct");
		verifyTrue(loansPage.isPropertyInfoCorrectly("1401 E. 27th", "Tampa", "FL", "33605", "SFR"));
		
		log.info("VP: Information of property 12 is correct");
		verifyTrue(loansPage.isPropertyInfoCorrectly("1409 E Sligh", "Tampa", "FL", "33604", "SFR"));
		
		log.info("VP: Information of property 13 is correct");
		verifyTrue(loansPage.isPropertyInfoCorrectly("1707 E Henry", "Tampa", "FL", "33610", "SFR"));
		
		log.info("VP: Information of property 14 is correct");
		verifyTrue(loansPage.isPropertyInfoCorrectly("1906 W Kirby", "Tampa", "FL", "33604", "SFR"));
		
		log.info("VP: Information of property 15 is correct");
		verifyTrue(loansPage.isPropertyInfoCorrectly("314 E Althea", "Tampa", "FL", "33612", "SFR"));
	}
	
	@Test(groups = { "regression" }, description = "Datatape 03 - Make a Property is Inactive")
	public void DataTape_03_MakeAPropertyInactive() {
		
		log.info("DataTape_03 - Step 01. Search property address");
		loansPage.searchAddress(address3);

		log.info("DataTape_03 - Step 02: Select Property checkbox in List Properties table");
		loansPage.isSelectedPropertyLoanCheckbox(address3);
		
		log.info("DataTape_03 - Step 03: Click 'Make Inactive' button");
		loansPage.clickMakeInactiveButtonAtPropertyTab();
		
		log.info("VP: Verify that the property is not there");
		verifyFalse(loansPage.isPropertyInfoCorrectly(address3, "Tampa", "FL", "33612", "SFR"));
	}
	
	@Test(groups = { "regression" }, description = "Datatape 04 - Make a Property is Active")
	public void DataTape_04_MakeAPropertyActive() {
		
		log.info("DataTape_04 - Step 01. Search Property by Inactive status");
		loansPage.searchByActiveOrInactive(address3, "No");

		log.info("DataTape_04 - Step 02: Select Property checkbox in List Properties table");
		loansPage.isSelectedPropertyLoanCheckbox(address3);
		
		log.info("DataTape_04 - Step 03: Click 'Make Active' button");
		loansPage.clickMakeActiveButtonAtPropertyTab();
		
		log.info("DataTape_04 - Step 04. Search Property by Active status");
		loansPage.searchByActiveOrInactive(address3, "Yes");
		
		log.info("VP: Verify that the property is there");
		verifyTrue(loansPage.isPropertyInfoCorrectly(address3, "Tampa", "FL", "33612", "SFR"));
	}
	
	@Test(groups = { "regression" }, description = "Datatape 05 - Change section 8 from yes to no the placeholder should be removed 'Section 8' if no document loaded")
	public void DataTape_05_ChangeSection8FromYesToNoThePlaceHolderShouldBeRemovedIfNoDocument() {
		
		log.info("DataTape_05 - Step 01. Open Loan tab");
		loansPage.openLoansPage(driver, ipClient);
		
		log.info("DataTape_05 - Step 02. Open Loan detail");
		loansPage.searchLoanByName(loanName);
		loansPage.openLoansDetailPage(loanName);

		log.info("DataTape_05 - Step 03. Open Properties tab");
		loansPage.openPropertiesTab();
		
		log.info("DataTape_05 - Step 04. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("DataTape_05 - Step 05. Select 'Section 8' Combobox");
		propertiesPage.selectSection8("No");

		log.info("DataTape_05 - Step 06. Click Save button");
		propertiesPage.clickSaveButton();

		log.info("VP: Removed 'Section 8 HAP Contract/ Lease'");
		verifyFalse(propertiesPage.isDocumentLoadedToDocumentType(documentType01,fileName01));
	}

	@Test(groups = { "regression" }, description = "Datatape 06 - Change section 8 from yes to no the 'Section 8' is still in placeholder if there is already document loaded")
	public void DataTape_06_ChangeSection8FromYesToNoTheSection8IsStillPlaceHolderIfAlreadyDocument() {
		log.info("DataTape_06 - Step 01. Select 'Section 8' Combobox");
		propertiesPage.selectSection8("Yes");

		log.info("DataTape_06 - Step 02. Click save button");
		propertiesPage.clickSaveButton();

		log.info("DataTape_06 - Step 03. Open Document type detail");
		propertiesPage.openDocumentTypeDetail(documentType01);

		log.info("DataTape_06 - Step 04. Enter document type");
		propertiesPage.enterDocumentType(documentType01);

		log.info("DataTape_06 - Step 05. Upload document file");
		propertiesPage.uploadDocumentFile(fileName01);

		log.info("DataTape_06 - Step 06. Click save button");
		propertiesPage.clickSaveButton();

		log.info("DataTape_06 - Step 07. Click Go To Property button");
		propertiesPage.clickGoToPropertyButton();

		log.info("DataTape_06 - Step 08. Select 'Section 8' Combobox");
		propertiesPage.selectSection8("No");

		log.info("DataTape_06 - Step 09. Click save button");
		propertiesPage.clickSaveButton();

		log.info("VP: 'Section 8 HAP Contract/ Lease' is still in placeholder");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType01,fileName01));
	}

	@Test(groups = { "regression" }, description = "Datatape 07 - Change section 8 from no to yes the placeholder should be added")
	public void DataTape_07_ChangeSection8FromNoToYesThePlaceHolderShouldBeAdded() {
		log.info("DataTape_07 - Step 01. Go to property list");
		loansPage = propertiesPage.clickOnPropertyListButton();

		log.info("DataTape_07 - Step 02. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address2);

		log.info("DataTape_07 - Step 02. Select 'Section 8' Combobox");
		propertiesPage.selectSection8("Yes");

		log.info("DataTape_07 - Step 03. Click save button");
		propertiesPage.clickSaveButton();

		log.info("VP: Added 'Section 8 HAP Contract/ Lease'");
		verifyTrue(propertiesPage.isDocumentTypeOnPropertiesDetailLoaded(documentType01));

		log.info("DataTape_07 - Step 04. Select 'Section 8' Combobox");
		propertiesPage.selectSection8("No");

		log.info("DataTape_07 - Step 05. Click save button");
		propertiesPage.clickSaveButton();

		log.info("VP: Removed 'Section 8 HAP Contract/ Lease'");
		verifyFalse(propertiesPage.isDocumentTypeOnPropertiesDetailLoaded(documentType01));

		log.info("DataTape_07 - Step 06. Select 'Section 8' Combobox");
		propertiesPage.selectSection8("Yes");

		log.info("DataTape_07 - Step 07. Click save button");
		propertiesPage.clickSaveButton();

		log.info("VP: Added 'Section 8 HAP Contract/ Lease'");
		verifyTrue(propertiesPage.isDocumentTypeOnPropertiesDetailLoaded(documentType01));
	}
	
	@Test(groups = { "regression" }, description = "Datatape 08 - Make sure Lender can see both the old and new document")
	public void DataTape_08_OpenBothTheOldDocumentAndNewDocument() {

		log.info("DataTape_08 - Step 01. Open Document type detail");
		propertiesPage.openDocumentTypeDetail(documentType02);

		log.info("DataTape_08 - Step 02. Upload document file 01");
		propertiesPage.uploadDocumentFile(fileName01);

		log.info("DataTape_08 - Step 03. Click Save button");
		propertiesPage.clickSaveButton();

		log.info("DataTape_08 - Step 04. Upload document file 02");
		propertiesPage.uploadDocumentFile(fileName02);

		log.info("DataTape_08 - Step 05. Click Save button");
		propertiesPage.clickSaveButton();
		
		log.info("VP: Document file name 01 is uploaded successfully");
		verifyTrue(propertiesPage.isDocumentHistoryLoadedToDocumentType(fileName01, submitVia, "/" + year));

		log.info("VP: Document file name 02 is uploaded successfully");
		verifyTrue(propertiesPage.isDocumentHistoryLoadedToDocumentType(fileName02, submitVia, "/" + year));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private String usernameLender, passwordLender, address1, address2, address3, documentType01, fileName01, submitVia;
	private String loanName, accountNumber, propertiesFileName, loanStatus, documentType02, fileName02;
	private int propertyNumberProtab, numberOfProperty, year;
}