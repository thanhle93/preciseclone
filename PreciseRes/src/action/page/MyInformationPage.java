package page;

import org.openqa.selenium.WebDriver;
import PreciseRes.Interfaces;

public class MyInformationPage extends AbstractPage {
	public MyInformationPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
	}

	/**
	 * check My Information page display
	 * @param N/A
	 */
	public boolean isMyInformationPageDisplay() {
		waitForControl(driver, Interfaces.MyInformationPage.MY_INFORMATION_PAGE_SECTION, timeWait);
		return isControlDisplayed(driver, Interfaces.MyInformationPage.MY_INFORMATION_PAGE_SECTION);
	}
	
	
	private WebDriver driver;
}