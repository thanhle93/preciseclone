package page;

import org.openqa.selenium.WebDriver;

import PreciseRes.Interfaces;

public class SalesForceLookupPage extends AbstractPage {
	
	private WebDriver driver;
	
	public SalesForceLookupPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
	}
	
	public SalesForceLookupPage(WebDriver driver) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
	}
	
	/**
	 * click Primary Contact Name
	 */
	public void clickPrimaryContactName(String primaryContact) {
		waitForControl(driver,Interfaces.SalesForceLookupPage.DYNAMIC_PRIMARY_CONTACT_NAME, primaryContact, timeWait);
		click(driver, Interfaces.SalesForceLookupPage.DYNAMIC_PRIMARY_CONTACT_NAME, primaryContact);
		sleep(2);
	}

}