package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Constant;
import page.HomePage;
import page.LoginPage;
import page.MailHomePage;
import page.MailLoginPage;
import page.PageFactory;
import page.ReportsPage;
import page.TransactionPage;
import page.UsersPage;

public class sanityTest_027_STO_SendInformationByEmail extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		accountNumber = getUniqueNumber();
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + accountNumber;
		mailPageUrl = "https://accounts.google.com";
		usernameEmail = "minhdam06@gmail.com";
		passwordEmail = "dam123!@#!@#";
		invalidEmail = "abc!@#@gmail.abc";
		emailSubject = "precise";
		emailMessage = "pres information";
		propertiesFileName = "StoreProperties.xlsx";
		documentType = "Title Commitment";
		address1 = "148 Craft Drive";
		documentFileName = "datatest1.pdf";
	}

	@Test(groups = { "regression" }, description = "Send Information 01 - Send information by email at Users page")
	public void SendInformation_01_UserPage() {

		log.info("SendInformation_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(storeLenderUsername, storeLenderPassword, false);
		preciseRESHomePageUrl = loginPage.getCurrentUrl(driver);
		
		log.info("SendInformation_01 - Step 02. Go to User Page");
		usersPage = homePage.openUsersPage(driver, ipClient);
		
		log.info("SendInformation_01 - Step 03. Get first username");
		emailContent = usersPage.getRecordInformationStore();
		
		log.info("SendInformation_01 - Step 04. Get number of records");
		numberOfRecord = usersPage.getNumberOfRecords(driver);
		
		log.info("SendInformation_01 - Step 05. Click on Send this information to Email button");
		usersPage.clickSendInformationToEmail();
		
		log.info("SendInformation_01 - Step 06. Check record table is displayed correctly");
		verifyTrue(usersPage.isRecordsTableDisplayedCorrectly(driver, "BorrowerSearch", numberOfRecord));
		
		log.info("SendInformation_01 - Step 07. Check Send this information to Email function working correctly");
		verifyTrue(usersPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_01 - Step 08. Open Email url");
		mailLoginPage = usersPage.openMailLink(driver, mailPageUrl);
		mailLoginPage.loginToGmail(usernameEmail, passwordEmail);

		log.info("SendInformation_01 - Step 09. Click Submit button");
		mailHomePage = mailLoginPage.clickSubmitButton(driver, ipClient);

		log.info("SendInformation_01 - Step 10. Click 'Google Apps' title");
		mailHomePage.clickGoogleAppsTitle();

		log.info("SendInformation_01 - Step 11. Click 'Gmail' application");
		mailHomePage.clickGmailApplication();

		log.info("VP. Gmail home page is displayed");
		verifyTrue(mailHomePage.isGmailHomePageDisplay());

		log.info("SendInformation_01 - Step 12. Click 'Inbox' title");
		mailHomePage.clickInboxTitle();

		log.info("SendInformation_01 - Step 13. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail("Store Automation Testing");
		
		log.info("SendInformation_01 - Step 14. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_01 - Step 15. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_01 - Step 16. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_01 - Step 17. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_01 - Step 18. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		log.info("SendInformation_01 - Step 19. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESHomePageUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 02 - Send information by email at Report page")
	public void SendInformation_02_ReportPage() {

		log.info("SendInformation_02 - Step 02. Go to User Page");
		reportsPage = homePage.openReportsPage(driver, ipClient);
		preciseRESUrl = reportsPage.getCurrentUrl(driver);
		
		log.info("SendInformation_02 - Step 03. Get first Reports name");
		emailContent = reportsPage.getRecordInformationStore();
		
		log.info("SendInformation_02 - Step 04. Get number of records");
		numberOfRecord = reportsPage.getNumberOfRecords(driver);
		
		log.info("SendInformation_02 - Step 05. Click on Send this information to Email button");
		reportsPage.clickSendInformationToEmail();
		
		log.info("SendInformation_02 - Step 06. Check record table is displayed correctly");
		verifyTrue(reportsPage.isRecordsTableDisplayedCorrectly(driver, "CommonReportSearch", numberOfRecord));
		
		log.info("SendInformation_02 - Step 07. Check Send this information to Email function working correctly");
		verifyTrue(reportsPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_02 - Step 15. Open Email url");
		mailLoginPage = reportsPage.openMailLink(driver, "https://mail.google.com");
		
		log.info("SendInformation_02 - Step 13. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail("Store Automation Testing");
		
		log.info("SendInformation_02 - Step 14. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_02 - Step 15. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_02 - Step 16. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_02 - Step 17. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_02 - Step 18. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		log.info("SendInformation_02 - Step 19. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 03 - Send information by email at Group Report page")
	public void SendInformation_03_GroupReportPage() {

		log.info("SendInformation_03 - Step 02. Go to User Page");
		reportsPage.openGroupReportsTab();
		
		log.info("SendInformation_03 - Step 03. Get first Reports name");
		emailContent = reportsPage.getRecordInformationStore();
		
		log.info("SendInformation_03 - Step 04. Get number of records");
		numberOfRecord = reportsPage.getNumberOfRecords(driver);
		
		log.info("SendInformation_03 - Step 05. Click on Send this information to Email button");
		reportsPage.clickSendInformationToEmail();
		
		log.info("SendInformation_03 - Step 06. Check record table is displayed correctly");
		verifyTrue(reportsPage.isRecordsTableDisplayedCorrectly(driver, "GroupReportSearch", numberOfRecord));
		
		log.info("SendInformation_03 - Step 07. Check Send this information to Email function working correctly");
		verifyTrue(reportsPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_03 - Step 15. Open Email url");
		mailLoginPage = reportsPage.openMailLink(driver, "https://mail.google.com");
		
		log.info("SendInformation_03 - Step 13. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail("Store Automation Testing");
		
		log.info("SendInformation_03 - Step 14. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_03 - Step 15. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_03 - Step 16. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_03 - Step 17. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_03 - Step 18. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		log.info("SendInformation_03 - Step 19. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 04 - Send information by email at Report Template Search page")
	public void SendInformation_04_ReportTemplateSearch() {

		log.info("SendInformation_04 - Step 02. Go to User Page");
		reportsPage.openReportsTemplateTab();
		
		log.info("SendInformation_04 - Step 03. Get first Reports name");
		emailContent = reportsPage.getTemplateInformationStore();
		
		log.info("SendInformation_04 - Step 04. Get number of records");
		numberOfRecord = reportsPage.getNumberOfRecords(driver);
		
		log.info("SendInformation_04 - Step 05. Click on Send this information to Email button");
		reportsPage.clickSendInformationToEmail();
		
		log.info("SendInformation_04 - Step 06. Check record table is displayed correctly");
		verifyTrue(reportsPage.isRecordsTableDisplayedCorrectly(driver, "ReportTemplateSearch", numberOfRecord));
		
		log.info("SendInformation_04 - Step 07. Check send email function working correctly");
		verifyTrue(reportsPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_04 - Step 08. Open Email url");
		mailLoginPage = reportsPage.openMailLink(driver, "https://mail.google.com");
		
		log.info("SendInformation_04 - Step 09. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail("Store Automation Testing");
		
		log.info("SendInformation_04 - Step 10. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_04 - Step 11. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_04 - Step 12. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_04 - Step 13. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_04 - Step 14. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		log.info("SendInformation_04 - Step 15. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESHomePageUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 05 - Send information by email at Transaction page")
	public void SendInformation_05_TransactionPage() {

		log.info("SendInformation_05 - Step 01. Go to User Page");
		transactionPage = homePage.openTransactionPage(driver, ipClient);
		preciseRESUrl = loginPage.getCurrentUrl(driver);
		
		log.info("SendInformation_05 - Step 02. Sort Lessee by Ascending");
		transactionPage.clickOnSortRecordLink(driver, "Sort by Date Created in ascending order");
		
		log.info("SendInformation_05 - Step 03. Get first Reports name");
		emailContent = transactionPage.getRecordInformationStore();
		
		log.info("SendInformation_05 - Step 04. Get number of records");
		numberOfRecord = transactionPage.getNumberOfRecords(driver);
		
		log.info("SendInformation_05 - Step 05. Click on Send this information to Email button");
		transactionPage.clickSendInformationToEmail();
		
		log.info("SendInformation_05 - Step 06. Check record table is displayed correctly");
		verifyTrue(transactionPage.isRecordsTableDisplayedCorrectly(driver, "LoanApplicationSearch", numberOfRecord));
		
		log.info("SendInformation_05 - Step 07. Check Send this information to Email function working correctly");
		verifyTrue(transactionPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_05 - Step 08. Open Email url");
		mailLoginPage = transactionPage.openMailLink(driver, "https://mail.google.com");
		
		log.info("SendInformation_05 - Step 09. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail("Store Automation Testing");
		
		log.info("SendInformation_05 - Step 10. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_05 - Step 11. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_05 - Step 12. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_05 - Step 13. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_05 - Step 14. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		log.info("SendInformation_05 - Step 15. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 06 - Send information by email at Properties tab")
	public void SendInformation_06_PropertiesTab() {

		log.info("SendInformation_06 - Step 01: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("SendInformation_06 - Step 02: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("SendInformation_06 - Step 03: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("SendInformation_06 - Step 04: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("SendInformation_06 - Step 05: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("SendInformation_06 - Step 06: Open Property tab");
		transactionPage.openPropertyTab();
		preciseRESUrl = loginPage.getCurrentUrl(driver);

		log.info("SendInformation_06 - Step 07: Click 'Add Properties' button");
		transactionPage.clickAddPropertyButton();

		log.info("SendInformation_06 - Step 08: Upload Excel filename");
		transactionPage.uploadFileProperties(propertiesFileName);
		
		log.info("SendInformation_06 - Step 09. Get first Reports name");
		emailContent = transactionPage.getPropertiesNumber();
		
		log.info("SendInformation_06 - Step 10. Get number of records");
		numberOfRecord = transactionPage.getNumberOfRecords(driver);
		
		log.info("SendInformation_06 - Step 11. Click on Send this information to Email button");
		transactionPage.clickSendInformationToEmail();
		
		log.info("SendInformation_06 - Step 12. Check record table is displayed correctly");
		verifyTrue(transactionPage.isRecordsTableDisplayedCorrectly(driver, "PropertyLoanApplicationSearch", numberOfRecord));
		
		log.info("SendInformation_06 - Step 13. Check Send this information to Email function working correctly");
		verifyTrue(transactionPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_06 - Step 14. Open Email url");
		mailLoginPage = transactionPage.openMailLink(driver, "https://mail.google.com");
		
		log.info("SendInformation_06 - Step 15. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail("Store Automation Testing");
		
		log.info("SendInformation_06 - Step 16. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_06 - Step 17. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_06 - Step 18. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_06 - Step 19. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_06 - Step 20. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		log.info("SendInformation_06 - Step 21. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 07 - Send information by email at Property documents tab")
	public void SendInformation_07_PropertyDocumentsTab() {
		
		log.info("SendInformation_07 - Step 01. Search with Address name is '148 Craft Drive'");
		transactionPage.searchPropertyAddress(address1);
		
		log.info("SendInformation_07 - Step 02. Open a property detail");
		transactionPage.openPropertyDetail(address1);

		log.info("SendInformation_07 - Step 03. Click 'New Property Documents' button");
		transactionPage.clickNewPropertyDocumentsButton();

		log.info("SendInformation_07 - Step 04. Select document type");
		transactionPage.selectDocumentType(documentType);

		log.info("SendInformation_07 - Step 05. Upload document file name");
		transactionPage.uploadDocumentFileName(documentFileName);

		log.info("Precondition - Step 04. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("SendInformation_07 - Step 06. Click 'Go to Property' button");
		transactionPage.clickGoToPropertyButtonAtDocumentTypeDetails();
		
		log.info("SendInformation_07 - Step 07: Open Property Documents tab");
		transactionPage.openPropertyDocumentsTab();
		
		log.info("SendInformation_07 - Step 08. Get first Reports name");
		emailContent = transactionPage.getCellContentByCellID(driver, "DataCell FieldTypeSQLServerIdentity datacell_Document");
		
		log.info("SendInformation_07 - Step 09. Get number of records");
		numberOfRecord = transactionPage.getNumberOfRecords(driver);
		
		log.info("SendInformation_07 - Step 10. Click on Send this information to Email button");
		transactionPage.clickSendInformationToEmail();
		
		log.info("SendInformation_07 - Step 11. Check record table is displayed correctly");
		verifyTrue(transactionPage.isRecordsTableDisplayedCorrectly(driver, "DocumentLoanApplicationSearch", numberOfRecord));
		
		log.info("SendInformation_07 - Step 12. Check Send this information to Email function working correctly");
		verifyTrue(transactionPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_07 - Step 13. Open Email url");
		mailLoginPage = transactionPage.openMailLink(driver, "https://mail.google.com");
		
		log.info("SendInformation_07 - Step 14. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail("Store Automation Testing");
		
		log.info("SendInformation_07 - Step 15. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_07 - Step 16. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_07 - Step 17. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_07 - Step 18. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_07 - Step 19. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		log.info("SendInformation_07 - Step 20. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESUrl);
	}
	
	@Test(groups = { "regression" }, description = "Send Information 08 - Send information by email at Transaction documents tab")
	public void SendInformation_08_TransactionDocumentsTab() {

		log.info("SendInformation_08 - Step 01: Open Transaction Documents tab");
		transactionPage.openTransactionDocumentsTab();

		log.info("SendInformation_08 - Step 02. Get first Reports name");
		emailContent = transactionPage.getCellContentByCellID(driver, "DataCell FieldTypeSQLServerIdentity datacell_Document");
		
		log.info("SendInformation_08 - Step 03. Get number of records");
		numberOfRecord = transactionPage.getNumberOfRecords(driver);
		
		log.info("SendInformation_08 - Step 04. Click on Send this information to Email button");
		transactionPage.clickSendInformationToEmail();
		
		log.info("SendInformation_08 - Step 05. Check record table is displayed correctly");
		verifyTrue(transactionPage.isRecordsTableDisplayedCorrectly(driver, "LeaseGeneralDocumentsSearch", numberOfRecord));
		
		log.info("SendInformation_08 - Step 06. Check Send this information to Email function working correctly");
		verifyTrue(transactionPage.isSendByEmailCorrectly(driver, invalidEmail, usernameEmail, emailSubject, emailMessage));

		log.info("SendInformation_08 - Step 07. Open Email url");
		mailLoginPage = transactionPage.openMailLink(driver, "https://mail.google.com");
		
		log.info("SendInformation_08 - Step 08. Click 'Subject first email'");
		mailHomePage.clickLastestSubjectEmail("Store Automation Testing");
		
		log.info("SendInformation_08 - Step 09. Verify Email Subject");
		verifyTrue(mailHomePage.isEmailSubjectDisplayed(emailSubject));
		
		log.info("SendInformation_08 - Step 10. Verify Email Message");
		verifyTrue(mailHomePage.isEmailMessageDisplayed(emailMessage));
		
		log.info("SendInformation_08 - Step 11. Verify Email content");
		verifyEquals(mailHomePage.getEmailContent(), emailContent);
		
		log.info("SendInformation_08 - Step 12. Click data tooltip 'More' dropdown");
		mailHomePage.clickDataTooltipMoreDropdown();

		log.info("SendInformation_08 - Step 13. Click 'Delete this message' title");
		mailHomePage.clickDeleteThisMessageTitle();
		
		log.info("SendInformation_08 - Step 14. Go back to Prescise page");
		mailHomePage.openLink(driver, preciseRESUrl);
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UsersPage usersPage;
	private MailLoginPage mailLoginPage;
	private MailHomePage mailHomePage;
	private TransactionPage transactionPage;
	private ReportsPage reportsPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName ;
	private String preciseRESUrl, preciseRESHomePageUrl;
	private String mailPageUrl, usernameEmail, passwordEmail;
	private String invalidEmail, emailSubject, emailMessage, emailContent;
	private String address1, documentFileName;
	private String accountNumber;
	private String propertiesFileName, documentType;
	private int numberOfRecord;
}