package sanityTest_WithoutApplicant;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_016_B2R_Lender_EditDocumentOnUploader extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		loanName = "UploaderUdiTesting"+getUniqueNumber();
		loanStatus = Constant.LOAN_STATUS_B2R;
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
		propertiesFileName = "B2R Data Tape.xlsx";
		fileNameDocument1 = "datatest.pdf";
		documentType1 = "Lease Agreement";
		property1Name = "37412 OAKHILL ST";
		property2Name = "111222 37TH ST";
		fileNameDocument2 = "Uploader_testing_document.txt";
		editFileNameDocument1 = "editdatatest.pdf";
	}
	
	@Test(groups = { "regression" },description = "UploaderLender09 - Change Document Name")
	public void Uploader_09_ChangeDocumentName()
	{
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("Precondition 02. Create new loans and get loans' id");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.createNewLoan(loanName, "", "", loanStatus, loanName);
		
		log.info("Precondition 03. Upload data tape");
		loansPage.openPropertiesTab();
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("Precondition 04. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("Precondition 05. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameLender, passwordLender);
		
		log.info("Uploader_09 - Step 01. Select loans item");
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("Uploader_09 - Step 02. Open Property folder");
		uploaderPage.clickPropertiesFolder();
		
		log.info("Uploader_09 - Step 03. Open Property item");
		uploaderPage.selectPropertyFolder(property1Name);
		
		log.info("Uploader_09 - Step 04. Upload document for a document type");
		uploaderPage.clickOnSelectNewFileIconByType(documentType1);
		uploaderPage.uploadFileOnUploader(fileNameDocument1);
		
		log.info("Uploader_09 - Step 05. Wait for upload complete");
		uploaderPage.waitForUploadCompleteOnUploader(fileNameDocument1);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(fileNameDocument1));
		
		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, documentType1));
		
		log.info("Uploader_09 - Step 07. Update document type for document");
		uploaderPage.clickOnEditDocumentIcon(fileNameDocument1);
		
		log.info("Uploader_09 - Step 08. Select new document type");
		uploaderPage.typeDocumentName(editFileNameDocument1);
		
		log.info("Uploader_09 - Step 09. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.clickYesButton();
		
		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(editFileNameDocument1, documentType1));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender10 - Upload New document")
	public void Uploader_10_UploadNewDocument()
	{
		log.info("Uploader_10 - Step 01. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherDocuments();
		
		log.info("Uploader_10 - Step 02. Select file to upload");
		uploaderPage.uploadFileOnUploader(fileNameDocument2);
		uploaderPage.waitForAddOtherDocument(fileNameDocument2);
			
		log.info("Uploader_10 - Step 03. Click 'OK' Button");
		uploaderPage.clickOkButtonOnKeywords();
		uploaderPage.waitForUploadCompleteOnUploader(fileNameDocument2);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(fileNameDocument2));
		
		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument2, "Unknown"));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender11 - View Document")
	public void Uploader_11_ViewDocument()
	{
		log.info("Uploader_11 - Step 01. Click on 'Eye' icon to view document");
		uploaderPage.clickOnViewDocumentIcon(fileNameDocument2);
		
		log.info("VP: The document is openned to view");
		verifyTrue(uploaderPage.isDocumentViewable(fileNameDocument2));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender12 - Attach document through the pencil screen")
	public void Uploader_12_AttachDocumentThroughThePencilScreen()
	{
		log.info("Uploader_12 - Step 01. Click on 'pencil' icon to edit document");
		uploaderPage.clickOnEditDocumentIcon(editFileNameDocument1);
		
		log.info("Uploader_12 - Step 02. Select other property on edit document popup");
		uploaderPage.selectPropertyOnEditDocument(property2Name);
		
		log.info("Uploader_12 - Step 03. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.clickYesButton();
		
		log.info("VP: Document is removed from property1 to property2");
		verifyFalse(uploaderPage.isDocumentTypeDisplay(editFileNameDocument1, documentType1));
		uploaderPage.selectPropertyFolder(property2Name);
		verifyTrue(uploaderPage.isDocumentTypeDisplay(editFileNameDocument1, documentType1));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender, loanStatus;
	private String loanName, uploaderPageUrl, propertiesFileName, fileNameDocument2, property2Name;
	private String property1Name, fileNameDocument1, documentType1, editFileNameDocument1;
}