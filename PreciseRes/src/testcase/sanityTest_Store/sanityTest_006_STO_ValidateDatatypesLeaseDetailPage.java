package sanityTest_Store;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;

import common.AbstractTest;
import common.Constant;

public class sanityTest_006_STO_ValidateDatatypesLeaseDetailPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + getUniqueNumber();
		invalidCurrency = "$40,0.00*%#$fail";
		invalidDate = "4/21/2015*%#$fail";
		invalidNumber = "20%*%#$";
		paymentFrequency = "M";
		increaseFrequency = "12";
		increaseType = "CPIL";
		purchaseOptionType = "NOCON";
		nnType = "MOD3N";
		validCurrency = "$2,000,000.00";
		validDate = "10/20/2015";
		validNumber = getUniqueNumber();
		validText = "STORE Capital Corporation";
		validRate = "2.36%";
	}

	@Test(groups = { "regression" }, description = "Validate Datatypes 01 - No input the data")
	public void ValidataDatatypes_01_NoInputTheData() {
		
		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);
		
		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("Precondition - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("ValidataDatatypes_01 - Step 01: Open Lessee detail tab");
		transactionPage.openLeaseDetailsTab();
		
		log.info("ValidataDatatypes_01 - Step 02: Input the data empty to 'Next Increase Date' field");
		transactionPage.inputNextIncreaseDate(" ");
		
		log.info("ValidataDatatypes_01 - Step 03: Input the data empty to 'Increase Frequency' field");
		transactionPage.inputIncreaseFrequency(" ");
		
		log.info("ValidataDatatypes_01 - Step 04: Input the data empty to 'Increase Rate' field");
		transactionPage.inputIncreaseRate(" ");
		
		log.info("ValidataDatatypes_01 - Step 05: Input the data empty to 'Leverage Factor' field");
		transactionPage.inputLeverageFactor(" ");
		
		log.info("ValidataDatatypes_01 - Step 06: Input the data empty to 'Adjustment Description' field");
		transactionPage.inputAdjustmentDescription(" ");
		
		log.info("ValidataDatatypes_01 - Step 07: Check to 'Rent Reset' checkbox");
		transactionPage.selectRentReset();
		
		log.info("ValidataDatatypes_01 - Step 08: Check to 'Tenant Purchase Option' checkbox");
		transactionPage.selectTenantPurchaseOption();
		
		log.info("ValidataDatatypes_01 - Step 09: Uncheck to 'TripleNet' checkbox");
		transactionPage.scrollPage(driver);
		transactionPage.unSelectTripleNet();
		
		log.info("ValidataDatatypes_01 - Step 10: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Next Increase Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_NextIncrease", "Next Increase Date is required"));
		
		log.info("VP: 'Increase Frequency is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_IncreaseFrequency", "Increase Frequency is required"));
		
		log.info("VP: 'Adjustment Description is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_AdjustmentDescription", "Adjustment Description is required"));
		
		log.info("VP: 'Next Reset Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_NextResetDate", "Next Reset Date is required"));
		
		log.info("VP: 'Reset Frequency is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_ResetFrequency", "Reset Frequency is required"));
		
		log.info("VP: 'Purchase Option Type is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_PurschareOptionType", "Purchase Option Type is required"));
		
		log.info("VP: 'Purchase Option Timing is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_PurschareOptionTiming", "Purchase Option Timing is required"));
		
		log.info("VP: 'Purchase Option Terms is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_PurschareOptionTerm", "Purchase Option Terms is required"));
		
		log.info("VP: 'NN Type is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_NNType", "NN Type is required"));
		
		log.info("VP: 'Description of Landlord Obligations is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_LandlordObligations", "Description of Landlord Obligations is required"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 02 - Input the invalid/ incorrect at Currency fields")
	public void ValidataDatatypes_02_InputInvalidOrIncorrectAtCurrencyFields() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("ValidataDatatypes_02 - Step 01: Open Lessee detail tab");
		transactionPage.openLeaseDetailsTab();
		
		log.info("ValidataDatatypes_02 - Step 02: Input the invalid data to 'Straight Line Average Rent Payment' field");
		transactionPage.inputStraightLineAverageRentPayment(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 03: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Currency value must be a number' message is displayed at 'Straight Line Average Rent Payment' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_StraightLineAvgRentPmt", "Currency value must be a number"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 03 - Input the invalid/ incorrect at Date fields")
	public void ValidataDatatypes_03_InputInvalidOrIncorrectAtDateFields() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("ValidataDatatypes_03 - Step 01: Open Lessee detail tab");
		transactionPage.openLeaseDetailsTab();
		
		log.info("ValidataDatatypes_03 - Step 02: Input the invalid data to 'Next Increase Date' field");
		transactionPage.inputNextIncreaseDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 03: Check to 'Rent Reset' field");
		transactionPage.selectRentReset();
		
		log.info("ValidataDatatypes_03 - Step 04: Input the invalid data to 'Next Reset Date' field");
		transactionPage.inputNextResetDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 05: Check to 'Tenant Purchase Option' checkbox");
		transactionPage.selectTenantPurchaseOption();
		
		log.info("ValidataDatatypes_03 - Step 06: Input the invalid data to 'Date First Exercisable' field");
		transactionPage.inputDateFirstExercisable(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 07: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Next Increase Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_NextIncrease", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Next Reset Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_NextResetDate", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Date First Exercisable' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_DateFirstExersable", "Value must be a date"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 04 - Input the invalid/ incorrect at Number fields")
	public void ValidataDatatypes_04_InputInvalidOrIncorrectAtNumberFields() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("ValidataDatatypes_04 - Step 01: Open Lessee detail tab");
		transactionPage.openLeaseDetailsTab();
		
		log.info("ValidataDatatypes_04 - Step 02: Input the invalid data to 'Increase Rate' field");
		transactionPage.inputIncreaseRate(invalidNumber);
		
		log.info("ValidataDatatypes_04 - Step 03: Input the invalid data to 'Leverage Factor' field");
		transactionPage.inputLeverageFactor(invalidNumber);
		
		log.info("ValidataDatatypes_04 - Step 04: Input the invalid data to 'Increase Frequency' field");
		transactionPage.inputIncreaseFrequency(invalidNumber);
		transactionPage.keyPressing("esc");

		log.info("ValidataDatatypes_04 - Step 05: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Value must be a whole number' message is displayed at 'Increase Frequency' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_IncreaseFrequency", "Value must be a whole number"));
		
		log.info("VP: 'Value left of % symbol must be a number' message is displayed at 'Increase Rate' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_IncreaseRate", "Please enter correct value ###.####%."));
		
		log.info("VP: 'Value must be a number' message is displayed at 'Leverage Factor' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_LeverageFactor", "Value must be a number"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 05 - Input the valid into all fields")
	public void ValidataDatatypes_05_InputValidIntoAllFields() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("ValidataDatatypes_05 - Step 01: Open Lease Details tab");
		transactionPage.openLeaseDetailsTab();
		
		log.info("ValidataDatatypes_05 - Step 02: Input the valid data to 'Payment Frequency' field");
		transactionPage.selectPaymentFrequency(paymentFrequency);
		
		log.info("ValidataDatatypes_05 - Step 03: Input the valid data to 'Increase Type' field");
		transactionPage.selectIncreaseType(increaseType);
		
		log.info("ValidataDatatypes_05 - Step 04: Input the valid data to 'Increase Frequency' field");
		transactionPage.inputIncreaseFrequency(increaseFrequency);
		
		log.info("ValidataDatatypes_05 - Step 05: Input the valid data to 'Next Increase Date' field");
		transactionPage.inputNextIncreaseDate(validDate);
		
		log.info("ValidataDatatypes_05 - Step 06: Input the valid data to 'Increase Rate' field");
		transactionPage.inputIncreaseRate(validRate);
		
		log.info("ValidataDatatypes_05 - Step 07: Input the valid data to 'Leverage Factor' field");
		transactionPage.inputLeverageFactor(validNumber);
		
		log.info("ValidataDatatypes_05 - Step 08: Input the valid data to 'Adjustment Description' field");
		transactionPage.inputAdjustmentDescription(validText);
		
		log.info("ValidataDatatypes_05 - Step 09: Input the valid data to 'Straight Line Average Rent Payment' field");
		transactionPage.inputStraightLineAverageRentPayment(validCurrency);
		
		log.info("ValidataDatatypes_05 - Step 10: Input the valid data to 'Rent Reset' field");
		transactionPage.selectRentReset();
		
		log.info("ValidataDatatypes_05 - Step 11: Input the valid data to 'Next Reset Date' field");
		transactionPage.inputNextResetDate(validDate);
		
		log.info("ValidataDatatypes_05 - Step 12: Input the valid data to 'Reset Frequency' field");
		transactionPage.inputResetFrequency(validText);
		
		log.info("ValidataDatatypes_05 - Step 13: Input the valid data to 'Tenant Purchase Option' field");
		transactionPage.selectTenantPurchaseOption();
		
		log.info("ValidataDatatypes_05 - Step 14: Input the valid data to 'Purchase Option Type' field");
		transactionPage.selectPurchaseOptionType(purchaseOptionType);
		
		log.info("ValidataDatatypes_05 - Step 15: Input the valid data to 'Purchase Option Timing' field");
		transactionPage.inputPurchaseOptionTiming(validNumber);
		
		log.info("ValidataDatatypes_05 - Step 16: Input the valid data to 'Purchase Option Terms' field");
		transactionPage.inputPurchaseOptionTerms(validNumber);
		
		log.info("ValidataDatatypes_05 - Step 17: Input the valid data to 'Date First Exercisable' field");
		transactionPage.inputDateFirstExercisable(validDate);
		
		log.info("ValidataDatatypes_05 - Step 18: Uncheck to 'TripleNet' field");
		transactionPage.scrollPage(driver);
		transactionPage.unSelectTripleNet();
		
		log.info("ValidataDatatypes_05 - Step 19: Input the valid data to 'Tenant subsitution/transfer rights' field");
		transactionPage.selectTenantSubsitutionTransferRights();
		
		log.info("ValidataDatatypes_05 - Step 20: Input the valid data to 'Collect Lease Sales Tax' field");
		transactionPage.selectCollectLeaseSalesTax();
		
		log.info("ValidataDatatypes_05 - Step 21: Input the valid data to 'Personal Property Lien' field");
		transactionPage.selectLeasePersonalPropertyLien();
		
		log.info("ValidataDatatypes_05 - Step 22: Input the valid data to 'NN Type' field");
		transactionPage.selectNNType(nnType);
		
		log.info("ValidataDatatypes_05 - Step 23: Input the valid data to 'Description of Landlord Obligations' field");
		transactionPage.inputDescriptionOfLandlordObligations(validText);
		
		log.info("ValidataDatatypes_05 - Step 24: Input the valid data to 'Liquor Sold' field");
		transactionPage.selectLeaseLiquorSold();
		
		log.info("ValidataDatatypes_05 - Step 25: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: Verify that 'Payment Frequency' is saved successfully");
		verifyTrue(transactionPage.isPaymentFrequencySavedSuccessfully(paymentFrequency));
		
		log.info("VP: Verify that 'Increase Type' is saved successfully");
		verifyTrue(transactionPage.isIncreaseTypeSavedSuccessfully(increaseType));
		
		log.info("VP: Verify that 'Increase Frequency' is saved successfully");
		verifyTrue(transactionPage.isIncreaseFrequencySavedSuccessfully(increaseFrequency));
		
		log.info("VP: Verify that 'Increase Rate' is saved successfully");
		verifyTrue(transactionPage.isIncreaseRateSavedSuccessfully(validRate));
		
		log.info("VP: Verify that 'Leverage Factor' is saved successfully");
		verifyTrue(transactionPage.isLeverageFactorSavedSuccessfully(validNumber));
		
		log.info("VP: Verify that 'Adjustment Description' is saved successfully");
		verifyTrue(transactionPage.isAdjustmentDescriptionSavedSuccessfully(validText));
		
		log.info("VP: Verify that 'Straight Line Average Rent Payment' is saved successfully");
		verifyTrue(transactionPage.isStraightLineAverageRentPaymentSavedSuccessfully(validCurrency));
		
		log.info("VP: Verify that 'Next Reset Date' is saved successfully");
		verifyTrue(transactionPage.isNextResetDateSavedSuccessfully(validDate));
		
		log.info("VP: Verify that 'Reset Frequency' is saved successfully");
		verifyTrue(transactionPage.isResetFrequencySavedSuccessfully(validText));
		
		log.info("VP: Verify that 'Purchase Option Type' is saved successfully");
		verifyTrue(transactionPage.isPurchaseOptionTypeSavedSuccessfully(purchaseOptionType));
		
		log.info("VP: Verify that 'Purchase Option Timing' is saved successfully");
		verifyTrue(transactionPage.isPurchaseOptionTimingSavedSuccessfully(validNumber));
		
		log.info("VP: Verify that 'Purchase Option Terms' is saved successfully");
		verifyTrue(transactionPage.isPurchaseOptionTermsSavedSuccessfully(validNumber));
		
		log.info("VP: Verify that 'Date First Exercisable' is saved successfully");
		verifyTrue(transactionPage.isDateFirstExercisableSavedSuccessfully(validDate));
		
		log.info("VP: Verify that 'NN Type' is saved successfully");
		verifyTrue(transactionPage.isNNTypeSavedSuccessfully(nnType));
		
		log.info("VP: Verify that 'Description of Landlord Obligations' is saved successfully");
		verifyTrue(transactionPage.isDescriptionOfLandlordObligationsSavedSuccessfully(validText));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName, invalidCurrency, invalidDate,invalidNumber;
	private String paymentFrequency, increaseType, purchaseOptionType, nnType, increaseFrequency;
	private String validCurrency, validDate, validNumber, validText, validRate;
	}