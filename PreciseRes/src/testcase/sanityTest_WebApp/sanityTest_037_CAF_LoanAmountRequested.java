package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_037_CAF_LoanAmountRequested extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		loanName = "UploaderUdiTesting"+getUniqueNumber();
		applicantName = "CAF Demo Applicant";
		status01 = "Term Sheet Issued";
		status02 = "Deals Under Review";
		loanAmountRequested = 15000;
	}

	@Test(groups = { "regression" }, description = "Dashboard 01 - Pre-Rent Roll Status")
	public void LoanAmountRequested_01_MoveLoansFromBucketToBucket() {
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("Precondition 02. Create new loans and get loans' id");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.createNewLoan(loanName, applicantName, "", status01);
		
		log.info("Precondition 03. Input Loan Amount Requested");
		loansPage.inputLoanAmountRequested(Integer.toString(loanAmountRequested));
		loansPage.inputFloor("10");
		loansPage.selectBridgeBorrower("Yes");
		
		log.info("Precondition 04. Click save button");
		loansPage.clickSaveButton();
		
		log.info("VP: Record Saved message displays");
		verifyTrue(loansPage.isRecordSavedMessageDisplays());
		
		log.info("Precondition 05. Open Dashboard page");
		homePage = loansPage.openHomePage(driver, ipClient);
		
		log.info("LoanAmountRequested_01 - Step 01. Get source bucket Amount number");
		sourceBucketAmountNumber= homePage.getBucketAmountNumber(status01);
		
		log.info("LoanAmountRequested_01 - Step 02. Get destination bucket Amount number");
		destinationBucketAmountNumber= homePage.getBucketAmountNumber(status02);
		
		log.info("LoanAmountRequested_01 - Step 03. Search for loan");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		
		log.info("LoanAmountRequested_01 - Step 04. Open loan basic detail tab");
		loansPage.openLoansDetailPage(loanName);
		
		log.info("LoanAmountRequested_01 - Step 05. Change loan status");
		loansPage.selectLoanStatusBasicDetail(status02);
		
		log.info("LoanAmountRequested_01 - Step 06. Click save button");
		loansPage.clickSaveButton();

		log.info("VP: Verify that Status is added successfully");
		verifyTrue(loansPage.isLoansReadOnlyDisplayWithCorrectInfo(loanName,status02));
		
		log.info("LoanAmountRequested_01 - Step 07. Open Dashboard page");
		homePage = loansPage.openHomePage(driver, ipClient);
		
		log.info("VP: Verify that source bucket amount number is correctly");
		verifyEquals(homePage.getBucketAmountNumber(status01),sourceBucketAmountNumber - loanAmountRequested);
		
		log.info("VP: Verify that destination bucket amount number is correctly");
		verifyEquals(homePage.getBucketAmountNumber(status02),destinationBucketAmountNumber + loanAmountRequested);
	}

	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender, status01, status02;
	private String applicantName;
	private String loanName;
	private int loanAmountRequested;
	private long sourceBucketAmountNumber, destinationBucketAmountNumber;
}