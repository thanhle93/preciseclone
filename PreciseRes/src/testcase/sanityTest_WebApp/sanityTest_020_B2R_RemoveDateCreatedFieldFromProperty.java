package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_020_B2R_RemoveDateCreatedFieldFromProperty extends
		AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		usernameApplicant = Constant.USERNAME_B2R_BORROWER;
		passwordApplicant = Constant.PASSWORD_B2R_BORROWER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower";
		propertiesFileName = "B2R Required.xlsx";
		address1 = "37412 OAKHILL ST";
	}

	@Test(groups = { "regression" }, description = "Loan70 - The field 'Date Created' has been removed from Property screen and from the searches")
	public void RemoveDateCreatedField_01_MakeSureLenderRemovedFieldDateCreated() {

		log.info("RemoveDateCreatedField_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender,false);

		log.info("RemoveDateCreatedField_01 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("RemoveDateCreatedField_01 - Step 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus,	loanName);
		
		log.info("RemoveDateCreatedField_01 - Step 04. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("VP: Make sure the field 'Date Created' has been removed from Property search");
		verifyFalse(loansPage.isDateCreatedNotDisplay());

		log.info("RemoveDateCreatedField_01 - Step 05. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("RemoveDateCreatedField_01 - Step 06. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("VP: Make sure the field 'Date Created' has been removed from Property screen");
		verifyFalse(propertiesPage.isDateCreatedNotDisplay());
	}

	@Test(groups = { "regression" }, description = "Loan71 - Login as borrower and make sure the field 'Date Created' has been removed from Property screen and from the searches")
	public void RemoveDateCreatedField_02_MakeSureBorrowerRemovedFieldDateCreated() {

		log.info("RemoveDateCreatedField_02 - Step 01. Log out and login with applicant");
		loginPage = logout(driver, ipClient);
		loansPage = loginPage.loginAsBorrower(usernameApplicant,	passwordApplicant, false);

		log.info("RemoveDateCreatedField_02 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("RemoveDateCreatedField_02 - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("RemoveDateCreatedField_02 - Step 03. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("RemoveDateCreatedField_02 - Step 04. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("VP: Make sure the field 'Date Created' has been removed from Property search");
		verifyFalse(loansPage.isDateCreatedNotDisplay());

		log.info("RemoveDateCreatedField_02 - Step 05. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("VP: Make sure the field 'Date Created' has been removed from Property screen");
		verifyFalse(propertiesPage.isDateCreatedNotDisplay());
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private String usernameLender, passwordLender, loanStatus;
	private String loanName, accountNumber, applicantName, propertiesFileName,address1;
	private String usernameApplicant, passwordApplicant;
}