package sanityTest_WebApp;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;

import common.AbstractTest;
import common.Constant;

public class sanityTest_041_B2R_UpdateFieldLoanBasicDetails extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan"+accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower New";
		state = "AA - Federal Services";
		propertyManagament = "In House";
		yearHold = "2-3 Years";
		source = "Proprietary";
		owner = "Kushy,Matthew";
		companyType = "Non Profit Corporation";
		validCurrency = "$2,000,000.00";
		validDate = "12/31/2000";
		validPhoneNumber = "12345678901";
		validZipCode = "12345";
		validText = "Common value !@#";
		validEmail = "abc@gmail.com";
		
		newAccountNumber = getUniqueNumber()+200;
		newLoanName = "UdiTeam-Loan"+newAccountNumber;
		newLoanStatus = "Due Diligence";
		newState = "AE - Federal Services";
		newPropertyManagament = "Outsourced";
		newYearHold = "3-4 Years";
		newSource = "Broker";
		newOwner = "Hensley,Adam";
		newCompanyType = "Limited Partnership";
		newValidCurrency = "$3,000,000.00";
		newValidDate = "11/11/2000";
		newValidPhoneNumber = "12345678902";
		newValidZipCode = "12346";
		newValidText = "Common new value !@#";
		newValidEmail = "newemail@gmail.com";
	}

	@Test(groups = { "regression" }, description = "Update field Loan Basic details 01 - Update field")
	public void UpdateFieldLoanBasicDetails_01_UpdateField() {
		
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Precondition 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);
		
		//input all valid date
		
		log.info("ValidataDatatypes_05 - Step 01: Input the valid data to 'Final Borrower LLC' field");
		loansPage.inputFinalBorrowerLLC(validText);
		
		log.info("ValidataDatatypes_05 - Step 02: Input the valid data to 'Company Website' field");
		loansPage.inputCompanyWebsite(validText);
		
		log.info("ValidataDatatypes_05 - Step 03: Input the valid data to 'Loan Amount Requested' field");
		loansPage.inputLoanAmountRequested(validCurrency);
		
		log.info("ValidataDatatypes_05 - Step 04: Input the valid data to 'SFR State' field");
		loansPage.selectSFRState(state);
		
		log.info("ValidataDatatypes_05 - Step 05: Input the valid data to 'Property Managament' field");
		loansPage.selectPropertyManagement(propertyManagament);
		
		log.info("ValidataDatatypes_05 - Step 06: Input the valid data to 'Outsourcing Company' field");
		loansPage.inputOutsourcingCompany(validText);
		
		log.info("ValidataDatatypes_05 - Step 07: Input the valid data to 'Portfolio Hold' field");
		loansPage.selectPotfolioHold(yearHold);
		
		log.info("ValidataDatatypes_05 - Step 08: Input the valid data to 'Source' field");
		loansPage.selectSource(source);
		
		log.info("ValidataDatatypes_05 - Step 09: Input the valid data to 'Midland ID' field");
		loansPage.inputMidlandID(validText);
		
		log.info("ValidataDatatypes_05 - Step 10: Input the valid data to 'Company Type' field");
		loansPage.selectCompanyType(companyType);
		
		log.info("ValidataDatatypes_05 - Step 11: Input the valid data to 'Place Of Formation' field");
		loansPage.inputPlaceOfFormation(validText);
		
		log.info("ValidataDatatypes_05 - Step 12: Input the valid data to 'Primary Contact' field");
		loansPage.inputPrimaryContact(validText);
		
		log.info("ValidataDatatypes_05 - Step 13: Input the valid data to 'Phone' field");
		loansPage.inputPhoneTextbox(validPhoneNumber);
		
		log.info("ValidataDatatypes_05 - Step 14: Input the valid data to 'Email' field");
		loansPage.inputEmailTextbox(validEmail);
		
		log.info("ValidataDatatypes_05 - Step 15: Input the valid data to 'Address' field");
		loansPage.inputAddress(validText);
		
		log.info("ValidataDatatypes_05 - Step 16: Input the valid data to 'City' field");
		loansPage.inputCity(validText);
		
		log.info("ValidataDatatypes_05 - Step 17: Input the valid data to 'State' field");
		loansPage.selectState(state);
		
		log.info("ValidataDatatypes_05 - Step 18: Input the valid data to 'Zip' field");
		loansPage.inputZipCode(validZipCode);
		
		log.info("ValidataDatatypes_05 - Step 19: Input the valid data to 'Comment' field");
		loansPage.inputCommentTextbox(validText);
		
		log.info("ValidataDatatypes_05 - Step 20: Input the invalid data to 'SFR Buying Started' field");
		loansPage.inputSFRBuyingStarted(validDate);
		
		log.info("ValidataDatatypes_05 - Step 21: Input the invalid data to 'Date Established ");
		loansPage.inputDateEstablished(validDate);
		
		log.info("ValidataDatatypes_05 - Step 22: Input the valid data to 'Owner' field");
		loansPage.selectOwner(owner);
		
		log.info("ValidataDatatypes_05 - Step 23: Click 'Save' button");
		loansPage.clickSaveButton();
		
		//Check all data saved correclty
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(loansPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: Verify that 'Final BorrowerLLC' is saved successfully");
		verifyTrue(loansPage.isFinalBorrowerLLCDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'SFR Buying Started' is saved successfully");
		verifyTrue(loansPage.isSFRBuyingStartedDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that 'Company Website' is saved successfully");
		verifyTrue(loansPage.isCompanyWebsiteDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Outsourcing Company' is saved successfully");
		verifyTrue(loansPage.isOutsourcingCompanyDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Place Of Formation' is saved successfully");
		verifyTrue(loansPage.isPlaceOfFormationDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Date Established' is saved successfully");
		verifyTrue(loansPage.isDateEstablishedDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that 'Primary Contact' is saved successfully");
		verifyTrue(loansPage.isPrimaryContactDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Phone Textbox' is saved successfully");
		verifyTrue(loansPage.isPhoneTextboxDisplayedCorrectly(validPhoneNumber));
		
		log.info("VP: Verify that 'Email' is saved successfully");
		verifyTrue(loansPage.isEmailTextboxDisplayedCorrectly(validEmail));
		
		log.info("VP: Verify that 'Address' is saved successfully");
		verifyTrue(loansPage.isAddressDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'City' is saved successfully");
		verifyTrue(loansPage.isCityDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Zipcode' is saved successfully");
		verifyTrue(loansPage.isZipcodeDisplayedCorrectly(validZipCode));
		
		log.info("VP: Verify that 'Comment' is saved successfully");
		verifyTrue(loansPage.isCommentTextboxDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'SFR State' is saved successfully");
		verifyTrue(loansPage.isSFRStateDisplayedCorrectly(state));
		
		log.info("VP: Verify that 'Property Managament' is saved successfully");
		verifyTrue(loansPage.isPropertyManagementDisplayedCorrectly(propertyManagament));
		
		log.info("VP: Verify that 'Potfolio Hold' is saved successfully");
		verifyTrue(loansPage.isPotfolioHoldDisplayedCorrectly(yearHold));
		
		log.info("VP: Verify that 'Source' is saved successfully");
		verifyTrue(loansPage.isSourceDisplayedCorrectly(source));
		
		log.info("VP: Verify that 'Owner' is saved successfully");
		verifyTrue(loansPage.isOwnerDisplayedCorrectly(owner));
		
		log.info("VP: Verify that 'Company Type' is saved successfully");
		verifyTrue(loansPage.isCompanyTypeDisplayedCorrectly(companyType));
		
		log.info("VP: Verify that 'State' is saved successfully");
		verifyTrue(loansPage.isStateDisplayedCorrectly(state));
		
		log.info("VP: Verify that 'Midland ID' is saved successfully");
		verifyTrue(loansPage.isMidlandIDDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Loan Amount Requested' is saved successfully");
		verifyTrue(loansPage.isLoanAmountRequestedDisplayedCorrectly(validCurrency));
		
		//input new valid data
		
		log.info("ValidataDatatypes_05 - Step 01: Input the valid data to 'Loan Name' field");
		loansPage.inputLoanName(newLoanName);
		
		log.info("ValidataDatatypes_05 - Step 02: Input the valid data to 'Loan ID' field");
		loansPage.inputLoanID(newLoanName);
		
		log.info("ValidataDatatypes_05 - Step 04: Input the valid data to 'Loan status' field");
		loansPage.selectLoanStatusBasicDetail(newLoanStatus);
		
		log.info("ValidataDatatypes_05 - Step 01: Input the valid data to 'Final Borrower LLC' field");
		loansPage.inputFinalBorrowerLLC(newValidText);
		
		log.info("ValidataDatatypes_05 - Step 02: Input the valid data to 'Company Website' field");
		loansPage.inputCompanyWebsite(newValidText);
		
		log.info("ValidataDatatypes_05 - Step 03: Input the valid data to 'Loan Amount Requested' field");
		loansPage.inputLoanAmountRequested(newValidCurrency);
		
		log.info("ValidataDatatypes_05 - Step 04: Input the valid data to 'SFR State' field");
		loansPage.selectSFRState(newState);
		
		log.info("ValidataDatatypes_05 - Step 05: Input the valid data to 'Property Managament' field");
		loansPage.selectPropertyManagement(newPropertyManagament);
		
		log.info("ValidataDatatypes_05 - Step 06: Input the valid data to 'Outsourcing Company' field");
		loansPage.inputOutsourcingCompany(newValidText);
		
		log.info("ValidataDatatypes_05 - Step 07: Input the valid data to 'Portfolio Hold' field");
		loansPage.selectPotfolioHold(newYearHold);
		
		log.info("ValidataDatatypes_05 - Step 08: Input the valid data to 'Source' field");
		loansPage.selectSource(newSource);
		
		log.info("ValidataDatatypes_05 - Step 09: Input the valid data to 'Midland ID' field");
		loansPage.inputMidlandID(newValidText);
		
		log.info("ValidataDatatypes_05 - Step 10: Input the valid data to 'Company Type' field");
		loansPage.selectCompanyType(newCompanyType);
		
		log.info("ValidataDatatypes_05 - Step 11: Input the valid data to 'Place Of Formation' field");
		loansPage.inputPlaceOfFormation(newValidText);
		
		log.info("ValidataDatatypes_05 - Step 12: Input the valid data to 'Primary Contact' field");
		loansPage.inputPrimaryContact(newValidText);
		
		log.info("ValidataDatatypes_05 - Step 13: Input the valid data to 'Phone' field");
		loansPage.inputPhoneTextbox(newValidPhoneNumber);
		
		log.info("ValidataDatatypes_05 - Step 14: Input the valid data to 'Email' field");
		loansPage.inputEmailTextbox(newValidEmail);
		
		log.info("ValidataDatatypes_05 - Step 15: Input the valid data to 'Address' field");
		loansPage.inputAddress(newValidText);
		
		log.info("ValidataDatatypes_05 - Step 16: Input the valid data to 'City' field");
		loansPage.inputCity(newValidText);
		
		log.info("ValidataDatatypes_05 - Step 17: Input the valid data to 'State' field");
		loansPage.selectState(newState);
		
		log.info("ValidataDatatypes_05 - Step 18: Input the valid data to 'Zip' field");
		loansPage.inputZipCode(newValidZipCode);
		
		log.info("ValidataDatatypes_05 - Step 19: Input the valid data to 'Comment' field");
		loansPage.inputCommentTextbox(newValidText);
		
		log.info("ValidataDatatypes_05 - Step 20: Input the invalid data to 'SFR Buying Started' field");
		loansPage.inputSFRBuyingStarted(newValidDate);
		
		log.info("ValidataDatatypes_05 - Step 21: Input the invalid data to 'Date Established ");
		loansPage.inputDateEstablished(newValidDate);
		
		log.info("ValidataDatatypes_05 - Step 22: Input the valid data to 'Owner' field");
		loansPage.selectOwner(newOwner);
		
		log.info("ValidataDatatypes_05 - Step 23: Click 'Save' button");
		loansPage.clickSaveButton();
		
		//Check all data saved correclty
		
		log.info("VP: Verify that 'Loan Name' is saved successfully");
		verifyTrue(loansPage.isLoanNameDisplayed(newLoanName));
		
		log.info("VP: Verify that 'Loan ID' is saved successfully");
		verifyTrue(loansPage.isLoanIDDisplayed(newLoanName));
		
		log.info("VP: Verify that 'Loan Status' is saved successfully");
		verifyTrue(loansPage.isLoanStatusDisplayedCorrectly(newLoanStatus));
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(loansPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: Verify that 'Final BorrowerLLC' is saved successfully");
		verifyTrue(loansPage.isFinalBorrowerLLCDisplayedCorrectly(newValidText));
		
		log.info("VP: Verify that 'SFR Buying Started' is saved successfully");
		verifyTrue(loansPage.isSFRBuyingStartedDisplayedCorrectly(newValidDate));
		
		log.info("VP: Verify that 'Company Website' is saved successfully");
		verifyTrue(loansPage.isCompanyWebsiteDisplayedCorrectly(newValidText));
		
		log.info("VP: Verify that 'Outsourcing Company' is saved successfully");
		verifyTrue(loansPage.isOutsourcingCompanyDisplayedCorrectly(newValidText));
		
		log.info("VP: Verify that 'Place Of Formation' is saved successfully");
		verifyTrue(loansPage.isPlaceOfFormationDisplayedCorrectly(newValidText));
		
		log.info("VP: Verify that 'Date Established' is saved successfully");
		verifyTrue(loansPage.isDateEstablishedDisplayedCorrectly(newValidDate));
		
		log.info("VP: Verify that 'Primary Contact' is saved successfully");
		verifyTrue(loansPage.isPrimaryContactDisplayedCorrectly(newValidText));
		
		log.info("VP: Verify that 'Phone Textbox' is saved successfully");
		verifyTrue(loansPage.isPhoneTextboxDisplayedCorrectly(newValidPhoneNumber));
		
		log.info("VP: Verify that 'Email' is saved successfully");
		verifyTrue(loansPage.isEmailTextboxDisplayedCorrectly(newValidEmail));
		
		log.info("VP: Verify that 'Address' is saved successfully");
		verifyTrue(loansPage.isAddressDisplayedCorrectly(newValidText));
		
		log.info("VP: Verify that 'City' is saved successfully");
		verifyTrue(loansPage.isCityDisplayedCorrectly(newValidText));
		
		log.info("VP: Verify that 'Zipcode' is saved successfully");
		verifyTrue(loansPage.isZipcodeDisplayedCorrectly(newValidZipCode));
		
		log.info("VP: Verify that 'Comment' is saved successfully");
		verifyTrue(loansPage.isCommentTextboxDisplayedCorrectly(newValidText));
		
		log.info("VP: Verify that 'SFR State' is saved successfully");
		verifyTrue(loansPage.isSFRStateDisplayedCorrectly(newState));
		
		log.info("VP: Verify that 'Property Managament' is saved successfully");
		verifyTrue(loansPage.isPropertyManagementDisplayedCorrectly(newPropertyManagament));
		
		log.info("VP: Verify that 'Potfolio Hold' is saved successfully");
		verifyTrue(loansPage.isPotfolioHoldDisplayedCorrectly(newYearHold));
		
		log.info("VP: Verify that 'Source' is saved successfully");
		verifyTrue(loansPage.isSourceDisplayedCorrectly(newSource));
		
		log.info("VP: Verify that 'Owner' is saved successfully");
		verifyTrue(loansPage.isOwnerDisplayedCorrectly(newOwner));
		
		log.info("VP: Verify that 'Company Type' is saved successfully");
		verifyTrue(loansPage.isCompanyTypeDisplayedCorrectly(newCompanyType));
		
		log.info("VP: Verify that 'State' is saved successfully");
		verifyTrue(loansPage.isStateDisplayedCorrectly(newState));
		
		log.info("VP: Verify that 'Midland ID' is saved successfully");
		verifyTrue(loansPage.isMidlandIDDisplayedCorrectly(newValidText));
		
		log.info("VP: Verify that 'Loan Amount Requested' is saved successfully");
		verifyTrue(loansPage.isLoanAmountRequestedDisplayedCorrectly(newValidCurrency));
	}
	
	@Test(groups = { "regression" }, description = "Update field Loan Basic details 02 - Check history box")
	public void UpdateFieldLoanBasicDetails_02_CheckHistoryBox() {
		//Check all data saved correclty
		
		log.info("VP: Verify that 'Loan Name' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("LOAN_NAME_TEXTBOX_BASIC_DETAIL_HISTORY", loanName, newLoanName));
		
		log.info("VP: Verify that 'Loan ID' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("LOAN_APPLICATION_ID_HISTORY", loanName, newLoanName));
		
		log.info("VP: Verify that 'Loan Status' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("LOAN_STATUS_COMBOBOX_BASIC_DETAIL_HISTORY", loanStatus, newLoanStatus));
		
		log.info("VP: Verify that 'Final BorrowerLLC' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("FINAL_BORROWER_LLC_HISTORY", validText, newValidText));
		
		log.info("VP: Verify that 'Company Website' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("COMPANY_WEBSITE_TEXTBOX_HISTORY", validText, newValidText));
		
		log.info("VP: Verify that 'Date Established' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("DATE_ESTABLISHED_TEXTBOX_HISTORY", validDate, newValidDate));
		
		log.info("VP: Verify that 'Phone Textbox' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("PHONE_TEXTBOX_HISTORY", validPhoneNumber, newValidPhoneNumber));
		
		log.info("VP: Verify that 'Email' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("EMAIL_TEXTBOX_HISTORY", validEmail, newValidEmail));
		
		log.info("VP: Verify that 'Address' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("ADDRESS_TEXTBOX_HISTORY", validText, newValidText));
		
		log.info("VP: Verify that 'City' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("CITY_TEXTBOX_HISTORY", validText, newValidText));
		
		log.info("VP: Verify that 'Zipcode' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("ZIPCODE_TEXTBOX_HISTORY", validZipCode, newValidZipCode));
		
		log.info("VP: Verify that 'Source' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("SOURCE_DROPDOWN_HISTORY", source, newSource));
		
		log.info("VP: Verify that 'Owner' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("OWNER_DROPDOWN_HISTORY", owner, newOwner));
		
		log.info("VP: Verify that 'Company Type' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("COMPANY_TYPE_DROPDOWN_HISTORY", companyType, newCompanyType));
		
		log.info("VP: Verify that 'Midland ID' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("MIDLAND_ID_TEXTBOX_HISTORY", validText, newValidText));
		
		log.info("VP: Verify that 'Loan Amount Requested' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("LOAN_AMOUNT_REQUESTED_TEXTBOX_HISTORY", validCurrency, newValidCurrency));
	
		log.info("VP: Verify that 'State' is saved successfully");
		verifyTrue(loansPage.isHistoryPopupBoxDisplayedCorrectly("STATE_DROPDOWN_HISTORY", "AA", "AE"));
	}
	
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender;
	private String loanName, loanStatus, accountNumber, applicantName;
	private String state, propertyManagament, yearHold, source, owner, companyType;
	private String validEmail, validCurrency, validDate, validPhoneNumber, validText, validZipCode;
	private String newLoanName, newLoanStatus, newAccountNumber;
	private String newState, newPropertyManagament, newYearHold, newSource, newOwner, newCompanyType;
	private String newValidEmail, newValidCurrency, newValidDate, newValidPhoneNumber, newValidText, newValidZipCode;
	}