package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Common;
import common.Constant;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;

public class sanityTest_029_CAF_SearchLoanByViewAllButtonAtDashboard extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "Loan" + accountNumber;
		loanNameWithoutApplicant = "LoanWithoutApp" + accountNumber;
		loanStatusOld = "Deals Under Review";
		loanStatus = Constant.LOAN_STATUS_CAF;
		applicantName = "CAF Demo Applicant";
		propertiesFileName = "CAF Required.xlsx";
		day = Common.getCommon().getCurrentDayOfWeek();
		month = Common.getCommon().getCurrentMonthOfWeek();
		year = Common.getCommon().getCurrentYearOfWeek();
		inputDay = month + "/" + day + "/" + year;
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 01 - Search by Loan name")
	public void SearchLoanHome_01_SearchByLoanName() {
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Precondition 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatusOld);

		log.info("Precondition 04. Change Loan Status to get Status Change Date");
		loansPage.selectLoanStatusBasicDetail(loanStatus);

		log.info("Precondition 05. Input Term Sheet Issued Date");
		loansPage.inputTermSheetIssuedDate(inputDay);

		log.info("Precondition 06. Input Term Sheet Signed Date");
		loansPage.inputTermSheetSignedDate(inputDay);

		log.info("Precondition 07. Input Loan Amount Requested");
		loansPage.inputLoanAmountRequested("1000");

		log.info("Precondition 08. Click Save button");
		loansPage.clickSaveButton();
		loansPage.isRecordSavedMessageDisplays();

		log.info("Precondition 09. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Precondition 10. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("SearchLoanHome_01 - Step 01. Open Home page");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("SearchLoanHome_01 - Step 02. Click 'View All' button");
		homePage.clickOnViewAllButton();

		log.info("SearchLoanHome_01 - Step 03. Search by Loan name");
		homePage.searchForLoansHomePageCAF(loanName, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");

		statusChange = homePage.getTextLoanSearch("DataCell FieldTypeDate datacell_StatusChangeDate");
		termSheetIssued = homePage.getTextLoanSearch("DataCell FieldTypeDate datacell_TermsheetDate");
		termSheetSigned = homePage.getTextLoanSearch("DataCell FieldTypeDate datacell_TermSheetSignedDate");
		properties = homePage.getTextLoanSearch("DataCell FieldTypeInteger datacell_PropertyTotal");
		estimatedValue = homePage.getTextLoanSearch("DataCell FieldTypeCurrency datacell_EstimatedPortfolioValue");
		reqLoanAmount = homePage.getTextLoanSearch("DataCell FieldTypeCurrency datacell_LoanAmount");
		requiredDocs = homePage.getTextLoanSearch("DataCell FieldTypeInteger datacell_RequiredDocs");
		noLoaded = homePage.getTextLoanSearch("DataCell FieldTypeInteger datacell_LoadedDocsCount");
		noReviewed = homePage.getTextLoanSearch("DataCell FieldTypeInteger datacell_ReviewedDocsCount");
		noRevised = homePage.getTextLoanSearch("DataCell FieldTypeHTML datacell_Revised");
		dateCreated = homePage.getTextLoanSearch("DataCell FieldTypeDate datacell_CreateDate");

		log.info("VP: Loan name display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, statusChange, termSheetIssued, termSheetSigned, properties, estimatedValue, reqLoanAmount,
				requiredDocs, noLoaded, noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 02 - Search by Applicant name")
	public void SearchLoanHome_02_SearchByApplicantName() {

		log.info("SearchLoanHome_02 - Step 01. Search by Applicant name");
		homePage.searchForLoansHomePageCAF(loanName, applicantName, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");

		log.info("VP: Applicant name display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, statusChange, termSheetIssued, termSheetSigned, properties, estimatedValue, reqLoanAmount,
				requiredDocs, noLoaded, noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 03 - Search by Status Change")
	public void SearchLoanHome_03_SearchByStatusChange() {

		log.info("SearchLoanHome_03 - Step 01. Search by Status Change");
		homePage.searchForLoansHomePageCAF(loanName, " ", statusChange, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");

		log.info("VP: Status Change display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, statusChange, termSheetIssued, termSheetSigned, properties, estimatedValue, reqLoanAmount,
				requiredDocs, noLoaded, noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 04 - Search by Term Sheet Issued")
	public void SearchLoanHome_04_SearchByTermSheetIssued() {

		log.info("SearchLoanHome_04 - Step 01. Search by Term Sheet Issued");
		homePage.searchForLoansHomePageCAF(loanName, " ", " ", termSheetIssued, " ", " ", " ", " ", " ", " ", " ", " ", " ");

		log.info("VP: Term Sheet Issued display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, statusChange, termSheetIssued, termSheetSigned, properties, estimatedValue, reqLoanAmount,
				requiredDocs, noLoaded, noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 05 - Search by Term Sheet Signed")
	public void SearchLoanHome_05_SearchByTermSheetSigned() {

		log.info("SearchLoanHome_05 - Step 01. Search by Term Sheet Signed");
		homePage.searchForLoansHomePageCAF(loanName, " ", " ", termSheetIssued, " ", " ", " ", " ", " ", " ", " ", " ", " ");

		log.info("VP: Term Sheet Signed display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, statusChange, termSheetIssued, termSheetSigned, properties, estimatedValue, reqLoanAmount,
				requiredDocs, noLoaded, noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 06 - Search by Properties")
	public void SearchLoanHome_06_SearchByProperties() {

		log.info("SearchLoanHome_06 - Step 01. Search by Properties");
		homePage.searchForLoansHomePageCAF(loanName, " ", " ", " ", " ", properties, " ", " ", " ", " ", " ", " ", " ");

		log.info("VP: Properties display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, statusChange, termSheetIssued, termSheetSigned, properties, estimatedValue, reqLoanAmount,
				requiredDocs, noLoaded, noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 07 - Search by Estimated Value")
	public void SearchLoanHome_07_SearchByEstimatedValue() {

		log.info("SearchLoanHome_07 - Step 01. Search by Estimated Value");
		homePage.searchForLoansHomePageCAF(loanName, " ", " ", " ", " ", " ", estimatedValue, " ", " ", " ", " ", " ", " ");

		log.info("VP: Estimated Value display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, statusChange, termSheetIssued, termSheetSigned, properties, estimatedValue, reqLoanAmount,
				requiredDocs, noLoaded, noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 07 - Search by Req. Loan Amount")
	public void SearchLoanHome_08_SearchByReqLoanAmount() {

		log.info("SearchLoanHome_08 - Step 01. Search by Req. Loan Amount");
		homePage.searchForLoansHomePageCAF(loanName, " ", " ", " ", " ", " ", " ", reqLoanAmount, " ", " ", " ", " ", " ");

		log.info("VP: Req. Loan Amount display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, statusChange, termSheetIssued, termSheetSigned, properties, estimatedValue, reqLoanAmount,
				requiredDocs, noLoaded, noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 09 - Search by Required Docs")
	public void SearchLoanHome_09_SearchByRequiredDocs() {

		log.info("SearchLoanHome_09 - Step 01. Search by Required Docs");
		homePage.searchForLoansHomePageCAF(loanName, " ", " ", " ", " ", " ", " ", " ", requiredDocs, " ", " ", " ", " ");

		log.info("VP: Required Docs display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, statusChange, termSheetIssued, termSheetSigned, properties, estimatedValue, reqLoanAmount,
				requiredDocs, noLoaded, noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 10 - Search by No. Loaded")
	public void SearchLoanHome_10_SearchByNoLoaded() {

		log.info("SearchLoanHome_10 - Step 01. Search by No. Loaded");
		homePage.searchForLoansHomePageCAF(loanName, " ", " ", " ", " ", " ", " ", " ", " ", noLoaded, " ", " ", " ");

		log.info("VP: No. Loaded display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, statusChange, termSheetIssued, termSheetSigned, properties, estimatedValue, reqLoanAmount,
				requiredDocs, noLoaded, noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 11 - Search by No. Reviewed")
	public void SearchLoanHome_11_SearchByNoReviewed() {

		log.info("SearchLoanHome_11 - Step 01. Search by No. Reviewed");
		homePage.searchForLoansHomePageCAF(loanName, " ", " ", " ", " ", " ", " ", " ", " ", " ", noReviewed, " ", " ");

		log.info("VP: No. Reviewed display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, statusChange, termSheetIssued, termSheetSigned, properties, estimatedValue, reqLoanAmount,
				requiredDocs, noLoaded, noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 12 - Search by No. Revised")
	public void SearchLoanHome_12_SearchByNoRevised() {

		log.info("SearchLoanHome_12 - Step 01. Search by No. Revised");
		homePage.searchForLoansHomePageCAF(loanName, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", noRevised, " ");

		log.info("VP: No. Revised display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, statusChange, termSheetIssued, termSheetSigned, properties, estimatedValue, reqLoanAmount,
				requiredDocs, noLoaded, noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 13 - Search by Date Created")
	public void SearchLoanHome_13_SearchByDateCreated() {

		log.info("SearchLoanHome_13 - Step 01. Search by Date Created");
		homePage.searchForLoansHomePageCAF(loanName, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", dateCreated);

		log.info("VP: Date Created display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, statusChange, termSheetIssued, termSheetSigned, properties, estimatedValue, reqLoanAmount,
				requiredDocs, noLoaded, noReviewed, noRevised, dateCreated));
	}

	@Test(groups = { "regression" }, description = "SearchLoanHome 14 - Search by Loan name without Applicant")
	public void SearchLoanHome_14_SearchByLoanNameWithoutApplicant() {

		log.info("Precondition 01. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 02. Create new loan");
		loansPage.createNewLoan(loanNameWithoutApplicant, "", "", loanStatus);

		log.info("Precondition 03. Change status of Loan");
		loansPage.selectLoanStatusBasicDetail("Term Sheet Issued");

		log.info("Precondition 04. Input Loan Amount Requested");
		loansPage.inputLoanAmountRequested("1000");

		log.info("Precondition 05. Click Save button");
		loansPage.clickSaveButton();

		log.info("Precondition 06. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Precondition 07. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("SearchLoanHome_13 - Step 01. Open Home page");
		homePage = loansPage.openHomePage(driver, ipClient);

		log.info("SearchLoanHome_13 - Step 02. Click 'View All' button");
		homePage.clickOnViewAllButton();

		log.info("SearchLoanHome_13 - Step 03. Search by Loan name");
		homePage.searchForLoansHomePageCAF(loanNameWithoutApplicant, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");

		statusChange = homePage.getTextLoanSearch("DataCell FieldTypeDate datacell_StatusChangeDate");
		termSheetIssued = homePage.getTextLoanSearch("DataCell FieldTypeDate datacell_TermsheetDate");
		termSheetSigned = homePage.getTextLoanSearch("DataCell FieldTypeDate datacell_TermSheetSignedDate");
		properties = homePage.getTextLoanSearch("DataCell FieldTypeInteger datacell_PropertyTotal");
		estimatedValue = homePage.getTextLoanSearch("DataCell FieldTypeCurrency datacell_EstimatedPortfolioValue");
		reqLoanAmount = homePage.getTextLoanSearch("DataCell FieldTypeCurrency datacell_LoanAmount");
		requiredDocs = homePage.getTextLoanSearch("DataCell FieldTypeInteger datacell_RequiredDocs");
		noLoaded = homePage.getTextLoanSearch("DataCell FieldTypeInteger datacell_LoadedDocsCount");
		noReviewed = homePage.getTextLoanSearch("DataCell FieldTypeInteger datacell_ReviewedDocsCount");
		noRevised = homePage.getTextLoanSearch("DataCell FieldTypeHTML datacell_Revised");
		dateCreated = homePage.getTextLoanSearch("DataCell FieldTypeDate datacell_CreateDate");

		log.info("VP: Loan name without Applicant display in Loan search table");
		verifyTrue(homePage.isAllValueDisplayOnSearchLoansTableCAF(loanNameWithoutApplicant, "", statusChange, termSheetIssued, termSheetSigned, properties, estimatedValue, reqLoanAmount,
				requiredDocs, noLoaded, noReviewed, noRevised, dateCreated));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private int day, month, year;
	private String usernameLender, passwordLender;
	private String loanName, accountNumber, applicantName, propertiesFileName, loanStatus, inputDay, loanNameWithoutApplicant, loanStatusOld;
	private String statusChange, termSheetIssued, termSheetSigned, properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded, noReviewed, noRevised, dateCreated;
}