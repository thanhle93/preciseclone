package sanityTest_WebApp;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_042_B2R_ValidateDatatypesPropertyDetailsPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan"+accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower New";
		invalidCurrency = "$40,0.00*%#$fail";
		invalidDate = "4/21/2015*%#$fail";
		invalidNumber = "123.12abc@";
		
		validCurrency = "$2,000,000.00";
		validDate = "12/31/2000";
		validZipCode = "12345";
		validText = "Common value value !@#";
		validNumber = "123.12";
		validWholeNumber = "123";
		propertiesFileName = "B2R Required.xlsx";
		propertyAddress = "37412 OAKHILL ST";
	}

	@Test(groups = { "regression" }, description = "Validate Datatypes 01 - No input the data")
	public void ValidataDatatypes_01_NoInputTheData() {
		
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Precondition 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);

		log.info("Precondition 04. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Precondition 05. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("Precondition 06. Open Properties tab");
		propertiesPage = loansPage.openPropertyDetail(propertyAddress);
		
		log.info("ValidataDatatypes_01 - Step 01. Input nulll Address");
		propertiesPage.inputAddress("");
		
		log.info("ValidataDatatypes_01 - Step 02. Click save button");
		propertiesPage.clickSaveButtonValidDataType();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(propertiesPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Address is required' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_AddressLine1", "Address is required"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 02 - Input the invalid/ incorrect at Currency fields")
	public void ValidataDatatypes_02_InputInvalidOrIncorrectAtCurrencyFields() {
		
		log.info("ValidataDatatypes_02 - Step 01. open Property Details");
		loansPage= propertiesPage.clickOnPropertyListButton();
		propertiesPage = loansPage.openPropertyDetail(propertyAddress);
		
		log.info("ValidataDatatypes_02 - Step 02. Input CapEx Reserves ");
		propertiesPage.inputCapExReserves(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 03. Input Vacancy Cost");
		propertiesPage.inputVacancyCost(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 04. Input Insurance ");
		propertiesPage.inputInsurance(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 05. Input Repair Cost ");
		propertiesPage.inputRepairCost(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 06. Input Taxes ");
		propertiesPage.inputTaxes(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 07. Input Management Fee");
		propertiesPage.inputManagementFee (invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 08. Input HOA Fee ");
		propertiesPage.inputHOAFee(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 09. Input Verified Rent");
		propertiesPage.inputVerifiedRent(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 10. Input Verified Tax ");
		propertiesPage.inputVerifiedTax(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 11. Input Verified Acquisition Price  ");
		propertiesPage.inputVerifiedAcquisitionPrice(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 12. Input Verified BPO");
		propertiesPage.inputVerifiedBPO(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 13. Input Rent ");
		propertiesPage.inputRent(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 14. Input Owner Paid Utilities ");
		propertiesPage.inputOwnerPaidUtilities(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 15. Input Security Deposit ");
		propertiesPage.inputSecurityDeposit(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 16. Input Borrower Opinion of Curr. Value");
		propertiesPage.inputBorrowerOpinionOfCurrValue(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 17. Input Acquisition Price");
		propertiesPage.inputAcquisitionPrice(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 18. Click save button");
		propertiesPage.clickSaveButtonValidDataType();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(propertiesPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_CapExReserves", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_VacancyFactorUSD", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_Insurance", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_Repair", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_AnnualTaxesAmount", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_MngFee", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_HOA", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_VerifiedRent", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_VerifiedTax", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_VerifiedBPO", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_Utilities", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_Rent", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_SecurityDepositAmount", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_EstValue", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_AcqPrice", "Currency value must be a number"));
	
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 03 - Input the invalid/ incorrect at Date fields")
	public void ValidataDatatypes_03_InputInvalidOrIncorrectAtDateFields() {
		
		log.info("ValidataDatatypes_02 - Step 01. open Property Details");
		loansPage= propertiesPage.clickOnPropertyListButton();
		propertiesPage = loansPage.openPropertyDetail(propertyAddress);
		
		log.info("ValidataDatatypes_02 - Step 02. Input Acquisition Date ");
		propertiesPage.inputAcquisitionDate(invalidDate);
		
		log.info("ValidataDatatypes_02 - Step 03. Input Rehab Completion Date");
		propertiesPage.inputRehabCompletionDate(invalidDate);
		
		log.info("ValidataDatatypes_02 - Step 04. Input Lease End Date ");
		propertiesPage.inputLeaseEndDate(invalidDate);
		
		log.info("ValidataDatatypes_02 - Step 05. Input Lease Start Date ");
		propertiesPage.inputLeaseStartDate(invalidDate);
		
		log.info("ValidataDatatypes_02 - Step 06. Click save button");
		propertiesPage.clickSaveButtonValidDataType();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(propertiesPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_AcqDate", "Value must be a date"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_RenovationDate", "Value must be a date"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_LeaseExpire", "Value must be a date"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_LeaseStartDate", "Value must be a date"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 04 - Input the invalid/ incorrect at Number fields")
	public void ValidataDatatypes_04_InputInvalidOrIncorrectAtNumberFields() {
		
		log.info("ValidataDatatypes_02 - Step 01. open Property Details");
		loansPage= propertiesPage.clickOnPropertyListButton();
		propertiesPage = loansPage.openPropertyDetail(propertyAddress);
		
		log.info("ValidataDatatypes_02 - Step 02. Input BD");
		propertiesPage.inputBD(invalidNumber);
		
		log.info("ValidataDatatypes_02 - Step 03. Input BA ");
		propertiesPage.inputBA(invalidNumber);
		
		log.info("ValidataDatatypes_02 - Step 04. Input SF ");
		propertiesPage.inputSF(invalidNumber);
		
		log.info("ValidataDatatypes_02 - Step 05. Input Year Built");
		propertiesPage.inputYearBuilt(invalidNumber);
		
		log.info("ValidataDatatypes_02 - Step 06. Input Vacancy Factor");
		propertiesPage.inputVacancyFactor(invalidNumber);
		
		log.info("ValidataDatatypes_02 - Step 07. Input Zip ");
		propertiesPage.inputZip(invalidNumber);
		
		log.info("ValidataDatatypes_02 - Step 33. Input unit");
		propertiesPage.inputUnit(invalidNumber);
		
		log.info("ValidataDatatypes_02 - Step 08. Click save button");
		propertiesPage.clickSaveButtonValidDataType();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(propertiesPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_Bedroom", "Value must be a number"));
		
		log.info("VP: 'Value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_Bath", "Value must be a number"));
		
		log.info("VP: 'Value must be a whole number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_SQFT", "Value must be a whole number"));
		
		log.info("VP: 'Value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_YearBuilt", "Value must be a whole number"));
		
		log.info("VP: 'Value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_VacancyFactor", "Value must be a number"));
		
		log.info("VP: 'Enter a valid ZIP or ZIP+4 code' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_Zip", "Enter a valid ZIP or ZIP+4 code"));
		
		log.info("VP: 'Value must be a number' message is displayed");
		verifyTrue(propertiesPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_Units", "Value must be a number"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 05 - Input the valid into all fields")
	public void ValidataDatatypes_05_InputValidIntoAllFields() {
		log.info("ValidataDatatypes_02 - Step 01. open Property Details");
		loansPage= propertiesPage.clickOnPropertyListButton();
		propertiesPage = loansPage.openPropertyDetail(propertyAddress);
		
		log.info("ValidataDatatypes_02 - Step 02. Input CapEx Reserves ");
		propertiesPage.inputCapExReserves(validCurrency);
		
		log.info("ValidataDatatypes_02 - Step 03. Input Vacancy Cost");
		propertiesPage.inputVacancyCost(validCurrency);
		
		log.info("ValidataDatatypes_02 - Step 04. Input Insurance ");
		propertiesPage.inputInsurance(validCurrency);
		
		log.info("ValidataDatatypes_02 - Step 05. Input Repair Cost ");
		propertiesPage.inputRepairCost(validCurrency);
		
		log.info("ValidataDatatypes_02 - Step 06. Input Taxes ");
		propertiesPage.inputTaxes(validCurrency);
		
		log.info("ValidataDatatypes_02 - Step 07. Input Management Fee");
		propertiesPage.inputManagementFee (validCurrency);
		
		log.info("ValidataDatatypes_02 - Step 08. Input HOA Fee ");
		propertiesPage.inputHOAFee(validCurrency);
		
		log.info("ValidataDatatypes_02 - Step 09. Input Verified Rent");
		propertiesPage.inputVerifiedRent(validCurrency);
		
		log.info("ValidataDatatypes_02 - Step 10. Input Verified Tax ");
		propertiesPage.inputVerifiedTax(validCurrency);
		
		log.info("ValidataDatatypes_02 - Step 11. Input Verified Acquisition Price  ");
		propertiesPage.inputVerifiedAcquisitionPrice(validCurrency);
		
		log.info("ValidataDatatypes_02 - Step 12. Input Verified BPO");
		propertiesPage.inputVerifiedBPO(validCurrency);
		
		log.info("ValidataDatatypes_02 - Step 13. Input Rent ");
		propertiesPage.inputRent(validCurrency);
		
		log.info("ValidataDatatypes_02 - Step 14. Input Owner Paid Utilities ");
		propertiesPage.inputOwnerPaidUtilities(validCurrency);
		
		log.info("ValidataDatatypes_02 - Step 15. Input Security Deposit ");
		propertiesPage.inputSecurityDeposit(validCurrency);
		
		log.info("ValidataDatatypes_02 - Step 16. Input Borrower Opinion of Curr. Value");
		propertiesPage.inputBorrowerOpinionOfCurrValue(validCurrency);
		
		log.info("ValidataDatatypes_02 - Step 17. Input Acquisition Price");
		propertiesPage.inputAcquisitionPrice(validCurrency);
		
		log.info("ValidataDatatypes_02 - Step 18. Input Acquisition Date ");
		propertiesPage.inputAcquisitionDate(validDate);
		
		log.info("ValidataDatatypes_02 - Step 19. Input Rehab Completion Date");
		propertiesPage.inputRehabCompletionDate(validDate);
		
		log.info("ValidataDatatypes_02 - Step 20. Input Lease End Date ");
		propertiesPage.inputLeaseEndDate(validDate);
		
		log.info("ValidataDatatypes_02 - Step 21. Input Lease Start Date ");
		propertiesPage.inputLeaseStartDate(validDate);
		
		log.info("ValidataDatatypes_02 - Step 22. Input BD");
		propertiesPage.inputBD(validNumber);
		
		log.info("ValidataDatatypes_02 - Step 23. Input BA");
		propertiesPage.inputBA(validNumber);
		
		log.info("ValidataDatatypes_02 - Step 24. Input SF");
		propertiesPage.inputSF(validWholeNumber);
		
		log.info("ValidataDatatypes_02 - Step 25. Input Year Built");
		propertiesPage.inputYearBuilt(validWholeNumber);
		
		log.info("ValidataDatatypes_02 - Step 26. Input Vacancy Factor");
		propertiesPage.inputVacancyFactor(validNumber);
		
		log.info("ValidataDatatypes_02 - Step 27. Input Zip");
		propertiesPage.inputZip(validZipCode);
		
		log.info("ValidataDatatypes_02 - Step 28. Input Property Name");
		propertiesPage.inputPropertyName(validText);
		
		log.info("ValidataDatatypes_02 - Step 29. Input County");
		propertiesPage.county(validText);
		
		log.info("ValidataDatatypes_02 - Step 30. Input Comment");
		propertiesPage.inputComment(validText);
		
		log.info("ValidataDatatypes_02 - Step 31. Input Flood zone name");
		propertiesPage.inputFloodZoneName(validText);
		
		log.info("ValidataDatatypes_02 - Step 32. Input City name");
		propertiesPage.inputCity(validText);
		
		log.info("ValidataDatatypes_02 - Step 33. Input unit");
		propertiesPage.inputUnit(validWholeNumber);
		
		log.info("ValidataDatatypes_02 - Step 34. Select 'Section 8' Combobox");
		propertiesPage.selectSection8("No");
		
		log.info("ValidataDatatypes_02 - Step 35. Select 'Property Type' Combobox");
		propertiesPage.selectPropertyTypeComboBox("SFR");
		
		log.info("ValidataDatatypes_02 - Step 36. Select 'Property State' Combobox");
		propertiesPage.selectPropertyStateComboBox("AL - Alabama");
		
		log.info("ValidataDatatypes_02 - Step 37. Select 'Partial Flood' Combobox");
		propertiesPage.selectPartialFloodComboBox("Yes");
		
		log.info("ValidataDatatypes_02 - Step 38. Select 'Current Leased' Combobox");
		propertiesPage.selectCurrentLeasedComboBox("Occupied");
		
		log.info("ValidataDatatypes_02 - Step 39. Select 'Flood Zone' Combobox");
		propertiesPage.selectFloodZoneComboBox("Yes");
		
		log.info("ValidataDatatypes_02 - Step 40. Select 'Air Conditioning' Combobox");
		propertiesPage.selectAirConditioningComboBox("Yes");
		
		log.info("ValidataDatatypes_05 - Step 41: Click 'Save' button");
		propertiesPage.clickSaveButton();
		
		//Check data saved
		
		log.info("VP: 'Record saved' message is displayed saved correctly");
		verifyTrue(loansPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: Verify that CapEx Reserves saved correctly ");
		verifyTrue(propertiesPage.isCapExReservesDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that Vacancy Cost saved correctly");
		verifyTrue(propertiesPage.isVacancyCostDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that Insurance saved correctly ");
		verifyTrue(propertiesPage.isInsuranceDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that Repair Cost saved correctly ");
		verifyTrue(propertiesPage.isRepairCostDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that Taxes saved correctly ");
		verifyTrue(propertiesPage.isTaxesDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that Management Fee saved correctly");
		verifyTrue(propertiesPage.isManagementFeeDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that HOA Fee saved correctly ");
		verifyTrue(propertiesPage.isHOAFeeDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that Verified Rent saved correctly");
		verifyTrue(propertiesPage.isVerifiedRentDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that Verified Tax saved correctly ");
		verifyTrue(propertiesPage.isVerifiedTaxDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that Verified Acquisition Price saved correctly  ");
		verifyTrue(propertiesPage.isVerifiedAcquisitionPriceDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that Verified BPO saved correctly");
		verifyTrue(propertiesPage.isVerifiedBPODisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that Rent saved correctly ");
		verifyTrue(propertiesPage.isRentDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that Owner Paid Utilities saved correctly ");
		verifyTrue(propertiesPage.isOwnerPaidUtilitiesDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that Security Deposit saved correctly ");
		verifyTrue(propertiesPage.isSecurityDepositDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that Borrower Opinion of Curr. Value saved correctly");
		verifyTrue(propertiesPage.isBorrowerOpinionOfCurrValueDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that Acquisition Price saved correctly");
		verifyTrue(propertiesPage.isAcquisitionPriceDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that Verified Rent saved correctly");
		verifyTrue(propertiesPage.isVerifiedRentDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that Verified Tax saved correctly ");
		verifyTrue(propertiesPage.isVerifiedTaxDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that Acquisition Date saved correctly ");
		verifyTrue(propertiesPage.isAcquisitionDateDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that Rehab Completion Date saved correctly");
		verifyTrue(propertiesPage.isRehabCompletionDateDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that Lease End Date saved correctly ");
		verifyTrue(propertiesPage.isLeaseEndDateDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that Lease Start Date saved correctly ");
		verifyTrue(propertiesPage.isLeaseStartDateDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that Input BD saved correctly");
		verifyTrue(propertiesPage.isBDDisplayedCorrectly(validNumber));
		
		log.info("VP: Verify that Input BA saved correctly");
		verifyTrue(propertiesPage.isBADisplayedCorrectly(validNumber));
		
		log.info("VP: Verify that Input SF saved correctly");
		verifyTrue(propertiesPage.isSFDisplayedCorrectly(validWholeNumber));
		
		log.info("VP: Verify that Year Built saved correctly");
		verifyTrue(propertiesPage.isYearBuiltDisplayedCorrectly(validWholeNumber));
		
		log.info("VP: Verify that Vacancy Factor saved correctly");
		verifyTrue(propertiesPage.isVacancyFactorDisplayedCorrectly(validNumber));
		
		log.info("VP: Verify that Zip saved correctly");
		verifyTrue(propertiesPage.isZipDisplayedCorrectly(validZipCode));
		
		log.info("VP: Verify that Property Name saved correctly");
		verifyTrue(propertiesPage.isPropertyNameDisplayedCorrectly(validText));
		
		log.info("VP: Verify that County saved correctly");
		verifyTrue(propertiesPage.isCountyDisplayedCorrectly(validText));
		
		log.info("VP: Verify that Comment saved correctly");
		verifyTrue(propertiesPage.isCommentDisplayedCorrectly(validText));
		
		log.info("VP: Verify that Flood zone name saved correctly");
		verifyTrue(propertiesPage.isFloodZoneNameDisplayedCorrectly(validText));
		
		log.info("VP: Verify that City saved correctly  saved correctly");
		verifyTrue(propertiesPage.isCityDisplayedCorrectly(validText));
		
		log.info("VP: Verify that #Unit saved correctly saved correctly");
		verifyTrue(propertiesPage.isUnitDisplayedCorrectly(validWholeNumber));
		
		log.info("VP: Verify that 'Section 8' Combobox saved correctly");
		verifyTrue(propertiesPage.isSection8ComboBoxDisplayedCorrectly("No"));
		
		log.info("VP: Verify that 'Property Type' Combobox saved correctly");
		verifyTrue(propertiesPage.isPropertyTypeComboBoxDisplayedCorrectly("SFR"));
		
		log.info("VP: Verify that 'Property State' Combobox saved correctly");
		verifyTrue(propertiesPage.isPropertyStateComboBoxDisplayedCorrectly("AL - Alabama"));
		
		log.info("VP: Verify that 'Partial Flood' Combobox saved correctly");
		verifyTrue(propertiesPage.isPartialFloodComboBoxDisplayedCorrectly("Yes"));
		
		log.info("VP: Verify that 'Current Leased' Combobox saved correctly");
		verifyTrue(propertiesPage.isCurrentLeasedComboBoxDisplayedCorrectly("Occupied"));
		
		log.info("VP: Verify that 'Flood Zone' Combobox saved correctly");
		verifyTrue(propertiesPage.isFloodZoneComboBoxDisplayedCorrectly("Yes"));
		
		log.info("VP: Verify that 'Air Conditioning' Combobox saved correctly");
		verifyTrue(propertiesPage.isAirConditioningComboBoxDisplayedCorrectly("Yes"));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private String usernameLender, passwordLender;
	private String loanName, loanStatus, accountNumber, applicantName;
	private String invalidCurrency, invalidDate, invalidNumber;
	private String propertyAddress, propertiesFileName;
	private String validCurrency, validDate, validText, validZipCode, validNumber, validWholeNumber;
	}