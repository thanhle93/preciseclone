package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_000_STO_TestSikuli extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileNameSingleGuarantor = "SingleGuarantorMapping.xml";
		xmlFileNameMultiGuarantorNotMatchNumber = "MultiGuarantorNotMatchNumber.xml";
		xmlFileNameMultiGuarantorMatchNumber = "MultiGuarantorMatchNumber.xml";
		xmlFileNameNoGuarantor = "NoGuarantor.xml";
		xmlFileNameCPIMultiplierTrue = "CPIMultiplierTrue.xml";
		xmlFileNameCPIMultiplierFalse = "CPIMultiplierFalse.xml";
		xmlFileNameLeasePriorFalse = "LeasePriorFalse.xml";
		xmlFileNameLeasePriorNo = "LeasePriorNo.xml";
		xmlFileNameLeasePriorTrue = "LeasePriorTrue.xml";
		xmlFileNameLeasePriorYes = "LeasePriorYes.xml";
	}

	@Test(groups = { "regression" }, description = "Mapping From Xml File 01 - Import Single Guarantor")
	public void MappingFromXmlFile_01_ImportSingleGuarantor() {
		
		log.info("MappingFromXmlFile_01 - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("MappingFromXmlFile_01 - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("MappingFromXmlFile_01 - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("MappingFromXmlFile_01 - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXMLTest(xmlFileNameSingleGuarantor);

		log.info("VP: 'Number of guarantors found in the file doesn�t match' message isn't displayed");
		verifyFalse(transactionPage.isWarningDisplayedCorrectly(driver, "Number of guarantors found in the file doesn�t match"));
		
		log.info("VP: Guarantors are imported correctly");
		verifyTrue(transactionPage.isGuarantorMappingFromXmlFileCorrectly(xmlFileNameSingleGuarantor));
	}
	

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileNameSingleGuarantor, xmlFileNameMultiGuarantorNotMatchNumber, xmlFileNameMultiGuarantorMatchNumber, xmlFileNameNoGuarantor, xmlFileNameCPIMultiplierTrue, xmlFileNameCPIMultiplierFalse;
	private String xmlFileNameLeasePriorFalse, xmlFileNameLeasePriorNo, xmlFileNameLeasePriorTrue, xmlFileNameLeasePriorYes;
	private String originalCommencementDate;
}