package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFLoansPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_014_FNF_MandatoryFieldsForMovingLoanStatus extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		loanName = "Edit Loan Auto " + getUniqueNumber();
		underwriter = "CAF FNF Lender";
		locClosingDate = "04/12/2014";
		validDate = "02/12/2014";
		validNumber ="10";
		validText = ""+ getUniqueNumber();
		acquisitionType = "Acquisition";
		interestRateType = "Fixed";
		renovationFunding = "Yes";
		underwritingException = "No";
		guarantorsName ="Guarantor"+ getUniqueNumber();
		lender = "CAF Bridge Lending";
	}

	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Check Mandatory fields for moving loan to 'Committee Review' Status")
	public void MandatoryFields_01_Lender_CheckMandatoryFieldsForMovingLoanToCommitteeReviewStatus() {
		log.info("MandatoryFields_01 - Step 01. Login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);

		log.info("MandatoryFields_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("MandatoryFields_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("MandatoryFields_01 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("MandatoryFields_01 - Step 05: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("MandatoryFields_01 - Step 06: Input loan name");
		loanPage.inputRequiredData(loanName, locClosingDate);
		
		log.info("MandatoryFields_01 - Step 07: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("MandatoryFields_01 - Step 08: Click on 'Send to Initial Review' button");
		loanPage.clickOnSendToInitialReviewButton();
		
		log.info("VP: Verify that 'Initial Review' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Initial Review"));
		
		log.info("MandatoryFields_01 - Step 09: Click on 'Send to Term-Sheet Sent' button");
		loanPage.clickOnSendToTermSheetSentButton();
		
		log.info("VP: Verify that 'Term-Sheet Sent' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Term-Sheet Sent"));
		
		log.info("MandatoryFields_01 - Step 10: Click on 'Send to Underwriting' button");
		loanPage.clickOnSendToUnderwritingButton();
		
		log.info("VP: Verify that 'Underwriting' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Underwriting"));
		
		log.info("MandatoryFields_01 - Step 11: Click on 'Send to Committee Review' button");
		loanPage.clickOnSendToCommitteeReviewButton();
		
		log.info("VP: Verify that 'Loan should have at least one 'Guarantor' error message is displayed");
		verifyTrue(loanPage.isErrorMessageDisplayed("Loan should have at least one “Guarantor”"));
		
		log.info("MandatoryFields_01 - Step 12: Create new guarantor for Loan");
		loanPage.createNewGuarantor(guarantorsName);
		
		log.info("MandatoryFields_01 - Step 13: Click on 'Send to Committee Review' button");
		loanPage.inputDataRequiredForCommitteeReview("", "", "", "", "", "", "", "", "");
		loanPage.clickOnSendToCommitteeReviewButton();
		
		log.info("VP: Verify that 'Please correct errors marked below' error message is displayed");
		verifyTrue(loanPage.isErrorMessageDisplayed("Please correct errors marked below"));
		
		log.info("VP: 'Underwriter is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_Underwriter", "Underwriter is required"));
		
		log.info("VP: 'Acquisition is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_Acquisition", "Acquisition is required"));
		
		log.info("VP: 'Advance Period (Days) is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_AdvancePeriod", "Advance Period (Days) is required"));
		
		log.info("VP: 'Renovation Funding is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_RenovationFunding", "Renovation Funding is required"));
		
		log.info("VP: 'Exit Fee (%) is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_ExitFee", "Exit Fee (%) is required"));
		
		log.info("VP: 'Loan Record Type is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_OpportunityRecordType", "Loan Record Type is required"));
		
		log.info("VP: 'CAF Draw Fee (%) is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_CAFDrawFee", "CAF Draw Fee (%) is required"));
		
		log.info("VP: 'Expense Deposit Paid Date is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_DepositDate", "Expense Deposit Paid Date is required"));
		
		log.info("VP: 'Date of Individual Search is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_DateIndividualSearch", "Date of Individual Search is required"));
		
		log.info("VP: 'DSCR is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_DSCR", "DSCR is required"));
		
		log.info("VP: 'Underwriting Exception (Y/N) is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_UnderwritingException", "Underwriting Exception (Y/N) is required"));
		
		log.info("VP: 'CAF Loan ID is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_CAFLoanNumber", "CAF Loan ID is required"));
		
		log.info("VP: 'Lender is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_Lender", "Lender is required"));
		
		log.info("VP: 'Interest Rate Type is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_InterestRateType", "Interest Rate Type is required"));
		
		log.info("VP: 'Revolving (Y/N) is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_Revolving", "Revolving (Y/N) is required"));
		
		log.info("VP: 'Origination Fee is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_FeesAmount", "Origination Fee is required"));
		
		log.info("VP: 'Underwriting File Complete Date is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_UnderwritingFileCompleteDate", "Underwriting File Complete Date is required"));
	
		log.info("MandatoryFields_01 - Step 01: Click on 'Send to Committee Review' button");
		loanPage.inputDataRequiredForCommitteeReview(validDate, validText, validNumber, acquisitionType, interestRateType, renovationFunding, underwritingException, underwriter, lender);
		loanPage.clickOnSendToCommitteeReviewButton();
		
		log.info("VP: Verify that 'Committee Review' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Committee Review"));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Check Mandatory fields for moving loan to 'Documentation' Status")
	public void MandatoryFields_02_Lender_CheckMandatoryFieldsForMovingLoanToDocumentationStatus() {

		log.info("MandatoryFields_02 - Step 01: Click on 'Send to Documentation' button");
		loanPage.inputDataRequiredForDocumentation("", "", "");
		loanPage.clickOnSendToDocumentationButton();
		
		log.info("VP: Verify that 'Please correct errors marked below' error message is displayed");
		verifyTrue(loanPage.isErrorMessageDisplayed("Please correct errors marked below"));
		
		log.info("VP: 'Approval Date is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_ApprovalDate", "Approval Date is required"));
		
		log.info("VP: 'Active Counties/States is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_ActiveCounties", "Active Counties/States is required"));
		
		log.info("VP: 'New Loan Crossed to Prior is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_NewLoanCrossedtoPrior", "New Loan Crossed to Prior is required"));
		
		log.info("VP: 'Date of Cert of Good Standing is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_DateCertGoodStanding", "Date of Cert of Good Standing is required"));
		
		log.info("VP: 'Date of Entity Search is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_DateEntitySearch", "Date of Entity Search is required"));
		
		log.info("VP: 'Entity Search Fee is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_EntitySearchFee", "Entity Search Fee is required"));
		
		log.info("MandatoryFields_02 - Step 02: Click on 'Send to Documentation' button");
		loanPage.inputDataRequiredForDocumentation(validDate, validText, validNumber);
		loanPage.clickOnSendToDocumentationButton();
		
		log.info("VP: Verify that 'Committee Review' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Documentation"));
	}	
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Check Mandatory fields for moving loan to 'Investment Period' Status")
	public void MandatoryFields_03_Lender_CheckMandatoryFieldsForMovingLoanToInvestmentPeriodStatus() {

		log.info("MandatoryFields_03 - Step 01: Click on 'Send to Investment Period' button");
		loanPage.inputDataRequiredForInvestmentPeriod("", "", "");
		loanPage.clickOnSendToInvestmentPeriodButton();
		
		log.info("VP: Verify that 'Please correct errors marked below' error message is displayed");
		verifyTrue(loanPage.isErrorMessageDisplayed("Please correct errors marked below"));
		
		log.info("VP: 'LOC Commitment is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_ExistingLineAmount", "LOC Commitment is required"));
		
		log.info("VP: 'Date Closing Docs Received is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_DateClosingDocsReceived", "Date Closing Docs Received is required"));
		
		log.info("VP: 'Legal Fee is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_LegalFee", "Legal Fee is required"));
		
		log.info("VP: 'LOC Closing Date is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_CloseDate", "LOC Closing Date is required"));
		
		log.info("VP: 'Current Close Date is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_CurrentCloseDate", "Current Close Date is required"));
		
		log.info("VP: 'Expiration Date is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_ExpirationDate", "Expiration Date is required"));
		
		log.info("VP: 'Origination Fee Paid Date is required' message is displayed");
		verifyTrue(loanPage.isDynamicAlertMessageTitleDisplay("field_OpportunityDetails_R1_FeesDate", "Origination Fee Paid Date is required"));
		
		log.info("MandatoryFields_03 - Step 02: Click on 'Send to Investment Period' button");
		loanPage.inputDataRequiredForInvestmentPeriod(validDate, validText, validNumber);
		loanPage.clickOnSendToInvestmentPeriodButton();
		
		log.info("VP: Verify that 'Matured' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Matured"));
	}	


	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private FNFLoansPage loanPage;
	private String FNFLenderUsername, FNFLenderPassword;
	private String accountNumber, loanName, underwriter, locClosingDate;
	private String companyName;
	private String validDate, validNumber, validText, acquisitionType, interestRateType, renovationFunding, underwritingException;
	private String guarantorsName, lender;
	
	}