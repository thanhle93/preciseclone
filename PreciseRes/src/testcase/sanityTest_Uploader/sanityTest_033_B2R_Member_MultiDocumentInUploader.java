package sanityTest_Uploader;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_033_B2R_Member_MultiDocumentInUploader extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameMember = "auto-B2R-member2014";
		passwordMember = "change";
		loanName = "UploaderUdiTesting"+getUniqueNumber();
		applicantName = "Consolidation Demo Borrower New";
		loanStatus = Constant.LOAN_STATUS_B2R;
		propertiesFileName = "B2RDataTape_multi-unit.xlsx";
		bpoDocName = "10105 Spring ST- P2.pdf";
		evidenceInsuranceDocName = "10105 Spring ST- P3.pdf";
		bpoDocType = "P2 - Lease Agreement";
		evidenceInsuranceDocType = "P3 - Purchase Contract/HUD-1 Settlement Stmt";
		newDocumentType = "P5 - Zoning Compliance Letter";
		propertyGroup = "Oakhill";
		propertyName = "10105 Spring ST";
		propertyName2 = "37225 OAKHILL ST - suite 19";
		bpoDocNameMulti = "37225 OAKHILL ST - suite 19- P10.pdf";
		bpoDocTypeMulti = "P10 - Lead-Based Paint Disclosure";
		bpoDocNameParent = "Oakhill- P10.pdf";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}
	
	@Test(groups = { "regression" },description = "UploaderMember12 - AutoCategorizing document with keyword")
	public void Uploader_13_AutoCategorizingDocumentWithKeyword()
	{
		log.info("Precondition 01. Login with member");
		homePage = loginPage.loginAsLender(usernameMember, passwordMember, false);
		
		log.info("Precondition 02. Create new loans and get loans' id");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);
		
		log.info("Precondition 03. Upload Property data");
		loansPage.openPropertiesTab();
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("Precondition 04. Click on logged user name on the top right corner");
//		loansPage.clickOnLoggedUserName(driver);
//		uploaderPageUrl = loansPage.getUploaderPageUrl(driver);
//		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
		
		log.info("Precondition 05. Go to Uploader");
		uploaderLoginPage = homePage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("Precondition 06. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameMember, passwordMember);
		
		log.info("Uploader_13 - Step 01. Select loans item");
		uploaderPage.clickChangeGroupedView();
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("Uploader_13 - Step 02. Select folder 'Uncategorized Property Documents'");
		uploaderPage.selectSubFolderLevel2("Uncategorized Property Documents");
		
		log.info("Uploader_13 - Step 03. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherDocuments();
		
		log.info("Uploader_13 - Step 04. Select file to upload");
		uploaderPage.uploadFileOnUploader(bpoDocName);
//		uploaderPage.waitForAddOtherDocument(bpoDocName);
		
		log.info("Uploader_13 - Step 05. Click 'OK' Button");
//		uploaderPage.clickOkButtonOnKeywords();
		uploaderPage.waitForUploadCompleteOnUploader(bpoDocName);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(bpoDocName));
		
		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(bpoDocName, "Unknown"));
		
		log.info("Uploader_13 - Step 06. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherDocuments();
		
		log.info("Uploader_13 - Step 07. Select file to upload");
		uploaderPage.uploadFileOnUploader(evidenceInsuranceDocName);
//		uploaderPage.waitForAddOtherDocument(evidenceInsuranceDocName);
		
		log.info("Uploader_13 - Step 08. Click 'OK' Button");
//		uploaderPage.clickOkButtonOnKeywords();
		uploaderPage.waitForUploadCompleteOnUploader(evidenceInsuranceDocName);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(evidenceInsuranceDocName));
		
		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(evidenceInsuranceDocName, "Unknown"));
		
		log.info("Uploader_13 - Step 09. Click 'Auto categorize documents' button");
		uploaderPage.clickAutocategorizeDocumentsButton();
		
		log.info("Uploader_13 - Step 10. Click 'OK Autocategorization result' button");
		uploaderPage.clickOkAutocategorizationResultButton();
		
		log.info("VP: Deeds document is categorized");
		verifyFalse(uploaderPage.isDocumentDisplay(bpoDocName));
				
		log.info("VP: Evidence of Insurance document is categorized");
		verifyFalse(uploaderPage.isDocumentDisplay(evidenceInsuranceDocName));
		
		log.info("Uploader_13 - Step 11. Click Property folder");
		uploaderPage.clickPropertiesFolder();
		
		log.info("Uploader_13 - Step 12. Select '10105 Spring ST' property");
		uploaderPage.selectPropertyFolder(propertyName);
		
		log.info("VP: These document is added to property");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(bpoDocName, bpoDocType));
		verifyTrue(uploaderPage.isDocumentTypeDisplay(evidenceInsuranceDocName, evidenceInsuranceDocType));
	}

	@Test(groups = { "regression" },description = "UploaderLender14 - Attach a document through autocat to a parent")
	public void Uploader_14_AutoCategorizingDocumentToParent()
	{
		log.info("Uploader_14 - Step 01. Select folder 'Uncategorized Property Documents'");
		uploaderPage.selectSubFolderLevel2("Uncategorized Property Documents");
		
		log.info("Uploader_14 - Step 02. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherDocuments();
		
		log.info("Uploader_14 - Step 03. Select file to upload");
		uploaderPage.uploadFileOnUploader(bpoDocNameParent);
		
		log.info("Uploader_14 - Step 04. Click 'OK' Button");
		uploaderPage.waitForUploadCompleteOnUploader(bpoDocNameParent);
		
		if(!driver.toString().toLowerCase().contains("internetexplorer")){
			log.info("VP: The document is uploaded successfully");
			verifyTrue(uploaderPage.isDocumentDisplay(bpoDocNameParent));
		}

		log.info("VP: The document type display correctly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(bpoDocNameParent, "Unknown"));
		
		log.info("Uploader_14 - Step 05. Click 'Auto categorize documents' button");
		uploaderPage.clickAutocategorizeDocumentsButton();
		
		log.info("Uploader_14 - Step 06. Click 'OK Autocategorization result' button");
		uploaderPage.clickOkAutocategorizationResultButton();
		
		log.info("VP: Oakhill document is categorized");
		verifyFalse(uploaderPage.isDocumentDisplay(bpoDocNameParent));
		
		log.info("Uploader_14 - Step 07. Select 'Oakhill' property");
		uploaderPage.selectPropertyFolder(propertyGroup);
		
		log.info("VP: These document is added to property");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(bpoDocNameParent, bpoDocTypeMulti));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender15 - Attach a document through autocat to a unit in a multi-unit")
	public void Uploader_15_AutoCategorizingDocumentToMultiUnit()
	{
		log.info("Uploader_15 - Step 01. Select folder 'Uncategorized Property Documents'");
		uploaderPage.selectSubFolderLevel2("Uncategorized Property Documents");
		
		log.info("Uploader_15 - Step 02. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherDocuments();
		
		log.info("Uploader_15 - Step 03. Select file to upload");
		uploaderPage.uploadFileOnUploader(bpoDocNameMulti);
		
		log.info("Uploader_15 - Step 04. Click 'OK' Button");
		uploaderPage.waitForUploadCompleteOnUploader(bpoDocNameMulti);
		
		if(!driver.toString().toLowerCase().contains("internetexplorer")){
			log.info("VP: The document is uploaded successfully");
			verifyTrue(uploaderPage.isDocumentDisplay(bpoDocNameMulti));
		}

		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(bpoDocNameMulti, "Unknown"));
		
		log.info("Uploader_15 - Step 05. Click 'Auto categorize documents' button");
		uploaderPage.clickAutocategorizeDocumentsButton();
		
		log.info("Uploader_15 - Step 06. Click 'OK Autocategorization result' button");
		uploaderPage.clickOkAutocategorizationResultButton();
		
		log.info("VP: Lease Agreement document is categorized");
		verifyFalse(uploaderPage.isDocumentDisplay(bpoDocNameMulti));
		
		log.info("Uploader_15 - Step 07. Expand property group");
		uploaderPage.expandPropertyGroup(propertyGroup);
		
		log.info("Uploader_15 - Step 08. Select '37225 OAKHILL ST - suite 19' property");
		uploaderPage.selectSubPropertyFolder(propertyName2);
		
		log.info("VP: These document is added to property");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(bpoDocNameMulti, bpoDocTypeMulti));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender15 - Multi change document type")
	public void Uploader_16_MultiChangeDocumentType()
	{
		log.info("Uploader_16 - Step 01. Select document");
		uploaderPage.selectPropertyFolder(propertyName);
		uploaderPage.selectCheckboxForDocumentName(bpoDocName);
		uploaderPage.selectCheckboxForDocumentName(evidenceInsuranceDocName);
		
		log.info("Uploader_16 - Step 02. Click on change document type link");
		uploaderPage.selectChangeDocumentTypeLink();
		
		log.info("Uploader_16 - Step 03. Select new document type");
		uploaderPage.selectDocumentTypeOnEditDocumentType(newDocumentType);
		
		log.info("Uploader_16 - Step 04. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.clickYesButton();
		
		log.info("VP: These document is changed type");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(bpoDocName, newDocumentType));
		verifyTrue(uploaderPage.isDocumentTypeDisplay(evidenceInsuranceDocName, newDocumentType));
		verifyFalse(uploaderPage.isDocumentTypeDisplay(bpoDocName, bpoDocType));
		verifyFalse(uploaderPage.isDocumentTypeDisplay(evidenceInsuranceDocName, evidenceInsuranceDocType));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender23 - Change document type but then in the dialogue click 'no'")
	public void Uploader_17_ChangeDocumentTypeButThenClickNo()
	{
		log.info("Uploader_17 - Step 01. Select document");
		uploaderPage.selectPropertyFolder(propertyName);
		uploaderPage.selectCheckboxForDocumentName(bpoDocName);
		
		log.info("Uploader_17 - Step 02. Click on change document type link");
		uploaderPage.selectChangeDocumentTypeLink();
		
		log.info("Uploader_17 - Step 03. Select new document type");
		uploaderPage.selectDocumentTypeOnEditDocumentType(bpoDocType);
		
		log.info("Uploader_17 - Step 04. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.clickNoButton();
		
		log.info("VP: The document type is not changed");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(bpoDocName, newDocumentType));
		verifyFalse(uploaderPage.isDocumentTypeDisplay(bpoDocName, bpoDocType));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender17 - Multi Delete Document")
	public void Uploader_18_MultiDeleteDocument()
	{
		log.info("Uploader_18 - Step 01. Select document");
		uploaderPage.selectCheckboxForDocumentName(bpoDocName);
		uploaderPage.selectCheckboxForDocumentName(evidenceInsuranceDocName);
		
		log.info("Uploader_18 - Step 02. Click on delete link");
		uploaderPage.clickDeleteLink();
		
		log.info("Uploader_18 - Step 03. Click Ok button");
		uploaderPage.clickYesButton();
		
		log.info("VP: These document is changed type");
		verifyFalse(uploaderPage.isDocumentTypeDisplay(bpoDocName, newDocumentType));
		verifyFalse(uploaderPage.isDocumentTypeDisplay(evidenceInsuranceDocName, newDocumentType));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private LoansPage loansPage;
	private String usernameMember, passwordMember, propertyGroup, bpoDocTypeMulti, bpoDocNameParent;
	private String loanName, uploaderPageUrl, propertiesFileName, newDocumentType;
	private String bpoDocName, evidenceInsuranceDocName, bpoDocType, propertyName, bpoDocNameMulti;
	private String evidenceInsuranceDocType, loanStatus, applicantName, propertyName2;
}