package page;

import org.openqa.selenium.WebDriver;
import PreciseRes.Interfaces;

public class UploaderLoginPage extends AbstractPage{
	public UploaderLoginPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}
	
	/**
	 * check Username Display Above Main Menu
	 */
	public UploaderPage loginUploader(String username, String password) {
		waitForControl(driver, Interfaces.UploaderLoginPage.USER_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.UploaderLoginPage.USER_NAME_TEXTBOX, username);
		sleep();
		type(driver, Interfaces.UploaderLoginPage.PASSWORD_TEXTBOX, password);
		sleep();
		doubleClick(driver, Interfaces.UploaderLoginPage.SIGN_IN_BUTTON);
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(60);
		}
		sleep(15);
		return PageFactory.getUploaderPage(driver, ipClient);
	}
	
	private WebDriver driver;
	private String ipClient; 
}