package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFDocumentPage;
import page.FNFLoansPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_030_FNF_GlobalDocumentsView extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		loanName = "Edit Loan Auto " + getUniqueNumber();
		validDate = "2/12/2014";
		documentFolder = "Entity Documents";
		documentFileName = "datatest1.pdf";
		text = "comments";
	}


	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Open documents file name")
	public void GlobalDocumentsView_01_Lender_OpenDocumentsName() {

		log.info("GlobalDocumentsView_01 - Step 01. Logout and login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("GlobalDocumentsView_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("GlobalDocumentsView_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("GlobalDocumentsView_01 - Step 05: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
	
		log.info("GlobalDocumentsView_01 - Step 06: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("GlobalDocumentsView_01 - Step 07: Input loan name");
		loanPage.inputRequiredData(loanName, validDate);
		
		log.info("GlobalDocumentsView_01 - Step 08: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("GlobalDocumentsView_01 - Step 09. Open document folder");
		documentsPage = loanPage.openDocumentsFolder(documentFolder);
		
		log.info("GlobalDocumentsView_01 - Step 10. Upload document file name 01");
		documentsPage.uploadDocumentFileForFolderByPlaceHolder(documentFileName);
		
		log.info("GlobalDocumentsView_01 - Step 11. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("GlobalDocumentsView_01 - Step 12. Input comment");
		documentsPage.inputComments(documentFileName, text);
		
		log.info("GlobalDocumentsView_01 - Step 13. Click Save button");
		documentsPage.clickSaveButton();
	
		log.info("GlobalDocumentsView_01 - Step 14: Open Documents Page and open Loan Documents tab");
		documentsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openLoanDocumentsTab();
		
		log.info("GlobalDocumentsView_01 - Step 15: Input 'Loan Name' and click on 'Search' button");
		documentsPage.clickOnSearchLoansButton(loanName);
		
		log.info("GlobalDocumentsView_01 - Step 16: Get information for checking in CSV file");
		documentFolder = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_DisplayName");
		documentFileName = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeHTML datacell_DocumentFile");
		lastUpoadDate = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeDateTime datacell_DocumentChangedDate");
		lastUploadBy = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeVarchar datacell_DocumentChangedByName");
		lastComments = documentsPage.getCellContentByCellID(driver,"DataCell FieldTypeLongVarchar datacell_ApproveReason");
		
		log.info("VP: Loans Documents information displayed correctly");
		verifyTrue(documentsPage.isAllLoansDocumentsRecordValuesDisplayedCorrectly(loanName, documentFolder, documentFileName, lastUpoadDate, lastUploadBy, lastComments));
		
		log.info("GlobalDocumentsView_01 - Step 17: Open Documents name");
		documentsPage.openLinkByText(driver, documentFileName);
		
		log.info("VP: The document is openned to view");
		verifyTrue(documentsPage.isDocumentViewable(documentFileName));
	}
	
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search borrower by Active status")
	public void GlobalDocumentsView_02_Lender_MakeDocumentsActiveInactive() {

		log.info("GlobalDocumentsView_02 - Step 01: Click on 'Check all' link");
		documentsPage.clickOnCheckAllLink();
		
		log.info("GlobalDocumentsView_02 - Step 02: Click on 'Make Borrower Inactive' button");
		documentsPage.clickMakeInactiveButton();
		
		log.info("VP: Loans Documents information displayed correctly");
		verifyFalse(documentsPage.isAllLoansDocumentsRecordValuesDisplayedCorrectly(loanName, documentFolder, documentFileName, lastUpoadDate, lastUploadBy, lastComments));
		
		log.info("GlobalDocumentsView_02 - Step 03: Select 'Status' ");
		documentsPage.selectActiveRadioButton("No");
	
		log.info("GlobalDocumentsView_02 - Step 04: Input 'Loan Name' and click on 'Search' button");
		documentsPage.clickOnSearchLoansButton(loanName);
		
		log.info("VP: Loans Documents information displayed correctly");
		verifyTrue(documentsPage.isAllLoansDocumentsRecordValuesDisplayedCorrectly(loanName, documentFolder, documentFileName, lastUpoadDate, lastUploadBy, lastComments));
		
		log.info("GlobalDocumentsView_02 - Step 05: Click on 'Check all' link");
		documentsPage.clickOnCheckAllLink();
		
		log.info("GlobalDocumentsView_02 - Step 06: Click on 'Make Borrower Inactive' button");
		documentsPage.clickMakeActiveButton();
		
		log.info("VP: Loans Documents information displayed correctly");
		verifyFalse(documentsPage.isAllLoansDocumentsRecordValuesDisplayedCorrectly(loanName, documentFolder, documentFileName, lastUpoadDate, lastUploadBy, lastComments));
		
		log.info("GlobalDocumentsView_02 - Step 07: Select 'Status' ");
		documentsPage.selectActiveRadioButton("Yes");
	
		log.info("GlobalDocumentsView_02 - Step 08: Input 'Loan Name' and click on 'Search' button");
		documentsPage.clickOnSearchLoansButton(loanName);
		
		log.info("VP: Loans Documents information displayed correctly");
		verifyTrue(documentsPage.isAllLoansDocumentsRecordValuesDisplayedCorrectly(loanName, documentFolder, documentFileName, lastUpoadDate, lastUploadBy, lastComments));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Open Documents file name")
	public void GlobalDocumentsView_03_Borrower_OpenDocumentsName() {

		log.info("GlobalDocumentsView_03 - Step 01. Login with Borrower");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);
	
		log.info("GlobalDocumentsView_03 - Step 02: Open Documents Page and open Loan Documents tab");
		documentsPage = applicantsPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openLoanDocumentsTab();
		
		log.info("GlobalDocumentsView_03 - Step 03: Input 'Loan Name' and click on 'Search' button");
		documentsPage.clickOnSearchLoansButton(loanName);
		
		log.info("VP: Loans Documents information displayed correctly");
		verifyTrue(documentsPage.isAllLoansDocumentsRecordValuesDisplayedCorrectly(loanName, documentFolder, documentFileName, lastUpoadDate, lastUploadBy, lastComments));
		
		log.info("GlobalDocumentsView_03 - Step 04: Open Documents name");
		documentsPage.openLinkByText(driver, documentFileName);
		
		log.info("VP: The document is openned to view");
		verifyTrue(documentsPage.isDocumentViewable(documentFileName));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private FNFLoansPage loanPage;
	private FNFDocumentPage documentsPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername, FNFBorrowerPassword;
	private String companyName, accountNumber;
	private String validDate;
	private String documentFolder;
	private String documentFileName;
	private String lastUpoadDate, lastUploadBy, lastComments;
	private String loanName, text;
	}