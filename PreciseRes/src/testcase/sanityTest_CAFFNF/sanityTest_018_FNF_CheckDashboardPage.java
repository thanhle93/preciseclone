package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFLoanDashboardPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_018_FNF_CheckDashboardPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
	}

	@Test(groups = { "regression" }, description = "Lender - Login as Lender - Make sure All loans are displayed")
	public void CheckDashboardPage_01_Lender_CheckAllLoansAreDisplayed() {

		log.info("DocumentHistorycal_01 - Step 01. Login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);

		log.info("DocumentHistorycal_01 - Step 02: Go to 'Loan Dashboard' page");
		dashboardPage = homePage.openLoanDashboardPage(driver, ipClient);
	
		log.info("DocumentHistorycal_01 - Step 03: Calculate number of Loans should be displayed");
		expectedNumberOfLoansDisplayed = dashboardPage.calculateTotalLoans();
		
		log.info("DocumentHistorycal_01 - Step 04: Get number of Loans are displaying");
		actualNumberOfLoansDisplayed = dashboardPage.getNumberOfLoansDisplayed();
		
		log.info("VP: All the loans are displayed");
		verifyEquals(expectedNumberOfLoansDisplayed, actualNumberOfLoansDisplayed);
	}
	
	@Test(groups = { "regression" }, description = "Lender - Login as Lender - Make sure All Loans Are Displayed In Right Category")
	public void CheckDashboardPage_02_Lender_CheckLoansAreDisplayedInRightCategory() {

		log.info("VP: All the loans are displayed in right category");
		verifyTrue(dashboardPage.checkLoansDisplaysInBucketCorrrectly());
		
	}


	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFLoanDashboardPage dashboardPage;
	private String FNFLenderUsername, FNFLenderPassword;
	private int expectedNumberOfLoansDisplayed, actualNumberOfLoansDisplayed;
	}