package page;

import org.openqa.selenium.WebDriver;

import PreciseRes.Interfaces;

public class MailHomePage extends AbstractPage {

	public MailHomePage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
	}
	
	/**
	 * check Gmail Home page display
	 * @param N/A
	 */
	public boolean isGmailHomePageDisplay() {
		waitForControl(driver, Interfaces.MailHomePage.MAIL_HOME_PAGE_BANNER, timeWait);
		return isControlDisplayed(driver, Interfaces.MailHomePage.MAIL_HOME_PAGE_BANNER);
	}
	
	/**
	 * click Inbox title
	 */
	public void clickInboxTitle() {
		waitForControl(driver, Interfaces.MailHomePage.INBOX_TITLE, timeWait);
		click(driver, Interfaces.MailHomePage.INBOX_TITLE);
		sleep();
	}
	
	/**
	 * click Subject fisrt email
	 */
	public void clickSubjectFirstEmail() {
		waitForControl(driver, Interfaces.MailHomePage.FIRST_SUBJECT_EMAIL, timeWait);
		click(driver, Interfaces.MailHomePage.FIRST_SUBJECT_EMAIL);
		sleep();
	}
	
	/**
	 * click Welcome email to B2R
	 */
	public void clickWelcomeEmailResend(String welcomeEmail) {
		waitForControl(driver, Interfaces.MailHomePage.WELCOME_EMAIL_RESEND_TITLE, welcomeEmail, timeWait);
		click(driver, Interfaces.MailHomePage.WELCOME_EMAIL_RESEND_TITLE, welcomeEmail);
		sleep(2);
	}
	
	/**
	 * click Link hyperlink
	 */
	public void clickLinkHyperlink() {
		waitForControl(driver, Interfaces.MailHomePage.LINK_HYPERLINK_DOWNLOAD, timeWait);
		click(driver, Interfaces.MailHomePage.LINK_HYPERLINK_DOWNLOAD);
		sleep(5);
		 if(driver.toString().toLowerCase().contains("internetexplorer")){
             sleep(5);
             keyPressing("alt_s");
             keyPressing("alt_s");
             keyPressing("alt_s");
      }
	}
	
	/**
	 * check Link hyperlink display
	 * @param N/A
	 */
	public boolean isLinkHyperlinkDisplay() {
		waitForControl(driver, Interfaces.MailHomePage.LINK_HYPERLINK_DOWNLOAD, timeWait);
		return isControlDisplayed(driver, Interfaces.MailHomePage.LINK_HYPERLINK_DOWNLOAD);
	}
	
	/**
	 * check Firstname/ Lastname display
	 * @param N/A
	 */
	public boolean isLastNameDisplay(String lastName) {
		waitForControl(driver, Interfaces.MailHomePage.LASTNAME_TITLE_DISPLAY, lastName, timeWait);
		return isControlDisplayed(driver, Interfaces.MailHomePage.LASTNAME_TITLE_DISPLAY, lastName);
	}
	
	/**
	 * click Google Apps title
	 */
	public void clickGoogleAppsTitle() {
		waitForControl(driver, Interfaces.MailHomePage.GOOGLE_APPS_TITLE, timeWait);
		click(driver, Interfaces.MailHomePage.GOOGLE_APPS_TITLE);
		sleep(2);
		while(!isControlDisplayed(driver, Interfaces.MailHomePage.GMAIL_APPS_TITLE)){
			click(driver, Interfaces.MailHomePage.GOOGLE_APPS_TITLE);
			sleep(2);
		}
	}
	
	/**
	 * click Gmail App
	 */
	public void clickGmailApplication() {
		waitForControl(driver, Interfaces.MailHomePage.GMAIL_APPS_TITLE, timeWait);
		click(driver, Interfaces.MailHomePage.GMAIL_APPS_TITLE);
		sleep(2);
	}
	
	/**
	 * click Data tooltip more dropdown
	 */
	public void clickDataTooltipMoreDropdown() {
		waitForControl(driver, Interfaces.MailHomePage.DATA_TOOLTIP_MORE_DROPDOWN, timeWait);
		click(driver, Interfaces.MailHomePage.DATA_TOOLTIP_MORE_DROPDOWN);
		sleep();
	}
	
	/**
	 * click Delete this message title
	 */
	public void clickDeleteThisMessageTitle() {
		waitForControl(driver, Interfaces.MailHomePage.DELETE_MESSAGE_TITLE, timeWait);
		click(driver, Interfaces.MailHomePage.DELETE_MESSAGE_TITLE);
		sleep(2);
	}
	
	/**
	 * click Lastest subject email
	 */
	public void clickLastestSubjectEmail() {
		waitForControl(driver, Interfaces.MailHomePage.LASTEST_SUBJECT_EMAIL, timeWait);
		click(driver, Interfaces.MailHomePage.LASTEST_SUBJECT_EMAIL);
		sleep();
	}
	
	/**
	 * click Lastest subject email
	 */
	public void clickLastestSubjectEmail(String emailTitle) {
		waitForControl(driver, Interfaces.MailHomePage.DYNAMIC_LASTEST_SUBJECT_EMAIL, emailTitle,  timeWait);
		click(driver, Interfaces.MailHomePage.DYNAMIC_LASTEST_SUBJECT_EMAIL, emailTitle);
		sleep();
	}
	
	/**
	 * click Lastest subject email dynamic
	 */
	public void clickLastestSubjectEmailDynamic(String title) {
		waitForControl(driver, Interfaces.MailHomePage.DYNAMIC_LASTEST_MAIL_TITLE, title, timeWait);
		click(driver, Interfaces.MailHomePage.DYNAMIC_LASTEST_MAIL_TITLE, title);
		sleep();
	}
	
	/**
	 * click Lastest subject email
	 */
	public void clickLastestSubjectEmailCAF() {
		waitForControl(driver, Interfaces.MailHomePage.LASTEST_SUBJECT_EMAIL_CAF, timeWait);
		click(driver, Interfaces.MailHomePage.LASTEST_SUBJECT_EMAIL_CAF);
		sleep();
	}
	
	/**
	 * Get email subject
	 */
	public Boolean isEmailSubjectDisplayed(String text) {
		waitForControl(driver, Interfaces.MailHomePage.DYNAMIC_EMAIL_SUBJECT, timeWait);
		return isControlDisplayed(driver, Interfaces.MailHomePage.DYNAMIC_EMAIL_SUBJECT, text);
	}
	
	/**
	 * Get email subject
	 */
	public Boolean isEmailMessageDisplayed(String text) {
		waitForControl(driver, Interfaces.MailHomePage.DYNAMIC_EMAIL_MESSAGE, timeWait);
		return isControlDisplayed(driver, Interfaces.MailHomePage.DYNAMIC_EMAIL_MESSAGE, text);
	}
	
	/**
	 * Get email content
	 */
	public Boolean isEmailContentDisplayed(String text) {
		waitForControl(driver, Interfaces.MailHomePage.DYNAMIC_EMAIL_CONTENT, timeWait);
		return isControlDisplayed(driver, Interfaces.MailHomePage.DYNAMIC_EMAIL_CONTENT, text);
	}
	
	/**
	 * Get email content
	 */
	public Boolean isEmailTableContentDisplayed(String text1, String text2, String text3, String text4, String text5) {
		waitForControl(driver, Interfaces.MailHomePage.DYNAMIC_EMAIL_TABLE_CONTENT, timeWait);
		return isControlDisplayed(driver, Interfaces.MailHomePage.DYNAMIC_EMAIL_TABLE_CONTENT, text1, text2, text3, text4, text5);
	}
	
	/**
	 * Get email content
	 */
	public String getEmailContent() {
		waitForControl(driver, Interfaces.MailHomePage.EMAIL_CONTENT, timeWait);
		return getText(driver, Interfaces.MailHomePage.EMAIL_CONTENT).trim().toLowerCase();
	}
	
	/**
	 * Get email content
	 */
	public String getEmailContentForCollumn2() {
		waitForControl(driver, Interfaces.MailHomePage.EMAIL_CONTENT_COLLUMN_2, timeWait);
		return getText(driver, Interfaces.MailHomePage.EMAIL_CONTENT_COLLUMN_2).trim().toLowerCase();
	}
	
	private WebDriver driver;
}