package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.ApplicantsPage;
import page.DocumentsPage;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.MyInformationPage;
import page.PageFactory;
import page.PropertiesPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_025_CAF_CheckAccessNavTab extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		applicantUsername = Constant.USERNAME_CAF_BORROWER;
		applicantPassword = Constant.PASSWORD_CAF_BORROWER;
		memberUsername = "auto-caf-member2014";
		memberPassword = "change";
	}
	
	@Test(groups = { "regression" },description = "NavTab - Lender - Checking nav tab")
	public void NavTab_01_Lender_CheckAccessNavTab()
	{
		log.info("NavTab_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("VP NavTab_01 - 01: Homepage displays correctly");
		verifyTrue(homePage.isHomePageDisplay());
		verifyFalse(homePage.isErrorMessageDisplay(driver, ipClient));
		
		log.info("NavTab_01 - Step 02. Open My Information nav-tab");
		myInformationPage = homePage.openMyInformationPage(driver, ipClient);
		
		log.info("VP NavTab_01 - 02: My Information displays correctly");
		verifyTrue(myInformationPage.isMyInformationPageDisplay());
		verifyFalse(myInformationPage.isErrorMessageDisplay(driver, ipClient));
		
		log.info("NavTab_01 - Step 03. Open Users nav-tab");
		usersPage = myInformationPage.openApplicantsPage(driver, ipClient);
		
		log.info("VP NavTab_01 - 03: Users displays correctly");
		verifyTrue(usersPage.isUsersPageDisplay());
		verifyFalse(usersPage.isErrorMessageDisplay(driver, ipClient));
		
		log.info("NavTab_01 - Step 04. Open Loan nav-tab");
		loansPage = usersPage.openLoansPage(driver, ipClient);
		
		log.info("VP NavTab_01 - 04: Loans displays correctly");
		verifyTrue(loansPage.isLoanPageDisplay());
		verifyFalse(loansPage.isErrorMessageDisplay(driver, ipClient));
		
		log.info("NavTab_01 - Step 05. Open Properties nav-tab");
		propertiesPage = loansPage.openPropertiesPage(driver, ipClient);
		
		log.info("VP NavTab_01 - 05: Properties displays correctly");
		verifyTrue(propertiesPage.isPropertiesPageDisplay());
		verifyFalse(propertiesPage.isErrorMessageDisplay(driver, ipClient));
		
		log.info("NavTab_01 - Step 06. Open Documents nav-tab");
		documentsPage = propertiesPage.openDocumentsPage(driver, ipClient);
		
		log.info("VP NavTab_01 - 06: Documents displays correctly");
		verifyTrue(documentsPage.isDocumentsPageDisplay());
		verifyFalse(documentsPage.isErrorMessageDisplay(driver, ipClient));
		
		//Remove Notification Template - Check Lists tab
		//https://preciseres.atlassian.net/browse/PRES-1386
//		log.info("NavTab_01 - Step 07. Open Notification Template nav-tab");
//		notificationTemplatePage = documentsPage.openNotificationTemplatesPage(driver, ipClient);
//		
//		log.info("VP NavTab_01 - 07: Notification Template displays correctly");
//		verifyTrue(notificationTemplatePage.isNotificationTemplatesPageDisplay());
//		verifyFalse(notificationTemplatePage.isErrorMessageDisplay(driver, ipClient));
//		
//		log.info("NavTab_01 - Step 08. Open Check Lists nav-tab");
//		checkListsPage = notificationTemplatePage.openCheckListsPage(driver, ipClient);
//		
//		log.info("VP NavTab_01 - 08: Check Lists displays correctly");
//		verifyTrue(checkListsPage.isCheckListsPageDisplay());
//		verifyFalse(checkListsPage.isErrorMessageDisplay(driver, ipClient));
	}
	
	@Test(groups = { "regression" }, description = "NavTab - Borrower - Checking nav tab")
	public void NavTab_02_Borrower_CheckAccessNavTab() {

		log.info("NavTab_02 - Step 01. Log out and login with applicant");
		loginPage = logout(driver, ipClient);
		loansPage = loginPage.loginAsBorrower(applicantUsername,	applicantPassword, false);
		
		log.info("VP NavTab_02 - 01: Loans displays correctly");
		verifyTrue(loansPage.isLoanPageDisplay());
		verifyFalse(loansPage.isErrorMessageDisplay(driver, ipClient));
		
		log.info("NavTab_02 - Step 02. Open Properties nav-tab");
		propertiesPage = loansPage.openPropertiesPage(driver, ipClient);
		
		log.info("VP NavTab_02 - 02: Properties displays correctly");
		verifyTrue(propertiesPage.isPropertiesPageDisplay());
		verifyFalse(propertiesPage.isErrorMessageDisplay(driver, ipClient));
		
		log.info("NavTab_02 - Step 03. Open Documents nav-tab");
		documentsPage = propertiesPage.openDocumentsPage(driver, ipClient);
		
		log.info("VP NavTab_02 - 03: Documents displays correctly");
		verifyTrue(documentsPage.isDocumentsPageDisplay());
		verifyFalse(documentsPage.isErrorMessageDisplay(driver, ipClient));
		
//		log.info("NavTab_02 - Step 04. Open Notification Template nav-tab");
//		notificationTemplatePage = documentsPage.openNotificationTemplatesPage(driver, ipClient);
//		
//		log.info("VP NavTab_02 - 04: Notification Template displays correctly");
//		verifyTrue(notificationTemplatePage.isNotificationTemplatesPageDisplay());
//		verifyFalse(notificationTemplatePage.isErrorMessageDisplay(driver, ipClient));
	}
	
	@Test(groups = { "regression" }, description = "NavTab - Member - Checking nav tab")
	public void NavTab_03_Member_CheckAccessNavTab() {
		
		log.info("NavTab_03 - Step 01. Log out and login with Member");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(memberUsername,	memberPassword, false);
		
		log.info("VP NavTab_03 - 01: Homepage displays correctly");
		verifyTrue(homePage.isHomePageDisplay());
		verifyFalse(homePage.isErrorMessageDisplay(driver, ipClient));
		
		log.info("NavTab_03 - Step 02. Open Users nav-tab");
		usersPage = homePage.openApplicantsPage(driver, ipClient);
		
		log.info("VP NavTab_03 - 02: Users displays correctly");
		verifyTrue(usersPage.isUsersPageDisplay());
		verifyFalse(usersPage.isErrorMessageDisplay(driver, ipClient));
		
		log.info("NavTab_03 - Step 03. Open Loan nav-tab");
		loansPage = usersPage.openLoansPage(driver, ipClient);
		
		log.info("VP NavTab_03 - 03: Loans displays correctly");
		verifyTrue(loansPage.isLoanPageDisplay());
		verifyFalse(loansPage.isErrorMessageDisplay(driver, ipClient));
		
		log.info("NavTab_03 - Step 04. Open Properties nav-tab");
		propertiesPage = loansPage.openPropertiesPage(driver, ipClient);
		
		log.info("VP NavTab_03 - 04: Properties displays correctly");
		verifyTrue(propertiesPage.isPropertiesPageDisplay());
		verifyFalse(propertiesPage.isErrorMessageDisplay(driver, ipClient));
		
		log.info("NavTab_03 - Step 05. Open Documents nav-tab");
		documentsPage = propertiesPage.openDocumentsPage(driver, ipClient);
		
		log.info("VP NavTab_03 - 05: Documents displays correctly");
		verifyTrue(documentsPage.isDocumentsPageDisplay());
		verifyFalse(documentsPage.isErrorMessageDisplay(driver, ipClient));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private MyInformationPage myInformationPage;
	private ApplicantsPage usersPage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private DocumentsPage documentsPage;
	private String usernameLender, passwordLender;
	private String memberUsername, memberPassword;
	private String applicantUsername, applicantPassword;
}