package sanityTest_PreCondition;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.AdminPage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_000_CheckManagingPlaceholders extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameAdmin = Constant.USERNAME_ADMIN;
		passwordAdmin = Constant.PASSWORD_ADMIN;
	}
	
	@Test(groups = { "regression" },description = "CheckManagingPlaceholders - B2R - Check Managing placeholders for B2R")
	public void CheckManagingPlaceholders_01_B2R()
	{
		log.info("CheckManagingPlaceholders_01 - Step 01. Login with Admin");
		adminPage = loginPage.loginWithAdmin(usernameAdmin, passwordAdmin, false);
		
		log.info("CheckManagingPlaceholders_01 - Step 02. Open Managing Placeholders tab");
		adminPage.openManagingPlaceholdersTab();
		
		log.info("CheckManagingPlaceholders_01 - Step 03. Open Section Types tab");
		adminPage.openSectionTypesTab();
		
		//Property Manager B2R
		
		log.info("CheckManagingPlaceholders_01 - Step 04. Select Loan Application for search Placeholders");
		adminPage.selectLoanApplicationForSearchPlaceholders("B2R");
		
		log.info("CheckManagingPlaceholders_01 - Step 05. Click on Search button");
		adminPage.clickOnSearchButton();
		
		log.info("CheckManagingPlaceholders_01 - Step 06. Open Property Manager placeholders");
		adminPage.openPlaceholdersGroup("Property Management");
		
		log.info("VP: Add On Loan Created checkbox is checked");
		verifyTrue(adminPage.isAddOnLoanCreatedChecked());
		
		log.info("VP: Check documents types is displayed correctly");
		verifyTrue(adminPage.isDocumentsTypeDisplayed("L31 - Management Company Questionnaire"));
		verifyTrue(adminPage.isDocumentsTypeDisplayed("L32 - Management Agreement"));
		verifyTrue(adminPage.isDocumentsTypeDisplayed("L33 - Management Company Certificate of Formation"));
		verifyTrue(adminPage.isDocumentsTypeDisplayed("L34 - Management Company Certificate of Good Standing"));
		
		log.info("CheckManagingPlaceholders_01 - Step 07. Click on Back to List button");
		adminPage.clickOnSearchButton();
		
		//B2R UW / Credit B2R
		
		log.info("CheckManagingPlaceholders_01 - Step 04. Select Loan Application for search Placeholders");
		adminPage.selectLoanApplicationForSearchPlaceholders("B2R");
		
		log.info("CheckManagingPlaceholders_01 - Step 05. Click on Search button");
		adminPage.clickOnSearchButton();
		
		log.info("CheckManagingPlaceholders_01 - Step 06. Open B2R UW / Credit placeholders");
		adminPage.openPlaceholdersGroup("B2R UW / Credit");
		
		log.info("VP: Add On Loan Created checkbox is checked");
		verifyTrue(adminPage.isAddOnLoanCreatedChecked());
		
		log.info("VP: Check documents types is displayed correctly");
		verifyTrue(adminPage.isDocumentsTypeDisplayed("L41 - Credit Memo/Approval"));
		verifyTrue(adminPage.isDocumentsTypeDisplayed("L42 - Final Rockport UW Model"));
		
		log.info("CheckManagingPlaceholders_01 - Step 07. Click on Back to List button");
		adminPage.clickOnSearchButton();
		
		//Borrower (SPE) B2R
		
		log.info("CheckManagingPlaceholders_01 - Step 04. Select Loan Application for search Placeholders");
		adminPage.selectLoanApplicationForSearchPlaceholders("B2R");
		
		log.info("CheckManagingPlaceholders_01 - Step 05. Click on Search button");
		adminPage.clickOnSearchButton();
		
		log.info("CheckManagingPlaceholders_01 - Step 06. Open Borrower (SPE) placeholders");
		adminPage.openPlaceholdersGroup("Borrower (SPE)");
		
		log.info("VP: Add On Loan Created checkbox is checked");
		verifyTrue(adminPage.isAddOnLoanCreatedChecked());
		
		log.info("VP: Check documents types is displayed correctly");
		verifyTrue(adminPage.isDocumentsTypeDisplayed("L8 - Borrower Org Chart"));
		verifyTrue(adminPage.isDocumentsTypeDisplayed("L7 - IRS Form W-9"));
		verifyTrue(adminPage.isDocumentsTypeDisplayed("L1 - Loan Application"));
		verifyTrue(adminPage.isDocumentsTypeDisplayed("L3 - Background Search"));
		
		log.info("CheckManagingPlaceholders_01 - Step 07. Click on Back to List button");
		adminPage.clickOnSearchButton();
		
		//Applicant B2R
		
		log.info("CheckManagingPlaceholders_01 - Step 04. Select Loan Application for search Placeholders");
		adminPage.selectLoanApplicationForSearchPlaceholders("B2R");
		
		log.info("CheckManagingPlaceholders_01 - Step 05. Click on Search button");
		adminPage.clickOnSearchButton();
		
		log.info("CheckManagingPlaceholders_01 - Step 06. Open Applicant placeholders");
		adminPage.openPlaceholdersGroup("Applicant");
		
		log.info("VP: Add On Loan Created checkbox is checked");
		verifyTrue(adminPage.isAddOnLoanCreatedChecked());
		
		log.info("VP: Check documents types is displayed correctly");
		verifyTrue(adminPage.isDocumentsTypeDisplayed("L2 - Executed Borrower Term Sheet"));
		
		log.info("CheckManagingPlaceholders_01 - Step 07. Click on Back to List button");
		adminPage.clickOnSearchButton();
		
		//Gurantor B2R
		
		log.info("CheckManagingPlaceholders_01 - Step 04. Select Loan Application for search Placeholders");
		adminPage.selectLoanApplicationForSearchPlaceholders("B2R");
		
		log.info("CheckManagingPlaceholders_01 - Step 05. Click on Search button");
		adminPage.clickOnSearchButton();
		
		log.info("CheckManagingPlaceholders_01 - Step 06. Open Guarantor placeholders");
		adminPage.openPlaceholdersGroup("Guarantor");
		
		log.info("VP: Add On Loan Created checkbox is checked");
		verifyTrue(adminPage.isAddOnLoanCreatedChecked());
		
		log.info("VP: Check documents types is displayed correctly");
		verifyTrue(adminPage.isDocumentsTypeDisplayed("L17 - Personal Financial Statement"));
		verifyTrue(adminPage.isDocumentsTypeDisplayed("L18 - PFS Certification"));
		verifyTrue(adminPage.isDocumentsTypeDisplayed("L19 - Proof of Liquidity Statements"));
		verifyTrue(adminPage.isDocumentsTypeDisplayed("L20 - Real Estate Resume"));
		
		log.info("CheckManagingPlaceholders_01 - Step 07. Click on Back to List button");
		adminPage.clickOnSearchButton();
		
		//Pledgor B2R
		
		log.info("CheckManagingPlaceholders_01 - Step 04. Select Loan Application for search Placeholders");
		adminPage.selectLoanApplicationForSearchPlaceholders("B2R");
		
		log.info("CheckManagingPlaceholders_01 - Step 05. Click on Search button");
		adminPage.clickOnSearchButton();
		
		log.info("CheckManagingPlaceholders_01 - Step 06. Open Pledgor placeholders");
		adminPage.openPlaceholdersGroup("Pledgor");
		
		log.info("VP: Add On Loan Created checkbox is checked");
		verifyTrue(adminPage.isAddOnLoanCreatedChecked());
		
		log.info("VP: Check documents types is displayed correctly");
		verifyTrue(adminPage.isDocumentsTypeDisplayed("L4 - Credit Report"));
		
		log.info("CheckManagingPlaceholders_01 - Step 07. Click on Back to List button");
		adminPage.clickOnSearchButton();
		
		//Sponsor B2R
		
		log.info("CheckManagingPlaceholders_01 - Step 04. Select Loan Application for search Placeholders");
		adminPage.selectLoanApplicationForSearchPlaceholders("B2R");
		
		log.info("CheckManagingPlaceholders_01 - Step 05. Click on Search button");
		adminPage.clickOnSearchButton();
		
		log.info("CheckManagingPlaceholders_01 - Step 06. Open Sponsor placeholders");
		adminPage.openPlaceholdersGroup("Sponsor");
		
		log.info("VP: Add On Loan Created checkbox is checked");
		verifyTrue(adminPage.isAddOnLoanCreatedChecked());
		
		log.info("VP: Check documents types is displayed correctly");
		verifyTrue(adminPage.isDocumentsTypeDisplayed("Placeholder for Sponsor 01"));
		
		log.info("CheckManagingPlaceholders_01 - Step 07. Click on Back to List button");
		adminPage.clickOnSearchButton();
	}
	
	@Test(groups = { "regression" },description = "CheckManagingPlaceholders - CAF - Check Managing placeholders for CAF")
	public void CheckManagingPlaceholders_02_CAF()
	{
		
		//General CAF
		
		log.info("CheckManagingPlaceholders_02 - Step 04. Select Loan Application for search Placeholders");
		adminPage.selectLoanApplicationForSearchPlaceholders("Colony");
		
		log.info("CheckManagingPlaceholders_02 - Step 05. Click on Search button");
		adminPage.clickOnSearchButton();
		
		log.info("CheckManagingPlaceholders_02 - Step 06. Open General placeholders");
		adminPage.openPlaceholdersGroup("General");
		
		log.info("VP: Add On Loan Created checkbox is checked");
		verifyTrue(adminPage.isAddOnLoanCreatedChecked());
		
		log.info("VP: Check documents types is displayed correctly");
		verifyTrue(adminPage.isDocumentsTypeDisplayed("Org Chart"));
		
		log.info("CheckManagingPlaceholders_02 - Step 07. Click on Back to List button");
		adminPage.clickOnSearchButton();
		
		//Property Manager CAF
		
		log.info("CheckManagingPlaceholders_02 - Step 04. Select Loan Application for search Placeholders");
		adminPage.selectLoanApplicationForSearchPlaceholders("Colony");
		
		log.info("CheckManagingPlaceholders_02 - Step 05. Click on Search button");
		adminPage.clickOnSearchButton();
		
		log.info("CheckManagingPlaceholders_02 - Step 06. Open Property Manager placeholders");
		adminPage.openPlaceholdersGroup("Property Manager");
		
		log.info("VP: Add On Loan Created checkbox is checked");
		verifyTrue(adminPage.isAddOnLoanCreatedChecked());
		
		log.info("VP: Check documents types is displayed correctly");
		verifyTrue(adminPage.isDocumentsTypeDisplayed("Management Agreement"));
		verifyTrue(adminPage.isDocumentsTypeDisplayed("Form of Lease"));
		
		log.info("CheckManagingPlaceholders_02 - Step 07. Click on Back to List button");
		adminPage.clickOnSearchButton();
		
		//Borrower CAF
		
		log.info("CheckManagingPlaceholders_02 - Step 04. Select Loan Application for search Placeholders");
		adminPage.selectLoanApplicationForSearchPlaceholders("Colony");
		
		log.info("CheckManagingPlaceholders_02 - Step 05. Click on Search button");
		adminPage.clickOnSearchButton();
		
		log.info("CheckManagingPlaceholders_02 - Step 06. Open Borrower placeholders");
		adminPage.openPlaceholdersGroup("Borrower");
		
		log.info("VP: Add On Loan Created checkbox is checked");
		verifyTrue(adminPage.isAddOnLoanCreatedChecked());
		
		log.info("VP: Check documents types is displayed correctly");
		verifyTrue(adminPage.isDocumentsTypeDisplayed("Operating Agreement"));
		verifyTrue(adminPage.isDocumentsTypeDisplayed("Certificate of Good Standing"));
		
		log.info("CheckManagingPlaceholders_02 - Step 07. Click on Back to List button");
		adminPage.clickOnSearchButton();
		
		//Applicant CAF
		
		log.info("CheckManagingPlaceholders_02 - Step 04. Select Loan Application for search Placeholders");
		adminPage.selectLoanApplicationForSearchPlaceholders("Colony");
		
		log.info("CheckManagingPlaceholders_02 - Step 05. Click on Search button");
		adminPage.clickOnSearchButton();
		
		log.info("CheckManagingPlaceholders_02 - Step 06. Open Applicant placeholders");
		adminPage.openPlaceholdersGroup("Applicant");
		
		log.info("VP: Add On Loan Created checkbox is checked");
		verifyTrue(adminPage.isAddOnLoanCreatedChecked());
		
		log.info("VP: Check documents types is displayed correctly");
		verifyTrue(adminPage.isDocumentsTypeDisplayed("Org Chart"));
		
		log.info("CheckManagingPlaceholders_02 - Step 07. Click on Back to List button");
		adminPage.clickOnSearchButton();
		
		//Guarantor CAF
		
		log.info("CheckManagingPlaceholders_02 - Step 04. Select Loan Application for search Placeholders");
		adminPage.selectLoanApplicationForSearchPlaceholders("Colony");
		
		log.info("CheckManagingPlaceholders_02 - Step 05. Click on Search button");
		adminPage.clickOnSearchButton();
		
		log.info("CheckManagingPlaceholders_02 - Step 06. Open Guarantor placeholders");
		adminPage.openPlaceholdersGroup("Guarantor");
		
		log.info("VP: Add On Loan Created checkbox is checked");
		verifyTrue(adminPage.isAddOnLoanCreatedChecked());
		
		log.info("VP: Check documents types is displayed correctly");
		verifyTrue(adminPage.isDocumentsTypeDisplayed("Financials"));
		
		log.info("CheckManagingPlaceholders_02 - Step 07. Click on Back to List button");
		adminPage.clickOnSearchButton();
		
		//Pledgor CAF
		
		log.info("CheckManagingPlaceholders_02 - Step 04. Select Loan Application for search Placeholders");
		adminPage.selectLoanApplicationForSearchPlaceholders("Colony");
		
		log.info("CheckManagingPlaceholders_02 - Step 05. Click on Search button");
		adminPage.clickOnSearchButton();
		
		log.info("CheckManagingPlaceholders_02 - Step 06. Open Pledgor placeholders");
		adminPage.openPlaceholdersGroup("Pledgor");
		
		log.info("VP: Add On Loan Created checkbox is checked");
		verifyTrue(adminPage.isAddOnLoanCreatedChecked());
		
		log.info("VP: Check documents types is displayed correctly");
		verifyTrue(adminPage.isDocumentsTypeDisplayed("Certificate/Articles of Formation"));
		
		log.info("CheckManagingPlaceholders_02 - Step 07. Click on Back to List button");
		adminPage.clickOnSearchButton();
		
		//Sponsor CAF
		
		log.info("CheckManagingPlaceholders_02 - Step 04. Select Loan Application for search Placeholders");
		adminPage.selectLoanApplicationForSearchPlaceholders("Colony");
		
		log.info("CheckManagingPlaceholders_02 - Step 05. Click on Search button");
		adminPage.clickOnSearchButton();
		
		log.info("CheckManagingPlaceholders_02 - Step 06. Open Sponsor placeholders");
		adminPage.openPlaceholdersGroup("Sponsor");
		
		log.info("VP: Add On Loan Created checkbox is checked");
		verifyTrue(adminPage.isAddOnLoanCreatedChecked());
		
		log.info("VP: Check documents types is displayed correctly");
		verifyTrue(adminPage.isDocumentsTypeDisplayed("Certificate of Good Standing"));
		
		log.info("CheckManagingPlaceholders_02 - Step 07. Click on Back to List button");
		adminPage.clickOnSearchButton();
	}
	
	@Test(groups = { "regression" },description = "CheckManagingPlaceholders - Store - Check Managing placeholders for Store")
	public void CheckManagingPlaceholders_03_Store()
	{
		
		//Lease Documents Store
		
		log.info("CheckManagingPlaceholders_03 - Step 04. Select Loan Application for search Placeholders");
		adminPage.selectLoanApplicationForSearchPlaceholders("Store");
		
		log.info("CheckManagingPlaceholders_03 - Step 05. Click on Search button");
		adminPage.clickOnSearchButton();
		
		log.info("CheckManagingPlaceholders_03 - Step 06. Open Lease Documents placeholders");
		adminPage.openPlaceholdersGroup("Lease Documents");
		
		log.info("VP: Add On Loan Created checkbox is checked");
		verifyTrue(adminPage.isAddOnLoanCreatedChecked());
		
		log.info("VP: Check documents types is displayed correctly");
		verifyTrue(adminPage.isDocumentsTypeDisplayed("Promissory Note"));
		
		log.info("CheckManagingPlaceholders_03 - Step 07. Click on Back to List button");
		adminPage.clickOnSearchButton();
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private AdminPage adminPage;
	private String usernameAdmin, passwordAdmin;
}