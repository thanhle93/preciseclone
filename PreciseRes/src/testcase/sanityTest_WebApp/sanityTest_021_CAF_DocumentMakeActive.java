package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.DocumentsPage;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_021_CAF_DocumentMakeActive extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_CAF;
		applicantName = "Udi Dorner";
		borrowerName = "UdiTeamBorrower";
		address1 = "504 W Euclid";
		documentType1 = "Lease Agreement";
		documentFileName = "datatest.pdf";
		propertiesFileNameRequired = "CAF Required.xlsx";
		documentSection1 = "General Loan Documents - General";
		documentSection2 = "Corporate Entity Documents - Borrower";
		documentType2 = "Org Chart";
		documentType3 = "Operating Agreement";
		active = "Yes";
		inActive = "No";
	}

	@Test(groups = { "regression" }, description = "Document 34 - Properties - Make a document active")
	public void DocumentTest_34_Properties_MakeADocumentActive() {

		log.info("DocumentTest_34 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender,	false);

		log.info("DocumentTest_34 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_34 - Step 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, borrowerName, loanStatus);
		
		log.info("DocumentTest_34 - Step 04. Open 'Properties' tab");
		loansPage.openPropertiesTab();

		log.info("DocumentTest_34 - Step 05. Load test file");
		loansPage.uploadFileProperties(propertiesFileNameRequired);
		
		log.info("DocumentTest_34 - Step 06. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);
		
		log.info("DocumentTest_34 - Step 07. Click New document button");
		propertiesPage.clickNewDocumentButton();

		log.info("DocumentTest_34 - Step 08. Select document type");
		propertiesPage.selectDocumentType(documentType1);

		log.info("DocumentTest_34 - Step 09. Uncheck active checkbox");
		propertiesPage.uncheckActiveCheckbox();

		log.info("DocumentTest_34 - Step 10. Upload document file");
		propertiesPage.uploadDocumentFile(documentFileName);

		log.info("DocumentTest_34 - Step 11. Click 'Save' button");
		propertiesPage.clickSaveButton();

		log.info("DocumentTest_34 - Step 12. Click 'Document List' button");
		propertiesPage.clickOnDocumentListButton();
		
		log.info("DocumentTest_34 - Step 13. Click 'Active' search");
		propertiesPage.searchActive();
		
		log.info("VP: Document is loaded to document type and active is 'No'");
		verifyTrue(propertiesPage.isActivatedDocumentLoadedToDocumentType(documentType1, inActive));
		
		log.info("DocumentTest_24 - Step 14. Get value 'Document#'");
		documentID = propertiesPage.getDocumentID();
		
		log.info("DocumentTest_34 - Step 15. Click 'Document Type' checkbox");
		propertiesPage.isSelectedDocumentTypeCheckbox(documentType1, inActive);
		
		log.info("DocumentTest_34 - Step 16. Click 'Make Document Active' button");
		propertiesPage.clickMakeDocumentActiveButton();
		
		log.info("DocumentTest_24 - Step 17. Input 'Document #' and Search");
		propertiesPage.searchDocumentID(documentID);
		
		log.info("VP: Document is loaded to document type and active is 'Yes'");
		verifyTrue(propertiesPage.isActivatedDocumentLoadedToDocumentType(documentType1, active));
	}
	
	@Test(groups = { "regression" }, description = "Document 35 - General Loan Document - Make a document active")
	public void DocumentTest_35_GeneralLoanDocument_MakeADocumentActive() {

		log.info("DocumentTest_35 - Step 01. Open General Loan Documents tab");
		documentsPage = propertiesPage.openGeneralDocumentTab();
		
		log.info("DocumentTest_35 - Step 02. Click New button");
		documentsPage.clickNewButton();

		log.info("DocumentTest_35 - Step 03. Select section value");
		documentsPage.selectGeneralDocumentSection(documentSection1);

		log.info("DocumentTest_35 - Step 04. Select document type");
		documentsPage.selectGeneralDocumentType(documentType2);

		log.info("DocumentTest_35 - Step 05. Upload general document");
		documentsPage.uploadDocumentFile(documentFileName);

		log.info("DocumentTest_35 - Step 06. Uncheck active checkbox");
		documentsPage.uncheckActiveCheckbox();

		log.info("DocumentTest_35 - Step 07. Click Save button");
		documentsPage.clickSaveButton();

		log.info("DocumentTest_35 - Step 08. Click Document List button");
		documentsPage.clickDocumentListButton();
		
		log.info("DocumentTest_35 - Step 09. Click 'Active' search");
		documentsPage.searchActive();
		
		log.info("VP: Document is loaded to document type and active is 'No'");
		verifyTrue(documentsPage.isActivatedDocumentLoadedToDocumentType(documentType2, inActive));
		
		log.info("DocumentTest_35 - Step 10. Get value 'Document#'");
		documentID = documentsPage.getDocumentID();
		
		log.info("DocumentTest_35 - Step 11. Click 'Document Type' checkbox");
		documentsPage.isSelectedDocumentTypeCheckbox(documentType2, inActive);
		
		log.info("DocumentTest_35 - Step 12. Click 'Make Document Active' button");
		documentsPage.clickMakeDocumentActiveButton();
		
		log.info("DocumentTest_35 - Step 13. Input 'Document #' and Search");
		documentsPage.searchID(documentID);
		
		log.info("VP: Document is loaded to document type and active is 'Yes'");
		verifyTrue(documentsPage.isActivatedDocumentLoadedToDocumentType(documentType2, active));
	}

	@Test(groups = { "regression" }, description = "Document 36 - Corporate Entity Document- Make a document active")
	public void DocumentTest_36_CorporateEntityDocument_MakeADocumentActive() {

		log.info("DocumentTest_36 - Step 01. Open Corporate Entity Documents tab");
		documentsPage.openCorporateEntityDocumentsTab();

		log.info("DocumentTest_36 - Step 02. Click new button");
		documentsPage.clickNewButton();

		log.info("DocumentTest_36 - Step 03. Select section value");
		documentsPage.selectGeneralDocumentSection(documentSection2);

		log.info("DocumentTest_36 - Step 04. Select document type");
		documentsPage.selectGeneralDocumentType(documentType3);

		log.info("DocumentTest_36 - Step 05. Upload general document");
		documentsPage.uploadDocumentFile(documentFileName);

		log.info("DocumentTest_36 - Step 06. Uncheck active checkbox");
		documentsPage.uncheckActiveCheckbox();

		log.info("DocumentTest_36 - Step 07. Click Save button");
		documentsPage.clickSaveButton();

		log.info("DocumentTest_36 - Step 08. Click Document List button");
		documentsPage.clickDocumentListButton();
		
		log.info("DocumentTest_36 - Step 09. Click 'Active' search");
		documentsPage.searchActive();
		
		log.info("VP: Document is loaded to document type and active is 'No'");
		verifyTrue(documentsPage.isActivatedDocumentLoadedToDocumentType(documentType3, inActive));
		
		log.info("DocumentTest_36 - Step 10. Get value 'Document#'");
		documentID = documentsPage.getDocumentID();
		
		log.info("DocumentTest_36 - Step 11. Click 'Document Type' checkbox");
		documentsPage.isSelectedDocumentTypeCheckbox(documentType3, inActive);
		
		log.info("DocumentTest_36 - Step 12. Click 'Make Document Active' button");
		documentsPage.clickMakeDocumentActiveButton();
		
		log.info("DocumentTest_36 - Step 13. Input 'Document #' and Search");
		documentsPage.searchID(documentID);
		
		log.info("VP: Document is loaded to document type and active is 'Yes'");
		verifyTrue(documentsPage.isActivatedDocumentLoadedToDocumentType(documentType3, active));
	}

	
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private DocumentsPage documentsPage;
	private String usernameLender, passwordLender, address1, documentFileName;
	private String loanName, accountNumber, applicantName, loanStatus;
	private String documentType1, documentType2, documentType3;
	private String borrowerName, propertiesFileNameRequired, documentSection1, documentSection2;
	private String active, inActive, documentID;
}