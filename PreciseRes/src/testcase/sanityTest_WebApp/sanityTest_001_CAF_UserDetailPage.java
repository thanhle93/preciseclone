package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import page.TermsOfUsePage;
import page.UserLoginsPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_001_CAF_UserDetailPage extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameUser = Constant.USERNAME_ADMIN;
		passwordUser = Constant.PASSWORD_ADMIN;
		accountNumber = getUniqueNumber();
		loginId = "uditeam"+accountNumber;
		password = "change123";
		newPassword = "change";
		userRole = "Member";
		loanApplication = "Colony";
		firstName = "uditeam";
		lastName = "testing"+accountNumber;
		userEmail = "uditeamtesting"+accountNumber+"@uditeam.com";
	}
	
	@Test(groups = { "regression" },description = "Create New Member With All Blank Fields")
	public void UserDetail_01_CreateNewMemberWithAllBlankFields()
	{
		log.info("UserDetail_01 - Step 01. Login with Admin");
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		
		log.info("UserDetail_01 - Step 02. Go to List of user");
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("UserDetail_01 - Step 03. Click new button to create new user");
		userLoginsPage.clickNewUserButton();
		
		log.info("UserDetail_01 - Step 04. Leave with all blank field");
		log.info("UserDetail_01 - Step 05. Click Save button to save New User Info");
		userLoginsPage.clickSaveUserDetailButton();
		
		log.info("VP: Error message 'Please correct the errors and try again' displays");
		verifyTrue(userLoginsPage.isErrorMessageWithContentDisplay("Please correct the errors and try again"));
	}
	
	@Test(groups = { "regression" },description = "Create New Member With Blank Last name")
	public void UserDetail_02_CreateNewMemberWithBlankLastname()
	{
		log.info("UserDetail_02 - Step 01. Go to List of user");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("UserDetail_02 - Step 02. Click new button to create new user");
		userLoginsPage.clickNewUserButton();
		
		log.info("UserDetail_02 - Step 03. Input first name");
		userLoginsPage.inputUserFirstName(firstName);
		
		log.info("UserDetail_02 - Step 04. Input last name");
		userLoginsPage.inputUserLastName("");
		
		log.info("UserDetail_02 - Step 05. Input login ID");
		userLoginsPage.inputUserLoginID(loginId);
		
		log.info("UserDetail_02 - Step 06. Input Password");
		userLoginsPage.inputPassword(password);
		
		log.info("UserDetail_02 - Step 07. Retype Password");
		userLoginsPage.retypePassword(password);
		
		log.info("UserDetail_02 - Step 08. Select User Role (Member)");
		userLoginsPage.selectUserRole(userRole);
		
		log.info("UserDetail_02 - Step 09. Input email");
		userLoginsPage.inputEmail(userEmail);
		
		log.info("UserDetail_02 - Step 10. Select Loan Application");
		userLoginsPage.selectLoanApplication(loanApplication);
		
		log.info("UserDetail_02 - Step 11. Click Save button to save New User Info");
		userLoginsPage.clickSaveUserDetailButton();		
		
		log.info("VP: Error message 'Please correct errors marked below' displays");
		verifyTrue(userLoginsPage.isErrorMessageWithContentDisplay("Please correct errors marked below"));
		
		log.info("VP: Error message 'Last Name is required' displays for last name field");
		verifyTrue(userLoginsPage.isErrorMessageForLastNameDisplay("Last Name is required"));
	}
	
	@Test(groups = { "regression" },description = "Create New Member With Blank Login ID")
	public void UserDetail_03_CreateNewMemberWithBlankLoginID()
	{
		log.info("UserDetail_03 - Step 01. Go to List of user");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("UserDetail_03 - Step 02. Click new button to create new user");
		userLoginsPage.clickNewUserButton();
		
		log.info("UserDetail_03 - Step 03. Input first name");
		userLoginsPage.inputUserFirstName(firstName);
		
		log.info("UserDetail_03 - Step 04. Input last name");
		userLoginsPage.inputUserLastName(lastName);
		
		log.info("UserDetail_03 - Step 05. Input login ID");
		userLoginsPage.inputUserLoginID("");
		
		log.info("UserDetail_03 - Step 06. Input Password");
		userLoginsPage.inputPassword(password);
		
		log.info("UserDetail_03 - Step 07. Retype Password");
		userLoginsPage.retypePassword(password);
		
		log.info("UserDetail_03 - Step 08. Select User Role (Member)");
		userLoginsPage.selectUserRole(userRole);
		
		log.info("UserDetail_03 - Step 09. Input email");
		userLoginsPage.inputEmail(userEmail);
		
		log.info("UserDetail_03 - Step 10. Select Loan Application");
		userLoginsPage.selectLoanApplication(loanApplication);
		
		log.info("UserDetail_03 - Step 11. Click Save button to save New User Info");
		userLoginsPage.clickSaveUserDetailButton();		
		
		log.info("VP: Error message 'Please correct errors marked below' displays");
		verifyTrue(userLoginsPage.isErrorMessageWithContentDisplay("Please correct errors marked below"));
		
		log.info("VP: Error message 'Login ID is required' displays for Login ID field");
		verifyTrue(userLoginsPage.isErrorMessageForLoginIdDisplay("Login ID is required"));
	}
	
	@Test(groups = { "regression" },description = "Create New Member With Blank Role")
	public void UserDetail_04_CreateNewMemberWithBlankRole()
	{
		log.info("UserDetail_04 - Step 01. Go to List of user");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("UserDetail_04 - Step 02. Click new button to create new user");
		userLoginsPage.clickNewUserButton();
		
		log.info("UserDetail_04 - Step 03. Input first name");
		userLoginsPage.inputUserFirstName(firstName);
		
		log.info("UserDetail_04 - Step 04. Input last name");
		userLoginsPage.inputUserLastName(lastName);
		
		log.info("UserDetail_04 - Step 05. Input login ID");
		userLoginsPage.inputUserLoginID(loginId);
		
		log.info("UserDetail_04 - Step 06. Input Password");
		userLoginsPage.inputPassword(password);
		
		log.info("UserDetail_04 - Step 07. Retype Password");
		userLoginsPage.retypePassword(password);
		
		log.info("UserDetail_04 - Step 08. Select User Role (Member)");
		userLoginsPage.selectUserRole("");
		
		log.info("UserDetail_04 - Step 09. Input email");
		userLoginsPage.inputEmail(userEmail);
		
		log.info("UserDetail_04 - Step 10. Select Loan Application");
		userLoginsPage.selectLoanApplication(loanApplication);
		
		log.info("UserDetail_04 - Step 11. Click Save button to save New User Info");
		userLoginsPage.clickSaveUserDetailButton();		
		
		log.info("VP: Error message 'Please correct errors marked below' displays");
		verifyTrue(userLoginsPage.isErrorMessageWithContentDisplay("Please correct errors marked below"));
		
		log.info("VP: Error message 'Role is required' displays for Role field");
		verifyTrue(userLoginsPage.isErrorMessageForRoleDisplay("Role is required"));
	}
	
	@Test(groups = { "regression" },description = "Create New Member With Blank Password")
	public void UserDetail_05_CreateNewMemberWithBlankPassword()
	{
		log.info("UserDetail_05 - Step 01. Go to List of user");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("UserDetail_05 - Step 02. Click new button to create new user");
		userLoginsPage.clickNewUserButton();
		
		log.info("UserDetail_05 - Step 03. Input first name");
		userLoginsPage.inputUserFirstName(firstName);
		
		log.info("UserDetail_05 - Step 04. Input last name");
		userLoginsPage.inputUserLastName(lastName);
		
		log.info("UserDetail_05 - Step 05. Input login ID");
		userLoginsPage.inputUserLoginID(loginId);
		
		log.info("UserDetail_05 - Step 06. Input Password");
		userLoginsPage.inputPassword("");
		
		log.info("UserDetail_05 - Step 07. Retype Password");
		userLoginsPage.retypePassword("");
		
		log.info("UserDetail_05 - Step 08. Select User Role (Member)");
		userLoginsPage.selectUserRole(userRole);
		
		log.info("UserDetail_05 - Step 09. Input email");
		userLoginsPage.inputEmail(userEmail);
		
		log.info("UserDetail_05 - Step 10. Select Loan Application");
		userLoginsPage.selectLoanApplication(loanApplication);
		
		log.info("UserDetail_05 - Step 11. Click Save button to save New User Info");
		userLoginsPage.clickSaveUserDetailButton();		
		
		log.info("VP: Error message 'Please correct the errors and try again' displays");
		verifyTrue(userLoginsPage.isErrorMessageWithContentDisplay("Please correct the errors and try again"));
		
		log.info("VP: Error message 'Required Field' displays for Password field");
		verifyTrue(userLoginsPage.isErrorMessageForPasswordDisplay("Required Field"));
		
		log.info("VP: Error message 'Required Field' displays for Retype Password field");
		verifyTrue(userLoginsPage.isErrorMessageForRetypePasswordDisplay("Required Field"));
	}
	
	@Test(groups = { "regression" },description = "Create New Member With Password And Retype Password Not Match")
	public void UserDetail_06_CreateNewMemberWithPasswordAndRetypePasswordNotMatch()
	{
		log.info("UserDetail_06 - Step 01. Go to List of user");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("UserDetail_06 - Step 02. Click new button to create new user");
		userLoginsPage.clickNewUserButton();
		
		log.info("UserDetail_06 - Step 03. Input first name");
		userLoginsPage.inputUserFirstName(firstName);
		
		log.info("UserDetail_06 - Step 04. Input last name");
		userLoginsPage.inputUserLastName(lastName);
		
		log.info("UserDetail_06 - Step 05. Input login ID");
		userLoginsPage.inputUserLoginID(loginId);
		
		log.info("UserDetail_06 - Step 06. Input Password");
		userLoginsPage.inputPassword(password);
		
		log.info("UserDetail_06 - Step 07. Retype Password");
		userLoginsPage.retypePassword(newPassword);
		
		log.info("UserDetail_06 - Step 08. Select User Role (Member)");
		userLoginsPage.selectUserRole(userRole);
		
		log.info("UserDetail_06 - Step 09. Input email");
		userLoginsPage.inputEmail(userEmail);
		
		log.info("UserDetail_06 - Step 10. Select Loan Application");
		userLoginsPage.selectLoanApplication(loanApplication);
		
		log.info("UserDetail_06 - Step 11. Click Save button to save New User Info");
		userLoginsPage.clickSaveUserDetailButton();	
		
		log.info("VP: Error message 'Please correct the errors and try again' displays");
		verifyTrue(userLoginsPage.isErrorMessageWithContentDisplay("Please correct the errors and try again"));
		
		log.info("VP: Error message 'New Passwords must match each other' displays for Password field");
		verifyTrue(userLoginsPage.isErrorMessageForPasswordDisplay("New Passwords must match each other"));
		
		log.info("VP: Error message 'New Passwords must match each other' displays for Retype Password field");
		verifyTrue(userLoginsPage.isErrorMessageForRetypePasswordDisplay("New Passwords must match each other"));
	}
	
	@Test(groups = { "regression" },description = "Create New Member With All valid field")
	public void UserDetail_07_CreateNewMemberWithAllValidField()
	{
		log.info("UserDetail_07 - Step 01. Go to List of user");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("UserDetail_07 - Step 02. Click new button to create new user");
		userLoginsPage.clickNewUserButton();
		
		log.info("UserDetail_07 - Step 03. Input first name");
		userLoginsPage.inputUserFirstName(firstName);
		
		log.info("UserDetail_07 - Step 04. Input last name");
		userLoginsPage.inputUserLastName(lastName);
		
		log.info("UserDetail_07 - Step 05. Input login ID");
		userLoginsPage.inputUserLoginID(loginId);
		
		log.info("UserDetail_07 - Step 06. Input Password");
		userLoginsPage.inputPassword(password);
		
		log.info("UserDetail_07 - Step 07. Retype Password");
		userLoginsPage.retypePassword(password);
		
		log.info("UserDetail_07 - Step 08. Select User Role (Member)");
		userLoginsPage.selectUserRole(userRole);
		
		log.info("UserDetail_07 - Step 09. Input email");
		userLoginsPage.inputEmail(userEmail);
		
		log.info("UserDetail_07 - Step 10. Select Loan Application");
		userLoginsPage.selectLoanApplication(loanApplication);
		
		log.info("UserDetail_07 - Step 11. Click Save button to save New User Info");
		userLoginsPage.clickSaveUserDetailButton();		
		
		log.info("VP: Message 'Record Saved' displays");
		verifyTrue(userLoginsPage.isCreateNewUserSuccessMessageDisplay());
	}
	
	@Test(groups = { "regression" },description = "New user dislays on List of Users")
	public void UserDetail_08_NewUserDisplaysOnListOfUsers()
	{
		log.info("UserDetail_08 - Step 01. Go to List of user");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsAdmin(usernameUser, passwordUser, false);
		userLoginsPage = homePage.openUserLoginsPage(driver, ipClient);
		
		log.info("UserDetail_08 - Step 02. Search New user by Login ID");
		userLoginsPage.searchUserbyId(loginId);
		
		log.info("VP: New user displays in User list");
		verifyTrue(userLoginsPage.isUserDisplay(loginId));
		
		log.info("VP: New user infomation displays correctly");
		verifyTrue(userLoginsPage.isUserInfoDisplayCorrectly(loginId, firstName, lastName, userRole));
	}
	
	@Test(groups = { "regression" },description = "Reset Password for user with blank password")
	public void UserDetail_09_ResetPasswordForUserWithBlankPassword()
	{
		log.info("UserDetail_09 - Step 01. Open user detail page");
		userLoginsPage.openUserDetailPage(loginId);
		
		log.info("UserDetail_09 - Step 02. Reset password for the user");
		userLoginsPage.resetPassword("");
		
		log.info("VP: Error message 'Please correct errors marked below' displays");
		verifyTrue(userLoginsPage.isErrorMessageResetPasswordWithContentDisplay("Please correct errors marked below"));
		
		log.info("VP: Error message 'New password is required' displays for Password field");
		verifyTrue(userLoginsPage.isErrorMessageResetPasswordForPasswordDisplay("New password is required"));
		
		log.info("VP: Error message 'Retype new password' displays for Retype Password field");
		verifyTrue(userLoginsPage.isErrorMessageResetPasswordForRetypePasswordDisplay("Retype new password"));
	}
	
	@Test(groups = { "regression" },description = "Reset Password for user with not match password")
	public void UserDetail_10_ResetPasswordForUserWithNotMatchPassword()
	{
		log.info("UserDetail_10 - Step 01. Input new password");
		userLoginsPage.inputNewPasswordForResetPassword(password);
		
		log.info("UserDetail_10 - Step 02. Input retype new password");
		userLoginsPage.inputRetypeNewPasswordForResetPassword(newPassword);
		
		log.info("UserDetail_10 - Step 03. Input retype new password");
		userLoginsPage.clickSaveUserDetailButton();
		
		log.info("VP: Error message 'Please correct the errors and try again' displays");
		verifyTrue(userLoginsPage.isErrorMessageResetPasswordWithContentDisplay("Please correct the errors and try again"));
		
		log.info("VP: Error message 'New Passwords must match each other' displays for Password field");
		verifyTrue(userLoginsPage.isErrorMessageResetPasswordForPasswordDisplay("New Passwords must match each other"));
		
		log.info("VP: Error message 'New Passwords must match each other' displays for Retype Password field");
		verifyTrue(userLoginsPage.isErrorMessageResetPasswordForRetypePasswordDisplay("New Passwords must match each other"));
	}
	
	@Test(groups = { "regression" },description = "Reset Password for user with all valid field")
	public void UserDetail_11_ResetPasswordForUserWithAllValidField()
	{
		log.info("UserDetail_11 - Step 01. Input new password");
		userLoginsPage.inputNewPasswordForResetPassword(newPassword);
		
		log.info("UserDetail_11 - Step 02. Input retype new password");
		userLoginsPage.inputRetypeNewPasswordForResetPassword(newPassword);
		
		log.info("UserDetail_11 - Step 03. Input retype new password");
		userLoginsPage.clickSaveUserDetailButton();
		
		log.info("VP: User page detail displays - Reset password button displays");
		verifyTrue(userLoginsPage.isResetPasswordButtonDisplay());
	}
	
	@Test(groups = { "regression" },description = "Login with user after reset password")
	public void UserDetail_12_LoginUserAfterResetPassword()
	{
		log.info("UserDetail_12 - Step 01. Login new user with new password");
		loginPage = logout(driver, ipClient);
		termsOfUsePage = loginPage.loginAsNewUser(loginId, newPassword, false);
		
		log.info("VP: New user logins successfully");
		verifyTrue(termsOfUsePage.isTermsOfUseTitleDisplay());
		
		log.info("VP: Username displays in left top corner");
		verifyTrue(termsOfUsePage.isUsernameDisplayTopLeftCorner("PreciseRES"));
		
		log.info("VP: 'Logged in as Username' displays in right top corner");
		verifyTrue(termsOfUsePage.isLoggedAsUsernameDisplayRightLeftCorner(firstName+" "+lastName));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UserLoginsPage userLoginsPage;
	private TermsOfUsePage termsOfUsePage;
	private String usernameUser, passwordUser, loginId, newPassword;
	private String accountNumber, loanApplication, password;
	private String userRole, firstName, lastName, userEmail;
}