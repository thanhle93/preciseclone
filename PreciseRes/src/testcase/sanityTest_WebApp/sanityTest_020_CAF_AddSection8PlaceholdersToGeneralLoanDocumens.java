package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.DocumentsPage;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_020_CAF_AddSection8PlaceholdersToGeneralLoanDocumens extends
		AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_CAF;
		applicantName = "Udi Dorner";
		propertiesFileName = "CAF Required.xlsx";
		documentType1 = "Section 8 HAP Contract/ Lease";
		documentType2 = "Section 8 Data Tape";
		documentType3 = "Section 8 Questionnaire";
		documentSectionSearch = "Operations";
		address = "504 W Euclid";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}

	@Test(groups = { "regression" }, description = "Document24 - Add section 8 placeholder to general loan documents")
	public void DocumentTest_24_AddSection8PlaceholderToGeneralLoanDocuments() {
		
		log.info("DocumentTest_24 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender,false);

		log.info("DocumentTest_24 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_24 - Step 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus);
		
		log.info("DocumentTest_24 - Step 04. Open Properties tab");
		loansPage.openPropertiesTab();
		
		log.info("DocumentTest_24 - Step 05. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("DocumentTest_24 - Step 06. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address);

		log.info("DocumentTest_24 - Step 07. Select 'Section 8' Combobox");
		propertiesPage.selectSection8("Yes");

		log.info("DocumentTest_24 - Step 08. Click Save button");
		propertiesPage.clickSaveButton();

		log.info("VP: Added 'Section 8 HAP Contract/ Lease'");
		verifyTrue(propertiesPage.isDocumentTypeOnPropertiesDetailLoaded(documentType1));

		log.info("DocumentTest_24 - Step 09. Open General Loan Documents tab");
		documentsPage = propertiesPage.openGeneralDocumentTab();
		
		log.info("DocumentTest_24 - Step 10. Click 'Section' search");
		documentsPage.searchSectionAndDocumentType(documentSectionSearch, "");

		log.info("VP: Document is loaded to loan item");
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType2));
		verifyTrue(documentsPage.isNoDocumentUploadedToDocumentType(documentType3));

		log.info("DocumentTest_24 - Step 11. Click on logged user name on the top right corner");
//		documentsPage.clickOnLoggedUserName(driver);
//		uploaderPageUrl = documentsPage.getUploaderPageUrl(driver);
		
		log.info("DocumentTest_24 - Step 12. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("DocumentTest_24 - Step 13. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameLender, passwordLender);
		
		log.info("DocumentTest_24 - Step 14. Select loans item");
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
			
		log.info("DocumentTest_24 - Step 15. Select folder General Loan Documents ");
		uploaderPage.selectSubFolderLevel2("General Loan Documents");
		
		log.info("DocumentTest_24 - Step 16. Select folder General Loan Documents ");
		uploaderPage.selectSubFolderLevel3("Operations");
		
		log.info("VP: The document type display correctly");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType2));
		
		log.info("VP: The document type display correctly");
		verifyTrue(uploaderPage.isDocumentDisplay(documentType3));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private DocumentsPage documentsPage;
	private PropertiesPage propertiesPage;
	private UploaderLoginPage uploaderLoginPage;
	private UploaderPage uploaderPage;
	private String usernameLender, passwordLender, uploaderPageUrl;
	private String loanName, accountNumber, applicantName, loanStatus;
	private String documentType1, documentType2, documentType3, documentSectionSearch, propertiesFileName, address;
}