package page;

import org.openqa.selenium.WebDriver;

import PreciseRes.Interfaces;

public class SalesForceHomePage extends AbstractPage {

	public SalesForceHomePage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
	}
	
	/**
	 * check SalesForce Home page display
	 * @param N/A
	 */
	public boolean isSalesForceHomePageDisplay() {
		waitForControl(driver, Interfaces.SalesForceHomePage.CURRENT_STATUS_USERNAME, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceHomePage.CURRENT_STATUS_USERNAME);
	}
	
	private WebDriver driver;
}