package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_028_CAF_EditNameWithSpecialCharacters extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		accountNumber = getUniqueNumber();
		specialCharacter = "-�-,.!@#$%^&*()[]{}/?<>";
		loanName = accountNumber + specialCharacter;
		loanStatus = Constant.LOAN_STATUS_CAF;
		address1 = "504 Euclid -�-,.!@#$%^&*()[]{}/?<>";
		propertiesFileName = "CAF SpecialCharacters.xlsx";
		documentType = "Lease - " + specialCharacter;
		documentFileName = "datatest.pdf";
	}
	
	@Test(groups = { "regression" },description = "SpecialCharacters01 - Create new loan with special characters")
	public void SpecialCharacters_01_CreateNewLoanWithSpecialCharacters()
	{
		log.info("SpecialCharacters_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("SpecialCharacters_01 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);
		
		log.info("SpecialCharacters_01 - Step 03. Create new loan");
		loansPage.createNewLoan(loanName, "", "", loanStatus);
		
		log.info("VP: Verify that Loan display correctly");
		verifyTrue(loansPage.isLoansDisplayWithCorrectInfo(loanName, loanStatus));
	}
	
	@Test(groups = { "regression" },description = "SpecialCharacters02 - Upload data tape with special characters")
	public void SpecialCharacters_02_UploadDatatapeWithSpecialCharacters()
	{
		log.info("SpecialCharacters_02 - Step 01. Open Properties tab");
		loansPage.openPropertiesTab();
		
		log.info("SpecialCharacters_02 - Step 02. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("VP: Information of property is correct");
		verifyTrue(loansPage.isPropertyInfoCorrectly(address1, "Tampa", "FL", "33602", "SFR"));
	}
	
	@Test(groups = { "regression" },description = "SpecialCharacters03 - Add a document with special characters to a property")
	public void SpecialCharacters_03_AddNewDocumentTypeWithSpecialCharacters()
	{
		log.info("SpecialCharacters_03 - Step 01. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("SpecialCharacters_03 - Step 02. Click New document button");
		propertiesPage.clickNewDocumentButton();
		
		log.info("SpecialCharacters_03 - Step 03. Type into 'Other Document Type' textbox");
		propertiesPage.enterDocumentType(documentType);
		
		log.info("SpecialCharacters_03 - Step 04. Upload document file");
		propertiesPage.uploadDocumentFile(documentFileName);
		
		log.info("SpecialCharacters_03 - Step 05. Uncheck 'Private' checkbox");
		propertiesPage.uncheckPrivateCheckbox();
		
		log.info("SpecialCharacters_03 - Step 06. Click 'Save' button");
		propertiesPage.clickSaveButton();
		
		log.info("SpecialCharacters_03 - Step 07. Click 'Go To Property' button");
		propertiesPage.clickGoToPropertyButton();
		
		log.info("VP: Document is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType, documentFileName));
	}
		
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private String specialCharacter;
	private String usernameLender, passwordLender, address1, documentFileName;
	private String loanName, accountNumber, propertiesFileName, loanStatus;
	private String documentType;
}