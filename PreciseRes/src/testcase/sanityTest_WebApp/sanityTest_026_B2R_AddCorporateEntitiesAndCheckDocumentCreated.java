package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.DocumentsPage;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_026_B2R_AddCorporateEntitiesAndCheckDocumentCreated extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		applicantUsername1 = Constant.USERNAME_B2R_BORROWER;
		applicantPassword1 = Constant.PASSWORD_B2R_BORROWER;
		applicantUsername2 = Constant.USERNAME_B2R_BORROWER_2;
		applicantPassword2 = Constant.PASSWORD_B2R_BORROWER_2;
		memberUsername = "auto-B2R-member2014";
		memberPassword = "change";
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantSearch = "Applicant";
		borrowerSearch = "Borrower (SPE)";
		pledgorSearch = "Pledgor";
		sponsorSearch = "Sponsor";
		guarantorSearch = "Guarantor";
		applicantName1 = "Consolidation Demo Borrower";
		applicantName2 = "Consolidation Demo Borrower Second";
		documentType0 = "L2 - Executed Borrower Term Sheet";
		borrowerName1 = "UdiTeamBorrower";
		borrowerName2 = "UdiTeamBorrower2";
		documentType1 = "L1 - Loan Application";
		guarantorName1 = "UdiTeamGuarantor";
		guarantorName2 = "UdiTeamGuarantor2";
		documentType2 = "L17 - Personal Financial Statement";
		sponsorName1 = "UdiTeamSponsor";
		sponsorName2 = "UdiTeamSponsor2";
		documentType3 = "Placeholder for Sponsor 01";
		pledgorName1 = "UdiTeamPledgor";
		pledgorName2 = "UdiTeamPledgor2";
		documentType4 = "L4 - Credit Report";
	}

	@Test(groups = { "regression" }, description = "Add Corporation 01 - Add Corporate Entities from New Loan")
	public void AddCorporate_01_AddCorporateEntitiesFromNewLoan() {
		log.info("AddCorporate_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("AddCorporate_01 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("AddCorporate_01 - Step 03. Create new loan");
		loansPage.createNewLoanFull(loanName, loanName, loanStatus, applicantName1, 
				borrowerName1, guarantorName1, sponsorName1, pledgorName1);
		
		log.info("VP: Loan item display with correct info");
		verifyTrue(loansPage.isLoansReadOnlyDisplayWithCorrectInfo(loanName, loanStatus));
		
		log.info("VP: Applicant displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(applicantName1));
		
		log.info("VP: The Borrower displays on Loan Application Entities table");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(borrowerName1));
		
		log.info("VP: Pledgor displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(pledgorName1));
		
		log.info("VP: Sponsor displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(sponsorName1));
		
		log.info("VP: The Guarantor displays on Loan Application Entities table");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(guarantorName1));
		
		log.info("AddCorporate_01 - Step 04. Add member to loan team");
		loansPage.addMemberInBasicDetailTeam(memberUsername);
		
		log.info("VP: The Member displays on Team table");
		verifyTrue(loansPage.isSaveSuccessMessageDisplay());
		
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			log.info("AddCorporate_01. Open Loans tab");
			loansPage.openLoansPage(driver, ipClient);

			log.info("AddCorporate_01. Search loan item");
			loansPage.searchLoanByName(loanName);

			log.info("AddCorporate_01. Open detail loan item");
			loansPage.openLoansDetailPage(loanName);
		}
		
		verifyTrue(loansPage.isMemberDisplayOnTeamTable(memberUsername));
		
		log.info("AddCorporate_01 - Step 05. Open Corporate Entity Documents tab");
		documentsPage = loansPage.openCorporateEntityDocumentsTab();
		
		log.info("AddCorporate_01 - Step 06. Select Applicant section and click Search button");
		documentsPage.searchSectionAndDocumentType(applicantSearch, documentType0);
		
		log.info("VP: Lender can see 'L2 - Executed Borrower Term Sheet' document of Applicant");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(applicantSearch + " - " + applicantName1, documentType0));
		
		log.info("AddCorporate_01 - Step 07. Select Borrower section and click Search button");
		documentsPage.searchSectionAndDocumentType(borrowerSearch, documentType1);
		
		log.info("VP: Lender can see 'L1 - Loan Application' document of Borrower");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(borrowerSearch + " - " + borrowerName1, documentType1));
		
		log.info("AddCorporate_01 - Step 08. Select Gurantor section and click Search button");
		documentsPage.searchSectionAndDocumentType(guarantorSearch, documentType2);
		
		log.info("VP: Lender can see 'L17 - Personal Financial Statement' document of Gurantor");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(guarantorSearch + " - " + guarantorName1, documentType2));
		
		log.info("AddCorporate_01 - Step 09. Select Sponsor section and click Search button");
		documentsPage.searchSectionAndDocumentType(sponsorSearch, documentType3);
		
		log.info("VP: Lender can see 'Placeholder for Sponsor 01' document of Sponsor");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(sponsorSearch + " - " + sponsorName1, documentType3));
		
		log.info("AddCorporate_01 - Step 10. Select Pledgor section and click Search button");
		documentsPage.searchSectionAndDocumentType(pledgorSearch, documentType4);
		
		log.info("VP: Lender can see 'L4 - Credit Report' document of Pledgor");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(pledgorSearch + " - " + pledgorName1, documentType4));
		
		log.info("AddCorporate_01 - Step 11. Log out and login with applicant first");
		loginPage = logout(driver, ipClient);
		loansPage = loginPage.loginAsBorrower(applicantUsername1, applicantPassword1, false);

		log.info("AddCorporate_01 - Step 12. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("AddCorporate_01 - Step 13. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("AddCorporate_01 - Step 14. Open Corporate Entity Documents tab");
		documentsPage = loansPage.openCorporateEntityDocumentsTab();
		
		log.info("AddCorporate_01 - Step 15. Select Applicant section and click Search button");
		documentsPage.searchSectionAndDocumentType(applicantSearch, documentType0);
		
		log.info("VP: Lender can see 'L2 - Executed Borrower Term Sheet' document of Applicant");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(applicantSearch + " - " + applicantName1, documentType0));
		
		log.info("AddCorporate_01 - Step 16. Select Borrower section and click Search button");
		documentsPage.searchSectionAndDocumentType(borrowerSearch, documentType1);
		
		log.info("VP: Lender can see 'L1 - Loan Application' document of Borrower");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(borrowerSearch + " - " + borrowerName1, documentType1));
		
		log.info("AddCorporate_01 - Step 17. Select Gurantor section and click Search button");
		documentsPage.searchSectionAndDocumentType(guarantorSearch, documentType2);
		
		log.info("VP: Lender can see 'L17 - Personal Financial Statement' document of Gurantor");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(guarantorSearch + " - " + guarantorName1, documentType2));
		
		log.info("AddCorporate_01 - Step 18. Select Sponsor section and click Search button");
		documentsPage.searchSectionAndDocumentType(sponsorSearch, documentType3);
		
		log.info("VP: Lender can see 'Placeholder for Sponsor 01' document of Sponsor");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(sponsorSearch + " - " + sponsorName1, documentType3));
		
		log.info("AddCorporate_01 - Step 19. Select Pledgor section and click Search button");
		documentsPage.searchSectionAndDocumentType(pledgorSearch, documentType4);
		
		log.info("VP: Lender can see 'L4 - Credit Report' document of Pledgor");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(pledgorSearch + " - " + pledgorName1, documentType4));
		
		log.info("AddCorporate_01 - Step 20. Log out and login with Member");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(memberUsername,	memberPassword, false);

		log.info("AddCorporate_01 - Step 21. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("AddCorporate_01 - Step 22. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("AddCorporate_01 - Step 23. Open loan detail");
		loansPage.openLoansDetailPage(loanName);
		
		log.info("VP: Applicant displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(applicantName1));
		
		log.info("VP: The Borrower displays on Loan Application Entities table");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(borrowerName1));
		
		log.info("VP: Pledgor displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(pledgorName1));
		
		log.info("VP: Sponsor displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(sponsorName1));
		
		log.info("VP: The Guarantor displays on Loan Application Entities table");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(guarantorName1));
		
		log.info("AddCorporate_01 - Step 24. Open Corporate Entity Documents tab");
		documentsPage = loansPage.openCorporateEntityDocumentsTab();
		
		log.info("AddCorporate_01 - Step 25. Select Applicant section and click Search button");
		documentsPage.searchSectionAndDocumentType(applicantSearch, documentType0);
		
		log.info("VP: Lender can see 'L2 - Executed Borrower Term Sheet' document of Applicant");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(applicantSearch + " - " + applicantName1, documentType0));
		
		log.info("AddCorporate_01 - Step 26. Select Borrower section and click Search button");
		documentsPage.searchSectionAndDocumentType(borrowerSearch, documentType1);
		
		log.info("VP: Lender can see 'L1 - Loan Application' document of Borrower");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(borrowerSearch + " - " + borrowerName1, documentType1));
		
		log.info("AddCorporate_01 - Step 27. Select Gurantor section and click Search button");
		documentsPage.searchSectionAndDocumentType(guarantorSearch, documentType2);
		
		log.info("VP: Lender can see 'L17 - Personal Financial Statement' document of Gurantor");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(guarantorSearch + " - " + guarantorName1, documentType2));
		
		log.info("AddCorporate_01 - Step 28. Select Sponsor section and click Search button");
		documentsPage.searchSectionAndDocumentType(sponsorSearch, documentType3);
		
		log.info("VP: Lender can see 'Placeholder for Sponsor 01' document of Sponsor");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(sponsorSearch + " - " + sponsorName1, documentType3));
		
		log.info("AddCorporate_01 - Step 29. Select Pledgor section and click Search button");
		documentsPage.searchSectionAndDocumentType(pledgorSearch, documentType4);
		
		log.info("VP: Lender can see 'L4 - Credit Report' document of Pledgor");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(pledgorSearch + " - " + pledgorName1, documentType4));
	}

	@Test(groups = { "regression" }, description = "Add Corporation 02 - Add Corporate Entities from Basic Detail tab")
	public void AddCorporate_02_AddCorporateEntitiesFromBasicDetailTab() {

		log.info("AddCorporate_02 - Step 01. Login with lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("AddCorporate_02 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("AddCorporate_02 - Step 03. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("AddCorporate_02 - Step 04. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("AddCorporate_02 - Step 05. Click new Loan Application Entity");
		loansPage.clickNewLoanApplicationEntity();
		
		log.info("AddCorporate_02 - Step 06. Select Applicant in Entity type dropdown list");
		loansPage.selectEntityType("Applicant");
		
		log.info("AddCorporate_02 - Step 07. Select Applicant in Applicant dropdown list");
		loansPage.selectApplicantType(applicantName2);
		loansPage.clickSaveLoansApplicantEntityButton();
		loansPage.clickBackButton();
		
		log.info("VP: The Applicant displays on Loan Application Entities table");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(applicantName2));
		
		log.info("AddCorporate_02 - Step 08. Click new Loan Application Entity");
		loansPage.clickNewLoanApplicationEntity();
		
		log.info("AddCorporate_02 - Step 09. Select Borrower in Entity type dropdown list");
		loansPage.selectEntityType("Borrower");
		
		log.info("AddCorporate_02 - Step 10. Select Borrower in Borrower dropdown list");
		loansPage.selectBorrowerType(borrowerName2);
		loansPage.clickSaveLoansApplicantEntityButton();
		loansPage.clickBackButton();
		
		log.info("VP: The Borrower displays on Loan Application Entities table");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(borrowerName2));
		
		log.info("AddCorporate_02 - Step 11. Click new Loan Application Entity");
		loansPage.clickNewLoanApplicationEntity();
		
		log.info("AddCorporate_02 - Step 12. Select Guarantor in Entity type dropdown list");
		loansPage.selectEntityType("Guarantor");
		
		log.info("AddCorporate_02 - Step 13. Select Guarantor in Guarantor dropdown list");
		loansPage.selectGuarantorType(guarantorName2);
		loansPage.clickSaveLoansApplicantEntityButton();
		loansPage.clickBackButton();
		
		log.info("VP: The Guarantor displays on Loan Application Entities table");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(guarantorName2));
		
		log.info("AddCorporate_02 - Step 14. Click new Loan Application Entity");
		loansPage.clickNewLoanApplicationEntity();
		
		log.info("AddCorporate_02 - Step 15. Select Sponsor in Entity type dropdown list");
		loansPage.selectEntityType("Sponsor");
		
		log.info("AddCorporate_02 - Step 16. Select Sponsor in Sponsor dropdown list");
		loansPage.selectSponsorType(sponsorName2);
		loansPage.clickSaveLoansApplicantEntityButton();
		loansPage.clickBackButton();
		
		log.info("VP: The Sponsor displays on Loan Application Entities table");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(sponsorName2));
		
		log.info("AddCorporate_02 - Step 17. Click new Loan Application Entity");
		loansPage.clickNewLoanApplicationEntity();
		
		log.info("AddCorporate_02 - Step 18. Select Pledgor in Entity type dropdown list");
		loansPage.selectEntityType("Pledgor");
		
		log.info("AddCorporate_02 - Step 19. Select Pledgor in Pledgor dropdown list");
		loansPage.selectPledgorType(pledgorName2);
		loansPage.clickSaveLoansApplicantEntityButton();
		loansPage.clickBackButton();
		
		log.info("VP: The Pledgor displays on Loan Application Entities table");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(pledgorName2));
		
		log.info("AddCorporate_02 - Step 20. Open Corporate Entity Documents tab");
		documentsPage = loansPage.openCorporateEntityDocumentsTab();

		log.info("AddCorporate_02 - Step 21. Select Applicant section and click Search button");
		documentsPage.searchSectionAndDocumentType(applicantSearch, documentType0);
		
		log.info("VP: Lender can see 'L2 - Executed Borrower Term Sheet' document of Applicant");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(applicantSearch + " - " + applicantName2, documentType0));
		
		log.info("AddCorporate_02 - Step 22. Select Borrower section and click Search button");
		documentsPage.searchSectionAndDocumentType(borrowerSearch, documentType1);
		
		log.info("VP: Lender can see 'L1 - Loan Application' document of Borrower");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(borrowerSearch + " - " + borrowerName2, documentType1));
		
		log.info("AddCorporate_02 - Step 23. Select Gurantor section and click Search button");
		documentsPage.searchSectionAndDocumentType(guarantorSearch, documentType2);
		
		log.info("VP: Lender can see 'L17 - Personal Financial Statement' document of Gurantor");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(guarantorSearch + " - " + guarantorName2, documentType2));
		
		log.info("AddCorporate_02 - Step 24. Select Sponsor section and click Search button");
		documentsPage.searchSectionAndDocumentType(sponsorSearch, documentType3);
		
		log.info("VP: Lender can see 'Placeholder for Sponsor 01' document of Sponsor");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(sponsorSearch + " - " + sponsorName2, documentType3));
		
		log.info("AddCorporate_02 - Step 25. Select Pledgor section and click Search button");
		documentsPage.searchSectionAndDocumentType(pledgorSearch, documentType4);
		
		log.info("VP: Lender can see 'L4 - Credit Report' document of Pledgor");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(pledgorSearch + " - " + pledgorName2, documentType4));
		
		log.info("AddCorporate_02 - Step 26. Log out and login with applicant second");
		loginPage = logout(driver, ipClient);
		loansPage = loginPage.loginAsBorrower(applicantUsername2, applicantPassword2, false);

		log.info("AddCorporate_02 - Step 27. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("AddCorporate_02 - Step 28. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("AddCorporate_02 - Step 29. Open Corporate Entity Documents tab");
		documentsPage = loansPage.openCorporateEntityDocumentsTab();
		
		log.info("AddCorporate_02 - Step 30. Select Applicant section and click Search button");
		documentsPage.searchSectionAndDocumentType(applicantSearch, documentType0);
		
		log.info("VP: Lender can see 'L2 - Executed Borrower Term Sheet' document of Applicant");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(applicantSearch + " - " + applicantName2, documentType0));
		
		log.info("AddCorporate_02 - Step 31. Select Borrower section and click Search button");
		documentsPage.searchSectionAndDocumentType(borrowerSearch, documentType1);
		
		log.info("VP: Lender can see 'L1 - Loan Application' document of Borrower");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(borrowerSearch + " - " + borrowerName2, documentType1));
		
		log.info("AddCorporate_02 - Step 32. Select Gurantor section and click Search button");
		documentsPage.searchSectionAndDocumentType(guarantorSearch, documentType2);
		
		log.info("VP: Lender can see 'L17 - Personal Financial Statement' document of Gurantor");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(guarantorSearch + " - " + guarantorName2, documentType2));
		
		log.info("AddCorporate_02 - Step 33. Select Sponsor section and click Search button");
		documentsPage.searchSectionAndDocumentType(sponsorSearch, documentType3);
		
		log.info("VP: Lender can see 'Placeholder for Sponsor 01' document of Sponsor");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(sponsorSearch + " - " + sponsorName2, documentType3));
		
		log.info("AddCorporate_02 - Step 34. Select Pledgor section and click Search button");
		documentsPage.searchSectionAndDocumentType(pledgorSearch, documentType4);
		
		log.info("VP: Lender can see 'L4 - Credit Report' document of Pledgor");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(pledgorSearch + " - " + pledgorName2, documentType4));
		
		log.info("AddCorporate_02 - Step 35. Log out and login with Member");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(memberUsername,	memberPassword, false);

		log.info("AddCorporate_02 - Step 36. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("AddCorporate_02 - Step 37. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("AddCorporate_02 - Step 38. Open loan detail");
		loansPage.openLoansDetailPage(loanName);
		
		log.info("VP: Applicant displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(applicantName2));
		
		log.info("VP: The Borrower displays on Loan Application Entities table");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(borrowerName2));
		
		log.info("VP: Pledgor displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(pledgorName2));
		
		log.info("VP: Sponsor displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(sponsorName2));
		
		log.info("VP: The Guarantor displays on Loan Application Entities table");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(guarantorName2));

		log.info("AddCorporate_02 - Step 39. Open Corporate Entity Documents tab");
		documentsPage = loansPage.openCorporateEntityDocumentsTab();
		
		log.info("AddCorporate_02 - Step 40. Select Applicant section and click Search button");
		documentsPage.searchSectionAndDocumentType(applicantSearch, documentType0);
		
		log.info("VP: Lender can see 'L2 - Executed Borrower Term Sheet' document of Applicant");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(applicantSearch + " - " + applicantName2, documentType0));
		
		log.info("AddCorporate_02 - Step 41. Select Borrower section and click Search button");
		documentsPage.searchSectionAndDocumentType(borrowerSearch, documentType1);
		
		log.info("VP: Lender can see 'L1 - Loan Application' document of Borrower");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(borrowerSearch + " - " + borrowerName2, documentType1));
		
		log.info("AddCorporate_02 - Step 42. Select Gurantor section and click Search button");
		documentsPage.searchSectionAndDocumentType(guarantorSearch, documentType2);
		
		log.info("VP: Lender can see 'L17 - Personal Financial Statement' document of Gurantor");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(guarantorSearch + " - " + guarantorName2, documentType2));
		
		log.info("AddCorporate_02 - Step 43. Select Sponsor section and click Search button");
		documentsPage.searchSectionAndDocumentType(sponsorSearch, documentType3);
		
		log.info("VP: Lender can see 'Placeholder for Sponsor 01' document of Sponsor");
		verifyTrue(documentsPage.isSectionAndDocumentTypeDisplay(sponsorSearch + " - " + sponsorName2, documentType3));
		
		log.info("AddCorporate_02 - Step 44. Select Pledgor section and click Search button");
		documentsPage.searchSectionAndDocumentType(pledgorSearch, documentType4);
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private DocumentsPage documentsPage;
	private String usernameLender, passwordLender;
	private String memberUsername, memberPassword;
	private String loanName, accountNumber, applicantName1,applicantName2, loanStatus;
	private String applicantUsername1, applicantPassword1, applicantUsername2, applicantPassword2;
	private String documentType0, documentType1, documentType2, documentType3, documentType4, guarantorName1, guarantorName2;
	private String borrowerName1, borrowerName2, sponsorName1, sponsorName2, pledgorName1, pledgorName2;
	private String applicantSearch, borrowerSearch, pledgorSearch, sponsorSearch, guarantorSearch;
}