package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFLoansPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_021_FNF_SearchForLoans extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		loanName = "Edit Loan Auto " + getUniqueNumber();
		underwriter = "CAF FNF Lender";
		validDate = "2/12/2014";
		validNumber ="10";
		originator = "CAF FNF Lender";
	}

	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search loans by Loan name")
	public void SearchForLoans_01_Lender_SearchByLoansName() {
		log.info("SearchForLoans_01 - Step 01. Login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);

		log.info("SearchForLoans_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("SearchForLoans_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("SearchForLoans_01 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("SearchForLoans_01 - Step 05: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("SearchForLoans_01 - Step 06: Input loan name");
		loanPage.inputRequiredData(loanName, validDate);
		
		log.info("SearchForLoans_01 - Step 07: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("SearchForLoans_01 - Step 07: Input data for loan searching");
		loanPage.inputDataForLoanSearching(originator, underwriter, validNumber, validDate);
		
		log.info("SearchForLoans_01 - Step 07: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("SearchForLoans_01 - Step 07: Click Back to loan list button");
		loanPage.clickBackToListButton();
		
		log.info("SearchForLoans_01 - Step 07: Click on Search button");
		loanPage.clickOnSearchButton(loanName);
		
		log.info("SearchForLoans_01 - Step 07: Get data for Loans searching");
		status = loanPage.getRecordValue("Status");
		productType = loanPage.getRecordValue("Product Type");
		
		log.info("VP: Loans information displayed correctly in Opportunity table");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(companyName, status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search loans by Borrower name")
	public void SearchForLoans_02_Lender_SearchByBorrowerName() {

		log.info("SearchForLoans_02 - Step 01: Go to 'Loans' page");
		loanPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_02 - Step 02: Select Borrower and click on Search button");
		loanPage.selectBorrowerForSearch(companyName);
		loanPage.clickOnSearchButton(loanName);
		
		log.info("VP: Loans information displayed correctly");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(companyName, status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search loans by Status")
	public void SearchForLoans_03_Lender_SearchByStatus() {

		log.info("SearchForLoans_03 - Step 01: Go to 'Loans' page");
		loanPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_03 - Step 02: Select Status and click on Search button");
		loanPage.selectStatusForSearch(status);
		loanPage.clickOnSearchButton(loanName);
		
		log.info("VP: Loans information displayed correctly");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(companyName, status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search loans by Product Type")
	public void SearchForLoans_04_Lender_SearchByProductType() {

		log.info("SearchForLoans_04 - Step 01: Go to 'Loans' page");
		loanPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_04 - Step 02: Select Product Type and click on Search button");
		loanPage.selectProductTypeForSearch(productType);
		loanPage.clickOnSearchButton(loanName);
		
		log.info("VP: Loans information displayed correctly");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(companyName, status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search loans by Originator")
	public void SearchForLoans_05_Lender_SearchByOriginator() {

		log.info("SearchForLoans_05 - Step 01: Go to 'Loans' page");
		loanPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_05 - Step 02: Select Originator and click on Search button");
		loanPage.selectOriginatorForSearch(originator);
		loanPage.clickOnSearchButton(loanName);
		
		log.info("VP: Loans information displayed correctly");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(companyName, status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search loans by Underwriter  ")
	public void SearchForLoans_06_Lender_SearchByUnderwriter  () {

		log.info("SearchForLoans_06 - Step 01: Go to 'Loans' page");
		loanPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_06 - Step 02: Select Underwriter  and click on Search button");
		loanPage.selectUnderwriterForSearch(underwriter);
		loanPage.clickOnSearchButton(loanName);
		
		log.info("VP: Loans information displayed correctly");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(companyName, status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search loans by CAF Draw Fee%")
	public void SearchForLoans_07_Lender_SearchByCAFDrawFee() {

		log.info("SearchForLoans_07 - Step 01: Go to 'Loans' page");
		loanPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_07 - Step 02: Select CAF Draw Fee% and click on Search button");
		loanPage.inputCAFDrawFeeForSearch(validNumber);
		loanPage.clickOnSearchButton(loanName);
		
		log.info("VP: Loans information displayed correctly");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(companyName, status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search loans by Current Close Date")
	public void SearchForLoans_08_Lender_SearchByCurrentCloseDate() {

		log.info("SearchForLoans_08 - Step 01: Go to 'Loans' page");
		loanPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_08 - Step 02: Select Current Close Date and click on Search button");
		loanPage.selectCurrentCloseDateForSearch(validDate);
		loanPage.clickOnSearchButton(loanName);
		
		log.info("VP: Loans information displayed correctly");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(companyName, status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search loans by Expiration Date")
	public void SearchForLoans_09_Lender_SearchByExpirationDate() {

		log.info("SearchForLoans_09 - Step 01: Go to 'Loans' page");
		loanPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_09 - Step 02: Select Expiration Date and click on Search button");
		loanPage.selectExpirationDateForSearch(validDate);
		loanPage.clickOnSearchButton(loanName);
		
		log.info("VP: Loans information displayed correctly");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(companyName, status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search loans by Line Maturity Date ")
	public void SearchForLoans_10_Lender_SearchByLineMaturityDate  () {

		log.info("SearchForLoans_10 - Step 01: Go to 'Loans' page");
		loanPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_10 - Step 02: Select Line Maturity Date  and click on Search button");
		loanPage.selectLineMaturityDateForSearch(validDate);
		loanPage.clickOnSearchButton(loanName);
		
		log.info("VP: Loans information displayed correctly");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(companyName, status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search loans by Loan name")
	public void SearchForLoans_11_Borrower_SearchByLoansName() {
		log.info("SearchForLoans_11 - Step 01. Login with Borrower");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);
		
		log.info("SearchForLoans_11 - Step 02: Open Loans page");
		applicantsPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_11 - Step 03: Click on Search button");
		loanPage.clickOnSearchButton(loanName);
		
		log.info("SearchForLoans_11 - Step 04: Get data for Loans searching");
		status = loanPage.getRecordValue("Status");
		productType = loanPage.getRecordValue("Product Type");
		
		log.info("VP: Loans information displayed correctly in Opportunity table");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search loans by Status")
	public void SearchForLoans_12_Borrower_SearchByStatus() {

		log.info("SearchForLoans_12 - Step 01: Go to 'Loans' page");
		loanPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_12 - Step 02: Select Status and click on Search button");
		loanPage.selectStatusForSearch(status);
		loanPage.clickOnSearchButton(loanName);
		
		log.info("VP: Loans information displayed correctly");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search loans by Product Type")
	public void SearchForLoans_13_Borrower_SearchByProductType() {

		log.info("SearchForLoans_13 - Step 01: Go to 'Loans' page");
		loanPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_13 - Step 02: Select Product Type and click on Search button");
		loanPage.selectProductTypeForSearch(productType);
		loanPage.clickOnSearchButton(loanName);
		
		log.info("VP: Loans information displayed correctly");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search loans by Originator")
	public void SearchForLoans_14_Borrower_SearchByOriginator() {

		log.info("SearchForLoans_14 - Step 01: Go to 'Loans' page");
		loanPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_14 - Step 02: Select Originator and click on Search button");
		loanPage.selectOriginatorForSearch(originator);
		loanPage.clickOnSearchButton(loanName);
		
		log.info("VP: Loans information displayed correctly");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search loans by Underwriter  ")
	public void SearchForLoans_15_Borrower_SearchByUnderwriter  () {

		log.info("SearchForLoans_15 - Step 01: Go to 'Loans' page");
		loanPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_15 - Step 02: Select Underwriter  and click on Search button");
		loanPage.selectUnderwriterForSearch(underwriter);
		loanPage.clickOnSearchButton(loanName);
		
		log.info("VP: Loans information displayed correctly");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search loans by CAF Draw Fee%")
	public void SearchForLoans_16_Borrower_SearchByCAFDrawFee() {

		log.info("SearchForLoans_16 - Step 01: Go to 'Loans' page");
		loanPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_16 - Step 02: Select CAF Draw Fee% and click on Search button");
		loanPage.inputCAFDrawFeeForSearch(validNumber);
		loanPage.clickOnSearchButton(loanName);
		
		log.info("VP: Loans information displayed correctly");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search loans by Current Close Date")
	public void SearchForLoans_17_Borrower_SearchByCurrentCloseDate() {

		log.info("SearchForLoans_17 - Step 01: Go to 'Loans' page");
		loanPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_17 - Step 02: Select Current Close Date and click on Search button");
		loanPage.selectCurrentCloseDateForSearch(validDate);
		loanPage.clickOnSearchButton(loanName);
		
		log.info("VP: Loans information displayed correctly");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search loans by Expiration Date")
	public void SearchForLoans_18_Borrower_SearchByExpirationDate() {

		log.info("SearchForLoans_18 - Step 01: Go to 'Loans' page");
		loanPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_18 - Step 02: Select Expiration Date and click on Search button");
		loanPage.selectExpirationDateForSearch(validDate);
		loanPage.clickOnSearchButton(loanName);
		
		log.info("VP: Loans information displayed correctly");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Borrower - Search loans by Line Maturity Date ")
	public void SearchForLoans_19_Borrower_SearchByLineMaturityDate  () {

		log.info("SearchForLoans_19 - Step 01: Go to 'Loans' page");
		loanPage.openLoansPage(driver, ipClient);
	
		log.info("SearchForLoans_19 - Step 02: Select Line Maturity Date  and click on Search button");
		loanPage.selectLineMaturityDateForSearch(validDate);
		loanPage.clickOnSearchButton(loanName);
		
		log.info("VP: Loans information displayed correctly");
		verifyTrue(loanPage.isAllRecordValuesDisplayedCorrectly(status, productType, originator, underwriter, validNumber, validDate));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private FNFLoansPage loanPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername, FNFBorrowerPassword;
	private String accountNumber, loanName, underwriter, originator, status, productType;
	private String companyName;
	private String validDate, validNumber;
	
	}