package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_043_B2R_DocumentsSummary extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
	}

	@Test(groups = { "regression" }, description = "DocumentsSummary 01 - Check Total Required Docs Documents Summary")
	public void DocumentsSummary_01_CheckTotalRequiredDocsDocumentsSummary() {
		log.info("DocumentsSummary_01 - Step 1. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("DocumentsSummary_01 - Step 2. Get random Loans to check");
		loanName = homePage.getRandomLoansName();
		
		log.info("DocumentsSummary_01 - Step 3. Get number of Required docs");
		numberOfRequiredDocs = homePage.getRequiredDocs(loanName);
		
		log.info("DocumentsSummary_01 - Step 4. Get number of Loaded docs");
		numberOfLoadedDocs = homePage.getLoadedDocs(loanName);
		
		log.info("DocumentsSummary_01 - Step 5. Get number of Reviewed docs");
		numberOfReviewedDocs = homePage.getReviewedDocs(loanName);
		
		log.info("DocumentsSummary_01 - Step 6. Get number of Revised docs");
		numberOfRevisedDocs = homePage.getRevisedDocs(loanName);
		
		log.info("DocumentsSummary_01 - Step 7. Click on Documents Summary button");
		homePage.clickOnDocumentsSummaryLink(loanName);
		
		log.info("VP: Check total required docs displayed correclty");
		verifyTrue(homePage.isTotalRequiredDocsDisplayedCorrectly(numberOfRequiredDocs, "1"));
	}
	
	@Test(groups = { "regression" }, description = "DocumentsSummary 02 - Check Total Loaded Docs Documents Summary")
	public void DocumentsSummary_02_CheckTotalRequiredDocsDocumentsSummary() {
		
		log.info("VP: Check total required docs displayed correclty");
		verifyTrue(homePage.isTotalLoadedDocsDisplayedCorrectly(numberOfLoadedDocs, "1"));
	}
	
	@Test(groups = { "regression" }, description = "DocumentsSummary 03 - Check Total Reviewed Docs Documents Summary")
	public void DocumentsSummary_03_CheckTotalReviewedDocsDocumentsSummary() {
		
		log.info("VP: Check total Reviewed docs displayed correclty");
		verifyTrue(homePage.isTotalReviewedDocsDisplayedCorrectly(numberOfReviewedDocs, "1"));
	}
	
	@Test(groups = { "regression" }, description = "DocumentsSummary 04 - Check Total Revised Docs Documents Summary")
	public void DocumentsSummary_04_CheckTotalRequiredDocsDocumentsSummary() {
		
		log.info("VP: Check total Revised docs displayed correclty");
		verifyTrue(homePage.isTotalRevisedDocsDisplayedCorrectly(numberOfRevisedDocs, "1"));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private String usernameLender, passwordLender;
	private String numberOfRequiredDocs, numberOfLoadedDocs, numberOfRevisedDocs, numberOfReviewedDocs;
	private String loanName;
}