package sanityTest_WebApp;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;

import common.AbstractTest;
import common.Constant;

public class sanityTest_038_B2R_ValidateDatatypesBasicDetailPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan"+accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower New";
		invalidCurrency = "$40,0.00*%#$fail";
		invalidDate = "4/21/2015*%#$fail";
		invalidContactNumber = "1234";
		invalidEmail = "abc@gmail,com";
		state = "AA - Federal Services";
		propertyManagament = "In House";
		yearHold = "2-3 Years";
		source = "Proprietary";
		owner = "Kushy,Matthew";
		companyType = "Non Profit Corporation";
		validCurrency = "$2,000,000.00";
		validDate = "12/31/2000";
		validPhoneNumber = "12345678901";
		validZipCode = "12345";
		validText = "Common value !@#";
		validEmail = "abc@gmail.com";
	}

	@Test(groups = { "regression" }, description = "Validate Datatypes 01 - No input the data")
	public void ValidataDatatypes_01_NoInputTheData() {
		
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Precondition 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);
		
		log.info("ValidataDatatypes_01 - Step 01. Input Loan name empty");
		loansPage.inputLoanName("");
		
		log.info("ValidataDatatypes_01 - Step 02. Input Loan ID empty");
		loansPage.inputLoanID("");
		
		log.info("ValidataDatatypes_01 - Step 03. Click save button");
		loansPage.clickSaveButton();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(loansPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Loan Name is required' message is displayed");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_LegalName", "Loan Name is required"));
		
		log.info("VP: 'SF ID is required' message is displayed");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_LoanApplicationID", "SF ID is required"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 02 - Input the invalid/ incorrect at Currency fields")
	public void ValidataDatatypes_02_InputInvalidOrIncorrectAtCurrencyFields() {
		
		log.info("ValidataDatatypes_02 - Step 01. Search for loan");
		loansPage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		
		log.info("ValidataDatatypes_02 - Step 02. Open loan basic detail tab");
		loansPage.openLoansDetailPage(loanName);
		
		log.info("ValidataDatatypes_02 - Step 03. Input Loan Amount Requested");
		loansPage.inputLoanAmountRequested(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 04. Click save button");
		loansPage.clickSaveButton();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(loansPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_LoanAmount", "Currency value must be a number"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 03 - Input the invalid/ incorrect at Date fields")
	public void ValidataDatatypes_03_InputInvalidOrIncorrectAtDateFields() {
		
		log.info("Precondition - Step 01. Search for loan");
		loansPage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		
		log.info("Precondition - Step 02. Open loan basic detail tab");
		loansPage.openLoansDetailPage(loanName);
		
		log.info("ValidataDatatypes_03 - Step 01: Input the invalid data to 'SFR Buying Started' field");
		loansPage.inputSFRBuyingStarted(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 02: Input the invalid data to 'Date Established ");
		loansPage.inputDateEstablished(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 05: Click 'Save' button");
		loansPage.clickSaveButton();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(loansPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'SFR Buying Started' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_SFRBuyingStarted", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Date Established' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_DateEstablish", "Value must be a date"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 04 - Input the invalid/ incorrect at Contact fields")
	public void ValidataDatatypes_04_InputInvalidOrIncorrectAtContactFields() {
		
		log.info("Precondition - Step 01. Search for loan");
		loansPage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		
		log.info("Precondition - Step 02. Open loan basic detail tab");
		loansPage.openLoansDetailPage(loanName);
		
		log.info("ValidataDatatypes_04 - Step 01: Input the invalid data to 'Phone' field");
		loansPage.inputPhoneTextbox(invalidContactNumber);
		
		log.info("ValidataDatatypes_04 - Step 02: Input the invalid data to 'Zip' field");
		loansPage.inputZipcode(invalidContactNumber);
		
		log.info("ValidataDatatypes_04 - Step 03: Input the invalid data to 'Email' field");
		loansPage.inputEmailTextbox(invalidEmail);
		
		log.info("ValidataDatatypes_04 - Step 04: Click 'Save' button");
		loansPage.clickSaveButton();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(loansPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Value must be a whole number' message is displayed at 'Phone' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_Phone", "Not a valid Phone Number."));
		
		log.info("VP: 'Value must be a whole number' message is displayed at 'Zip' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_Zip", "Enter a valid ZIP or ZIP+4 code"));
		
		log.info("VP: 'Value left of % symbol must be a number' message is displayed at 'Email' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_Email", "Enter a valid email address"));
		
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 05 - Input the valid into all fields")
	public void ValidataDatatypes_05_InputValidIntoAllFields() {
		
		log.info("Precondition - Step 01. Search for loan");
		loansPage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		
		log.info("Precondition - Step 02. Open loan basic detail tab");
		loansPage.openLoansDetailPage(loanName);
		
		log.info("ValidataDatatypes_05 - Step 01: Input the valid data to 'Final Borrower LLC' field");
		loansPage.inputFinalBorrowerLLC(validText);
		
		log.info("ValidataDatatypes_05 - Step 02: Input the valid data to 'Company Website' field");
		loansPage.inputCompanyWebsite(validText);
		
		log.info("ValidataDatatypes_05 - Step 03: Input the valid data to 'Loan Amount Requested' field");
		loansPage.inputLoanAmountRequested(validCurrency);
		
		log.info("ValidataDatatypes_05 - Step 04: Input the valid data to 'SFR State' field");
		loansPage.selectSFRState(state);
		
		log.info("ValidataDatatypes_05 - Step 05: Input the valid data to 'Property Managament' field");
		loansPage.selectPropertyManagement(propertyManagament);
		
		log.info("ValidataDatatypes_05 - Step 06: Input the valid data to 'Outsourcing Company' field");
		loansPage.inputOutsourcingCompany(validText);
		
		log.info("ValidataDatatypes_05 - Step 07: Input the valid data to 'Portfolio Hold' field");
		loansPage.selectPotfolioHold(yearHold);
		
		log.info("ValidataDatatypes_05 - Step 08: Input the valid data to 'Source' field");
		loansPage.selectSource(source);
		
		log.info("ValidataDatatypes_05 - Step 09: Input the valid data to 'Midland ID' field");
		loansPage.inputMidlandID(validText);
		
		log.info("ValidataDatatypes_05 - Step 10: Input the valid data to 'Company Type' field");
		loansPage.selectCompanyType(companyType);
		
		log.info("ValidataDatatypes_05 - Step 11: Input the valid data to 'Place Of Formation' field");
		loansPage.inputPlaceOfFormation(validText);
		
		log.info("ValidataDatatypes_05 - Step 12: Input the valid data to 'Primary Contact' field");
		loansPage.inputPrimaryContact(validText);
		
		log.info("ValidataDatatypes_05 - Step 13: Input the valid data to 'Phone' field");
		loansPage.inputPhoneTextbox(validPhoneNumber);
		
		log.info("ValidataDatatypes_05 - Step 14: Input the valid data to 'Email' field");
		loansPage.inputEmailTextbox(validEmail);
		
		log.info("ValidataDatatypes_05 - Step 15: Input the valid data to 'Address' field");
		loansPage.inputAddress(validText);
		
		log.info("ValidataDatatypes_05 - Step 16: Input the valid data to 'City' field");
		loansPage.inputCity(validText);
		
		log.info("ValidataDatatypes_05 - Step 17: Input the valid data to 'State' field");
		loansPage.selectState(state);
		
		log.info("ValidataDatatypes_05 - Step 18: Input the valid data to 'Zip' field");
		loansPage.inputZipCode(validZipCode);
		
		log.info("ValidataDatatypes_05 - Step 19: Input the valid data to 'Comment' field");
		loansPage.inputCommentTextbox(validText);
		
		log.info("ValidataDatatypes_05 - Step 20: Input the invalid data to 'SFR Buying Started' field");
		loansPage.inputSFRBuyingStarted(validDate);
		
		log.info("ValidataDatatypes_05 - Step 21: Input the invalid data to 'Date Established ");
		loansPage.inputDateEstablished(validDate);
		
		log.info("ValidataDatatypes_05 - Step 22: Input the valid data to 'Owner' field");
		loansPage.selectOwner(owner);
		
		log.info("ValidataDatatypes_05 - Step 23: Click 'Save' button");
		loansPage.clickSaveButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(loansPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: Verify that 'Final BorrowerLLC' is saved successfully");
		verifyTrue(loansPage.isFinalBorrowerLLCDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'SFR Buying Started' is saved successfully");
		verifyTrue(loansPage.isSFRBuyingStartedDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that 'Company Website' is saved successfully");
		verifyTrue(loansPage.isCompanyWebsiteDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Outsourcing Company' is saved successfully");
		verifyTrue(loansPage.isOutsourcingCompanyDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Place Of Formation' is saved successfully");
		verifyTrue(loansPage.isPlaceOfFormationDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Date Established' is saved successfully");
		verifyTrue(loansPage.isDateEstablishedDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that 'Primary Contact' is saved successfully");
		verifyTrue(loansPage.isPrimaryContactDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Phone Textbox' is saved successfully");
		verifyTrue(loansPage.isPhoneTextboxDisplayedCorrectly(validPhoneNumber));
		
		log.info("VP: Verify that 'Email' is saved successfully");
		verifyTrue(loansPage.isEmailTextboxDisplayedCorrectly(validEmail));
		
		log.info("VP: Verify that 'Address' is saved successfully");
		verifyTrue(loansPage.isAddressDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'City' is saved successfully");
		verifyTrue(loansPage.isCityDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Zipcode' is saved successfully");
		verifyTrue(loansPage.isZipcodeDisplayedCorrectly(validZipCode));
		
		log.info("VP: Verify that 'Comment' is saved successfully");
		verifyTrue(loansPage.isCommentTextboxDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'SFR State' is saved successfully");
		verifyTrue(loansPage.isSFRStateDisplayedCorrectly(state));
		
		log.info("VP: Verify that 'Property Managament' is saved successfully");
		verifyTrue(loansPage.isPropertyManagementDisplayedCorrectly(propertyManagament));
		
		log.info("VP: Verify that 'Potfolio Hold' is saved successfully");
		verifyTrue(loansPage.isPotfolioHoldDisplayedCorrectly(yearHold));
		
		log.info("VP: Verify that 'Source' is saved successfully");
		verifyTrue(loansPage.isSourceDisplayedCorrectly(source));
		
		log.info("VP: Verify that 'Owner' is saved successfully");
		verifyTrue(loansPage.isOwnerDisplayedCorrectly(owner));
		
		log.info("VP: Verify that 'Company Type' is saved successfully");
		verifyTrue(loansPage.isCompanyTypeDisplayedCorrectly(companyType));
		
		log.info("VP: Verify that 'Unit Level Financials Frequency' is saved successfully");
		verifyTrue(loansPage.isStateDisplayedCorrectly(state));
		
		log.info("VP: Verify that 'Midland ID' is saved successfully");
		verifyTrue(loansPage.isMidlandIDDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Loan Amount Requested' is saved successfully");
		verifyTrue(loansPage.isLoanAmountRequestedDisplayedCorrectly(validCurrency));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender;
	private String loanName, loanStatus, accountNumber, applicantName;
	private String invalidEmail, invalidCurrency, invalidDate,invalidContactNumber;
	private String state, propertyManagament, yearHold, source, owner, companyType;
	private String validEmail, validCurrency, validDate, validPhoneNumber, validText, validZipCode;
	}