package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFDocumentPage;
import page.FNFLoansPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Common;
import common.Constant;

public class sanityTest_012_FNF_DocumentApproval extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		loggedLender = "CAF FNF Lender";
		loggedApplicant = "CAF FNF Applicant";
		year = Common.getCommon().getCurrentYearOfWeek();
		companyName = "Edit_Borrower " + accountNumber;
		documentFolder1 = "Entity Documents";
		documentFolder2 = "Financials";
		documentFileName1 = "datatest 1.pdf";
		documentFileName2 = "datatest 2.pdf";
		statusChange1 = "Under Review";
		statusChange3 = "Rejected";
		statusChange4 = "Approved";
		locClosingDate = "04/12/2014";
		loanName = "Edit Loan Auto " + getUniqueNumber();
	}


	@Test(groups = { "regression" }, description = "Applicant 24 - Login as Lender - Upload document and check status to 'Under Review'")
	public void DocumentApproval_01_Lender_AddDocumentAndCheckStatusToUnderReview() {

		log.info("DocumentApproval_01 - Step 01. Logout and login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("DocumentHistorycal_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("DocumentHistorycal_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("DocumentHistorycal_01 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("DocumentHistorycal_01 - Step 05: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("DocumentHistorycal_01 - Step 06: Input loan name");
		loanPage.inputRequiredData(loanName, locClosingDate);
		
		log.info("DocumentHistorycal_01 - Step 07: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("DocumentApproval_01 - Step 12. Upload document type by clicking the place holder");
		loanPage.uploadDynamicDocumentFileByPlaceHolder(documentFolder1, documentFileName1);
		loanPage.clickOnSaveButton();
		
		log.info("VP: Document is loaded in 'Document' child-table with 'Under Review' status");
		verifyTrue(loanPage.isDocumentLoadedToDocumentType(documentFolder1, documentFileName1.replace(".pdf","")));
//		verifyTrue(applicantsPage.isDocumentLoadedToDocumentType(documentTypeDefault2, documentFileName2, statusChange2, "/" + year + " by  " + loggedLender));
		
		log.info("DocumentApproval_01 - Step 13. Open 'Document folders' detail");
		documentsPage = loanPage.openDocumentsFolder(documentFolder1);
		
		log.info("DocumentHistorycal_01 - Step 14. Click History link");
		documentsPage.openHistoryPopupBox(documentFileName1);
		
		log.info("DocumentHistorycal_01 - Step 15. Switch to History popup frame");
		documentsPage.switchToHistoryPopupFrame(driver);
		
		log.info("VP: Document is loaded in 'Document Detail' table with 'Under Review' status");
		verifyTrue(documentsPage.isDocumentHistoryLoadedToDocumentType(documentFileName1, loggedLender, "/" + year, " ", statusChange1));
		
		log.info("DocumentHistorycal_01 - Step 16. Click 'Document File Name 1' detail");
		documentsPage.clickDocumentFileNameDetail( loggedLender, documentFileName1);
		
		log.info("VP: The document 1 is openned to view");
		verifyTrue(documentsPage.isDocumentViewable(documentFileName1));
		documentsPage.switchToTopWindowFrame(driver);
		
		log.info("DocumentHistorycal_01 - Step 17. Close History pop-up box");
		documentsPage.closeHistoryPopupBox();
	}
	

	@Test(groups = { "regression" }, description = "Applicant 23 - Login as Applicant - Upload document and check status to 'Under Review'")
	public void DocumentApproval_02_Borrower_AddDocumentAndCheckStatusToUnderReview() {

		log.info("DocumentApproval_01 - Step 01: Login with Applicant");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);
		applicantsPage.setApplicantNameToDefault("CAF FNF", "Applicant");
		
		log.info("DocumentHistorycal_02 - Step 02: Open 'Loan Name'");
		loanPage = applicantsPage.openLoanNameInOpportunityTable(loanName);
		
		log.info("DocumentApproval_01 - Step 06. Upload document type by clicking the place holder");
		loanPage.uploadDynamicDocumentFileByPlaceHolder(documentFolder2, documentFileName2);
		loanPage.clickOnSaveButton();
		
		log.info("VP: Document is loaded in 'Document' child-table with 'Under Review' status");
		verifyTrue(loanPage.isDocumentLoadedToDocumentType(documentFolder2, documentFileName2.replace(".pdf","")));
		
		log.info("DocumentApproval_01 - Step 07. Click 'Document Type' detail");
		documentsPage = loanPage.openDocumentsFolder(documentFolder2);
		
		log.info("DocumentHistorycal_01 - Step 08. Click History link");
		documentsPage.openHistoryPopupBox(documentFileName2);
		
		log.info("DocumentHistorycal_01 - Step 09. Switch to History popup frame");
		documentsPage.switchToHistoryPopupFrame(driver);
		
		log.info("VP: Document is loaded in 'Document Detail' table with 'Under Review' status");
		verifyTrue(documentsPage.isDocumentHistoryLoadedToDocumentType(documentFileName2, loggedApplicant, "/" + year, " ", statusChange1));
		
		log.info("DocumentHistorycal_01 - Step 10. Click 'Document File Name 1' detail");
		documentsPage.clickDocumentFileNameDetail( loggedApplicant, documentFileName2);
		
		log.info("VP: The document 1 is openned to view");
		verifyTrue(documentsPage.isDocumentViewable(documentFileName2));
		documentsPage.switchToTopWindowFrame(driver);
		
		log.info("DocumentHistorycal_01 - Step 11. Close History pop-up box");
		documentsPage.closeHistoryPopupBox();
	}

	@Test(groups = { "regression" }, description = "Applicant 25 - Login as Lender - Change document status to 'Rejected'")
	public void DocumentApproval_03_Lender_ChangeDocumentStatusToRejected() {
		
		log.info("DocumentApproval_03 - Step 01: Login with Applicant");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
		
		log.info("DocumentApproval_03 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("DocumentApproval_03 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("DocumentApproval_03 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("DocumentApproval_03 - Step 05: Open 'Loan Name'");
		loanPage = applicantsPage.openLoanNameInOpportunityTable(loanName);
		
		log.info("DocumentApproval_03 - Step 06. Click 'Document Type' detail");
		loanPage.openDocumentsFolder(documentFolder1);
		
		log.info("DocumentApproval_03 - Step 07: Click 'Rejected' button");
		loanPage.changeDocumentsStatus(documentFileName1, statusChange3);
		loanPage.clickOnSaveButton();
		
		log.info("VP: Record Saved message displays");
		verifyTrue(loanPage.isRecordSavedMessageDisplays());
		
		log.info("DocumentApproval_03 - Step 08. Click History link");
		documentsPage.openHistoryPopupBox(documentFileName1);
		
		log.info("DocumentApproval_03 - Step 09. Switch to History popup frame");
		documentsPage.switchToHistoryPopupFrame(driver);
		
		log.info("VP: Document is loaded in 'Document Detail' table with 'Under Review' status");
		verifyTrue(documentsPage.isDocumentHistoryLoadedToDocumentType(documentFileName1, "Uploaded By: " + loggedLender, "/" + year, " ", statusChange1));
		
		log.info("VP: Document is loaded in 'Document Detail' table with 'Under Review' previous status and 'Reject' status");
		verifyTrue(documentsPage.isNoDocumentHistoryLoadedToDocumentType("Status Change By: " + loggedLender, "/" + year, statusChange1, statusChange3));
		documentsPage.switchToTopWindowFrame(driver);
		
		log.info("DocumentApproval_03 - Step 10. Close History pop-up box");
		documentsPage.closeHistoryPopupBox();
	}
	
	@Test(groups = { "regression" }, description = "Applicant 28 - Login as Lender - Change document status to 'Approved'")
	public void DocumentApproval_04_Lender_ChangeDocumentStatusToApproved() {
		log.info("DocumentApproval_04 - Step 01: Login with Applicant");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
		
		log.info("DocumentApproval_04 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("DocumentApproval_04 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("DocumentApproval_04 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("DocumentApproval_04 - Step 05: Open 'Loan Name'");
		loanPage = applicantsPage.openLoanNameInOpportunityTable(loanName);
		
		log.info("DocumentApproval_04 - Step 06. Click 'Document Type' detail");
		loanPage.openDocumentsFolder(documentFolder1);
		
		log.info("DocumentApproval_04 - Step 07: Change status to Approved");
		loanPage.changeDocumentsStatus(documentFileName1, statusChange4);
		loanPage.clickOnSaveButton();
		
		log.info("VP: Record Saved message displays");
		verifyTrue(loanPage.isRecordSavedMessageDisplays());
		
		log.info("DocumentApproval_04 - Step 08. Click History link");
		documentsPage.openHistoryPopupBox(documentFileName1);
		
		log.info("DocumentApproval_04 - Step 09. Switch to History popup frame");
		documentsPage.switchToHistoryPopupFrame(driver);
		
		log.info("VP: Document is loaded in 'Document Detail' table with 'Under Review' status");
		verifyTrue(documentsPage.isDocumentHistoryLoadedToDocumentType(documentFileName1, "Uploaded By: " + loggedLender, "/" + year, " ", statusChange1));
		
		log.info("VP: Document is loaded in 'Document Detail' table with 'Under Review' previous status and 'Reject' status");
		verifyTrue(documentsPage.isNoDocumentHistoryLoadedToDocumentType("Status Change By: " + loggedLender, "/" + year, statusChange1, statusChange3));
		
		log.info("VP: Document is loaded in 'Document Detail' table with 'Rejected' previous status and 'Approved' status");
		verifyTrue(documentsPage.isNoDocumentHistoryLoadedToDocumentType("Status Change By: " + loggedLender, "/" + year, statusChange3, statusChange4));
		documentsPage.switchToTopWindowFrame(driver);
		
		log.info("DocumentApproval_04 - Step 10. Close History pop-up box");
		documentsPage.closeHistoryPopupBox();
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFLoansPage loanPage;
	private FNFBorrowersPage applicantsPage;
	private FNFDocumentPage documentsPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername,	FNFBorrowerPassword;
	private String documentFolder1, documentFolder2;
	private String documentFileName1, documentFileName2;
	private String statusChange1, statusChange3, statusChange4;
	private String companyName, accountNumber, loanName;
	private String loggedLender, loggedApplicant, locClosingDate;
	private int year;
	}