package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;

import common.AbstractTest;
import common.Constant;

public class sanityTest_004_FNF_UpdateApplicantInvalidDateAndNumber extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		invalidDate = "4/21/2011*%#$fail";
	}

	@Test(groups = { "regression" }, description = "Applicant 05 - Login as Lender and input invalid 'Date of Formation' field")
	public void Applicant_11_InputInvalidDateOfFormation () {
		log.info("Applicant_15 - Step 01: Login with valid username password");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);

		log.info("Applicant_15 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
		
		log.info("Applicant_15 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("Applicant_15 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();

		log.info("Applicant_15 - Step 05: Input invalid 'Date of Formation'");
		applicantsPage.inputDate(invalidDate, "");

		log.info("Applicant_15 - Step 06: Click on 'Save' button");
		applicantsPage.clickOnSaveButton();

		log.info("VP: The error message should be displayed");
		verifyTrue(applicantsPage.isErrorMessageDisplayed("Value must be a date"));
	}
	
	@Test(groups = { "regression" }, description = "Applicant 05 - Login as Lender and input invalid 'Date of Fiancials' field")
	public void Applicant_12_InputInvalidDateOfFiancials() {
		
		log.info("Applicant_16 - Step 01: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
		
		log.info("Applicant_16 - Step 02: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("Applicant_16 - Step 03: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();

		log.info("Applicant_16 - Step 04: Input invalid 'Date of Fiancials'");
		applicantsPage.inputDate("", invalidDate);

		log.info("Applicant_16 - Step 05: Click on 'Save' button");
		applicantsPage.clickOnSaveButton();

		log.info("VP: The error message should be displayed");
		verifyTrue(applicantsPage.isErrorMessageDisplayed("Value must be a date"));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private String FNFLenderUsername, FNFLenderPassword, companyName;
	private String invalidDate, accountNumber;
	}