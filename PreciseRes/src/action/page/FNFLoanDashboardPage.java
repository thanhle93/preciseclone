package page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import common.Common;

import PreciseRes.Interfaces;

public class FNFLoanDashboardPage extends AbstractPage {
	public FNFLoanDashboardPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}

	/**
	 * check Loans Detail Page Display
	 */
	public boolean isLoansDetailPageDisplay(String loanName) {
		waitForControl(driver,
				Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL, timeWait);
		String realLoanName = getAttributeValue(driver,
				Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL, "value");
		return realLoanName.contains(loanName);
	}

	/**
	 * get First Loan Item Name
	 */
	public String getFirstLoanItemName() {
		waitForControl(driver, Interfaces.LoansPage.FIRST_LOAN_ITEM, timeWait);
		String itemName = getText(driver, Interfaces.LoansPage.FIRST_LOAN_ITEM);
		return itemName;
	}

	/**
	 * select First Loan Item Name
	 */
	public void selectFirstLoansItem() {
		waitForControl(driver, Interfaces.LoansPage.FIRST_LOAN_ITEM, timeWait);
		String url = getControlHref(driver,
				Interfaces.LoansPage.FIRST_LOAN_ITEM);
		driver.get(url);
		sleep();
	}

	/**
	 * check Error Process Request Display
	 */
	public boolean isErrorProcessRequestDisplay() {
		waitForControl(driver,
				Interfaces.LoansPage.PROCESS_REQUEST_ERROR_TITLE, timeWait);
		boolean titleDisplay = isControlDisplayed(driver,
				Interfaces.LoansPage.PROCESS_REQUEST_ERROR_TITLE);
		boolean contentDisplay = isControlDisplayed(driver,
				Interfaces.LoansPage.PROCESS_REQUEST_ERROR_CONTENT);
		return titleDisplay && contentDisplay;
	}

	/**
	 * check Username Display Above Main Menu
	 */
	public boolean isUsernameDisplayTopLeftCorner(String userFullName) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_USER_FULL_NAME_TITLE,
				userFullName, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_USER_FULL_NAME_TITLE, userFullName);
	}

	/**
	 * check Logged As Username Display Right Left Corner
	 */
	public boolean isLoggedAsUsernameDisplayRightLeftCorner(String userName) {
		waitForControl(driver,
				Interfaces.LoansPage.USER_FULL_NAME_LOGGED, userName,
				timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.USER_FULL_NAME_LOGGED, userName);
	}

	/**
	 * check Tab Active On Main Menu
	 */
	public boolean isTabActiveOnMainMenu(String tabName) {
		waitForControl(driver, Interfaces.LoansPage.TAB_ACTIVE_ON_MAIN_MENU,
				timeWait);
		String actualTab = getText(driver,
				Interfaces.LoansPage.TAB_ACTIVE_ON_MAIN_MENU);
		return actualTab.contains(tabName);
	}

	/**
	 * search Loan By Name
	 */
	public void searchLoanByName(String loanName) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX,
				timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, loanName);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search2').click();");
		sleep(2);
	}

	/**
	 * open Loans Detail Page
	 */
	public void openLoansDetailPage(String loanName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_LOAN_ITEM,
				loanName, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_LOAN_ITEM, loanName);
		sleep();
	}

	/**
	 * open Basic Detail Tab
	 */
	public void openBasicDetailTab() {
		waitForControl(driver, Interfaces.LoansPage.BASIC_DETAIL_TAB, timeWait);
		click(driver, Interfaces.LoansPage.BASIC_DETAIL_TAB);
		sleep();
	}

	/**
	 * click New Loan Application Entity
	 */
	public void clickNewLoanApplicationEntity() {
		waitForControl(driver,
				Interfaces.LoansPage.NEW_LOAN_APPLICATION_ENTITY_BUTTON,
				timeWait);
		click(driver, Interfaces.LoansPage.NEW_LOAN_APPLICATION_ENTITY_BUTTON);
		sleep();
	}

	/**
	 * select Entity Type
	 */
	public void selectEntityType(String entityType) {
		waitForControl(driver, Interfaces.LoansPage.ENTITY_TYPE_COMBOBOX,
				timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.ENTITY_TYPE_COMBOBOX,
				entityType);
		sleep(1);
	}

	/**
	 * click New Applicant Link
	 */
	public void clickNewApplicantLink() {
		waitForControl(driver, Interfaces.LoansPage.NEW_APPLICANT_LINK,
				timeWait);
		click(driver, Interfaces.LoansPage.NEW_APPLICANT_LINK);
		sleep();
	}

	/**
	 * input Applicant Info To Create
	 */
	public void inputApplicantInfoToCreate(String accountNumber,
			String accountName, String addressNumber, String addressDictrict,
			String city, String state, String zipCode) {
		waitForControl(driver, Interfaces.LoansPage.ACCOUNT_NUMBER_TEXTBOX,
				timeWait);
		type(driver, Interfaces.LoansPage.ACCOUNT_NUMBER_TEXTBOX, accountNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_NAME_TEXTBOX, accountName);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_ADDRESS_STREET_TEXTBOX,
				addressNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_ADDRESS_DISTRICT_TEXTBOX,
				addressDictrict);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_CITY_TEXTBOX, city);
		sleep(1);
		selectItemCombobox(driver, Interfaces.LoansPage.ACCOUNT_STATE_TEXTBOX,
				state);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_ZIPCODE_TEXTBOX, zipCode);
		sleep(1);
	}

	/**
	 * click Save New Applicant Button
	 */
	public void clickSaveNewApplicantButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_NEW_APPLICANT_BUTTON,
				timeWait);
		click(driver, Interfaces.LoansPage.SAVE_NEW_APPLICANT_BUTTON);
		sleep();
	}

	/**
	 * click Save Loans Applicant Entity Button
	 */
	public void clickSaveLoansApplicantEntityButton() {
		waitForControl(driver,
				Interfaces.LoansPage.SAVE_LOAN_APPLICANT_ENTITY_BUTTON,
				timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		sleep();
	}

	/**
	 * check New Applicant Saved
	 */
	public boolean isNewApplicantSaved(String applicantName) {
		waitForControl(driver, Interfaces.LoansPage.APPLICANT_ENTITY_COMBOBOX,
				timeWait);
		sleep();
		String applicant = getItemSelectedCombobox(driver,
				Interfaces.LoansPage.APPLICANT_ENTITY_COMBOBOX);
		String fullname = getAttributeValue(driver,
				Interfaces.LoansPage.APPLICANT_FULLNAME_TEXTBOX, "value");
		return applicant.contains(applicantName)
				&& fullname.contains(applicantName);
	}

	/**
	 * click Back Button
	 */
	public void clickBackButton() {
		waitForControl(driver, Interfaces.LoansPage.BACK_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Back').click();");
		sleep();
	}

	/**
	 * check New Applicant Display On LAE Table
	 */
	public boolean isNewApplicantDisplayOnLAETable(String applicantName) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_APPLICANT_NAME_ON_TABLE,
				applicantName, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_APPLICANT_NAME_ON_TABLE,
				applicantName);
	}

	/**
	 * delete Applicant In Table
	 */
	public void deleteApplicantInTable(String applicantName) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_DELETE_APPLICANT_CHECKBOX,
				applicantName, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_DELETE_APPLICANT_CHECKBOX,
				applicantName);
		sleep(1);
		clickSaveButton();
		sleep();
	}

	/**
	 * switch to new loan entity frame
	 */
	public WebDriver switchToNewLoansEntityFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.LoansPage.NEW_LOAN_ENTITY_FRAME,
				timeWait);
		sleep();
		driver = driver.switchTo().frame(
				driver.findElement(By
						.xpath(Interfaces.LoansPage.NEW_LOAN_ENTITY_FRAME)));
		return driver;
	}

	/**
	 * create new Loan item
	 */
	public void createNewLoan(String loanName, String applicantName,
			String borrowerName, String loanStatus) {
		clickNewLoanButton();
		waitForControl(driver, Interfaces.LoansPage.LEGAL_NAME_TEXTBOX,
				timeWait);
		type(driver, Interfaces.LoansPage.LEGAL_NAME_TEXTBOX, loanName);
		selectItemCombobox(driver, Interfaces.LoansPage.APPLICANT_COMBOBOX,
				applicantName);
		if (!borrowerName.equals("")) {
			selectItemCombobox(driver, Interfaces.LoansPage.BORROWER_COMBOBOX,
					borrowerName);
		}
		selectItemCombobox(driver,
				Interfaces.LoansPage.LOAN_APPLICANTION_STATUS_COMBOBOX,
				loanStatus);
		clickSaveLoanApplicationButton();
	}

	/**
	 * create new Loan item
	 */
	public void createNewLoan(String loanName, String applicantName,
			String borrowerName, String loanStatus, String loanID) {
		clickNewLoanButton();
		waitForControl(driver, Interfaces.LoansPage.LEGAL_NAME_TEXTBOX,
				timeWait);
		type(driver, Interfaces.LoansPage.LEGAL_NAME_TEXTBOX, loanName);
		selectItemCombobox(driver, Interfaces.LoansPage.APPLICANT_COMBOBOX,
				applicantName);
		if (!borrowerName.equals("")) {
			selectItemCombobox(driver, Interfaces.LoansPage.BORROWER_COMBOBOX, borrowerName);
		}
		selectItemCombobox(driver,	Interfaces.LoansPage.LOAN_APPLICANTION_STATUS_COMBOBOX, loanStatus);
		type(driver, Interfaces.LoansPage.LOAN_ID_TEXTBOX, loanID);
		clickSaveLoanApplicationButton();
	}

	/**
	 * input Loan ID
	 */
	public void inputLoanID(String loanID) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_ID_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.LOAN_ID_TEXTBOX, loanID);
		sleep(1);
	}

	/**
	 * click New Loan Button
	 */
	public void clickNewLoanButton() {
		waitForControl(driver, Interfaces.LoansPage.NEW_LOAN_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('New').click();");
		executeJavaScript(driver, "document.getElementById('New2').click();");
		sleep();
	}

	/**
	 * click Save Loan Application Button
	 */
	public void clickSaveLoanApplicationButton() {
		waitForControl(driver,
				Interfaces.LoansPage.SAVE_LOAN_APPLICATION_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		sleep();
	}

	/**
	 * check Loans Display With Correct Info
	 */
	public boolean isLoansDisplayWithCorrectInfo(String loanName,
			String loanStatus) {
		waitForControl(driver,
				Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL, timeWait);
		String realLoanName = getAttributeValue(driver,
				Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL, "value");
		String realStatus = getItemSelectedCombobox(driver,
				Interfaces.LoansPage.LOAN_STATUS_COMBOBOX_BASIC_DETAIL);
		return realLoanName.contains(loanName) && realStatus.contains(loanStatus);
	}
	
	/**
	 * check Loans [Read Only - B2R] Display With Correct Info
	 */
	public boolean isLoansReadOnlyDisplayWithCorrectInfo(String loanName,
			String loanStatus) {
		waitForControl(driver,
				Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL, timeWait);
		String realLoanName = getAttributeValue(driver,
				Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL, "value");
		String realStatus = getText(driver,
				Interfaces.LoansPage.LOAN_STATUS_COMBOBOX_BASIC_DETAIL);
		return realLoanName.contains(loanName) && realStatus.contains(loanStatus);
	}

	/**
	 * open Properties Tab
	 */
	public void openPropertiesTab() {
		waitForControl(driver, Interfaces.LoansPage.PROPERTIES_TAB, timeWait);
		executeJavaScript(driver,
				"document.getElementById('PropertyTB').click();");
		executeJavaScript(driver,
				"document.getElementById('CBRPropertyTB').click();");
		sleep();
	}

	/**
	 * upload File Properties
	 */
	public void uploadFileProperties(String propertiesFileName) {
		waitForControl(driver, Interfaces.LoansPage.UPLOAD_FILE_BUTTON,
				timeWait);
		click(driver, Interfaces.LoansPage.UPLOAD_FILE_BUTTON);
		waitForControl(driver, Interfaces.LoansPage.CHOOSE_FILE_BUTTON,
				timeWait);
		click(driver, Interfaces.LoansPage.CHOOSE_FILE_BUTTON);
		if (driver.toString().toLowerCase().contains("internetexplorerdriver")) {
			keyPressing("space");
		}
		sleep(1);
		Common.getCommon().openFileForUpload(driver, propertiesFileName);
		sleep();
		clickSaveLoadPropertyButton();
		sleep();
	}

	/**
	 * click Save Load Property Button
	 */
	public void clickSaveLoadPropertyButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_LOAD_PROPERTY_BUTTON,
				timeWait);
		click(driver, Interfaces.LoansPage.SAVE_LOAD_PROPERTY_BUTTON);
		sleep();
	}

	//@DamDM
	/**
	 * click Go To Loan Button
	 */
	public void clickGoToLoanButton() {
		waitForControl(driver, Interfaces.LoansPage.GO_TO_LOAN_BUTTON,
				timeWait);
		click(driver, Interfaces.LoansPage.GO_TO_LOAN_BUTTON);
		sleep();
	}
	/**
	 * check Property Display
	 */
	public boolean isPropertyDisplay(String address, String city, String state,
			String zip) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_PROPERTY_ITEM,
				city, state, zip, address, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_PROPERTY_ITEM, city, state, zip,
				address);
	}

	/**
	 * open General Document Tab
	 */
	public DocumentsPage openGeneralDocumentTab() {
		waitForControl(driver, Interfaces.LoansPage.GENERAL_LOANS_DOCUMENT_TAB,
				timeWait);
		executeJavaScript(driver,
				"document.getElementById('GeneralLoanDocsTB').click();");
		executeJavaScript(driver,
				"document.getElementById('CBRGeneralLoanDocsTB').click();");
		sleep();
		return PageFactory.getDocumentsPage(driver, ipClient);
	}

	/**
	 * open Property Detail
	 */
	public PropertiesPage openPropertyDetail(String address) {
		searchAddress(address);
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_ADDRESS_PROPERTY,
				address, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_ADDRESS_PROPERTY, address);
		sleep();
		return PageFactory.getPropertiesPage(driver, ipClient);
	}

	/**
	 * search property address
	 */
	public void searchAddress(String address) {
		waitForControl(driver,
				Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX,
				address);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep();
	}

	/**
	 * create New Reporting Schedule
	 */
	public void createNewReportingSchedule(String reportType, String frequency,
			String daysPrior, boolean repeat) {
		clickNewReportingSchedule();
		waitForControl(driver, Interfaces.LoansPage.REPORT_TYPE_COMBOBOX,
				timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.REPORT_TYPE_COMBOBOX,
				reportType);
		selectItemCombobox(driver, Interfaces.LoansPage.FREQUENCY_COMBOBOX,
				frequency);
		type(driver, Interfaces.LoansPage.DAYS_PRIOR_TEXTBOX, daysPrior);
		checkTheCheckbox(driver, Interfaces.LoansPage.REPEAT_CHECKBOX);
		clickSaveReportingScheduleDetail();
	}

	/**
	 * open Reporting Schedule Tab
	 */
	public void openReportingScheduleTab() {
		waitForControl(driver, Interfaces.LoansPage.REPORTING_SCHEDULE_TAB,
				timeWait);
		executeJavaScript(driver,
				"document.getElementById('DocScheduleTB').click();");
		sleep();
	}

	/**
	 * click New Reporting Schedule
	 */
	public void clickNewReportingSchedule() {
		waitForControl(driver,
				Interfaces.LoansPage.NEW_REPORTING_SCHEDULE_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.NEW_REPORTING_SCHEDULE_BUTTON);
		sleep();
	}

	/**
	 * click Save Reporting Schedule Detail
	 */
	public void clickSaveReportingScheduleDetail() {
		waitForControl(driver,
				Interfaces.LoansPage.SAVE_REPORTING_SCHEDULE_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.SAVE_REPORTING_SCHEDULE_BUTTON);
		sleep();
	}

	/**
	 * search Reporting Schedule
	 */
	public void searchReportingSchedule(String reportType, String frequency,
			String daysPrior, boolean repeat) {
		waitForControl(driver,
				Interfaces.LoansPage.SEARCH_REPORT_TYPE_COMBOBOX, timeWait);
		selectItemCombobox(driver,
				Interfaces.LoansPage.SEARCH_REPORT_TYPE_COMBOBOX, reportType);
		selectItemCombobox(driver,
				Interfaces.LoansPage.SEARCH_FREQUENCY_COMBOBOX, frequency);
		type(driver, Interfaces.LoansPage.SEARCH_DAYS_PRIOR_TEXTBOX, daysPrior);
		if (repeat) {
			click(driver, Interfaces.LoansPage.YES_RADIOBUTTON);
		} else {
			click(driver, Interfaces.LoansPage.NO_RADIOBUTTON);
		}
		click(driver, Interfaces.LoansPage.SEARCH_BUTTON);
		sleep();
	}

	/**
	 * check Reporting Schedule Display
	 */
	public boolean isReportingScheduleDisplay(String reportType,
			String frequency, String daysPrior, boolean repeat) {
		String temp = "No";
		if (repeat) {
			temp = "Yes";
		}
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_REPORTING_SCHEDULE_ITEM,
				reportType, frequency, daysPrior, temp, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_REPORTING_SCHEDULE_ITEM,
				reportType, frequency, daysPrior, temp);
	}

	/**
	 * open Corporate Entity Document Tab
	 */
	public DocumentsPage openCorporateEntityDocumentsTab() {
		waitForControl(driver,
				Interfaces.LoansPage.CORPORATE_ENTITY_DOCUMENT_TAB, timeWait);
		executeJavaScript(driver,
				"document.getElementById('CorpEntityLoanDocsTB').click();");
		executeJavaScript(driver, "document.getElementById('CBRCorpEntityLoanDocsTB').click();");
		sleep();
		return PageFactory.getDocumentsPage(driver, ipClient);
	}

	/**
	 * input Legal Name For Loan
	 */
	public void inputLegalNameForLoan(String loanName) {
		waitForControl(driver, Interfaces.LoansPage.LEGAL_NAME_TEXTBOX,
				timeWait);
		type(driver, Interfaces.LoansPage.LEGAL_NAME_TEXTBOX, loanName);
	}

	/**
	 * select Loan Status
	 */
	public void selectLoanStatus(String loanStatus) {
		waitForControl(driver,
				Interfaces.LoansPage.LOAN_APPLICANTION_STATUS_COMBOBOX,
				timeWait);
		selectItemCombobox(driver,
				Interfaces.LoansPage.LOAN_APPLICANTION_STATUS_COMBOBOX,
				loanStatus);
	}

	/**
	 * click New Applicant Link On Add Loan
	 */
	public void clickNewApplicantLinkOnAddLoan() {
		waitForControl(driver,
				Interfaces.LoansPage.NEW_APPLICANT_LINK_ADD_LOAN, timeWait);
		click(driver, Interfaces.LoansPage.NEW_APPLICANT_LINK_ADD_LOAN);
		sleep();
	}

	/**
	 * switch To New Applicant For Loan Frame
	 */
	public WebDriver switchToNewApplicantForLoanFrame(WebDriver driver) {
		waitForControl(driver,
				Interfaces.LoansPage.NEW_APPLICANT_FOR_LOAN_FRAME, timeWait);
		sleep();
		driver = driver
				.switchTo()
				.frame(driver.findElement(By
						.xpath(Interfaces.LoansPage.NEW_APPLICANT_FOR_LOAN_FRAME)));
		return driver;
	}

	/**
	 * input Applicant Info To Create For Loans
	 */
	public void inputApplicantInfoToCreateForLoans(String accountNumber,
			String accountName, String addressNumber, String addressDictrict,
			String city, String state, String zipCode) {
		waitForControl(driver,
				Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_NUMBER_TEXTBOX,
				timeWait);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_NUMBER_TEXTBOX,
				accountNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_NAME_TEXTBOX,
				accountName);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_ADDRESS_STREET_TEXTBOX,
				addressNumber);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_ADDRESS_DISTRICT_TEXTBOX,
				addressDictrict);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_CITY_TEXTBOX,
				city);
		sleep(1);
		selectItemCombobox(driver,
				Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_STATE_TEXTBOX, state);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_ZIPCODE_TEXTBOX,
				zipCode);
		sleep(1);
	}

	/**
	 * click Save Create New Applicant Button
	 */
	public void clickSaveCreateNewApplicantButton() {
		waitForControl(driver,
				Interfaces.LoansPage.SAVE_CREATE_NEW_APPLICANT_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.SAVE_CREATE_NEW_APPLICANT_BUTTON);
		sleep();
	}

	/**
	 * click New Guarantor Link
	 */
	public void clickNewGuarantorLink() {
		waitForControl(driver, Interfaces.LoansPage.NEW_GUARANTOR_LINK,
				timeWait);
		click(driver, Interfaces.LoansPage.NEW_GUARANTOR_LINK);
		sleep();
	}

	/**
	 * select Guarantor
	 */
	public void selectGuarantor(String guarantor) {
		waitForControl(driver, Interfaces.LoansPage.GUARANTOR_ENTITY_R1_COMBOBOX, guarantor, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.GUARANTOR_ENTITY_R1_COMBOBOX, guarantor);
		sleep(1);
	}
	
	/**
	 * select Borrower
	 */
	public void selectBorrower(String borrower) {
		waitForControl(driver, Interfaces.LoansPage.NEW_BORROWER_COMBOBOX, borrower, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.NEW_BORROWER_COMBOBOX, borrower);
		sleep(1);
	}
	
	
	/**
	 * switch To New Guarantor Entity Frame
	 */
	public WebDriver switchToNewGuarantorEntityFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_FRAME,
				timeWait);
		sleep();
		driver = driver
				.switchTo()
				.frame(driver.findElement(By
						.xpath(Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_FRAME)));
		return driver;
	}

	/**
	 * input Guarantor Info To Create
	 */
	public void inputGuarantorInfoToCreate(String accountNumber,
			String accountName, String addressNumber, String addressDictrict,
			String city, String state, String zipCode) {
		waitForControl(
				driver,
				Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_ACCOUNT_NUMBER_TEXTBOX,
				timeWait);
		type(driver,
				Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_ACCOUNT_NUMBER_TEXTBOX,
				accountNumber);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_ACCOUNT_NAME_TEXTBOX,
				accountName);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_ACCOUNT_ADDRESS_STREET_TEXTBOX,
				addressNumber);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_ACCOUNT_ADDRESS_DISTRICT_TEXTBOX,
				addressDictrict);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_ACCOUNT_CITY_TEXTBOX,
				city);
		sleep(1);
		selectItemCombobox(
				driver,
				Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_ACCOUNT_STATE_TEXTBOX,
				state);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_ACCOUNT_ZIPCODE_TEXTBOX,
				zipCode);
		sleep(1);
	}

	/**
	 * click Save New Guarantor Button
	 */
	public void clickSaveNewGuarantorButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_NEW_GUARANTOR_BUTTON,
				timeWait);
		click(driver, Interfaces.LoansPage.SAVE_NEW_GUARANTOR_BUTTON);
		sleep();
	}

	/**
	 * check New Guarantor Saved
	 */
	public boolean isNewGuarantorSaved(String guarantorName) {
		waitForControl(driver, Interfaces.LoansPage.GUARANTOR_ENTITY_R1_COMBOBOX,
				timeWait);
		sleep();
		String username = getItemSelectedCombobox(driver, Interfaces.LoansPage.GUARANTOR_ENTITY_R1_COMBOBOX);
		String fullname = getAttributeValue(driver,
				Interfaces.LoansPage.APPLICANT_FULLNAME_TEXTBOX, "value");
		return username.contains(guarantorName)
				&& fullname.contains(guarantorName);
	}

	/**
	 * click New Borrower Link
	 */
	public void clickNewBorrowerLink() {
		waitForControl(driver, Interfaces.LoansPage.NEW_BORROWER_LINK, timeWait);
		click(driver, Interfaces.LoansPage.NEW_BORROWER_LINK);
		sleep();
	}

	/**
	 * switch To New Borrower Entity Frame
	 */
	public WebDriver switchToNewBorrowerEntityFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.LoansPage.NEW_BORROWER_ENTITY_FRAME,
				timeWait);
		sleep();
		driver = driver
				.switchTo()
				.frame(driver.findElement(By
						.xpath(Interfaces.LoansPage.NEW_BORROWER_ENTITY_FRAME)));
		return driver;
	}

	/**
	 * input Borrower Info To Create
	 */
	public void inputBorrowerInfoToCreate(String accountNumber,
			String accountName, String addressNumber, String addressDictrict,
			String city, String state, String zipCode) {
		waitForControl(
				driver,
				Interfaces.LoansPage.NEW_BORROWER_ENTITY_ACCOUNT_NUMBER_TEXTBOX,
				timeWait);
		type(driver,
				Interfaces.LoansPage.NEW_BORROWER_ENTITY_ACCOUNT_NUMBER_TEXTBOX,
				accountNumber);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_BORROWER_ENTITY_ACCOUNT_NAME_TEXTBOX,
				accountName);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_BORROWER_ENTITY_ACCOUNT_ADDRESS_STREET_TEXTBOX,
				addressNumber);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_BORROWER_ENTITY_ACCOUNT_ADDRESS_DISTRICT_TEXTBOX,
				addressDictrict);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_BORROWER_ENTITY_ACCOUNT_CITY_TEXTBOX,
				city);
		sleep(1);
		selectItemCombobox(driver,
				Interfaces.LoansPage.NEW_BORROWER_ENTITY_ACCOUNT_STATE_TEXTBOX,
				state);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_BORROWER_ENTITY_ACCOUNT_ZIPCODE_TEXTBOX,
				zipCode);
		sleep(1);
	}

	/**
	 * click Save New Borrower Button
	 */
	public void clickSaveNewBorrowerButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_NEW_BORROWER_BUTTON,
				timeWait);
		click(driver, Interfaces.LoansPage.SAVE_NEW_BORROWER_BUTTON);
		sleep();
	}

	/**
	 * check New Borrower Saved
	 */
	public boolean isNewBorrowerSaved(String borrowerName) {
		waitForControl(driver, Interfaces.LoansPage.BORROWER_ENTITY_COMBOBOX,
				timeWait);
		sleep();
		String username = getItemSelectedCombobox(driver,
				Interfaces.LoansPage.BORROWER_ENTITY_COMBOBOX);
		String fullname = getAttributeValue(driver,
				Interfaces.LoansPage.APPLICANT_FULLNAME_TEXTBOX, "value");
		return username.contains(borrowerName)
				&& fullname.contains(borrowerName);
	}

	/**
	 * click New Sponsor Link
	 */
	public void clickNewSponsorLink() {
		waitForControl(driver, Interfaces.LoansPage.NEW_SPONSOR_LINK, timeWait);
		click(driver, Interfaces.LoansPage.NEW_SPONSOR_LINK);
		sleep();
	}

	/**
	 * switch To New Sponsor Entity Frame
	 */
	public WebDriver switchToNewSponsorEntityFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.LoansPage.NEW_SPONSOR_ENTITY_FRAME,
				timeWait);
		sleep();
		driver = driver.switchTo().frame(
				driver.findElement(By
						.xpath(Interfaces.LoansPage.NEW_SPONSOR_ENTITY_FRAME)));
		return driver;
	}

	/**
	 * input Sponsor Info To Create
	 */
	public void inputSponsorInfoToCreate(String accountNumber,
			String accountName, String addressNumber, String addressDictrict,
			String city, String state, String zipCode) {
		waitForControl(driver,
				Interfaces.LoansPage.NEW_SPONSOR_ENTITY_ACCOUNT_NUMBER_TEXTBOX,
				timeWait);
		type(driver,
				Interfaces.LoansPage.NEW_SPONSOR_ENTITY_ACCOUNT_NUMBER_TEXTBOX,
				accountNumber);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_SPONSOR_ENTITY_ACCOUNT_NAME_TEXTBOX,
				accountName);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_SPONSOR_ENTITY_ACCOUNT_ADDRESS_STREET_TEXTBOX,
				addressNumber);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_SPONSOR_ENTITY_ACCOUNT_ADDRESS_DISTRICT_TEXTBOX,
				addressDictrict);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_SPONSOR_ENTITY_ACCOUNT_CITY_TEXTBOX,
				city);
		sleep(1);
		selectItemCombobox(driver,
				Interfaces.LoansPage.NEW_SPONSOR_ENTITY_ACCOUNT_STATE_TEXTBOX,
				state);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_SPONSOR_ENTITY_ACCOUNT_ZIPCODE_TEXTBOX,
				zipCode);
		sleep(1);
	}

	/**
	 * click Save New Sponsor Button
	 */
	public void clickSaveNewSponsorButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_NEW_SPONSOR_BUTTON,
				timeWait);
		click(driver, Interfaces.LoansPage.SAVE_NEW_SPONSOR_BUTTON);
		sleep();
	}

	/**
	 * check New Sponsor Saved
	 */
	public boolean isNewSponsorSaved(String sponsorName) {
		waitForControl(driver, Interfaces.LoansPage.SPONSOR_COMBOBOX, timeWait);
		sleep();
		String username = getItemSelectedCombobox(driver,
				Interfaces.LoansPage.SPONSOR_COMBOBOX);
		String fullname = getAttributeValue(driver,
				Interfaces.LoansPage.APPLICANT_FULLNAME_TEXTBOX, "value");
		return username.contains(sponsorName) && fullname.contains(sponsorName);
	}

	/**
	 * click New Pledgor Link
	 */
	public void clickNewPledgorLink() {
		waitForControl(driver, Interfaces.LoansPage.NEW_PLEDGOR_LINK, timeWait);
		click(driver, Interfaces.LoansPage.NEW_PLEDGOR_LINK);
		sleep();
	}

	/**
	 * switch To New Pledgor Entity Frame
	 */
	public WebDriver switchToNewPledgorEntityFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_FRAME,
				timeWait);
		sleep();
		driver = driver.switchTo().frame(
				driver.findElement(By
						.xpath(Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_FRAME)));
		return driver;
	}

	/**
	 * input Pledgor Info To Create
	 */
	public void inputPledgorInfoToCreate(String accountNumber,
			String accountName, String addressNumber, String addressDictrict,
			String city, String state, String zipCode) {
		waitForControl(driver,
				Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_ACCOUNT_NUMBER_TEXTBOX,
				timeWait);
		type(driver,
				Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_ACCOUNT_NUMBER_TEXTBOX,
				accountNumber);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_ACCOUNT_NAME_TEXTBOX,
				accountName);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_ACCOUNT_ADDRESS_STREET_TEXTBOX,
				addressNumber);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_ACCOUNT_ADDRESS_DISTRICT_TEXTBOX,
				addressDictrict);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_ACCOUNT_CITY_TEXTBOX,
				city);
		sleep(1);
		selectItemCombobox(driver,
				Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_ACCOUNT_STATE_TEXTBOX,
				state);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_ACCOUNT_ZIPCODE_TEXTBOX,
				zipCode);
		sleep(1);
	}

	/**
	 * click Save New Pledgor Button
	 */
	public void clickSaveNewPledgorButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_NEW_PLEDGOR_BUTTON,
				timeWait);
		click(driver, Interfaces.LoansPage.SAVE_NEW_PLEDGOR_BUTTON);
		sleep();
	}

	/**
	 * check New Pledgor Saved
	 */
	public boolean isNewPledgorSaved(String pledgorName) {
		waitForControl(driver, Interfaces.LoansPage.PLEDGOR_COMBOBOX, timeWait);
		sleep();
		String username = getItemSelectedCombobox(driver,
				Interfaces.LoansPage.PLEDGOR_COMBOBOX);
		String fullname = getAttributeValue(driver,
				Interfaces.LoansPage.APPLICANT_FULLNAME_TEXTBOX, "value");
		return username.contains(pledgorName) && fullname.contains(pledgorName);
	}

	/**
	 * input New Login Info
	 */
	public void inputNewLoginInfo(String firstname, String lastname,
			String email) {
		waitForControl(driver,
				Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_FIRST_NAME_TEXTBOX,
				timeWait);
		type(driver,
				Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_FIRST_NAME_TEXTBOX,
				lastname);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_LAST_NAME_TEXTBOX,
				firstname);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_EMAIL_TEXTBOX,
				email);
		sleep(1);
	}

	/**
	 * open Application Entity Detail
	 */
	public void openApplicationEntityDetail(String entityName) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_APPLICANT_NAME_ON_TABLE,
				entityName, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_APPLICANT_NAME_ON_TABLE,
				entityName);
	}

	/**
	 * check Loans Page Display
	 */
	public boolean isLoansPageDisplay() {
		waitForControl(driver, Interfaces.LoansPage.LIST_OF_LOANS_TITLE,
				timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.LIST_OF_LOANS_TITLE);
	}

	/**
	 * check No Properties Message Display
	 */
	public boolean isNoPropertiesMessageDisplay() {
		waitForControl(driver, Interfaces.LoansPage.NO_PROPERTY_MESSAGE,
				timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.NO_PROPERTY_MESSAGE);
	}

	/**
	 * open Property Documents Tab
	 */
	public void openPropertyDocumentsTab() {
		waitForControl(driver, Interfaces.LoansPage.PROPERTY_DOCUMENTS_TAB,
				timeWait);
		click(driver, Interfaces.LoansPage.PROPERTY_DOCUMENTS_TAB);
		sleep();
	}

	/**
	 * check No Documents Message Display
	 */
	public boolean isNoDocumentsMessageDisplay() {
		waitForControl(driver, Interfaces.LoansPage.NO_DOCUMENT_MESSAGE,
				timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.NO_DOCUMENT_MESSAGE);
	}

	/**
	 * check No Reporting Schedules Display
	 */
	public boolean isNoReportingSchedulesDisplay() {
		waitForControl(driver,
				Interfaces.LoansPage.NO_REPORTING_SCHEDULE_MESSAGE, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.NO_REPORTING_SCHEDULE_MESSAGE);
	}

	/**
	 * check Map Page Display
	 */
	public boolean isMapPageDisplay() {
		waitForControl(driver, Interfaces.LoansPage.MAP_CONTROL, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.MAP_CONTROL);
	}

	/**
	 * check No Data Tapes Display
	 */
	public boolean isNoDataTapesDisplay() {
		waitForControl(driver, Interfaces.LoansPage.NO_DATA_TAPES_MESSAGE,
				timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.NO_DATA_TAPES_MESSAGE);
	}

	/**
	 * open Map Tab
	 */
	public void openMapTab() {
		waitForControl(driver, Interfaces.LoansPage.MAP_TAB, timeWait);
		click(driver, Interfaces.LoansPage.MAP_TAB);
		sleep();
	}

	/**
	 * open Load Data Tapes Tab
	 */
	public void openLoadDataTapesTab() {
		waitForControl(driver, Interfaces.LoansPage.LOAD_DATA_TAPES_TAB,
				timeWait);
		click(driver, Interfaces.LoansPage.LOAD_DATA_TAPES_TAB);
		sleep();
	}

	/**
	 * click On Logged User Name
	 */
	public void clickOnLoggedUserName(String loggedUsername) {
		waitForControl(driver,
				Interfaces.LoansPage.USER_FULL_NAME_LOGGED,
				loggedUsername, timeWait);
		click(driver, Interfaces.LoansPage.USER_FULL_NAME_LOGGED,
				loggedUsername);
		sleep();
	}

	/**
	 * get Uploader Page Url
	 */
	public String getUploaderPageUrl() {
		waitForControl(driver, Interfaces.LoansPage.UPLOADER_DROPDOWN_ITEM,
				timeWait);
		return getAttributeValue(driver,
				Interfaces.LoansPage.UPLOADER_DROPDOWN_ITEM, "href");
	}

	/**
	 * go to Uploader Page
	 */
	public UploaderLoginPage gotoUploaderPage(String uploaderPageUrl) {
		driver.get(uploaderPageUrl);
		sleep(3);
		return PageFactory.getUploaderLoginPage(driver, ipClient);
	}

	/**
	 * open Document Detail For Type
	 */
	public void openDocumentDetailForType(String docType) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_ID_LINK,
				docType, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_ID_LINK, docType);
		sleep();
	}

	/**
	 * check Document Uploaded For Document Type
	 */
	public boolean isDocumentUploadedForDocumentType(String docFileName) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_UPLOADED_DOCUMENT_NAME,
				docFileName, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_UPLOADED_DOCUMENT_NAME,
				docFileName);
	}

	/**
	 * check Document Display Attached Document table
	 */
	public boolean isDocumentDisplayAttachedDocument(String docFileName) {
		waitForControl(
				driver,
				Interfaces.LoansPage.DYNAMIC_DOCUMENT_NAME_ON_ATTACHED_DOCUMENT,
				docFileName, timeWait);
		return isControlDisplayed(
				driver,
				Interfaces.LoansPage.DYNAMIC_DOCUMENT_NAME_ON_ATTACHED_DOCUMENT,
				docFileName);
	}

	/**
	 * click On Document List Button
	 */
	public void clickOnDocumentListButton() {
		waitForControl(driver, Interfaces.LoansPage.DOCUMENT_LIST_BUTTON,
				timeWait);
//		click(driver, Interfaces.LoansPage.DOCUMENT_LIST_BUTTON);
		executeJavaScript(driver, "document.getElementById('GotoLAList').click();");
		sleep(1);
	}

	/**
	 * create New Task
	 */
	public void createNewTask(String taskTemplate, String taskName) {
		clickNewTaskButton();
		waitForControl(driver, Interfaces.LoansPage.TASK_TEMPLATE_COMBOBOX,
				timeWait);
		selectItemCombobox(driver,
				Interfaces.LoansPage.TASK_TEMPLATE_COMBOBOX, taskTemplate);
		type(driver, Interfaces.LoansPage.TASK_NAME_TEXTBOX, taskName);
		checkTheCheckbox(driver, Interfaces.LoansPage.ALL_PROPERTIES_CHECKBOX);
		clickSaveButton();
	}

	/**
	 * click New Task Button
	 */
	public void clickNewTaskButton() {
		waitForControl(driver, Interfaces.LoansPage.NEW_TASK_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.NEW_TASK_BUTTON);
		sleep();
	}

	/**
	 * click Task List Button
	 */
	public void clickTaskListButton() {
		waitForControl(driver, Interfaces.LoansPage.TASK_LIST_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.TASK_LIST_BUTTON);
		sleep();
	}

	/**
	 * click Save Button
	 */
	public void clickSaveButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_BUTTON, timeWait);
		// click(driver, Interfaces.LoansPage.SAVE_BUTTON);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		executeJavaScript(driver, "document.getElementById('Save2').click();");
		sleep();
	}

	/**
	 * search by Task Name
	 */
	public void searchbyTaskName(String taskName) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_TASKNAME_TEXTBOX,
				timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_TASKNAME_TEXTBOX, taskName);
		clickSearchTaskButton();
	}

	/**
	 * click Search Task Button
	 */
	public void clickSearchTaskButton() {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_TASK_BUTTON,
				timeWait);
		click(driver, Interfaces.LoansPage.SEARCH_TASK_BUTTON);
		sleep();
	}

	/**
	 * check Task Display
	 */
	public boolean isTaskDisplay(String taskName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_TASK_ITEM,
				taskName, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_TASK_ITEM, taskName);
	}

	/**
	 * open Task Tab
	 */
	public void openTaskTab() {
		waitForControl(driver, Interfaces.LoansPage.TASK_TAB, timeWait);
		executeJavaScript(driver,
				"document.getElementById('TaskSearchTB').click();");
		sleep();
	}

	/**
	 * open Task Detail
	 */
	public void openTaskDetail(String taskName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_TASK_ITEM,
				taskName, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_TASK_ITEM, taskName);
		sleep();
	}

	/**
	 * open Task Property Detail
	 */
	public void openTaskPropertyDetail(String propertyName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_TASK_PROPERTY_ITEM,
				propertyName, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_TASK_PROPERTY_ITEM,
				propertyName);
		sleep();
	}

	/**
	 * add Comment For Task Level1 Value
	 */
	public void addCommentForTaskLevel1Value(String comment) {
		waitForControl(driver,
				Interfaces.LoansPage.TASK_LEVEL1_COMMENT_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.TASK_LEVEL1_COMMENT_TEXTBOX, comment);
		sleep(1);
	}

	/**
	 * add Comment For Task Level2 Value
	 */
	public void addCommentForTaskLevel2Value(String comment) {
		waitForControl(driver,
				Interfaces.LoansPage.TASK_LEVEL2_COMMENT_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.TASK_LEVEL2_COMMENT_TEXTBOX, comment);
		sleep(1);
	}

	/**
	 * add Comment For Task Level3 Value
	 */
	public void addCommentForTaskLevel3Value(String comment) {
		waitForControl(driver,
				Interfaces.LoansPage.TASK_LEVEL3_COMMENT_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.TASK_LEVEL3_COMMENT_TEXTBOX, comment);
		sleep(1);
	}

	/**
	 * click On Back To Task Button
	 */
	public void clickOnBackToTaskButton() {
		waitForControl(driver, Interfaces.LoansPage.BACK_TO_TASK_BUTTON,
				timeWait);
		click(driver, Interfaces.LoansPage.BACK_TO_TASK_BUTTON);
		sleep();
	}

	/**
	 * select Task Document For Using
	 */
	public void selectTaskDocumentForUsing(String docType) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_USE_TASK_DOCUMENT_CHECKBOX,
				docType, timeWait);
		checkTheCheckbox(driver,
				Interfaces.LoansPage.DYNAMIC_USE_TASK_DOCUMENT_CHECKBOX,
				docType);
		sleep(1);
	}

	/**
	 * check Comment Display For Level1
	 */
	public boolean isCommentDisplayForLevel1(String taskName, String comment) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_TASK_COMMENT_LEVEL1, taskName,
				comment, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_TASK_COMMENT_LEVEL1, taskName,
				comment);
	}

	/**
	 * check Comment Display For Level2
	 */
	public boolean isCommentDisplayForLevel2(String taskName, String comment) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_TASK_COMMENT_LEVEL2, taskName,
				comment, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_TASK_COMMENT_LEVEL2, taskName,
				comment);
	}

	/**
	 * check Comment Display For Level3
	 */
	public boolean isCommentDisplayForLevel3(String taskName, String comment) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_TASK_COMMENT_LEVEL3, taskName,
				comment, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_TASK_COMMENT_LEVEL3, taskName,
				comment);
	}

	/**
	 * check Document Type Display On Task Property Table
	 */
	public boolean isDocumentTypeDisplayOnTaskPropertyTable(String taskName,
			String docType) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_TASK_USED_DOCUMENT_TYPE, taskName,
				docType, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_TASK_USED_DOCUMENT_TYPE, taskName,
				docType);
	}

	/**
	 * click Appliant Link On Loan Basic Detail tab
	 */
	public void clickAppliantLinkOnBasicDetail(String applicantName) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_APPLICANT_LINK_ON_BASIC_DETAIL,
				applicantName, timeWait);
		click(driver,
				Interfaces.LoansPage.DYNAMIC_APPLICANT_LINK_ON_BASIC_DETAIL,
				applicantName);
		sleep();
	}

	/**
	 * check Applicant Detail Info Correctly
	 */
	public boolean isApplicantDetailInfoCorrectly(String applicantNumber,
			String applicantName) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_ACCOUNT_NUMBER_APPLICANT_DETAIL,
				applicantNumber, timeWait);
		boolean isAccNum = isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_ACCOUNT_NUMBER_APPLICANT_DETAIL,
				applicantNumber);
		boolean isAccName = isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_ACCOUNT_NAME_APPLICANT_DETAIL,
				applicantName);
		return isAccNum && isAccName;
	}

	/**
	 * click On Excel Icon
	 */
	public void clickOnExcelIcon() {
		waitForControl(driver, Interfaces.LoansPage.EXCEL_ICON, timeWait);
		click(driver, Interfaces.LoansPage.EXCEL_ICON);
		sleep();
	}

	/**
	 * input Applicant Info To Create For Loans
	 */
	public void inputApplicantInfoToCreateForLoans(String accountNumber,
			String accountName, String addressNumber, String addressDictrict,
			String city, String state, String zipCode, String loginName) {
		waitForControl(driver,
				Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_NUMBER_TEXTBOX,
				timeWait);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_NUMBER_TEXTBOX,
				accountNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_NAME_TEXTBOX,
				accountName);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_ADDRESS_STREET_TEXTBOX,
				addressNumber);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_ADDRESS_DISTRICT_TEXTBOX,
				addressDictrict);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_CITY_TEXTBOX,
				city);
		sleep(1);
		selectItemCombobox(driver,
				Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_STATE_TEXTBOX, state);
		sleep(1);
		type(driver,
				Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_ZIPCODE_TEXTBOX,
				zipCode);
		sleep(1);
		selectItemCombobox(driver,
				Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_LOGIN_TEXTBOX,
				loginName);
		sleep(1);
	}

	/**
	 * check Legal name display
	 */
	public boolean isLegalNameDisplay(String legalName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_LEGAL_NAME,
				legalName, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_LEGAL_NAME, legalName);
	}

	/**
	 * Select Legal Name
	 */
	public void selectLegalName(String legalName) {
		int i = 0;
		while (i < 100) {
			if (isControlDisplayed(driver, Interfaces.LoansPage.LAST_LINK)) {
				waitForControl(driver, Interfaces.LoansPage.LAST_LINK, timeWait);
				click(driver, Interfaces.LoansPage.LAST_LINK);
				sleep();
				if (isControlDisplayed(driver,
						Interfaces.LoansPage.PREVIOUS_LINK)) {
					waitForControl(driver,
							Interfaces.LoansPage.DYNAMIC_LEGAL_NAME, legalName,
							timeWait);
					click(driver, Interfaces.LoansPage.DYNAMIC_LEGAL_NAME,
							legalName);
					sleep();
					i = 100;
				}
				i = i + 1;
			} else {
				waitForControl(driver, Interfaces.LoansPage.DYNAMIC_LEGAL_NAME,
						legalName, timeWait);
				click(driver, Interfaces.LoansPage.DYNAMIC_LEGAL_NAME,
						legalName);
				sleep();
				i = 100;
			}

		}
	}

	/**
	 * check Applicant name display
	 */
	public boolean isApplicantNameNameDisplay(String legalName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_APPLICANT_NAME,
				legalName, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_APPLICANT_NAME, legalName);
	}

	/**
	 * click on Address link
	 */
	public void clickPropertiesAddressLink(String addressName) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_PROPERTIES_ADDRESS_LINK,
				addressName, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_PROPERTIES_ADDRESS_LINK,
				addressName);
		sleep();
	}

	/**
	 * click on New Document link
	 */
	public void clickNewDocument() {
		waitForControl(driver, Interfaces.LoansPage.NEW_DOCUMENTS_LINK,
				timeWait);
		click(driver, Interfaces.LoansPage.NEW_DOCUMENTS_LINK);
		sleep();
	}

	/**
	 * add new a Properties Document
	 */
	public void addNewPropertiesDocument(String documentType,
			String documentFile, String isprivate) {
		waitForControl(driver,
				Interfaces.LoansPage.PROPERTIES_DOCUMENTS_TYPE_COMBOBOX,
				timeWait);
		selectItemCombobox(driver,
				Interfaces.LoansPage.PROPERTIES_DOCUMENTS_TYPE_COMBOBOX,
				documentType);
		sleep(1);
		uploadFileDocument(documentFile);
		sleep();
		if (isprivate == "yes") {
			click(driver,
					Interfaces.LoansPage.PROPERTIES_DOCUMENTS_PRIVATE_CHECKBOX);
		}
		sleep();
		click(driver, Interfaces.LoansPage.SAVE_BUTTON);
		sleep();
	}

	/**
	 * check contents of Loan Applicantion Details are read-only
	 */
	public boolean isReadOnlyLoanApplicantionDetails() {

		boolean readonly = false;
		boolean bool1 = isControlDisplayed(driver,
				Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL);
		boolean bool2 = isControlDisplayed(driver,
				Interfaces.LoansPage.LOAN_STATUS_COMBOBOX_BASIC_DETAIL);

		if ((bool1 == false) && (bool2 == false)) {
			readonly = true;
		}
		return readonly;
	}

	/**
	 * open Loan Application Entities link
	 */
	public void openLoansApplicationEntities(String entityName) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_LOAN_APPLICATION_ENTITY_LINK,
				entityName, timeWait);
		click(driver,
				Interfaces.LoansPage.DYNAMIC_LOAN_APPLICATION_ENTITY_LINK,
				entityName);
		sleep();
	}

	/**
	 * check Applicant should not be able to add, delete, edit entities
	 */
	public boolean isReadOnlyApplicant() {

		boolean readonly = false;
		boolean bool = isControlDisplayed(driver,
				Interfaces.LoansPage.NEW_APPLICANT_LINK);
		if (bool == false) {
			readonly = true;
		}
		return readonly;
	}

	/**
	 * check properties display on List of Properties of Borrower
	 */
	public boolean isPropertyDisplayBorrower(String address, String city,
			String state) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_PROPERTY_ITEM_BORROWER, address,
				city, state, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_PROPERTY_ITEM_BORROWER, address,
				city, state);
	}

	/**
	 * check Document Display Attached Document table
	 */
	public boolean isDocumentNotDisplayInPropertiesTab(String docFileName) {
		boolean display = false;
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_DOCUMENT_NAME_IN_PROPERTIES_TAB,
				docFileName, timeWait);
		boolean bool = isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_DOCUMENT_NAME_IN_PROPERTIES_TAB,
				docFileName);
		if (bool == false) {
			display = true;
		}
		return display;
	}

	/**
	 * click a Document In Properties Tab
	 */
	public void clickDocumentInPropertiesTab(String docFileName) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_DOCUMENT_NAME_IN_PROPERTIES_TAB,
				docFileName, timeWait);
		click(driver,
				Interfaces.LoansPage.DYNAMIC_DOCUMENT_NAME_IN_PROPERTIES_TAB,
				docFileName);
	}

	/**
	 * upload File Properties Document
	 */
	public void uploadFileDocument(String documentFileName) {
		sleep(4);
		if (driver.toString().toLowerCase().contains("chrome")) {
			click(driver, Interfaces.LoansPage.CHOOSE_FILE_DOCUMENT_BUTTON);
		} else if (driver.toString().toLowerCase().contains("firefox")) {
			click(driver, Interfaces.LoansPage.BROWSE_DOCUMENT_BUTTON);
		} else if (driver.toString().toLowerCase().contains("ie")) {
			click(driver, Interfaces.LoansPage.BROWSE_DOCUMENT_BUTTON);
			keyPressing("space");
		}
		sleep(1);
		Common.getCommon().openFileForUpload(driver, documentFileName);
		sleep();
	}

	/**
	 * Click Go To Property button
	 */
	public void clickGoToPropertyButton() {
		waitForControl(driver, Interfaces.LoansPage.GO_TO_PROPERTY_BUTTON,
				timeWait);
		click(driver, Interfaces.LoansPage.GO_TO_PROPERTY_BUTTON);
	}

	/**
	 * check Document Display Attached Document table
	 */
	public boolean isDocumentFileUploaded(String docType, String docFileName) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_DOCUMENT_NAME_IN_DOCUMENT_TABLE,
				docType, docFileName, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_DOCUMENT_NAME_IN_DOCUMENT_TABLE,
				docType, docFileName);
	}

	/**
	 * Click on Private checkbox
	 */
	public void clickPrivateCheckbox() {
		waitForControl(driver,
				Interfaces.LoansPage.PROPERTIES_DOCUMENTS_PRIVATE_CHECKBOX,
				timeWait);
		click(driver,
				Interfaces.LoansPage.PROPERTIES_DOCUMENTS_PRIVATE_CHECKBOX);
	}

	/**
	 * input Applicant Info To Create
	 */
	public void inputApplicantInfoToCreate(String accountNumber,
			String accountName, String addressNumber, String addressDictrict,
			String city, String state, String zipCode, String loginName) {
		waitForControl(driver, Interfaces.LoansPage.ACCOUNT_NUMBER_TEXTBOX,
				timeWait);
		type(driver, Interfaces.LoansPage.ACCOUNT_NUMBER_TEXTBOX, accountNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_NAME_TEXTBOX, accountName);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_ADDRESS_STREET_TEXTBOX,
				addressNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_ADDRESS_DISTRICT_TEXTBOX,
				addressDictrict);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_CITY_TEXTBOX, city);
		sleep(1);
		selectItemCombobox(driver, Interfaces.LoansPage.ACCOUNT_STATE_TEXTBOX,
				state);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_ZIPCODE_TEXTBOX, zipCode);
		sleep(1);
		selectItemCombobox(driver, Interfaces.LoansPage.CHOOSE_LOGIN_COMBOBOX,
				loginName);
		sleep(1);
	}

	/**
	 * check Loans Display With Correct Info
	 */
	public boolean isLoanApplicationEntityDisplayWithCorrectInfo(
			String entityName, String applicantName) {
		boolean bool = false;
		waitForControl(driver, Interfaces.LoansPage.ENTITY_TYPE_COMBOBOX,
				timeWait);
		boolean bool1 = isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_ENTITY_TYPE_OPTION, entityName);
		boolean bool2 = isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_APPLICANT_OPTION, applicantName);
		if (bool1 == true && bool2 == true) {
			return bool = true;
		}
		return bool;
	}

	/**
	 * check New Applicant Display On LAE Table
	 */
	public boolean isLoanApplicationEntityDisplayOnTable(String applicantName) {
		waitForControl(driver, Interfaces.LoansPage.APPLICANT_ENTITY_ROW,
				applicantName, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.APPLICANT_ENTITY_ROW, applicantName);
	}

	/**
	 * check Applicant Saved In New Loan Form
	 */
	public boolean isApplicantSavedInNewLoanForm(String applicantName) {
		waitForControl(driver, Interfaces.LoansPage.APPLICANT_COMBOBOX,
				timeWait);
		String realName = getItemSelectedCombobox(driver,
				Interfaces.LoansPage.APPLICANT_COMBOBOX);
		return realName.contains(applicantName);
	}

	/**
	 * check number of Item Of Source Combobox On Add New Loan
	 */
	public boolean isItemOfSourceComboboxOnAddNewLoan(int numberItem) {
		waitForControl(driver,
				Interfaces.LoansPage.SOURCE_COMBOBOX_ADD_NEW_LOAN, timeWait);
		int realNumber = countElement(driver,
				Interfaces.LoansPage.OPTION_SOURCE_COMBOBOX_ADD_NEW_LOAN) - 1;
		return realNumber == numberItem;
	}

	/**
	 * check Item Exist In Source Combobox On Add New Loan
	 */
	public boolean isItemExistInSourceComboboxOnAddNewLoan(String item) {
		boolean returnValue = false;
		waitForControl(driver,
				Interfaces.LoansPage.SOURCE_COMBOBOX_ADD_NEW_LOAN, timeWait);
		int realNumber = countElement(driver,
				Interfaces.LoansPage.OPTION_SOURCE_COMBOBOX_ADD_NEW_LOAN);
		if (realNumber == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver,
				Interfaces.LoansPage.OPTION_SOURCE_COMBOBOX_ADD_NEW_LOAN);
		for (WebElement element : elements) {
			String text = element.getText();
			if (text.contains(item)) {
				returnValue = true;
				break;
			}
		}
		return returnValue;
	}

	/**
	 * edit Loan Info
	 */
	public void editLoanInfo(String companyType, String email) {
		waitForControl(driver, Interfaces.LoansPage.COMPANY_TYPE_COMBOBOX,
				timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.COMPANY_TYPE_COMBOBOX,
				companyType);
		sleep(1);
		type(driver, Interfaces.LoansPage.EMAIL_TEXTBOX_BASIC_DETAIL, email);
		sleep(1);
	}

	/**
	 * edit Loan Info
	 */
	public void editLoanBasicDetailInfo(String companyType) {
		waitForControl(driver, Interfaces.LoansPage.COMPANY_TYPE_COMBOBOX,
				timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.COMPANY_TYPE_COMBOBOX,
				companyType);
		sleep(1);
	}

	/**
	 * check Error Message About Invalid Email Display
	 */
	public boolean isErrorMessageAboutInvalidEmailDisplay() {
		waitForControl(driver,
				Interfaces.LoansPage.INVALID_EMAIL_ERROR_MESSAGE, 5);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.INVALID_EMAIL_ERROR_MESSAGE);
	}

	/**
	 * check Save Success Message Display
	 */
	public boolean isSaveSuccessMessageDisplay() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_SUCCESS_MESSAGE, 5);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.SAVE_SUCCESS_MESSAGE);
	}

	/**
	 * check number of Item Of Source Combobox On Basic Detail Tab
	 */
	public boolean isItemOfSourceComboboxOnBasicDetailTab(int numberItem) {
		waitForControl(driver,
				Interfaces.LoansPage.SOURCE_COMBOBOX_BASIC_DETAIL, timeWait);
		int realNumber = countElement(driver,
				Interfaces.LoansPage.OPTION_SOURCE_COMBOBOX_BASIC_DETAIL) - 1;
		return realNumber == numberItem;
	}

	/**
	 * check Item Exist In Source Combobox On Basic Detail Tab
	 */
	public boolean isItemExistInSourceComboboxOnBasicDetailTab(String item) {
		boolean returnValue = false;
		waitForControl(driver,
				Interfaces.LoansPage.SOURCE_COMBOBOX_BASIC_DETAIL, timeWait);
		int realNumber = countElement(driver,
				Interfaces.LoansPage.OPTION_SOURCE_COMBOBOX_BASIC_DETAIL);
		if (realNumber == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver,
				Interfaces.LoansPage.OPTION_SOURCE_COMBOBOX_BASIC_DETAIL);
		for (WebElement element : elements) {
			String text = element.getText();
			if (text.contains(item)) {
				returnValue = true;
				break;
			}
		}
		return returnValue;
	}

	// /**
	// * check Loan Display On Dashboard
	// */
	// public boolean isLoanDisplayOnDashboard(String loanName) {
	// waitForControl(driver, Interfaces.LoansPage.DYNAMIC_LOAN_ITEM_DASHBOARD,
	// loanName, timeWait);
	// return isControlDisplayed(driver,
	// Interfaces.LoansPage.DYNAMIC_LOAN_ITEM_DASHBOARD, loanName);
	// }

	/**
	 * check Loans Display On Search Loan Page
	 */
	public boolean isLoansDisplayOnSearchLoanPage(String loanName) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_LOAN_ITEM_SEARCH_LOAN_PAGE,
				loanName, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_LOAN_ITEM_SEARCH_LOAN_PAGE,
				loanName);
	}

	/**
	 * select Member In Basic Detail Team
	 */
	public void selectMemberInBasicDetailTeam(String member) {
		waitForControl(driver,
				Interfaces.LoansPage.MEMBER_COMBOBOX_BASIC_DETAIL_TEAM,
				timeWait);
		selectItemCombobox(driver,
				Interfaces.LoansPage.MEMBER_COMBOBOX_BASIC_DETAIL_TEAM, member);
		sleep(1);
	}

	/**
	 * check Member Display On Team Table
	 */
	public boolean isMemberDisplayOnTeamTable(String member) {
		waitForControl(driver,
				Interfaces.LoansPage.MEMBER_COMBOBOX_BASIC_DETAIL_TEAM,
				timeWait);
		String addedMember = getItemSelectedCombobox(driver,
				Interfaces.LoansPage.MEMBER_COMBOBOX_BASIC_DETAIL_TEAM);
		return addedMember.contains(member);
	}

	/**
	 * check second Member Display On Team Table
	 */
	public boolean isSecondMemberDisplayOnTeamTable(String member) {
		waitForControl(driver,
				Interfaces.LoansPage.SECOND_MEMBER_COMBOBOX_BASIC_DETAIL_TEAM,
				timeWait);
		String addedMember = getItemSelectedCombobox(driver,
				Interfaces.LoansPage.SECOND_MEMBER_COMBOBOX_BASIC_DETAIL_TEAM);
		return addedMember.contains(member);
	}

	/**
	 * click On Missing Document Summary Button
	 */
	public void clickOnMissingDocumentSummaryButton() {
		waitForControl(driver,	Interfaces.LoansPage.MISSING_DOCUMENT_SUMMARY_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.MISSING_DOCUMENT_SUMMARY_BUTTON);
		sleep();
	}

	/**
	 * get Number Of Property in Properties Tab
	 */
	public int getNumberOfPropertyInPropertiesTab() {
		waitForControl(driver, Interfaces.LoansPage.TOTAL_PROPERTY_LABEL,timeWait);
		String text = getText(driver, Interfaces.LoansPage.TOTAL_PROPERTY_LABEL);
		String[] subString = text.split(" ");
		int result = Integer.parseInt(subString[subString.length - 1]);
		return result;
	}


	/**
	 * get Number Documents Required Of Property In Properties Tab
	 */
	public int getNumberDocumentsRequiredOfPropertyInPropertiesTab() {
		waitForControl(driver, Interfaces.LoansPage.TOTAL_REQUIRED_DOCS_PROPERTY_LABEL, timeWait);
		String text = getText(driver, Interfaces.LoansPage.TOTAL_REQUIRED_DOCS_PROPERTY_LABEL);
		String[] subString = text.split(" ");
		int result = Integer.parseInt(subString[subString.length - 1]);
		return result;
	}

	/**
	 * check Number Of Property Display Correct
	 */
	public boolean isNumberOfPropertyDisplayCorrect(int input) {
		waitForControl(driver,
				Interfaces.LoansPage.PROPERTY_BASIC_DETAIL_FIELD, timeWait);
		String text = getText(driver,
				Interfaces.LoansPage.PROPERTY_BASIC_DETAIL_FIELD);
		int result = Integer.parseInt(text);
		return result == input;
	}

	/**
	 * check Property Info Correctly
	 */
	public boolean isPropertyInfoCorrectly(String adress, String city,
			String state, String zipCode, String propertyType) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_PROPERTY_INFOR,
				adress, city, state, zipCode, propertyType, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_PROPERTY_INFOR, adress, city,
				state, zipCode, propertyType);
	}

	/**
	 * click Export Datatape Button
	 */
	public void clickExportDatatapeButton() {
		waitForControl(driver, Interfaces.LoansPage.EXPORT_DATATAPE_BUTTON,
				timeWait);
		click(driver, Interfaces.LoansPage.EXPORT_DATATAPE_BUTTON);
		sleep();
		click(driver, Interfaces.LoansPage.OK_EXPORT_DATATAPE_BUTTON);
		sleep();
	}

	/**
	 * get Number Of Document In Property Documents Tab
	 */
	public int getNumberOfDocumentInPropertyDocumentsTab() {
		waitForControl(driver, Interfaces.LoansPage.TOTAL_DOCUMENT_LABEL,
				timeWait);
		String text = getText(driver, Interfaces.LoansPage.TOTAL_DOCUMENT_LABEL);
		String[] subString = text.split(" ");
		int result = Integer.parseInt(subString[subString.length - 1]);
		return result;
	}

	/**
	 * check Applicant Exist In Drop down List On Add New Loan
	 */
	public boolean isApplicantExistInDropdownListOnAddNewLoan(
			String applicantName) {
		boolean returnValue = false;
		waitForControl(driver, Interfaces.LoansPage.APPLICANT_COMBOBOX,
				timeWait);
		int realNumber = countElement(driver,
				Interfaces.LoansPage.APPLICANT_COMBOBOX_OPTION);
		if (realNumber == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver,
				Interfaces.LoansPage.APPLICANT_COMBOBOX_OPTION);
		for (WebElement element : elements) {
			String text = element.getText();
			if (text.contains(applicantName)) {
				returnValue = true;
				break;
			}
		}
		return returnValue;
	}

	/**
	 * click New Team Member Button
	 */
	public void clickNewTeamMemberButton() {
		waitForControl(driver, Interfaces.LoansPage.NEW_TEAM_MEMBER_BUTTON,
				timeWait);
		click(driver, Interfaces.LoansPage.NEW_TEAM_MEMBER_BUTTON);
		sleep();
	}

	/**
	 * select second Member In Basic Detail Team
	 */
	public void selectSecondMemberInBasicDetailTeam(String member) {
		waitForControl(driver,
				Interfaces.LoansPage.SECOND_MEMBER_COMBOBOX_BASIC_DETAIL_TEAM,
				timeWait);
		selectItemCombobox(driver,
				Interfaces.LoansPage.SECOND_MEMBER_COMBOBOX_BASIC_DETAIL_TEAM,
				member);
		sleep(1);
	}

	/**
	 * remove Member From Loan Team
	 */
	public void removeMemberFromLoanTeam(int index) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_DELETE_TEAM_MEMBER_CHECKBOX, index
						+ "", timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_DELETE_TEAM_MEMBER_CHECKBOX,
				index + "");
		sleep(1);
		clickSaveButton();
		sleep();
	}

	/**
	 * delete Second Entity In Table
	 */
	public void deleteEntityInTable(String entity) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_ENTITY_CHECKBOX_IN_TABLE, entity,
				timeWait);
		checkTheCheckbox(driver,
				Interfaces.LoansPage.DYNAMIC_ENTITY_CHECKBOX_IN_TABLE, entity);
		sleep(1);
		clickSaveButton();
		sleep();
	}

	/**
	 * check Parents Property Display Correctly
	 */
	public boolean isParentsPropertyDisplayCorrectly(String parentAddress) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_PARENT_PROPERTY_ITEM,
				parentAddress, timeWait);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_PARENT_PROPERTY_ITEM,
				parentAddress);
	}

	/**
	 * select Loan Status Basic Detail
	 */
	public void selectLoanStatusBasicDetail(String loanStatus) {
		waitForControl(driver,
				Interfaces.LoansPage.LOAN_STATUS_COMBOBOX_BASIC_DETAIL,
				timeWait);
		selectItemCombobox(driver,
				Interfaces.LoansPage.LOAN_STATUS_COMBOBOX_BASIC_DETAIL,
				loanStatus);
		sleep(1);
	}

	/**
	 * get Document Number Of Property
	 */
	public int getDocumentNumberOfProperty(String property) {
		searchAddress(property);
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_DOCUMENT_NUMBER_OF_PROPERTY,
				property, timeWait);
		String text = getText(driver,
				Interfaces.LoansPage.DYNAMIC_DOCUMENT_NUMBER_OF_PROPERTY,
				property);
		String[] subString = text.split(" ");
		int result = Integer.parseInt(subString[0]);
		return result;
	}

	/**
	 * check Applicant Exist In Dropdown List On New Loan Application Entity
	 */
	public boolean isApplicantExistInDropdownListOnNewLoanApplicationEntity(
			String applicantName) {
		boolean returnValue = false;
		waitForControl(driver, Interfaces.LoansPage.APPLICANT_ENTITY_COMBOBOX,
				timeWait);
		int realNumber = countElement(driver,
				Interfaces.LoansPage.APPLICANT_ENTITY_COMBOBOX_OPTION);
		if (realNumber == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver,
				Interfaces.LoansPage.APPLICANT_ENTITY_COMBOBOX_OPTION);
		for (WebElement element : elements) {
			String text = element.getText();
			if (text.contains(applicantName)) {
				returnValue = true;
				break;
			}
		}
		return returnValue;
	}

	/**
	 * check Applicant Exist In Dropdown List On Loan Properties tab
	 */
	public boolean isApplicantExistInDropdownListOnLoanPropertiesTab(
			String applicantName) {
		boolean returnValue = false;
		waitForControl(driver,
				Interfaces.LoansPage.LOAN_PROPERTY_APPLICANT_COMBOBOX, timeWait);
		int realNumber = countElement(driver,
				Interfaces.LoansPage.LOAN_PROPERTY_APPLICANT_COMBOBOX_OPTION);
		if (realNumber == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver,
				Interfaces.LoansPage.LOAN_PROPERTY_APPLICANT_COMBOBOX_OPTION);
		for (WebElement element : elements) {
			String text = element.getText();
			if (text.contains(applicantName)) {
				returnValue = true;
				break;
			}
		}
		return returnValue;
	}

	/**
	 * check Property Added To Parent
	 */
	public boolean isPropertyAddedToParent(String address, String parent) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_ADDED_PROPERTY_TO_PARENT, address,
				parent, 5);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_ADDED_PROPERTY_TO_PARENT, address,
				parent);
	}

	/**
	 * check Property Document Display
	 */
	public boolean isPropertyDocumentDisplay(String address, String documentType) {
		waitForControl(driver,
				Interfaces.LoansPage.DYNAMIC_PROPERTY_DOCUMENT_WITH_TYPE,
				address, documentType, 5);
		return isControlDisplayed(driver,
				Interfaces.LoansPage.DYNAMIC_PROPERTY_DOCUMENT_WITH_TYPE,
				address, documentType);
	}

	/**
	 * check Applicant Exist In Login Dropdown List On New Loan
	 *        Application Entity
	 */
	public boolean isApplicantExistInLoginDropdownListOnNewLoanApplicationEntity(
			String applicantName) {
		boolean returnValue = false;
		waitForControl(driver, Interfaces.LoansPage.LOGIN_COMBOBOX, timeWait);
		int realNumber = countElement(driver,
				Interfaces.LoansPage.LOGIN_COMBOBOX);
		if (realNumber == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver,
				Interfaces.LoansPage.LOGIN_COMBOBOX_OPTION);
		for (WebElement element : elements) {
			String text = element.getText();
			if (text.contains(applicantName)) {
				returnValue = true;
				break;
			}
		}
		return returnValue;
	}
	
	/**
	 * check Document Loaded To Document Column
	 */
	public boolean isDocumentLoadedToDocumentColumn(String address, String documentColumn) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_FOR_DOCUMENTS_COLUMN,
				address, documentColumn, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_FOR_DOCUMENTS_COLUMN,
				address, documentColumn);
	}
	
	/**
	 * check 'Date Created' field not display
	 */
	public boolean isDateCreatedNotDisplay() {
		waitForControl(driver, Interfaces.LoansPage.DATE_CREATED_TEXTBOX,
				timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DATE_CREATED_TEXTBOX);
	}
	
	/**
	 * calculate total loans
	 */
	public int calculateTotalLoans() {
		sleep(5);
		waitForControl(driver, Interfaces.FNFLoanDashboardPage.FNF_BUCKET_LIST, timeWait);
		int numberOfBucket = countElement(driver, Interfaces.FNFLoanDashboardPage.FNF_BUCKET_LIST);
		int numberOfRegularLoans = 0;
		int numberOfModLoans = 0;
		int totalLoans = 0;
		String index;
		for(int i = 1; i<= numberOfBucket; i++){
			index = Integer.toString(i);
			numberOfRegularLoans = Integer.parseInt(getText(driver, Interfaces.FNFLoanDashboardPage.FNF_DYNAMIC_BUCKET_TOTAL_LOANS, index).replace(" ", "").replace("TotalLoans", ""));
			if(isControlDisplayed(driver, Interfaces.FNFLoanDashboardPage.FNF_DYNAMIC_BUCKET_TOTAL_MOD_LOANS, index))
			numberOfModLoans = Integer.parseInt(getText(driver, Interfaces.FNFLoanDashboardPage.FNF_DYNAMIC_BUCKET_TOTAL_MOD_LOANS, index).replace(" ", "").replace("TotalModLoans", ""));
			else numberOfModLoans = 0;
			totalLoans = totalLoans + numberOfRegularLoans + numberOfModLoans;
		}
		return totalLoans;
	}
	
	/**
	 * calculate total loans
	 */
	public int calculateNumbersOfLoanInOneBucket(String index) {
		waitForControl(driver, Interfaces.FNFLoanDashboardPage.FNF_DYNAMIC_BUCKET_TOTAL_LOANS, index, timeWait);
		int numberOfRegularLoans;
		int numberOfModLoans;
		int totalLoans;
		numberOfRegularLoans = Integer.parseInt(getText(driver, Interfaces.FNFLoanDashboardPage.FNF_DYNAMIC_BUCKET_TOTAL_LOANS, index).replace(" ", "").replace("TotalLoans", ""));
		if(isControlDisplayed(driver, Interfaces.FNFLoanDashboardPage.FNF_DYNAMIC_BUCKET_TOTAL_MOD_LOANS, index))
		numberOfModLoans = Integer.parseInt(getText(driver, Interfaces.FNFLoanDashboardPage.FNF_DYNAMIC_BUCKET_TOTAL_MOD_LOANS, index).replace(" ", "").replace("TotalModLoans", ""));
		else numberOfModLoans = 0;		
		totalLoans = numberOfRegularLoans + numberOfModLoans;
		return totalLoans;
	}
	
	/**
	 * get Number of loans displayed
	 */
	public int getNumberOfLoansDisplayed() {
		sleep();
		waitForControl(driver, Interfaces.FNFLoanDashboardPage.FNF_ALL_LOANS_STATUS, timeWait);
		return countElement(driver, Interfaces.FNFLoanDashboardPage.FNF_ALL_LOANS_STATUS) - countElement(driver, Interfaces.FNFLoanDashboardPage.FNF_ALL_LOANS_WITHOUT_STATUS) - countElement(driver, Interfaces.FNFLoanDashboardPage.FNF_DYNAMIC_LOANS_STATUS, "Closed Loan Mod");
	}
	
	/**
	 * get Number of loans displayed
	 */
	public int getNumberOfLoansDisplayedInBucket() {
		waitForControl(driver, Interfaces.FNFLoanDashboardPage.FNF_ALL_LOANS_STATUS, timeWait);
		return countElement(driver, Interfaces.FNFLoanDashboardPage.FNF_ALL_LOANS_STATUS);
	}
	
	/**
	 * get Number of Loands displayed in one bucket
	 */
	public int getNumberOfLoansInOneBucketDisplayed(String status) {
		status = convertToValidString(status);
		waitForControl(driver, Interfaces.FNFLoanDashboardPage.FNF_DYNAMIC_LOANS_STATUS, status, timeWait);
		return countElement(driver, Interfaces.FNFLoanDashboardPage.FNF_DYNAMIC_LOANS_STATUS, status);
	}
	
	
	
	/**
	 * check Loans Displays In Correct Bucket
	 */
	public boolean checkLoansDisplaysInBucketCorrrectly() {
		waitForControl(driver, Interfaces.FNFLoanDashboardPage.FNF_BUCKET_LIST, timeWait);
		int numberOfBucket = countElement(driver, Interfaces.FNFLoanDashboardPage.FNF_BUCKET_LIST);
		String index, bucketName;
		int numberOfLoan;
		for(int i = 1; i<= numberOfBucket; i++){
			index = Integer.toString(i);
			bucketName = getText(driver, Interfaces.FNFLoanDashboardPage.FNF_DYNAMIC_BUCKET_NAME, index).trim();
			System.out.println("Start with bucket: "+ bucketName);
			click(driver, Interfaces.FNFLoanDashboardPage.FNF_DYNAMIC_BUCKET_LINK, index);
			sleep(5);
			numberOfLoan = getNumberOfLoansDisplayedInBucket();
			if((calculateNumbersOfLoanInOneBucket(index) != numberOfLoan) || (numberOfLoan != getNumberOfLoansInOneBucketDisplayed(bucketName))) {
				System.out.println("Failed at bucket: "+ bucketName);
				return false;
			}
		}
		return true;
	}
	
	public String convertToValidString(String value){
		String validString;
		switch (value) {
        case "INVITED":  validString = "Invited";
                 break;
        case "INITIAL REVIEW":  validString = "Initial Review";
                 break;
        case "TERM-SHEET SENT":  validString = "Term-Sheet Sent";
                 break;
        case "UNDERWRITING":  validString = "Underwriting";
                 break;
        case "COMMITTEE REVIEW":  validString = "Committee Review";
                 break;
        case "DOCUMENTATION":  validString = "Documentation";
                 break;
        case "INVESTMENT PERIOD":  validString = "Investment Period";
                 break;
        case "REJECTED":  validString = "Rejected";
                 break;
        case "EXPIRED":  validString = "Expired";
                 break;
        case "MATURED": validString = "Matured";
                 break;
        case "UNKNOWN STATUS": validString = "Unknown Status";
                 break;
        case "ON HOLD": validString = "On Hold";
                 break;
        default: validString = "Invalid status";
                 break;
    }
		return validString;
	}
	
	private WebDriver driver;
	private String ipClient;
}