package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_019_FNF_SearchBorrower extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
	}


	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search borrower by Company Name")
	public void SearchBorrower_01_Lender_SearchByCompanyName() {

		log.info("SearchBorrower_01 - Step 01. Logout and login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("SearchBorrower_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("SearchBorrower_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("SearchBorrower_01 - Step 04: Get all record values");
		email = applicantsPage.getRecordValue("Email");
		phone = applicantsPage.getRecordValue("Phone");
		dateInvited = applicantsPage.getRecordValue("Date Invited");
		status = applicantsPage.getRecordValue("Active");
		
		log.info("VP: Borrower information displayed correctly");
		verifyTrue(applicantsPage.isAllRecordValuesDisplayedCorrectly(companyName, email, dateInvited, phone, status));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search borrower by Email")
	public void SearchBorrower_02_Lender_SearchByEmail() {

		log.info("SearchBorrower_02 - Step 01: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("SearchBorrower_02 - Step 02: Input 'Email'");
		applicantsPage.inputEmail(email);
	
		log.info("SearchBorrower_02 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("VP: Borrower information displayed correctly");
		verifyTrue(applicantsPage.isAllRecordValuesDisplayedCorrectly(companyName, email, dateInvited, phone, status));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search borrower by Date Invited")
	public void SearchBorrower_03_Lender_SearchByDateInvited() {

		log.info("SearchBorrower_03 - Step 01: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("SearchBorrower_03 - Step 02: Input 'Date invited' ");
		applicantsPage.inputDateInvited(dateInvited);
	
		log.info("SearchBorrower_03 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("VP: Borrower information displayed correctly");
		verifyTrue(applicantsPage.isAllRecordValuesDisplayedCorrectly(companyName, email, dateInvited, phone, status));
	}
	
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search borrower by Date Invited")
	public void SearchBorrower_04_Lender_SearchByPhoneNumber() {

		log.info("SearchBorrower_04 - Step 01: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("SearchBorrower_04 - Step 02: Input 'Date invited' ");
		applicantsPage.inputDateInvited(dateInvited);
	
		log.info("SearchBorrower_04 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("VP: Borrower information displayed correctly");
		verifyTrue(applicantsPage.isAllRecordValuesDisplayedCorrectly(companyName, email, dateInvited, phone, status));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Search borrower by Active status")
	public void SearchBorrower_05_Lender_SearchByStatus() {

		log.info("SearchBorrower_05 - Step 01: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("SearchBorrower_05 - Step 02: Select 'Status' ");
		applicantsPage.selectActiveRadioButton(status);
	
		log.info("SearchBorrower_05 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("VP: Borrower information displayed correctly");
		verifyTrue(applicantsPage.isAllRecordValuesDisplayedCorrectly(companyName, email, dateInvited, phone, status));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private String FNFLenderUsername, FNFLenderPassword;
	private String companyName, accountNumber;
	private String email, phone, dateInvited, status;
	}