package common;

import org.openqa.selenium.WebDriver;

public class Constant  extends AbstractTest{
	// CAF - Borrower - Applicant
	public static final String USERNAME_CAF_BORROWER = "CAF.Dorner";
	public static final String PASSWORD_CAF_BORROWER = "change";
	
	// CAF - Borrower - Applicant - Second
	public static final String USERNAME_CAF_BORROWER_2 = "CAF.Dorner.Auto";
	public static final String PASSWORD_CAF_BORROWER_2 = "CAF.Dorner.Auto";

	// B2R - Borrower - Applicant - Second
	public static final String USERNAME_B2R_BORROWER_2 = "dbb2rauto";
	public static final String PASSWORD_B2R_BORROWER_2 = "dbb2rauto";

	// FNF - Lender
	public static final String USERNAME_FNF_LENDER = "caffnflender";
	public static final String PASSWORD_FNF_LENDER = "caffnf555";

	// Store - Lender
	public static final String USERNAME_STORE_LENDER = "storeauto";
	public static final String PASSWORD_STORE_LENDER = "storeauto";

	// Status CAF - B2R
	public static final String LOAN_STATUS_B2R = "Pre-RentRoll";
	public static final String LOAN_STATUS_CAF = "Term Sheet Issued";
	
	// Sales Force
	public static final String USERNAME_SALESFORCE = "uzi@preciseres.com.dev";
	public static final String PASSWORD_SALESFORCE = "exryd6723";
	
	//********************LINK UPLOADER PAGE*******************//
//	public static final String UPLOADER_PAGE_URL = "https://staging.preciseres.com/FileUploader/Login.aspx";
	public static final String UPLOADER_PAGE_URL = "https://preprod.preciseres.com/FileUploader/Login.aspx";
//	public static final String UPLOADER_PAGE_URL = "https://live.preciseres.com/FileUploader/Login.aspx";
	
//	********************STAGING*******************//
//	 All - Admin - CAF, B2R, FKL
//	public static final String USERNAME_ADMIN = "samark";
//	public static final String PASSWORD_ADMIN = "Boss555";
//	
//	// B2R -Lender
//	public static final String USERNAME_B2R_LENDER = "dlb2r";
//	public static final String PASSWORD_B2R_LENDER = "dlb2r555";
//	
//	// B2R -Audit
//	public static final String USERNAME_B2R_AUDIT = "dlb2r_audit";
//	public static final String PASSWORD_B2R_AUDIT = "audit555";
//	
//	// B2R - Borrower - Applicant
//	public static final String USERNAME_B2R_BORROWER = "dbb2r";
//	public static final String PASSWORD_B2R_BORROWER = "dbb2r555";
//	
//	// CAF - Lender
//	public static final String USERNAME_CAF_LENDER = "cdl1";
//	public static final String PASSWORD_CAF_LENDER = "cdl555";
//	
//	// FNF - Borrower - Applicant
//	public static final String USERNAME_FNF_BORROWER = "caffnf0529.Account_21042016";
//	public static final String PASSWORD_FNF_BORROWER = "VFRLDP$413";
//	
//	public static final String ACCOUNT_NUMBER = "21042016";
	
//	********************PREPROD*******************//
//	 All - Admin - CAF, B2R, FKL
	public static final String USERNAME_ADMIN = "samark";
	public static final String PASSWORD_ADMIN = "Boss555";
	
	// B2R -Lender
	public static final String USERNAME_B2R_LENDER = "dlb2r";
	public static final String PASSWORD_B2R_LENDER = "dlb2r555";
	
	// B2R -Audit
	public static final String USERNAME_B2R_AUDIT = "dlb2r_audit";
	public static final String PASSWORD_B2R_AUDIT = "audit555";
	
	// B2R - Borrower - Applicant
	public static final String USERNAME_B2R_BORROWER = "dbb2r";
	public static final String PASSWORD_B2R_BORROWER = "dbb2r741";
	
	// CAF - Lender
	public static final String USERNAME_CAF_LENDER = "cdl1";
	public static final String PASSWORD_CAF_LENDER = "cdl555";
	
	// FNF - Borrower - Applicant
	public static final String USERNAME_FNF_BORROWER = "CAF.LOC.CAF FNFA041";
	public static final String PASSWORD_FNF_BORROWER = "CejDfa!056";
	
	public static final String ACCOUNT_NUMBER = "5140923423";
	
////////	******************** LIVE*******************//
//////All - Admin - CAF, B2R, FKL
////public static final String USERNAME_ADMIN = "samark";
////public static final String PASSWORD_ADMIN = "Boss741";
////
////// B2R -Lender
////public static final String USERNAME_B2R_LENDER = "dlb2r";
////public static final String PASSWORD_B2R_LENDER = "dlb2r741";
////
////// B2R -Audit
////public static final String USERNAME_B2R_AUDIT = "dlb2r_audit";
////public static final String PASSWORD_B2R_AUDIT = "audit555";
////
////// B2R - Borrower - Applicant
////public static final String USERNAME_B2R_BORROWER = "dbb2r";
////public static final String PASSWORD_B2R_BORROWER = "dbb2r741";
////
////// CAF - Lender
////public static final String USERNAME_CAF_LENDER = "cdl1";
////public static final String PASSWORD_CAF_LENDER = "cdl741";
////
////// FNF - Borrower - Applicant
////public static final String USERNAME_FNF_BORROWER = "CAF.LOC.CAF FNFA041";
////public static final String PASSWORD_FNF_BORROWER = "DkrHYZ!068";
//	
//	public static final String ACCOUNT_NUMBER = "5140923423";
	
//	//*******************************************************//
	public static final String AUTOMATION_CONFIG_XML = "src/resource/automation.config.xml";
	public static final String DATA_XML = "src/resource/data.xml";
	public static final String INTERFACES_XML = "src/resource/interfaces.xml";
	public static final String JQUERY_LOAD = "src/resource/jQueryLoad.js";
	public static final String FOLDER_DOWNLOAD_ONE_WIN = "%s\\Downloads\\";
	public static final String PRESICERES_DATA_PATH = "src/PreciseResData/";
	public static final String CURRENT_YEAR = "2016";
	public static WebDriver driver = null;
}
