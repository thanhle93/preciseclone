package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_004_STO_AddAndDeleteExternalEntities extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		accountNumber = getUniqueNumber();
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + accountNumber;
		firstName = "Testing ";
		email = "qatesting" + accountNumber + "@gmail.com";
		address = "Street " + accountNumber ;
		city = "Los Angeles";
		state = "CA - California";
		zipCode = "90001";
		lesseeName = "New Lessee " + accountNumber;
		guarantorName = "New Guarantor " + accountNumber;
		borrowerName = "New Borrower " + accountNumber;
		sponsorName = "New Sponsor " + accountNumber;
		pledgorName = "New Pledgor" + accountNumber;
		franchisorName = "New Franchisor " + accountNumber;
		diligenceProviderName = "New Diligence Provider " + accountNumber;
		sellerName = "New Seller" + accountNumber;
		surveyProviderName = "New Survey Provider " + accountNumber;
		phaseIProviderName = "New Phase I Provider" + accountNumber;
		pcaProviderName = "New PCA Provider " + accountNumber;
		titleProviderName = "New Title Provider " + accountNumber;

	}

	@Test(groups = { "regression" }, description = "New Entities 01 - Add new Lessee in External Entities")
	public void NewEntities_01_AddNewLessee() {
		
		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("Precondition - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("NewEntities_01 - Step 01: Click 'New External Entities' button");
		transactionPage.clickNewExternalEntitiesButton();
		
		log.info("NewEntities_01 - Step 02: Select 'Lessee' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Lessee");
		
		log.info("NewEntities_01 - Step 03: Select 'Individual' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Individual");
		
		log.info("NewEntities_01 - Step 04. Input Lessee information");
		transactionPage.inputExternalEntitiesInfoToCreate(accountNumber, firstName, lesseeName, lesseeName, email, address, city, state, zipCode);
		
		log.info("NewEntities_01 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: New Lessee is saved");
		verifyTrue(transactionPage.isNewExternalEntitiesSaved(lesseeName));
		
		log.info("NewEntities_01 - Step 06. Input Lessee Provider information");
		transactionPage.inputExternalEntitiesInfoToCreateNewLogin(firstName, lesseeName, email);
		
		log.info("NewEntities_01 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewLoginButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("NewEntities_01 - Step 08. Click Back button on New External Entites");
		transactionPage.clickBackExternalEntitiesButton();	
		
		log.info("VP: The Lessee display on External Entities table");
		verifyTrue(transactionPage.isNewEntitiesDisplayOnExternalEntitiesTable(lesseeName, "Lessee"));
	}
	
	@Test(groups = { "regression" }, description = "New Entities 02 - Add new Guarantor in External Entities")
	public void NewEntities_02_AddNewGuarantor() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("NewEntities_02 - Step 01: Click 'New External Entities' button");
		transactionPage.clickNewExternalEntitiesButton();
		
		log.info("NewEntities_02 - Step 02: Select 'Guarantor' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Guarantor");
		
		log.info("NewEntities_02 - Step 03: Select 'Corporation' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Corporation");
		
		log.info("NewEntities_02 - Step 04. Input Guarantor information");
		transactionPage.inputExternalEntitiesInfoToCreate(accountNumber, firstName, guarantorName, guarantorName, email, address, city, state, zipCode);
		
		log.info("NewEntities_02 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: New Guarantor is saved");
		verifyTrue(transactionPage.isNewExternalEntitiesSaved(guarantorName));
		
		log.info("NewEntities_02 - Step 06. Input Guarantor Provider information");
		transactionPage.inputExternalEntitiesInfoToCreateNewLogin(firstName, guarantorName, email);
		
		log.info("NewEntities_02 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewLoginButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("NewEntities_02 - Step 06. Click Back button on New External Entites");
		transactionPage.clickBackExternalEntitiesButton();	
		
		log.info("VP: The Guarantor display on External Entities table");
		verifyTrue(transactionPage.isNewEntitiesDisplayOnExternalEntitiesTable(guarantorName, "Guarantor"));
	}
	
	@Test(groups = { "regression" }, description = "New Entities 03 - Add new Borrower in External Entities")
	public void NewEntities_03_AddNewBorrower() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("NewEntities_03 - Step 01: Click 'New External Entities' button");
		transactionPage.clickNewExternalEntitiesButton();
		
		log.info("NewEntities_03 - Step 02: Select 'Borrower' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Borrower");
		
		log.info("NewEntities_03 - Step 03: Select 'Limited Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Partnership");
		
		log.info("NewEntities_03 - Step 04. Input Borrower information");
		transactionPage.inputExternalEntitiesInfoToCreate(accountNumber, firstName, borrowerName, borrowerName, email, address, city, state, zipCode);
		
		log.info("NewEntities_03 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: New Borrower is saved");
		verifyTrue(transactionPage.isNewExternalEntitiesSaved(borrowerName));
		
		log.info("NewEntities_03 - Step 06. Input Borrower information");
		transactionPage.inputExternalEntitiesInfoToCreateNewLogin(firstName, borrowerName, email);
		
		log.info("NewEntities_03 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewLoginButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("NewEntities_03 - Step 06. Click Back button on New External Entites");
		transactionPage.clickBackExternalEntitiesButton();	
		
		log.info("VP: The Borrower display on External Entities table");
		verifyTrue(transactionPage.isNewEntitiesDisplayOnExternalEntitiesTable(borrowerName, "Borrower"));
	}
	
	@Test(groups = { "regression" }, description = "New Entities 04 - Add new Sponsor in External Entities")
	public void NewEntities_04_AddNewSponsor() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("NewEntities_04 - Step 01: Click 'New External Entities' button");
		transactionPage.clickNewExternalEntitiesButton();
		
		log.info("NewEntities_04 - Step 02: Select 'Sponsor' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Sponsor");
		
		log.info("NewEntities_04 - Step 03: Select 'General Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("General Partnership");
		
		log.info("NewEntities_04 - Step 04. Input Sponsor information");
		transactionPage.inputExternalEntitiesInfoToCreate(accountNumber, firstName, sponsorName, sponsorName, email, address, city, state, zipCode);
		
		log.info("NewEntities_04 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: New Sponsor is saved");
		verifyTrue(transactionPage.isNewExternalEntitiesSaved(sponsorName));
		
		log.info("NewEntities_04 - Step 06. Input Sponsor information");
		transactionPage.inputExternalEntitiesInfoToCreateNewLogin(firstName, sponsorName, email);
		
		log.info("NewEntities_04 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewLoginButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("NewEntities_04 - Step 06. Click Back button on New External Entites");
		transactionPage.clickBackExternalEntitiesButton();	
		
		log.info("VP: The Sponsor display on External Entities table");
		verifyTrue(transactionPage.isNewEntitiesDisplayOnExternalEntitiesTable(sponsorName, "Sponsor"));
	}
	
	@Test(groups = { "regression" }, description = "New Entities 05 - Add new Pledgor in External Entities")
	public void NewEntities_05_AddNewPledgor() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("NewEntities_05 - Step 01: Click 'New External Entities' button");
		transactionPage.clickNewExternalEntitiesButton();
		
		log.info("NewEntities_05 - Step 02: Select 'Pledgor' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Pledgor");
		
		log.info("NewEntities_05 - Step 03: Select 'Limited Liability Company' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Liability Company");
		
		log.info("NewEntities_05 - Step 04. Input Pledgor information");
		transactionPage.inputExternalEntitiesInfoToCreate(accountNumber, firstName, pledgorName, pledgorName, email, address, city, state, zipCode);
		
		log.info("NewEntities_05 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: New Pledgor is saved");
		verifyTrue(transactionPage.isNewExternalEntitiesSaved(pledgorName));
		
		log.info("NewEntities_05 - Step 06. Input Pledgor information");
		transactionPage.inputExternalEntitiesInfoToCreateNewLogin(firstName, pledgorName, email);
		
		log.info("NewEntities_05 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewLoginButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("NewEntities_05 - Step 06. Click Back button on New External Entites");
		transactionPage.clickBackExternalEntitiesButton();	
		
		log.info("VP: The Pledgor display on External Entities table");
		verifyTrue(transactionPage.isNewEntitiesDisplayOnExternalEntitiesTable(pledgorName, "Pledgor"));
	}
	
	@Test(groups = { "regression" }, description = "New Entities 06 - Add new Diligence Provider in External Entities")
	public void NewEntities_06_AddNewDiligenceProvider() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("NewEntities_06 - Step 01: Click 'New External Entities' button");
		transactionPage.clickNewExternalEntitiesButton();
		
		log.info("NewEntities_06 - Step 02: Select 'Diligence Provider' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Diligence Provider");
		
		log.info("NewEntities_06 - Step 03: Select 'Individual' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Individual");
		
		log.info("NewEntities_06 - Step 04. Input Diligence Provider information");
		transactionPage.inputExternalEntitiesInfoToCreate(accountNumber, firstName, diligenceProviderName, diligenceProviderName, email, address, city, state, zipCode);
		
		log.info("NewEntities_06 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: New Diligence Provider is saved");
		verifyTrue(transactionPage.isNewExternalEntitiesSaved(diligenceProviderName));
		
		log.info("NewEntities_06 - Step 06. Input Diligence Provider information");
		transactionPage.inputExternalEntitiesInfoToCreateNewLogin(firstName, diligenceProviderName, email);
		
		log.info("NewEntities_06 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewLoginButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("NewEntities_06 - Step 06. Click Back button on New External Entites");
		transactionPage.clickBackExternalEntitiesButton();	
		
		log.info("VP: The Diligence Provider display on External Entities table");
		verifyTrue(transactionPage.isNewEntitiesDisplayOnExternalEntitiesTable(diligenceProviderName, "Diligence Provider"));
	}
	
	@Test(groups = { "regression" }, description = "New Entities 07 - Add new Franchisor in External Entities")
	public void NewEntities_07_AddNewFranchisor() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("NewEntities_07 - Step 01: Click 'New External Entities' button");
		transactionPage.clickNewExternalEntitiesButton();
		
		log.info("NewEntities_07 - Step 02: Select 'Franchisor' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Franchisor");
		
		log.info("NewEntities_07 - Step 03: Select 'Corporation' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Corporation");
		
		log.info("NewEntities_07 - Step 04. Input Franchisor information");
		transactionPage.inputExternalEntitiesInfoToCreate(accountNumber, firstName, franchisorName, franchisorName, email, address, city, state, zipCode);
		
		log.info("NewEntities_07 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: New Franchisor is saved");
		verifyTrue(transactionPage.isNewExternalEntitiesSaved(franchisorName));
		
		log.info("NewEntities_07 - Step 06. Input Franchisor information");
		transactionPage.inputExternalEntitiesInfoToCreateNewLogin(firstName, franchisorName, email);
		
		log.info("NewEntities_07 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewLoginButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("NewEntities_07 - Step 06. Click Back button on New External Entites");
		transactionPage.clickBackExternalEntitiesButton();	
		
		log.info("VP: The Franchisor display on External Entities table");
		verifyTrue(transactionPage.isNewEntitiesDisplayOnExternalEntitiesTable(franchisorName, "Franchisor"));
	}
	
	@Test(groups = { "regression" }, description = "New Entities 08 - Add new Seller in External Entities")
	public void NewEntities_08_AddNewSeller() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("NewEntities_08 - Step 01: Click 'New External Entities' button");
		transactionPage.clickNewExternalEntitiesButton();
		
		log.info("NewEntities_08 - Step 02: Select 'Seller' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Seller");
		
		log.info("NewEntities_08 - Step 03: Select 'Limited Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Partnership");
		
		log.info("NewEntities_08 - Step 04. Input Seller information");
		transactionPage.inputExternalEntitiesInfoToCreate(accountNumber, firstName, sellerName, sellerName, email, address, city, state, zipCode);
		
		log.info("NewEntities_08 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: New Seller is saved");
		verifyTrue(transactionPage.isNewExternalEntitiesSaved(sellerName));
		
		log.info("NewEntities_08 - Step 06. Input Seller information");
		transactionPage.inputExternalEntitiesInfoToCreateNewLogin(firstName, sellerName, email);
		
		log.info("NewEntities_08 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewLoginButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("NewEntities_08 - Step 06. Click Back button on New External Entites");
		transactionPage.clickBackExternalEntitiesButton();	
		
		log.info("VP: The Seller display on External Entities table");
		verifyTrue(transactionPage.isNewEntitiesDisplayOnExternalEntitiesTable(sellerName, "Seller"));
	}
	
	@Test(groups = { "regression" }, description = "New Entities 09 - Add new Survey Provider in External Entities")
	public void NewEntities_09_AddNewSurveyProvider() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("NewEntities_09 - Step 01: Click 'New External Entities' button");
		transactionPage.clickNewExternalEntitiesButton();
		
		log.info("NewEntities_09 - Step 02: Select 'Survey Provider' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Survey Provider");
		
		log.info("NewEntities_09 - Step 03: Select 'General Partnership' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("General Partnership");
		
		log.info("NewEntities_09 - Step 04. Input Survey Provider information");
		transactionPage.inputExternalEntitiesInfoToCreate(accountNumber, firstName, surveyProviderName, surveyProviderName, email, address, city, state, zipCode);
		
		log.info("NewEntities_09 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: New Survey Provider is saved");
		verifyTrue(transactionPage.isNewExternalEntitiesSaved(surveyProviderName));
		
		log.info("NewEntities_09 - Step 06. Input Survey Provider information");
		transactionPage.inputExternalEntitiesInfoToCreateNewLogin(firstName, surveyProviderName, email);
		
		log.info("NewEntities_09 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewLoginButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("NewEntities_09 - Step 06. Click Back button on New External Entites");
		transactionPage.clickBackExternalEntitiesButton();	
		
		log.info("VP: The Survey Provider display on External Entities table");
		verifyTrue(transactionPage.isNewEntitiesDisplayOnExternalEntitiesTable(surveyProviderName, "Survey Provider"));
	}
	
	@Test(groups = { "regression" }, description = "New Entities 10 - Add new Phase I Provider in External Entities")
	public void NewEntities_10_AddNewPhaseIProvider() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("NewEntities_10 - Step 01: Click 'New External Entities' button");
		transactionPage.clickNewExternalEntitiesButton();
		
		log.info("NewEntities_10 - Step 02: Select 'Phase I Provider' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Phase I Provider");
		
		log.info("NewEntities_10 - Step 03: Select 'Limited Liability Company' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Limited Liability Company");
		
		log.info("NewEntities_10 - Step 04. Input Phase I Provider information");
		transactionPage.inputExternalEntitiesInfoToCreate(accountNumber, firstName, phaseIProviderName, phaseIProviderName, email, address, city, state, zipCode);
		
		log.info("NewEntities_10 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: New Phase I Provider is saved");
		verifyTrue(transactionPage.isNewExternalEntitiesSaved(phaseIProviderName));
		
		log.info("NewEntities_10 - Step 06. Input Phase I Provider information");
		transactionPage.inputExternalEntitiesInfoToCreateNewLogin(firstName, phaseIProviderName, email);
		
		log.info("NewEntities_10 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewLoginButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("NewEntities_10 - Step 06. Click Back button on New External Entites");
		transactionPage.clickBackExternalEntitiesButton();	
		
		log.info("VP: The Phase I Provider display on External Entities table");
		verifyTrue(transactionPage.isNewEntitiesDisplayOnExternalEntitiesTable(phaseIProviderName, "Phase I Provider"));
	}
	
	@Test(groups = { "regression" }, description = "New Entities 11 - Add new PCA Provider in External Entities")
	public void NewEntities_11_AddNewPCAProvider() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("NewEntities_11 - Step 01: Click 'New External Entities' button");
		transactionPage.clickNewExternalEntitiesButton();
		
		log.info("NewEntities_11 - Step 02: Select 'PCA Provider' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("PCA Provider");
		
		log.info("NewEntities_11 - Step 03: Select 'Individual' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Individual");
		
		log.info("NewEntities_11 - Step 04. Input PCA Provider information");
		transactionPage.inputExternalEntitiesInfoToCreate(accountNumber, firstName, pcaProviderName, pcaProviderName, email, address, city, state, zipCode);
		
		log.info("NewEntities_11 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: New PCA Provider is saved");
		verifyTrue(transactionPage.isNewExternalEntitiesSaved(pcaProviderName));
		
		log.info("NewEntities_11 - Step 06. Input PCA Provider information");
		transactionPage.inputExternalEntitiesInfoToCreateNewLogin(firstName, pcaProviderName, email);
		
		log.info("NewEntities_11 - Step 07: Click 'Create New Entity' button");
		transactionPage.clickNewLoginButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("NewEntities_11 - Step 06. Click Back button on New External Entites");
		transactionPage.clickBackExternalEntitiesButton();	
		
		log.info("VP: The PCA Provider display on External Entities table");
		verifyTrue(transactionPage.isNewEntitiesDisplayOnExternalEntitiesTable(pcaProviderName, "PCA Provider"));
	}
	
	@Test(groups = { "regression" }, description = "New Entities 12 - Add new Title Provider in External Entities")
	public void NewEntities_12_AddNewTitleProvider() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("NewEntities_12 - Step 01: Click 'New External Entities' button");
		transactionPage.clickNewExternalEntitiesButton();
		
		log.info("NewEntities_12 - Step 02: Select 'Title Provider' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Title Provider");
		
		log.info("NewEntities_12 - Step 03: Select 'Corporation' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Corporation");
		
		log.info("NewEntities_12 - Step 04. Input Title Provider information");
		transactionPage.inputExternalEntitiesInfoToCreate(accountNumber, firstName, titleProviderName, titleProviderName, email, address, city, state, zipCode);
		
		log.info("NewEntities_12 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: New Title Provider is saved");
		verifyTrue(transactionPage.isNewExternalEntitiesSaved(titleProviderName));
		
		log.info("NewEntities_12 - Step 04. Input Title Provider information");
		transactionPage.inputExternalEntitiesInfoToCreateNewLogin(firstName, titleProviderName, email);
		
		log.info("NewEntities_12 - Step 05: Click 'Create New Entity' button");
		transactionPage.clickNewLoginButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("NewEntities_12 - Step 06. Click Back button on New External Entites");
		transactionPage.clickBackExternalEntitiesButton();	
		
		log.info("VP: The Title Provider display on External Entities table");
		verifyTrue(transactionPage.isNewEntitiesDisplayOnExternalEntitiesTable(titleProviderName, "Title Provider"));
	}
	
	@Test(groups = { "regression" }, description = "New Entities 12 - Delete the entities in External Entities")
	public void NewEntities_13_DeleteTheEntitiesInExternalEntitiesTable() {
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("NewEntities_13 - Step 01: Delete 'Title Provider' in External Entities table");
		transactionPage.deleteEntityInTable(titleProviderName);
		
		log.info("NewEntities_13 - Step 02: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: The Title Provider is not display on External Entities table");
		verifyFalse(transactionPage.isNewEntitiesDisplayOnExternalEntitiesTable(titleProviderName, "Title Provider"));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName;
	private String accountNumber, firstName, lesseeName, guarantorName, borrowerName, sponsorName, pledgorName, franchisorName;
	private String diligenceProviderName, sellerName, surveyProviderName, phaseIProviderName, pcaProviderName, titleProviderName;
	private String address, city, state, zipCode, email;
}