package sanityTest_LoginDelay;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class loginTime_CAF_LoginTimeWithTheRoles extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		usernameMember = "auto-CAF-member2014";
		passwordMember = "change";
		usernameApplicant = Constant.USERNAME_CAF_BORROWER;
		passwordApplicant = Constant.PASSWORD_CAF_BORROWER;
	}
	
	@Test(groups = { "regression" },description = "LoginTime01 - Login Time For Lender/ Admin")
	public void LoginTime_01_LoginTimeForLender()
	{
		log.info("LoginTime_01: Login with Lender");
		loginPage.loginAsLender(usernameLender, passwordLender, false);
		loginPage = logout(driver, ipClient);
	}
	
	@Test(groups = { "regression" },description = "LoginTime02 - Login Time For Member")
	public void LoginTime_02_LoginTimeForMember()
	{
		log.info("LoginTime_02: Login with Member");
		loginPage.loginAsLender(usernameMember, passwordMember, false);
		loginPage = logout(driver, ipClient);
	}
	
	@Test(groups = { "regression" },description = "LoginTime03 - Login Time For Applicant")
	public void LoginTime_03_LoginTimeForApplicant()
	{
		log.info("LoginTime_03: Login with Applicant");
		loginPage.loginAsBorrower(usernameApplicant,passwordApplicant, false);
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private String usernameMember, passwordMember;
	private String usernameLender, passwordLender;
	private String usernameApplicant, passwordApplicant;
}