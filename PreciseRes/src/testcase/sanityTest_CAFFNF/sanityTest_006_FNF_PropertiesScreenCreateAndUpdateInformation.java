package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFLoansPage;
import page.FNFPropertiesPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;

import common.AbstractTest;
import common.Constant;

public class sanityTest_006_FNF_PropertiesScreenCreateAndUpdateInformation extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		loanName = "Edit Loan Auto " + accountNumber;
		companyName = "Edit_Borrower " + accountNumber;
		addressApplicant = "CAF-FNF Applicant Address " + getUniqueNumber();
		changeAddressApplicant = "CAF-FNF Applicant Address Change " + getUniqueNumber();
		addressLender = "CAF-FNF Lender Address " + getUniqueNumber();
		changeAddressLender = "CAF-FNF Lender Address Change " + getUniqueNumber();
		city = "Fresno";
		changeCity = "Orlando";
		state = "CA - California";
		changeState = "FL - Florida";
		zip = "95542";
		changeZip = "24341";
		date = "04/12/2016";
		changeDate = "04/12/2010";
		number = "30";
		changeNumber = "60";
		price = "$40.00";
		changePrice = "$80.00";
		propertyType ="SFR";
		changePropertyType ="Condo";
		transactionType ="Refinance";
		changeTransactionType ="Purchase";
		titleCompany= "testing company global";
		changeTitleCompany= "testing company local";
		escrowAgent = "testing escrow global";
		changeEscrowAgent = "testing escrow local";
		insuranceCompany = "testing insurance global";
		changeInsuranceCompany = "testing insurance local";
		commonText = "Common text !@#";
		changeCommonText = "Changed Common text !@#";
		valuationType = "Interior BPO";
		changeValuationType = "Exterior BPO";
		includeRehabCosts = "Yes";
		changeIncludeRehabCosts = "No";
		productType = "Institutional";
		maxLTC = "10.25 %";
		maxLTV = "15.11 %";
		advanceFee = "20.00 %";
		warehouseLine = "Deutsche Bank";
	}



	@Test(groups = { "regression" }, description = "Applicant 34 - Login as Lender - Property screen create and update information")
	public void Properties_01_Lender_CreateAndUpdateInformation() {

		log.info("Properties_02 - Step 01. Logout and login with Lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
		
		log.info("Properties_02 - Step 02: Go to 'Properties' page");
		loanPage = homePage.openFNFLoansPage(driver, ipClient);
		
		log.info("Properties_02 - Step 03: Go to 'Loan Details' page");
		loanPage.clickOnSearchButton(loanName);
		loanPage.openLoansDetailPage(loanName);
		
		log.info("Properties_01 - Step 03: Select product type and input data for Properties creating'");
		loanPage.selectProductType(productType);
		loanPage.inputDataForCheckingAddPropertyBorrower(maxLTC,  maxLTV,  advanceFee);
		loanPage.clickOnSaveButton();
		
		log.info("Properties_02 - Step 03: Select Warehouse line and save change");
		loanPage.selectItemFromDropdown("Warehouse Line", warehouseLine);
		loanPage.clickOnSaveButton();
	
		log.info("Properties_02 - Step 02: Go to 'Properties' page");
		propertiesPage = loanPage.openPropertiesPageFNF(driver, ipClient);
		
		log.info("VP: Verify that 'Add Properties Button' displayed");
		verifyTrue(propertiesPage.isAddPropertyButtonDisplayed());
		
		log.info("VP: Verify that 'Import Properties Button' displayed");
		verifyTrue(propertiesPage.isImportPropertiesButtonDisplayed());
		
		log.info("VP: Verify that 'Make Property Active' button displayed");
		verifyTrue(propertiesPage.isMenuButtonDisplayed(driver, "Make Property Active"));
		
		log.info("VP: Verify that 'Make Property Inactive' button displayed");
		verifyTrue(propertiesPage.isMenuButtonDisplayed(driver, "Make Property Inactive"));
		
		log.info("Properties_02 - Step 03: Click on 'New' button");
		propertiesPage.clickNewPropertyButton();
		
		log.info("VP: Verify that 'State and Property type' default value is empty");
		verifyTrue(propertiesPage.isDropdownDefaultValueDisplaysCorrectly("","",""));
		
		log.info("VP: Verify that 'BPO/Appraisal Value' textfield isn't displayed");
		verifyFalse(propertiesPage.isTextfieldByNameDisplayed("BPO/Appraisal Value"));

		log.info("VP: Verify that 'BPO/Appraisal Date' textfield isn't displayed");
		verifyFalse(propertiesPage.isTextfieldByNameDisplayed("BPO/Appraisal Date"));
		
		log.info("Properties_02 - Step 04: Input valid  all required information");
		propertiesPage.inputRequiredInformationProperties(companyName, loanName, addressLender, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany);
		propertiesPage.inputOptinalInformationsForLender(valuationType, includeRehabCosts, price, commonText, date);
		
		log.info("Properties_02 - Step 05: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();
		
		log.info("VP: Verify that 'Warehouse Line' textfield is read-only and take data from Loan Details page");
		verifyTrue(propertiesPage.isTextfieldReadOnly("Warehouse Line"));
		verifyTrue(propertiesPage.isReadOnlyTextfieldDisplayCorrectly("Warehouse Line", warehouseLine));

		log.info("VP: Record Saved message displays");
		verifyTrue(propertiesPage.isRecordSavedMessageDisplays());
		
		log.info("VP: Verify that all required information is saved successfully");
		verifyTrue(propertiesPage.isApplicantPropertiesRequiredInformationSaved(loanName, companyName, addressLender, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany));
		verifyTrue(propertiesPage.isOptinalInformationsForLenderSaved(valuationType, includeRehabCosts, price, commonText, date));

		log.info("Properties_02 - Step 06: Click on 'Back To List' button");
		propertiesPage.clickOnBackToListButton();
		
		log.info("Properties_02 - Step 07. Input 'Properties Address' and click Search");
		propertiesPage.searchAddress(addressLender);
		
		log.info("Properties_02 - Step 08. Open 'Property Address' detail");
		propertiesPage.openPropertyDetail(addressLender);
		
		log.info("VP: Verify that 'BPO/Appraisal Value' textfield is displayed");
		verifyTrue(propertiesPage.isTextfieldByNameDisplayed("BPO/Appraisal Value"));

		log.info("VP: Verify that 'BPO/Appraisal Date' textfield is displayed");
		verifyTrue(propertiesPage.isTextfieldByNameDisplayed("BPO/Appraisal Date"));
		
		log.info("VP: Verify that 'Partial Paydown Date' textfield is displayed");
		verifyTrue(propertiesPage.isTextfieldByNameDisplayed("Partial Paydown Date"));

		log.info("VP: Verify that 'Partial Paydown ($)' textfield is displayed");
		verifyTrue(propertiesPage.isTextfieldByNameDisplayed("Partial Paydown ($)"));
		
		log.info("VP: Verify that 'Override Max LTC %' textfield is displayed");
		verifyTrue(propertiesPage.isTextfieldByNameDisplayed("Override Max LTC %"));

		log.info("VP: Verify that 'Override Max LTV %' textfield is displayed");
		verifyTrue(propertiesPage.isTextfieldByNameDisplayed("Override Max LTV %"));
		
		log.info("VP: Verify that 'Warehouse Draw #' textfield is displayed");
		verifyTrue(propertiesPage.isTextfieldByNameDisplayed("Warehouse Draw #"));
		
		log.info("VP: Verify that 'Payoff Received Date' textfield is displayed");
		verifyTrue(propertiesPage.isTextfieldByNameDisplayed("Payoff Received Date"));
		
		log.info("VP: Verify that 'Maturity Default Y/N' textfield is read-only");
		verifyTrue(propertiesPage.isTextfieldReadOnly("Maturity Default Y/N"));
		
		log.info("VP: Verify that 'Hold Time' textfield is read-only");
		verifyTrue(propertiesPage.isTextfieldReadOnly("Hold Time"));
		
		log.info("VP: Verify that 'Turn Time' textfield is read-only");
		verifyTrue(propertiesPage.isTextfieldReadOnly("Turn Time"));
		
		log.info("VP: Verify that 'Current Outstanding Loan Amount' textfield is read-only");
		verifyTrue(propertiesPage.isTextfieldReadOnly("Current Outstanding Loan Amount"));
		
		log.info("VP: Verify that 'Warehouse Line' textfield is read-only and take data from Loan");
		verifyTrue(propertiesPage.isTextfieldReadOnly("Warehouse Line"));
		verifyTrue(propertiesPage.isReadOnlyTextfieldDisplayCorrectly("Warehouse Line", warehouseLine));
		
		log.info("VP: Verify that 'Current Outstanding Loan Amount' textfield is read-only");
		verifyTrue(propertiesPage.isTextfieldReadOnly("Current Outstanding Loan Amount"));
		
		log.info("Properties_02 - Step 09: Input valid  all required information");
		propertiesPage.inputRequiredInformationProperties(companyName, loanName, changeAddressLender, changeCity, changeState, changeZip, changeDate, changeNumber, changePrice, changePropertyType, changeTransactionType, changeTitleCompany, changeEscrowAgent, changeInsuranceCompany);		
		propertiesPage.inputOptinalInformationsForLender(changeValuationType, changeIncludeRehabCosts, changePrice, changeCommonText, changeDate);
		
		log.info("Properties_02 - Step 10: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();

		log.info("VP: Record Saved message displays");
		verifyTrue(propertiesPage.isRecordSavedMessageDisplays());
		
		log.info("VP: Verify that all required information is changed successfully");
		verifyTrue(propertiesPage.isApplicantPropertiesRequiredInformationSaved(loanName, companyName, changeAddressLender, changeCity, changeState, changeZip, changeDate, changeNumber, changePrice, changePropertyType, changeTransactionType, changeTitleCompany, changeEscrowAgent, changeInsuranceCompany));
		verifyTrue(propertiesPage.isOptinalInformationsForLenderSaved(changeValuationType, changeIncludeRehabCosts, changePrice, changeCommonText, changeDate));
	}
	
	
	@Test(groups = { "regression" }, description = "Applicant 33 - Login as Applicant - Property screen create and update information")
	public void Properties_03_Borrower_CreateAndUpdateInformation() {

		log.info("Properties_01 - Step 01: Login with Applicant");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);
		
		log.info("Properties_01 - Step 02: Open 'Loan Name'");
		loanPage = applicantsPage.openLoanNameInOpportunityTable(loanName);
		
		log.info("Properties_01 - Step 04: Go to 'Properties' page");
		propertiesPage = loanPage.openPropertiesTab();
		
		log.info("VP: Verify that 'Add Properties Button' displayed");
		verifyTrue(propertiesPage.isAddPropertyButtonDisplayed());
		
		log.info("VP: Verify that 'Import Properties Button' displayed");
		verifyTrue(propertiesPage.isImportPropertiesButtonDisplayed());
		
		log.info("Properties_01 - Step 05: Click on 'New' button");
		propertiesPage.clickNewPropertyButtonPropertyTab();
		
		log.info("VP: Verify that 'State and Property type' default value is empty");
		verifyTrue(propertiesPage.isDropdownDefaultValueDisplaysCorrectly("","",""));
		
		log.info("VP: Verify that 'BPO/Appraisal Value' textfield isn't displayed");
		verifyFalse(propertiesPage.isTextfieldByNameDisplayed("BPO/Appraisal Value"));

		log.info("VP: Verify that 'BPO/Appraisal Date' textfield isn't displayed");
		verifyFalse(propertiesPage.isTextfieldByNameDisplayed("BPO/Appraisal Date"));
		
		log.info("VP: Verify that 'Max LTC%' textfield is read-only and take data from Loans");
		verifyTrue(propertiesPage.isTextfieldReadOnly("Max LTC %"));
		verifyTrue(propertiesPage.isReadOnlyTextfieldDisplayCorrectly("Max LTC %", maxLTC));

		log.info("VP: Verify that 'Max LTV%' textfield is read-only and take data from Loans");
		verifyTrue(propertiesPage.isTextfieldReadOnly("Max LTV %"));
		verifyTrue(propertiesPage.isReadOnlyTextfieldDisplayCorrectly("Max LTV %", maxLTV));
		
		log.info("VP: Verify that 'Advance Fee%' textfield is read-only and take data from Loans");
		verifyTrue(propertiesPage.isTextfieldReadOnly("Advance Fee %"));
		verifyTrue(propertiesPage.isReadOnlyTextfieldDisplayCorrectly("Advance Fee %", advanceFee));
		
		log.info("Properties_01 - Step 06: Input valid all required information");
		propertiesPage.inputRequiredInformationProperties("", loanName, addressApplicant, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany);
		propertiesPage.inputOptinalInformationsForBorrower(includeRehabCosts, price, commonText);
		
		log.info("Properties_01 - Step 07: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();

		log.info("VP: Record Saved message displays");
		verifyTrue(propertiesPage.isRecordSavedMessageDisplays());
		
		log.info("VP: Verify that all data is saved successfully");
		verifyTrue(propertiesPage.isApplicantPropertiesRequiredInformationSaved(loanName, companyName, addressApplicant, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany));
		verifyTrue(propertiesPage.isOptinalInformationsForBorrowerSaved(includeRehabCosts, price, commonText));

		log.info("Properties_01 - Step 08: Click on 'Back To List' button");
		propertiesPage.clickOnBackToListButton();
		
		log.info("Properties_01 - Step 09. Input 'Properties Address' and click Search");
		propertiesPage.searchAddress(addressApplicant);
		
		log.info("Properties_01 - Step 10. Open 'Property Address' detail");
		propertiesPage.openPropertyDetail(addressApplicant);
		
		log.info("VP: Verify that 'BPO/Appraisal Value' textfield is read-only");
		verifyTrue(propertiesPage.isTextfieldReadOnly("BPO/Appraisal Value"));

		log.info("VP: Verify that 'BPO/Appraisal Date' textfield is read-only");
		verifyTrue(propertiesPage.isTextfieldReadOnly("BPO/Appraisal Date"));
		
		log.info("VP: Verify that 'Approved Completed Rehab Costs' textfield is read-only");
		verifyTrue(propertiesPage.isTextfieldReadOnly("Approved Completed Rehab Costs"));

		log.info("VP: Verify that 'Partial Paydown Date' textfield is read-only");
		verifyTrue(propertiesPage.isTextfieldReadOnly("Partial Paydown Date"));
		
		log.info("VP: Verify that 'Partial Paydown ($)' textfield is read-only");
		verifyTrue(propertiesPage.isTextfieldReadOnly("Partial Paydown ($)"));

		log.info("VP: Verify that 'Maturity Default Y/N' textfield is read-only");
		verifyTrue(propertiesPage.isTextfieldReadOnly("Maturity Default Y/N"));
		
		log.info("Properties_01 - Step 11: Input all required information");
		propertiesPage.inputRequiredInformationProperties("", loanName, changeAddressApplicant, changeCity, changeState, changeZip, changeDate, changeNumber, changePrice, changePropertyType, changeTransactionType, changeTitleCompany, changeEscrowAgent, changeInsuranceCompany);
		propertiesPage.inputOptinalInformationsForBorrower(changeIncludeRehabCosts, changePrice, changeCommonText);
		
		log.info("Properties_01 - Step 12: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();

		log.info("VP: Record Saved message displays");
		verifyTrue(propertiesPage.isRecordSavedMessageDisplays());
		
		log.info("VP: Verify that all required information is changed successfully");
		verifyTrue(propertiesPage.isApplicantPropertiesRequiredInformationSaved(loanName, companyName, changeAddressApplicant, changeCity, changeState, changeZip, changeDate, changeNumber, changePrice, changePropertyType, changeTransactionType, changeTitleCompany, changeEscrowAgent, changeInsuranceCompany));
		verifyTrue(propertiesPage.isOptinalInformationsForBorrowerSaved(changeIncludeRehabCosts, changePrice, changeCommonText));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private FNFPropertiesPage propertiesPage;
	private FNFLoansPage loanPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername, FNFBorrowerPassword;
	private String addressApplicant, addressLender, city, state, zip, companyName, date, number, price;
	private String propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany;
	private String changeAddressApplicant, changeAddressLender, changeCity, changeState, changeZip , changeDate, changeNumber, changePrice;
	private String changePropertyType, changeTransactionType, changeTitleCompany, changeEscrowAgent, changeInsuranceCompany;
	private String accountNumber, loanName, productType;
	private String commonText, changeCommonText, valuationType, includeRehabCosts, changeValuationType, changeIncludeRehabCosts;
	private String maxLTC, maxLTV, advanceFee, warehouseLine;
	}