package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.MailHomePage;
import page.MailLoginPage;
import page.PageFactory;
import page.PropertiesPage;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_040_CAF_CheckContentInExcelFile extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "CheckContentExcel2016B2R";
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower New";
		zipExtension = "CheckingDownloadFile.zip";
		zipDownload = ".zip";
		excelExtension = ".xlsx";
		excelTemplate = "B2RDataTape.xlsx";
		excelMissingSummary = "MissingDocumentsSummary.xlsx";
		propertiesFileName = "B2R Required.xlsx";
		address1 = "37412 OAKHILL ST";
		documentType1 = "P3 - Purchase Contract/HUD-1 Settlement Stmt";
		documentType2 = "P4 - Final Property Condition/Environmental) Report";
		documentType3 = "P5 - Zoning Compliance Letter";
		documentFileName = "datatest.pdf";
		mailPageUrl = "https://accounts.google.com";
		usernameEmail = "minhdam06@gmail.com";
		passwordEmail = "dam123!@#!@#";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}

	@Test(groups = { "regression" }, description = "Download 01 - Export CSV Dashboad list Loan items")
	public void Download_01_ExportCSVDashboard()  {
		log.info("Download_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

//		log.info("Precondition 01. Open Loans tab");
//		loansPage = homePage.openLoansPage(driver, ipClient);
//
//		log.info("Download_11 - Step 04. Open Loan detail");
//		loansPage.searchLoanByName(loanName);
//		loansPage.openLoansDetailPage(loanName);
//
//		log.info("Precondition 03. Open Properties tab");
//		loansPage.openPropertiesTab();
//
//		log.info("Download_02 - Step 01. Click 'Export datatape' button");
//		loansPage.deleteAllFileInFolder();
//		loansPage.clickExportDatatapeButton();
//
//		log.info("Download_02 - Step 02. Click 'Export all Properties' radio button");
//		loansPage.clickExportAllPropertiesRadioButton("1");
//
//		log.info("Download_02 - Step 03. Wait for file downloaded complete");
//		loansPage.waitForDownloadFileContainsNameCompleted(excelExtension);

		log.info("Download_02 - Step 03. Open the Excel file");
		loansPage.setExcelFile("Data Tape");

		log.info("Download_02 - Step 03. Get value");
		String address1 = loansPage.getCellData(11, 3);
		String address2 = loansPage.getCellData(12, 4);
		String address3 = loansPage.getCellData(12, 5);

		System.out.println(address1);
		System.out.println(address2);
		System.out.println(address3);

	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private MailLoginPage mailLoginPage;
	private HomePage homePage;
	private MailHomePage mailHomePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private String excelExtension, zipExtension, zipDownload, excelTemplate, excelMissingSummary;
	private String usernameLender, passwordLender, address1, documentFileName;
	private String loanName, accountNumber, applicantName, propertiesFileName, loanStatus;
	private int countFile;
	private String documentType1, documentType2, documentType3, mailPageUrl;
	private String usernameEmail, passwordEmail, uploaderPageUrl, preciseRESUrl;
}