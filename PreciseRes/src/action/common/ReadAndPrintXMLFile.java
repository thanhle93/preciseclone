
package common;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

public class ReadAndPrintXMLFile{

	public static void main(String argv[]) {

	    try {

		File fXmlFile = new File("D:/book2.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
				
		//optional, but recommended
		//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
		doc.getDocumentElement().normalize();

		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
				
		NodeList nList = doc.getElementsByTagName("Variable");
				
		System.out.println("----------------------------");

		for (int temp = 0; temp < nList.getLength(); temp++) {

			Node nNode = nList.item(temp);
					
//			System.out.println("\nCurrent Element :" + nNode.getNodeName());
					
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				String a[];
				Element eElement = (Element) nNode;
				if(eElement.getAttribute("Name").contains("Guarantor Name") && eElement.getAttribute("RepeatContext").contains("]"))
				{
					
//				System.out.println("Staff id : " + eElement.getAttribute("id"));
				System.out.println("Guarantor Name : " + eElement.getElementsByTagName("Value").item(0).getTextContent());
//				System.out.println("Last Name : " + eElement.getElementsByTagName("lastname").item(0).getTextContent());
//				System.out.println("Nick Name : " + eElement.getElementsByTagName("nickname").item(0).getTextContent());
//				System.out.println("Salary : " + eElement.getElementsByTagName("salary").item(0).getTextContent());
				}
				if(eElement.getAttribute("Name").contains("Number of Guarantors"))
				{
				System.out.println("Number of Guarantors : " + eElement.getElementsByTagName("Value").item(0).getTextContent());
				}
				if(eElement.getAttribute("Name").contains("Multiple Guarantors"))
				{
				System.out.println("Multiple Guarantors : " + eElement.getElementsByTagName("Value").item(0).getTextContent());
				}
			}
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	  }

}