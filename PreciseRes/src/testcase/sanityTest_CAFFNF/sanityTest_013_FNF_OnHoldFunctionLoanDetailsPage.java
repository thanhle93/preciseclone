package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFLoansPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_013_FNF_OnHoldFunctionLoanDetailsPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		loanName = "Edit Loan Auto " + getUniqueNumber();
		underwriter = "CAF FNF Lender";
		locClosingDate = "04/12/2014";
		validDate = "02/12/2030";
		validNumber ="10";
		validText = ""+ getUniqueNumber();
		acquisitionType = "Acquisition";
		interestRateType = "Fixed";
		renovationFunding = "Yes";
		underwritingException = "No";
		guarantorsName ="Guarantor"+ getUniqueNumber();
		lender = "CAF Bridge Lending";
	}

	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Make sure On hold function works correctly with 'Invited' status")
	public void OnHoldFunction_01_Lender_CheckOnHoldFunctionWithInvitedStatus() {
		log.info("OnHoldFunction_01 - Step 01. Login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);

		log.info("OnHoldFunction_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("OnHoldFunction_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("OnHoldFunction_01 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("OnHoldFunction_01 - Step 05: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("OnHoldFunction_01 - Step 06: Input loan name");
		loanPage.inputRequiredData(loanName, locClosingDate);
		
		log.info("OnHoldFunction_01 - Step 07: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("VP: Verify that 'Invited' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Invited"));
		
		log.info("OnHoldFunction_01 - Step 08: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("VP: Verify that 'On Hold' button is displayed");
		verifyTrue(loanPage.isOnHoldButtonDisplayed());
		
		log.info("OnHoldFunction_01 - Step 09: Click On hold button");
		loanPage.clickOnOnHoldButton();
		
		log.info("VP: Verify that 'Invited' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("On Hold"));
		
		log.info("OnHoldFunction_01 - Step 10: Click On Re-open button");
		loanPage.clickOnReopenButton();
		
		log.info("VP: Verify that 'Invited' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Invited"));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Make sure On hold function works correctly with 'Initial Review' status")
	public void OnHoldFunction_02_Lender_CheckOnHoldFunctionWithInitialPreviewStatus() {
		
		log.info("OnHoldFunction_02 - Step 01: Click on 'Send to Initial Review' button");
		loanPage.clickOnSendToInitialReviewButton();
		
		log.info("VP: Verify that 'Initial Review' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Initial Review"));
		
		log.info("VP: Verify that 'On Hold' button is displayed");
		verifyTrue(loanPage.isOnHoldButtonDisplayed());
		
		log.info("OnHoldFunction_01 - Step 02: Click On hold button");
		loanPage.clickOnOnHoldButton();
		
		log.info("VP: Verify that 'On Hold' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("On Hold"));
		
		log.info("OnHoldFunction_01 - Step 03: Click On Re-open button");
		loanPage.clickOnReopenButton();
		
		log.info("VP: Verify that 'Invited' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Initial Review"));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Make sure On hold function works correctly with 'Term-Sheet Sent' status")
	public void OnHoldFunction_03_Lender_CheckOnHoldFunctionWithTermSheetSentStatus() {
		
		log.info("OnHoldFunction_03 - Step 01: Click on 'Send to Term-Sheet Sent' button");
		loanPage.clickOnSendToTermSheetSentButton();
		
		log.info("VP: Verify that 'Term-Sheet Sent' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Term-Sheet Sent"));
		
		log.info("VP: Verify that 'On Hold' button is displayed");
		verifyTrue(loanPage.isOnHoldButtonDisplayed());
		
		log.info("OnHoldFunction_03 - Step 02: Click On hold button");
		loanPage.clickOnOnHoldButton();
		
		log.info("VP: Verify that 'On Hold' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("On Hold"));
		
		log.info("OnHoldFunction_03 - Step 03: Click On Re-open button");
		loanPage.clickOnReopenButton();
		
		log.info("VP: Verify that 'Term-Sheet Sent' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Term-Sheet Sent"));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Make sure On hold function works correctly with 'Underwriting' status")
	public void OnHoldFunction_04_Lender_CheckOnHoldFunctionWithUnderwritingStatus() {
		
		log.info("OnHoldFunction_04 - Step 01: Click on 'Send to Term-Sheet Sent' button");
		loanPage.clickOnSendToUnderwritingButton();
		
		log.info("VP: Verify that 'Underwriting' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Underwriting"));
		
		log.info("VP: Verify that 'On Hold' button is displayed");
		verifyTrue(loanPage.isOnHoldButtonDisplayed());
		
		log.info("OnHoldFunction_04 - Step 02: Click On hold button");
		loanPage.clickOnOnHoldButton();
		
		log.info("VP: Verify that 'On Hold' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("On Hold"));
		
		log.info("OnHoldFunction_04 - Step 03: Click On Re-open button");
		loanPage.clickOnReopenButton();
		
		log.info("VP: Verify that 'Underwriting' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Underwriting"));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Make sure On hold function works correctly with 'Committee Review' status")
	public void OnHoldFunction_05_Lender_CheckOnHoldFunctionWithCommitteeReviewStatus() {
		
		log.info("OnHoldFunction_05 - Step 01: Click on 'Send to Term-Sheet Sent' button");
		loanPage.createNewGuarantor(guarantorsName);
		loanPage.inputDataRequiredForCommitteeReview(validDate, validText, validNumber, acquisitionType, interestRateType, renovationFunding, underwritingException, underwriter, lender);
		loanPage.clickOnSendToCommitteeReviewButton();
		
		log.info("VP: Verify that 'Committee Review' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Committee Review"));
		
		log.info("VP: Verify that 'On Hold' button is displayed");
		verifyTrue(loanPage.isOnHoldButtonDisplayed());
		
		log.info("OnHoldFunction_05 - Step 02: Click On hold button");
		loanPage.clickOnOnHoldButton();
		
		log.info("VP: Verify that 'On Hold' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("On Hold"));
		
		log.info("OnHoldFunction_05 - Step 03: Click On Re-open button");
		loanPage.clickOnReopenButton();
		
		log.info("VP: Verify that 'Invited' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Committee Review"));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Make sure On hold function works correctly with 'Documentation' status")
	public void OnHoldFunction_06_Lender_CheckOnHoldFunctionWithDocumentationStatus() {
		
		log.info("OnHoldFunction_06 - Step 01: Click on 'Send to Documentation' button");
		loanPage.inputDataRequiredForDocumentation(validDate, validText, validNumber);
		loanPage.clickOnSendToDocumentationButton();
		
		log.info("VP: Verify that 'Documentation' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Documentation"));
		
		log.info("VP: Verify that 'On Hold' button is displayed");
		verifyTrue(loanPage.isOnHoldButtonDisplayed());
		
		log.info("OnHoldFunction_06 - Step 02: Click On hold button");
		loanPage.clickOnOnHoldButton();
		
		log.info("VP: Verify that 'On Hold' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("On Hold"));
		
		log.info("OnHoldFunction_06 - Step 03: Click On Re-open button");
		loanPage.clickOnReopenButton();
		
		log.info("VP: Verify that 'Documentation' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Documentation"));
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Make sure On hold function works correctly with 'Documentation' status")
	public void OnHoldFunction_07_Lender_CheckOnHoldFunctionWithInvestmentPeriod() {
		
		log.info("OnHoldFunction_07 - Step 01: Click on 'Send to Investment Period' button");
		loanPage.inputDataRequiredForInvestmentPeriod(validDate, validText, validNumber);
		loanPage.clickOnSendToInvestmentPeriodButton();
		
		log.info("VP: Verify that 'Investment Period' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Investment Period"));
		
		log.info("VP: Verify that 'On Hold' button is displayed");
		verifyTrue(loanPage.isOnHoldButtonDisplayed());
		
		log.info("OnHoldFunction_07 - Step 02: Click On hold button");
		loanPage.clickOnOnHoldButton();
		
		log.info("VP: Verify that 'On Hold' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("On Hold"));
		
		log.info("OnHoldFunction_07 - Step 03: Click On Re-open button");
		loanPage.clickOnReopenButton();
		
		log.info("VP: Verify that 'Investment Period' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Investment Period"));
		
		log.info("OnHoldFunction_07 - Step 03: Click On Back to Documentation button");
		loanPage.clickOnBackToDocumentationButton();
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Make sure On hold function works correctly with 'Matured' status")
	public void OnHoldFunction_08_Lender_CheckOnHoldFunctionWithMaturedStatus() {
		
		log.info("OnHoldFunction_08 - Step 01: Click on 'Send to Investment Period' button");
//		loanPage.inputExpirationDate(locClosingDate);

		loanPage.inputRequiredData(loanName, locClosingDate);
		loanPage.clickOnSendToInvestmentPeriodButton();
		
		log.info("VP: Verify that 'Matured' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Matured"));
		
		log.info("VP: Verify that 'On Hold' button is not displayed");
		verifyFalse(loanPage.isOnHoldButtonDisplayed());
		
		log.info("OnHoldFunction_08 - Step 02: Click On Re-open button");
		loanPage.clickOnReopenButton();
	}
	
	@Test(groups = { "regression" }, description = "Applicant  - Login as Lender - Make sure On hold function works correctly with 'Matured' status")
	public void OnHoldFunction_09_Lender_CheckOnHoldFunctionWithExpiredStatus() {
		
		log.info("OnHoldFunction_09 - Step 01: Click on 'Send to Investment Period' button");
		loanPage.inputRequiredData(loanName, validDate);
		loanPage.inputExpirationDate(locClosingDate);
		loanPage.clickOnSendToInvestmentPeriodButton();
		
		log.info("VP: Verify that 'Expired' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull("Expired"));
		
		log.info("VP: Verify that 'On Hold' button is not displayed");
		verifyFalse(loanPage.isOnHoldButtonDisplayed());
	}


	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private FNFLoansPage loanPage;
	private String FNFLenderUsername, FNFLenderPassword;
	private String accountNumber, loanName, underwriter, locClosingDate;
	private String companyName;
	private String validDate, validNumber, validText, acquisitionType, interestRateType, renovationFunding, underwritingException;
	private String guarantorsName, lender;
	
	}