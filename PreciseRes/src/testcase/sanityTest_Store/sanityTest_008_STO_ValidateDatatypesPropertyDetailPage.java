package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;

import common.AbstractTest;
import common.Constant;

public class sanityTest_008_STO_ValidateDatatypesPropertyDetailPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + getUniqueNumber();
		propertiesFileName = "StoreDataTape.xlsx";
		address = "1020 Bonforte Blvd";
		invalidCurrency = "$40,0.00*%#$fail";
		invalidDate = "4/21/2015*%#$fail";
		invalidNumber = "20%*%#$fail";
	}

	@Test(groups = { "regression" }, description = "Validate Datatypes 01 - No input the data")
	public void ValidataDatatypes_01_NoInputTheData() {

		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("Precondition - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("ValidataDatatypes_01 - Step 01: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("ValidataDatatypes_01 - Step 02: Click 'New' Property button");
		transactionPage.clickNewTransactionButton();

		log.info("ValidataDatatypes_01 - Step 03: Check to 'Subleased' checkbox");
		transactionPage.selectSubleased();

		log.info("ValidataDatatypes_01 - Step 04: Check to 'Lien Subordinated' checkbox");
		transactionPage.selectLienSubordinated();

		log.info("ValidataDatatypes_01 - Step 05: Check to 'Zoning Restriction' checkbox");
		transactionPage.selectZoningRestriction();

		log.info("ValidataDatatypes_01 - Step 06: Check to 'Phase I' checkbox");
		transactionPage.selectPhaseI();

		log.info("ValidataDatatypes_01 - Step 07: Check to 'Phase II' checkbox");
		transactionPage.selectPhaseII();

		log.info("ValidataDatatypes_01 - Step 08: Check to 'Environmental Insurance' checkbox");
		transactionPage.selectEnvironmentalInsurance();

		log.info("ValidataDatatypes_01 - Step 09: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Address is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_AddressLine1", "Address is required"));

		log.info("VP: 'Subtenant is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_ObligationsSubtenant", "Subtenant is required"));

		log.info("VP: 'Subordinated To is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_ObligationsSubordinatedTo", "Subordinated To is required"));

		log.info("VP: 'Subordination End Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_ObligationsSubordinationEndDate", "Subordination End Date is required"));

		log.info("VP: 'Zoning Restriction Description is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_ZoningRestrictionDescription", "Zoning Restriction Description is required"));

		log.info("VP: 'Phase I Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_EnvironmentalPhase1Date", "Phase I Date is required"));

		log.info("VP: 'Phase II Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_EnvironmentalPhase2Date", "Phase II Date is required"));

		log.info("VP: 'Phase II Result is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_EnvironmentalPhase2Result", "Phase II Result is required"));

		log.info("VP: 'Environmental Insurance Policy Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_EnvironmentalInsurancePolicyDate", "Environmental Insurance Policy Date is required"));

		log.info("VP: 'Environmental Insurance Expiration Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_EnvironmentalInsuranceExpirationDate", "Environmental Insurance Expiration Date is required"));
		
		log.info("VP: 'Environmental Insurer is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_EnvironmentalInsuer", "Environmental Insurer is required"));
	}

	@Test(groups = { "regression" }, description = "Validate Datatypes 02 - Input the invalid/ incorrect at Currency fields")
	public void ValidataDatatypes_02_InputInvalidOrIncorrectAtCurrencyFields() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("Precondition - Step 04: Open Property tab");
		transactionPage.openPropertyTab();
		
		log.info("Precondition - Step 05: Click 'Add Properties' button");
		transactionPage.clickAddPropertyButton();
		
		log.info("Precondition - Step 06: Load 'Property Excel Template' file");
		transactionPage.uploadFileProperties(propertiesFileName);
		
		log.info("Precondition - Step 07. Search with Address name is '1020 Bonforte Blvd'");
		transactionPage.searchPropertyAddress(address);
		
		log.info("Precondition - Step 08. Open a property detail");
		transactionPage.openPropertyDetail(address);
		
		log.info("ValidataDatatypes_02 - Step 01: Input the invalid data to 'Acquisition Price' field");
		transactionPage.inputAcquisitionPrice(invalidCurrency);

		log.info("ValidataDatatypes_02 - Step 02: Input the invalid data to 'Verified Acquisition Price' field");
		transactionPage.inputVerifiedAcquisitionPrice(invalidCurrency);

		log.info("ValidataDatatypes_02 - Step 03: Input the invalid data to 'Transaction Costs' field");
		transactionPage.inputTransactionCosts(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 04: Input the invalid data to 'Appraised Value' field");
		transactionPage.inputAppraisedValue(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 05: Input the invalid data to 'Raw Land Value' field");
		transactionPage.inputRawLandValue(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 06: Input the invalid data to 'Cost Approach Value' field");
		transactionPage.inputCostApproachValue(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 07: Input the invalid data to 'Sales Comparison Value' field");
		transactionPage.inputSalesComparisonValue(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 08: Input the invalid data to 'Income Value' field");
		transactionPage.inputIncomeValue(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 09: Input the invalid data to 'Replacement Cost New' field");
		transactionPage.inputReplacementCostNew(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 10: Input the invalid data to 'Pre-Paid Sales Tax' field");
		transactionPage.inputPrePaidSalesTax(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 11: Input the invalid data to 'Ground Lease Monthly Rent' field");
		transactionPage.inputGroundLeaseMonthlyRent(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 12: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Currency value must be a number' message is displayed at 'Acquisition Price' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_AcqPrice", "Currency value must be a number"));

		log.info("VP: 'Currency value must be a number' message is displayed at 'Verified Acquisition Price' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_VerifiedAcquisitionPrice", "Currency value must be a number"));

		log.info("VP: 'Currency value must be a number' message is displayed at 'Transaction Costs' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_TransactionCosts", "Currency value must be a number"));
	
		log.info("VP: 'Currency value must be a number' message is displayed at 'Appraised Value' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_AppraisedValue", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed at 'Raw Land Value' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_RawLandValue", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed at 'Cost Approach Value' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_AppraisalCostApproachValue", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed at 'Sales Comparison Value' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_AppraisalSalesComparisonValue", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed at 'Income Value' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_AppraisalIncomeValue", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed at 'Replacement Cost New' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_AppraisalReplacementCostNew", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed at 'Pre-Paid Sales Tax' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_PaidRentPrePaidSalesTax", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed at 'Ground Lease Monthly Rent ' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_ObligationsGroundLeaseMonthlyRent", "Currency value must be a number"));
	}

	@Test(groups = { "regression" }, description = "Validate Datatypes 03 - Input the invalid/ incorrect at Date fields")
	public void ValidataDatatypes_03_InputInvalidOrIncorrectAtDateFields() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("Precondition - Step 04: Open Property tab");
		transactionPage.openPropertyTab();
		
		log.info("Precondition - Step 05. Search with Address name is '1020 Bonforte Blvd'");
		transactionPage.searchPropertyAddress(address);
		
		log.info("Precondition - Step 06. Open a property detail");
		transactionPage.openPropertyDetail(address);
		
		log.info("Precondition - Step 07: Check to 'Lien Subordinated' checkbox");
		transactionPage.selectLienSubordinated();
		
		log.info("Precondition - Step 08: Check to 'Phase II' checkbox");
		transactionPage.selectPhaseII();
		
		log.info("Precondition - Step 09: Check to 'Environmental Insurance' checkbox");
		transactionPage.selectEnvironmentalInsurance();
		
		log.info("ValidataDatatypes_03 - Step 01: Input the invalid data to 'Open Date' field");
		transactionPage.inputOpenDate(invalidDate);

		log.info("ValidataDatatypes_03 - Step 02: Input the invalid data to 'Lease End Date' field");
		transactionPage.inputLeaseEndDate(invalidDate);

		log.info("ValidataDatatypes_03 - Step 03: Input the invalid data to 'Appraisal Date' field");
		transactionPage.inputAppraisalDate(invalidDate);

		log.info("ValidataDatatypes_03 - Step 04: Input the invalid data to 'Appraisal Draft Delivery Date' field");
		transactionPage.inputAppraisalDraftDeliveryDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 05: Input the invalid data to 'Appraisal Final Delivery Date' field");
		transactionPage.inputAppraisalFinalDeliveryDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 06: Input the invalid data to 'Subordination End Date' field");
		transactionPage.inputSubordinationEndDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 07: Input the invalid data to 'Original Survey Date' field");
		transactionPage.inputOriginalSurveyDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 08: Input the invalid data to 'Revision Date' field");
		transactionPage.inputRevisionDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 09: Input the invalid data to 'Phase I Date' field");
		transactionPage.inputPhaseIDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 10: Input the invalid data to 'Phase II Date' field");
		transactionPage.inputPhaseIIDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 11: Input the invalid data to 'Environmental Insurance Policy Date' field");
		transactionPage.inputEnvironmentalInsurancePolicyDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 12: Input the invalid data to 'Environmental Insurance Expiration Date' field");
		transactionPage.inputEnvironmentalInsuranceExpirationDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 13: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Value must be a date' message is displayed at 'Open Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_OpeningDate", "Value must be a date"));

		log.info("VP: 'Value must be a date' message is displayed at 'Lease End Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_LeaseExpire", "Value must be a date"));

		log.info("VP: 'Value must be a date' message is displayed at 'Appraisal Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_AppraiselValueDate", "Value must be a date"));

		log.info("VP: 'Value must be a date' message is displayed at 'Appraisal Draft Delivery Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_AppraisalDraftDeliveryDate", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Appraisal Final Delivery Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_AppraisalFinalDeliveryDate", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Subordination End Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_ObligationsSubordinationEndDate", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Original Survey Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_OriginalSurveyDate", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Revision Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_RevisionDate", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Phase I Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_EnvironmentalPhase1Date", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Phase II Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_EnvironmentalPhase2Date", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Environmental Insurance Policy Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_EnvironmentalInsurancePolicyDate", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Environmental Insurance Expiration Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_EnvironmentalInsuranceExpirationDate", "Value must be a date"));
	}

	@Test(groups = { "regression" }, description = "Validate Datatypes 04 - Input the invalid/ incorrect at Number fields")
	public void ValidataDatatypes_04_InputInvalidOrIncorrectAtNumberFields() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("Precondition - Step 04: Open Property tab");
		transactionPage.openPropertyTab();
		
		log.info("Precondition - Step 05. Search with Address name is '1020 Bonforte Blvd'");
		transactionPage.searchPropertyAddress(address);
		
		log.info("Precondition - Step 06. Open a property detail");
		transactionPage.openPropertyDetail(address);

		log.info("ValidataDatatypes_04 - Step 01: Input the invalid data to '# of Units' field");
		transactionPage.inputUnit(invalidNumber);

		log.info("ValidataDatatypes_04 - Step 02: Input the invalid data to 'Zip Code' field");
		transactionPage.inputZipCode(invalidNumber);

		log.info("ValidataDatatypes_04 - Step 03: Input the invalid data to 'Year Built' field");
		transactionPage.inputYearBuilt(invalidNumber);

		log.info("ValidataDatatypes_04 - Step 04: Input the invalid data to 'Year Renovated' field");
		transactionPage.inputYearRenovated(invalidNumber);

		log.info("ValidataDatatypes_04 - Step 05: Input the invalid data to 'Number of Stories' field");
		transactionPage.inputNumberOfStories(invalidNumber);

		log.info("ValidataDatatypes_04 - Step 06: Input the invalid data to 'Land Sq. Ft.' field");
		transactionPage.inputLandSqFt(invalidNumber);
		
		log.info("ValidataDatatypes_04 - Step 07: Input the invalid data to 'Building Sq. Ft.' field");
		transactionPage.inputBuildingSqFt(invalidNumber);
		
		log.info("ValidataDatatypes_04 - Step 08: Input the invalid data to 'Sales Tax State Rate' field");
		transactionPage.inputSalesTaxStateRate(invalidNumber);
		
		log.info("ValidataDatatypes_04 - Step 09: Input the invalid data to 'Sales Tax County Rate' field");
		transactionPage.inputSalesTaxCountyRate(invalidNumber);
		
		log.info("ValidataDatatypes_04 - Step 10: Input the invalid data to 'Sales Tax City Rate' field");
		transactionPage.inputSalesTaxCityRate(invalidNumber);

		log.info("ValidataDatatypes_04 - Step 11: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));

		log.info("VP: 'Value must be a whole number' message is displayed at ' # of Units' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_NumberofUnits", "Value must be a whole number"));

		log.info("VP: 'Enter a valid ZIP or ZIP+4 code' message is displayed at 'Zip Code' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_Zip", "Enter a valid ZIP or ZIP+4 code"));

		log.info("VP: 'Value must be a whole number' message is displayed at 'Year Built' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_YearBuilt", "Value must be a whole number"));

		log.info("VP: 'Value must be a whole number' message is displayed at 'Year Renovated' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_YearRenovated", "Value must be a whole number"));

		log.info("VP: 'Value must be a whole number' message is displayed at 'Number of Stories' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_NumberOfStories", "Value must be a whole number"));
		
		log.info("VP: 'Value left of % symbol must be a number' message is displayed at 'Sales Tax State Rate' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_PaidRentSalesTaxStateRate", "Value left of % symbol must be a number"));
		
		log.info("VP: 'Value left of % symbol must be a number' message is displayed at 'Sales Tax County Rate' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_PaidRentSalesTaxCountyRate", "Value left of % symbol must be a number"));
		
		log.info("VP: 'Value left of % symbol must be a number' message is displayed at 'Sales Tax City Rate' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_PaidRentSalesTaxCityRate", "Value left of % symbol must be a number"));
		
		log.info("VP: 'Value must be a number' message is displayed at 'Land Sq. Ft.' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_LandAcres", "Please enter correct value #############.##"));
		
		log.info("VP: 'Value must be a number' message is displayed at 'Building Sq. Ft.' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_PropertyLoanApplicationDetails_R1_SquareFeet", "Please enter correct value #############.##"));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName, invalidCurrency, invalidDate, invalidNumber;
	private String propertiesFileName, address;
}