package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFLoansPage;
import page.FNFPropertiesPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;

import common.AbstractTest;
import common.Constant;

public class sanityTest_032_FNF_CalculationPropertiesDetails extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		loanName = "Edit Loan Auto " + accountNumber;
		companyName = "Edit_Borrower " + accountNumber;
		addressApplicant = "CAF-FNF Applicant Address " + getUniqueNumber();
		changeAddressApplicant = "CAF-FNF Applicant Address Change " + getUniqueNumber();
		addressLender = "CAF-FNF Lender Address " + getUniqueNumber();
		changeAddressLender = "CAF-FNF Lender Address Change " + getUniqueNumber();
		city = "Fresno";
		changeCity = "Orlando";
		state = "CA - California";
		changeState = "FL - Florida";
		zip = "95542";
		changeZip = "24341";
		date = "04/12/2016";
		changeDate = "04/12/2010";
		number = "30";
		changeNumber = "60";
		price = "$40.00";
		changePrice = "$80.00";
		propertyType ="SFR";
		changePropertyType ="Condo";
		transactionType ="Refinance";
		changeTransactionType ="Purchase";
		titleCompany= "testing company global";
		changeTitleCompany= "testing company local";
		escrowAgent = "testing escrow global";
		changeEscrowAgent = "testing escrow local";
		insuranceCompany = "testing insurance global";
		changeInsuranceCompany = "testing insurance local";
		commonText = "Common text !@#";
		changeCommonText = "Changed Common text !@#";
		valuationType = "Interior BPO";
		changeValuationType = "Exterior BPO";
		includeRehabCosts = "Yes";
		changeIncludeRehabCosts = "No";
		productType = "Institutional";
		maxLTC = "10.25 %";
		maxLTV = "15.11 %";
		advanceFee = "20.00 %";
		warehouseLine = "Deutsche Bank";
		fundingDate = "12/02/2016";
		assetMatureDatePast = "12/2/2014";
		assetMatureDateFuture = "12/2/2050";
	}

	@Test(groups = { "regression" }, description = "Applicant 34 - Login as Lender - Check calculation for Hold time and Turn time")
	public void CalculationPropertyDetails_01_Lender_HoldTimeAndTurnTime() {

		log.info("CalculationPropertyDetails_01 - Step 01. Logout and login with Lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
		
		log.info("CalculationPropertyDetails_01 - Step 02: Go to 'Properties' page");
		propertiesPage = homePage.openPropertiesPageFNF(driver, ipClient);
		
		log.info("CalculationPropertyDetails_01 - Step 03: Click on 'New' button");
		propertiesPage.clickNewPropertyButton();
		
		log.info("CalculationPropertyDetails_01 - Step 04: Input valid  all required information");
		propertiesPage.inputRequiredInformationProperties(companyName, loanName, addressLender, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany);
		propertiesPage.inputOptinalInformationsForLender(valuationType, includeRehabCosts, price, commonText, date);
		
		log.info("CalculationPropertyDetails_01 - Step 05: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();
		
		log.info("CalculationPropertyDetails_01 - Step 06: Input Funding date");
		propertiesPage.inputFundingDate(fundingDate);
		
		log.info("CalculationPropertyDetails_01 - Step 07: Input Payoff date");
		propertiesPage.inputTextfieldByName("Payoff Received Date", "10/25/2017");
		
		log.info("CalculationPropertyDetails_01 - Step 08: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();
		
		log.info("VP: Verify that 'Hold Time' textfield is displayed correctly");
		verifyTrue(propertiesPage.isReadOnlyTextfieldDisplayCorrectly("Hold Time", propertiesPage.calculateHoldTime(fundingDate)));
		
		log.info("VP: Verify that 'Turn Time' textfield is displayed correctly");
		verifyTrue(propertiesPage.isReadOnlyTextfieldDisplayCorrectly("Turn Time", propertiesPage.calculateTurnTime(fundingDate)));
		
		log.info("CalculationPropertyDetails_01 - Step 09: Remove Funding date");
		propertiesPage.inputFundingDate("");
		
		log.info("CalculationPropertyDetails_01 - Step 10: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();
		
		log.info("VP: Verify that 'Hold Time' textfield is empty");
		verifyTrue(propertiesPage.isReadOnlyTextfieldDisplayCorrectly("Hold Time", propertiesPage.calculateHoldTime("")));
		
		log.info("VP: Verify that 'Turn Time' textfield is empty");
		verifyTrue(propertiesPage.isReadOnlyTextfieldDisplayCorrectly("Turn Time", propertiesPage.calculateTurnTime("")));
		
		log.info("CalculationPropertyDetails_01 - Step 11: Input Funding date");
		propertiesPage.inputFundingDate(fundingDate);
		
		log.info("CalculationPropertyDetails_01 - Step 12: Input Payoff date");
		propertiesPage.inputTextfieldByName("Payoff Received Date", "");
		
		log.info("CalculationPropertyDetails_01 - Step 13: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();
		
		log.info("VP: Verify that 'Turn Time' textfield is empty");
		verifyTrue(propertiesPage.isReadOnlyTextfieldDisplayCorrectly("Turn Time", propertiesPage.calculateTurnTime(fundingDate)));
	}
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
	@Test(groups = { "regression" }, description = "Applicant 34 - Login as Lender - Check Calculation Properties Details Page for Asset Maturity Date")
	public void CalculationPropertyDetails_02_Lender_AssetMaturityDate() {
		
		log.info("CalculationPropertyDetails_02 - Step 01: Input Asset Maturity Date with a date in the past");
		propertiesPage.inputTextfieldByName("Asset Maturity Date Override", assetMatureDatePast);
		
		log.info("CalculationPropertyDetails_01 - Step 02: Input Payoff date");
		propertiesPage.inputTextfieldByName("Payoff Received Date", "10/25/2017");
		
		log.info("CalculationPropertyDetails_02 - Step 03: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();
		
		log.info("VP: Verify that 'Maturity Default Y/N' textfield is displayed correctly");
		verifyTrue(propertiesPage.isReadOnlyTextfieldDisplayCorrectly("Maturity Default Y/N", "Yes"));
		
		log.info("CalculationPropertyDetails_02 - Step 04: Input Asset Maturity Date with a date in the future");
		propertiesPage.inputTextfieldByName("Asset Maturity Date Override", assetMatureDateFuture);
		
		log.info("CalculationPropertyDetails_02 - Step 05: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();
		
		log.info("VP: Verify that 'Maturity Default Y/N' textfield is displayed correctly");
		verifyTrue(propertiesPage.isReadOnlyTextfieldDisplayCorrectly("Maturity Default Y/N", "No"));
	}
	
	//coninous after run UI
//	@Test(groups = { "regression" }, description = "Applicant 34 - Login as Lender - Check Calculation Properties Details Page for Approved Advance Amount")
//	public void CalculationPropertyDetails_03_Lender_ApprovedAdvanceAmount() {
//		
//		log.info("CalculationPropertyDetails_02 - Step 01: Input Asset Maturity Date with a date in the past");
//		propertiesPage.inputTextfieldByName("Asset Maturity Date Override", assetMatureDatePast);
//		
//		log.info("CalculationPropertyDetails_01 - Step 02: Input Payoff date");
//		propertiesPage.inputTextfieldByName("Payoff Received Date", "10/25/2017");
//		
//		log.info("CalculationPropertyDetails_02 - Step 03: Click on 'Save' button");
//		propertiesPage.clickOnSaveButton();
//		
//		log.info("VP: Verify that 'Maturity Default Y/N' textfield is displayed correctly");
//		verifyTrue(propertiesPage.isReadOnlyTextfieldDisplayCorrectly("Maturity Default Y/N", "Yes"));
//		
//		log.info("CalculationPropertyDetails_02 - Step 04: Input Asset Maturity Date with a date in the future");
//		propertiesPage.inputTextfieldByName("Asset Maturity Date Override", assetMatureDateFuture);
//		
//		log.info("CalculationPropertyDetails_02 - Step 05: Click on 'Save' button");
//		propertiesPage.clickOnSaveButton();
//		
//		log.info("VP: Verify that 'Maturity Default Y/N' textfield is displayed correctly");
//		verifyTrue(propertiesPage.isReadOnlyTextfieldDisplayCorrectly("Maturity Default Y/N", "No"));
//	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private FNFPropertiesPage propertiesPage;
	private FNFLoansPage loanPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername, FNFBorrowerPassword;
	private String addressApplicant, addressLender, city, state, zip, companyName, date, number, price;
	private String propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany;
	private String changeAddressApplicant, changeAddressLender, changeCity, changeState, changeZip , changeDate, changeNumber, changePrice;
	private String changePropertyType, changeTransactionType, changeTitleCompany, changeEscrowAgent, changeInsuranceCompany;
	private String accountNumber, loanName, productType;
	private String commonText, changeCommonText, valuationType, includeRehabCosts, changeValuationType, changeIncludeRehabCosts;
	private String maxLTC, maxLTV, advanceFee, warehouseLine, fundingDate, assetMatureDatePast, assetMatureDateFuture;
	}