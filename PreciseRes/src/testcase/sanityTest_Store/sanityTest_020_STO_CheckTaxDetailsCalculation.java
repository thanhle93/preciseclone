package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;

import common.AbstractTest;
import common.Constant;

public class sanityTest_020_STO_CheckTaxDetailsCalculation extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + getUniqueNumber();
		propertiesFileName = "StoreProperties.xlsx";
		address1 = "148 Craft Drive";
		address2 = "650 Beverly Street";
		address3 = "1020 Bonforte Blvd";
		appraisedValueProperty1 = "400";
		appraisedValueProperty2 = "500";
		appraisedValueProperty3 = "600";
		appraisedValuePropertyNew = "700";
		initialRent = "90,240.25";
		contractCommencementDate = "12/1/2015";
	}

	@Test(groups = { "regression" }, description = "Check Tax Details Calculation 01 - Check Allocated Rate calculation")
	public void CheckTaxDetailsCalculation_01_CheckAllocatedRate() {

		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("Precondition - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("ValidataDatatypes_01 - Step 07: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("ValidataDatatypes_01 - Step 08: Click 'Add Properties' button");
		transactionPage.clickAddPropertyButton();

		log.info("ValidataDatatypes_01 - Step 09: Upload Excel filename");
		transactionPage.uploadFileProperties(propertiesFileName);
		
		//Property 1

		log.info("ValidataDatatypes_01 - Step 10. Search with Address name is '148 Craft Drive'");
		transactionPage.searchPropertyAddress(address1);
		
		log.info("ValidataDatatypes_01 - Step 11. Open a property detail");
		transactionPage.openPropertyDetail(address1);

		log.info("ValidataDatatypes_01 - Step 12: Input the valid data to 'Appraised Value' field");
		transactionPage.inputAppraisedValue(appraisedValueProperty1);

		log.info("ValidataDatatypes_01 - Step 13: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Appraised Value saved successfully' message is displayed");
		verifyTrue(transactionPage.isAppraisedValueSavedSuccessfully(appraisedValueProperty1));
		
		log.info("ValidataDatatypes_01 - Step 14: Click 'Go to Properties' button");
		transactionPage.clickGotoPropertiesButtonAtPropertyDetails();
		
		//Property 2
		
		log.info("ValidataDatatypes_01 - Step 15. Search with Address name is '650 Beverly Street'");
		transactionPage.searchPropertyAddress(address2);
		
		log.info("ValidataDatatypes_01 - Step 16. Open a property detail");
		transactionPage.openPropertyDetail(address2);

		log.info("ValidataDatatypes_01 - Step 17: Input the valid data to 'Appraised Value' field");
		transactionPage.inputAppraisedValue(appraisedValueProperty2);

		log.info("ValidataDatatypes_01 - Step 18: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Appraised Value saved successfully' message is displayed");
		verifyTrue(transactionPage.isAppraisedValueSavedSuccessfully(appraisedValueProperty2));
		
		log.info("ValidataDatatypes_01 - Step 19: Click 'Go to Properties' button");
		transactionPage.clickGotoPropertiesButtonAtPropertyDetails();
		
		//Property 3
		
		log.info("ValidataDatatypes_01 - Step 20. Search with Address name is '1020 Bonforte Blvd'");
		transactionPage.searchPropertyAddress(address3);
		
		log.info("ValidataDatatypes_01 - Step 21. Open a property detail");
		transactionPage.openPropertyDetail(address3);

		log.info("ValidataDatatypes_01 - Step 22: Input the valid data to 'Appraised Value' field");
		transactionPage.inputAppraisedValue(appraisedValueProperty3);

		log.info("ValidataDatatypes_01 - Step 23: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Appraised Value saved successfully' message is displayed");
		verifyTrue(transactionPage.isAppraisedValueSavedSuccessfully(appraisedValueProperty3));
		
		log.info("ValidataDatatypes_01 - Step 24: Click 'Go to Properties' button");
		transactionPage.clickGotoPropertiesButtonAtPropertyDetails();
		
		//Checking Allocated Rate Property 1 
		
		log.info("ValidataDatatypes_01 - Step 25. Search with Address name is '148 Craft Drive'");
		transactionPage.searchPropertyAddress(address1);
		
		log.info("ValidataDatatypes_01 - Step 26. Open a property detail");
		transactionPage.openPropertyDetail(address1);
		
		log.info("ValidataDatatypes_01 - Step 27. Get actual Alloccated Rate");
		actualAllocatedRate = transactionPage.getValueAllocationRate().replace("%", "").replace(" ", "");
		
		log.info("ValidataDatatypes_01 - Step 28. Calculate Allocated Rate");
		expectedAllocatedRate = transactionPage.getExpectedAllocatedRate(appraisedValueProperty1, appraisedValueProperty1, appraisedValueProperty2, appraisedValueProperty3, 2);
		
		log.info("VP: Allocated Rate displays correctly");
		verifyEquals(actualAllocatedRate, expectedAllocatedRate);
		
		log.info("ValidataDatatypes_02 - Step 29: Change 'Appraised Value' field value");
		transactionPage.inputAppraisedValue(appraisedValuePropertyNew);
		
		log.info("ValidataDatatypes_01 - Step 30. Get actual Alloccated Rate");
		actualAllocatedRate = transactionPage.getValueAllocationRate().replace("%", "").replace(" ", "");
		
		log.info("ValidataDatatypes_01 - Step 31. Calculate Allocated Rate");
		expectedAllocatedRate = transactionPage.getExpectedAllocatedRate(appraisedValuePropertyNew, appraisedValuePropertyNew, appraisedValueProperty2, appraisedValueProperty3, 2);
		
		log.info("VP: Allocated Rate displays correctly");
		verifyEquals(actualAllocatedRate, expectedAllocatedRate);
		
		log.info("ValidataDatatypes_01 - Step 32: Click 'Go to Properties' button");
		transactionPage.clickGotoPropertiesButtonAtPropertyDetails();
		
		//Checking Allocated Rate Property 2 
		
		log.info("ValidataDatatypes_01 - Step 33. Search with Address name is '650 Beverly Street'");
		transactionPage.searchPropertyAddress(address2);
		
		log.info("ValidataDatatypes_01 - Step 34. Open a property detail");
		transactionPage.openPropertyDetail(address2);
		
		log.info("ValidataDatatypes_01 - Step 35. Get actual Alloccated Rate");
		actualAllocatedRate = transactionPage.getValueAllocationRate().replace("%", "").replace(" ", "");
		
		log.info("ValidataDatatypes_01 - Step 36. Calculate Allocated Rate");
		expectedAllocatedRate = transactionPage.getExpectedAllocatedRate(appraisedValueProperty2, appraisedValueProperty1, appraisedValueProperty2, appraisedValueProperty3, 2);
		
		log.info("VP: Allocated Rate displays correctly");
		verifyEquals(actualAllocatedRate, expectedAllocatedRate);
		
		log.info("ValidataDatatypes_01 - Step 37: Change 'Appraised Value' field value");
		transactionPage.inputAppraisedValue(appraisedValuePropertyNew);
		transactionPage.keyPressing("enter");
		
		log.info("ValidataDatatypes_01 - Step 38. Get actual Alloccated Rate");
		actualAllocatedRate = transactionPage.getValueAllocationRate().replace("%", "").replace(" ", "");
		
		log.info("ValidataDatatypes_01 - Step 39. Calculate Allocated Rate");
		expectedAllocatedRate = transactionPage.getExpectedAllocatedRate(appraisedValuePropertyNew, appraisedValuePropertyNew, appraisedValueProperty1, appraisedValueProperty3, 2);
		
		log.info("VP: Allocated Rate displays correctly");
		verifyEquals(actualAllocatedRate, expectedAllocatedRate);
		
		log.info("ValidataDatatypes_01 - Step 40: Click 'Go to Properties' button");
		transactionPage.clickGotoPropertiesButtonAtPropertyDetails();
		
		//Checking Allocated Rate Property 3
		
		log.info("ValidataDatatypes_01 - Step 41. Search with Address name is '1020 Bonforte Blvd'");
		transactionPage.searchPropertyAddress(address3);
		
		log.info("ValidataDatatypes_01 - Step 42. Open a property detail");
		transactionPage.openPropertyDetail(address3);
		
		log.info("ValidataDatatypes_01 - Step 43. Get actual Alloccated Rate");
		actualAllocatedRate = transactionPage.getValueAllocationRate().replace("%", "").replace(" ", "");
		
		log.info("ValidataDatatypes_01 - Step 44. Calculate Allocated Rate");
		expectedAllocatedRate = transactionPage.getExpectedAllocatedRate(appraisedValueProperty3, appraisedValueProperty1, appraisedValueProperty2, appraisedValueProperty3, 2);
		
		log.info("VP: Allocated Rate displays correctly");
		verifyEquals(actualAllocatedRate, expectedAllocatedRate);
		
		log.info("ValidataDatatypes_01 - Step 45: Change 'Appraised Value' field value");
		transactionPage.inputAppraisedValue(appraisedValuePropertyNew);
		transactionPage.keyPressing("enter");
		
		log.info("ValidataDatatypes_01 - Step 46. Get actual Alloccated Rate");
		actualAllocatedRate = transactionPage.getValueAllocationRate().replace("%", "").replace(" ", "");
		
		log.info("ValidataDatatypes_01 - Step 47. Calculate Allocated Rate");
		expectedAllocatedRate = transactionPage.getExpectedAllocatedRate(appraisedValuePropertyNew, appraisedValuePropertyNew, appraisedValueProperty2, appraisedValueProperty1, 2);
		
		log.info("VP: Allocated Rate displays correctly");
		verifyEquals(actualAllocatedRate, expectedAllocatedRate);
		
		log.info("ValidataDatatypes_01 - Step 48: Click 'Go to Properties' button");
		transactionPage.clickGotoPropertiesButtonAtPropertyDetails();
	}
	
	@Test(groups = { "regression" }, description = "Check Tax Details Calculation 02 - Check Pre-paid rent calculation")
	public void CheckTaxDetailsCalculation_02_CheckPrePaidRent() {


		log.info("ValidataDatatypes_01 - Step 07: Open Basic details tab");
		transactionPage.openBasicDetailsTab();

		log.info("ValidataDatatypes_01 - Step 08: Click 'Add Properties' button");
		transactionPage.inputInitialRent(initialRent);
		
		log.info("ValidataDatatypes_01 - Step 08: Click 'Add Properties' button");
		transactionPage.inputContractCommencementDate(contractCommencementDate);
		
		log.info("ValidataDatatypes_01 - Step 13: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Initial Rent saved successfully' message is displayed");
		verifyTrue(transactionPage.isInitialRentSavedSuccessfully(initialRent));
		
		log.info("VP: 'Contract Commencement Date saved successfully' message is displayed");
		verifyTrue(transactionPage.isContractCommencementDateSavedSuccessfully(contractCommencementDate));

		log.info("ValidataDatatypes_01 - Step 13: Click 'Save' button");
		transactionPage.clickOnDateSelecterIcon();
		
		log.info("ValidataDatatypes_01 - Step 09: Get number of days in month and prorated Days");
		numberOfDaysInMonth= transactionPage.getNumberOfDaysInMonth();
		proratedDays= numberOfDaysInMonth ;
		transactionPage.keyPressing("esc");
		
		log.info("ValidataDatatypes_01 - Step 19: Open Properties tab");
		transactionPage.openPropertyTab();
		
		//Check Pre-paid rent property 1
		
		log.info("ValidataDatatypes_01 - Step 10. Search with Address name is '148 Craft Drive'");
		transactionPage.searchPropertyAddress(address1);
		
		log.info("ValidataDatatypes_01 - Step 11. Open a property detail");
		transactionPage.openPropertyDetail(address1);
		
		log.info("ValidataDatatypes_01 - Step 11. Get actual Pre-paid rent");
		actualPrePaidRent = transactionPage.getValuePrePaidRent().replace("$", "").replace(",", "");
		
		log.info("ValidataDatatypes_01 - Step 11. Get expected Pre-paid rent");
		expectedPrePaidRent = transactionPage.getExpectedPrePaidRent(appraisedValueProperty1, appraisedValueProperty1, appraisedValueProperty2, appraisedValueProperty3, proratedDays, numberOfDaysInMonth, initialRent, 2);
		
		log.info("ValidataDatatypes_01 - Step 11. Get expected Pre-paid rent");
		actualTotalPrePaidRent += transactionPage.getExpectedPrePaidRentWithoutRound(appraisedValueProperty1, appraisedValueProperty1, appraisedValueProperty2, appraisedValueProperty3, proratedDays, numberOfDaysInMonth, initialRent);
		
		log.info("VP: Allocated Rate displays correctly");
		verifyEquals(actualPrePaidRent, expectedPrePaidRent);
		
		//Change Appraised value property 1
		
		log.info("ValidataDatatypes_01 - Step 37: Change 'Appraised Value' field value");
		transactionPage.inputAppraisedValue(appraisedValuePropertyNew);
		
		log.info("ValidataDatatypes_01 - Step 11. Get actual Pre-paid rent");
		actualPrePaidRent = transactionPage.getValuePrePaidRent().replace("$", "").replace(",", "");
		
		log.info("ValidataDatatypes_01 - Step 11. Get expected Pre-paid rent");
		expectedPrePaidRent = transactionPage.getExpectedPrePaidRent(appraisedValuePropertyNew, appraisedValuePropertyNew, appraisedValueProperty2, appraisedValueProperty3, proratedDays, numberOfDaysInMonth, initialRent, 2);
		
		log.info("VP: Allocated Rate displays correctly");
		verifyEquals(actualPrePaidRent, expectedPrePaidRent);
		
		log.info("ValidataDatatypes_01 - Step 48: Click 'Go to Properties' button");
		transactionPage.clickGotoPropertiesButtonAtPropertyDetails();
		
		//Check Pre-paid rent property 2
		
		log.info("ValidataDatatypes_01 - Step 33. Search with Address name is '650 Beverly Street'");
		transactionPage.searchPropertyAddress(address2);
		
		log.info("ValidataDatatypes_01 - Step 34. Open a property detail");
		transactionPage.openPropertyDetail(address2);
		
		log.info("ValidataDatatypes_01 - Step 11. Get actual Pre-paid rent");
		actualPrePaidRent = transactionPage.getValuePrePaidRent().replace("$", "").replace(",", "");
		
		log.info("ValidataDatatypes_01 - Step 11. Get expected Pre-paid rent");
		expectedPrePaidRent = transactionPage.getExpectedPrePaidRent(appraisedValueProperty2, appraisedValueProperty1, appraisedValueProperty2, appraisedValueProperty3, proratedDays, numberOfDaysInMonth, initialRent, 2);
		
		log.info("ValidataDatatypes_01 - Step 11. Get expected Pre-paid rent");
		actualTotalPrePaidRent += transactionPage.getExpectedPrePaidRentWithoutRound(appraisedValueProperty2, appraisedValueProperty1, appraisedValueProperty2, appraisedValueProperty3, proratedDays, numberOfDaysInMonth, initialRent);
		
		log.info("VP: Allocated Rate displays correctly");
		verifyEquals(actualPrePaidRent, expectedPrePaidRent);

		//Change Appraised value property 2
		
		log.info("ValidataDatatypes_01 - Step 37: Change 'Appraised Value' field value");
		transactionPage.inputAppraisedValue(appraisedValuePropertyNew);
		
		log.info("ValidataDatatypes_01 - Step 11. Get actual Pre-paid rent");
		actualPrePaidRent = transactionPage.getValuePrePaidRent().replace("$", "").replace(",", "");
		
		log.info("ValidataDatatypes_01 - Step 11. Get expected Pre-paid rent");
		expectedPrePaidRent = transactionPage.getExpectedPrePaidRent(appraisedValuePropertyNew, appraisedValuePropertyNew, appraisedValueProperty1, appraisedValueProperty3, proratedDays, numberOfDaysInMonth, initialRent, 2);
		
		log.info("VP: Allocated Rate displays correctly");
		verifyEquals(actualPrePaidRent, expectedPrePaidRent);
		
		log.info("ValidataDatatypes_01 - Step 48: Click 'Go to Properties' button");
		transactionPage.clickGotoPropertiesButtonAtPropertyDetails();
		
		//Check Pre-paid rent property 3
		
		log.info("ValidataDatatypes_01 - Step 41. Search with Address name is '1020 Bonforte Blvd'");
		transactionPage.searchPropertyAddress(address3);
		
		log.info("ValidataDatatypes_01 - Step 42. Open a property detail");
		transactionPage.openPropertyDetail(address3);
		
		log.info("ValidataDatatypes_01 - Step 11. Get actual Pre-paid rent");
		actualPrePaidRent = transactionPage.getValuePrePaidRent().replace("$", "").replace(",", "");
		
		log.info("ValidataDatatypes_01 - Step 11. Get expected Pre-paid rent");
		expectedPrePaidRent = transactionPage.getExpectedPrePaidRent(appraisedValueProperty3, appraisedValueProperty1, appraisedValueProperty2, appraisedValueProperty3, proratedDays, numberOfDaysInMonth, initialRent, 2);
		
		log.info("ValidataDatatypes_01 - Step 11. Get expected Pre-paid rent");
		actualTotalPrePaidRent += transactionPage.getExpectedPrePaidRentWithoutRound(appraisedValueProperty3, appraisedValueProperty1, appraisedValueProperty2, appraisedValueProperty3, proratedDays, numberOfDaysInMonth, initialRent);
		
		log.info("VP: Allocated Rate displays correctly");
		verifyEquals(actualPrePaidRent, expectedPrePaidRent);
		
		//Change Appraised value property 3
		
		log.info("ValidataDatatypes_01 - Step 37: Change 'Appraised Value' field value");
		transactionPage.inputAppraisedValue(appraisedValuePropertyNew);
		
		log.info("ValidataDatatypes_01 - Step 11. Get actual Pre-paid rent");
		actualPrePaidRent = transactionPage.getValuePrePaidRent().replace("$", "").replace(",", "");
		
		log.info("ValidataDatatypes_01 - Step 11. Get expected Pre-paid rent");
		expectedPrePaidRent = transactionPage.getExpectedPrePaidRent(appraisedValuePropertyNew, appraisedValuePropertyNew, appraisedValueProperty2, appraisedValueProperty1, proratedDays, numberOfDaysInMonth, initialRent, 2);
		
		log.info("VP: Allocated Rate displays correctly");
		verifyEquals(actualPrePaidRent, expectedPrePaidRent);
		
		log.info("ValidataDatatypes_01 - Step 48: Click 'Go to Properties' button");
		transactionPage.clickGotoPropertiesButtonAtPropertyDetails();
		
		//Check sums of all pre-paid rent
		
		log.info("VP: Sums pre-paid rent of all properties is correctly");
		expectedTotalPrePaidRent = Float.parseFloat(initialRent.replace(",", "")) * proratedDays/numberOfDaysInMonth; 
		verifyEquals(actualTotalPrePaidRent, expectedTotalPrePaidRent);
	}


	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName, appraisedValueProperty1, appraisedValueProperty2, appraisedValueProperty3, appraisedValuePropertyNew;
	private String propertiesFileName, address1, address2, address3;
	private String expectedAllocatedRate, actualAllocatedRate;
	private String initialRent, contractCommencementDate; 
	private int proratedDays, numberOfDaysInMonth;
	private String expectedPrePaidRent, actualPrePaidRent;
	private float expectedTotalPrePaidRent, actualTotalPrePaidRent = 0;
}