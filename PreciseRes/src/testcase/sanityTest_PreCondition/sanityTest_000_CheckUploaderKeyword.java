package sanityTest_PreCondition;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.LoginPage;
import page.PageFactory;
import page.UploaderLoginPage;
import page.UploaderPage;

import common.AbstractTest;
import common.Constant;

public class sanityTest_000_CheckUploaderKeyword extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameB2RLender = Constant.USERNAME_B2R_LENDER;
		passwordB2RLender = Constant.PASSWORD_B2R_LENDER;
		usernameCAFLender = Constant.USERNAME_CAF_LENDER;
		passwordCAFLender = Constant.PASSWORD_CAF_LENDER;
		usernameB2RMember = "auto-B2R-member2014";
		passwordB2RMember = "change";
		usernameCAFMember = "auto-CAF-member2014";
		passwordCAFMember = "change";
		usernameB2RApplicant = Constant.USERNAME_B2R_BORROWER;
		passwordB2RApplicant = Constant.PASSWORD_B2R_BORROWER;
		usernameCAFApplicant = Constant.USERNAME_CAF_BORROWER;
		passwordCAFApplicant = Constant.PASSWORD_CAF_BORROWER;
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}
	
	@Test(groups = { "regression" },description = "CheckUploaderKeyword_01 - Check Uploader keyword for CAF ")
	public void CheckUploaderKeyword_01_CAF()
	{
		
		log.info("CheckUploaderKeyword_01 - Step 01. Go to Uploader");
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		//Check with Lender
		log.info("CheckUploaderKeyword_01 - Step 02. Login with Lender users on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameCAFLender, passwordCAFLender);
		
		log.info("CheckUploaderKeyword_01 - Step 03. Click on 'Keywords' button");
		uploaderPage.clickOnKeywordsButton();
		
		log.info("VP: Abbreviation for keywords display correctly on table");
		verifyEquals(uploaderPage.getKeywordForAutoCast("Evidence of Insurance"),"ins");
		verifyEquals(uploaderPage.getKeywordForAutoCast("Lease Agreement"),"lea");
		verifyEquals(uploaderPage.getKeywordForAutoCast("Lead-Based Paint Disclosure"),"lbpd");
		uploaderPage.clickOkButtonOnKeywords();
		
		//Check with Member
		log.info("CheckUploaderKeyword_01 - Step 04. Login with Member users on Uploader page");
		uploaderLoginPage = uploaderPage.logoutUploader();
		uploaderPage = uploaderLoginPage.loginUploader(usernameCAFMember, passwordCAFMember);
		
		log.info("CheckUploaderKeyword_01 - Step 05. Click on 'Keywords' button");
		uploaderPage.clickOnKeywordsButton();
		
		log.info("VP: Abbreviation for keywords display correctly on table");
		verifyEquals(uploaderPage.getKeywordForAutoCast("Evidence of Insurance"),"ins");
		verifyEquals(uploaderPage.getKeywordForAutoCast("Lease Agreement"),"lea");
		verifyEquals(uploaderPage.getKeywordForAutoCast("Lead-Based Paint Disclosure"),"lbpd");
		uploaderPage.clickOkButtonOnKeywords();
		
		//Check with Borrower
		log.info("CheckUploaderKeyword_01 - Step 06. Login with Member users on Uploader page");
		uploaderLoginPage = uploaderPage.logoutUploader();
		uploaderPage = uploaderLoginPage.loginUploader(usernameCAFApplicant, passwordCAFApplicant);
		
		log.info("CheckUploaderKeyword_01 - Step 07. Click on 'Keywords' button");
		uploaderPage.clickOnKeywordsButton();
		
		log.info("VP: Abbreviation for keywords display correctly on table");
		verifyEquals(uploaderPage.getKeywordForAutoCast("Evidence of Insurance"),"ins");
		verifyEquals(uploaderPage.getKeywordForAutoCast("Lease Agreement"),"lea");
		verifyEquals(uploaderPage.getKeywordForAutoCast("Lead-Based Paint Disclosure"),"lbpd");
		uploaderPage.clickOkButtonOnKeywords();
	}
	
	@Test(groups = { "regression" },description = "CheckUploaderKeyword_01 - Check Uploader keyword for B2R ")
	public void CheckUploaderKeyword_02_B2R()
	{
		
		log.info("CheckUploaderKeyword_02 - Step 01. Go to Uploader");
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		//Check with Lender
		log.info("CheckUploaderKeyword_02 - Step 02. Login with Lender users on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameB2RLender, passwordB2RLender);
		
		log.info("CheckUploaderKeyword_02 - Step 03. Click on 'Keywords' button");
		uploaderPage.clickOnKeywordsButton();
		
		log.info("VP: Abbreviation for keywords display correctly on table");
		verifyEquals(uploaderPage.getKeywordForAutoCast("P2 - Lease Agreement"),"P2");
		verifyEquals(uploaderPage.getKeywordForAutoCast("P3 - Purchase Contract/HUD-1 Settlement Stmt"),"P3");
		verifyEquals(uploaderPage.getKeywordForAutoCast("P10 - Lead-Based Paint Disclosure"),"P10");
		uploaderPage.clickOkButtonOnKeywords();
		
		//Check with Member
		log.info("CheckUploaderKeyword_02 - Step 04. Login with Member users on Uploader page");
		uploaderLoginPage = uploaderPage.logoutUploader();
		uploaderPage = uploaderLoginPage.loginUploader(usernameB2RMember, passwordB2RMember);
		
		log.info("CheckUploaderKeyword_02 - Step 05. Click on 'Keywords' button");
		uploaderPage.clickOnKeywordsButton();
		
		log.info("VP: Abbreviation for keywords display correctly on table");
		verifyEquals(uploaderPage.getKeywordForAutoCast("P2 - Lease Agreement"),"P2");
		verifyEquals(uploaderPage.getKeywordForAutoCast("P3 - Purchase Contract/HUD-1 Settlement Stmt"),"P3");
		verifyEquals(uploaderPage.getKeywordForAutoCast("P10 - Lead-Based Paint Disclosure"),"P10");
		uploaderPage.clickOkButtonOnKeywords();
		
		//Check with Borrower
		log.info("CheckUploaderKeyword_02 - Step 06. Login with Member users on Uploader page");
		uploaderLoginPage = uploaderPage.logoutUploader();
		uploaderPage = uploaderLoginPage.loginUploader(usernameB2RApplicant, passwordB2RApplicant);
		
		log.info("CheckUploaderKeyword_02 - Step 07. Click on 'Keywords' button");
		uploaderPage.clickOnKeywordsButton();
		
		log.info("VP: Abbreviation for keywords display correctly on table");
		verifyEquals(uploaderPage.getKeywordForAutoCast("P2 - Lease Agreement"),"P2");
		verifyEquals(uploaderPage.getKeywordForAutoCast("P3 - Purchase Contract/HUD-1 Settlement Stmt"),"P3");
		verifyEquals(uploaderPage.getKeywordForAutoCast("P10 - Lead-Based Paint Disclosure"),"P10");
		uploaderPage.clickOkButtonOnKeywords();
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private String usernameB2RLender, passwordB2RLender;
	private String uploaderPageUrl;
	private String usernameCAFLender, passwordCAFLender, usernameB2RMember, passwordB2RMember, usernameCAFMember, passwordCAFMember, usernameB2RApplicant, passwordB2RApplicant, usernameCAFApplicant, passwordCAFApplicant;
}