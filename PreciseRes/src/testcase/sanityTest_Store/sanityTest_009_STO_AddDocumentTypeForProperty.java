package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;
import common.AbstractTest;
import common.Common;
import common.Constant;

public class sanityTest_009_STO_AddDocumentTypeForProperty extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + getUniqueNumber();
		propertiesFileName = "StoreDataTape.xlsx";
		address = "1020 Bonforte Blvd";
		newDocumentType = "New Document Type";
		documentType01 = "Title Commitment";
		documentType02 = "Appraisal";
		documentFileName01 = "datatest1.pdf";
		documentFileName02 = "datatest2.pdf";
		year = Common.getCommon().getCurrentYearOfWeek();
		submitVia = "Store Automation";
	}

	@Test(groups = { "regression" }, description = "Document 01 - Add new document type in Property details screen")
	public void Document_01_AddNewDocumentType() {
		
		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("Precondition - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("Precondition - Step 07: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("Precondition - Step 08: Click 'Add Properties' button");
		transactionPage.clickAddPropertyButton();

		log.info("Precondition - Step 09: Load 'Property Excel Template' file");
		transactionPage.uploadFileProperties(propertiesFileName);

		log.info("Precondition - Step 10. Search with Address name is '1020 Bonforte Blvd'");
		transactionPage.searchPropertyAddress(address);
		propertyNumberBefore = transactionPage.getNumberOfAddressInPropertiesTab(address);

		log.info("Precondition - Step 11. Open a property detail");
		transactionPage.openPropertyDetail(address);

		log.info("Document_01 - Step 01. Click 'New Property Documents' button");
		transactionPage.clickNewPropertyDocumentsButton();

		log.info("Document_01 - Step 02. Input to 'Other Document Type' textbox");
		transactionPage.inputOtherDocumentType(newDocumentType);

		log.info("Document_01 - Step 03. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("Document_01 - Step 04. Click 'Go to Property' button");
		transactionPage.clickGoToPropertyButtonAtDocumentTypeDetails();

		log.info("VP: New Document Type is created successfully");
		verifyTrue(transactionPage.isDocumentTypeCreatedSuccessfully(newDocumentType));
	}

	@Test(groups = { "regression" }, description = "Document 02 - Verify the document type for the property is increased by one")
	public void Document_02_VerifyDocumentOfPropertyIncreasedByOne() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("Document_02 - Step 01: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("Document_02 - Step 02. Search with Address name is '1020 Bonforte Blvd'");
		transactionPage.searchPropertyAddress(address);

		log.info("VP: Document type for the property is increased by one");
		propertyNumberBefore = propertyNumberBefore + 1;
		propertyNumberAfter = transactionPage.getNumberOfAddressInPropertiesTab(address);
		verifyEquals(propertyNumberBefore, propertyNumberAfter);
	}

	@Test(groups = { "regression" }, description = "Document 03 - Delete document type in Property details screen")
	public void Document_03_DeleteDocumentType() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("Document_03 - Step 01: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("Document_03 - Step 02. Search with Address name is '1020 Bonforte Blvd'");
		transactionPage.searchPropertyAddress(address);

		log.info("Document_03 - Step 03. Open a property detail");
		transactionPage.openPropertyDetail(address);

		log.info("Document_03 - Step 04. Delete new Document Type");
		transactionPage.deleteDocumentTypeNotFileName(newDocumentType);

		log.info("Document_03 - Step 05. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("VP: New Document Type is deleted successfully");
		verifyFalse(transactionPage.isDocumentTypeCreatedSuccessfully(newDocumentType));
	}

	@Test(groups = { "regression" }, description = "Document 04 - Verify the document type for the property is decreased by one")
	public void Document_04_VerifyDocumentOfPropertyDecreasedByOne() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("Document_04 - Step 01: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("Document_04 - Step 02. Search with Address name is '1020 Bonforte Blvd'");
		transactionPage.searchPropertyAddress(address);

		log.info("VP: Document type for the property is decreased by one");
		propertyNumberBefore = propertyNumberBefore - 1;
		propertyNumberAfter = transactionPage.getNumberOfAddressInPropertiesTab(address);
		verifyEquals(propertyNumberBefore, propertyNumberAfter);
	}

	@Test(groups = { "regression" }, description = "Document 05 - Upload file document (.pdf, .txt, .xml...) for document type (Open document type details -> Upload file)")
	public void Document_05_UploadDocumentFileByNewDocumentType() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("Document_05 - Step 01: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("Document_05 - Step 02. Search with Address name is '1020 Bonforte Blvd'");
		transactionPage.searchPropertyAddress(address);

		log.info("Document_05 - Step 03. Open a property detail");
		transactionPage.openPropertyDetail(address);

		log.info("Document_05 - Step 04. Click 'New Property Documents' button");
		transactionPage.clickNewPropertyDocumentsButton();

		log.info("Document_05 - Step 05. Select document type");
		transactionPage.selectDocumentType(documentType01);

		log.info("Document_05 - Step 06. Upload document file name");
		transactionPage.uploadDocumentFileName(documentFileName01);

		log.info("Document_05 - Step 07. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("Document_05 - Step 08. Click 'Go to Property' button");
		transactionPage.clickGoToPropertyButtonAtDocumentTypeDetails();

		log.info("VP: Document type/ file name is created/ uploaded successfully");
		verifyTrue(transactionPage.isDocumentLoadedToDocumentType(documentType01, documentFileName01));
	}

	@Test(groups = { "regression" }, description = "Document 06 - Upload file document (.pdf, .txt, .xml...) for document type (Selecting the placeholder)")
	public void Document_06_UploadDocumentFileBySelectingThePlaceholder() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("Document_06 - Step 01: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("Document_06 - Step 02. Search with Address name is '1020 Bonforte Blvd'");
		transactionPage.searchPropertyAddress(address);

		log.info("Document_06 - Step 03. Open a property detail");
		transactionPage.openPropertyDetail(address);

		log.info("Document_06 - Step 04. Add document file name by clicking the placeholder");
		transactionPage.uploadDynamicDocumentFileByPlaceHolder(documentType02, documentFileName01);

		log.info("Document_06 - Step 05. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("VP: Document file name is uploaded successfully");
		verifyTrue(transactionPage.isDocumentLoadedToDocumentType(documentType02, documentFileName01));
	}

	@Test(groups = { "regression" }, description = "Document 07 - Make sure you can see both the old and new document")
	public void Document_07_OpenBothTheOldDocumentAndNewDocument() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("Document_07 - Step 01: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("Document_07 - Step 02. Search with Address name is '1020 Bonforte Blvd'");
		transactionPage.searchPropertyAddress(address);

		log.info("Document_07 - Step 03. Open a property detail");
		transactionPage.openPropertyDetail(address);

		log.info("Document_07 - Step 04. Click 'New Property Documents' button");
		transactionPage.clickNewPropertyDocumentsButton();

		log.info("Document_07 - Step 05. Select document type");
		transactionPage.selectDocumentType(documentType01);

		log.info("Document_07 - Step 06. Upload document file name 01");
		transactionPage.uploadDocumentFileName(documentFileName01);

		log.info("Document_07 - Step 07. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("Document_07 - Step 08. Upload document file name 02");
		transactionPage.uploadDocumentFileName(documentFileName02);

		log.info("Document_07 - Step 09. Click Save button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("VP: Document file name 01 is uploaded successfully");
		verifyTrue(transactionPage.isDocumentHistoryLoadedToDocumentType(documentFileName01, submitVia, "/" + year));

		log.info("VP: Document file name 02 is uploaded successfully");
		verifyTrue(transactionPage.isDocumentHistoryLoadedToDocumentType(documentFileName02, submitVia, "/" + year));
	}

	@Test(groups = { "regression" }, description = "Document 08 - Verify the required documents for the property equal 3")
	public void Document_08_VerifyRequiredDocumentOfPropertyEqualThree() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("Document_08 - Step 01: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("Document_08 - Step 02. Search with Address name is '1020 Bonforte Blvd'");
		transactionPage.searchPropertyAddress(address);

		log.info("VP: Verify the required documents for the property equal 3");
		verifyTrue(transactionPage.isRequiredDocumentLoadedToDocumentColumn(address, "3 of"));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName, propertiesFileName, address, submitVia;
	private int propertyNumberBefore, propertyNumberAfter, year;
	private String newDocumentType, documentType01, documentType02, documentFileName01, documentFileName02;
}