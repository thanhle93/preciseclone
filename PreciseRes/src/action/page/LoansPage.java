package page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import PreciseRes.Interfaces;
import common.Common;

public class LoansPage extends AbstractPage {
	public LoansPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}

	/**
	 * check Loans page display
	 * 
	 * @param N/A
	 */
	public boolean isLoanPageDisplay() {
		waitForControl(driver, Interfaces.LoansPage.LOANS_PAGE_SECTION, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.LOANS_PAGE_SECTION);
	}

	/**
	 * check Loans Detail Page Display
	 */
	public boolean isLoansDetailPageDisplay(String loanName) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL, timeWait);
		String realLoanName = getAttributeValue(driver, Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL, "value");
		return realLoanName.contains(loanName);
	}

	/**
	 * get First Loan Item Name
	 */
	public String getFirstLoanItemName() {
		waitForControl(driver, Interfaces.LoansPage.FIRST_LOAN_ITEM, timeWait);
		String itemName = getText(driver, Interfaces.LoansPage.FIRST_LOAN_ITEM);
		return itemName;
	}

	/**
	 * select First Loan Item Name
	 */
	public void selectFirstLoansItem() {
		waitForControl(driver, Interfaces.LoansPage.FIRST_LOAN_ITEM, timeWait);
		String url = getControlHref(driver, Interfaces.LoansPage.FIRST_LOAN_ITEM);
		driver.get(url);
		sleep();
	}

	/**
	 * check Error Process Request Display
	 */
	public boolean isErrorProcessRequestDisplay() {
		waitForControl(driver, Interfaces.LoansPage.PROCESS_REQUEST_ERROR_TITLE, timeWait);
		boolean titleDisplay = isControlDisplayed(driver, Interfaces.LoansPage.PROCESS_REQUEST_ERROR_TITLE);
		boolean contentDisplay = isControlDisplayed(driver, Interfaces.LoansPage.PROCESS_REQUEST_ERROR_CONTENT);
		return titleDisplay && contentDisplay;
	}

	/**
	 * check Username Display Above Main Menu
	 */
	public boolean isUsernameDisplayTopLeftCorner(String userFullName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_USER_FULL_NAME_TITLE, userFullName, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_USER_FULL_NAME_TITLE, userFullName);
	}

	/**
	 * check Logged As Username Display Right Left Corner
	 */
	public boolean isLoggedAsUsernameDisplayRightLeftCorner(String userName) {
		waitForControl(driver, Interfaces.LoansPage.USER_FULL_NAME_LOGGED, userName, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.USER_FULL_NAME_LOGGED, userName);
	}

	/**
	 * check Tab Active On Main Menu
	 */
	public boolean isTabActiveOnMainMenu(String tabName) {
		waitForControl(driver, Interfaces.LoansPage.TAB_ACTIVE_ON_MAIN_MENU, timeWait);
		String actualTab = getText(driver, Interfaces.LoansPage.TAB_ACTIVE_ON_MAIN_MENU);
		return actualTab.contains(tabName);
	}

	/**
	 * search Loan By Name
	 */
	public void searchLoanByName(String loanName) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, loanName);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search2').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(7);
		}
		sleep();
	}

	/**
	 * open Loans Detail Page
	 */
	public void openLoansDetailPage(String loanName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_LOAN_ITEM, loanName, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_LOAN_ITEM, loanName);
		sleep(1);
	}

	/**
	 * open Loans Audit Detail Page
	 */
	public void openLoansAuditDetailPage(String loanName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_LOAN_AUDIT_ITEM, loanName, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_LOAN_AUDIT_ITEM, loanName);
		sleep(1);
	}

	/**
	 * open Basic Detail Tab
	 */
	public void openBasicDetailTab() {
		waitForControl(driver, Interfaces.LoansPage.BASIC_DETAIL_TAB, timeWait);
		executeJavaScript(driver, "document.getElementById('CBRLoanApplicationTB').click();");
		executeJavaScript(driver, "document.getElementById('LoanApplicationTB').click();");
		sleep(1);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * click New Loan Application Entity
	 */
	public void clickNewLoanApplicationEntity() {
		waitForControl(driver, Interfaces.LoansPage.NEW_LOAN_APPLICATION_ENTITY_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.NEW_LOAN_APPLICATION_ENTITY_BUTTON);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * select Entity Type
	 */
	public void selectEntityType(String entityType) {
		waitForControl(driver, Interfaces.LoansPage.ENTITY_TYPE_COMBOBOX, entityType, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.ENTITY_TYPE_COMBOBOX, entityType);
		sleep(1);
	}

	/**
	 * select Applicant Type
	 */
	public void selectApplicantType(String applicantType) {
		waitForControl(driver, Interfaces.LoansPage.APPLICANT_ENTITY_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.APPLICANT_ENTITY_COMBOBOX, applicantType);
		sleep(1);
	}

	/**
	 * select Borrower Type
	 */
	public void selectBorrowerType(String borrowerType) {
		waitForControl(driver, Interfaces.LoansPage.BORROWER_ENTITY_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.BORROWER_ENTITY_COMBOBOX, borrowerType);
		sleep(1);
	}

	/**
	 * select Guarantor Type
	 */
	public void selectGuarantorType(String guarantorType) {
		waitForControl(driver, Interfaces.LoansPage.GUARANTOR_ENTITY_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.GUARANTOR_ENTITY_COMBOBOX, guarantorType);
		sleep(1);
	}

	/**
	 * select Sponsor Type
	 */
	public void selectSponsorType(String sponsorType) {
		waitForControl(driver, Interfaces.LoansPage.SPONSOR_ENTITY_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.SPONSOR_ENTITY_COMBOBOX, sponsorType);
		sleep(1);
	}

	/**
	 * select Pledgor Type
	 */
	public void selectPledgorType(String pledgorType) {
		waitForControl(driver, Interfaces.LoansPage.PLEDGOR_ENTITY_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.PLEDGOR_ENTITY_COMBOBOX, pledgorType);
		sleep(1);
	}

	/**
	 * click New Applicant Link
	 */
	public void clickNewApplicantLink() {
		waitForControl(driver, Interfaces.LoansPage.NEW_APPLICANT_LINK, timeWait);
		click(driver, Interfaces.LoansPage.NEW_APPLICANT_LINK);
		sleep();
	}

	/**
	 * input Applicant Info To Create
	 */
	public void inputApplicantInfoToCreate(String accountNumber, String accountName, String addressNumber, String addressDictrict, String city, String state, String zipCode) {
		waitForControl(driver, Interfaces.LoansPage.ACCOUNT_NUMBER_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.ACCOUNT_NUMBER_TEXTBOX, accountNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_NAME_TEXTBOX, accountName);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_ADDRESS_STREET_TEXTBOX, addressNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_ADDRESS_DISTRICT_TEXTBOX, addressDictrict);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_CITY_TEXTBOX, city);
		sleep(1);
		selectItemCombobox(driver, Interfaces.LoansPage.ACCOUNT_STATE_TEXTBOX, state);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_ZIPCODE_TEXTBOX, zipCode);
		sleep(1);
	}

	/**
	 * click Save New Applicant Button
	 */
	public void clickSaveNewApplicantButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_NEW_APPLICANT_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.SAVE_NEW_APPLICANT_BUTTON);
		sleep();
	}

	/**
	 * click Save Loans Applicant Entity Button
	 */
	public void clickSaveLoansApplicantEntityButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_LOAN_APPLICANT_ENTITY_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
		sleep();
	}

	/**
	 * check New Applicant Saved
	 */
	public boolean isNewApplicantSaved(String applicantName) {
		waitForControl(driver, Interfaces.LoansPage.APPLICANT_ENTITY_COMBOBOX, timeWait);
		sleep();
		String applicant = getItemSelectedCombobox(driver, Interfaces.LoansPage.APPLICANT_ENTITY_COMBOBOX);
		String fullname = getAttributeValue(driver, Interfaces.LoansPage.APPLICANT_FULLNAME_TEXTBOX, "value");
		return applicant.contains(applicantName) && fullname.contains(applicantName);
	}

	/**
	 * click Back Button
	 */
	public void clickBackButton() {
		waitForControl(driver, Interfaces.LoansPage.BACK_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Back').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
		sleep();
	}

	/**
	 * check New Applicant Display On LAE Table
	 */
	public boolean isNewApplicantDisplayOnLAETable(String applicantName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_APPLICANT_NAME_ON_TABLE, applicantName, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_APPLICANT_NAME_ON_TABLE, applicantName);
	}

	/**
	 * delete Applicant In Table
	 */
	public void deleteApplicantInTable(String applicantName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_DELETE_APPLICANT_CHECKBOX, applicantName, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_DELETE_APPLICANT_CHECKBOX, applicantName);
		sleep(1);
		clickSaveButton();
		sleep();
	}

	/**
	 * switch to new loan entity frame
	 */
	public WebDriver switchToNewLoansEntityFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.LoansPage.NEW_LOAN_ENTITY_FRAME, timeWait);
		sleep();
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.LoansPage.NEW_LOAN_ENTITY_FRAME)));
		return driver;
	}

	/**
	 * create new Loan item
	 */
	public void createNewLoan(String loanName, String applicantName, String borrowerName, String loanStatus) {
		clickNewLoanButton();
		waitForControl(driver, Interfaces.LoansPage.LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.LEGAL_NAME_TEXTBOX, loanName);
		if (!applicantName.equals("")) {
			selectItemCombobox(driver, Interfaces.LoansPage.APPLICANT_COMBOBOX, applicantName);
		}
		if (!borrowerName.equals("")) {
			selectItemCombobox(driver, Interfaces.LoansPage.BORROWER_COMBOBOX, borrowerName);
		}
		selectItemCombobox(driver, Interfaces.LoansPage.LOAN_APPLICANTION_STATUS_COMBOBOX, loanStatus);
		clickSaveLoanApplicationButton();
	}

	/**
	 * create new Loan item
	 */
	public void createNewLoan(String loanName, String applicantName, String borrowerName, String loanStatus, String loanID) {
		clickNewLoanButton();
		waitForControl(driver, Interfaces.LoansPage.LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.LEGAL_NAME_TEXTBOX, loanName);
		if (!applicantName.equals("")) {
			selectItemCombobox(driver, Interfaces.LoansPage.APPLICANT_COMBOBOX, applicantName);
		}
		if (!borrowerName.equals("")) {
			selectItemCombobox(driver, Interfaces.LoansPage.BORROWER_COMBOBOX, borrowerName);
		}
		selectItemCombobox(driver, Interfaces.LoansPage.LOAN_APPLICANTION_STATUS_COMBOBOX, loanStatus);
		type(driver, Interfaces.LoansPage.LOAN_ID_TEXTBOX, loanID);
		clickSaveLoanApplicationButton();
	}

	/**
	 * create new Loan - Audit item
	 */
	public void createNewLoanAudit(String loanName, String loanID, String loanStatus, String applicantName, String borrowerName, String gurantorName) {
		clickNewLoanButton();
		waitForControl(driver, Interfaces.LoansPage.LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.LEGAL_NAME_TEXTBOX, loanName);
		type(driver, Interfaces.LoansPage.LOAN_ID_TEXTBOX, loanID);
		selectItemCombobox(driver, Interfaces.LoansPage.LOAN_APPLICANTION_STATUS_COMBOBOX, loanStatus);
		selectItemCombobox(driver, Interfaces.LoansPage.APPLICANT_COMBOBOX, applicantName);
		selectItemCombobox(driver, Interfaces.LoansPage.BORROWER_COMBOBOX, borrowerName);
		selectItemCombobox(driver, Interfaces.LoansPage.GUARANTOR_COMBOBOX, gurantorName);
		clickSaveLoanApplicationButton();
	}

	/**
	 * create new Loan full
	 */
	public void createNewLoanFull(String loanName, String loanID, String loanStatus, String applicantName, String borrowerName, String gurantorName, String sponsorName,
			String pledgorName) {
		clickNewLoanButton();
		waitForControl(driver, Interfaces.LoansPage.LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.LEGAL_NAME_TEXTBOX, loanName);
		if (!loanID.equals("")) {
			type(driver, Interfaces.LoansPage.LOAN_ID_TEXTBOX, loanID);
		}
		selectItemCombobox(driver, Interfaces.LoansPage.LOAN_APPLICANTION_STATUS_COMBOBOX, loanStatus);
		selectItemCombobox(driver, Interfaces.LoansPage.APPLICANT_COMBOBOX, applicantName);
		selectItemCombobox(driver, Interfaces.LoansPage.BORROWER_COMBOBOX, borrowerName);
		selectItemCombobox(driver, Interfaces.LoansPage.GUARANTOR_COMBOBOX, gurantorName);
		selectItemCombobox(driver, Interfaces.LoansPage.NEW_SPONSOR_COMBOBOX, sponsorName);
		selectItemCombobox(driver, Interfaces.LoansPage.NEW_PLEDGOR_COMBOBOX, pledgorName);
		clickSaveLoanApplicationButton();
	}

	/**
	 * get text Error message
	 */
	public String getTextErrorMessage() {
		waitForControl(driver, Interfaces.LoansPage.ERROR_MESSAGE_TITLE, timeWait);
		String text = getText(driver, Interfaces.LoansPage.ERROR_MESSAGE_TITLE);
		return text;
	}

	/**
	 * input Loan ID
	 */
	public void inputLoanID(String loanID) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_APPLICATION_ID, timeWait);
		type(driver, Interfaces.LoansPage.LOAN_APPLICATION_ID, loanID);
		sleep(1);
	}

	/**
	 * input Final Borrower LLC, Loan Application Status
	 */
	public void inputFinalBorrowerAndFundedStatus(String finalLLC, String fundedStatus) {
		waitForControl(driver, Interfaces.LoansPage.FINAL_BORROWER_LLC_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.FINAL_BORROWER_LLC_TEXTBOX, finalLLC);
		selectItemCombobox(driver, Interfaces.LoansPage.LOAN_STATUS_COMBOBOX_BASIC_DETAIL, fundedStatus);
		sleep(1);
	}

	/**
	 * click New Loan Button
	 */
	public void clickNewLoanButton() {
		waitForControl(driver, Interfaces.LoansPage.NEW_LOAN_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('New').click();");
		sleep();
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * click Save Loan Application Button
	 */
	public void clickSaveLoanApplicationButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_LOAN_APPLICATION_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * check Loans Display With Correct Info
	 */
	public boolean isLoansDisplayWithCorrectInfo(String loanName, String loanStatus) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL, timeWait);
		String realLoanName = getAttributeValue(driver, Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL, "value");
		String realStatus = getItemSelectedCombobox(driver, Interfaces.LoansPage.LOAN_STATUS_COMBOBOX_BASIC_DETAIL);
		return realLoanName.contains(loanName) && realStatus.contains(loanStatus);
	}

	/**
	 * check Loans [Read Only - B2R] display with correct info
	 */
	public boolean isLoansReadOnlyDisplayWithCorrectInfo(String loanName, String loanStatus) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL, timeWait);
		String realLoanName = getAttributeValue(driver, Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL, "value");
		String realStatus = getText(driver, Interfaces.LoansPage.LOAN_STATUS_COMBOBOX_BASIC_DETAIL);
		return realLoanName.contains(loanName) && realStatus.contains(loanStatus);
	}

	/**
	 * open Properties Tab
	 */
	public void openPropertiesTab() {
		waitForControl(driver, Interfaces.LoansPage.PROPERTIES_TAB, timeWait);
		click(driver, Interfaces.LoansPage.PROPERTIES_TAB);
		executeJavaScript(driver, "document.getElementById('PropertyTB').click();");
		sleep();
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * upload File Properties
	 */
	public void uploadFileProperties(String propertiesFileName) {
		waitForControl(driver, Interfaces.LoansPage.UPLOAD_FILE_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.UPLOAD_FILE_BUTTON);
		waitForControl(driver, Interfaces.LoansPage.CHOOSE_FILE_BUTTON, timeWait);
		doubleClick(driver, Interfaces.LoansPage.CHOOSE_FILE_BUTTON);
		sleep();
		Common.getCommon().openFileForUpload(driver, propertiesFileName);
		sleep();
		clickSaveLoadPropertyButton();
		sleep();
		waitForControl(driver, Interfaces.LoansPage.FIND_PROPERTIES_LOAN_APPLICANT_SEARCH_SECTION, timeWait);
	}

	/**
	 * click Save Load Property Button
	 */
	public void clickSaveLoadPropertyButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_LOAD_PROPERTY_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.SAVE_LOAD_PROPERTY_BUTTON);
		executeJavaScript(driver, "document.getElementById('SaveFile').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(20);
		}
		sleep(1);
	}

	/**
	 * click Go To Loan Button
	 */
	public void clickGoToLoanButton() {
		waitForControl(driver, Interfaces.LoansPage.GO_TO_LOAN_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.GO_TO_LOAN_BUTTON);
		sleep();
	}

	/**
	 * check Property Display
	 */
	public boolean isPropertyDisplay(String address, String city, String state, String zip) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_PROPERTY_ITEM, city, state, zip, address, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_PROPERTY_ITEM, city, state, zip, address);
	}

	/**
	 * open General Document Tab
	 */
	public DocumentsPage openGeneralDocumentTab() {
		waitForControl(driver, Interfaces.LoansPage.GENERAL_LOANS_DOCUMENT_TAB, timeWait);
		executeJavaScript(driver, "document.getElementById('GeneralLoanDocsTB').click();");
		executeJavaScript(driver, "document.getElementById('CBRGeneralLoanDocsTB').click();");
		sleep();
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		return PageFactory.getDocumentsPage(driver, ipClient);
	}

	/**
	 * open Property Detail
	 */
	public PropertiesPage openPropertyDetail(String address) {
		searchAddress(address);
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_ADDRESS_PROPERTY, address, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_ADDRESS_PROPERTY, address);
		sleep(3);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		return PageFactory.getPropertiesPage(driver, ipClient);
	}

	/**
	 * search property address
	 */
	public void searchAddress(String address) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, address);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * create New Reporting Schedule
	 */
	public void createNewReportingSchedule(String reportType, String frequency, String daysPrior, boolean repeat) {
		clickNewReportingSchedule();
		waitForControl(driver, Interfaces.LoansPage.REPORT_TYPE_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.REPORT_TYPE_COMBOBOX, reportType);
		selectItemCombobox(driver, Interfaces.LoansPage.FREQUENCY_COMBOBOX, frequency);
		type(driver, Interfaces.LoansPage.DAYS_PRIOR_TEXTBOX, daysPrior);
		checkTheCheckbox(driver, Interfaces.LoansPage.REPEAT_CHECKBOX);
		clickSaveReportingScheduleDetail();
	}

	/**
	 * open Reporting Schedule Tab
	 */
	public void openReportingScheduleTab() {
		waitForControl(driver, Interfaces.LoansPage.REPORTING_SCHEDULE_TAB, timeWait);
		executeJavaScript(driver, "document.getElementById('DocScheduleTB').click();");
		sleep();
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * click New Reporting Schedule
	 */
	public void clickNewReportingSchedule() {
		waitForControl(driver, Interfaces.LoansPage.NEW_REPORTING_SCHEDULE_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.NEW_REPORTING_SCHEDULE_BUTTON);
		sleep();
	}

	/**
	 * click Save Reporting Schedule Detail
	 */
	public void clickSaveReportingScheduleDetail() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_REPORTING_SCHEDULE_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.SAVE_REPORTING_SCHEDULE_BUTTON);
		sleep();
	}

	/**
	 * search Reporting Schedule
	 */
	public void searchReportingSchedule(String reportType, String frequency, String daysPrior, boolean repeat) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_REPORT_TYPE_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.SEARCH_REPORT_TYPE_COMBOBOX, reportType);
		selectItemCombobox(driver, Interfaces.LoansPage.SEARCH_FREQUENCY_COMBOBOX, frequency);
		type(driver, Interfaces.LoansPage.SEARCH_DAYS_PRIOR_TEXTBOX, daysPrior);
		if (repeat) {
			click(driver, Interfaces.LoansPage.YES_RADIOBUTTON);
		} else {
			click(driver, Interfaces.LoansPage.NO_RADIOBUTTON);
		}
		click(driver, Interfaces.LoansPage.SEARCH_BUTTON);
		sleep();
	}

	/**
	 * check Reporting Schedule Display
	 */
	public boolean isReportingScheduleDisplay(String reportType, String frequency, String daysPrior, boolean repeat) {
		String temp = "No";
		if (repeat) {
			temp = "Yes";
		}
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_REPORTING_SCHEDULE_ITEM, reportType, frequency, daysPrior, temp, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_REPORTING_SCHEDULE_ITEM, reportType, frequency, daysPrior, temp);
	}

	/**
	 * open Corporate Entity Document Tab
	 */
	public DocumentsPage openCorporateEntityDocumentsTab() {
		waitForControl(driver, Interfaces.LoansPage.CORPORATE_ENTITY_DOCUMENT_TAB, timeWait);
		executeJavaScript(driver, "document.getElementById('CorpEntityLoanDocsTB').click();");
		executeJavaScript(driver, "document.getElementById('CBRCorpEntityLoanDocsTB').click();");
		sleep();
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		return PageFactory.getDocumentsPage(driver, ipClient);
	}

	/**
	 * input Legal Name For Loan
	 */
	public void inputLegalNameForLoan(String loanName) {
		waitForControl(driver, Interfaces.LoansPage.LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.LEGAL_NAME_TEXTBOX, loanName);
	}

	/**
	 * select Loan Status
	 */
	public void selectLoanStatus(String loanStatus) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_APPLICANTION_STATUS_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.LOAN_APPLICANTION_STATUS_COMBOBOX, loanStatus);
	}

	/**
	 * click New Applicant Link On Add Loan
	 */
	public void clickNewApplicantLinkOnAddLoan() {
		waitForControl(driver, Interfaces.LoansPage.NEW_APPLICANT_LINK_ADD_LOAN, timeWait);
		click(driver, Interfaces.LoansPage.NEW_APPLICANT_LINK_ADD_LOAN);
		sleep();
	}

	/**
	 * switch To New Applicant For Loan Frame
	 */
	public WebDriver switchToNewApplicantForLoanFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.LoansPage.NEW_APPLICANT_FOR_LOAN_FRAME, timeWait);
		sleep();
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.LoansPage.NEW_APPLICANT_FOR_LOAN_FRAME)));
		return driver;
	}

	/**
	 * input Applicant Info To Create For Loans
	 */
	public void inputApplicantInfoToCreateForLoans(String accountNumber, String accountName, String addressNumber, String addressDictrict, String city, String state,
			String zipCode) {
		waitForControl(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_NUMBER_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_NUMBER_TEXTBOX, accountNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_NAME_TEXTBOX, accountName);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_ADDRESS_STREET_TEXTBOX, addressNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_ADDRESS_DISTRICT_TEXTBOX, addressDictrict);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_CITY_TEXTBOX, city);
		sleep(1);
		selectItemCombobox(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_STATE_TEXTBOX, state);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_ZIPCODE_TEXTBOX, zipCode);
		sleep(1);
	}

	/**
	 * click Save Create New Applicant Button
	 */
	public void clickSaveCreateNewApplicantButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_CREATE_NEW_APPLICANT_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.SAVE_CREATE_NEW_APPLICANT_BUTTON);
		sleep();
	}

	/**
	 * click New Guarantor Link
	 */
	public void clickNewGuarantorLink() {
		waitForControl(driver, Interfaces.LoansPage.NEW_GUARANTOR_LINK, timeWait);
		click(driver, Interfaces.LoansPage.NEW_GUARANTOR_LINK);
		sleep();
	}

	/**
	 * select Guarantor
	 */
	public void selectGuarantor(String guarantor) {
		waitForControl(driver, Interfaces.LoansPage.GUARANTOR_ENTITY_R1_COMBOBOX, guarantor, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.GUARANTOR_ENTITY_R1_COMBOBOX, guarantor);
		sleep(1);
	}

	/**
	 * select Borrower
	 */
	public void selectBorrower(String borrower) {
		waitForControl(driver, Interfaces.LoansPage.NEW_BORROWER_COMBOBOX, borrower, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.NEW_BORROWER_COMBOBOX, borrower);
		sleep(1);
	}

	/**
	 * switch To New Guarantor Entity Frame
	 */
	public WebDriver switchToNewGuarantorEntityFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_FRAME, timeWait);
		sleep();
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_FRAME)));
		return driver;
	}

	/**
	 * input Guarantor Info To Create
	 */
	public void inputGuarantorInfoToCreate(String accountNumber, String accountName, String addressNumber, String addressDictrict, String city, String state, String zipCode) {
		waitForControl(driver, Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_ACCOUNT_NUMBER_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_ACCOUNT_NUMBER_TEXTBOX, accountNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_ACCOUNT_NAME_TEXTBOX, accountName);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_ACCOUNT_ADDRESS_STREET_TEXTBOX, addressNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_ACCOUNT_ADDRESS_DISTRICT_TEXTBOX, addressDictrict);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_ACCOUNT_CITY_TEXTBOX, city);
		sleep(1);
		selectItemCombobox(driver, Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_ACCOUNT_STATE_TEXTBOX, state);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_GUARANTOR_ENTITY_ACCOUNT_ZIPCODE_TEXTBOX, zipCode);
		sleep(1);
	}

	/**
	 * click Save New Guarantor Button
	 */
	public void clickSaveNewGuarantorButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_NEW_GUARANTOR_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.SAVE_NEW_GUARANTOR_BUTTON);
		sleep();
	}

	/**
	 * check New Guarantor Saved
	 */
	public boolean isNewGuarantorSaved(String guarantorName) {
		waitForControl(driver, Interfaces.LoansPage.GUARANTOR_ENTITY_R1_COMBOBOX, timeWait);
		sleep();
		String username = getItemSelectedCombobox(driver, Interfaces.LoansPage.GUARANTOR_ENTITY_R1_COMBOBOX);
		String fullname = getAttributeValue(driver, Interfaces.LoansPage.APPLICANT_FULLNAME_TEXTBOX, "value");
		return username.contains(guarantorName) && fullname.contains(guarantorName);
	}

	/**
	 * click New Borrower Link
	 */
	public void clickNewBorrowerLink() {
		waitForControl(driver, Interfaces.LoansPage.NEW_BORROWER_LINK, timeWait);
		click(driver, Interfaces.LoansPage.NEW_BORROWER_LINK);
		sleep();
	}

	/**
	 * switch To New Borrower Entity Frame
	 */
	public WebDriver switchToNewBorrowerEntityFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.LoansPage.NEW_BORROWER_ENTITY_FRAME, timeWait);
		sleep();
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.LoansPage.NEW_BORROWER_ENTITY_FRAME)));
		return driver;
	}

	/**
	 * input Borrower Info To Create
	 */
	public void inputBorrowerInfoToCreate(String accountNumber, String accountName, String addressNumber, String addressDictrict, String city, String state, String zipCode) {
		waitForControl(driver, Interfaces.LoansPage.NEW_BORROWER_ENTITY_ACCOUNT_NUMBER_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.NEW_BORROWER_ENTITY_ACCOUNT_NUMBER_TEXTBOX, accountNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_BORROWER_ENTITY_ACCOUNT_NAME_TEXTBOX, accountName);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_BORROWER_ENTITY_ACCOUNT_ADDRESS_STREET_TEXTBOX, addressNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_BORROWER_ENTITY_ACCOUNT_ADDRESS_DISTRICT_TEXTBOX, addressDictrict);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_BORROWER_ENTITY_ACCOUNT_CITY_TEXTBOX, city);
		sleep(1);
		selectItemCombobox(driver, Interfaces.LoansPage.NEW_BORROWER_ENTITY_ACCOUNT_STATE_TEXTBOX, state);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_BORROWER_ENTITY_ACCOUNT_ZIPCODE_TEXTBOX, zipCode);
		sleep(1);
	}

	/**
	 * click Save New Borrower Button
	 */
	public void clickSaveNewBorrowerButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_NEW_BORROWER_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.SAVE_NEW_BORROWER_BUTTON);
		sleep();
	}

	/**
	 * check New Borrower Saved
	 */
	public boolean isNewBorrowerSaved(String borrowerName) {
		waitForControl(driver, Interfaces.LoansPage.BORROWER_ENTITY_COMBOBOX, timeWait);
		sleep();
		String username = getItemSelectedCombobox(driver, Interfaces.LoansPage.BORROWER_ENTITY_COMBOBOX);
		String fullname = getAttributeValue(driver, Interfaces.LoansPage.APPLICANT_FULLNAME_TEXTBOX, "value");
		return username.contains(borrowerName) && fullname.contains(borrowerName);
	}

	/**
	 * click New Sponsor Link
	 */
	public void clickNewSponsorLink() {
		waitForControl(driver, Interfaces.LoansPage.NEW_SPONSOR_LINK, timeWait);
		click(driver, Interfaces.LoansPage.NEW_SPONSOR_LINK);
		sleep();
	}

	/**
	 * switch To New Sponsor Entity Frame
	 */
	public WebDriver switchToNewSponsorEntityFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.LoansPage.NEW_SPONSOR_ENTITY_FRAME, timeWait);
		sleep();
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.LoansPage.NEW_SPONSOR_ENTITY_FRAME)));
		return driver;
	}

	/**
	 * input Sponsor Info To Create
	 */
	public void inputSponsorInfoToCreate(String accountNumber, String accountName, String addressNumber, String addressDictrict, String city, String state, String zipCode) {
		waitForControl(driver, Interfaces.LoansPage.NEW_SPONSOR_ENTITY_ACCOUNT_NUMBER_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.NEW_SPONSOR_ENTITY_ACCOUNT_NUMBER_TEXTBOX, accountNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_SPONSOR_ENTITY_ACCOUNT_NAME_TEXTBOX, accountName);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_SPONSOR_ENTITY_ACCOUNT_ADDRESS_STREET_TEXTBOX, addressNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_SPONSOR_ENTITY_ACCOUNT_ADDRESS_DISTRICT_TEXTBOX, addressDictrict);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_SPONSOR_ENTITY_ACCOUNT_CITY_TEXTBOX, city);
		sleep(1);
		selectItemCombobox(driver, Interfaces.LoansPage.NEW_SPONSOR_ENTITY_ACCOUNT_STATE_TEXTBOX, state);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_SPONSOR_ENTITY_ACCOUNT_ZIPCODE_TEXTBOX, zipCode);
		sleep(1);
	}

	/**
	 * click Save New Sponsor Button
	 */
	public void clickSaveNewSponsorButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_NEW_SPONSOR_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.SAVE_NEW_SPONSOR_BUTTON);
		sleep();
	}

	/**
	 * check New Sponsor Saved
	 */
	public boolean isNewSponsorSaved(String sponsorName) {
		waitForControl(driver, Interfaces.LoansPage.SPONSOR_COMBOBOX, timeWait);
		sleep();
		String username = getItemSelectedCombobox(driver, Interfaces.LoansPage.SPONSOR_COMBOBOX);
		String fullname = getAttributeValue(driver, Interfaces.LoansPage.APPLICANT_FULLNAME_TEXTBOX, "value");
		return username.contains(sponsorName) && fullname.contains(sponsorName);
	}

	/**
	 * click New Pledgor Link
	 */
	public void clickNewPledgorLink() {
		waitForControl(driver, Interfaces.LoansPage.NEW_PLEDGOR_LINK, timeWait);
		click(driver, Interfaces.LoansPage.NEW_PLEDGOR_LINK);
		sleep();
	}

	/**
	 * switch To New Pledgor Entity Frame
	 */
	public WebDriver switchToNewPledgorEntityFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_FRAME, timeWait);
		sleep();
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_FRAME)));
		return driver;
	}

	/**
	 * input Pledgor Info To Create
	 */
	public void inputPledgorInfoToCreate(String accountNumber, String accountName, String addressNumber, String addressDictrict, String city, String state, String zipCode) {
		waitForControl(driver, Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_ACCOUNT_NUMBER_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_ACCOUNT_NUMBER_TEXTBOX, accountNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_ACCOUNT_NAME_TEXTBOX, accountName);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_ACCOUNT_ADDRESS_STREET_TEXTBOX, addressNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_ACCOUNT_ADDRESS_DISTRICT_TEXTBOX, addressDictrict);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_ACCOUNT_CITY_TEXTBOX, city);
		sleep(1);
		selectItemCombobox(driver, Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_ACCOUNT_STATE_TEXTBOX, state);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_PLEDGOR_ENTITY_ACCOUNT_ZIPCODE_TEXTBOX, zipCode);
		sleep(1);
	}

	/**
	 * click Save New Pledgor Button
	 */
	public void clickSaveNewPledgorButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_NEW_PLEDGOR_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.SAVE_NEW_PLEDGOR_BUTTON);
		sleep();
	}

	/**
	 * check New Pledgor Saved
	 */
	public boolean isNewPledgorSaved(String pledgorName) {
		waitForControl(driver, Interfaces.LoansPage.PLEDGOR_COMBOBOX, timeWait);
		sleep();
		String username = getItemSelectedCombobox(driver, Interfaces.LoansPage.PLEDGOR_COMBOBOX);
		String fullname = getAttributeValue(driver, Interfaces.LoansPage.APPLICANT_FULLNAME_TEXTBOX, "value");
		return username.contains(pledgorName) && fullname.contains(pledgorName);
	}

	/**
	 * input New Login Info
	 */
	public void inputNewLoginInfo(String firstname, String lastname, String email) {
		waitForControl(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_FIRST_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_FIRST_NAME_TEXTBOX, lastname);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_LAST_NAME_TEXTBOX, firstname);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_EMAIL_TEXTBOX, email);
		sleep(1);
	}

	/**
	 * open Application Entity Detail
	 */
	public void openApplicationEntityDetail(String entityName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_APPLICANT_NAME_ON_TABLE, entityName, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_APPLICANT_NAME_ON_TABLE, entityName);
	}

	/**
	 * check Loans Page Display
	 */
	public boolean isLoansPageDisplay() {
		waitForControl(driver, Interfaces.LoansPage.LIST_OF_LOANS_TITLE, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.LIST_OF_LOANS_TITLE);
	}

	/**
	 * check No Properties Message Display
	 */
	public boolean isNoPropertiesMessageDisplay() {
		waitForControl(driver, Interfaces.LoansPage.NO_PROPERTY_MESSAGE, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.NO_PROPERTY_MESSAGE);
	}

	/**
	 * open Property Documents Tab
	 */
	public void openPropertyDocumentsTab() {
		waitForControl(driver, Interfaces.LoansPage.PROPERTY_DOCUMENTS_TAB, timeWait);
		click(driver, Interfaces.LoansPage.PROPERTY_DOCUMENTS_TAB);
		sleep();
	}

	/**
	 * check No Documents Message Display
	 */
	public boolean isNoDocumentsMessageDisplay() {
		waitForControl(driver, Interfaces.LoansPage.NO_DOCUMENT_MESSAGE, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.NO_DOCUMENT_MESSAGE);
	}

	/**
	 * check No Reporting Schedules Display
	 */
	public boolean isNoReportingSchedulesDisplay() {
		waitForControl(driver, Interfaces.LoansPage.NO_REPORTING_SCHEDULE_MESSAGE, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.NO_REPORTING_SCHEDULE_MESSAGE);
	}

	/**
	 * check Map Page Display
	 */
	public boolean isMapPageDisplay() {
		waitForControl(driver, Interfaces.LoansPage.MAP_CONTROL, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.MAP_CONTROL);
	}

	/**
	 * check No Data Tapes Display
	 */
	public boolean isNoDataTapesDisplay() {
		waitForControl(driver, Interfaces.LoansPage.NO_DATA_TAPES_MESSAGE, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.NO_DATA_TAPES_MESSAGE);
	}

	/**
	 * open Map Tab
	 */
	public void openMapTab() {
		waitForControl(driver, Interfaces.LoansPage.MAP_TAB, timeWait);
		click(driver, Interfaces.LoansPage.MAP_TAB);
		sleep();
	}

	/**
	 * open Load Data Tapes Tab
	 */
	public DocumentsPage openLoadDataTapesTab() {
		waitForControl(driver, Interfaces.LoansPage.LOAD_DATA_TAPES_TAB, timeWait);
		click(driver, Interfaces.LoansPage.LOAD_DATA_TAPES_TAB);
		sleep();
		return PageFactory.getDocumentsPage(driver, ipClient);
	}

	/**
	 * go to Uploader Page
	 */
	public UploaderLoginPage gotoUploaderPage(String uploaderPageUrl) {
		driver.get(uploaderPageUrl);
		sleep(3);
		return PageFactory.getUploaderLoginPage(driver, ipClient);
	}

	/**
	 * open Document Detail For Type
	 */
	public void openDocumentDetailForType(String docType) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_ID_LINK, docType, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_ID_LINK, docType);
		sleep();
	}

	/**
	 * check Document Uploaded For Document Type
	 */
	public boolean isDocumentUploadedForDocumentType(String docFileName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_UPLOADED_DOCUMENT_NAME, docFileName, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_UPLOADED_DOCUMENT_NAME, docFileName);
	}

	/**
	 * check Document Display Attached Document table
	 */
	public boolean isDocumentDisplayAttachedDocument(String docFileName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_NAME_ON_ATTACHED_DOCUMENT, docFileName, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_NAME_ON_ATTACHED_DOCUMENT, docFileName);
	}

	/**
	 * click On Document List Button
	 */
	public void clickOnDocumentListButton() {
		waitForControl(driver, Interfaces.LoansPage.DOCUMENT_LIST_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('GotoLAList').click();");
		sleep(1);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * create New Task
	 */
	public void createNewTask(String taskTemplate, String taskName) {
		clickNewTaskButton();
		waitForControl(driver, Interfaces.LoansPage.TASK_TEMPLATE_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.TASK_TEMPLATE_COMBOBOX, taskTemplate);
		type(driver, Interfaces.LoansPage.TASK_NAME_TEXTBOX, taskName);
		checkTheCheckbox(driver, Interfaces.LoansPage.ALL_PROPERTIES_CHECKBOX);
		clickSaveButton();
	}

	/**
	 * click New Task Button
	 */
	public void clickNewTaskButton() {
		waitForControl(driver, Interfaces.LoansPage.NEW_TASK_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.NEW_TASK_BUTTON);
		sleep();
	}

	/**
	 * click Task List Button
	 */
	public void clickTaskListButton() {
		waitForControl(driver, Interfaces.LoansPage.TASK_LIST_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.TASK_LIST_BUTTON);
		sleep();
	}

	/**
	 * click Save Button
	 */
	public void clickSaveButton() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		executeJavaScript(driver, "document.getElementById('Save2').click();");
		sleep(2);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(15);
		}
	}

	/**
	 * search by Task Name
	 */
	public void searchbyTaskName(String taskName) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_TASKNAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_TASKNAME_TEXTBOX, taskName);
		clickSearchTaskButton();
	}

	/**
	 * click Search Task Button
	 */
	public void clickSearchTaskButton() {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_TASK_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.SEARCH_TASK_BUTTON);
		sleep();
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(7);
		}
	}

	/**
	 * check Task Display
	 */
	public boolean isTaskDisplay(String taskName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_TASK_ITEM, taskName, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_TASK_ITEM, taskName);
	}

	/**
	 * open Task Tab
	 */
	public void openTaskTab() {
		waitForControl(driver, Interfaces.LoansPage.TASK_TAB, timeWait);
		executeJavaScript(driver, "document.getElementById('TaskSearchTB').click();");
		sleep();
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * open Task Detail
	 */
	public void openTaskDetail(String taskName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_TASK_ITEM, taskName, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_TASK_ITEM, taskName);
		sleep();
	}

	/**
	 * open Task Property Detail
	 */
	public void openTaskPropertyDetail(String propertyName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_TASK_PROPERTY_ITEM, propertyName, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_TASK_PROPERTY_ITEM, propertyName);
		sleep();
	}

	/**
	 * add Comment For Task Level1 Value
	 */
	public void addCommentForTaskLevel1Value(String comment) {
		waitForControl(driver, Interfaces.LoansPage.TASK_LEVEL1_COMMENT_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.TASK_LEVEL1_COMMENT_TEXTBOX, comment);
		sleep(1);
	}

	/**
	 * add Comment For Task Level2 Value
	 */
	public void addCommentForTaskLevel2Value(String comment) {
		waitForControl(driver, Interfaces.LoansPage.TASK_LEVEL2_COMMENT_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.TASK_LEVEL2_COMMENT_TEXTBOX, comment);
		sleep(1);
	}

	/**
	 * add Comment For Task Level3 Value
	 */
	public void addCommentForTaskLevel3Value(String comment) {
		waitForControl(driver, Interfaces.LoansPage.TASK_LEVEL3_COMMENT_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.TASK_LEVEL3_COMMENT_TEXTBOX, comment);
		sleep(1);
	}

	/**
	 * click On Back To Task Button
	 */
	public void clickOnBackToTaskButton() {
		waitForControl(driver, Interfaces.LoansPage.BACK_TO_TASK_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.BACK_TO_TASK_BUTTON);
		sleep();
	}

	/**
	 * select Task Document For Using
	 */
	public void selectTaskDocumentForUsing(String docType) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_USE_TASK_DOCUMENT_CHECKBOX, docType, timeWait);
		checkTheCheckbox(driver, Interfaces.LoansPage.DYNAMIC_USE_TASK_DOCUMENT_CHECKBOX, docType);
		sleep(1);
	}

	/**
	 * check Comment Display For Level1
	 */
	public boolean isCommentDisplayForLevel1(String taskName, String comment) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_TASK_COMMENT_LEVEL1, taskName, comment, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_TASK_COMMENT_LEVEL1, taskName, comment);
	}

	/**
	 * check Comment Display For Level2
	 */
	public boolean isCommentDisplayForLevel2(String taskName, String comment) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_TASK_COMMENT_LEVEL2, taskName, comment, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_TASK_COMMENT_LEVEL2, taskName, comment);
	}

	/**
	 * check Comment Display For Level3
	 */
	public boolean isCommentDisplayForLevel3(String taskName, String comment) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_TASK_COMMENT_LEVEL3, taskName, comment, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_TASK_COMMENT_LEVEL3, taskName, comment);
	}

	/**
	 * check Document Type Display On Task Property Table
	 */
	public boolean isDocumentTypeDisplayOnTaskPropertyTable(String taskName, String docType) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_TASK_USED_DOCUMENT_TYPE, taskName, docType, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_TASK_USED_DOCUMENT_TYPE, taskName, docType);
	}

	/**
	 * click Appliant Link On Loan Basic Detail tab
	 */
	public void clickAppliantLinkOnBasicDetail(String applicantName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_APPLICANT_LINK_ON_BASIC_DETAIL, applicantName, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_APPLICANT_LINK_ON_BASIC_DETAIL, applicantName);
		sleep();
	}

	/**
	 * check Applicant Detail Info Correctly
	 */
	public boolean isApplicantDetailInfoCorrectly(String applicantNumber, String applicantName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_ACCOUNT_NUMBER_APPLICANT_DETAIL, applicantNumber, timeWait);
		boolean isAccNum = isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_ACCOUNT_NUMBER_APPLICANT_DETAIL, applicantNumber);
		boolean isAccName = isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_ACCOUNT_NAME_APPLICANT_DETAIL, applicantName);
		return isAccNum && isAccName;
	}

	/**
	 * click On Excel Icon
	 */
	public void clickOnExcelIcon() {
		waitForControl(driver, Interfaces.LoansPage.EXCEL_ICON, timeWait);
		click(driver, Interfaces.LoansPage.EXCEL_ICON);
		sleep();
	}

	/**
	 * input Applicant Info To Create For Loans
	 */
	public void inputApplicantInfoToCreateForLoans(String accountNumber, String accountName, String addressNumber, String addressDictrict, String city, String state,
			String zipCode, String loginName) {
		waitForControl(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_NUMBER_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_NUMBER_TEXTBOX, accountNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_NAME_TEXTBOX, accountName);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_ADDRESS_STREET_TEXTBOX, addressNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_ADDRESS_DISTRICT_TEXTBOX, addressDictrict);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_CITY_TEXTBOX, city);
		sleep(1);
		selectItemCombobox(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_STATE_TEXTBOX, state);
		sleep(1);
		type(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_ZIPCODE_TEXTBOX, zipCode);
		sleep(1);
		selectItemCombobox(driver, Interfaces.LoansPage.NEW_APPLICANT_ACCOUNT_LOGIN_TEXTBOX, loginName);
		sleep(1);
	}

	/**
	 * check Legal name display
	 */
	public boolean isLegalNameDisplay(String legalName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_LEGAL_NAME, legalName, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_LEGAL_NAME, legalName);
	}

	/**
	 * Select Legal Name
	 */
	public void selectLegalName(String legalName) {
		int i = 0;
		while (i < 100) {
			if (isControlDisplayed(driver, Interfaces.LoansPage.LAST_LINK)) {
				waitForControl(driver, Interfaces.LoansPage.LAST_LINK, timeWait);
				click(driver, Interfaces.LoansPage.LAST_LINK);
				sleep();
				if (isControlDisplayed(driver, Interfaces.LoansPage.PREVIOUS_LINK)) {
					waitForControl(driver, Interfaces.LoansPage.DYNAMIC_LEGAL_NAME, legalName, timeWait);
					click(driver, Interfaces.LoansPage.DYNAMIC_LEGAL_NAME, legalName);
					sleep();
					i = 100;
				}
				i = i + 1;
			} else {
				waitForControl(driver, Interfaces.LoansPage.DYNAMIC_LEGAL_NAME, legalName, timeWait);
				click(driver, Interfaces.LoansPage.DYNAMIC_LEGAL_NAME, legalName);
				sleep();
				i = 100;
			}

		}
	}

	/**
	 * check Applicant name display
	 */
	public boolean isApplicantNameNameDisplay(String legalName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_APPLICANT_NAME, legalName, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_APPLICANT_NAME, legalName);
	}

	/**
	 * click on Address link
	 */
	public void clickPropertiesAddressLink(String addressName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_PROPERTIES_ADDRESS_LINK, addressName, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_PROPERTIES_ADDRESS_LINK, addressName);
		sleep();
	}

	/**
	 * click on New Document link
	 */
	public void clickNewDocument() {
		waitForControl(driver, Interfaces.LoansPage.NEW_DOCUMENTS_LINK, timeWait);
		click(driver, Interfaces.LoansPage.NEW_DOCUMENTS_LINK);
		sleep();
	}

	/**
	 * add new a Properties Document
	 */
	public void addNewPropertiesDocument(String documentType, String documentFile, String isprivate) {
		waitForControl(driver, Interfaces.LoansPage.PROPERTIES_DOCUMENTS_TYPE_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.PROPERTIES_DOCUMENTS_TYPE_COMBOBOX, documentType);
		sleep(1);
		uploadFileDocument(documentFile);
		sleep();
		if (isprivate == "yes") {
			click(driver, Interfaces.LoansPage.PROPERTIES_DOCUMENTS_PRIVATE_CHECKBOX);
		}
		sleep();
		click(driver, Interfaces.LoansPage.SAVE_BUTTON);
		sleep();
	}

	/**
	 * check contents of Loan Applicantion Details are read-only
	 */
	public boolean isReadOnlyLoanApplicantionDetails() {

		boolean readonly = false;
		boolean bool1 = isControlDisplayed(driver, Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL);
		boolean bool2 = isControlDisplayed(driver, Interfaces.LoansPage.LOAN_STATUS_COMBOBOX_BASIC_DETAIL);

		if ((bool1 == false) && (bool2 == false)) {
			readonly = true;
		}
		return readonly;
	}

	/**
	 * open Loan Application Entities link
	 */
	public void openLoansApplicationEntities(String entityName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_LOAN_APPLICATION_ENTITY_LINK, entityName, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_LOAN_APPLICATION_ENTITY_LINK, entityName);
		sleep();
	}

	/**
	 * check Applicant should not be able to add, delete, edit entities
	 */
	public boolean isReadOnlyApplicant() {

		boolean readonly = false;
		boolean bool = isControlDisplayed(driver, Interfaces.LoansPage.NEW_APPLICANT_LINK);
		if (bool == false) {
			readonly = true;
		}
		return readonly;
	}

	/**
	 * check properties display on List of Properties of Borrower
	 */
	public boolean isPropertyDisplayBorrower(String address, String city, String state) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_PROPERTY_ITEM_BORROWER, address, city, state, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_PROPERTY_ITEM_BORROWER, address, city, state);
	}

	/**
	 * check Document Display Attached Document table
	 */
	public boolean isDocumentNotDisplayInPropertiesTab(String docFileName) {
		boolean display = false;
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_NAME_IN_PROPERTIES_TAB, docFileName, timeWait);
		boolean bool = isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_NAME_IN_PROPERTIES_TAB, docFileName);
		if (bool == false) {
			display = true;
		}
		return display;
	}

	/**
	 * click a Document In Properties Tab
	 */
	public void clickDocumentInPropertiesTab(String docFileName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_NAME_IN_PROPERTIES_TAB, docFileName, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_NAME_IN_PROPERTIES_TAB, docFileName);
	}

	/**
	 * upload File Properties Document
	 */
	public void uploadFileDocument(String documentFileName) {
		sleep(4);
		if (driver.toString().toLowerCase().contains("chrome")) {
			doubleClick(driver, Interfaces.LoansPage.CHOOSE_FILE_DOCUMENT_BUTTON);
		} else if (driver.toString().toLowerCase().contains("firefox")) {
			doubleClick(driver, Interfaces.LoansPage.BROWSE_DOCUMENT_BUTTON);
		} else if (driver.toString().toLowerCase().contains("ie")) {
			doubleClick(driver, Interfaces.LoansPage.BROWSE_DOCUMENT_BUTTON);
		}
		sleep(1);
		Common.getCommon().openFileForUpload(driver, documentFileName);
		sleep();
	}

	/**
	 * Click Go To Property button
	 */
	public void clickGoToPropertyButton() {
		waitForControl(driver, Interfaces.LoansPage.GO_TO_PROPERTY_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('GotoLAProp').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * check Document Display Attached Document table
	 */
	public boolean isDocumentFileUploaded(String docType, String docFileName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_NAME_IN_DOCUMENT_TABLE, docType, docFileName, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_NAME_IN_DOCUMENT_TABLE, docType, docFileName);
	}

	/**
	 * Click on Private checkbox
	 */
	public void clickPrivateCheckbox() {
		waitForControl(driver, Interfaces.LoansPage.PROPERTIES_DOCUMENTS_PRIVATE_CHECKBOX, timeWait);
		click(driver, Interfaces.LoansPage.PROPERTIES_DOCUMENTS_PRIVATE_CHECKBOX);
	}

	/**
	 * input Applicant Info To Create
	 */
	public void inputApplicantInfoToCreate(String accountNumber, String accountName, String addressNumber, String addressDictrict, String city, String state, String zipCode,
			String loginName) {
		waitForControl(driver, Interfaces.LoansPage.ACCOUNT_NUMBER_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.ACCOUNT_NUMBER_TEXTBOX, accountNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_NAME_TEXTBOX, accountName);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_ADDRESS_STREET_TEXTBOX, addressNumber);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_ADDRESS_DISTRICT_TEXTBOX, addressDictrict);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_CITY_TEXTBOX, city);
		sleep(1);
		selectItemCombobox(driver, Interfaces.LoansPage.ACCOUNT_STATE_TEXTBOX, state);
		sleep(1);
		type(driver, Interfaces.LoansPage.ACCOUNT_ZIPCODE_TEXTBOX, zipCode);
		sleep(1);
		selectItemCombobox(driver, Interfaces.LoansPage.CHOOSE_LOGIN_COMBOBOX, loginName);
		sleep(1);
	}

	/**
	 * check Loans Display With Correct Info
	 */
	public boolean isLoanApplicationEntityDisplayWithCorrectInfo(String entityName, String applicantName) {
		boolean bool = false;
		waitForControl(driver, Interfaces.LoansPage.ENTITY_TYPE_COMBOBOX, timeWait);
		boolean bool1 = isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_ENTITY_TYPE_OPTION, entityName);
		boolean bool2 = isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_APPLICANT_OPTION, applicantName);
		if (bool1 == true && bool2 == true) {
			return bool = true;
		}
		return bool;
	}

	/**
	 * check New Applicant Display On LAE Table
	 */
	public boolean isLoanApplicationEntityDisplayOnTable(String applicantName) {
		waitForControl(driver, Interfaces.LoansPage.APPLICANT_ENTITY_ROW, applicantName, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.APPLICANT_ENTITY_ROW, applicantName);
	}

	/**
	 * check Applicant Saved In New Loan Form
	 */
	public boolean isApplicantSavedInNewLoanForm(String applicantName) {
		waitForControl(driver, Interfaces.LoansPage.APPLICANT_COMBOBOX, timeWait);
		String realName = getItemSelectedCombobox(driver, Interfaces.LoansPage.APPLICANT_COMBOBOX);
		return realName.contains(applicantName);
	}

	/**
	 * check number of Item Of Source Combobox On Add New Loan
	 */
	public boolean isItemOfSourceComboboxOnAddNewLoan(int numberItem) {
		waitForControl(driver, Interfaces.LoansPage.SOURCE_COMBOBOX_ADD_NEW_LOAN, timeWait);
		int realNumber = countElement(driver, Interfaces.LoansPage.OPTION_SOURCE_COMBOBOX_ADD_NEW_LOAN) - 1;
		return realNumber == numberItem;
	}

	/**
	 * check Item Exist In Source Combobox On Add New Loan
	 */
	public boolean isItemExistInSourceComboboxOnAddNewLoan(String item) {
		boolean returnValue = false;
		waitForControl(driver, Interfaces.LoansPage.SOURCE_COMBOBOX_ADD_NEW_LOAN, timeWait);
		int realNumber = countElement(driver, Interfaces.LoansPage.OPTION_SOURCE_COMBOBOX_ADD_NEW_LOAN);
		if (realNumber == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver, Interfaces.LoansPage.OPTION_SOURCE_COMBOBOX_ADD_NEW_LOAN);
		for (WebElement element : elements) {
			String text = element.getText();
			if (text.contains(item)) {
				returnValue = true;
				break;
			}
		}
		return returnValue;
	}

	/**
	 * edit Loan Info
	 */
	public void editLoanInfo(String companyType, String email) {
		waitForControl(driver, Interfaces.LoansPage.COMPANY_TYPE_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.COMPANY_TYPE_COMBOBOX, companyType);
		sleep(1);
		type(driver, Interfaces.LoansPage.EMAIL_TEXTBOX_BASIC_DETAIL, email);
		sleep(1);
	}

	/**
	 * edit Loan Info
	 */
	public void editLoanBasicDetailInfo(String companyType) {
		waitForControl(driver, Interfaces.LoansPage.COMPANY_TYPE_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.COMPANY_TYPE_COMBOBOX, companyType);
		sleep(1);
	}

	/**
	 * check Error Message About Invalid Email Display
	 */
	public boolean isErrorMessageAboutInvalidEmailDisplay() {
		waitForControl(driver, Interfaces.LoansPage.INVALID_EMAIL_ERROR_MESSAGE, 5);
		return isControlDisplayed(driver, Interfaces.LoansPage.INVALID_EMAIL_ERROR_MESSAGE);
	}

	/**
	 * check Save Success Message Display
	 */
	public boolean isSaveSuccessMessageDisplay() {
		waitForControl(driver, Interfaces.LoansPage.SAVE_SUCCESS_MESSAGE, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.SAVE_SUCCESS_MESSAGE);
	}

	/**
	 * check Save Funded Status Error Message Display
	 */
	public boolean isSaveFundedStatusErrorMessageDisplay(String messageContent) {
		waitForControl(driver, Interfaces.LoansPage.SAVE_FUNDED_ERROR_STATUS_MESSAGE, messageContent, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.SAVE_FUNDED_ERROR_STATUS_MESSAGE, messageContent);
	}

	/**
	 * check 'Final Borrower LLC, Loan Application Status' Fields Set Read-Only
	 */
	public boolean isCompanyNameAddressSiteCityReadOnly(String loanName, String finalBorrowerLLC, String loanStatus) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL, timeWait);
		String realLoanName = getText(driver, Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL);
		String reaFinalLLC = getText(driver, Interfaces.LoansPage.FINAL_BORROWER_LLC_TEXTBOX);
		String realLoanStatus = getText(driver, Interfaces.LoansPage.LOAN_STATUS_COMBOBOX_BASIC_DETAIL);
		return realLoanName.contains(loanName) && reaFinalLLC.contains(finalBorrowerLLC) && realLoanStatus.contains(loanStatus);
	}

	/**
	 * check number of Item Of Source Combobox On Basic Detail Tab
	 */
	public boolean isItemOfSourceComboboxOnBasicDetailTab(int numberItem) {
		waitForControl(driver, Interfaces.LoansPage.SOURCE_COMBOBOX_BASIC_DETAIL, timeWait);
		int realNumber = countElement(driver, Interfaces.LoansPage.OPTION_SOURCE_COMBOBOX_BASIC_DETAIL) - 1;
		return realNumber == numberItem;
	}

	/**
	 * check Item Exist In Source Combobox On Basic Detail Tab
	 */
	public boolean isItemExistInSourceComboboxOnBasicDetailTab(String item) {
		boolean returnValue = false;
		waitForControl(driver, Interfaces.LoansPage.SOURCE_COMBOBOX_BASIC_DETAIL, timeWait);
		int realNumber = countElement(driver, Interfaces.LoansPage.OPTION_SOURCE_COMBOBOX_BASIC_DETAIL);
		if (realNumber == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver, Interfaces.LoansPage.OPTION_SOURCE_COMBOBOX_BASIC_DETAIL);
		for (WebElement element : elements) {
			String text = element.getText();
			if (text.contains(item)) {
				returnValue = true;
				break;
			}
		}
		return returnValue;
	}

	/**
	 * check Loans Display On Search Loan Page
	 */
	public boolean isLoansDisplayOnSearchLoanPage(String loanName) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_LOAN_ITEM_SEARCH_LOAN_PAGE, loanName, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_LOAN_ITEM_SEARCH_LOAN_PAGE, loanName);
	}

	/**
	 * select Member In Basic Detail Team
	 */
	public void selectMemberInBasicDetailTeam(String member) {
		waitForControl(driver, Interfaces.LoansPage.MEMBER_COMBOBOX_BASIC_DETAIL_TEAM, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.MEMBER_COMBOBOX_BASIC_DETAIL_TEAM, member);
		sleep(1);
	}

	/**
	 * Add Member In Basic Detail Team
	 */
	public void addMemberInBasicDetailTeam(String member) {
		waitForControl(driver, Interfaces.LoansPage.ADD_BUTTON_BASIC_DETAIL_TEAM, timeWait);
		click(driver, Interfaces.LoansPage.ADD_BUTTON_BASIC_DETAIL_TEAM);
		sleep();
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_MEMBER_COMBOBOX_BASIC_DETAIL_TEAM, member, timeWait);
		checkTheCheckbox(driver, Interfaces.LoansPage.DYNAMIC_MEMBER_COMBOBOX_BASIC_DETAIL_TEAM, member);
		sleep();
		waitForControl(driver, Interfaces.LoansPage.MEMBER_OK_BUTTON_BASIC_DETAIL_TEAM, timeWait);
		click(driver, Interfaces.LoansPage.MEMBER_OK_BUTTON_BASIC_DETAIL_TEAM);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(15);
		} else {
			sleep(5);
		}
	}

	/**
	 * check Member Display On Team Table
	 */
	public boolean isMemberDisplayOnTeamTable(String memberName) {
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			waitForControl(driver, Interfaces.LoansPage.MEMBER_COMBOBOX_BASIC_DETAIL_TEAM_IE, timeWait);
			return isControlDisplayed(driver, Interfaces.LoansPage.MEMBER_COMBOBOX_BASIC_DETAIL_TEAM_IE);
		} else {
			waitForControl(driver, Interfaces.LoansPage.MEMBER_COMBOBOX_BASIC_DETAIL_TEAM, memberName, timeWait);
			return isControlDisplayed(driver, Interfaces.LoansPage.MEMBER_COMBOBOX_BASIC_DETAIL_TEAM, memberName);
		}
	}

	/**
	 * check second Member Display On Team Table
	 */
	public boolean isSecondMemberDisplayOnTeamTable(String memberName) {
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			waitForControl(driver, Interfaces.LoansPage.SECOND_MEMBER_COMBOBOX_BASIC_DETAIL_TEAM_IE, timeWait);
			return isControlDisplayed(driver, Interfaces.LoansPage.SECOND_MEMBER_COMBOBOX_BASIC_DETAIL_TEAM_IE);
		} else {
			waitForControl(driver, Interfaces.LoansPage.SECOND_MEMBER_COMBOBOX_BASIC_DETAIL_TEAM, memberName, timeWait);
			return isControlDisplayed(driver, Interfaces.LoansPage.SECOND_MEMBER_COMBOBOX_BASIC_DETAIL_TEAM, memberName);
		}
	}

	/**
	 * click On Missing Document Summary Button
	 */
	public void clickOnMissingDocumentSummaryButton() {
		waitForControl(driver, Interfaces.LoansPage.MISSING_DOCUMENT_SUMMARY_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.MISSING_DOCUMENT_SUMMARY_BUTTON);
		sleep();
	}

	/**
	 * get Number Of Property In Properties Tab
	 */
	public int getNumberOfPropertyInPropertiesTab() {
		waitForControl(driver, Interfaces.LoansPage.TOTAL_PROPERTY_LABEL, timeWait);
		String text = getText(driver, Interfaces.LoansPage.TOTAL_PROPERTY_LABEL);
		String[] subString = text.split(" ");
		int result = Integer.parseInt(subString[subString.length - 1]);
		return result;
	}

	/**
	 * get Number Documents Required Of Property In Properties Tab
	 */
	public int getNumberDocumentsRequiredOfPropertyInPropertiesTab() {
		waitForControl(driver, Interfaces.LoansPage.TOTAL_REQUIRED_DOCS_PROPERTY_LABEL, timeWait);
		String text = getText(driver, Interfaces.LoansPage.TOTAL_REQUIRED_DOCS_PROPERTY_LABEL);
		String[] subString = text.split(" ");
		int result = Integer.parseInt(subString[subString.length - 1]);
		return result;
	}

	/**
	 * check Number Of Property Display Correct
	 */
	public boolean isNumberOfPropertyDisplayCorrect(int input) {
		waitForControl(driver, Interfaces.LoansPage.PROPERTY_BASIC_DETAIL_FIELD, timeWait);
		String text = getText(driver, Interfaces.LoansPage.PROPERTY_BASIC_DETAIL_FIELD);
		int result = Integer.parseInt(text);
		return result == input;
	}

	/**
	 * check Property Info Correctly
	 */
	public boolean isPropertyInfoCorrectly(String adress, String city, String state, String zipCode, String propertyType) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_PROPERTY_INFOR, adress, city, state, zipCode, propertyType, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_PROPERTY_INFOR, adress, city, state, zipCode, propertyType);
	}

	/**
	 * click Export Datatape Button
	 */
	public void clickExportDatatapeButton() {
		waitForControl(driver, Interfaces.LoansPage.EXPORT_DATATAPE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('ExportDataTape').click();");
		sleep();
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * click Export CSV Button
	 */
	public void clickCSVButton() {
		waitForControl(driver, Interfaces.LoansPage.EXPORT_CSV_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('ExportCSV').click();");
		sleep(2);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * click Download Template button
	 */
	public void clickDownloadTemplateButton() {
		waitForControl(driver, Interfaces.LoansPage.DOWNLOAD_TEMPLATE_BUTTON, timeWait);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			WebElement webElement = driver.findElement(By.xpath("//*[@id='DownloadTemplate']"));
			webElement.sendKeys(Keys.ENTER);
			sleep();
			keyPressing("enter");
			sleep(3);
			keyPressing("alt_s");
			keyPressing("alt_s");
			keyPressing("alt_s");
		} else {
			executeJavaScript(driver, "document.getElementById('DownloadTemplate').click();");
			sleep();
			acceptJavascriptAlert(driver);
		}
	}

	/**
	 * click Missing Summary button
	 */
	public void clickMissingSummaryButton() {
		waitForControl(driver, Interfaces.LoansPage.MISSING_DOCUMENT_SUMMARY_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('MissingDocumentsSummary').click();");
		sleep();
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
			keyPressing("alt_s");
			keyPressing("alt_s");
			keyPressing("alt_s");
		}
	}

	/**
	 * click Download Documents button
	 */
	public void clickDownloadDocumentsButton() {
		waitForControl(driver, Interfaces.LoansPage.DOWNLOAD_DOCUMENTS_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('DownloadDocsSearch').click();");
		sleep();
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
	}

	/**
	 * get Number Of Document In Property Documents Tab
	 */
	public int getNumberOfDocumentInPropertyDocumentsTab() {
		waitForControl(driver, Interfaces.LoansPage.TOTAL_DOCUMENT_LABEL, timeWait);
		String text = getText(driver, Interfaces.LoansPage.TOTAL_DOCUMENT_LABEL);
		String[] subString = text.split(" ");
		int result = Integer.parseInt(subString[subString.length - 1]);
		return result;
	}

	/**
	 * check Applicant Exist In Drop down List On Add New Loan
	 */
	public boolean isApplicantExistInDropdownListOnAddNewLoan(String applicantName) {
		boolean returnValue = false;
		waitForControl(driver, Interfaces.LoansPage.APPLICANT_COMBOBOX, timeWait);
		int realNumber = countElement(driver, Interfaces.LoansPage.APPLICANT_COMBOBOX_OPTION);
		if (realNumber == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver, Interfaces.LoansPage.APPLICANT_COMBOBOX_OPTION);
		for (WebElement element : elements) {
			String text = element.getText();
			if (text.contains(applicantName)) {
				returnValue = true;
				break;
			}
		}
		return returnValue;
	}

	/**
	 * click New Team Member Button
	 */
	public void clickNewTeamMemberButton() {
		waitForControl(driver, Interfaces.LoansPage.NEW_TEAM_MEMBER_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.NEW_TEAM_MEMBER_BUTTON);
		sleep();
	}

	/**
	 * select second Member In Basic Detail Team
	 */
	public void selectSecondMemberInBasicDetailTeam(String member) {
		waitForControl(driver, Interfaces.LoansPage.SECOND_MEMBER_COMBOBOX_BASIC_DETAIL_TEAM, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.SECOND_MEMBER_COMBOBOX_BASIC_DETAIL_TEAM, member);
		sleep(1);
	}

	/**
	 * remove Member From Loan Team
	 */
	public void removeMemberFromLoanTeam(int index) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_DELETE_TEAM_MEMBER_CHECKBOX, index + "", timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_DELETE_TEAM_MEMBER_CHECKBOX, index + "");
		sleep(1);
		clickSaveButton();
		sleep();
	}

	/**
	 * delete Second Entity In Table
	 */
	public void deleteEntityInTable(String entity) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_ENTITY_CHECKBOX_IN_TABLE, entity, timeWait);
		checkTheCheckbox(driver, Interfaces.LoansPage.DYNAMIC_ENTITY_CHECKBOX_IN_TABLE, entity);
		sleep(1);
		clickSaveButton();
		sleep();
	}

	/**
	 * check Parents Property Display Correctly
	 */
	public boolean isParentsPropertyDisplayCorrectly(String parentAddress) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_PARENT_PROPERTY_ITEM, parentAddress, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_PARENT_PROPERTY_ITEM, parentAddress);
	}

	/**
	 * select Loan Status Basic Detail
	 */
	public void selectLoanStatusBasicDetail(String loanStatus) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_STATUS_COMBOBOX_BASIC_DETAIL, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.LOAN_STATUS_COMBOBOX_BASIC_DETAIL, loanStatus);
		sleep(1);
	}

	/**
	 * get Document Number Of Property
	 */
	public int getDocumentNumberOfProperty(String property) {
		searchAddress(property);
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_NUMBER_OF_PROPERTY, property, timeWait);
		String text = getText(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_NUMBER_OF_PROPERTY, property);
		String[] subString = text.split(" ");
		int result = Integer.parseInt(subString[0]);
		return result;
	}

	/**
	 * check Applicant Exist In Dropdown List On New Loan Application Entity
	 */
	public boolean isApplicantExistInDropdownListOnNewLoanApplicationEntity(String applicantName) {
		boolean returnValue = false;
		waitForControl(driver, Interfaces.LoansPage.APPLICANT_ENTITY_COMBOBOX, timeWait);
		int realNumber = countElement(driver, Interfaces.LoansPage.APPLICANT_ENTITY_COMBOBOX_OPTION);
		if (realNumber == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver, Interfaces.LoansPage.APPLICANT_ENTITY_COMBOBOX_OPTION);
		for (WebElement element : elements) {
			String text = element.getText();
			if (text.contains(applicantName)) {
				returnValue = true;
				break;
			}
		}
		return returnValue;
	}

	/**
	 * check Applicant Exist In Dropdown List On Loan Properties tab
	 */
	public boolean isApplicantExistInDropdownListOnLoanPropertiesTab(String applicantName) {
		boolean returnValue = false;
		waitForControl(driver, Interfaces.LoansPage.LOAN_PROPERTY_APPLICANT_COMBOBOX, timeWait);
		int realNumber = countElement(driver, Interfaces.LoansPage.LOAN_PROPERTY_APPLICANT_COMBOBOX_OPTION);
		if (realNumber == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver, Interfaces.LoansPage.LOAN_PROPERTY_APPLICANT_COMBOBOX_OPTION);
		for (WebElement element : elements) {
			String text = element.getText();
			if (text.contains(applicantName)) {
				returnValue = true;
				break;
			}
		}
		return returnValue;
	}

	/**
	 * check Property Added To Parent
	 */
	public boolean isPropertyAddedToParent(String address, String parent) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_ADDED_PROPERTY_TO_PARENT, address, parent, 5);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_ADDED_PROPERTY_TO_PARENT, address, parent);
	}

	/**
	 * check Property Document Display
	 */
	public boolean isPropertyDocumentDisplay(String address, String documentType) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_PROPERTY_DOCUMENT_WITH_TYPE, address, documentType, 5);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_PROPERTY_DOCUMENT_WITH_TYPE, address, documentType);
	}

	/**
	 * check Applicant Exist In Login Dropdown List On New Loan Application
	 * Entity
	 */
	public boolean isApplicantExistInLoginDropdownListOnNewLoanApplicationEntity(String applicantName) {
		boolean returnValue = false;
		waitForControl(driver, Interfaces.LoansPage.LOGIN_COMBOBOX, timeWait);
		int realNumber = countElement(driver, Interfaces.LoansPage.LOGIN_COMBOBOX);
		if (realNumber == 0) {
			return false;
		}
		List<WebElement> elements = getElements(driver, Interfaces.LoansPage.LOGIN_COMBOBOX_OPTION);
		for (WebElement element : elements) {
			String text = element.getText();
			if (text.contains(applicantName)) {
				returnValue = true;
				break;
			}
		}
		return returnValue;
	}

	/**
	 * check Document Loaded To Document Column
	 */
	public boolean isDocumentLoadedToDocumentColumn(String address, String documentColumn) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_FOR_DOCUMENTS_COLUMN, address, documentColumn, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_FOR_DOCUMENTS_COLUMN, address, documentColumn);
	}

	/**
	 * check 'Date Created' field not display
	 */
	public boolean isDateCreatedNotDisplay() {
		waitForControl(driver, Interfaces.LoansPage.DATE_CREATED_TEXTBOX, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DATE_CREATED_TEXTBOX);
	}

	/**
	 * input ZipCode
	 */
	public void inputZipCode(String zipCode) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_ZIP_CODE_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.LOAN_ZIP_CODE_TEXTBOX, zipCode);
		sleep(1);
	}

	/**
	 * input Kick off date
	 */
	public void inputKickOffDate(String kickOffDate) {
		waitForControl(driver, Interfaces.LoansPage.KICK_OFF_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.KICK_OFF_DATE_TEXTBOX, kickOffDate);
		sleep();
	}

	/**
	 * input Loan Amount Requested
	 */
	public void inputLoanAmountRequested(String loanAmountRequested) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_AMOUNT_REQUESTED_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.LOAN_AMOUNT_REQUESTED_TEXTBOX, loanAmountRequested);
		sleep();
	}

	/**
	 * input Loan Status Change Date (CAF)
	 */
	public void inputLoanStatusChangeDate(String loanStatusChangeDate) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_STATUS_CHANGE_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.LOAN_STATUS_CHANGE_DATE_TEXTBOX, loanStatusChangeDate);
		sleep();
	}

	/**
	 * input Term Sheet Issued Date (CAF)
	 */
	public void inputTermSheetIssuedDate(String termSheetIssuedDate) {
		waitForControl(driver, Interfaces.LoansPage.TERM_SHEET_ISSUED_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.TERM_SHEET_ISSUED_DATE_TEXTBOX, termSheetIssuedDate);
		sleep();
	}

	/**
	 * input Anticipated IC Approval Date (CAF)
	 */
	public void inputAnticipatedICApprovalDate(String value) {
		waitForControl(driver, Interfaces.LoansPage.ANTICIPATED_IC_APPROVAL_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.ANTICIPATED_IC_APPROVAL_DATE_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input Term Sheet Signed Date (CAF)
	 */
	public void inputTermSheetSignedDate(String termSheetSignedDate) {
		waitForControl(driver, Interfaces.LoansPage.TERM_SHEET_SIGNED_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.TERM_SHEET_SIGNED_DATE_TEXTBOX, termSheetSignedDate);
		sleep();
	}

	/**
	 * check Record Saved Message Displays
	 */
	public boolean isRecordSavedMessageDisplays() {
		waitForControl(driver, Interfaces.LoansPage.RECORD_SAVED_MESSAGE, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.RECORD_SAVED_MESSAGE);
	}

	/**
	 * check 'Zip code' Saved
	 */
	public boolean isZipCodeSaved(String zipCode) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_ZIP_CODE_TEXTBOX, timeWait);
		String realZipCode = getAttributeValue(driver, Interfaces.LoansPage.LOAN_ZIP_CODE_TEXTBOX, "value");
		return realZipCode.contains(zipCode);
	}

	/**
	 * click 'Create Audit Copy' Button
	 */
	public void clickCreateAuditCopyButton() {
		waitForControl(driver, Interfaces.LoansPage.CREATE_AUDIT_COPY_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('CreateAuditCopy').click();");
		sleep(1);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * select your Profile audit
	 */
	public void selectProfileAudit(String auditProfile) {
		waitForControl(driver, Interfaces.LoansPage.SELECT_PROFILE_COMBOBOX, auditProfile, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.SELECT_PROFILE_COMBOBOX, auditProfile);
		sleep();
	}

	/**
	 * check 'Copy Empty PlaceHolders' checkbox
	 */
	public void checkCopyEmptyPlaceholderCheckbox() {
		waitForControl(driver, Interfaces.LoansPage.COPY_EMPTY_PLACEHOLDER_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.LoansPage.COPY_EMPTY_PLACEHOLDER_CHECKBOX);
		sleep(1);
	}

	/**
	 * Uncheck 'Copy Empty PlaceHolders' checkbox
	 */
	public void unCheckCopyEmptyPlaceholderCheckbox() {
		waitForControl(driver, Interfaces.LoansPage.COPY_EMPTY_PLACEHOLDER_CHECKBOX, timeWait);
		uncheckTheCheckbox(driver, Interfaces.LoansPage.COPY_EMPTY_PLACEHOLDER_CHECKBOX);
		sleep(1);
	}

	/**
	 * click 'OK' Button
	 */
	public void clickOkButton() {
		waitForControl(driver, Interfaces.LoansPage.OK_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.OK_BUTTON);
		sleep(1);
	}

	/**
	 * check Audit Profile duplicating message displays
	 * 
	 * @return
	 */
	public boolean isAuditDuplicatingMessageDisplays() {
		waitForControl(driver, Interfaces.LoansPage.AUDIT_DUPLICATING_DISPLAY_MESSAGE, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.AUDIT_DUPLICATING_DISPLAY_MESSAGE);
	}

	/**
	 * check Loan Applicant Status default displays
	 * 
	 * @return
	 */
	public boolean isLoanApplicantStatusDefaultDisplays(String loanStatus) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_LOAN_APPLICANT_STATUS_DEFAULT_DISPLAY, loanStatus, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_LOAN_APPLICANT_STATUS_DEFAULT_DISPLAY, loanStatus);
	}

	/**
	 * check Document Loaded To Document Type
	 */
	public boolean isDocumentLoadedToDocumentType(String docType) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DOCUMENTS_DETAIL, docType, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DOCUMENTS_DETAIL, docType);
	}

	/**
	 * click By Document Type radio button
	 */
	public void clickDocumentTypeRadioButton() {
		waitForControl(driver, Interfaces.LoansPage.BY_DOCUMENTS_TYPE_RADIO_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.BY_DOCUMENTS_TYPE_RADIO_BUTTON);
		sleep();
	}

	/**
	 * click By Address radio button
	 */
	public void clickAddressRadioButton() {
		waitForControl(driver, Interfaces.LoansPage.BY_ADDRESS_RADIO_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.BY_ADDRESS_RADIO_BUTTON);
		sleep();
	}

	/**
	 * click Download button
	 */
	public void clickDownloadButton() {
		waitForControl(driver, Interfaces.LoansPage.DOWNLOAD_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.DOWNLOAD_BUTTON);
		sleep();
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
			keyPressing("alt_s");
			keyPressing("alt_s");
			keyPressing("alt_s");
		}
	}

	/**
	 * click Cancel button
	 */
	public void clickCancelButton() {
		waitForControl(driver, Interfaces.LoansPage.CANCEL_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('cncl').click();");
		sleep();
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * check success message is displayed
	 */
	public boolean isSuccessMessageDisplayed(String messageContent) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_SUCCESS_MESSAGE_DISPLAY, messageContent, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_SUCCESS_MESSAGE_DISPLAY, messageContent);
	}

	/**
	 * click Download All Documents button
	 */
	public void clickDownloadAllDocumentsButton() {
		waitForControl(driver, Interfaces.LoansPage.DOWNLOAD_ALL_DOCUMENTS_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('DownloadAll').click();");
		sleep();
		waitForControl(driver, Interfaces.LoansPage.DOWNLOAD_ALL_DOCUMENTS_OK_BUTTON, timeWait);
		click(driver, Interfaces.LoansPage.DOWNLOAD_ALL_DOCUMENTS_OK_BUTTON);
		sleep(60);
	}

	/**
	 * Click Export Data Tape radio button
	 */
	public void clickExportAllPropertiesRadioButton(String value) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_EXPORT_DATA_TAPE_RADIO_BUTTON, value, timeWait);
		doubleClick(driver, Interfaces.LoansPage.DYNAMIC_EXPORT_DATA_TAPE_RADIO_BUTTON, value);
		sleep();
		doubleClick(driver, Interfaces.LoansPage.OK_EXPORT_DATATAPE_BUTTON);
		sleep();
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
			keyPressing("alt_s");
			keyPressing("alt_s");
			keyPressing("alt_s");
		}
	}

	/**
	 * Check Property/Loan checkbox in List Properties/Loan table
	 */
	public void isSelectedPropertyLoanCheckbox(String address) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_PROPERTY_CHECKBOX_IN_LIST_PROPERTIES_TABLE, address, timeWait);
		checkTheCheckbox(driver, Interfaces.LoansPage.DYNAMIC_PROPERTY_CHECKBOX_IN_LIST_PROPERTIES_TABLE, address);
		sleep();
	}

	/**
	 * click Make Inactive Button At Property Tab
	 */
	public void clickMakeInactiveButtonAtPropertyTab() {
		waitForControl(driver, Interfaces.LoansPage.PROPERTY_MAKE_INACTIVE_BUTTON, timeWait);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			WebElement webElement = driver.findElement(By.xpath("//*[@id='SearchMakeInactive']"));
			webElement.sendKeys(Keys.ENTER);
			sleep();
			keyPressing("enter");
			sleep(3);
		} else {
			executeJavaScript(driver, "document.getElementById('SearchMakeInactive').click();");
			sleep();
			acceptJavascriptAlert(driver);
		}
	}

	/**
	 * click Make Active Button At Property Tab
	 */
	public void clickMakeActiveButtonAtPropertyTab() {
		waitForControl(driver, Interfaces.LoansPage.PROPERTY_MAKE_ACTIVE_BUTTON, timeWait);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			WebElement webElement = driver.findElement(By.xpath("//*[@id='SearchMakeActive']"));
			webElement.sendKeys(Keys.ENTER);
			sleep();
			keyPressing("enter");
			sleep(3);
		} else {
			executeJavaScript(driver, "document.getElementById('SearchMakeActive').click();");
			sleep();
			acceptJavascriptAlert(driver);
		}
	}

	/**
	 * click Make Inactive Button At Loan Tab
	 */
	public void clickMakeInactiveButtonAtLoanTab() {
		waitForControl(driver, Interfaces.LoansPage.LOAN_MAKE_INACTIVE_BUTTON, timeWait);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			WebElement webElement = driver.findElement(By.xpath("//*[@id='SetInactive']"));
			webElement.sendKeys(Keys.ENTER);
			sleep();
			keyPressing("enter");
			sleep(3);
		} else {
			executeJavaScript(driver, "document.getElementById('SetInactive').click();");
			sleep();
			acceptJavascriptAlert(driver);
		}
	}

	/**
	 * click Make Active Button At Loan Tab
	 */
	public void clickMakeActiveButtonAtLoanTab() {
		waitForControl(driver, Interfaces.LoansPage.LOAN_MAKE_ACTIVE_BUTTON, timeWait);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			WebElement webElement = driver.findElement(By.xpath("//*[@id='SetActive']"));
			webElement.sendKeys(Keys.ENTER);
			sleep();
			keyPressing("enter");
			sleep(3);
		} else {
			executeJavaScript(driver, "document.getElementById('SetActive').click();");
			sleep();
			acceptJavascriptAlert(driver);
		}
	}

	/**
	 * Search by Active/Inactive status
	 */
	public void searchByActiveOrInactive(String address, String status) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_PROPERTY_ADDRESS_TEXTBOX, address);
		sleep(1);
		click(driver, Interfaces.LoansPage.DYNAMIC_ACTIVE_RADIO_BUTTON, status);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * Input Midland ID
	 */
	public void inputMidlandID(String midlandID) {
		waitForControl(driver, Interfaces.LoansPage.MIDLAND_ID_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.MIDLAND_ID_TEXTBOX, midlandID);
		sleep(1);
	}

	/**
	 * All value display in Loan search table (B2R)
	 * @param legalName
	 * @param loanID
	 * @param finalBorrowerLLC
	 * @param midlandID
	 * @param applicant
	 * @param numberProperties
	 * @param totalEstimatedValue
	 * @param loanStatusChangeDate
	 * @param loanStatus
	 * @param dateCreated
	 * @param active
	 * @return
	 */
	public boolean isAllValueDisplayOnSearchLoansTableB2R(String legalName, String loanID, String finalBorrowerLLC, String midlandID, String applicant, String numberProperties,
			String totalEstimatedValue, String loanStatusChangeDate, String loanStatus, String dateCreated, String active) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_ALL_VALUE_DISPLAY_IN_SEARCH_LOANS_TABLE_B2R, legalName, loanID, finalBorrowerLLC, midlandID, applicant,
				numberProperties, totalEstimatedValue, loanStatusChangeDate, loanStatus, dateCreated, active, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_ALL_VALUE_DISPLAY_IN_SEARCH_LOANS_TABLE_B2R, legalName, loanID, finalBorrowerLLC, midlandID, applicant,
				numberProperties, totalEstimatedValue, loanStatusChangeDate, loanStatus, dateCreated, active);
	}

	/**
	 * All value display in Loan search table (CAF)
	 * @param legalName
	 * @param applicant
	 * @param expectedClosedDate
	 * @param numberProperties
	 * @param totalEstimatedValue
	 * @param loanStatus
	 * @param dateCreated
	 * @param originator
	 * @param loanStatusChangeDate
	 * @param comments
	 * @param active
	 * @return
	 */
	public boolean isAllValueDisplayOnSearchLoansTableCAF(String legalName, String applicant, String expectedClosedDate, String numberProperties, String totalEstimatedValue,
			String loanStatus, String dateCreated, String originator, String loanStatusChangeDate, String comments, String active) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_ALL_VALUE_DISPLAY_IN_SEARCH_LOANS_TABLE_CAF, legalName, applicant, expectedClosedDate, numberProperties,
				totalEstimatedValue, loanStatus, dateCreated, originator, loanStatusChangeDate, comments, active, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_ALL_VALUE_DISPLAY_IN_SEARCH_LOANS_TABLE_CAF, legalName, applicant, expectedClosedDate, numberProperties,
				totalEstimatedValue, loanStatus, dateCreated, originator, loanStatusChangeDate, comments, active);
	}

	/**
	 * Get text in Loan search
	 * 
	 * @param text
	 * @return
	 */
	public String getTextLoanSearch(String text) {
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_GET_TEXT_IN_SEARCH_LOAN_TABLE, text, timeWait);
		String itemName = getText(driver, Interfaces.HomePage.DYNAMIC_GET_TEXT_IN_SEARCH_LOAN_TABLE, text);
		return itemName;
	}
	
	/**
	 * Get text in Loan search
	 * 
	 * @param text
	 * @return
	 */
	public String getTextLoanSearch(String text, int i) {
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_GET_TEXT_IN_SEARCH_LOAN_TABLE, text, timeWait);
		String itemName = getText(driver, Interfaces.HomePage.DYNAMIC_GET_TEXT_IN_SEARCH_LOAN_TABLE, text);
		if (i==1) return convertDate(itemName);
		else return itemName.replace("$", "").replace(".00", "").replace(",", "");
	}

	/**
	 * search by Loan ID
	 * 
	 * @param loanID
	 */
	public void searchLoanByLoanID(String loanID) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_LOAN_ID_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_LOAN_ID_TEXTBOX, loanID);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * search by Final Borrower LLC
	 * 
	 * @param loanName
	 * @param finalBorrowerLLC
	 */
	public void searchLoanByFinalBorrowerLLC(String loanName, String finalBorrowerLLC) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, loanName);
		sleep(1);
		type(driver, Interfaces.LoansPage.SEARCH_FINAL_BORROWER_LLC_TEXTBOX, finalBorrowerLLC);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * search by Midland ID
	 * 
	 * @param loanName
	 * @param midlandID
	 */
	public void searchLoanByMidlandID(String loanName, String midlandID) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, loanName);
		sleep(1);
		type(driver, Interfaces.LoansPage.SEARCH_MIDLAND_ID_TEXTBOX, midlandID);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * search by Applicant
	 * 
	 * @param loanName
	 * @param applicant
	 */
	public void searchLoanByApplicant(String loanName, String applicant) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, loanName);
		sleep(1);
		selectItemCombobox(driver, Interfaces.LoansPage.LOAN_PROPERTY_APPLICANT_COMBOBOX, applicant);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * search by Loan Status Change Date
	 * 
	 * @param loanName
	 * @param changeDate
	 */
	public void searchLoanByLoanStatusChangeDate(String loanName, String changeDate) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, loanName);
		sleep(1);
		type(driver, Interfaces.LoansPage.SEARCH_LOAN_STATUS_CHANGE_DATE_TEXTBOX, changeDate);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * search by Loan Application Status (B2R)
	 * 
	 * @param loanName
	 * @param loanStatus
	 */
	public void searchLoanByLoanApplicationStatusB2R(String loanName, String loanStatus) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, loanName);
		sleep(1);
		click(driver, Interfaces.LoansPage.SEARCH_APPLICANT_STATUS_DROPDOWN);
		waitForControl(driver, Interfaces.LoansPage.UNCHECK_ALL_LINK, timeWait);
		sleep(1);
		click(driver, Interfaces.LoansPage.UNCHECK_ALL_LINK);
		sleep(1);
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_APPLICANT_STATUS_CHECKBOX, loanStatus, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_APPLICANT_STATUS_CHECKBOX, loanStatus);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}


	/**
	 * search by Loan Application Status (CAF)
	 * 
	 * @param loanName
	 * @param loanStatus
	 */
	public void searchLoanByLoanApplicationStatusCAF(String loanName, String loanStatus) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, loanName);
		sleep(1);
		selectItemCombobox(driver, Interfaces.LoansPage.SEARCH_LOAN_STATUS_COMBOBOX, loanStatus);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	
	/**
	 * search by Date Created
	 * 
	 * @param loanName
	 * @param dateCreated
	 */
	public void searchLoanByDateCreated(String loanName, String dateCreated) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, loanName);
		sleep(1);
		type(driver, Interfaces.LoansPage.SEARCH_DATE_CREATED_TEXTBOX, dateCreated);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}
	
	/**
	 * search by Expected Close Date
	 * 
	 * @param loanName
	 * @param expectedCloseDate
	 */
	public void searchLoanByExpectedCloseDate(String loanName, String expectedCloseDate) {
		waitForControl(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SEARCH_LEGAL_NAME_TEXTBOX, loanName);
		sleep(1);
		type(driver, Interfaces.LoansPage.SEARCH_EXPECTED_CLOSED_DATE_TEXTBOX, expectedCloseDate);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}

	/**
	 * Input Comments
	 */
	public void inputComments(String comments) {
		waitForControl(driver, Interfaces.LoansPage.COMMENTS_TEXT_AREA, timeWait);
		type(driver, Interfaces.LoansPage.COMMENTS_TEXT_AREA, comments);
		sleep(1);
	}

	/**
	 * Input Expected Close Date
	 */
	public void inputExpectedCloseDate(String expectedCloseDate) {
		waitForControl(driver, Interfaces.LoansPage.EXPECTED_CLOSED_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.EXPECTED_CLOSED_DATE_TEXTBOX, expectedCloseDate);
		sleep(1);
	}

	/**
	 * check number of Item in Loan Applicant Entities table on Basic Detail Tab
	 */
	public boolean isItemOfLoanApplicantEntitiesOnBasicDetailTab(int numberItem) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_APPLICANT_ENTITIES_FULLNAME_ITEMS, timeWait);
		int realNumber = countElement(driver, Interfaces.LoansPage.LOAN_APPLICANT_ENTITIES_FULLNAME_ITEMS);
		return realNumber == numberItem;
	}
	
	/**
	 * check Locked Loan save successfully message displays
	 */
	public boolean isLockedLoanSavedSuccessfullyMessageDisplay(String lockedLoanSuccess) {
		waitForControl(driver, Interfaces.LoansPage.LOCKED_LOAN_SAVED_SUCCESSFULLY_MESSAGE, lockedLoanSuccess, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.LOCKED_LOAN_SAVED_SUCCESSFULLY_MESSAGE, lockedLoanSuccess);
	}
	
	/**
	 * check Total number loan equal Loan Applicants list
	 */
	public boolean isTotalNumberLoanEqualLoanApplicantList(int numberTotalLoan) {
		waitForControl(driver, Interfaces.LoansPage.TOTAL_PROPERTY_LABEL, timeWait);
		String text = getText(driver, Interfaces.LoansPage.TOTAL_PROPERTY_LABEL);
		String[] subString = text.split(" ");
		int numberLoanApplicant = Integer.parseInt(subString[subString.length - 1]);
		System.out.println(numberLoanApplicant);
		if (numberTotalLoan == numberLoanApplicant) {
			return true;
		}
		return false;
	}
	
	/**
	 * click Send this information to email
	 */
	public void clickSendInformationToEmail() {
		waitForControl(driver, Interfaces.LoansPage.SEND_INFORMATION_TO_EMAIL, timeWait);
		click(driver, Interfaces.LoansPage.SEND_INFORMATION_TO_EMAIL);
		sleep();
	}
	
	/**
	 * Get user information
	 */
	public String getLegalName() {
		waitForControl(driver, Interfaces.LoansPage.FIRST_LEGAL_NAME, timeWait);
		return getText(driver, Interfaces.LoansPage.FIRST_LEGAL_NAME).trim().toLowerCase();
	}
	
	/**
	 * Get user information
	 */
	public String getAddressProperties() {
		waitForControl(driver, Interfaces.PropertiesPage.FIRST_ADDRESS, timeWait);
		return getText(driver, Interfaces.PropertiesPage.FIRST_ADDRESS).trim().toLowerCase();
	}
	
	/**
	 * Get user information
	 */
	public String getDocumentNumber() {
		waitForControl(driver, Interfaces.DocumentsPage.FIRST_DOCUMENT_NUMBER, timeWait);
		return getText(driver, Interfaces.DocumentsPage.FIRST_DOCUMENT_NUMBER).trim().toLowerCase();
	}
	
	/**
	 * check Load data message title display
	 * 
	 * @param N/A
	 */
	public boolean isBasicDetailMessageTitleDisplay(String messageContent) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_BASIC_DETAIL_MESSAGE_TITLE, messageContent, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_BASIC_DETAIL_MESSAGE_TITLE, messageContent);
	}
	
	/**
	 * check Dynamic Alert message title display
	 * 
	 * @param id
	 * @param messageContent
	 * @return
	 */
	public boolean isDynamicAlertMessageTitleDisplay(String id, String messageContent) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_FIELD_ALERT_MESSAGE_TITLE, id, messageContent, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_FIELD_ALERT_MESSAGE_TITLE, id, messageContent);
	}
	
	/**
	 * type in Loan Name textfield
	 * 
	 * @param loanName
	 */
	public void inputLoanName(String loanName){
		waitForControl(driver, Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL, timeWait);
		type(driver, Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL, loanName);
		
	}
	
	/**
	 * type in SF ID textfield
	 * 
	 * @param loanName
	 */
	public void inputLoansID(String loanID){
		waitForControl(driver, Interfaces.LoansPage.LOAN_APPLICATION_ID, timeWait);
		type(driver, Interfaces.LoansPage.LOAN_APPLICATION_ID, loanID);
		
	}
	
	/**
	 * type in final borrower LLC
	 * 
	 * @param loanName
	 */
	public void inputFinalBorrowerLLC(String loanID){
		waitForControl(driver, Interfaces.LoansPage.FINAL_BORROWER_LLC, timeWait);
		type(driver, Interfaces.LoansPage.FINAL_BORROWER_LLC, loanID);
		
	}
	
	/**
	 * Input SFR Buying Started
	 */
	public void inputSFRBuyingStarted(String value) {
		waitForControl(driver, Interfaces.LoansPage.SFR_BUYING_STARTED_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.SFR_BUYING_STARTED_TEXTBOX, value);
		sleep(1);
	}
	
	/**
	 * Input Company Website
	 */
	public void inputCompanyWebsite(String value) {
		waitForControl(driver, Interfaces.LoansPage.COMPANY_WEBSITE_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.COMPANY_WEBSITE_TEXTBOX, value);
		sleep(1);
	}
	
	/**
	 * Input Outsourcing Company 
	 */
	public void inputOutsourcingCompany (String value) {
		waitForControl(driver, Interfaces.LoansPage.OUTSOURCING_COMPANY_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.OUTSOURCING_COMPANY_TEXTBOX, value);
		sleep(1);
	}
	
	/**
	 * Input place of formation
	 */
	public void inputPlaceOfFormation(String value) {
		waitForControl(driver, Interfaces.LoansPage.PLACE_OF_FORMATION_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.PLACE_OF_FORMATION_TEXTBOX, value);
		sleep(1);
	}
	
	/**
	 * Input Date Established
	 */
	public void inputDateEstablished(String value) {
		waitForControl(driver, Interfaces.LoansPage.DATE_ESTABLISHED_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.DATE_ESTABLISHED_TEXTBOX, value);
		sleep(1);
	}
	
	/**
	 * Input primary contact 
	 */
	public void inputPrimaryContact(String value) {
		waitForControl(driver, Interfaces.LoansPage.PRIMARY_CONTACT_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.PRIMARY_CONTACT_TEXTBOX, value);
		sleep(1);
	}
	
	/**
	 * Input Phone textbox
	 */
	public void inputPhoneTextbox(String value) {
		waitForControl(driver, Interfaces.LoansPage.PHONE_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.PHONE_TEXTBOX, value);
		sleep(1);
	}
	
	/**
	 * Input Email textbox
	 */
	public void inputEmailTextbox(String value) {
		waitForControl(driver, Interfaces.LoansPage.EMAIL_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.EMAIL_TEXTBOX, value);
		sleep(1);
	}
	
	/**
	 * Input Address
	 */
	public void inputAddress(String value) {
		waitForControl(driver, Interfaces.LoansPage.ADDRESS_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.ADDRESS_TEXTBOX, value);
		sleep(1);
	}
	
	/**
	 * Input city
	 */
	public void inputCity(String value) {
		waitForControl(driver, Interfaces.LoansPage.CITY_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.CITY_TEXTBOX, value);
		sleep(1);
	}
	
	/**
	 * Input zipcode
	 */
	public void inputZipcode(String value) {
		waitForControl(driver, Interfaces.LoansPage.ZIPCODE_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.ZIPCODE_TEXTBOX, value);
		sleep(1);
	}
	
	/**
	 * Input comment
	 */
	public void inputCommentTextbox(String value) {
		waitForControl(driver, Interfaces.LoansPage.COMMENT_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.COMMENT_TEXTBOX, value);
		sleep(1);
	}
	
	/**
	 * Select SFR state combobox
	 */
	public void selectSFRState(String value) {
		waitForControl(driver, Interfaces.LoansPage.SFR_STATE_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.SFR_STATE_DROPDOWN, value);
		sleep();
	}
	
	/**
	 * Select Property Managament combobox
	 */
	public void selectPropertyManagement(String value) {
		waitForControl(driver, Interfaces.LoansPage.PROPERTY_MANAGAMENT_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.PROPERTY_MANAGAMENT_DROPDOWN, value);
		sleep();
	}
	
	/**
	 * Select Potfolio Hold combobox
	 */
	public void selectPotfolioHold(String value) {
		waitForControl(driver, Interfaces.LoansPage.POTFOLIO_HOLD_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.POTFOLIO_HOLD_DROPDOWN, value);
		sleep();
	}
	
	/**
	 * Select Source combobox
	 */
	public void selectSource(String value) {
		waitForControl(driver, Interfaces.LoansPage.SOURCE_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.SOURCE_DROPDOWN, value);
		sleep();
	}
	
	/**
	 * Select Owner combobox
	 */
	public void selectOwner(String value) {
		waitForControl(driver, Interfaces.LoansPage.OWNER_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.OWNER_DROPDOWN, value);
		sleep();
	}
	
	/**
	 * Select Company Type combobox
	 */
	public void selectCompanyType(String value) {
		waitForControl(driver, Interfaces.LoansPage.COMPANY_TYPE_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.COMPANY_TYPE_DROPDOWN, value);
		sleep();
	}
	
	/**
	 * Select State combobox
	 */
	public void selectState(String value) {
		waitForControl(driver, Interfaces.LoansPage.STATE_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.STATE_DROPDOWN, value);
		sleep();
	}
	
	
	/////////
	
	/**
	 * Is Final Borrower displayed correctly
	 * 
	 * @param loanName
	 */
	public Boolean isFinalBorrowerLLCDisplayedCorrectly(String value){
		waitForControl(driver, Interfaces.LoansPage.FINAL_BORROWER_LLC, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.FINAL_BORROWER_LLC, "value").contains(value);
		
	}
	
	/**
	 * is SFR Buying Started displayed correctly
	 */
	public Boolean isSFRBuyingStartedDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.SFR_BUYING_STARTED_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.SFR_BUYING_STARTED_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Company Website displayed correctly
	 */
	public Boolean isCompanyWebsiteDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.COMPANY_WEBSITE_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.COMPANY_WEBSITE_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Outsourcing Company  displayed correctly
	 */
	public Boolean isOutsourcingCompanyDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.OUTSOURCING_COMPANY_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.OUTSOURCING_COMPANY_TEXTBOX, "value").contains(value);
	}
	
	/**
	 * is place of formation displayed correctly
	 */
	public Boolean isPlaceOfFormationDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.PLACE_OF_FORMATION_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.PLACE_OF_FORMATION_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Date Established displayed correctly
	 */
	public Boolean isDateEstablishedDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.DATE_ESTABLISHED_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.DATE_ESTABLISHED_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is primary contact  displayed correctly
	 */
	public Boolean isPrimaryContactDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.PRIMARY_CONTACT_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.PRIMARY_CONTACT_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Phone textbox displayed correctly
	 */
	public Boolean isPhoneTextboxDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.PHONE_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.PHONE_TEXTBOX, "value").contains(value);
	}
	
	/**
	 * is Email textbox displayed correctly
	 */
	public Boolean isEmailTextboxDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.EMAIL_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.EMAIL_TEXTBOX, "value").contains(value);
	}
	
	/**
	 * is Address displayed correctly
	 */
	public Boolean isAddressDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.ADDRESS_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.ADDRESS_TEXTBOX, "value").contains(value);
	}
	
	/**
	 * is city displayed correctly
	 */
	public Boolean isCityDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.CITY_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.CITY_TEXTBOX, "value").contains(value);
	}
	
	/**
	 * is zipcode displayed correctly
	 */
	public Boolean isZipcodeDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.ZIPCODE_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.ZIPCODE_TEXTBOX, "value").contains(value);
	}
	
	/**
	 * is comment displayed correctly
	 */
	public Boolean isCommentTextboxDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.COMMENT_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.COMMENT_TEXTBOX, "value").contains(value);
	}
	
	/**
	 * is SFR state combobox displayed correctly
	 */
	public Boolean isSFRStateDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.SFR_STATE_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.SFR_STATE_DROPDOWN).contains(value);
	}
	
	/**
	 * is Property Managament combobox displayed correctly
	 */
	public Boolean isPropertyManagementDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.PROPERTY_MANAGAMENT_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.PROPERTY_MANAGAMENT_DROPDOWN).contains(value);
	}
	
	/**
	 * is Potfolio Hold combobox displayed correctly
	 */
	public Boolean isPotfolioHoldDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.POTFOLIO_HOLD_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.POTFOLIO_HOLD_DROPDOWN).contains(value);
	}
	
	/**
	 * is Source combobox displayed correctly
	 */
	public Boolean isSourceDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.SOURCE_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.SOURCE_DROPDOWN).contains(value);
	}
	
	/**
	 * is Owner combobox displayed correctly
	 */
	public Boolean isOwnerDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.OWNER_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.OWNER_DROPDOWN).contains(value);
	}
	
	/**
	 * is Company Type combobox displayed correctly
	 */
	public Boolean isCompanyTypeDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.COMPANY_TYPE_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.COMPANY_TYPE_DROPDOWN).contains(value);
	}
	
	/**
	 * is State combobox  displayed correctly
	 */
	public Boolean isStateDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.STATE_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.STATE_DROPDOWN).contains(value);
	}
	
	/**
	 * is Midland ID displayed correctly
	 */
	public Boolean isMidlandIDDisplayedCorrectly(String midlandID) {
		waitForControl(driver, Interfaces.LoansPage.MIDLAND_ID_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.MIDLAND_ID_TEXTBOX, "value").contains(midlandID);
	}
	
	/**
	 * is Loan Amount Requested displayed correctly
	 */
	public Boolean isLoanAmountRequestedDisplayedCorrectly(String loanAmountRequested) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_AMOUNT_REQUESTED_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.LOAN_AMOUNT_REQUESTED_TEXTBOX, "value").contains(loanAmountRequested);
	}
	
	/**
	 * Select Bridge borrower combobox
	 */
	public void selectBridgeBorrower(String value) {
		waitForControl(driver, Interfaces.LoansPage.BRIDGE_BORROWER_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.BRIDGE_BORROWER_DROPDOWN, value);
		sleep();
	}
	
	/**
	 * is Loan ID displayed correctly
	 */
	public Boolean isLoanIDDisplayed(String loanID) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_APPLICATION_ID, timeWait);
		String loanIDText = getAttributeValue(driver, Interfaces.LoansPage.LOAN_APPLICATION_ID, "Value");
		return loanIDText.contains(loanID);
	}
	
	/**
	 * is Loan Name displayed correctly
	 */
	public Boolean isLoanNameDisplayed(String loanName) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL, timeWait);
		String loanNameText = getAttributeValue(driver, Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL, "Value");
		return loanNameText.contains(loanName);
	}
	
	/**
	 * is Loan status combobox displayed correctly
	 */
	public Boolean isLoanStatusDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_STATUS_COMBOBOX_BASIC_DETAIL, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.LOAN_STATUS_COMBOBOX_BASIC_DETAIL).contains(value);
	}
	
	/**
	 * is Loan status combobox displayed correctly
	 */
	public Boolean isHistoryPopupBoxDisplayedCorrectly(String historyBoxName, String previousValue, String currentValue) {
		
		Boolean result;
		switch (historyBoxName) {
		
		        case "LOAN_NAME_TEXTBOX_BASIC_DETAIL_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.LOAN_NAME_TEXTBOX_BASIC_DETAIL_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "LOAN_STATUS_COMBOBOX_BASIC_DETAIL_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.LOAN_STATUS_COMBOBOX_BASIC_DETAIL_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.LOAN_STATUS_COMBOBOX_BASIC_DETAIL_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "LOAN_APPLICATION_ID_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.LOAN_APPLICATION_ID_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.LOAN_APPLICATION_ID_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "COMPANY_WEBSITE_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.COMPANY_WEBSITE_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.COMPANY_WEBSITE_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "MIDLAND_ID_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.MIDLAND_ID_TEXTBOX_HISTORY, 15);
		        	executeClick(driver, Interfaces.LoansPage.MIDLAND_ID_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "DATE_ESTABLISHED_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.DATE_ESTABLISHED_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.DATE_ESTABLISHED_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "PHONE_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.PHONE_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.PHONE_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "EMAIL_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.EMAIL_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.EMAIL_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "ADDRESS_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.ADDRESS_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.ADDRESS_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result; 
		        	
		        case "CITY_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.CITY_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.CITY_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result; 
		        	
		        case "ZIPCODE_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.ZIPCODE_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.ZIPCODE_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "FINAL_BORROWER_LLC_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.FINAL_BORROWER_LLC_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.FINAL_BORROWER_LLC_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "SFR_STATE_DROPDOWN_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.SFR_STATE_DROPDOWN_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.SFR_STATE_DROPDOWN_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "POTFOLIO_HOLD_DROPDOWN_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.POTFOLIO_HOLD_DROPDOWN_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.POTFOLIO_HOLD_DROPDOWN_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "SOURCE_DROPDOWN_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.SOURCE_DROPDOWN_HISTORY, 15);
		        	scrollPageToControl(driver, Interfaces.LoansPage.PHONE_TEXTBOX_HISTORY);
		        	while(!isControlDisplayed(driver, Interfaces.LoansPage.CLOSE_HISTORY_POPUP))
		        	click(driver, Interfaces.LoansPage.SOURCE_DROPDOWN_HISTORY);
		    		sleep();
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "OWNER_DROPDOWN_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.OWNER_DROPDOWN_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.OWNER_DROPDOWN_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "COMPANY_TYPE_DROPDOWN_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.COMPANY_TYPE_DROPDOWN_HISTORY, 15);
		        	executeClick(driver, Interfaces.LoansPage.COMPANY_TYPE_DROPDOWN_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "STATE_DROPDOWN_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.STATE_DROPDOWN_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.STATE_DROPDOWN_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "BRIDGE_BORROWER_DROPDOWN_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.BRIDGE_BORROWER_DROPDOWN_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.BRIDGE_BORROWER_DROPDOWN_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "FLOOR_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.FLOOR_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.FLOOR_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "LOAN_AMOUNT_REQUESTED_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.LOAN_AMOUNT_REQUESTED_TEXTBOX_HISTORY, 15);
		        	executeClick(driver, Interfaces.LoansPage.LOAN_AMOUNT_REQUESTED_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        //For CAF
		        	
		        case "TERM_SHEET_ISSUED_DATE_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.TERM_SHEET_ISSUED_DATE_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.TERM_SHEET_ISSUED_DATE_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "TERM_SHEET_SIGNED_DATE_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.TERM_SHEET_SIGNED_DATE_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.TERM_SHEET_SIGNED_DATE_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "ANTICIPATED_IC_APPROVAL_DATE_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.ANTICIPATED_IC_APPROVAL_DATE_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.ANTICIPATED_IC_APPROVAL_DATE_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "EXPECTED_CLOSED_DATE_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.EXPECTED_CLOSED_DATE_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.EXPECTED_CLOSED_DATE_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "KICK_OFF_DATE_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.KICK_OFF_DATE_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.KICK_OFF_DATE_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "AMORTIZATION_TERM_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.AMORTIZATION_TERM_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.AMORTIZATION_TERM_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "LOAN_TERM_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.LOAN_TERM_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.LOAN_TERM_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "IO_TERM_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.IO_TERM_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.IO_TERM_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "LTV_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.LTV_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.LTV_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "COUNTRY_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.COUNTRY_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.COUNTRY_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "CANCELLATION_REASON_TEXTBOX_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.CANCELLATION_REASON_TEXTBOX_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.CANCELLATION_REASON_TEXTBOX_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "FOREIGN_NATIONAL_DROPDOWN_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.FOREIGN_NATIONAL_DROPDOWN_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.FOREIGN_NATIONAL_DROPDOWN_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "LOAN_PURPOSE_DROPDOWN_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.LOAN_PURPOSE_DROPDOWN_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.LOAN_PURPOSE_DROPDOWN_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "REFINANCE_TYPE_DROPDOWN_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.REFINANCE_TYPE_DROPDOWN_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.REFINANCE_TYPE_DROPDOWN_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "RECOURSE_DROPDOWN_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.RECOURSE_DROPDOWN_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.RECOURSE_DROPDOWN_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		        	
		        case "PROPERTY_MANAGAMENT_DROPDOWN_HISTORY":
		        	waitForControl(driver, Interfaces.LoansPage.PROPERTY_MANAGAMENT_DROPDOWN_HISTORY, 15);
		        	click(driver, Interfaces.LoansPage.PROPERTY_MANAGAMENT_DROPDOWN_HISTORY);
		        	waitForControl(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue, 15);
		        	result = isControlDisplayed(driver, Interfaces.LoansPage.HISTORY_POPUP_CONTENT, previousValue, currentValue);
		        	clickCloseHistoryBoxButton();
		        	sleep();
		        	if(driver.toString().toLowerCase().contains("internetexplorer")){
		                sleep(5);
		           }
		        	return result;
		}
		return false;
	}
	
	
	
	/**
	 * click close history box Button
	 */
	public void clickCloseHistoryBoxButton() {
		waitForControl(driver, Interfaces.LoansPage.CLOSE_HISTORY_POPUP, timeWait);
		click(driver, Interfaces.LoansPage.CLOSE_HISTORY_POPUP);
	}
	
	/**
	 * click Save Button [NEW DOCUMENT]
	 */
	public void clickSaveButtonValidDataType() {
		waitForControl(driver, Interfaces.PropertiesPage.SAVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
          sleep(10);
     }
		sleep(5);
		isBasicDetailMessageTitleDisplay("Please correct errors marked below");
	}
	
	
	/**
	 * input Floor
	 */
	public void inputFloor(String value) {
		waitForControl(driver, Interfaces.LoansPage.FLOOR_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.FLOOR_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input Amortization Term
	 */
	public void inputAmortizationTerm(String value) {
		waitForControl(driver, Interfaces.LoansPage.AMORTIZATION_TERM_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.AMORTIZATION_TERM_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input Loan Term 
	 */
	public void selectLoanTerm(String value) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_TERM_TEXTBOX, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.LOAN_TERM_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input IO Term 
	 */
	public void inputIOTerm (String value) {
		waitForControl(driver, Interfaces.LoansPage.IO_TERM_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.IO_TERM_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input Margin
	 */
	public void inputMargin(String value) {
		waitForControl(driver, Interfaces.LoansPage.MARGIN_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.MARGIN_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input LTV
	 */
	public void inputLTV(String value) {
		waitForControl(driver, Interfaces.LoansPage.LTV_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.LTV_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input  County 
	 */
	public void inputCounty(String value) {
		waitForControl(driver, Interfaces.LoansPage.COUNTRY_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.COUNTRY_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input Call Protection
	 */
	public void inputCallProtection(String value) {
		waitForControl(driver, Interfaces.LoansPage.CALL_PROTECTION, timeWait);
		type(driver, Interfaces.LoansPage.CALL_PROTECTION, value);
		sleep();
	}
	
	/**
	 * input Cancellation Reason 
	 */
	public void inputCancellationReason (String value) {
		waitForControl(driver, Interfaces.LoansPage.CANCELLATION_REASON_TEXTBOX, timeWait);
		type(driver, Interfaces.LoansPage.CANCELLATION_REASON_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * Select Foreign National  combobox
	 */
	public void selectForeignNational (String value) {
		waitForControl(driver, Interfaces.LoansPage.FOREIGN_NATIONAL_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.FOREIGN_NATIONAL_DROPDOWN, value);
		sleep();
	}
	
	/**
	 * Select Loan Purpose combobox
	 */
	public void selectLoanPurpose(String value) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_PURPOSE_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.LOAN_PURPOSE_DROPDOWN, value);
		sleep();
	}
	
	/**
	 * Select Owner combobox
	 */
	public void selectRefinanceType (String value) {
		waitForControl(driver, Interfaces.LoansPage.REFINANCE_TYPE_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.REFINANCE_TYPE_DROPDOWN, value);
		sleep();
	}
	
	/**
	 * Select Underwriter combobox
	 */
	public void selectUnderwriter (String value) {
		waitForControl(driver, Interfaces.LoansPage.UNDERWRITER_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.UNDERWRITER_DROPDOWN, value);
		sleep();
	}
	
	/**
	 * Select Loan Coordinator combobox
	 */
	public void selectLoanCoordinator (String value) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_COORDINATOR_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.LOAN_COORDINATOR_DROPDOWN, value);
		sleep();
	}
	
	/**
	 * Select Closer  combobox
	 */
	public void selectCloser (String value) {
		waitForControl(driver, Interfaces.LoansPage.CLOSER_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.CLOSER_DROPDOWN, value);
		sleep();
	}
	
	/**
	 * Select Recourse   combobox
	 */
	public void selectRecourse(String value) {
		waitForControl(driver, Interfaces.LoansPage.RECOURSE_DROPDOWN, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.RECOURSE_DROPDOWN, value);
		sleep();
	}
	
	/**
	 * Input Originator
	 */
	public void selectOriginator(String originator) {
		waitForControl(driver, Interfaces.LoansPage.ORIGINATOR_DROPDOWN_LIST, timeWait);
		selectItemCombobox(driver, Interfaces.LoansPage.ORIGINATOR_DROPDOWN_LIST, originator);
		sleep(1);
	}
	
	/////
	
	/**
	 * is Floor Displayed Correctly
	 */
	public Boolean isFloorDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.FLOOR_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.FLOOR_TEXTBOX, "value").contains(value);
	}
	
	/**
	 * is Amortization Term Displayed Correctly
	 */
	public Boolean isAmortizationTermDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.AMORTIZATION_TERM_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.AMORTIZATION_TERM_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Loan Term Displayed Correctly 
	 */
	public Boolean isLoanTermDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_TERM_TEXTBOX, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.LOAN_TERM_TEXTBOX).contains(value);
		
	}
	
	/**
	 * is IO Term Displayed Correctly 
	 */
	public Boolean isIOTermDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.IO_TERM_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.IO_TERM_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Margin Displayed Correctly
	 */
	public Boolean isMarginDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.MARGIN_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.MARGIN_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is LTV Displayed Correctly
	 */
	public Boolean isLTVDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.LTV_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.LTV_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is  County Displayed Correctly 
	 */
	public Boolean isCountyDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.COUNTRY_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.COUNTRY_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Call Protection Displayed Correctly
	 */
	public Boolean isCallProtectionDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.CALL_PROTECTION, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.CALL_PROTECTION, "value").contains(value);
		
	}
	
	/**
	 * is Cancellation Reason Displayed Correctly 
	 */
	public Boolean isCancellationReasonDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.CANCELLATION_REASON_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.CANCELLATION_REASON_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Foreign National  combobox  Displayed Correctly
	 */
	public Boolean isForeignNationalDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.FOREIGN_NATIONAL_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.FOREIGN_NATIONAL_DROPDOWN).contains(value);
		
	}
	
	/**
	 * is Loan Purpose combobox  Displayed Correctly
	 */
	public Boolean isLoanPurposeDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_PURPOSE_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.LOAN_PURPOSE_DROPDOWN).contains(value);
		
	}
	
	/**
	 * Is Owner combobox Displayed Correctly
	 */
	public Boolean isRefinanceTypeDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.REFINANCE_TYPE_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.REFINANCE_TYPE_DROPDOWN).contains(value);
		
	}
	
	/**
	 * is Underwriter combobox Displayed Correctly
	 */
	public Boolean isUnderwriterDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.UNDERWRITER_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.UNDERWRITER_DROPDOWN).contains(value);
		
	}
	
	/**
	 * is Loan Coordinator combobox  Displayed Correctly
	 */
	public Boolean isLoanCoordinatorDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.LOAN_COORDINATOR_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.LOAN_COORDINATOR_DROPDOWN).contains(value);
		
	}
	
	/**
	 * is Closer combobox Displayed Correctly
	 */
	public Boolean isCloserDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.CLOSER_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.CLOSER_DROPDOWN).contains(value);
		
	}
	
	/**
	 * is Recourse combobox  Displayed Correctly
	 */
	public Boolean isRecourseDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.RECOURSE_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.RECOURSE_DROPDOWN).contains(value);
		
	}
	
	/**
	 * is  Originator Displayed Correctly
	 */
	public Boolean isOriginatorDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.ORIGINATOR_DROPDOWN_LIST, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.ORIGINATOR_DROPDOWN_LIST).contains(value);
	}
	
	////
	
	/**
	 * is Term Sheet Issued Date (CAF) Displayed Correctly
	 */
	public Boolean isTermSheetIssuedDateDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.TERM_SHEET_ISSUED_DATE_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.TERM_SHEET_ISSUED_DATE_TEXTBOX, "value").contains(value);
		
	}

	/**
	 * is Anticipated IC Approval Date (CAF) Displayed Correctly
	 */
	public Boolean isAnticipatedICApprovalDateDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.ANTICIPATED_IC_APPROVAL_DATE_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.ANTICIPATED_IC_APPROVAL_DATE_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Term Sheet Signed Date (CAF) Displayed Correctly
	 */
	public Boolean isTermSheetSignedDateDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.TERM_SHEET_SIGNED_DATE_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.TERM_SHEET_SIGNED_DATE_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * Input Expected Close Date Displayed Correctly
	 */
	public Boolean isExpectedCloseDateDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.EXPECTED_CLOSED_DATE_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.EXPECTED_CLOSED_DATE_TEXTBOX, "value").contains(value);
	}
	
	/**
	 * is Kick off date Displayed Correctly
	 */
	public Boolean isKickOffDateDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.KICK_OFF_DATE_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.KICK_OFF_DATE_TEXTBOX, "value").contains(value);
		
	}
	
	/**
	 * is Bridge borrower combobox Displayed Correctly
	 */
	public Boolean isBridgeBorrowerDisplayedCorrectly(String value) {
		waitForControl(driver, Interfaces.LoansPage.BRIDGE_BORROWER_DROPDOWN, timeWait);
		return getItemSelectedCombobox(driver, Interfaces.LoansPage.BRIDGE_BORROWER_DROPDOWN).contains(value);
		
	}
	
	/**
	 * get First option Underwriter
	 */
	public String getFirstOptionUnderwriter() {
		waitForControl(driver, Interfaces.LoansPage.UNDERWRITER_DROPDOWN_FIRST_OPTION, timeWait);
		return getText(driver, Interfaces.LoansPage.UNDERWRITER_DROPDOWN_FIRST_OPTION);
	}
	
	/**
	 * get First option Loan Coordinator
	 */
	public String getFirstOptionLoanCoord() {
		waitForControl(driver, Interfaces.LoansPage.LOAN_COORDINATOR_DROPDOWN_FIRST_OPTION, timeWait);
		return getText(driver, Interfaces.LoansPage.LOAN_COORDINATOR_DROPDOWN_FIRST_OPTION);
	}
	
	/**
	 * get First option Closer
	 */
	public String getFirstOptionCloser() {
		waitForControl(driver, Interfaces.LoansPage.CLOSER_DROPDOWN_FIRST_OPTION, timeWait);
		return getText(driver, Interfaces.LoansPage.CLOSER_DROPDOWN_FIRST_OPTION);
	}
	
	/**
	 * Check Property displayed correctly
	 */
	public boolean isPropertyDisplayedCorrectly(String propertyIndex, String propertyAddress, String isActive) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_PROPERTY_RECORD, propertyIndex, propertyAddress, isActive, 10);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_PROPERTY_RECORD, propertyIndex, propertyAddress, isActive);
	}
	
	/**
	 * Check property checkbox
	 */
	public void selectPropertyCheckbox(String propertyIndex, String propertyAddress) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_PROPERTY_CHECKBOX, propertyIndex, propertyAddress, timeWait);
		click(driver, Interfaces.LoansPage.DYNAMIC_PROPERTY_CHECKBOX, propertyIndex, propertyAddress);
		sleep();
	}
	
	private WebDriver driver;
	private String ipClient;
}