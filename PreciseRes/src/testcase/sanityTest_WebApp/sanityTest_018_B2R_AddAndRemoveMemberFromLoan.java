package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;

import common.AbstractTest;
import common.Constant;

public class sanityTest_018_B2R_AddAndRemoveMemberFromLoan extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		usernameBorrower = Constant.USERNAME_B2R_BORROWER;
		passwordBorrower = Constant.PASSWORD_B2R_BORROWER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan-B2R" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Drexel Investments (Demo)";
		memberName = "auto-B2R-member2014";
		memberLoginId = "auto-B2R-member2014";
		memberPassword = "change";
		memberName2 = "auto2-B2R-member2014";
		memberLoginId2 = "auto2-B2R-member2014";
		propertiesFileName = "B2R Data Tape.xlsx";
		numberOfProperty = 8;
		address = "111222 37TH ST";
		newCity = "Buffalo";
		newState = "NY - New York";
		shortNewState = "NY";
		newZip = "67833";
		newPropertyType = "Townhome";
	}

	@Test(groups = { "regression" }, description = "Security17 - Member add other members to the team")
	public void SecurityTest_17_MemberAddMemberToLoan() {
		log.info("Pre-Condition - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender,false);

		log.info("Pre-Condition - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Pre-Condition - Step 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus,	loanName);
		
		log.info("VP: Applicant displays correctly in Loan Application Entity");
		verifyTrue(loansPage.isNewApplicantDisplayOnLAETable(applicantName));

		log.info("Pre-Condition - Step 04. Add member to loan team");
		loansPage.addMemberInBasicDetailTeam(memberName);

		log.info("VP: The Member displays on Team table");
		verifyTrue(loansPage.isSaveSuccessMessageDisplay());
		
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			log.info("SecurityTest_17. Open Loans tab");
			loansPage.openLoansPage(driver, ipClient);

			log.info("SecurityTest_17. Search loan item");
			loansPage.searchLoanByName(loanName);

			log.info("SecurityTest_17. Open detail loan item");
			loansPage.openLoansDetailPage(loanName);
		}
		verifyTrue(loansPage.isMemberDisplayOnTeamTable(memberName));

		log.info("SecurityTest_17 - Step 01. Login with the member");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(memberLoginId, memberPassword, false);

		log.info("VP: Member can see the loan");
		verifyTrue(homePage.isLoanDisplayOnDashboard(loanName));

		log.info("SecurityTest_17 - Step 02. Go to Loan page");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("SecurityTest_17 - Step 03. Search loan by name");
		loansPage.searchLoanByName(loanName);

		log.info("SecurityTest_17 - Step 04. Open loan item");
		loansPage.openLoansDetailPage(loanName);

//		log.info("SecurityTest_17 - Step 05. Click New team member button");
//		loansPage.clickNewTeamMemberButton();

		log.info("SecurityTest_17 - Step 06. Add other member to loan team");
		loansPage.addMemberInBasicDetailTeam(memberName2);

		log.info("VP: The Member displays on Team table");
		verifyTrue(loansPage.isSaveSuccessMessageDisplay());
		
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			log.info("SecurityTest_17. Open Loans tab");
			loansPage.openLoansPage(driver, ipClient);

			log.info("SecurityTest_17. Search loan item");
			loansPage.searchLoanByName(loanName);

			log.info("SecurityTest_17. Open detail loan item");
			loansPage.openLoansDetailPage(loanName);
		}
		verifyTrue(loansPage.isSecondMemberDisplayOnTeamTable(memberName2));

		log.info("Pre-Condition - Step 08. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Pre-Condition - Step 09. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);
	}

	@Test(groups = { "regression" }, description = "Security18 - Other member can view added loan with property")
	public void SecurityTest_18_OtherMemberCanViewAddedLoanWithProperty() {
		log.info("SecurityTest_18 - Step 01. Login with other the member");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(memberLoginId2, memberPassword,false);

		log.info("VP: Member can see the loan");
		verifyTrue(homePage.isLoanDisplayOnDashboard(loanName));

		log.info("SecurityTest_18 - Step 02. Go to Loan page");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("SecurityTest_18 - Step 03. Search loan by name");
		loansPage.searchLoanByName(loanName);

		log.info("VP: The loan displays in table");
		verifyTrue(loansPage.isLoansDisplayOnSearchLoanPage(loanName));

		log.info("SecurityTest_18 - Step 04. Open loan item");
		loansPage.openLoansDetailPage(loanName);

		log.info("SecurityTest_18 - Step 05. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("SecurityTest_18 - Step 06. Get Property number");
		propertyNumberProtab = loansPage.getNumberOfPropertyInPropertiesTab();

		log.info("VP: Number of property is uploaded enough");
		verifyEquals(propertyNumberProtab, numberOfProperty);
	}

	@Test(groups = { "regression" }, description = "Security21 - Lender remove member from loan team")
	public void SecurityTest_21_LenderRemoveMemberFromLoanTeam() {
		log.info("SecurityTest_21 - Step 01. Login with lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameLender, passwordLender,false);

		log.info("SecurityTest_21 - Step 02. Go to Loan page");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("SecurityTest_21 - Step 03. Search loan by name");
		loansPage.searchLoanByName(loanName);

		log.info("SecurityTest_21 - Step 04. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("SecurityTest_21 - Step 05. Remove member from loan team");
		loansPage.removeMemberFromLoanTeam(2);

		log.info("VP: Member is removed from loan team");
		verifyFalse(loansPage.isSecondMemberDisplayOnTeamTable(memberName2));
	}

	@Test(groups = { "regression" }, description = "Security22 - Other Member cannot view loan after remove from loan team")
	public void SecurityTest_22_MemberCannotViewLoanAfterRemoveFromLoanTeam() {
		log.info("SecurityTest_22 - Step 01. Login with the member");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(memberLoginId2, memberPassword,false);

		log.info("VP: Member cannot see the loan");
		verifyFalse(homePage.isLoanDisplayOnDashboard(loanName));

		log.info("SecurityTest_22 - Step 02. Go to Loan page");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("SecurityTest_22 - Step 03. Search loan by name");
		loansPage.searchLoanByName(loanName);

		log.info("VP: The loan does not display in table");
		verifyFalse(loansPage.isLoansDisplayOnSearchLoanPage(loanName));
	}

	@Test(groups = { "regression" }, description = "Security25 - Member cannot view detail loan by url when not add to loan")
	public void SecurityTest_25_MemberCannotViewDetailNotAddedLoanByUrl() {
		log.info("SecurityTest_25 - Step 01. Login with Member");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameLender, passwordLender,false);

		log.info("SecurityTest_25 - Step 02. Go to Loan page");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("SecurityTest_25 - Step 03. Search loan by name");
		loansPage.searchLoanByName(loanName);

		log.info("SecurityTest_25 - Step 04. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("SecurityTest_25 - Step 05. Get loan detail url");
		currentUrl = loansPage.getCurrentUrl(driver);

		log.info("SecurityTest_25 - Step 06: Log out and login with member");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(memberLoginId2, memberPassword, false);

		log.info("SecurityTest_25 - Step 05: Navigate to the loans by url");
		homePage.openLink(driver, currentUrl);

		log.info("VP: Verify Member cannot view loan detail of not added loan");
		verifyTrue(homePage.isErrorProcessRequestDisplay());
	}

	@Test(groups = { "regression" }, description = "Security26 - Borrower cannot view his detail loan by url")
	public void SecurityTest_26_BorrowerCannotViewDetailNotAddedLoanByUrl() {
		log.info("SecurityTest_26 - Step 01: Log out and login with borrower");
		loginPage = logout(driver, ipClient);
		loansPage = loginPage.loginAsBorrower(usernameBorrower,	passwordBorrower, false);

		log.info("SecurityTest_26 - Step 02: Navigate to the loans by url");
		loansPage.openLink(driver, currentUrl);

		log.info("VP: Verify Borrower cannot view loan detail of not added loan");
		verifyTrue(homePage.isErrorProcessRequestDisplay());
	}

	 @Test(groups = { "regression" }, description = "Security27 - go to a property in the loan and change property details")
	 public void SecurityTest_27_EditPropertyDetails() {
		 
	 log.info("SecurityTest_27 - Step 01: Log out and login with member");
	 loginPage = logout(driver, ipClient);
	 homePage = loginPage.loginAsLender(memberLoginId, memberPassword, false);
	
	 log.info("SecurityTest_27 - Step 02: Open loan page");
	 loansPage = homePage.openLoansPage(driver, ipClient);
	
	 log.info("SecurityTest_27 - Step 03: Search Loan by name");
	 loansPage.searchLoanByName(loanName);
	
	 log.info("SecurityTest_27 - Step 04: Open loan detail page");
	 loansPage.openLoansDetailPage(loanName);
	
	 log.info("SecurityTest_27 - Step 05: Open property tab");
	 loansPage.openPropertiesTab();
	
	 log.info("SecurityTest_27 - Step 06: Open a property detail");
	 propertiesPage = loansPage.openPropertyDetail(address);
	
	 log.info("SecurityTest_27 - Step 07: Update property information");
	 propertiesPage.updatePropertyInfor(newCity, newState, newZip,newPropertyType);
	
	 log.info("SecurityTest_27 - Step 08: Go to Property list");
	 loansPage = propertiesPage.clickOnPropertyListButton();
	
	 log.info("VP: Make sure the details are updated correctly");
	 verifyTrue(loansPage.isPropertyInfoCorrectly(address, newCity,shortNewState, newZip, newPropertyType));
	 }

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private String usernameLender, passwordLender, loanStatus;
	private String loanName, accountNumber, applicantName, propertiesFileName;
	private String memberName, memberLoginId, memberPassword, currentUrl;
	private String usernameBorrower, passwordBorrower, memberName2,
			memberLoginId2;
	private int numberOfProperty, propertyNumberProtab;
	private String address, newCity, newState, shortNewState, newZip,newPropertyType;
}