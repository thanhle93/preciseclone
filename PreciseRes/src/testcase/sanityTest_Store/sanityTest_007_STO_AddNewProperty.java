package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;

import common.AbstractTest;
import common.Constant;

public class sanityTest_007_STO_AddNewProperty extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + getUniqueNumber();
		propertiesFileName = "StoreDataTape.xlsx";
		propertiesFileNameUpdated = "StoreDataTape_Updated.xlsx";
		numberOfProperty = 8;
		address = "1020 Bonforte Blvd";
		addressUpdated = "1020 Bonforte Blvd Updated";
	}

	@Test(groups = { "regression" }, description = "New Property 01 - Add new Property (Load Property by excel file)")
	public void NewProperty_01_AddNewPropertyByExcelTemplate() {
		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("Precondition - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("NewProperty_01 - Step 01: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("NewProperty_01 - Step 02: Click 'Add Properties' button");
		transactionPage.clickAddPropertyButton();

		log.info("NewProperty_01 - Step 03: Click 'Import' button");
		transactionPage.clickSaveLoadPropertyButton();

		log.info("VP: 'No record saved, nothing entered.' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("No record saved, nothing entered."));

		log.info("NewProperty_01 - Step 04: Load 'Property Excel Template' file");
		transactionPage.uploadFileProperties(propertiesFileName);

		log.info("VP: Number of property is uploaded enough");
		propertyNumberProtab = transactionPage.getNumberOfPropertyInPropertiesTab();
		verifyEquals(propertyNumberProtab, numberOfProperty);

		log.info("VP: Information of property 1 is correct");
		verifyTrue(transactionPage.isPropertyInfoCorrectly("P0001688", "148 Craft Drive", "Alamosa", "CO", "81101"));

		log.info("VP: Information of property 2 is correct");
		verifyTrue(transactionPage.isPropertyInfoCorrectly("P0001692", "150 7th Street", "Limon", "CO", "80828"));

		log.info("VP: Information of property 3 is correct");
		verifyTrue(transactionPage.isPropertyInfoCorrectly("P0001687", "650 Beverly Street", "Elizabeth", "CO", "80107"));

		log.info("VP: Information of property 4 is correct");
		verifyTrue(transactionPage.isPropertyInfoCorrectly("P0001689", "840 Spanish Bit Drive", "Monument", "CO", "80921"));

		log.info("VP: Information of property 5 is correct");
		verifyTrue(transactionPage.isPropertyInfoCorrectly("P0001693", "1010 Cherry Street", "La Veta", "CO", "81055"));

		log.info("VP: Information of property 6 is correct");
		verifyTrue(transactionPage.isPropertyInfoCorrectly("P0001686", "1020 Bonforte Blvd", "Pueblo", "CO", "81001"));

		log.info("VP: Information of property 7 is correct");
		verifyTrue(transactionPage.isPropertyInfoCorrectly("P0001690", "8625 US Highway 50", "Lamar", "CO", "81052"));

		log.info("VP: Information of property 8 is correct");
		verifyTrue(transactionPage.isPropertyInfoCorrectly("P0001691", "26980 W US Highway", "La Junta", "CO", "81050"));

		log.info("NewProperty_01 - Step 05. Search with Address name is '1020 Bonforte Blvd'");
		transactionPage.searchPropertyAddress(address);

		log.info("NewProperty_01 - Step 06. Open a property detail");
		transactionPage.openPropertyDetail(address);

		log.info("VP: Value of P# field is 'P0001686'");
		pNumber = transactionPage.getValuePNumber();
		verifyEquals(pNumber, "P0001686");

		log.info("VP: Value of Unit Number field is 'Loc #3, Bldg #1'");
		verifyTrue(transactionPage.isUnitNumberSavedSuccessfully("Loc #3, Bldg #1"));

		log.info("VP: Value of APN  field is '81'");
		verifyTrue(transactionPage.isAPNSavedSuccessfully("81"));

		log.info("VP: Value of Seller  field is 'D & B Development, LLC'");
		verifyTrue(transactionPage.isSellerSavedSuccessfully("D & B Development, LLC"));

		log.info("VP: Value of Address  field is '1020 Bonforte Blvd'");
		verifyTrue(transactionPage.isAddressSavedSuccessfully("1020 Bonforte Blvd"));

		log.info("VP: Value of City  field is 'Pueblo'");
		verifyTrue(transactionPage.isCitySavedSuccessfully("Pueblo"));

		log.info("VP: Value of State field is 'CO - Colorado'");
		verifyTrue(transactionPage.isStateSavedSuccessfully("CO - Colorado"));

		log.info("VP: Value of Zip Code field is '81001'");
		verifyTrue(transactionPage.isZipCodeSavedSuccessfully("81001"));

		log.info("VP: Value of County  field is 'Pueblo'");
		verifyTrue(transactionPage.isCountySavedSuccessfully("Pueblo"));

		log.info("VP: Value of Open Date  field is '10/07/2015'");
		verifyTrue(transactionPage.isOpenDateSavedSuccessfully("10/07/2015"));

		log.info("VP: Value of Acquisition Price  field is '$5,600,000.00'");
		verifyTrue(transactionPage.isAcquisitionPriceSavedSuccessfully("$5,600,000.00"));

		log.info("VP: Value of Escrow #  field is '81'");
		verifyTrue(transactionPage.isEscrowSavedSuccessfully("81"));

		log.info("VP: Value of Land Sq. Ft.  field is '81'");
		verifyTrue(transactionPage.isLandSqFtSavedSuccessfully("81"));

		log.info("VP: Value of Building Sq. Ft  field is '801'");
		verifyTrue(transactionPage.isBuildingSqFtSavedSuccessfully("801"));
	}

	@Test(groups = { "regression" }, description = "New Property 02 - Update Property (Load Property by excel file)")
	public void NewProperty_02_UpdatePropertyByExcelTemplate() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("NewProperty_02 - Step 01: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("NewProperty_02 - Step 02: Click 'Add Properties' button");
		transactionPage.clickAddPropertyButton();

		log.info("NewProperty_02 - Step 03: Load 'Property Excel Template Updated' file");
		transactionPage.uploadFileProperties(propertiesFileNameUpdated);

		log.info("NewProperty_02 - Step 04. Search with P# is 'P0001686'");
		transactionPage.searchPropertyPNumber(pNumber);

		log.info("VP: Information of property updated successfully");
		verifyTrue(transactionPage.isPropertyInfoCorrectly("P0001686", "1020 Bonforte Blvd Updated", "California", "CA", "80921"));

		log.info("NewProperty_02 - Step 05. Open a property detail");
		transactionPage.openPropertyDetail(addressUpdated);

		log.info("VP: Value of P# field is 'P0001686'");
		pNumber = transactionPage.getValuePNumber();
		verifyEquals(pNumber, "P0001686");

		log.info("VP: Value of Unit Number field is 'Loc #15, Bldg #1'");
		verifyTrue(transactionPage.isUnitNumberSavedSuccessfully("Loc #15, Bldg #1"));

		log.info("VP: Value of APN  field is '84'");
		verifyTrue(transactionPage.isAPNSavedSuccessfully("84"));

		log.info("VP: Value of Seller  field is 'Monument Farm and Ranch, LLC'");
		verifyTrue(transactionPage.isSellerSavedSuccessfully("Monument Farm and Ranch, LLC"));

		log.info("VP: Value of Address  field is '1020 Bonforte Blvd Updated'");
		verifyTrue(transactionPage.isAddressSavedSuccessfully("1020 Bonforte Blvd Updated"));

		log.info("VP: Value of City  field is 'California'");
		verifyTrue(transactionPage.isCitySavedSuccessfully("California"));

		log.info("VP: Value of State field is 'Ca - California'");
		verifyTrue(transactionPage.isStateSavedSuccessfully("CA - California"));

		log.info("VP: Value of Zip Code field is '80921'");
		verifyTrue(transactionPage.isZipCodeSavedSuccessfully("80921"));

		log.info("VP: Value of County  field is 'El Paso'");
		verifyTrue(transactionPage.isCountySavedSuccessfully("El Paso"));

		log.info("VP: Value of Open Date  field is '10/10/2015'");
		verifyTrue(transactionPage.isOpenDateSavedSuccessfully("10/10/2015"));

		log.info("VP: Value of Acquisition Price  field is '$5,040,000.00'");
		verifyTrue(transactionPage.isAcquisitionPriceSavedSuccessfully("$5,040,000.00"));

		log.info("VP: Value of Escrow #  field is '84'");
		verifyTrue(transactionPage.isEscrowSavedSuccessfully("84"));

		log.info("VP: Value of Land Sq. Ft.  field is '84'");
		verifyTrue(transactionPage.isLandSqFtSavedSuccessfully("84"));

		log.info("VP: Value of Building Sq. Ft  field is '807'");
		verifyTrue(transactionPage.isBuildingSqFtSavedSuccessfully("807"));
	}

	@Test(groups = { "regression" }, description = "New Property 03 - Add new Property (Enter required data into screen)")
	public void NewProperty_03_EnterRequiredDataIntoScreen() {

		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);

		log.info("NewProperty_03 - Step 01: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("NewProperty_03 - Step 02: Click 'New' Property button");
		transactionPage.clickNewTransactionButton();

		log.info("NewProperty_03 - Step 03: Input the valid data to 'Unit Number' field");
		transactionPage.inputUnitNumber("Loc #3, Bldg #1");

		log.info("NewProperty_03 - Step 04: Input the valid data to 'APN' field");
		transactionPage.inputAPN("81");

		log.info("NewProperty_03 - Step 05: Input the valid data to 'Seller' field");
		transactionPage.inputSeller("D & B Development, LLC");

		log.info("NewProperty_03 - Step 06: Input the valid data to 'Address' field");
		transactionPage.inputAddress("New Property Address");

		log.info("NewProperty_03 - Step 07: Input the valid data to 'City' field");
		transactionPage.inputCity("Virginia");

		log.info("NewProperty_03 - Step 08: Input the valid data to 'State' field");
		transactionPage.selectState("WV - West Virginia");

		log.info("NewProperty_03 - Step 09: Input the valid data to 'Zip Code' field");
		transactionPage.inputZipCode("80982");

		log.info("NewProperty_03 - Step 10: Input the valid data to 'County' field");
		transactionPage.inputCounty("Virginia");

		log.info("NewProperty_03 - Step 11: Input the valid data to 'Open Date' field");
		transactionPage.inputOpenDate("10/07/2015");

		log.info("NewProperty_03 - Step 12: Input the valid data to 'Acquisition Price' field");
		transactionPage.inputAcquisitionPrice("$5,600,000.00");

		log.info("NewProperty_03 - Step 13: Input the valid data to 'Escrow' field");
		transactionPage.inputEscrow("82");

		log.info("NewProperty_03 - Step 14: Input the valid data to 'Land Sq. Ft.' field");
		transactionPage.inputLandSqFt("0.15");

		log.info("NewProperty_03 - Step 15: Input the valid data to 'Building Sq. Ft.' field");
		transactionPage.inputBuildingSqFt("0.21");

		log.info("NewProperty_03 - Step 16: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("VP: Verify that 'Unit Number' is saved successfully");
		verifyTrue(transactionPage.isUnitNumberSavedSuccessfully("Loc #3, Bldg #1"));

		log.info("VP: Verify that 'APN' is saved successfully");
		verifyTrue(transactionPage.isAPNSavedSuccessfully("81"));

		log.info("VP: Verify that 'Seller' is saved successfully");
		verifyTrue(transactionPage.isSellerSavedSuccessfully("D & B Development, LLC"));

		log.info("VP: Verify that 'Address' is saved successfully");
		verifyTrue(transactionPage.isAddressSavedSuccessfully("New Property Address"));

		log.info("VP: Verify that 'City' is saved successfully");
		verifyTrue(transactionPage.isCitySavedSuccessfully("Virginia"));

		log.info("VP: Verify that 'State' is saved successfully");
		verifyTrue(transactionPage.isStateSavedSuccessfully("WV - West Virginia"));

		log.info("VP: Verify that 'Zip Code' is saved successfully");
		verifyTrue(transactionPage.isZipCodeSavedSuccessfully("80982"));

		log.info("VP: Verify that 'County' is saved successfully");
		verifyTrue(transactionPage.isCountySavedSuccessfully("Virginia"));

		log.info("VP: Verify that 'Open Date' is saved successfully");
		verifyTrue(transactionPage.isOpenDateSavedSuccessfully("10/07/2015"));

		log.info("VP: Verify that 'Acquisition Price' is saved successfully");
		verifyTrue(transactionPage.isAcquisitionPriceSavedSuccessfully("$5,600,000.00"));

		log.info("VP: Verify that 'Escrow' is saved successfully");
		verifyTrue(transactionPage.isEscrowSavedSuccessfully("82"));

		log.info("VP: Verify that 'Land Sq. Ft.' is saved successfully");
		verifyTrue(transactionPage.isLandSqFtSavedSuccessfully("0.15"));

		log.info("VP: Verify that 'Building Sq. Ft.' is saved successfully");
		verifyTrue(transactionPage.isBuildingSqFtSavedSuccessfully("0.21"));

		log.info("NewProperty_03 - Step 16: Go to List of Properties page");
		transactionPage.clickGotoPropertiesButtonAtPropertyDetails();

		log.info("NewProperty_03 - Step 17. Search with Address name is 'New Property Address'");
		transactionPage.searchPropertyAddress("New Property Address");

		log.info("VP: Information of property saved successfully");
		verifyTrue(transactionPage.isPropertyInfoCorrectly("1", "New Property Address", "Virginia", "WV", "80982"));
	}

	@Test(groups = { "regression" }, description = "New Property 04 - Verify the Property for the Transaction is increased by one")
	public void NewProperty_04_PropertyOfTransactionIncreasedByOne() {
		
		log.info("NewProperty_04 - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("NewProperty_04 - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("VP: Property for the Transaction is increased by one");
		propertyNumberProtab = propertyNumberProtab + 1;
		verifyTrue(transactionPage.isPropertyNumberDisplayOnSearchTransactionPage(companyName, propertyNumberProtab + ""));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName, propertiesFileName, address, addressUpdated;
	private int numberOfProperty, propertyNumberProtab;
	private String pNumber, propertiesFileNameUpdated;
}