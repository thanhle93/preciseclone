package page;

import org.openqa.selenium.WebDriver;
import PreciseRes.Interfaces;

public class DocumentSecurityPage extends AbstractPage{
	public DocumentSecurityPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
	}
	
	/**
	 * open Document Security Configuration By Index
	 */
	public void openDocumentSecurityConfigurationByIndex(int index) {
		waitForControl(driver, Interfaces.DocumentSecurityPage.DYNAMIC_DOC_SECURITY_NAME, index+"", timeWait);
		click(driver, Interfaces.DocumentSecurityPage.DYNAMIC_DOC_SECURITY_NAME, index+"");
		sleep();
	}
	
	/**
	 * delete All Document Security Item
	 */
	public void deleteAllDocumentSecurityItem() {
		String currentUrl = getCurrentUrl(driver);
		int elementCount=0;
		try {
			elementCount = countElement(driver, Interfaces.DocumentSecurityPage.ALL_DOC_SECURITY_DELETE_CHECKBOX);
			for (int i = 1; i <= elementCount; i++) {
				click(driver, Interfaces.DocumentSecurityPage.DYNAMIC_DOC_SECURITY_DELETE_CHECKBOX, i+"");
				sleep(1);
			}
			clickSaveDocSecurityConfigButton();
		} catch (Exception e) {
			System.out.print(e.getMessage());
			openLink(driver, currentUrl);
		}
	}
	
	/**
	 * type Doc Security Config Name
	 */
	public void typeDocSecurityConfigName(String docName) {
		waitForControl(driver, Interfaces.DocumentSecurityPage.DOCUMENT_SECURITY_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.DocumentSecurityPage.DOCUMENT_SECURITY_NAME_TEXTBOX, docName);
		sleep(1);
	}
	
	/**
	 * type Doc Security Config Description
	 */
	public void typeDocSecurityConfigDescription(String docDescription) {
		waitForControl(driver, Interfaces.DocumentSecurityPage.DOCUMENT_SECURITY_DES_TEXTBOX, timeWait);
		type(driver, Interfaces.DocumentSecurityPage.DOCUMENT_SECURITY_DES_TEXTBOX, docDescription);
		sleep(1);
	}
	
	/**
	 * select Document Type
	 */
	public void selectDocumentType(String docType, int index) {
		waitForControl(driver, Interfaces.DocumentSecurityPage.DYNAMIC_DOCUMENT_TYPE_COMBOBOX, index+"", timeWait);
		selectItemCombobox(driver, Interfaces.DocumentSecurityPage.DYNAMIC_DOCUMENT_TYPE_COMBOBOX, index+"", docType);
		sleep(1);
	}
	
	/**
	 * select Title
	 */
	public void selectTitle(String title, int index) {
		waitForControl(driver, Interfaces.DocumentSecurityPage.DYNAMIC_TITLE_COMBOBOX, index+"", timeWait);
		selectItemCombobox(driver, Interfaces.DocumentSecurityPage.DYNAMIC_TITLE_COMBOBOX, index+"", title);
		sleep(1);
	}
	
	/**
	 * select Type Of Access
	 */
	public void selectTypeOfAccess(String typeOfAccess, int index) {
		waitForControl(driver, 
				Interfaces.DocumentSecurityPage.DYNAMIC_TYPE_OF_ACCESS_COMBOBOX, index+"", timeWait);
		selectItemCombobox(driver, 
				Interfaces.DocumentSecurityPage.DYNAMIC_TYPE_OF_ACCESS_COMBOBOX, index+"", typeOfAccess);
		sleep(1);
	}
	
	/**
	 * click Save Doc Security ConfigButton
	 */
	public void clickSaveDocSecurityConfigButton() {
		waitForControl(driver, Interfaces.DocumentSecurityPage.SAVE_DOCUMENT_SECURITY_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		sleep();
	}
	
	/**
	 * check Doc Security Config Name Correct
	 */
	public boolean isDocSecurityConfigNameCorrect(String docName) {
		waitForControl(driver, Interfaces.DocumentSecurityPage.DOCUMENT_SECURITY_NAME_TEXTBOX, timeWait);
		String realName = getAttributeValue(driver,
				Interfaces.DocumentSecurityPage.DOCUMENT_SECURITY_NAME_TEXTBOX, "value");
		return realName.contains(docName);
	}
	
	/**
	 * check Doc Security Config Des Correct
	 */
	public boolean isDocSecurityConfigDesCorrect(String docDescription) {
		waitForControl(driver, Interfaces.DocumentSecurityPage.DOCUMENT_SECURITY_DES_TEXTBOX, timeWait);
		String realName = getAttributeValue(driver,
				Interfaces.DocumentSecurityPage.DOCUMENT_SECURITY_DES_TEXTBOX, "value");
		return realName.contains(docDescription);
	}
	
	/**
	 * check Document Type Correct
	 */
	public boolean isDocumentTypeCorrect(String docType, int index) {
		waitForControl(driver, Interfaces.DocumentSecurityPage.DYNAMIC_DOCUMENT_TYPE_COMBOBOX, index+"", timeWait);
		String realType = getItemSelectedCombobox(driver,
				Interfaces.DocumentSecurityPage.DYNAMIC_DOCUMENT_TYPE_COMBOBOX, index+"");
		return realType.contains(docType);
	}
	
	/**
	 * check Title Correct
	 */
	public boolean isTitleCorrect(String title, int index) {
		waitForControl(driver, Interfaces.DocumentSecurityPage.DYNAMIC_TITLE_COMBOBOX, index+"", timeWait);
		String realTitle = getItemSelectedCombobox(driver,
				Interfaces.DocumentSecurityPage.DYNAMIC_TITLE_COMBOBOX, index+"");
		return realTitle.contains(title);
	}
	
	/**
	 * check Type Of Access Correct
	 */
	public boolean isTypeOfAccessCorrect(String typeOfAccess, int index) {
		waitForControl(driver, Interfaces.DocumentSecurityPage.DYNAMIC_TYPE_OF_ACCESS_COMBOBOX, index+"", timeWait);
		String realTypeAccsess = getItemSelectedCombobox(driver,
				Interfaces.DocumentSecurityPage.DYNAMIC_TYPE_OF_ACCESS_COMBOBOX, index+"");
		return realTypeAccsess.contains(typeOfAccess);
	}
	
	/**
	 * click Back To List Button
	 */
	public void clickBackToListButton() {
		waitForControl(driver, Interfaces.DocumentSecurityPage.BACK_TO_LIST_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('GoToList').click();");
		sleep();
	}
	
	/**
	 * check Doc Security Display in List
	 */
	public boolean isDocSecurityDisplay(String docName, String docDescription, int index) {
		waitForControl(driver, Interfaces.DocumentSecurityPage.DYNAMIC_DOC_SECURITY_NAME, index+"", timeWait);
		String realDocName = getText(driver, Interfaces.DocumentSecurityPage.DYNAMIC_DOC_SECURITY_NAME, index+"");
		String realDocDes = getText(driver, Interfaces.DocumentSecurityPage.DYNAMIC_DOC_SECURITY_DES, index+"");
		return realDocName.contains(docName) && realDocDes.contains(docDescription);
	}
	
	private WebDriver driver;
}