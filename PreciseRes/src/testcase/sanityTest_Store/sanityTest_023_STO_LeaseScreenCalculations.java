package sanityTest_Store;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;

import common.AbstractTest;
import common.Constant;

public class sanityTest_023_STO_LeaseScreenCalculations extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileName = "Transaction Newest.xml";
		companyName = "Testing QA " + getUniqueNumber();
		paymentFrequency = "M";
		increaseType = "CPIL";
		defaultCompany = "STORE Capital Corporation";
		rent = "386761";
	}

	@Test(groups = { "regression" }, description = "LeaseScreenCalculations_01 - Check default value displayed correctly")
	public void LeaseScreenCalculations_01_CheckDefaultValue() {
		
		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);
		
		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("Precondition - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: Renewal Option checkbox is selected");
		verifyTrue(transactionPage.isRenewalOptionSelected());
		
		log.info("VP: Default Portfolio Company displayed correctly");
		verifyTrue(transactionPage.isPortfolioCompanySavedSuccessfully(defaultCompany));
		
		log.info("ValidataDatatypes_02 - Step 01: Open Lessee detail tab");
		transactionPage.openLeaseDetailsTab();
		
		log.info("VP: Verify that default 'Payment Frequency' displayed correctly");
		verifyTrue(transactionPage.isPaymentFrequencySavedSuccessfully(paymentFrequency));
		
		log.info("VP: Verify that default 'Increase Type' displayed correctly");
		verifyTrue(transactionPage.isIncreaseTypeSavedSuccessfully(increaseType));
		
		log.info("VP: Verify that 'TripleNet' is selected");
		verifyTrue(transactionPage.isTripleNetSelected());
	}
	
	@Test(groups = { "regression" }, description = "LeaseScreenCalculations_02 - Check calculation in Basic details page")
	public void LeaseScreenCalculations_02_CheckCalculationBasicDetails() {
		
		log.info("LeaseScreenCalculations_02 - Step 01: Open Basic detail tab");
		transactionPage.openBasicDetailsTab();
		
		//Calculate Initial
		
		log.info("LeaseScreenCalculations_02 - Step 02: Get expected Initial");
		expectedInitial = transactionPage.calculateInitialPayment(rent);
		
		log.info("LeaseScreenCalculations_02 - Step 03: Get actual Initial");
		actualInitial = transactionPage.getInitialRent().replace("$", "").replace(",", "");
		
		log.info("VP: Verify that 'Initial rent' is displayed correctly'");
		verifyEquals(expectedInitial, actualInitial);
		
		//Calculate Prorated Days and 1st Payment Day
		
		log.info("LeaseScreenCalculations_02 - Step 04: Click 'Save' button");
		transactionPage.clickOnDateSelecterIcon();
		
		log.info("LeaseScreenCalculations_02 - Step 05: Get number of days in month and calculate prorated Days");
		numberOfDaysInMonth= transactionPage.getNumberOfDaysInMonth();
		transactionPage.keyPressing("esc");
		expectedProratedDays = transactionPage.calculateProratedDays(numberOfDaysInMonth);
		
		log.info("LeaseScreenCalculations_02 - Step 06: Get actual Prorated Days");
		actualProratedDays = transactionPage.getProratedDays();
		
		log.info("VP: Verify that 'Prorated Days' is displayed correctly'");
		verifyEquals(expectedProratedDays, actualProratedDays);
		
		log.info("LeaseScreenCalculations_02 - Step 07: Get number of days in month and calculate prorated Days");
		expected1stPaymentDay = transactionPage.calculate1stPaymentDay(numberOfDaysInMonth);
		
		log.info("LeaseScreenCalculations_02 - Step 08: Get actual Prorated Days");
		actual1stPaymentDay = transactionPage.get1stPaymentDay();
		
		log.info("VP: Verify that 'Prorated Days' is displayed correctly'");
		verifyEquals(expected1stPaymentDay, actual1stPaymentDay);
		
		// Check Unit Sales Only Required status
		log.info("LeaseScreenCalculations_02 - Step 09: Select 'Unit Sales Only Required' checkbox");
		transactionPage.selectUnitSalesOnlyRequired();
		
		log.info("VP: Verify that 'Unit Sales Only Required' is selected");
		verifyTrue(transactionPage.isUnitSalesOnlySelected());
		
		log.info("LeaseScreenCalculations_02 - Step 10: Select 'Unit Financial Statements Required' checkbox");
		transactionPage.selectUnitFinancialStatementsRequired();
		
		log.info("VP: Verify that 'Unit Financial Statements Required' is selected");
		verifyTrue(transactionPage.isUnitFinancialStatementsRequiredSelected());
		
		log.info("VP: Verify that 'Unit Sales Only Required' is selected");
		verifyFalse(transactionPage.isUnitSalesOnlySelected());
		
		//Calculate Lease Expiration Date
		
		log.info("VP: Verify that 'Lease Expiration Date' displayed correctly");
		verifyTrue(transactionPage.isLeaseExpirationDateSavedSuccessfully(transactionPage.calculateLeaseExpirationDate(numberOfDaysInMonth)));
		
		//Calculate Earliest Burnoff Date 
		
		log.info("LeaseScreenCalculations_02 - Step 02: Get expected Initial");
		expectedEarliestBurnoffDate = transactionPage.calculateEarliestBurnoffDate();
		
		log.info("LeaseScreenCalculations_02 - Step 03: Get actual Initial");
		actualEarliestBurnoffDate = transactionPage.getEarliestBurnoffDate();
		
		log.info("VP: Verify that 'Initial rent' is displayed correctly'");
		verifyEquals(expectedEarliestBurnoffDate, actualEarliestBurnoffDate);
		
		//Calculate Next Increase Date
		
		log.info("LeaseScreenCalculations_02 - Step 11: Get contract Commencement Date");
		contractCommencementDate= transactionPage.getContractCommencementDate();
		
		log.info("LeaseScreenCalculations_02 - Step 12: Open Lessee detail tab");
		transactionPage.openLeaseDetailsTab();
		
		log.info("VP: Verify that 'Next Increase date' displayed correctly");
		verifyTrue(transactionPage.isNextIncreaseDateSavedSuccessfully(transactionPage.calculateNextIncreaseDate(contractCommencementDate)));
	}
	
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName;
	private String paymentFrequency, increaseType;
	private String defaultCompany, rent;
	private String expectedInitial, actualInitial, expected1stPaymentDay, actual1stPaymentDay, contractCommencementDate, expectedEarliestBurnoffDate, actualEarliestBurnoffDate ;
	private int expectedProratedDays, actualProratedDays, numberOfDaysInMonth;
	}