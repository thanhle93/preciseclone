package page;

import org.openqa.selenium.WebDriver;

import PreciseRes.Interfaces;

public class SalesForceContactsPage extends AbstractPage {

	public SalesForceContactsPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}
	
	/**
	 * type Last name
	 * @param lastName
	 */
	public void typeLastName(String lastName) {
		waitForControl(driver, Interfaces.SalesForceContactsPage.LASTNAME_TEXTBOX, timeWait);
		type(driver,Interfaces.SalesForceContactsPage.LASTNAME_TEXTBOX, lastName);
	}
	
	/**
	 * type Inbound Date
	 * @param inboundDate
	 */
	public void typeInboundDate(String inboundDate) {
		waitForControl(driver, Interfaces.SalesForceContactsPage.INBOUND_DATE_TEXTBOX, timeWait);
		type(driver,Interfaces.SalesForceContactsPage.INBOUND_DATE_TEXTBOX, inboundDate);
	}
	
	/**
	 * type Email
	 * @param email
	 */
	public void typeEmail(String email) {
		waitForControl(driver, Interfaces.SalesForceContactsPage.EMAIL_TEXTBOX, timeWait);
		type(driver,Interfaces.SalesForceContactsPage.EMAIL_TEXTBOX, email);
	}
	
	/**
	 * type Introduction Source
	 * @param email
	 */
	public void typeIntroductionSource(String introductionSource) {
		waitForControl(driver, Interfaces.SalesForceContactsPage.INTRODUCTION_SOURCE_DROPDOWN, timeWait);
		type(driver,Interfaces.SalesForceContactsPage.INTRODUCTION_SOURCE_DROPDOWN, introductionSource);
	}
	
	/**
	 * type Fax
	 * @param fax
	 */
	public void typeFax(String fax) {
		waitForControl(driver, Interfaces.SalesForceContactsPage.FAX_TEXTBOX, timeWait);
		type(driver,Interfaces.SalesForceContactsPage.FAX_TEXTBOX, fax);
	}
	
	/**
	 * click Save button
	 */
	public void clickSaveButton() {
		waitForControl(driver,Interfaces.SalesForceContactsPage.SAVE_BUTTON, timeWait);
		click(driver, Interfaces.SalesForceContactsPage.SAVE_BUTTON);
		sleep();
	}
	
	/**
	 * check Last Name saved successfully
	 * @param lastName
	 */
	public boolean isLastNameSavedSuccessfully(String lastName) {
		waitForControl(driver, Interfaces.SalesForceContactsPage.DYNAMIC_LAST_NAME_TITLE, lastName, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceContactsPage.DYNAMIC_LAST_NAME_TITLE, lastName);
	}
	
	/**
	 * check Inbound Date saved successfully
	 * @param inboundDate
	 */
	public boolean isInboundDateSavedSuccessfully(String inboundDate) {
		waitForControl(driver, Interfaces.SalesForceContactsPage.DYNAMIC_INBOUND_DATE_TITLE, inboundDate, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceContactsPage.DYNAMIC_INBOUND_DATE_TITLE, inboundDate);
	}
	
	/**
	 * check Email saved successfully
	 * @param email
	 */
	public boolean isEmailSavedSuccessfully(String email) {
		waitForControl(driver, Interfaces.SalesForceContactsPage.DYNAMIC_EMAIL_TITLE, email, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceContactsPage.DYNAMIC_EMAIL_TITLE, email);
	}
	
	/**
	 * check Fax saved successfully
	 * @param fax
	 */
	public boolean isFaxSavedSuccessfully(String fax) {
		waitForControl(driver, Interfaces.SalesForceContactsPage.DYNAMIC_FAX_TITLE, fax, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceContactsPage.DYNAMIC_FAX_TITLE, fax);
	}
	
	/**
	 * click New Deal button
	 */
	public SalesForceDealsPage clickNewDealButton() {
		waitForControl(driver,Interfaces.SalesForceContactsPage.NEW_DEAL_BUTTON, timeWait);
		click(driver, Interfaces.SalesForceContactsPage.NEW_DEAL_BUTTON);
		sleep();
		return PageFactory.getSalesforceDealsPage(driver, ipClient);
	}
	
	/**
	 * open Deal Name
	 */
	public SalesForceDealsPage openDealsName(String dealsName) {
		waitForControl(driver,Interfaces.SalesForceContactsPage.DYNAMIC_DEAL_NAME_TITLE, dealsName, timeWait);
		click(driver, Interfaces.SalesForceContactsPage.DYNAMIC_DEAL_NAME_TITLE, dealsName);
		sleep();
		return PageFactory.getSalesforceDealsPage(driver, ipClient);
	}
	
	/**
	 * click Edit button
	 */
	public void clickEditButton() {
		waitForControl(driver,Interfaces.SalesForceContactsPage.EDIT_BUTTON,timeWait);
		click(driver, Interfaces.SalesForceContactsPage.EDIT_BUTTON);
		sleep();
	}
	
	private WebDriver driver;
	private String ipClient;
}