package sanityTest_Uploader;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_039_B2R_Member_MoveAndCopyDocuments extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameMember = "auto-B2R-member2014";
		passwordMember = "change";
		loanName = "UploaderUdiTesting"+getUniqueNumber();
		applicantName = "Consolidation Demo Borrower New";
		loanStatus = Constant.LOAN_STATUS_B2R;
		propertiesFileName = "B2R Data Tape.xlsx";
		property1Name = "37412 OAKHILL ST";
		fileNameDocument2 = "Uploader_testing_document.txt";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}
	
	@Test(groups = { "regression" },description = "MoveAndCopyDocuments_01 - Move and Copy Documents from Properties Folder")
	public void MoveAndCopyDocuments_01_MoveAndCopyDocumentsFromPropertiesFolder()
	{
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameMember, passwordMember, false);
		
		log.info("Precondition 02. Create new loans and get loans' id");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus, loanName);
		
		log.info("Precondition 03. Upload data tape");
		loansPage.openPropertiesTab();
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("Precondition 05. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("Precondition 06. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameMember, passwordMember);
		
		log.info("MoveAndCopyDocuments_01 - Step 01. Select loans item");
		uploaderPage.clickChangeGroupedView();
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("MoveAndCopyDocuments_01 - Step 02. Open Property folder");
		uploaderPage.clickPropertiesFolder();
		
		log.info("MoveAndCopyDocuments_01 - Step 03. Open Property item");
		uploaderPage.selectPropertyFolder(property1Name);
		
		log.info("MoveAndCopyDocuments_01 - Step 04. Check all checkbox");
		uploaderPage.checkSelectAllCheckbox();
		
		log.info("VP. Check Move documents button is disabled");
		verifyTrue(uploaderPage.isMoveDocumentsButtonDisabled());
		
		log.info("VP. Check Copy documents button is disabled");
		verifyTrue(uploaderPage.isCopyDocumentsButtonDisabled());
	}
	
	@Test(groups = { "regression" },description = "MoveAndCopyDocuments_01 - Move and Copy Documents from UncategorizedFolder Folder")
	public void MoveAndCopyDocuments_02_MoveAndCopyDocumentsFromUncategorizedFolder()
	{
		log.info("MoveAndCopyDocuments_02 - Step 01. Open Uncategorized Property Documents folder");
		uploaderPage.selectSubFolderLevel2("Uncategorized Property Documents");
		
		log.info("MoveAndCopyDocuments_02 - Step 02. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherDocuments();
		
		log.info("MoveAndCopyDocuments_02 - Step 03. Select file to upload");
		uploaderPage.uploadFileOnUploader(fileNameDocument2);
			
		log.info("MoveAndCopyDocuments_02 - Step 04. Click 'OK' Button");
		uploaderPage.waitForUploadCompleteOnUploader(fileNameDocument2);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(fileNameDocument2));
		
		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument2, "Unknown"));
		
		log.info("MoveAndCopyDocuments_02 - Step 04. Check all checkbox");
		uploaderPage.checkSelectAllCheckbox();
		
		log.info("VP. Check Move documents button is disabled");
		verifyTrue(uploaderPage.isMoveDocumentsButtonDisabled());
		
		log.info("VP. Check Copy documents button is disabled");
		verifyTrue(uploaderPage.isCopyDocumentsButtonDisabled());
	}
	
	@Test(groups = { "regression" },description = "MoveAndCopyDocuments_01 - Copy Documents to Folder")
	public void MoveAndCopyDocuments_03_CopyDocumentsToFolder()
	{
		log.info("MoveAndCopyDocuments_03 - Step 01. Open General Loan Documents ");
		uploaderPage.selectSubFolderLevel2("General Loan Documents");
		
		log.info("MoveAndCopyDocuments_03 - Step 02. Open 'Insurance' documents");
		uploaderPage.selectSubFolderLevel3("Insurance");
		
		log.info("MoveAndCopyDocuments_03 - Step 03. Get number of documents in here");
		numberOfDocumentsExpected = uploaderPage.getNumberOfDocuments();
		
		log.info("MoveAndCopyDocuments_03 - Step 04. Open 'Property Management' documents");
		uploaderPage.selectSubFolderLevel3("Property Management");
		
		log.info("MoveAndCopyDocuments_03 - Step 05. Get number of documents expected");
		numberOfDocumentsExpected+=uploaderPage.getNumberOfDocuments();
		numberOfDocuments = Integer.toString(uploaderPage.getNumberOfDocuments());
		
		log.info("MoveAndCopyDocuments_03 - Step 06. Upload document for first two document types");
		uploaderPage.UploadFileToPropertyByNumbericOrder(1,2);
		
		log.info("MoveAndCopyDocuments_03 - Step 07. Check all checkbox");
		uploaderPage.checkSelectAllCheckbox();
		
		log.info("VP. Check Copy documents button is disabled");
		verifyFalse(uploaderPage.isCopyDocumentsButtonDisabled());
		
		log.info("MoveAndCopyDocuments_03 - Step 08. Click on Copy documents button");
		uploaderPage.clickCopyDocumentsButton();
		
		log.info("MoveAndCopyDocuments_03 - Step 09. Select Insurance folder ");
		uploaderPage.selectFolderForCopyAndMove("Insurance");
		
		log.info("MoveAndCopyDocuments_03 - Step 10. Click on Ok button ");
		uploaderPage.clickOkButtonOnCopyAndMoveDocument();
		
		log.info("MoveAndCopyDocuments_03 - Step 11. Click on Yes button ");
		uploaderPage.clickYesButton();
		
		log.info("VP. Check all documents aren't removed");
		verifyEquals(Integer.toString(uploaderPage.getNumberOfDocuments()), numberOfDocuments);
		
		log.info("VP. Check all documents file isn't removed");
		verifyTrue(uploaderPage.checkDocumentFilesUploadedByNumbericOrder(1,2));
		
		log.info("MoveAndCopyDocuments_03 - Step 12. Open 'Insurance' folder");
		uploaderPage.selectSubFolderLevel3("Insurance");
		
		log.info("VP. Check all documents are copied to here");
		verifyEquals(Integer.toString(uploaderPage.getNumberOfDocuments()), Integer.toString(numberOfDocumentsExpected));
		
		log.info("VP. Check all documents file are copied to here");
		verifyTrue(uploaderPage.checkDocumentFilesUploadedByNumbericOrder(1,2));
	}
	
	@Test(groups = { "regression" },description = "MoveAndCopyDocuments_04 - Move Documents to Folder")
	public void MoveAndCopyDocuments_04_MoveDocumentsToFolder()
	{
		log.info("MoveAndCopyDocuments_04 - Step 01. Open 'Legal Documents' documents");
		uploaderPage.selectSubFolderLevel3("Legal Documents");
		
		log.info("MoveAndCopyDocuments_04 - Step 02. Get Number of documents in here");
		numberOfDocumentsExpected = uploaderPage.getNumberOfDocuments();
		
		log.info("MoveAndCopyDocuments_04 - Step 03. Open 'Property Management' documents");
		uploaderPage.selectSubFolderLevel3("Property Management");
		
		log.info("MoveAndCopyDocuments_04 - Step 04. Get number of documents in here");
		numberOfDocumentsExpected+=uploaderPage.getNumberOfDocuments();
		
		log.info("MoveAndCopyDocuments_04 - Step 05. Check all checkbox");
		uploaderPage.checkSelectAllCheckbox();
		
		log.info("VP. Check Move documents button is disabled");
		verifyFalse(uploaderPage.isMoveDocumentsButtonDisabled());
		
		log.info("MoveAndCopyDocuments_04 - Step 06. Click on Move documents button");
		uploaderPage.clickMoveDocumentsButton();
		
		log.info("MoveAndCopyDocuments_04 - Step 07. Select Legal Documents folder ");
		uploaderPage.selectFolderForCopyAndMove("Legal Documents");
		
		log.info("MoveAndCopyDocuments_04 - Step 08. Click on Ok button ");
		uploaderPage.clickOkButtonOnCopyAndMoveDocument();
		
		log.info("MoveAndCopyDocuments_04 - Step 09. Click on Yes button ");
		uploaderPage.clickYesButton();
		
		log.info("VP. Check all documents are removed");
		verifyEquals(Integer.toString(uploaderPage.getNumberOfDocuments()), "0");
		
		log.info("MoveAndCopyDocuments_04 - Step 10. Open 'Legal Documents' folder");
		uploaderPage.selectSubFolderLevel3("Legal Documents");
		
		log.info("VP. Check all documents are moved to here");
		verifyEquals(Integer.toString(uploaderPage.getNumberOfDocuments()), Integer.toString(numberOfDocumentsExpected));
		
		log.info("VP. Check all documents file are moved to here");
		verifyTrue(uploaderPage.checkDocumentFilesUploadedByNumbericOrder(1,2));
	}
	
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private LoansPage loansPage;
	private String usernameMember, passwordMember, applicantName, loanStatus;
	private String loanName, uploaderPageUrl, propertiesFileName, fileNameDocument2;
	private String property1Name;
	private String numberOfDocuments;
	private int numberOfDocumentsExpected;
}