package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_010_STO_SearchAndActiveInactiveProperty extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		accountNumber = getUniqueNumber();
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + accountNumber;
		propertiesFileName = "StoreDataTape.xlsx";
		address = "1020 Bonforte Blvd";
		pNumber = "P0001686";
		city = "Pueblo";
		state = "CO";
		zipCode = "81001";
		active = "Yes";
		inActive = "No";
		either = "Either";
	}

	@Test(groups = { "regression" }, description = "Search And Active/Inactive 01 - Search by P#")
	public void SearchActiveInactive_01_SearchByPNumber() {
		
		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("Precondition - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));

		log.info("Precondition - Step 07: Open Property tab");
		transactionPage.openPropertyTab();

		log.info("Precondition - Step 08: Click 'Add Properties' button");
		transactionPage.clickAddPropertyButton();

		log.info("Precondition - Step 09: Load 'Property Excel Template' file");
		transactionPage.uploadFileProperties(propertiesFileName);

		log.info("SearchActiveInactive_01 - Step 01. Search with P# is 'P0001686'");
		transactionPage.searchPropertyPNumber(pNumber);
		propertyID = transactionPage.getPropertyIDOfAddressInPropertiesTab(address);
		System.out.println(propertyID);
		
		log.info("VP: The Property with P# display in table");
		verifyTrue(transactionPage.isPropertyDisplayOnSearchPropertiesPage(pNumber, propertyID, address, city, state, zipCode, active));
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 02 - Search by Property ID")
	public void SearchActiveInactive_02_SearchByPropertyID() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("Precondition - Step 04: Open Property tab");
		transactionPage.openPropertyTab();
		
		log.info("SearchActiveInactive_02 - Step 01. Search with Property ID");
		transactionPage.searchPropertyPropertyID(propertyID);
		
		log.info("VP: The Property with Property ID display in table");
		verifyTrue(transactionPage.isPropertyDisplayOnSearchPropertiesPage(pNumber, propertyID, address, city, state, zipCode, active));
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 03 - Search by Address")
	public void SearchActiveInactive_03_SearchByAddress() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("Precondition - Step 04: Open Property tab");
		transactionPage.openPropertyTab();
		
		log.info("SearchActiveInactive_03 - Step 01. Search with Property Address");
		transactionPage.searchPropertyAddress(address);
		
		log.info("VP: The Property with Property Address display in table");
		verifyTrue(transactionPage.isPropertyDisplayOnSearchPropertiesPage(pNumber, propertyID, address, city, state, zipCode, active));
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 04 - Search by City")
	public void SearchActiveInactive_04_SearchByCity() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("Precondition - Step 04: Open Property tab");
		transactionPage.openPropertyTab();
		
		log.info("SearchActiveInactive_04 - Step 01. Search with City");
		transactionPage.searchPropertyCity(city);
		
		log.info("VP: The Property with Property City display in table");
		verifyTrue(transactionPage.isPropertyDisplayOnSearchPropertiesPage(pNumber, propertyID, address, city, state, zipCode, active));
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 05 - Search by State")
	public void SearchActiveInactive_05_SearchByState() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("Precondition - Step 04: Open Property tab");
		transactionPage.openPropertyTab();
		
		log.info("SearchActiveInactive_05 - Step 01. Search with State");
		transactionPage.searchPropertyState("CO - Colorado");
		
		log.info("VP: The Property with Property State display in table");
		verifyTrue(transactionPage.isPropertyDisplayOnSearchPropertiesPage(pNumber, propertyID, address, city, state, zipCode, active));
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 06 - Search by Zip code")
	public void SearchActiveInactive_06_SearchByZipCode() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("Precondition - Step 04: Open Property tab");
		transactionPage.openPropertyTab();
		
		log.info("SearchActiveInactive_06 - Step 01. Search with Zip Code");
		transactionPage.searchPropertyZipCode(zipCode);
		
		log.info("VP: The Property with Property Zip Code display in table");
		verifyTrue(transactionPage.isPropertyDisplayOnSearchPropertiesPage(pNumber, propertyID, address, city, state, zipCode, active));
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 07 - Search by Property active status")
	public void SearchActiveInactive_07_SearchByActiveStatus() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("Precondition - Step 04: Open Property tab");
		transactionPage.openPropertyTab();
		
		log.info("SearchActiveInactive_07 - Step 01: Search with Active status");
		transactionPage.searchByActiveOrInactive(pNumber, active);
		
		log.info("VP: The Property with Property Active status display in table");
		verifyTrue(transactionPage.isPropertyDisplayOnSearchPropertiesPage(pNumber, propertyID, address, city, state, zipCode, active));
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 08 - Search by Property either status")
	public void SearchActiveInactive_08_SearchByEitherStatus() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("Precondition - Step 04: Open Property tab");
		transactionPage.openPropertyTab();
		
		log.info("SearchActiveInactive_08 - Step 01: Search with Either status");
		transactionPage.searchByActiveOrInactive(pNumber, either);
		
		log.info("VP: The Property with Active status display in table");
		verifyTrue(transactionPage.isPropertyDisplayOnSearchPropertiesPage(pNumber, propertyID, address, city, state, zipCode, active));
		
		log.info("SearchActiveInactive_08 - Step 02: Select Property checkbox in List Properties table");
		transactionPage.isSelectedPropertyCheckbox(address);
		
		log.info("SearchActiveInactive_08 - Step 03: Click 'Make Inactive' button");
		transactionPage.clickMakeInactiveButton();
		
		log.info("SearchActiveInactive_08 - Step 04: Search with Either status");
		transactionPage.searchByActiveOrInactive(pNumber, either);
		
		log.info("VP: The Property with Property Inactive status display in table");
		verifyTrue(transactionPage.isPropertyDisplayOnSearchPropertiesPage(pNumber, propertyID, address, city, state, zipCode, inActive));
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 09 - Search by Property inactive status")
	public void SearchActiveInactive_09_SearchByInactiveStatus() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("Precondition - Step 04: Open Property tab");
		transactionPage.openPropertyTab();
		
		log.info("SearchActiveInactive_09 - Step 01: Search with Inactive status");
		transactionPage.searchByActiveOrInactive(pNumber, inActive);
		
		log.info("VP: The Property with Property Inactive status display in table");
		verifyTrue(transactionPage.isPropertyDisplayOnSearchPropertiesPage(pNumber, propertyID, address, city, state, zipCode, inActive));
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 10 - Make a Property is Active")
	public void SearchActiveInactive_10_MakeAPropertyIsActive() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("Precondition - Step 04: Open Property tab");
		transactionPage.openPropertyTab();
		
		log.info("SearchActiveInactive_10 - Step 01: Search Property by Inactive status");
		transactionPage.searchByActiveOrInactive(pNumber, inActive);
		
		log.info("VP: The Property with Property Inactive status display in table");
		verifyTrue(transactionPage.isPropertyDisplayOnSearchPropertiesPage(pNumber, propertyID, address, city, state, zipCode, inActive));
		
		log.info("SearchActiveInactive_10 - Step 02: Select Property checkbox in List Properties table");
		transactionPage.isSelectedPropertyCheckbox(address);
		
		log.info("SearchActiveInactive_10 - Step 03: Click 'Make Active' button");
		transactionPage.clickMakeActiveButtonAtTransactionPropertyTab();
		
		log.info("SearchActiveInactive_10 - Step 04: Search Property by Active status");
		transactionPage.searchByActiveOrInactive(pNumber, active);
		
		log.info("VP: The Property with Property Active status display in table");
		verifyTrue(transactionPage.isPropertyDisplayOnSearchPropertiesPage(pNumber, propertyID, address, city, state, zipCode, active));
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 11 - Make a Property is Inactive")
	public void SearchActiveInactive_11_MakeAPropertyIsInactive() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("Precondition - Step 04: Open Property tab");
		transactionPage.openPropertyTab();
		propertyNumber = transactionPage.getNumberOfPropertyInPropertiesTab();
		
		log.info("SearchActiveInactive_11 - Step 01: Search Property by Active status");
		transactionPage.searchByActiveOrInactive(pNumber, active);
		
		log.info("VP: The Property with Property Active status display in table");
		verifyTrue(transactionPage.isPropertyDisplayOnSearchPropertiesPage(pNumber, propertyID, address, city, state, zipCode, active));
		
		log.info("SearchActiveInactive_11 - Step 02: Select Property checkbox in List Properties table");
		transactionPage.isSelectedPropertyCheckbox(address);
		
		log.info("SearchActiveInactive_11 - Step 03: Click 'Make Inactive' button");
		transactionPage.clickMakeInactiveButton();
		
		log.info("SearchActiveInactive_11 - Step 04: Search Property by Inactive status");
		transactionPage.searchByActiveOrInactive(pNumber, inActive);
		
		log.info("VP: The Property with Property Inactive status display in table");
		verifyTrue(transactionPage.isPropertyDisplayOnSearchPropertiesPage(pNumber, propertyID, address, city, state, zipCode, inActive));
	}
	
	@Test(groups = { "regression" }, description = "Search And Active/Inactive 12 - Verify the Property for the Transaction is decreased by one")
	public void SearchActiveInactive_12_PropertyOfTransactionDecreasedByOne() {
		
		log.info("SearchActiveInactive_12 - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("SearchActiveInactive_12 - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);

		log.info("VP: Property for the Transaction is increased by one");
		propertyNumber = propertyNumber - 1;
		verifyTrue(transactionPage.isPropertyNumberDisplayOnSearchTransactionPage(companyName, propertyNumber + ""));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword, propertiesFileName, address;
	private String xmlFileName, companyName, accountNumber, active, inActive, either;
	private String pNumber, propertyID, city, state, zipCode;
	private int propertyNumber;
}