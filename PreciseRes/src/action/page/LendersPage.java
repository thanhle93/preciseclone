package page;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import PreciseRes.Interfaces;

public class LendersPage extends AbstractPage{
	public LendersPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
	}
	
	/**
	 * search Lender By Name
	 */
	public void searchLenderByName(String lenderName) {
		waitForControl(driver, Interfaces.LendersPage.SEARCH_LENDER_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LendersPage.SEARCH_LENDER_NAME_TEXTBOX, lenderName);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep();
	}
	
	/**
	 * open Lender Detail Page
	 */
	public void openLenderDetailPage(String lenderName) {
		waitForControl(driver, Interfaces.LendersPage.DYNAMIC_LENDER_ITEM_IN_LIST, lenderName, timeWait);
		click(driver, Interfaces.LendersPage.DYNAMIC_LENDER_ITEM_IN_LIST, lenderName);
		sleep();
	}
	
	/**
	 * click New Contact Detail Button
	 */
	public void clickNewContactDetailButton() {
		waitForControl(driver, Interfaces.LendersPage.NEW_CONTACT_DETAIL_BUTTON, timeWait);
		WebElement element = driver.findElement(By.id("LenderDetails_R1_City"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
		sleep();
		click(driver, Interfaces.LendersPage.NEW_CONTACT_DETAIL_BUTTON);
		sleep(2);
		if(driver.toString().toLowerCase().contains("internetexplorer")){
			sleep(6);
		}
	}
	
	/**
	 * input Last Name On Lender Contact Detail
	 */
	public void inputLastNameOnLenderContactDetail(String lastname) {
		waitForControl(driver, Interfaces.LendersPage.CONTACT_LASTNAME_TEXTBOX, timeWait);
		type(driver, Interfaces.LendersPage.CONTACT_LASTNAME_TEXTBOX, lastname);
		sleep(1);
	}
	
	/**
	 * select Login Value On Lender Contact Detail
	 */
	public void selectLoginValueOnLenderContactDetail(String item) {
		waitForControl(driver, Interfaces.LendersPage.CONTACT_LOGIN_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.LendersPage.CONTACT_LOGIN_COMBOBOX, item);
		sleep(1);
	}
	
	/**
	 * click Save Lender Contact Detail
	 */
	public void clickSaveLenderContactDetail() {
		waitForControl(driver, Interfaces.LendersPage.SAVE_LENDER_CONTACT_DETAIL_BUTTON, timeWait);
		click(driver, Interfaces.LendersPage.SAVE_LENDER_CONTACT_DETAIL_BUTTON);
		sleep();
	}

	/**
	 * click Back to Lender
	 */
	public void clickBackToLender() {
		waitForControl(driver, Interfaces.LendersPage.BACK_TO_LENDER_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Back').click();");
		sleep();
	}
	
	/**
	 * click Save Lender Detail
	 */
	public void clickSaveLenderDetail() {
		waitForControl(driver, Interfaces.LendersPage.SAVE_LENDER_DETAIL_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		sleep();
	}
	
	/**
	 * check User Display On Lender Contact Detail Table
	 */
	public boolean isUserDisplayOnLenderContactDetailTable(String lastname) {
		waitForControl(driver, Interfaces.LendersPage.DYNAMIC_LENDER_CONTACT_IN_TABLE, lastname, timeWait);
		return isControlDisplayed(driver, Interfaces.LendersPage.DYNAMIC_LENDER_CONTACT_IN_TABLE, lastname);
	}
	
	/**
	 * switch To Lender Contact Detail Frame
	 */
	public WebDriver switchToLenderContactDetailFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.LendersPage.LENDER_CONTACT_DETAIL_FRAME, timeWait);
		sleep();
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.LendersPage.LENDER_CONTACT_DETAIL_FRAME)));
		return driver;
	}
	
	private WebDriver driver;
}