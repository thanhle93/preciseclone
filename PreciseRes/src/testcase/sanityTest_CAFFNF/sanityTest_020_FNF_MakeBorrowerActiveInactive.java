package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFLoansPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_020_FNF_MakeBorrowerActiveInactive extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		locClosingDate = "04/12/2014";
		loanName = "Edit Loan Auto " + getUniqueNumber();
	}


	@Test(groups = { "regression" }, description = "Applicant - Login as Lender - Make Borrower inactive")
	public void MakeBorrowerActiveInactive_01_Lender_MakeBorrowerInactive() {

		log.info("MakeBorrowerActiveInactive_01 - Step 01. Logout and login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("MakeBorrowerActiveInactive_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("MakeBorrowerActiveInactive_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("MakeBorrowerActiveInactive_01 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("MakeBorrowerActiveInactive_01 - Step 05: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("MakeBorrowerActiveInactive_01 - Step 06: Input loan name");
		loanPage.inputRequiredData(loanName, locClosingDate);
		
		log.info("MakeBorrowerActiveInactive_01 - Step 07: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("MakeBorrowerActiveInactive_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("MakeBorrowerActiveInactive_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.selectActiveRadioButton("");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("MakeBorrowerActiveInactive_01 - Step 03: Click on 'Check all' link");
		applicantsPage.clickOnCheckAllLink();
		
		log.info("MakeBorrowerActiveInactive_01 - Step 03: Click on 'Make Borrower Inactive' button");
		applicantsPage.clickMakeInactiveButton();
		
		log.info("VP: Borrower status set to Inactive");
		verifyTrue(applicantsPage.getRecordValue("Active").contains("No"));
		
		log.info("MakeBorrowerActiveInactive_01 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("VP: Make sure that 'This record is inactive!' is displayed");
		verifyTrue(applicantsPage.isInfoMessageDisplayed("This record is inactive!"));
		
		log.info("VP: Make sure that loan isn't displayed in Loan Opportunity table");
		verifyFalse(applicantsPage.isLoanDisplayedInOpportunityTable(loanName));
		
		log.info("DocumentApproval_01 - Step 01: Login with Applicant");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);
		
		log.info("VP: Make sure that 'This record is inactive!' is displayed");
		verifyTrue(applicantsPage.isInfoMessageDisplayed("This record is inactive!"));
		
		log.info("VP: Make sure that loan isn't displayed in Loan Opportunity table");
		verifyFalse(applicantsPage.isLoanDisplayedInOpportunityTable(loanName));
	}
	
	@Test(groups = { "regression" }, description = "Applicant - Login as Lender - Make Borrower Active")
	public void MakeBorrowerActiveInactive_02_Lender_MakeBorrowerActiveAgain() {

		log.info("DocumentApproval_01 - Step 02. Logout and login with Lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("MakeBorrowerActiveInactive_02 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("MakeBorrowerActiveInactive_02 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.selectActiveRadioButton("");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("MakeBorrowerActiveInactive_02 - Step 03: Click on 'Check all' link");
		applicantsPage.clickOnCheckAllLink();
		
		log.info("MakeBorrowerActiveInactive_02 - Step 03: Click on 'Make Borrower Inactive' button");
		applicantsPage.clickMakeActiveButton();
		
		log.info("VP: Borrower status set to Inactive");
		verifyTrue(applicantsPage.getRecordValue("Active").contains("Yes"));
		
		log.info("MakeBorrowerActiveInactive_02 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("VP: Make sure that 'This record is inactive!' is displayed");
		verifyFalse(applicantsPage.isInfoMessageDisplayed("This record is inactive!"));
		
		log.info("VP: Make sure that loan isn't displayed in Loan Opportunity table");
		verifyTrue(applicantsPage.isLoanDisplayedInOpportunityTable(loanName));
		
		log.info("DocumentApproval_01 - Step 01: Login with Applicant");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);
		
		log.info("VP: Make sure that 'This record is inactive!' is displayed");
		verifyFalse(applicantsPage.isInfoMessageDisplayed("This record is inactive!"));
		
		log.info("VP: Make sure that loan isn't displayed in Loan Opportunity table");
		verifyTrue(applicantsPage.isLoanDisplayedInOpportunityTable(loanName));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFLoansPage loanPage;
	private FNFBorrowersPage applicantsPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername,	FNFBorrowerPassword;
	private String companyName, accountNumber, loanName;
	private String locClosingDate;
	}