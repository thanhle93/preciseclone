package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Constant;
import page.LoginPage;
import page.PageFactory;
import page.ReportsPage;
import page.TransactionPage;

public class sanityTest_017_STO_ReportsTemplateTab extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		csvReportTemplate = "List of Report Templates.csv";
		csvReport = "List of Reports.csv";
		csvGroupReport = "List of Group Reports.csv";
		reportingLevel = "Lease";
		fileNameTemplate = "ReportTemplate.xlsx";
		reportName = "Report Name " + getUniqueNumber();
		fileName = "File Name Template " + getUniqueNumber();
		fileFormat = "xls";
		groupReportName = "Group Report Name " + getUniqueNumber();
	}

	@Test(groups = { "regression" }, description = "Report Page 01 - Add new Report Templates")
	public void ReportPage_01_AddNewReportTemplates() {

		log.info("ReportPage_01 - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("ReportPage_01 - Step 02: Open Reports page");
		reportsPage = transactionPage.openReportsPage(driver, ipClient);

		log.info("ReportPage_01 - Step 03: Open Report Templates tab");
		reportsPage.openReportsTemplateTab();

		log.info("ReportPage_01 - Step 04: Click 'New' Report Templates button");
		reportsPage.clickNewButton();

		log.info("ReportPage_01 - Step 05: Select Reporting Level is Lease");
		reportsPage.selectReportingLevelNewTab(reportingLevel);

		log.info("ReportPage_01 - Step 06: Upload File Template name");
		reportsPage.uploadFileTemplate(fileNameTemplate);

		log.info("ReportPage_01 - Step 07: Click 'Save' button");
		reportsPage.clickSaveButton();

		log.info("ReportPage_01 - Step 08: Click 'Back To List' button");
		reportsPage.clickBackToListButton();

		log.info("ReportPage_01 - Step 09: Select Reporting Level is Lease");
		reportsPage.selectReportingLevel(reportingLevel);

		log.info("ReportPage_01 - Step 10: Click 'Search' button");
		reportsPage.clickSearchButton();

		log.info("VP: Report Templates is created successfully");
		verifyTrue(reportsPage.isGroupAndTemplateReportDisplayonListGroupAndTemplateReports(fileNameTemplate, reportingLevel));
	}

	@Test(groups = { "regression" }, description = "Report Page 02 - Add new Reports")
	public void ReportPage_02_AddNewReports() {

		log.info("ReportPage_02 - Step 01: Open Reports tab");
		reportsPage.openReportsTab();

		log.info("ReportPage_02 - Step 02: Click 'New' Report button");
		reportsPage.clickNewButton();

		log.info("ReportPage_02 - Step 03: Select Reporting Level is Lease");
		reportsPage.selectReportingLevelNewTab(reportingLevel);

		log.info("ReportPage_02 - Step 04: Input data into Report Name textbox");
		reportsPage.inputReportNameAtReportTab(reportName);

		log.info("ReportPage_02 - Step 05: Input data into File Name Template textbox");
		reportsPage.inputFileNameTemplateAtReportTab(fileName);

		log.info("ReportPage_02 - Step 06: Select Reporting Template");
		reportsPage.selectReportingTemplateAtReportTab(fileNameTemplate);

		log.info("ReportPage_02 - Step 07: Select File Format is xls");
		reportsPage.selectFileFormatAtReportTab(fileFormat);

		log.info("ReportPage_02 - Step 08: Click 'Save' button");
		reportsPage.clickSaveButton();

		log.info("ReportPage_02 - Step 09: Click 'Back To List' button");
		reportsPage.clickBackToListButton();

		log.info("ReportPage_02 - Step 10: Input data into Report Name textbox");
		reportsPage.inputReportName(reportName);

		log.info("ReportPage_02 - Step 11: Click 'Search' button");
		reportsPage.clickSearchButton();

		log.info("VP: Report is created successfully");
		verifyTrue(reportsPage.isReportDisplayonListReports(reportName, reportingLevel, fileName));
	}

	@Test(groups = { "regression" }, description = "Report Page 03 - Add new Group Reports")
	public void ReportPage_03_AddNewGroupReports() {

		log.info("ReportPage_03 - Step 01: Open Group Reports tab");
		reportsPage.openGroupReportsTab();

		log.info("ReportPage_03 - Step 02: Click 'New' Group Report button");
		reportsPage.clickNewButton();

//		log.info("ReportPage_03 - Step 03: Select Reporting Level is Lease");
//		reportsPage.selectReportingLevelNewTab(reportingLevel);

		log.info("ReportPage_03 - Step 04: Input data into Group Report Name textbox");
		reportsPage.inputReportNameAtReportTab(groupReportName);

		log.info("ReportPage_03 - Step 05: Select 'Report Name' checkbox");
		reportsPage.selectReportName(reportName);

		log.info("ReportPage_03 - Step 06: Click 'Save' button");
		reportsPage.clickSaveButton();

		log.info("ReportPage_03 - Step 07: Click 'Back To List' button");
		reportsPage.clickBackToListButton();

		log.info("ReportPage_03 - Step 08: Input data into Group Report Name textbox");
		reportsPage.inputReportName(groupReportName);

		log.info("ReportPage_03 - Step 09: Click 'Search' button");
		reportsPage.clickSearchButton();

		log.info("VP: Group Reports is created successfully");
		verifyTrue(reportsPage.isGroupAndTemplateReportDisplayonListGroupAndTemplateReports(groupReportName, ""));

	}

	@Test(groups = { "regression" }, description = "Report Page 04 - Search for Report Templates by (Level, File name template)")
	public void ReportPage_04_SearchReportTemplates() {

		log.info("ReportPage_04 - Step 01: Open Reports page");
		reportsPage.openReportsPage(driver, ipClient);

		log.info("ReportPage_04 - Step 02: Open Report Templates tab");
		reportsPage.openReportsTemplateTab();

		log.info("ReportPage_04 - Step 03: Input data into File Template textbox");
		reportsPage.inputFileTemplate(fileNameTemplate);

		log.info("ReportPage_04 - Step 04: Click 'Search' button");
		reportsPage.clickSearchButton();

		log.info("VP: Report Templates is displayed correctly");
		verifyTrue(reportsPage.isGroupAndTemplateReportDisplayonListGroupAndTemplateReports(fileNameTemplate, reportingLevel));

		log.info("ReportPage_04 - Step 04: Select Reporting Level is Lease");
		reportsPage.selectReportingLevel(reportingLevel);

		log.info("ReportPage_04 - Step 05: Click 'Search' button");
		reportsPage.clickSearchButton();

		log.info("VP: Report Templates is displayed correctly");
		verifyTrue(reportsPage.isGroupAndTemplateReportDisplayonListGroupAndTemplateReports(fileNameTemplate, reportingLevel));
	}

	@Test(groups = { "regression" }, description = "Report Page 05 - Search for Reports by (Name, Level, File name template)")
	public void ReportPage_05_SearchForReports() {

		log.info("ReportPage_05 - Step 01: Open Reports page");
		reportsPage.openReportsPage(driver, ipClient);

		log.info("ReportPage_05 - Step 02: Input data into Report Name textbox");
		reportsPage.inputReportName(reportName);

		log.info("ReportPage_05 - Step 03: Click 'Search' button");
		reportsPage.clickSearchButton();

		log.info("VP: Report is displayed correctly");
		verifyTrue(reportsPage.isReportDisplayonListReports(reportName, reportingLevel, fileName));

		log.info("ReportPage_05 - Step 04: Select Reporting Level is Lease");
		reportsPage.selectReportingLevel(reportingLevel);

		log.info("ReportPage_05 - Step 05: Click 'Search' button");
		reportsPage.clickSearchButton();

		log.info("VP: Report is displayed correctly");
		verifyTrue(reportsPage.isReportDisplayonListReports(reportName, reportingLevel, fileName));

		log.info("ReportPage_05 - Step 06: Input data into File Name Template textbox");
		reportsPage.inputFileNameTemplate(fileName);

		log.info("ReportPage_05 - Step 07: Click 'Search' button");
		reportsPage.clickSearchButton();

		log.info("VP: Report is displayed correctly");
		verifyTrue(reportsPage.isReportDisplayonListReports(reportName, reportingLevel, fileName));
	}

	@Test(groups = { "regression" }, description = "Report Page 06 - Search for Group Reports by (Name, Level)")
	public void ReportPage_06_SearchForGroupReports() {

		log.info("ReportPage_06 - Step 01: Open Reports page");
		reportsPage.openReportsPage(driver, ipClient);

		log.info("ReportPage_06 - Step 02: Open Group Reports tab");
		reportsPage.openGroupReportsTab();

		log.info("ReportPage_06 - Step 03: Input data into Group Report Name textbox");
		reportsPage.inputReportName(groupReportName);

		log.info("ReportPage_06 - Step 04: Click 'Search' button");
		reportsPage.clickSearchButton();

		log.info("VP: Group Report is displayed correctly");
		verifyTrue(reportsPage.isGroupAndTemplateReportDisplayonListGroupAndTemplateReports(groupReportName, ""));

//		log.info("ReportPage_06 - Step 04: Select Reporting Level is Lease");
//		reportsPage.selectReportingLevel(reportingLevel);
//
//		log.info("ReportPage_06 - Step 05: Click 'Search' button");
//		reportsPage.clickSearchButton();
//
//		log.info("VP: Group Report is displayed correctly");
//		verifyTrue(reportsPage.isGroupAndTemplateReportDisplayonListGroupAndTemplateReports(groupReportName, reportingLevel));
	}

	@Test(groups = { "regression" }, description = "Report Page 07 - Delete new Group Reports")
	public void ReportPage_07_DeleteNewGroupReports() {

		log.info("ReportPage_07 - Step 01: Open Reports page");
		reportsPage.openReportsPage(driver, ipClient);

		log.info("ReportPage_07 - Step 02: Open Group Reports tab");
		reportsPage.openGroupReportsTab();

		log.info("ReportPage_07 - Step 03: Input data into Group Report Name textbox");
		reportsPage.inputReportName(groupReportName);

		log.info("ReportPage_07 - Step 04: Click 'Search' button");
		reportsPage.clickSearchButton();

		log.info("VP: Group Report is displayed correctly");
		verifyTrue(reportsPage.isGroupAndTemplateReportDisplayonListGroupAndTemplateReports(groupReportName, ""));

		log.info("ReportPage_07 - Step 05: Select Group Report checkbox in List Group Reports table");
		reportsPage.isSelectedReportCheckbox(groupReportName);

		log.info("ReportPage_07 - Step 06: Click 'Delete' button");
		reportsPage.clickDeleteButton();

		log.info("VP: 'No Group Report found.' message is displayed");
		verifyTrue(reportsPage.isMessageTitleDisplayInTable("No Group Report  found."));

		log.info("VP: Group Report is deleted successfully");
		verifyFalse(reportsPage.isGroupAndTemplateReportDisplayonListGroupAndTemplateReports(groupReportName, ""));
	}

	@Test(groups = { "regression" }, description = "Report Page 08 - Delete new Reports")
	public void ReportPage_08_DeleteNewReports() {

		log.info("ReportPage_08 - Step 01: Open Reports page");
		reportsPage.openReportsPage(driver, ipClient);

		log.info("ReportPage_08 - Step 02: Input data into Report Name textbox");
		reportsPage.inputReportName(reportName);

		log.info("ReportPage_08 - Step 03: Click 'Search' button");
		reportsPage.clickSearchButton();

		log.info("VP: Report is displayed correctly");
		verifyTrue(reportsPage.isReportDisplayonListReports(reportName, reportingLevel, fileName));

		log.info("ReportPage_08 - Step 04: Select Report checkbox in List Reports table");
		reportsPage.isSelectedReportCheckbox(reportName);

		log.info("ReportPage_08 - Step 05: Click 'Delete' button");
		reportsPage.clickDeleteButton();

		log.info("VP: 'No Report found.' message is displayed");
		verifyTrue(reportsPage.isMessageTitleDisplayInTable("No Report  found."));

		log.info("VP: Report is deleted successfully");
		verifyFalse(reportsPage.isReportDisplayonListReports(reportName, reportingLevel, fileName));
	}

	@Test(groups = { "regression" }, description = "Report Page 09 - Delete new Report Templates")
	public void ReportPage_09_DeleteNewReportTemplates() {

		log.info("ReportPage_09 - Step 01: Open Reports page");
		reportsPage.openReportsPage(driver, ipClient);

		log.info("ReportPage_09 - Step 02: Open Report Templates tab");
		reportsPage.openReportsTemplateTab();

		log.info("ReportPage_09 - Step 03: Input data into File Template textbox");
		reportsPage.inputFileTemplate(fileNameTemplate);

		log.info("ReportPage_09 - Step 04: Click 'Search' button");
		reportsPage.clickSearchButton();

		log.info("VP: Report Templates is displayed correctly");
		verifyTrue(reportsPage.isGroupAndTemplateReportDisplayonListGroupAndTemplateReports(fileNameTemplate, reportingLevel));

		log.info("ReportPage_09 - Step 05: Select Report Templates checkbox in List Report Templates table");
		reportsPage.isSelectedReportCheckbox(fileNameTemplate);

		log.info("ReportPage_09 - Step 06: Click 'Delete' button");
		reportsPage.clickDeleteButton();

		log.info("VP: 'No Report Template  found.' message is displayed");
		verifyTrue(reportsPage.isMessageTitleDisplayInTable("No Report Template  found."));

		log.info("VP: Report Templates is deleted successfully");
		verifyFalse(reportsPage.isGroupAndTemplateReportDisplayonListGroupAndTemplateReports(fileNameTemplate, reportingLevel));
	}

	@Test(groups = { "regression" }, description = "Report Page 10 - Download list of Group Reports (Excel file that is created)")
	public void ReportPage_10_DownloadListGroupReports() {

		log.info("ReportPage_10 - Step 01: Open Reports page");
		reportsPage.openReportsPage(driver, ipClient);

		log.info("ReportPage_10 - Step 02: Open Group Reports tab");
		reportsPage.openGroupReportsTab();

		log.info("ReportPage_10 - Step 03: Click 'Download CSV' image");
		reportsPage.deleteAllFileInFolder();
		reportsPage.clickOnDownloadCSVImage();

		log.info("ReportPage_10 - Step 04. Wait for file downloaded complete");
		reportsPage.waitForDownloadFileFullnameCompleted(csvGroupReport);

		log.info("VP: File number in folder equal 1");
		countFile = reportsPage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("ReportPage_10 - Step 05. Delete the downloaded file");
		reportsPage.deleteContainsFileName(csvGroupReport);
	}

	@Test(groups = { "regression" }, description = "Report Page 11 - Download list of Reports (Excel file that is created)")
	public void ReportPage_11_DownloadListReports() {

		log.info("ReportPage_11 - Step 01: Open Reports page");
		reportsPage.openReportsPage(driver, ipClient);

		log.info("ReportPage_11 - Step 02: Click 'Download CSV' image");
		reportsPage.deleteAllFileInFolder();
		reportsPage.clickOnDownloadCSVImage();

		log.info("ReportPage_11 - Step 03. Wait for file downloaded complete");
		reportsPage.waitForDownloadFileFullnameCompleted(csvReport);

		log.info("VP: File number in folder equal 1");
		countFile = reportsPage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("ReportPage_11 - Step 04. Delete the downloaded file");
		reportsPage.deleteContainsFileName(csvReport);
	}

	@Test(groups = { "regression" }, description = "Report Page 12 - Download list of Report Templates (Excel file that is created)")
	public void ReportPage_12_DownloadListReportTemplates() {

		log.info("ReportPage_12 - Step 01: Open Reports page");
		reportsPage.openReportsPage(driver, ipClient);

		log.info("ReportPage_12 - Step 02: Open Report Templates tab");
		reportsPage.openReportsTemplateTab();

		log.info("ReportPage_12 - Step 03: Click 'Download CSV' image");
		reportsPage.deleteAllFileInFolder();
		reportsPage.clickOnDownloadCSVImage();

		log.info("ReportPage_12 - Step 04. Wait for file downloaded complete");
		reportsPage.waitForDownloadFileFullnameCompleted(csvReportTemplate);

		log.info("VP: File number in folder equal 1");
		countFile = reportsPage.countFilesInDirectory();
		verifyEquals(countFile, 1);

		log.info("ReportPage_12 - Step 05. Delete the downloaded file");
		reportsPage.deleteContainsFileName(csvReportTemplate);
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private int countFile;
	private WebDriver driver;
	private LoginPage loginPage;
	private ReportsPage reportsPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String reportingLevel, csvGroupReport,csvReport, csvReportTemplate,  fileNameTemplate;
	private String reportName, fileName, fileFormat, groupReportName;
}