package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.DocumentsPage;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_011_B2R_DocumentDeleted extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		applicantUsername = Constant.USERNAME_B2R_BORROWER;
		applicantPassword = Constant.PASSWORD_B2R_BORROWER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower";
		borrowerName = "UdiTeamBorrower";
		propertiesFileName = "B2R Required.xlsx";
		address1 = "37412 OAKHILL ST";
		documentType1 = "P2 - Lease Agreement";
		documentType2 = "P3 - Purchase Contract/HUD-1 Settlement Stmt";
		documentFileName = "datatest.pdf";
		documentSectionSearch = "Legal Documents";
	}

	@Test(groups = { "regression" }, description = "Document01 - Add Public Document For Property By clicking New Button")
	public void DocumentTest_01_AddPublicDocumentForPropertyByNewButtonLender() {
		log.info("DocumentTest_01 - Step 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("DocumentTest_01 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_01 - Step 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, borrowerName, loanStatus, loanName);
		
		log.info("DocumentTest_01 - Step 04. Open 'Properties' tab");
		loansPage.openPropertiesTab();

		log.info("DocumentTest_01 - Step 05. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);
		
		log.info("DocumentTest_01 - Step 06. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("DocumentTest_01 - Step 07. Add document through the 'New' button");
		propertiesPage.clickNewDocumentButton();
		propertiesPage.selectDocumentType(documentType1);
		propertiesPage.uploadDocumentFile(documentFileName);
		propertiesPage.uncheckPrivateCheckbox();
		propertiesPage.clickSaveButton();
		propertiesPage.clickGoToPropertyButton();
		
		log.info("VP: Document is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, documentFileName));
		loansPage = propertiesPage.clickOnPropertyListButton();

		log.info("DocumentTest_01 - Step 08. Get 'Property' number");
		documentNumberProtab = loansPage.getNumberDocumentsRequiredOfPropertyInPropertiesTab();

		log.info("DocumentTest_01 - Step 09. Open 'General Loan Documents' page");
		documentsPage = loansPage.openGeneralDocumentTab();

		log.info("DocumentTest_01 - Step 10. Get 'General Loan Documents' number");
		documentNumberGeneraltab = documentsPage.getNumberOfPropertyInPropertiesTab();
		
		log.info("DocumentTest_01 - Step 11. Click 'Section' search");
		documentsPage.searchSectionAndDocumentType(documentSectionSearch, "");

		log.info("DocumentTest_01 - Step 12. Get 'General Loan Documents' number with Section is 'Legal Documents'");
		documentNumberGeneraltab1 = documentsPage.getNumberOfPropertyInPropertiesTab();
		
		log.info("DocumentTest_01 - Step 13. Open 'Corporate Entity Documents' page");
		documentsPage.openCorporateEntityDocumentsTab();
		
		log.info("DocumentTest_01 - Step 14. Get 'Corporate Entity Documents' number");
		documentNumberCorporatetab = documentsPage.getNumberOfPropertyInPropertiesTab();
		sumDocument = documentNumberProtab + documentNumberGeneraltab + documentNumberCorporatetab - documentNumberGeneraltab1;
	}

	@Test(groups = { "regression" }, description = "Document02 - Add Private Document For Property By clicking New Button")
	public void DocumentTest_02_AddPrivateDocumentForPropertyByNewButtonLender() {
		
		log.info("DocumentTest_02 - Step 01. Open Loans tab");
		loansPage = documentsPage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_02 - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_02 - Step 03. Open detail loan item");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_02 - Step 04. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("DocumentTest_02 - Step 05. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("DocumentTest_02 - Step 06. Click New document button");
		propertiesPage.clickNewDocumentButton();

		log.info("DocumentTest_02 - Step 07. Select document type");
		propertiesPage.selectDocumentType(documentType2);

		log.info("DocumentTest_02 - Step 08. Check private checkbox");
		propertiesPage.checkPrivateCheckbox();

		log.info("DocumentTest_02 - Step 09. Upload document file");
		propertiesPage.uploadDocumentFile(documentFileName);

		log.info("DocumentTest_02 - Step 10. Click save button");
		propertiesPage.clickSaveButton();

		log.info("DocumentTest_02 - Step 11. Click Go To Property button");
		propertiesPage.clickGoToPropertyButton();

		log.info("VP: Document is loaded to document type");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType2, documentFileName));
	}

	@Test(groups = { "regression" }, description = "Document04 - Verify the required documents for the property is increased")
	public void DocumentTest_03_VerifyTheRequiredDocumentsForPropertyIncreased() {

		log.info("DocumentTest_03 - Step 01. Open Loans tab");
		loansPage = propertiesPage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_03 - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_03 - Step 03. Open detail loan item");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_03 - Step 04. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("VP: Verify the required documents for the property is increased");
		verifyTrue(loansPage.isDocumentLoadedToDocumentColumn(address1, "2 of"));
	}

	@Test(groups = { "regression" }, description = "Document07 - Make sure that you can see the document")
	public void DocumentTest_04_MakeSureLenderCanSeeTheDocumentLender() {

		log.info("DocumentTest_04 - Step 01. Open Loans tab");
		loansPage.openLoansPage(driver, ipClient);
				
		log.info("DocumentTest_04 - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_04 - Step 03. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_04 - Step 04. Open property tab");
		loansPage.openPropertiesTab();

		log.info("DocumentTest_04 - Step 05. Open property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("VP: Lender can see the public document from Lender");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, documentFileName));
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType2, documentFileName));
	}

	@Test(groups = { "regression" }, description = "Document09 - Login as borrower and make sure he can't see the document")
	public void DocumentTest_05_MakeSureBorrowerCanNotSeeTheDocumentLender() {

		log.info("DocumentTest_05 - Step 01. Log out and login with applicant");
		loginPage = logout(driver, ipClient);
		loansPage = loginPage.loginAsBorrower(applicantUsername, applicantPassword, false);

		log.info("DocumentTest_05 - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_05 - Step 03. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_05 - Step 04. Open property tab");
		loansPage.openPropertiesTab();

		log.info("DocumentTest_05 - Step 05. Open property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("VP: Borrower cannot see the private document from Lender");
		verifyTrue(propertiesPage.isDocumentLoadedToDocumentType(documentType1, documentFileName));
		verifyFalse(propertiesPage.isDocumentLoadedToDocumentType(documentType2, documentFileName));
	}

	@Test(groups = { "regression" }, description = "Document08 - Make sure the document is included in the document count")
	public void DocumentTest_06_AllDocumentsIncludedInLenderDocumentCountLender() {

		log.info("DocumentTest_06 - Step 01. Login with lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameLender, passwordLender,false);

		log.info("DocumentTest_06 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_06 - Step 03. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_06 - Step 04. Open detail loan item");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_06 - Step 05. Open Property Documents tab");
		loansPage.openPropertyDocumentsTab();

		log.info("DocumentTest_06 - Step 06. Get number of total document");
		numberOfDocument = loansPage.getNumberOfDocumentInPropertyDocumentsTab();

		log.info("VP: Make sure document is included in the document count");
		verifyEquals(numberOfDocument, 2);
	}

	@Test(groups = { "regression" }, description = "Document10 - Make sure the private document is not included in the document count")
	public void DocumentTest_07_PrivateDocumentNotIncludedInBorrowerDocumentCountBorrower() {

		log.info("DocumentTest_07 - Step 01. Login with borrower");
		loginPage = logout(driver, ipClient);
		loansPage = loginPage.loginAsBorrower(applicantUsername,	applicantPassword, false);

		log.info("DocumentTest_07 - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_07 - Step 03. Open loan detail");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_07 - Step 04. Open Property Documents tab");
		loansPage.openPropertyDocumentsTab();

		log.info("DocumentTest_07 - Step 05. Get number of total document");
		numberOfDocument = loansPage.getNumberOfDocumentInPropertyDocumentsTab();

		log.info("VP: Make sure private document is not included in the borrower document count");
		verifyEquals(numberOfDocument, 1);
	}

	@Test(groups = { "regression" }, description = "Document05 - Delete a document throught the webapp")
	public void DocumentTest_08_DeleteDocumentThroughtWebAppLender() {
		
		log.info("DocumentTest_08 - Step 01. Login with lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameLender, passwordLender,	false);
		
		log.info("DocumentTest_08 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_08 - Step 03. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_08 - Step 04. Open detail loan item");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_08 - Step 05. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("DocumentTest_08 - Step 06. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);

		log.info("DocumentTest_08 - Step 07. Delete a document");
		propertiesPage.deletePropertyDocument(documentType1, documentFileName);

		log.info("VP: Lender cannot see the document after deleting");
		verifyFalse(propertiesPage.isDocumentLoadedToDocumentType(documentType1, documentFileName));
	}

	@Test(groups = { "regression" }, description = "Document 34 - Verify that document updated in the document count in the Dashboard")
	public void DocumentTest_09_VerifyDocumentUpdatedInDocumentCountInDashboard() {
		log.info("DocumentTest_09 - Step 01. Open Homepage");
		homePage = propertiesPage.openHomePage(driver, ipClient);

		log.info("VP: The value in 'Required Docs' number on dashboard is decreased by 1");
		verifyTrue(homePage.isNumberRequiredDocsOfLoanDisplayCorrect(loanName, sumDocument + ""));
	}
	
	@Test(groups = { "regression" }, description = "Document 35 - Verify that document updated in the Property list screen")
	public void DocumentTest_10_VerifyDocumentUpdatedInPropertyListScreen() {
		
		log.info("DocumentTest_10 - Step 01. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("DocumentTest_10 - Step 02. Search loan item");
		loansPage.searchLoanByName(loanName);

		log.info("DocumentTest_10 - Step 03. Open detail loan item");
		loansPage.openLoansDetailPage(loanName);

		log.info("DocumentTest_10 - Step 04. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("VP: Verify the documents for the property list is decreased by 1");
		verifyTrue(loansPage.isDocumentLoadedToDocumentColumn(address1, "1 of"));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private DocumentsPage documentsPage;
	private String usernameLender, passwordLender, address1, documentType1,documentFileName, documentSectionSearch;
	private String loanName, accountNumber, applicantName, borrowerName, propertiesFileName,loanStatus;
	private String applicantUsername, applicantPassword, documentType2;
	private int numberOfDocument;
	private int documentNumberProtab, documentNumberGeneraltab, documentNumberGeneraltab1, documentNumberCorporatetab, sumDocument;
}