package page;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import PreciseRes.Interfaces;
import common.AutomationControl;
import common.Common;
import common.Constant;

public class AbstractPage {

	/**
	 * Refresh a page
	 * 
	 * @param driver
	 */
	public void refresh(WebDriver driver) {
		driver.navigate().refresh();
		sleep();
	}

	/**
	 * Back to page
	 * 
	 * @param driver
	 */
	public void back(WebDriver driver) {
		driver.navigate().back();
	}

	/**
	 * Get title of page
	 * 
	 * @param driver
	 * @return
	 */
	public String getPageTitle(WebDriver driver) {
		return driver.getTitle();
	}
	
	public boolean isAlertPresent(WebDriver driver) 
	{ 
	    try 
	    { 
	        driver.switchTo().alert(); 
	        return true; 
	    }   // try 
	    catch (Exception Ex) 
	    { 
	        return false; 
	    }   // catch 
	}  

	/**
	 * Accept Javascript alert
	 * 
	 * @param driver
	 */
	public void acceptJavascriptAlert(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		alert.accept();
		sleep(2);
	}

	/**
	 * Dismiss Javascript alert
	 * 
	 * @param driver
	 */
	public void dismissJavascriptAlert(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
	}

	/**
	 * Get text of Javascript alert
	 * 
	 * @param driver
	 */
	public String getTextJavascriptAlert(WebDriver driver) {
		sleep(10);
		String message;
		try {
			Alert alert = driver.switchTo().alert();
			message = alert.getText();
			alert.accept();
		} catch (final WebDriverException e) {
			message = null;
		}
		return message;
	}

	/**
	 * Input text into Javascript prompt
	 * 
	 * @param driver
	 */
	public void acceptJavascriptPrompt(WebDriver driver, String value) {
		Alert alert = driver.switchTo().alert();
		driver.switchTo().alert().sendKeys(value);
		alert.accept();
	}

	/**
	 * Check control displayed
	 * 
	 * @param by:
	 *            id, name, xpath...
	 * @return
	 */
	public boolean isControlDisplayed(WebDriver driver, String controlName) {
		try {
			element = control.findElement(driver, controlName);
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * check control is displayed
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @return
	 */
	public boolean isControlDisplayed(WebDriver driver, String controlName, String value) {
		try {
			element = control.findElement(driver, controlName, value);
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * check control is displayed
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @return
	 */
	public boolean isControlDisplayed(WebDriver driver, String controlName, String value1, float value2) {
		try {
			element = control.findElement(driver, controlName, value1, value2);
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * check control is displayed
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @return
	 */
	public boolean isControlDisplayed(WebDriver driver, String controlName, String value1, String value2) {
		try {
			element = control.findElement(driver, controlName, value1, value2);
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * check control is displayed
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @return
	 */
	public boolean isControlDisplayed(WebDriver driver, String controlName, String value1, String value2, String value3) {
		try {
			element = control.findElement(driver, controlName, value1, value2, value3);
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * check control is displayed
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @return
	 */
	public boolean isControlDisplayed(WebDriver driver, String controlName, String value1, String value2, String value3, String value4) {
		try {
			element = control.findElement(driver, controlName, value1, value2, value3, value4);
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * check control is displayed
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @return
	 */
	public boolean isControlDisplayed(WebDriver driver, String controlName, String value1, String value2, String value3, String value4, String value5) {
		try {
			element = control.findElement(driver, controlName, value1, value2, value3, value4, value5);
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * check control is displayed
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @return
	 */
	public boolean isControlDisplayed(WebDriver driver, String controlName, String value1, String value2, String value3, String value4, String value5, String value6) {
		try {
			element = control.findElement(driver, controlName, value1, value2, value3, value4, value5, value6);
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * check control is displayed
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @return
	 */
	public boolean isControlDisplayed(WebDriver driver, String controlName, String value1, String value2, String value3, String value4, String value5, String value6,
			String value7) {
		try {
			element = control.findElement(driver, controlName, value1, value2, value3, value4, value5, value6, value7);
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * check control is displayed
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @return
	 */
	public boolean isControlDisplayed(WebDriver driver, String controlName, String value1, String value2, String value3, String value4, String value5, String value6, String value7,
			String value8, String value9, String value10, String value11) {
		try {
			element = control.findElement(driver, controlName, value1, value2, value3, value4, value5, value6, value7, value8, value9, value10, value11);
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * check control is displayed
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @return
	 */
	public boolean isControlDisplayed(WebDriver driver, String controlName, String value1, String value2, String value3, String value4, String value5, String value6, String value7,
			String value8, String value9, String value10, String value11, String value12) {
		try {
			element = control.findElement(driver, controlName, value1, value2, value3, value4, value5, value6, value7, value8, value9, value10, value11, value12);
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * check control is displayed
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @return
	 */
	public boolean isControlDisplayed(WebDriver driver, String controlName, String value1, String value2, String value3, String value4, String value5, String value6, String value7,
			String value8, String value9, String value10, String value11, String value12, String value13) {
		try {
			element = control.findElement(driver, controlName, value1, value2, value3, value4, value5, value6, value7, value8, value9, value10, value11, value12, value13);
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	

	/**
	 * check control is selected
	 * 
	 * @param driver
	 * @param controlName
	 * @return
	 */
	public boolean isControlSelected(WebDriver driver, String controlName) {
		try {
			element = control.findElement(driver, controlName);
			return element.isSelected();
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * check control is selected
	 * 
	 * @param driver
	 * @param controlName
	 * @return
	 */
	public boolean isControlSelected(WebDriver driver, String controlName, String value) {
		try {
			element = control.findElement(driver, controlName, value);
			return element.isSelected();
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Check control enabled
	 * 
	 * @param controlName
	 * @return
	 */
	public boolean isControlEnabled(WebDriver driver, String controlName) {
		try {
			element = control.findElement(driver, controlName);
			return element.isEnabled();
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Wait for control is displayed
	 * 
	 * @param By
	 *            :id,name,xpath...
	 * @param timeout
	 */
	public void waitForControl(WebDriver driver, final String controlName, long timeout) {
		try {
			By by = control.getBy(driver, controlName);
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			log.debug("Element doesn't exist");
		}
	}

	/**
	 * Wait for control is displayed
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @param timeout
	 */
	public void waitForControl(WebDriver driver, final String controlName, final String value, long timeout) {
		try {
			By by = control.getBy(driver, controlName, value);
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			log.debug("Element doesn't exist");
		}
	}

	/**
	 * Wait for control is displayed
	 * 
	 * @param driver
	 * @param controlName
	 * @param value1
	 * @param value2
	 * @param timeout
	 */
	public void waitForControl(WebDriver driver, final String controlName, final String value1, final String value2, long timeout) {
		try {
			By by = control.getBy(driver, controlName, value1, value2);
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			log.debug("Element doesn't exist");
		}
	}

	/**
	 * Wait for control is displayed
	 * 
	 * @param driver
	 * @param controlName
	 * @param value1
	 * @param value2
	 * @param timeout
	 */
	public void waitForControl(WebDriver driver, final String controlName, final String value1, final String value2, final String value3, long timeout) {
		try {
			By by = control.getBy(driver, controlName, value1, value2, value3);
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			log.debug("Element doesn't exist");
		}
	}

	/**
	 * Wait for control is displayed
	 */
	public void waitForControl(WebDriver driver, final String controlName, final String value1, final String value2, final String value3, final String value4, long timeout) {
		try {
			By by = control.getBy(driver, controlName, value1, value2, value3, value4);
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			log.debug("Element doesn't exist");
		}
	}

	/**
	 * Wait for control is displayed
	 */
	public void waitForControl(WebDriver driver, final String controlName, final String value1, final String value2, final String value3, final String value4, final String value5,
			long timeout) {
		try {
			By by = control.getBy(driver, controlName, value1, value2, value3, value4, value5);
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			log.debug("Element doesn't exist");
		}
	}

	/**
	 * Wait for control is displayed
	 */
	public void waitForControl(WebDriver driver, final String controlName, final String value1, final String value2, final String value3, final String value4, final String value5,
			final String value6, long timeout) {
		try {
			By by = control.getBy(driver, controlName, value1, value2, value3, value4, value5, value6);
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			log.debug("Element doesn't exist");
		}
	}

	/**
	 * Wait for control is displayed
	 */
	public void waitForControl(WebDriver driver, final String controlName, final String value1, final String value2, final String value3, final String value4, final String value5,
			final String value6, final String value7, long timeout) {
		try {
			By by = control.getBy(driver, controlName, value1, value2, value3, value4, value5, value6, value7);
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			log.debug("Element doesn't exist");
		}
	}

	/**
	 * Wait for control is displayed
	 */
	public void waitForControl(WebDriver driver, final String controlName, final String value1, final String value2, final String value3, final String value4, final String value5,
			final String value6, final String value7, final String value8, final String value9, final String value10, final String value11, long timeout) {
		try {
			By by = control.getBy(driver, controlName, value1, value2, value3, value4, value5, value6, value7, value8, value9, value10, value11);
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			log.debug("Element doesn't exist");
		}
	}
	
	/**
	 * Wait for control is displayed
	 */
	public void waitForControl(WebDriver driver, final String controlName, final String value1, final String value2, final String value3, final String value4, final String value5,
			final String value6, final String value7, final String value8, final String value9, final String value10, final String value11, final String value12, long timeout) {
		try {
			By by = control.getBy(driver, controlName, value1, value2, value3, value4, value5, value6, value7, value8, value9, value10, value11, value12);
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			log.debug("Element doesn't exist");
		}
	}
	
	/**
	 * Wait for control is displayed
	 */
	public void waitForControl(WebDriver driver, final String controlName, final String value1, final String value2, final String value3, final String value4, final String value5,
			final String value6, final String value7, final String value8, final String value9, final String value10, final String value11, final String value12, final String value13, long timeout) {
		try {
			By by = control.getBy(driver, controlName, value1, value2, value3, value4, value5, value6, value7, value8, value9, value10, value11, value12, value13);
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			log.debug("Element doesn't exist");
		}
	}

	/**
	 * Wait for control is closed
	 * 
	 * @param by
	 * @param timeout
	 */
	public void waitForControlNotDisplayed(WebDriver driver, final String controlName, long timeout) {
		try {
			By by = control.getBy(driver, controlName);
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
		} catch (Exception e) {
			log.debug("Control does not close");
		}
	}

	/**
	 * Wait for control is closed
	 * 
	 * @param by
	 * @param timeout
	 */
	public void waitForControlNotDisplayed(WebDriver driver, final String controlName, final String value, long timeout) {
		try {
			By by = control.getBy(driver, controlName, value);
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
		} catch (Exception e) {
			log.debug("Control does not close");
		}
	}

	/**
	 * Close dialog download because web driver does not capture or use hot key
	 * 
	 * @param keyEvent
	 */
	public void keyPressing(String keyEvent) {
		try {
			Robot robot = new Robot();
			if (keyEvent == "esc") {
				robot.keyPress(KeyEvent.VK_ESCAPE);
				robot.keyRelease(KeyEvent.VK_ESCAPE);
				sleep();
			}
			if (keyEvent == "enter") {
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				sleep(5);
			}
			if (keyEvent == "tab") {
				robot.keyPress(KeyEvent.VK_TAB);
				robot.keyRelease(KeyEvent.VK_TAB);
			}
			if (keyEvent == "space") {
				robot.keyPress(KeyEvent.VK_SPACE);
				robot.keyRelease(KeyEvent.VK_SPACE);
			}
			if (keyEvent == "alt_s") {
				robot.keyPress(KeyEvent.VK_ALT);
				robot.keyPress(KeyEvent.VK_S);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
			}
			if (keyEvent == "window_q") {
				robot.keyPress(KeyEvent.VK_WINDOWS);
				robot.keyPress(KeyEvent.VK_Q);
				robot.keyRelease(KeyEvent.VK_WINDOWS);
				robot.keyRelease(KeyEvent.VK_Q);
			}
		} catch (AWTException e) {
			log.debug("Can not use Key");
		}
	}

	/**
	 * get attribute of element
	 * 
	 * @param controlName
	 * @param attribute
	 * @return
	 */
	public String getAttributeValue(WebDriver driver, String controlName, String attribute) {
		waitForControl(driver, controlName, timeWait);
		element = control.findElement(driver, controlName);
		return element.getAttribute(attribute);
	}

	/**
	 * get attribute of element
	 * 
	 * @param controlName
	 * @param attribute
	 * @return value
	 */
	public String getAttributeValue(WebDriver driver, String controlName, String value, String attribute) {
		waitForControl(driver, controlName, value, timeWait);
		element = control.findElement(driver, controlName, value);
		return element.getAttribute(attribute);
	}

	/**
	 * get attribute of element
	 * 
	 * @param controlName
	 * @param attribute
	 * @return value1, value2
	 */
	public String getAttributeValue(WebDriver driver, String controlName, String value1, String value2, String attribute) {
		waitForControl(driver, controlName, value1, value2, timeWait);
		element = control.findElement(driver, controlName, value1, value2);
		return element.getAttribute(attribute);
	}

	/**
	 * click on element
	 * 
	 * @param driver
	 * @param controlName
	 */
	public void click(WebDriver driver, String controlName) {
		waitForControl(driver, controlName, timeWait);
		element = control.findElement(driver, controlName);
		element.click();
	}

	/**
	 * doubleClick on element
	 * 
	 * @param driver
	 * @param controlName
	 */
	public void doubleClick(WebDriver driver, String controlName) {
		waitForControl(driver, controlName, timeWait);
		Actions action = new Actions(driver);
		element = control.findElement(driver, controlName);
		action.doubleClick(element).perform();
	}

	/**
	 * doubleClick on element
	 * 
	 * @param driver
	 * @param controlName
	 */
	public void doubleClick(WebDriver driver, String controlName, String fileName) {
		waitForControl(driver, controlName, fileName, timeWait);
		Actions action = new Actions(driver);
		element = control.findElement(driver, controlName, fileName);
		action.doubleClick(element).perform();
	}

	/**
	 * clear text of control
	 * 
	 * @param driver
	 * @param controlName
	 */
	public void clear(WebDriver driver, String controlName) {
		waitForControl(driver, controlName, timeWait);
		element = control.findElement(driver, controlName);
		element.clear();
	}

	/**
	 * click on element
	 * 
	 * @param driver
	 * @param controlName
	 */
	public void click(WebDriver driver, String controlName, String value1, String value2) {
		waitForControl(driver, controlName, value1, value2, timeWait);
		element = control.findElement(driver, controlName, value1, value2);
		element.click();
	}

	public void click(WebDriver driver, String controlName, String value1, String value2, String value3, String value4) {
		waitForControl(driver, controlName, value1, value2, value3, value4, timeWait);
		element = control.findElement(driver, controlName, value1, value2, value3, value4);
		element.click();
	}

	public void click(WebDriver driver, String controlName, String value1, String value2, String value3, String value4, String value5) {
		waitForControl(driver, controlName, value1, value2, value3, value4, value5, timeWait);
		element = control.findElement(driver, controlName, value1, value2, value3, value4, value5);
		element.click();
	}

	/**
	 * enter value for element
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 */
	public void type(WebDriver driver, String controlName, String value) {
		waitForControl(driver, controlName, timeWait);
		element = control.findElement(driver, controlName);
		element.clear();
		element.sendKeys("\u0008" + value);
	}
	
	/**
	 * enter value for element
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 */
	public void type(WebDriver driver, String controlName, String value, String valueinput) {
		waitForControl(driver, controlName, value, timeWait);
		element = control.findElement(driver, controlName, value);
		sleep(1);
		element.clear();
		sleep(1);
		element.sendKeys("\u0008" +valueinput);
	}

	/**
	 * get element
	 * 
	 * @param driver
	 * @param controlName
	 */
	public WebElement getElement(WebDriver driver, String controlName) {
		element = control.findElement(driver, controlName);
		return element;
	}

	/**
	 * get a list element
	 * 
	 * @param driver
	 * @param controlName
	 */
	public List<WebElement> getElements(WebDriver driver, String controlName) {
		return control.findElements(driver, controlName);
	}

	/**
	 * get a list elements
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 */
	public List<WebElement> getElements(WebDriver driver, String controlName, String value) {
		return control.findElements(driver, controlName, value);
	}

	/**
	 * get a list elements
	 * 
	 * @param driver
	 * @param controlName
	 * @param value1
	 * @param value2
	 */
	public List<WebElement> getElements(WebDriver driver, String controlName, String value1, String value2) {
		return control.findElements(driver, controlName, value1, value2);
	}

	/**
	 * get element
	 * 
	 * @param driver
	 * @param controlName
	 */
	public WebElement getElement(WebDriver driver, String controlName, String value) {
		element = control.findElement(driver, controlName, value);
		return element;
	}

	/**
	 * get element
	 * 
	 * @param driver
	 * @param controlName
	 */
	public WebElement getElement(WebDriver driver, String controlName, String value1, String value2) {
		element = control.findElement(driver, controlName, value1, value2);
		return element;
	}

	/**
	 * press enter
	 * 
	 * @param driver
	 * @param controlName
	 */
	public void pressEnter(WebDriver driver, String controlName, String value) {
		WebElement element = control.findElement(driver, controlName, value);
		element.sendKeys(Keys.ENTER);
	}

	/**
	 * get text of element
	 * 
	 * @param driver
	 * @param controlName
	 */
	public String getText(WebDriver driver, String controlName) {
		try {
			waitForControl(driver, controlName, timeWait);
			WebElement element = control.findElement(driver, controlName);
			return element.getText();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * get text of element
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @return text
	 */
	public String getText(WebDriver driver, String controlName, String value) {
		try {
			waitForControl(driver, controlName, value, timeWait);
			WebElement element = control.findElement(driver, controlName, value);
			return element.getText();
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * get text of element
	 * 
	 * @param driver
	 * @param controlName
	 * @param value1
	 * @return text
	 */
	public String getText(WebDriver driver, String controlName, String value1, String value2) {
		try {
			waitForControl(driver, controlName, value1, timeWait);
			WebElement element = control.findElement(driver, controlName, value1, value2);
			return element.getText();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * open link
	 * 
	 * @param driver
	 * @param url
	 */
	public void openLink(WebDriver driver, String url) {
		driver.get(url);
		sleep(3); //wait for link load successfully
		if(isAlertPresent(driver)) acceptJavascriptAlert(driver);
	}

	/**
	 * open Mail link
	 * 
	 * @param driver
	 * @param url
	 * @return
	 */
	public MailLoginPage openMailLink(WebDriver driver, String url) {
		driver.get(url);
		sleep(3);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		return PageFactory.getMailLoginPage(driver, url);
	}

	/**
	 * open Salesforce link
	 * 
	 * @param driver
	 * @param url
	 * @return
	 */
	public SalesForceLoginPage openSalesForceLink(WebDriver driver, String url) {
		driver.get(url);
		sleep(3);
		return PageFactory.getSalesforceLoginPage(driver, url);
	}

	/**
	 * get control href
	 * 
	 * @param driver
	 * @param controlName
	 * @return href
	 */
	public String getControlHref(WebDriver driver, String controlName) {
		return getAttributeValue(driver, controlName, "href");
	}

	/**
	 * get control href
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @return href
	 */
	public String getControlHref(WebDriver driver, String controlName, String value) {
		waitForControl(driver, controlName, value, timeWait);
		element = control.findElement(driver, controlName, value);
		return element.getAttribute("href");
	}

	/**
	 * count element
	 * 
	 * @param driver
	 * @param controlName
	 * @return number of element
	 */
	public int countElement(WebDriver driver, String controlName) {
		waitForControl(driver, controlName, timeWait);
		return control.findElements(driver, controlName).size();
	}

	/**
	 * count element
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @return number of element
	 */
	public int countElement(WebDriver driver, String controlName, String value) {
		waitForControl(driver, controlName, timeWait);
		return control.findElements(driver, controlName, value).size();
	}

	/**
	 * get CSS value of element
	 * 
	 * @param driver
	 * @param controlName
	 * @param css
	 * @return css value
	 */
	public String getCssValue(WebDriver driver, String controlName, String css) {
		element = control.findElement(driver, controlName);
		return element.getCssValue(css);
	}

	/**
	 * get CSS value of element
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @param css
	 * @return css value
	 */
	public String getCssValue(WebDriver driver, String controlName, String value, String css) {
		waitForControl(driver, controlName, value, timeWait);
		element = control.findElement(driver, controlName, value);
		return element.getCssValue(css);
	}

	protected AbstractPage() {
		log = LogFactory.getLog(getClass());
		log.debug("Created page abstraction for " + getClass().getName());
	}

	/**
	 * sleep
	 * 
	 * @param timeout
	 */
	public void sleep(long timeout) {
		try {
			Thread.sleep(timeout * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * sleep
	 */
	public void sleep() {
		sleep(timeSleep);
	}

	/**
	 * move mouse to element
	 * 
	 * @param driver
	 * @param controlName
	 */
	public void moveMouseToElement(WebDriver driver, String controlName) {
		waitForControl(driver, controlName, timeWait);
		Actions action = new Actions(driver);
		action.moveToElement(getElement(driver, controlName), 156, 20);
		action.build().perform();
	}

	/**
	 * move mouse to element
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 */
	public void moveMouseToElement(WebDriver driver, String controlName, String value) {
		waitForControl(driver, controlName, value, timeWait);
		Actions action = new Actions(driver);
		action.moveToElement(getElement(driver, controlName, value));
		action.perform();
	}

	/**
	 * move mouse to element
	 * 
	 * @param driver
	 * @param controlName
	 * @param value1
	 * @param value2
	 */
	public void moveMouseToElement(WebDriver driver, String controlName, String value1, String value2) {
		waitForControl(driver, controlName, value1, value2, timeWait);
		Actions action = new Actions(driver);
		action.moveToElement(getElement(driver, controlName, value1, value2));
		action.perform();
	}

	/**
	 * close browser
	 * 
	 * @param driver
	 */
	public void closeBrowser(WebDriver driver) {
		driver.quit();
	}

	/**
	 * execute javascript
	 * 
	 * @param driver
	 * @param javaSript
	 */
	public Object executeJavaScript(WebDriver driver, String javaSript) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript(javaSript);
		} catch (Exception e) {
			log.debug(e.getMessage());
			return null;
		}
	}

	/**
	 * get current url of page
	 * 
	 * @param driver
	 * @return
	 */
	public String getCurrentUrl(WebDriver driver) {
		return driver.getCurrentUrl();
	}

	/**
	 * scroll page down
	 * 
	 * @param driver
	 * @author test
	 */
	public void scrollPage(WebDriver driver) {
		executeJavaScript(driver, "window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
	}

	/**
	 * scroll page down
	 * 
	 * @param driver
	 * @param x
	 * @param y
	 */
	public void scrollPage(WebDriver driver, int x, int y) {
		executeJavaScript(driver, "window.scrollTo(" + x + "," + y + ");");
		sleep();
	}

	/**
	 * select item in Combobox
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @param item
	 */
	public void selectItemCombobox(WebDriver driver, String controlName, String value, String item) {
		element = control.findElement(driver, controlName, value);
		Select select = new Select(element);
		select.selectByVisibleText(item);
	}

	/**
	 * select item in Combobox
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @param item
	 */
	public void selectItemCombobox(WebDriver driver, String controlName, String item) {
		element = control.findElement(driver, controlName);
		Select select = new Select(element);
		select.selectByVisibleText(item);
	}
	
	/**
	 * select item in Combobox by index
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @param item
	 */
	public void selectItemComboboxByIndex(WebDriver driver, String controlName, int index) {
		element = control.findElement(driver, controlName);
		Select select = new Select(element);
		select.selectByIndex(index);
	}

	/**
	 * get item selected in combobox
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @return item
	 */
	public String getItemSelectedCombobox(WebDriver driver, String controlName, String value) {
		element = control.findElement(driver, controlName, value);
		Select select = new Select(element);
		String itemSelected = select.getFirstSelectedOption().getText();
		return itemSelected;
	}

	/**
	 * get item selected in combobox
	 * 
	 * @param driver
	 * @param controlName
	 * @param value
	 * @return item
	 */
	public String getItemSelectedCombobox(WebDriver driver, String controlName) {
		element = control.findElement(driver, controlName);
		Select select = new Select(element);
		String itemSelected = select.getFirstSelectedOption().getText();
		return itemSelected;
	}
	
	/**
	 * check The Checkbox
	 * 
	 * @param driver
	 * @param controlName
	 */
	public void checkTheCheckbox(WebDriver driver, String controlName) {
		element = control.findElement(driver, controlName);
		if (!element.isSelected()) {
			click(driver, controlName);
		}
	}

	/**
	 * check The Checkbox
	 * 
	 * @param driver
	 * @param controlName
	 */
	public void checkTheCheckbox(WebDriver driver, String controlName, String value) {
		element = control.findElement(driver, controlName, value);
		if (!element.isSelected()) {
			click(driver, controlName, value);
		}
	}

	/**
	 * check The Checkbox
	 * 
	 * @param driver
	 * @param controlName
	 */
	public void checkTheCheckbox(WebDriver driver, String controlName, String value1, String value2) {
		element = control.findElement(driver, controlName, value1, value2);
		if (!element.isSelected()) {
			click(driver, controlName, value1, value2);
		}
	}

	/**
	 * uncheck The Checkbox
	 * 
	 * @param driver
	 * @param controlName
	 */
	public void uncheckTheCheckbox(WebDriver driver, String controlName) {
		element = control.findElement(driver, controlName);
		if (element.isSelected()) {
			click(driver, controlName);
		}
	}

	/**
	 * check checkbox is checked or not
	 * 
	 * @param driver
	 * @param controlName
	 */
	public boolean isCheckboxChecked(WebDriver driver, String controlName) {
		element = control.findElement(driver, controlName);
		return element.isSelected();
	}

	/**
	 * switch To Top Window Frame
	 */
	public WebDriver switchToTopWindowFrame(WebDriver driver) {
		driver = driver.switchTo().defaultContent();
		return driver;
	}

	/**
	 * Open PreciseRes page by Lender
	 */
	public HomePage openPreciseResUrl(WebDriver driver, String ipClient, String Url) {
		driver.get(Url);
		sleep();
		return PageFactory.getHomePage(driver, ipClient);
	}

	/**
	 * Open PreciseRes page by Borrower
	 */
	public LoginPage openPreciseResUrlByBorrower(WebDriver driver, String ipClient, String Url) {
		driver.get(Url);
		sleep();
		return PageFactory.getLoginPage(driver, ipClient);
	}

	/**
	 * goto Uploader Page
	 */
	public UploaderLoginPage gotoUploaderPage(WebDriver driver, String ipClient, String uploaderPageUrl) {
		if(isControlDisplayed(driver, Interfaces.UploaderLoginPage.LOG_OUT_BUTTON))
			executeClick(driver, Interfaces.UploaderLoginPage.LOG_OUT_BUTTON);
		driver.get(uploaderPageUrl);
		if(isControlDisplayed(driver, Interfaces.UploaderLoginPage.LOG_OUT_BUTTON))
			executeClick(driver, Interfaces.UploaderLoginPage.LOG_OUT_BUTTON);
		sleep(3);
		return PageFactory.getUploaderLoginPage(driver, ipClient);
	}

	/**
	 * wait For Download File Fullname Completed
	 * 
	 */
	public void waitForDownloadFileFullnameCompleted(String fileName) {
		int i = 0;
		while (i < 30) {
			boolean exist = Common.getCommon().isFileExists(fileName);
			if (exist == true) {
				i = 30;
			}
			sleep(1);
			i = i + 1;
		}
	}

	/**
	 * wait For Download File Contains name Completed
	 * 
	 */
	public void waitForDownloadFileContainsNameCompleted(String fileName) {
		int i = 0;
		while (i < 30) {
			boolean exist = Common.getCommon().isFileContains(fileName);
			if (exist == true) {
				i = 30;
			}
			sleep(1);
			i = i + 1;
		}
	}

	/**
	 * delete file full name in folder
	 * 
	 */
	public void deleteFileFullName(String fileName) {
		if (Common.getCommon().isFileExists(fileName)) {
			Common.getCommon().deleteFileFullName(fileName);
		}
	}

	/**
	 * delete file contains name in folder
	 * 
	 */
	public void deleteContainsFileName(String fileName) {
		Common.getCommon().deleteFileContainsName(fileName);
	}

	/**
	 * count number of file in folder
	 * 
	 */
	public int countFilesInDirectory() {
		return Common.getCommon().countFilesInDirectory();
	}

	/**
	 * Get filename in directory
	 * 
	 * @return
	 */
	public String getFileNameInDirectory() {
		return Common.getCommon().getFileNameInDirectory();
	}

	/**
	 * delete all files in folder
	 * 
	 */
	public void deleteAllFileInFolder() {
		Common.getCommon().deleteAllFileInFolder();
	}

	/**
	 * check file exist
	 * 
	 */
	public boolean isFileExist(String fileName) {
		waitForDownloadFileFullnameCompleted(fileName);
		if (Common.getCommon().isFileExists(fileName)) {
			return true;
		} else
			return false;
	}

	/**
	 * check file contains
	 */
	public boolean isFileContain(String fileName) {
		waitForDownloadFileContainsNameCompleted(fileName);
		if (Common.getCommon().isFileContains(fileName)) {
			return true;
		} else
			return false;
	}

	/**
	 * get Location X
	 */
	public int getLocationX(WebDriver driver, String controlName, String value) {
		waitForControl(driver, controlName, value, timeWait);
		element = control.findElement(driver, controlName, value);
		return element.getLocation().x;
	}

	/**
	 * get Location X
	 */
	public int getLocationX(WebDriver driver, String controlName) {
		waitForControl(driver, controlName, timeWait);
		element = control.findElement(driver, controlName);
		return element.getLocation().x;
	}

	/**
	 * get Location Y
	 */
	public int getLocationY(WebDriver driver, String controlName, String value) {
		waitForControl(driver, controlName, value, timeWait);
		element = control.findElement(driver, controlName, value);
		return element.getLocation().y;
	}

	/**
	 * get Location Y
	 */
	public int getLocationY(WebDriver driver, String controlName) {
		waitForControl(driver, controlName, timeWait);
		element = control.findElement(driver, controlName);
		return element.getLocation().y;
	}

	/**
	 * goto Loans page at any current page
	 */
	public LoansPage openLoansPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.LOANS_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.LOANS_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getLoansPage(driver, ipClient);
	}
	
	/**
	 * goto Properties page at any current page
	 */
	public PropertiesPage openPropertiesPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.PROPERTIES_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.PROPERTIES_LINK));
			sleep(5);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getPropertiesPage(driver, ipClient);
	}

	/**
	 * goto Documents page at any current page
	 */
	public DocumentsPage openDocumentsPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.DOCUMENTS_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.DOCUMENTS_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getDocumentsPage(driver, ipClient);
	}

	/**
	 * goto Applicants/Users page at any current page [SAMARK]
	 */
	public ApplicantsPage openApplicantsPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.APPLICANTS_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.APPLICANTS_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getApplicantsPage(driver, ipClient);
	}

	/**
	 * goto Borrowers page [CAF-FNF]
	 */
	public FNFBorrowersPage openBorrowersPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.FNF_BORROWERS_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.FNF_BORROWERS_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getFNFBorrowersPage(driver, ipClient);
	}

	/**
	 * goto Loan Dashboard page [CAF-FNF] page at any current page
	 */
	public FNFLoanDashboardPage openLoanDashboardPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.FNF_LOAN_DASHBOARD_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.FNF_LOAN_DASHBOARD_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getLoanDashboardPage(driver, ipClient);
	}

	/**
	 * goto Properties page [FNF] page at any current page
	 */
	public FNFPropertiesPage openPropertiesPageFNF(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.FNF_PROPERTIES_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.FNF_PROPERTIES_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getFNFPropertiesPage(driver, ipClient);
	}

	/**
	 * goto Documents page [FNF] page at any current page
	 */
	public FNFDocumentPage openDocumentsPageFNF(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.FNF_DOCUMENTS_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.FNF_DOCUMENTS_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getDocumentsPageFNF(driver, ipClient);
	}

	/**
	 * goto Notification Templates page [FNF] page at any current page
	 */
	public NotificationTemplatesPage openNotificationTemplatesPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.NOTIFICATION_TEMPLATES_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.NOTIFICATION_TEMPLATES_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getNotificationTemplatesPage(driver, ipClient);
	}

	/**
	 * goto Home page at any current page
	 */
	public HomePage openHomePage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.HOME_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.HOME_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getHomePage(driver, ipClient);
	}

	/**
	 * goto User Logins page at any current page
	 */
	public UserLoginsPage openUserLoginsPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.LOGINS_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.LOGINS_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getUserLoginsPage(driver, ipClient);
	}

	/**
	 * goto Lenders page at any current page
	 */
	public LendersPage openLendersPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.LENDERS_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.LENDERS_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getLendersPage(driver, ipClient);
	}

	/**
	 * goto Document Security page at any current page
	 */
	public DocumentSecurityPage openDocumentSecurityPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.DOCUMENT_SECURITY_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.DOCUMENT_SECURITY_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getDocumentSecurityPage(driver, ipClient);
	}

	/**
	 * goto My Information page at any current page
	 */
	public MyInformationPage openMyInformationPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.MY_INFORMATION_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.MY_INFORMATION_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getMyInformationPage(driver, ipClient);
	}

	/**
	 * goto Notification Templates page at any current page
	 */
	public CheckListsPage openCheckListsPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.CHECK_LISTS_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.CHECK_LISTS_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getCheckListsPage(driver, ipClient);
	}

	/**
	 * check Error message display
	 * 
	 * @param N/A
	 */
	public boolean isErrorMessageDisplay(WebDriver driver, String ipClient) {
		waitForControl(driver, Interfaces.AbstractPage.ERROR_MESSAGE_TITLE, 3);
		return isControlDisplayed(driver, Interfaces.AbstractPage.ERROR_MESSAGE_TITLE);
	}

	/**
	 * goto My Information page at any current page
	 */
	public FNFBorrowersPage openFNFMyInformationPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.MY_INFORMATION_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.MY_INFORMATION_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getFNFMyInformationPage(driver, ipClient);
	}

	/**
	 * goto SalesForce Account page at any current page
	 */
	public SalesForceAccountPage openSalesforceAccountPage(WebDriver driver, String ipClient) {
		try {
			String url = "";
			Thread.sleep(2000);
			control.setPage("AbstractPage");
			url = control.findElement(driver, Interfaces.AbstractPage.SALES_FORCE_ACCOUNT_LINK).getAttribute("href");
			driver.get(url);
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getSalesforceAccountPage(driver, ipClient);
	}

	/**
	 * goto SalesForce Contacts page at any current page
	 */
	public SalesForceContactsPage openSalesforceContactsPage(WebDriver driver, String ipClient) {
		try {
			String url = "";
			Thread.sleep(2000);
			control.setPage("AbstractPage");
			url = control.findElement(driver, Interfaces.AbstractPage.SALES_FORCE_CONTACTS_LINK).getAttribute("href");
			driver.get(url);
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getSalesforceContactsPage(driver, ipClient);
	}

	/**
	 * goto SalesForce Deals page at any current page
	 */
	public SalesForceDealsPage openSalesforceDealsPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.SALES_FORCE_DEALS_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.SALES_FORCE_DEALS_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getSalesforceDealsPage(driver, ipClient);
	}

	/**
	 * Drag and drop element
	 * 
	 * @param driver
	 * @param sourceControl
	 * @param targetControl
	 */
	public void dragAndDrop(WebDriver driver, String sourceControl, String targetControl) {
		WebElement source = control.findElement(driver, sourceControl);
		WebElement target = control.findElement(driver, targetControl);
		Actions action = new Actions(driver);
		action.dragAndDrop(source, target);
		action.perform();
	}

	/**
	 * Drag and drop element
	 * 
	 * @param driver
	 * @param sourceControl
	 * @param targetControl
	 */
	public void dragAndDrop(WebDriver driver, String sourceControl, String value1, String targetControl, String value2) {
		WebElement source = control.findElement(driver, sourceControl, value1);
		WebElement target = control.findElement(driver, targetControl, value2);
		Actions action = new Actions(driver);
		action.dragAndDrop(source, target);
		action.perform();
	}

	/**
	 * click on element
	 * 
	 * @param driver
	 * @param controlName
	 */
	public void click(WebDriver driver, String controlName, String value) {
		waitForControl(driver, controlName, value, timeWait);
		element = control.findElement(driver, controlName, value);
		element.click();
	}

	/**
	 * scroll page to Control
	 * 
	 * @param driver
	 * @param x
	 * @param y
	 */
	public void scrollPageToControl(WebDriver driver, String controlName, String... value) {

		waitForControl(driver, 5, controlName, value);
		element = control.findElement(driver, controlName, value);

		int x = element.getLocation().x;
		int y = element.getLocation().y;
		executeJavaScript(driver, "window.scrollTo(" + x + "," + y + ");");
		sleep();

	}
	
	/**
	 * scroll page to Control
	 * 
	 * @param driver
	 * @param x
	 * @param y
	 */
	public void observeControl(WebDriver driver, String controlName, String... value) {

		waitForControl(driver, 5, controlName, value);
		element = control.findElement(driver, controlName, value);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		sleep();

	}

	/**
	 * Wait for control is displayed
	 * 
	 * @param driver
	 * @param controlName
	 * @param value...
	 * @param timeout
	 */
	public void waitForControl(WebDriver driver, long timeout, final String controlName, final String... value) {
		try {
			By by = control.getBy(driver, controlName, value);
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			log.debug("Element doesn't exist");
		}
	}

	/**
	 * Check hyperlink exists
	 * 
	 * @param URLName
	 * @return
	 */
	public boolean checkHyperlinkExists(String URLName) {
		try {
			HttpURLConnection.setFollowRedirects(false);
			HttpURLConnection con = (HttpURLConnection) new URL(URLName).openConnection();
			con.setRequestMethod("HEAD");
			return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * click On Logged User Name
	 */
	public void clickOnLoggedUserName(WebDriver driver) {
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			waitForControl(driver, Interfaces.LoansPage.USER_FULL_NAME_LOGGED, timeWait);
			doubleClick(driver, Interfaces.LoansPage.USER_FULL_NAME_LOGGED);
			sleep(2);
			click(driver, Interfaces.LoansPage.USER_FULL_NAME_LOGGED);
			sleep(2);
			while (!isControlDisplayed(driver, Interfaces.HomePage.SETTING_NAV_PULL_RIGHT_TITLE)) {
				doubleClick(driver, Interfaces.LoansPage.USER_FULL_NAME_LOGGED);
				sleep(2);
				click(driver, Interfaces.LoansPage.USER_FULL_NAME_LOGGED);
				sleep(2);
			}
		} else {
			waitForControl(driver, Interfaces.LoansPage.USER_FULL_NAME_LOGGED, timeWait);
			click(driver, Interfaces.LoansPage.USER_FULL_NAME_LOGGED);
			sleep(2);
			if (!isControlDisplayed(driver, Interfaces.HomePage.SETTING_NAV_PULL_RIGHT_TITLE)) {
				executeJavaScript(driver, "document.getElementByClassName('dropdown-toggle').click();");
				sleep(2);
			}
		}
	}

	/**
	 * get Uploader Page Url
	 */
	public String getUploaderPageUrl(WebDriver driver) {
		waitForControl(driver, Interfaces.LoansPage.UPLOADER_DROPDOWN_ITEM, timeWait);
		return getAttributeValue(driver, Interfaces.LoansPage.UPLOADER_DROPDOWN_ITEM, "href");
	}

	/**
	 * Delete All Cookies
	 */
	public void deleteAllCookie(WebDriver driver) {
		driver.manage().deleteAllCookies();
		sleep(10);
	}

	/**
	 * goto Transaction (Store) page at any current page
	 */
	public TransactionPage openTransactionPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.TRANSACTION_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.TRANSACTION_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getTransactionPage(driver, ipClient);
	}

	/**
	 * goto Reports (Store) page at any current page
	 */
	public ReportsPage openReportsPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.REPORTS_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.REPORTS_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getReportsPage(driver, ipClient);
	}

	/**
	 * Open the excel file
	 * 
	 * @param sheetName
	 */
	public void setExcelFile(String sheetName) {
		Common.getCommon().setExcelFile(sheetName);
	}

	/**
	 * Get data in Excel file
	 * 
	 * @param rowNum
	 * @param colNum
	 * @return
	 */
	public String getCellData(int rowNum, int colNum) {
		return Common.getCommon().getCellData(rowNum, colNum);
	}

	/**
	 * Write data in excel file
	 * 
	 * @param sheetName
	 */
	public void setExcelFile(String value, int rowNum, int colNum) {
		Common.getCommon().setCellData(value, rowNum, colNum);
	}

	/**
	 * goto User page at any current page
	 */
	public UsersPage openUsersPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.USERS_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.USERS_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getUsersPage(driver, ipClient);
	}
	
	/**
	 * check Send by Email
	 */
	public boolean isSendByEmailCorrectly(WebDriver driver, String emailInvalid, String emailValid, String subject, String message) {
		String currentHandle = driver.getWindowHandle();
		openWindowHandles.push(driver.getWindowHandle());
		Set<String> newHandles = driver.getWindowHandles();
		newHandles.removeAll(openWindowHandles);
		String handle = newHandles.iterator().next();
		driver.switchTo().window(handle);
		
		//click on Send button
		waitForControl(driver, Interfaces.UsersPage.SEND_EMAIL_BUTTON, timeWait);
		click(driver, Interfaces.UsersPage.SEND_EMAIL_BUTTON);
		sleep(2);
		
//		An email address is required.
		if(!isControlDisplayed(driver, Interfaces.UsersPage.DYNAMIC_ERROR_MESSAGE, "An email address is required.")) return false;
		
		//type invalid email
		type(driver, Interfaces.UsersPage.TO_EMAIL_TEXTFIELD, emailInvalid);
		
		//click on Send button
		click(driver, Interfaces.UsersPage.SEND_EMAIL_BUTTON);
		sleep(2);
		
//		Email address emailinvalid*&@*$!@*$&@gmail.$#@$* is not in correct format.
		if(!isControlDisplayed(driver, Interfaces.UsersPage.DYNAMIC_ERROR_MESSAGE, "Email address "+emailInvalid+" is not in correct format.")) return false;		
		
		//type invalid cc email
		type(driver, Interfaces.UsersPage.TO_EMAIL_TEXTFIELD, emailValid);
		type(driver, Interfaces.UsersPage.CC_EMAIL_TEXTFIELD, emailInvalid);
		
		//click on Send button
		click(driver, Interfaces.UsersPage.SEND_EMAIL_BUTTON);
		sleep(2);
		
//		Email address emailinvalid*&@*$!@*$&@gmail.$#@$* is not in correct format.
		if(!isControlDisplayed(driver, Interfaces.UsersPage.DYNAMIC_ERROR_MESSAGE, "Email address "+emailInvalid+" is not in correct format.")) return false;		
		
		//type invalid bcc email
		type(driver, Interfaces.UsersPage.CC_EMAIL_TEXTFIELD, emailValid);
		type(driver, Interfaces.UsersPage.BCC_EMAIL_TEXTFIELD, emailInvalid);
		
		//click on Send button
		click(driver, Interfaces.UsersPage.SEND_EMAIL_BUTTON);
		sleep(2);
		
//		Email address emailinvalid*&@*$!@*$&@gmail.$#@$* is not in correct format.
		if(!isControlDisplayed(driver, Interfaces.UsersPage.DYNAMIC_ERROR_MESSAGE, "Email address "+emailInvalid+" is not in correct format.")) return false;		

		//type valid email, subject and message
		type(driver, Interfaces.UsersPage.BCC_EMAIL_TEXTFIELD, emailValid);
		type(driver, Interfaces.UsersPage.SUBJECT_TEXTFIELD, subject);
		type(driver, Interfaces.UsersPage.MESSAGE_TEXTFIELD, message);
		
		//click on Send button
		click(driver, Interfaces.UsersPage.SEND_EMAIL_BUTTON);
		sleep(2);
		
		driver.switchTo().window(currentHandle);
		waitForControl(driver, Interfaces.AbstractPage.LOGGED_IN_LINK, timeWait);
		return true;
	}
	
	/**
	 * get attribute of element
	 * 
	 * @param controlName
	 * @param attribute
	 * @return value1, value2
	 */
	public String getAttributeValueOfList(WebDriver driver, String controlName, String value, String attribute) {
		waitForControl(driver, controlName, value, timeWait);
		List<WebElement> elementList;
		elementList = getElements(driver, controlName, value);
		for (WebElement element: elementList) {
			if (!element.getAttribute(attribute).equals("")){ 
				return element.getAttribute(attribute);
			}
		}
		return "Not found";
	}
	
	public static BigDecimal round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);       
        return bd;
    }
	
	/**
	 * goto Loans page at any current page
	 */
	public FNFLoansPage openFNFLoansPage(WebDriver driver, String ipClient) {
		try {
			waitForControl(driver, Interfaces.AbstractPage.LOANS_LINK, timeWait);
			driver.get(getControlHref(driver, Interfaces.AbstractPage.LOANS_LINK));
			sleep(2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageFactory.getFNFLoansPage(driver, ipClient);
	}
	
	/**
	 * Check field name displays
	 */
	public Boolean isFieldNameDisplayed(WebDriver driver, String fieldName) {
		waitForControl(driver, Interfaces.AbstractPage.DYNAMIC_ALL_FIELDS_NAME, fieldName, 10);
		return isControlDisplayed(driver, Interfaces.AbstractPage.DYNAMIC_ALL_FIELDS_NAME, fieldName);
	}
	
	/**
	 * Get number of records
	 */
	public int getNumberOfRecords(WebDriver driver) {
		waitForControl(driver, Interfaces.AbstractPage.NUMBER_OF_RECORD_SHOWED, timeWait);
		String text = getText(driver, Interfaces.AbstractPage.NUMBER_OF_RECORD_SHOWED);
		String parts[] = text.split(" of ");
		return Integer.parseInt(parts[1]);
	}
	
	/**
	 * Count number of records displayed in email content
	 */
	public Boolean isRecordsTableDisplayedCorrectly(WebDriver driver, String tableID, int numberOfRecords) {
		String currentHandle = driver.getWindowHandle();
		openWindowHandles.push(driver.getWindowHandle());
		Set<String> newHandles = driver.getWindowHandles();
		newHandles.removeAll(openWindowHandles);
		String handle = newHandles.iterator().next();
		driver.switchTo().window(handle);
		waitForControl(driver, Interfaces.AbstractPage.DYNAMIC_RECORDS_TABLE_IN_EMAIL, tableID, timeWait);
		int countRecords = countElement(driver, Interfaces.AbstractPage.DYNAMIC_NUMBER_OF_RECORDS_IN_EMAIL, tableID);
		if (!isControlDisplayed(driver, Interfaces.AbstractPage.DYNAMIC_RECORDS_TABLE_IN_EMAIL, tableID) 
				|| (numberOfRecords != countRecords)){
			driver.switchTo().window(currentHandle);
			return false;
		}
		driver.switchTo().window(currentHandle);
		return true;
		
	}
	
	/**
	 * Click on sort lessee by ascending button
	 */
	public void clickOnSortRecordLink(WebDriver driver, String title) {
		waitForControl(driver, Interfaces.AbstractPage.DYNAMIC_SORT_RECORDS_LINK, title, timeWait);
		click(driver, Interfaces.AbstractPage.DYNAMIC_SORT_RECORDS_LINK, title);
		sleep(5);
	}
	
	/**
	 * Get cell content by cell xpath
	 */
	public String getCellContentByCellID(WebDriver driver, String cellId) {
		waitForControl(driver, Interfaces.AbstractPage.DYNAMIC_CELL_CONTENT, cellId, timeWait);
		return getText(driver, Interfaces.AbstractPage.DYNAMIC_CELL_CONTENT, cellId);
	}
	
	/**
	 * Get cell content by cell xpath
	 */
	public String getCellContentByCellID(WebDriver driver, String cellId, int i) {
		waitForControl(driver, Interfaces.AbstractPage.DYNAMIC_CELL_CONTENT, cellId, timeWait);
		String itemName = getText(driver, Interfaces.AbstractPage.DYNAMIC_CELL_CONTENT, cellId);
		if(i==1) return convertDate(itemName);
		else return itemName.replace("$", "").replace(".00", "");
	}
	
	/**
	 * click using javaScript
	 * 
	 * @param controlName
	 * @return
	 */
	public void executeClick(WebDriver driver, String controlName) {
			element = control.findElement(driver, controlName);
			JavascriptExecutor executor = (JavascriptExecutor)driver;
		    executor.executeScript("arguments[0].click();", element);
	}
	
	/**
	 * click using javaScript
	 * 
	 * @param controlName
	 * @return
	 */
	public void executeClick(WebDriver driver, String controlName, String value) {
			element = control.findElement(driver, controlName, value);
			JavascriptExecutor executor = (JavascriptExecutor)driver;
		    executor.executeScript("arguments[0].click();", element);
	}
	
	/**
	 * select All element in a list
	 * 
	 * @param driver
	 * @param controlName
	 */
	public void selectAllElementsInList(WebDriver driver, String controlName) {
		waitForControl(driver, controlName, timeWait);
		List<WebElement> elementList;
		elementList = getElements(driver, controlName);
		for (WebElement element: elementList) {
			element.click();
		}
	}
	
	public void clickOnDownloadCsvFileButton(WebDriver driver){
		waitForControl(driver, Interfaces.AbstractPage.DOWNLOAD_EXEL_FILE_BUTTON, timeWait);
		executeClick(driver, Interfaces.AbstractPage.DOWNLOAD_EXEL_FILE_BUTTON);
		sleep(3);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(25);
			keyPressing("alt_s");
			keyPressing("alt_s");
			keyPressing("alt_s");
		}
	}
	
	/**
	 * Check CSV file exported correctly
	 * @param csvFilePath
	 * @param text
	 * @return
	 */
	public Boolean isCSVFileExportedCorrectly(String csvFilePath, String text){
		return 	Common.getCommon().isTextDisplayedInCSVFile(csvFilePath, text);
	}
	
	/**
	 * Check CSV file exported correctly
	 * @param csvFilePath
	 * @param text
	 * @return
	 */
	public Boolean isCSVFileExportedCorrectly(String csvFilePath, String text, String collumnName){
		return 	Common.getCommon().isTextDisplayedInCSVFile(csvFilePath, text, collumnName);
	}
	
	/**
	 * Check CSV file exported correctly
	 * @param csvFilePath
	 * @param text
	 * @return
	 */
	public Boolean isRecordsExportedCorrectly(String csvFileName, String uniqueText, String collumnName, int numberOfRecords, String... values){
		return 	Common.getCommon().checkRecordExportedCorrectly(csvFileName, uniqueText, collumnName, numberOfRecords, values);
	}
	
	/**
	 * convert date to none "0"
	 * @param date
	 * @return
	 */
	public String convertDate(String date){
		String[] dateParts = date.split("/");
		int month = Integer.parseInt(dateParts[0]);
		int day = Integer.parseInt(dateParts[1]);
		return Integer.toString(month)+"/"+Integer.toString(day)+"/"+dateParts[2];
	}
	
	/**
	 * convert valid number to percent
	 * @param date
	 * @return
	 */
	public String convertNumberToPercent(String number){
		int temp = Integer.parseInt(number);
		temp = temp/100;
		return Integer.toString(temp);
	}
	
	/**
	 * click Send this information to email
	 */
	public void clickSendInformationToEmail(WebDriver driver) {
		waitForControl(driver, Interfaces.UsersPage.SEND_INFORMATION_TO_EMAIL, timeWait);
		click(driver, Interfaces.UsersPage.SEND_INFORMATION_TO_EMAIL);
		sleep();
	}
	
	/**
	 * Get cell content by cell xpath
	 */
	public String getCurrentUserName(WebDriver driver) {
		waitForControl(driver, Interfaces.AbstractPage.LOGGED_USER_NAME, timeWait);
		return getText(driver, Interfaces.AbstractPage.LOGGED_USER_NAME);
	}
	
	/**
	 * Edit data tape file
	 * @param csvFileName
	 * @param sheetName
	 * @param oldValue
	 * @param newValue
	 */
	public void editDataTapeFile(String csvFileName, String sheetName, String oldValue, String newValue){
		Common.getCommon().editDataTapeFile(csvFileName, sheetName, oldValue, newValue);
	}
	
	/**
	 * Check menu button displayed
	 * @param driver
	 * @param buttonName
	 * @return
	 */
	public boolean isMenuButtonDisplayed(WebDriver driver, String buttonName){
		waitForControl(driver, Interfaces.AbstractPage.DYNAMIC_BUTTON_NAME, buttonName, timeWait);
		return isControlDisplayed(driver, Interfaces.AbstractPage.DYNAMIC_BUTTON_NAME, buttonName);
	}
	
	/**
	 * Get number of one Menu button
	 * @param driver
	 * @param buttonName
	 * @return
	 */
	public int getNumberOfOneMenuButton(WebDriver driver, String buttonName){
		waitForControl(driver, Interfaces.AbstractPage.DYNAMIC_BUTTON_NAME, buttonName, timeWait);
		return countElement(driver, Interfaces.AbstractPage.DYNAMIC_BUTTON_NAME, buttonName);
	}
	
	/**
	 * Click on Menu button
	 * @param driver
	 * @param buttonName
	 * @return
	 */
	public void clickOnMenuButton(WebDriver driver, String buttonName){
		waitForControl(driver, Interfaces.AbstractPage.DYNAMIC_BUTTON_NAME, buttonName, timeWait);
		executeClick(driver, Interfaces.AbstractPage.DYNAMIC_BUTTON_NAME, buttonName);
		sleep(2);
	}
	
	/**
	 * Check Page name displayed correctly
	 * @param driver
	 * @param pageName
	 * @return
	 */
	public boolean isPageNameDisplayedCorrectly(WebDriver driver, String pageName){
		waitForControl(driver, Interfaces.AbstractPage.DYNAMIC_PAGE_NAME, pageName, timeWait);
		return isControlDisplayed(driver, Interfaces.AbstractPage.DYNAMIC_PAGE_NAME, pageName);
	}
	
	/**
	 * Check Page name displayed correctly
	 * @param driver
	 * @param pageName
	 * @return
	 */
	public void openFirstRecord(WebDriver driver){
		waitForControl(driver, Interfaces.AbstractPage.FIRST_RECORD_LINK, timeWait);
		click(driver, Interfaces.AbstractPage.FIRST_RECORD_LINK);
		sleep(3);
	}
	
	/**
	 * Open Link by text
	 * @param driver
	 * @param pageName
	 * @return
	 */
	public void openLinkByText(WebDriver driver, String text){
		waitForControl(driver, Interfaces.AbstractPage.DYNAMIC_LINK_TEXT, text, timeWait);
		click(driver, Interfaces.AbstractPage.DYNAMIC_LINK_TEXT, text);
		sleep(3);
	}
	
	/**
	 * Calculation for Date time by Date
	 * @param inputString
	 * @param value
	 * @return date time
	 */
	 public String addDateTimeByDate(String inputString, int value){
		 SimpleDateFormat myFormat = new SimpleDateFormat("M/d/yyyy");

		 try {
		     Date date = myFormat.parse(inputString);
		     Calendar cal = Calendar.getInstance();
		     cal.setTime(date);
		     cal.add(Calendar.DATE, value );
		     return myFormat.format(cal.getTime()).toString(); 
		 } catch (Exception e) {
		     e.printStackTrace();
		 }
		 return "";
	 }
	 
	 /**
		 * Calculation for Date time by Month
		 * @param inputString
		 * @param value
		 * @return date time
		 */
	 public String addDateTimeByMonth(String inputString, int value){
		 SimpleDateFormat myFormat = new SimpleDateFormat("M/d/yyyy");
		 
		 try {
		     Date date = myFormat.parse(inputString);
		     Calendar cal = Calendar.getInstance();
		     cal.setTime(date);
		     cal.add(Calendar.MONTH, value );
		     return myFormat.format(cal.getTime()).toString(); 
		 } catch (Exception e) {
		     e.printStackTrace();
		 }
		 return "";
	 }
	 
	 public static String minusTwoDates(String date1, String date2, String timeType, Boolean acceptPossitive){
		 SimpleDateFormat myFormat = new SimpleDateFormat("M/d/yyyy");
		 try {
			 Date firstDate = myFormat.parse(date1);
			 Date secondDate = myFormat.parse(date2);
			 Boolean isFirstDateGreaterThanSecondDate = true;
			 if(firstDate == secondDate) return "0";
			 if(firstDate.after(secondDate)){
				 Date tempDate;
				 tempDate = firstDate;
				 firstDate = secondDate;
				 secondDate = tempDate;
				 isFirstDateGreaterThanSecondDate = false;
			 }
			 Calendar cal1 = Calendar.getInstance();
			 Calendar cal2 = Calendar.getInstance();
		     cal1.setTime(firstDate);
		     cal2.setTime(secondDate);
		     int daysBetween = 0; 
		     switch(timeType){
		     case "Date": 	while (cal1.before(cal2)) {  
		     					cal1.add(Calendar.DATE, 1);  
		     					daysBetween++;  
		     				} 
							break;
		     case "Month": 	while (cal1.before(cal2)) {  
								cal1.add(Calendar.MONTH, 1);  
								daysBetween++; 
		     				} 
							break;
		     case "Year": 	while (cal1.before(cal2)) {  
								cal1.add(Calendar.YEAR, 1);  
								daysBetween++;  
							} 
		     				break;

		     default: System.out.println("Time type isn't exist: "+timeType);
		     }
		     if(acceptPossitive&&isFirstDateGreaterThanSecondDate) 
		     return Integer.toString(-daysBetween);
		     else return Integer.toString(daysBetween);
		 } catch (Exception e) {
		     e.printStackTrace();
		 }
		 return "";
	 }
	 
		/**
		 * Error message displayed correctly
		 * @param driver
		 * @param errorMessage
		 * @return
		 */
		public boolean isErrorDisplayedCorrectly(WebDriver driver, String errorMessage){
			waitForControl(driver, Interfaces.AbstractPage.DYNAMIC_ERROR_MESSAGE_TITLE, errorMessage, timeWait);
			return isControlDisplayed(driver, Interfaces.AbstractPage.DYNAMIC_ERROR_MESSAGE_TITLE, errorMessage);
		}
		
		/**
		 * Warning message displayed correctly
		 * @param driver
		 * @param errorMessage
		 * @return
		 */
		public boolean isWarningDisplayedCorrectly(WebDriver driver, String errorMessage){
			waitForControl(driver, Interfaces.AbstractPage.DYNAMIC_WARNING_MESSAGE_TITLE, errorMessage, 10);
			return isControlDisplayed(driver, Interfaces.AbstractPage.DYNAMIC_WARNING_MESSAGE_TITLE, errorMessage);
		}
		
		/**
		 * Get data from Xml File
		 * @param fileName
		 * @param tagName
		 * @param attributeName
		 * @param attributeValue
		 * @return
		 */
		public String getDataFromXmlFile(String fileName, String tagName, String attributeName, String attributeValue){
			return Common.getCommon().getDataFromXmlFile(fileName, tagName, attributeName, attributeValue);
		}
		
		/**
		 * Get data list from Xml File
		 * @param fileName
		 * @param tagName
		 * @param attributeName
		 * @param attributeValue
		 * @return
		 */
		public List getGuarantorListFromXmlFile(String fileName, String tagName, String attributeName, Boolean isRepeated, String attributeValue){
			return Common.getCommon().getGuarantorListFromXmlFile(fileName, tagName, attributeName, isRepeated, attributeValue);
		}
		
		/**
		 * get item selected in combobox
		 * 
		 * @param driver
		 * @param controlName
		 * @param value
		 * @return item
		 */
		
		public boolean isItemInComboboxDisabled(WebDriver driver, String controlID, String itemValue){
			waitForControl(driver, Interfaces.AbstractPage.DYNAMIC_DROPDOWN_DISABLED_ITEM, controlID, itemValue, timeWait);
			return isControlDisplayed(driver, Interfaces.AbstractPage.DYNAMIC_DROPDOWN_DISABLED_ITEM, controlID, itemValue);
		}
		
		/**
		 * Convert Date to another format
		 * @param oldDateType
		 * @param newDateType
		 * @param date
		 * @return newDateFormat
		 */
		public String convertDateFormat(String oldDateType, String newDateType, String date){
			return Common.getCommon().convertDate(oldDateType, newDateType, date);
		}
		
		public void dragDropFile(String source, String destination){
			Common.getCommon().dragDropFile(source, destination);
			sleep(5);
		}
		
		/**
		 * Check image is displayed
		 * @param imageName
		 * @return
		 */
		public boolean isImageDisplayed(String imageName){
			return Common.getCommon().isImageDisplayed( imageName);
		}
		
		public String getCurrentDate(WebDriver driver, String dateFormat){
//			String currentDay, currentMonth, currentYear;
//			waitForControl(driver, Interfaces.AbstractPage.DATE_PICKER_IMAGE, timeWait);
//			click(driver, Interfaces.AbstractPage.DATE_PICKER_IMAGE);
//			waitForControl(driver, Interfaces.AbstractPage.TODAY_BUTTON, timeWait);
//			click(driver, Interfaces.AbstractPage.TODAY_BUTTON);
//			sleep(2);
//			int selectedMonthValue = Integer.parseInt(getAttributeValue(driver, Interfaces.AbstractPage.CURRENT_MONTH_DROPDOWN, "value"));
//			currentMonth = Integer.toString(selectedMonthValue+1);
//			currentYear = getAttributeValue(driver, Interfaces.AbstractPage.CURRENT_YEAR_DROPDOWN, "value");
//			currentDay = getText(driver, Interfaces.AbstractPage.CURRENT_DATE_SELECTOR);
//			String currentDate = String.format("%s/%s/%s", currentMonth, currentDay, currentYear);
//			click(driver, Interfaces.AbstractPage.DONE_BUTTON);
			waitForControl(driver, Interfaces.AbstractPage.RECORD_SAVED_MESSAGE, timeWait);
			String savedMessage = getText(driver, Interfaces.AbstractPage.RECORD_SAVED_MESSAGE);
			String currentDate[] = savedMessage.split(" ");
			return convertDateFormat("m/d/yyyy", dateFormat, currentDate[2].replace("[", ""));
		}
		
		public void openFirstRecordLinkByID(WebDriver driver, String id){
			waitForControl(driver, Interfaces.AbstractPage.DYNAMIC_RECORD_LINK, id, timeWait);
			click(driver, Interfaces.AbstractPage.DYNAMIC_RECORD_LINK, id);
		}
	
	private final Stack<String> openWindowHandles = new Stack<String>();
	protected int timeWait = 30;
	protected int timeSleep = 2;
	protected WebElement element;
	protected AutomationControl control = new AutomationControl();
	protected final Log log;
}
