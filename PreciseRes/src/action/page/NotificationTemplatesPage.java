package page;

import org.openqa.selenium.WebDriver;
import PreciseRes.Interfaces;

public class NotificationTemplatesPage extends AbstractPage {
	public NotificationTemplatesPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
	}

	/**
	 * check Notification Templates page display
	 * @param N/A
	 */
	public boolean isNotificationTemplatesPageDisplay() {
		waitForControl(driver, Interfaces.NotificationTemplatesPage.NOTIFICATION_TEMPLATES_PAGE_SECTION, timeWait);
		return isControlDisplayed(driver, Interfaces.NotificationTemplatesPage.NOTIFICATION_TEMPLATES_PAGE_SECTION);
	}
	
	
	private WebDriver driver;
}