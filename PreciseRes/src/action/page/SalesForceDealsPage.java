package page;

import java.util.Set;
import java.util.Stack;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import PreciseRes.Interfaces;

public class SalesForceDealsPage extends AbstractPage {

	public SalesForceDealsPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}
	
	/**
	 *select Record Type New Dropdown
	 */
	public void selectRecordTypeNewDropdown(String locLoan) {
		waitForControl(driver,Interfaces.SalesForceDealsPage.RECORD_TYPE_NEW_DROPDOWN, locLoan, timeWait);
		selectItemCombobox(driver, Interfaces.SalesForceDealsPage.RECORD_TYPE_NEW_DROPDOWN, locLoan);
		sleep();
	}
	
	/**
	 * click Continue button
	 */
	public void clickContinueButton() {
		waitForControl(driver,Interfaces.SalesForceDealsPage.CONTINUE_BUTTON, timeWait);
		click(driver, Interfaces.SalesForceDealsPage.CONTINUE_BUTTON);
		sleep();
	}
	
	/**
	 * type Deal Name
	 * @param dealName
	 */
	public void typeDealName(String dealName) {
		waitForControl(driver, Interfaces.SalesForceDealsPage.DEAL_NAME_TEXTBOX, timeWait);
		type(driver,Interfaces.SalesForceDealsPage.DEAL_NAME_TEXTBOX, dealName);
	}
	
	/**
	 *select Stage dropdown
	 * @param stage
	 */
	public void selectStageDropdown(String stage) {
		waitForControl(driver,Interfaces.SalesForceDealsPage.STAGE_DROPDOWN, stage, timeWait);
		selectItemCombobox(driver, Interfaces.SalesForceDealsPage.STAGE_DROPDOWN, stage);
		sleep();
	}
	
	/**
	 * type Close Date
	 * @param closeDate
	 */
	public void typeCloseDate(String closeDate) {
		waitForControl(driver, Interfaces.SalesForceDealsPage.CLOSE_DATE_TEXTBOX, timeWait);
		type(driver,Interfaces.SalesForceDealsPage.CLOSE_DATE_TEXTBOX, closeDate);
	}
	
	/**
	 *select LOC Loan Type dropdown
	 * @param locLoanType
	 */
	public void selectLOCLoanTypeDropdown(String locLoanType) {
		waitForControl(driver,Interfaces.SalesForceDealsPage.LOC_LOAN_TYPE_DROPDOWN, locLoanType, timeWait);
		selectItemCombobox(driver, Interfaces.SalesForceDealsPage.LOC_LOAN_TYPE_DROPDOWN, locLoanType);
		sleep();
	}
	
	/**
	 * type Loan Size
	 * @param loanSize
	 */
	public void typeLoanSize(String loanSize) {
		waitForControl(driver, Interfaces.SalesForceDealsPage.LOAN_SIZE_TEXTBOX, timeWait);
		type(driver,Interfaces.SalesForceDealsPage.LOAN_SIZE_TEXTBOX, loanSize);
	}
	
	
	/**
	 *select Rate dropdown
	 * @param rate
	 */
	public void selectRateDropdown(String rate) {
		waitForControl(driver,Interfaces.SalesForceDealsPage.RATE_DROPDOWN, rate, timeWait);
		selectItemCombobox(driver, Interfaces.SalesForceDealsPage.RATE_DROPDOWN, rate);
		sleep();
	}
	
	/**
	 * type Primary Contact
	 * @param primaryContact
	 */
	public void typePrimaryContact(String primaryContact) {
		waitForControl(driver, Interfaces.SalesForceDealsPage.PRIMARY_CONTACT_TEXTBOX, timeWait);
		type(driver,Interfaces.SalesForceDealsPage.PRIMARY_CONTACT_TEXTBOX, primaryContact);
	}
	
	/**
	 * click Save button
	 */
	public void clickSaveButton() {
		waitForControl(driver,Interfaces.SalesForceDealsPage.SAVE_BUTTON, timeWait);
		click(driver, Interfaces.SalesForceDealsPage.SAVE_BUTTON);
		sleep();
	}
	
	/**
	 * check Deal Name saved successfully
	 * @param dealName
	 */
	public boolean isDealNameSavedSuccessfully(String dealName) {
		waitForControl(driver, Interfaces.SalesForceDealsPage.DYNAMIC_DEAL_NAME_TITLE, dealName, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceDealsPage.DYNAMIC_DEAL_NAME_TITLE, dealName);
	}
	
	/**
	 * check Stage saved successfully
	 * @param stage
	 */
	public boolean isStageSavedSuccessfully(String stage) {
		waitForControl(driver, Interfaces.SalesForceDealsPage.DYNAMIC_STAGE_TITLE, stage, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceDealsPage.DYNAMIC_STAGE_TITLE, stage);
	}
	
	/**
	 * check Close Date saved successfully
	 * @param closeDate
	 */
	public boolean isCloseDateSavedSuccessfully(String closeDate) {
		waitForControl(driver, Interfaces.SalesForceDealsPage.DYNAMIC_CLOSE_DATE_NAME_TITLE, closeDate, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceDealsPage.DYNAMIC_CLOSE_DATE_NAME_TITLE, closeDate);
	}
	
	/**
	 * check Loc Loan Type saved successfully
	 * @param locLoanType
	 */
	public boolean isLocLoanTypeSavedSuccessfully(String locLoanType) {
		waitForControl(driver, Interfaces.SalesForceDealsPage.DYNAMIC_LOC_LOAN_TYPE_TITLE, locLoanType, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceDealsPage.DYNAMIC_LOC_LOAN_TYPE_TITLE, locLoanType);
	}
	
	/**
	 * check Loan Size saved successfully
	 * @param loanSize
	 */
	public boolean isLoanSizeSavedSuccessfully(String loanSize) {
		waitForControl(driver, Interfaces.SalesForceDealsPage.DYNAMIC_LOAN_SIZE_TITLE, loanSize, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceDealsPage.DYNAMIC_LOAN_SIZE_TITLE, loanSize);
	}
	
	/**
	 * check Primary Contact saved successfully
	 * @param primaryContact
	 */
	public boolean isPrimaryContactSavedSuccessfully(String primaryContact) {
		waitForControl(driver, Interfaces.SalesForceDealsPage.DYNAMIC_PRIMARY_CONTACT_TITLE, primaryContact, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceDealsPage.DYNAMIC_PRIMARY_CONTACT_TITLE, primaryContact);
	}
	
	/**
	 * check Rate saved successfully
	 * @param rate
	 */
	public boolean isRateSavedSuccessfully(String rate) {
		waitForControl(driver, Interfaces.SalesForceDealsPage.DYNAMIC_RATE_TITLE, rate, timeWait);
		return isControlDisplayed(driver, Interfaces.SalesForceDealsPage.DYNAMIC_RATE_TITLE, rate);
	}
	
	/**
	 * click PreciseRE button
	 */
	public void clickPreciseREButton() {
		waitForControl(driver,Interfaces.SalesForceDealsPage.PRECISERE_BUTTON, timeWait);
		click(driver, Interfaces.SalesForceDealsPage.PRECISERE_BUTTON);
		executeJavaScript(driver, "document.getElementByClassName('precisere').click();");
		sleep(10);
	}
	
	/**
	 * check Document Viewable
	 */
	public boolean isDocumentViewable(String contentMessage) {
		sleep(3);
		String currentHandle = driver.getWindowHandle();
		openWindowHandles.push(driver.getWindowHandle());
		Set<String> newHandles = driver.getWindowHandles();
		newHandles.removeAll(openWindowHandles);
		String handle = newHandles.iterator().next();
		driver.switchTo().window(handle);
		waitForControl(driver, Interfaces.SalesForceDealsPage.DYNAMIC_RESPONSE_FROM_PRES_MESSAGE, contentMessage, timeWait);
		boolean exists = isControlDisplayed(driver, Interfaces.SalesForceDealsPage.DYNAMIC_RESPONSE_FROM_PRES_MESSAGE, contentMessage);
		driver.close();
		driver.switchTo().window(currentHandle);
		sleep(3);
		return exists;
	}
	
	/**
	 * click Lookup Primary Contact
	 */
	public void clickLookupPrimaryContactIcon() {
		waitForControl(driver,Interfaces.SalesForceDealsPage.LOOKUP_PRIMARY_CONTACT_ICON, timeWait);
		click(driver, Interfaces.SalesForceDealsPage.LOOKUP_PRIMARY_CONTACT_ICON);
		sleep(2);
	}
	
	/**
	 * click On Logged User Name
	 */
	public SalesForceLoginPage clickOnLoggedUserName(String loggedUsername) {
		waitForControl(driver,	Interfaces.SalesForceDealsPage.DYNAMIC_USER_FULL_NAME_LOGGED, loggedUsername, timeWait);
		click(driver, Interfaces.SalesForceDealsPage.DYNAMIC_USER_FULL_NAME_LOGGED, loggedUsername);
		sleep();
		waitForControl(driver,	Interfaces.SalesForceDealsPage.LOGOUT_BUTTON, timeWait);
		click(driver, Interfaces.SalesForceDealsPage.LOGOUT_BUTTON);
		return PageFactory.getSalesforceLoginPage(driver, ipClient);
	}
	
	/**
	 * click Edit button
	 */
	public void clickEditButton() {
		waitForControl(driver,Interfaces.SalesForceDealsPage.EDIT_BUTTON,timeWait);
		click(driver, Interfaces.SalesForceDealsPage.EDIT_BUTTON);
		sleep();
	}
	
	/**
	 * click Primary Contact Name
	 */
	public void clickPrimaryContactName(String primaryContact) {
		waitForControl(driver,Interfaces.SalesForceDealsPage.DYNAMIC_PRIMARY_CONTACT_NAME, primaryContact, timeWait);
		click(driver, Interfaces.SalesForceDealsPage.DYNAMIC_PRIMARY_CONTACT_NAME, primaryContact);
		sleep(2);
	}
	
	/**
	 * switch To Search Contact Frame
	 */
	public WebDriver switchToSearchContactFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.SalesForceDealsPage.SEARCH_PRIMARY_CONTACT_FRAME,	timeWait);
		sleep();
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.SalesForceDealsPage.SEARCH_PRIMARY_CONTACT_FRAME)));
		return driver;
	}
	
	private final Stack<String> openWindowHandles = new Stack<String>();
	private WebDriver driver;
	private String ipClient;
}