package page;

import java.util.Random;

import org.openqa.selenium.WebDriver;

import PreciseRes.Interfaces;

public class HomePage extends AbstractPage {
	public HomePage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}

	/**
	 * check Username Display Above Main Menu
	 */
	public boolean isUsernameDisplayTopLeftCorner(String userFullName) {
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_USER_FULL_NAME_TITLE, userFullName, timeWait);
		return isControlDisplayed(driver, Interfaces.HomePage.DYNAMIC_USER_FULL_NAME_TITLE, userFullName);
	}

	/**
	 * check Logged As Username Display Right Left Corner
	 */
	public boolean isLoggedAsUsernameDisplayRightLeftCorner(String userName) {
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_USER_FULL_NAME_LOGGED, userName, timeWait);
		return isControlDisplayed(driver, Interfaces.HomePage.DYNAMIC_USER_FULL_NAME_LOGGED, userName);
	}

	/**
	 * check Tab Active On Main Menu
	 */
	public boolean isTabActiveOnMainMenu(String tabName) {
		waitForControl(driver, Interfaces.HomePage.TAB_ACTIVE_ON_MAIN_MENU, timeWait);
		String actualTab = getText(driver, Interfaces.HomePage.TAB_ACTIVE_ON_MAIN_MENU);
		return actualTab.contains(tabName);
	}

	/**
	 * get First Loan Item Name
	 */
	public String getFirstLoanItemName() {
		waitForControl(driver, Interfaces.HomePage.FIRST_LOAN_ITEM, timeWait);
		String itemName = getText(driver, Interfaces.HomePage.FIRST_LOAN_ITEM);
		return itemName;
	}

	/**
	 * select First Loan Item Name
	 */
	public LoansPage selectFirstLoansItem() {
		waitForControl(driver, Interfaces.HomePage.FIRST_LOAN_ITEM, timeWait);
		String url = getControlHref(driver, Interfaces.HomePage.FIRST_LOAN_ITEM);
		driver.get(url);
		sleep();
		return PageFactory.getLoansPage(driver, ipClient);
	}

	/**
	 * check Error Process Request Display
	 */
	public boolean isErrorProcessRequestDisplay() {
		waitForControl(driver, Interfaces.HomePage.PROCESS_REQUEST_ERROR_TITLE, timeWait);
		boolean titleDisplay = isControlDisplayed(driver, Interfaces.HomePage.PROCESS_REQUEST_ERROR_TITLE);
		boolean contentDisplay = isControlDisplayed(driver, Interfaces.HomePage.PROCESS_REQUEST_ERROR_CONTENT);
		return titleDisplay && contentDisplay;
	}

	/**
	 * click On Export CSV Button
	 */
	public void clickOnExportCSVButton() {
		waitForControl(driver, Interfaces.HomePage.EXPORT_CSV_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('ExportCSV').click();");
		sleep(3);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(25);
			keyPressing("alt_s");
			keyPressing("alt_s");
			keyPressing("alt_s");
		}
	}

	/**
	 * click On U/W Pipeline Button
	 */
	public void clickOnUWPipelineButton() {
		waitForControl(driver, Interfaces.HomePage.UW_PIPELINE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('UWPipelineReportExport').click();");
		sleep(3);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(25);
			keyPressing("alt_s");
			keyPressing("alt_s");
			keyPressing("alt_s");
		}
	}

	/**
	 * check Loan Display On Dashboard
	 */
	public boolean isLoanDisplayOnDashboard(String loanName) {
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_LOAN_ITEM_DASHBOARD, loanName, timeWait);
		return isControlDisplayed(driver, Interfaces.HomePage.DYNAMIC_LOAN_ITEM_DASHBOARD, loanName);
	}

	/**
	 * check Loan Display In Bucket
	 */
	public boolean isLoansDisplayInBucket(String loanName, String bucket) {
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_LOAN_ITEM_DASHBOARD, loanName, timeWait);
		int y1 = getLocationY(driver, Interfaces.HomePage.DYNAMIC_LOAN_ITEM_DASHBOARD, loanName);
		int y2 = getLocationY(driver, Interfaces.HomePage.DYNAMIC_BUCKET_ITEM_DASHBOARD, bucket);
		return y1 > y2;
	}

	/**
	 * check Loan Display In Bucket
	 */
	public LoansPage openCategory(String category) {
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_CATEGORY, category, timeWait);
		click(driver, Interfaces.HomePage.DYNAMIC_CATEGORY, category);
		sleep();
		return PageFactory.getLoansPage(driver, ipClient);
	}

	/**
	 * check Number Property Of Loan Display Dashboard
	 */
	public boolean isNumberPropertyOfLoanDisplayDashboard(String loanName, String numberProperty) {
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_NUMBER_PROPERTY_OF_LOAN_DASHBOARD, loanName, numberProperty, timeWait);
		return isControlDisplayed(driver, Interfaces.HomePage.DYNAMIC_NUMBER_PROPERTY_OF_LOAN_DASHBOARD, loanName, numberProperty);
	}

	/**
	 * check Number 'Required Doc' Of Loan Display Correct
	 */
	public boolean isNumberRequiredDocsOfLoanDisplayCorrect(String loanName, String numberRequiredDocs) {
		waitForControl(driver, Interfaces.HomePage.NUMBER_REQUIRED_DOCS_OF_ALL_LOAN_ITEM, loanName, numberRequiredDocs, timeWait);
		return isControlDisplayed(driver, Interfaces.HomePage.NUMBER_REQUIRED_DOCS_OF_ALL_LOAN_ITEM, loanName, numberRequiredDocs);
	}

	/**
	 * get number '%Loaded' Of Loan
	 */
	public String getPercentLoaded(String loanName) {
		waitForControl(driver, Interfaces.HomePage.GET_PERCENT_LOADED_OF_LOAN_ITEM, loanName, timeWait);
		String percentLoaded = getText(driver, Interfaces.HomePage.GET_PERCENT_LOADED_OF_LOAN_ITEM, loanName);
		return percentLoaded;
	}

	/**
	 * get number '%Reviewed' Of Loan
	 */
	public String getPercentReviewed(String loanName) {
		waitForControl(driver, Interfaces.HomePage.GET_PERCENT_REVIEWED_OF_LOAN_ITEM, loanName, timeWait);
		String percentReviewed = getText(driver, Interfaces.HomePage.GET_PERCENT_REVIEWED_OF_LOAN_ITEM, loanName);
		return percentReviewed;
	}

	/**
	 * get count element LOAN
	 */
	public int getElementLoan() {
		waitForControl(driver, Interfaces.HomePage.COUNT_ELEMENT_LOAN, timeWait);
		return countElement(driver, Interfaces.HomePage.COUNT_ELEMENT_LOAN);
	}

	/**
	 * select Default Loan Status
	 */
	public void selectDefaultLoanStatus(String loanStatus) {
		waitForControl(driver, Interfaces.HomePage.DEFAULT_LOAN_STATUS_COMBOBOX, loanStatus, timeWait);
		selectItemCombobox(driver, Interfaces.HomePage.DEFAULT_LOAN_STATUS_COMBOBOX, loanStatus);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(15);
		} else {
			sleep(5);
		}
	}

	/**
	 * click Save Default Status button
	 */
	public void clickSaveDefaultStatusbutton() {
		waitForControl(driver, Interfaces.HomePage.SAVE_DEFAULT_STATUS_BUTTON, timeWait);
		click(driver, Interfaces.HomePage.SAVE_DEFAULT_STATUS_BUTTON);
		sleep(5);
	}

	/**
	 * check Default Loan Status Selected Display On Dashboard
	 */
	public boolean isDefaultLoanStatusSelectedDisplayOnDashboard(String loanStatus) {
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_DEFAULT_LOAN_STATUS_SELECTED_DASHBOARD, loanStatus, timeWait);
		return isControlDisplayed(driver, Interfaces.HomePage.DYNAMIC_DEFAULT_LOAN_STATUS_SELECTED_DASHBOARD, loanStatus);
	}

	/**
	 * check Default Loan Status First Display On Dashboard
	 */
	public boolean isDefaultLoanStatusDisplayFirstOnDashboard(String loanStatus) {
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_DEFAULT_LOAN_STATUS_FISRT_DASHBOARD, loanStatus, timeWait);
		return isControlDisplayed(driver, Interfaces.HomePage.DYNAMIC_DEFAULT_LOAN_STATUS_FISRT_DASHBOARD, loanStatus);
	}

	/**
	 * Open Loan Display On Dashboard
	 */
	public LoansPage openLoanDisplayOnDashboard(String loanName) {
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_LOAN_ITEM_DASHBOARD, loanName, timeWait);
		click(driver, Interfaces.HomePage.DYNAMIC_LOAN_ITEM_DASHBOARD, loanName);
		sleep();
		return PageFactory.getLoansPage(driver, ipClient);
	}

	/**
	 * check Homepage display
	 * 
	 * @param N/A
	 */
	public boolean isHomePageDisplay() {
		waitForControl(driver, Interfaces.HomePage.HOME_PAGE_SECTION, timeWait);
		return isControlDisplayed(driver, Interfaces.HomePage.HOME_PAGE_SECTION);
	}

	/**
	 * click Settings title
	 */
	public void clickOnSettingsTitle() {
		waitForControl(driver, Interfaces.HomePage.SETTING_NAV_PULL_RIGHT_TITLE, timeWait);
		click(driver, Interfaces.HomePage.SETTING_NAV_PULL_RIGHT_TITLE);
		sleep();
	}

	/**
	 * type gmail
	 * 
	 * @param email
	 */
	public void typeEmail(String email) {
		waitForControl(driver, Interfaces.HomePage.EMAIL_TEXTBOX, timeWait);
		type(driver, Interfaces.HomePage.EMAIL_TEXTBOX, email);
	}

	/**
	 * click Save button
	 */
	public void clickSaveButton() {
		waitForControl(driver, Interfaces.HomePage.SAVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		sleep();
	}

	/**
	 * click On View All button
	 */
	public void clickOnViewAllButton() {
		waitForControl(driver, Interfaces.HomePage.VIEW_ALL_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('ViewAll').click();");
		sleep(30);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		scrollPage(driver);
	}

	/**
	 * click On Search button
	 */
	public void clickOnSearchButton() {
		waitForControl(driver, Interfaces.HomePage.SEARCH_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(10);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(10);
		}
	}

	/**
	 * Search for Loans at Homepage (CAF)
	 * 
	 * @param loanName
	 * @param applicantName
	 * @param statusChange
	 * @param termSheetIssued
	 * @param termSheetSigned
	 * @param properties
	 * @param estimatedValue
	 * @param reqLoanAmount
	 * @param requiredDocs
	 * @param noLoaded
	 * @param noReviewed
	 * @param noRevised
	 * @param dateCreated
	 */
	public void searchForLoansHomePageCAF(String loanName, String applicantName, String statusChange, String termSheetIssued, String termSheetSigned, String properties,
			String estimatedValue, String reqLoanAmount, String requiredDocs, String noLoaded, String noReviewed, String noRevised, String dateCreated) {
		waitForControl(driver, Interfaces.HomePage.LOAN_TEXTBOX, timeWait);
		type(driver, Interfaces.HomePage.LOAN_TEXTBOX, loanName);
		type(driver, Interfaces.HomePage.APPLICANT_TEXTBOX, applicantName);
		type(driver, Interfaces.HomePage.STATUS_CHANGE_DATE_TEXTBOX, statusChange);
		type(driver, Interfaces.HomePage.TERM_SHEET_ISSUE_TEXTBOX, termSheetIssued);
		type(driver, Interfaces.HomePage.TERM_SHEET_SIGNED_TEXTBOX, termSheetSigned);
		type(driver, Interfaces.HomePage.PROPERTY_TEXTBOX, properties);
		type(driver, Interfaces.HomePage.ESTIMATED_VALUE_TEXTBOX, estimatedValue);
		type(driver, Interfaces.HomePage.REQ_LOAN_AMOUNT_TEXTBOX, reqLoanAmount);
		type(driver, Interfaces.HomePage.REQUIRED_DOCS_TEXTBOX, requiredDocs);
		type(driver, Interfaces.HomePage.NUMBER_LOADED_TEXTBOX, noLoaded);
		type(driver, Interfaces.HomePage.NUMBER_REVIEWED_TEXTBOX, noReviewed);
		type(driver, Interfaces.HomePage.NUMBER_REVISED_TEXTBOX, noRevised);
		type(driver, Interfaces.HomePage.DATE_CREATED_TEXTBOX, dateCreated);
		clickOnSearchButton();
	}

	/**
	 * Search for Loans at Homepage (B2R)
	 * 
	 * @param loanName
	 * @param applicantName
	 * @param statusChange
	 * @param termSheetIssued
	 * @param termSheetSigned
	 * @param properties
	 * @param estimatedValue
	 * @param reqLoanAmount
	 * @param requiredDocs
	 * @param noLoaded
	 * @param noReviewed
	 * @param noRevised
	 * @param dateCreated
	 */
	public void searchForLoansHomePageB2R(String loanName, String applicantName, String statusChange, String termSheetIssued, String properties, String estimatedValue,
			String reqLoanAmount, String requiredDocs, String noLoaded, String noReviewed, String noRevised, String dateCreated) {
		waitForControl(driver, Interfaces.HomePage.LOAN_TEXTBOX, timeWait);
		type(driver, Interfaces.HomePage.LOAN_TEXTBOX, loanName);
		type(driver, Interfaces.HomePage.APPLICANT_TEXTBOX, applicantName);
		type(driver, Interfaces.HomePage.STATUS_CHANGE_DATE_TEXTBOX, statusChange);
		type(driver, Interfaces.HomePage.TERM_SHEET_ISSUE_TEXTBOX, termSheetIssued);
		type(driver, Interfaces.HomePage.PROPERTY_TEXTBOX, properties);
		type(driver, Interfaces.HomePage.ESTIMATED_VALUE_TEXTBOX, estimatedValue);
		type(driver, Interfaces.HomePage.REQ_LOAN_AMOUNT_TEXTBOX, reqLoanAmount);
		type(driver, Interfaces.HomePage.REQUIRED_DOCS_TEXTBOX, requiredDocs);
		type(driver, Interfaces.HomePage.NUMBER_LOADED_TEXTBOX, noLoaded);
		type(driver, Interfaces.HomePage.NUMBER_REVIEWED_TEXTBOX, noReviewed);
		type(driver, Interfaces.HomePage.NUMBER_REVISED_TEXTBOX, noRevised);
		type(driver, Interfaces.HomePage.DATE_CREATED_TEXTBOX, dateCreated);
		clickOnSearchButton();
		sleep(30);
	}

	/**
	 * Get text in Loan search
	 * 
	 * @param text
	 * @return
	 */
	public String getTextLoanSearch(String text) {
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_GET_TEXT_IN_SEARCH_LOAN_TABLE, text, timeWait);
		String itemName = getText(driver, Interfaces.HomePage.DYNAMIC_GET_TEXT_IN_SEARCH_LOAN_TABLE, text);
		return itemName;
	}

	/**
	 * Check all value display on search Loans table (B2R)
	 * 
	 * @param loanName
	 * @param applicantName
	 * @param statusChange
	 * @param termSheetIssued
	 * @param properties
	 * @param estimatedValue
	 * @param reqLoanAmount
	 * @param requiredDocs
	 * @param noLoaded
	 * @param noReviewed
	 * @param noRevised
	 * @param dateCreated
	 * @return
	 */
	public boolean isAllValueDisplayOnSearchLoansTableB2R(String loanName, String applicantName, String statusChange, String termSheetIssued, String properties,
			String estimatedValue, String reqLoanAmount, String requiredDocs, String noLoaded, String noReviewed, String noRevised, String dateCreated) {
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_ALL_VALUE_DISPLAY_IN_SEARCH_LOAN_TABLE_B2R, loanName, applicantName, statusChange, termSheetIssued, properties,
				estimatedValue, reqLoanAmount, requiredDocs, noLoaded, noReviewed, noRevised, dateCreated, timeWait);
		return isControlDisplayed(driver, Interfaces.HomePage.DYNAMIC_ALL_VALUE_DISPLAY_IN_SEARCH_LOAN_TABLE_B2R, loanName, applicantName, statusChange, termSheetIssued,
				properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded, noReviewed, noRevised, dateCreated);
	}

	/**
	 * Check all value display on search Loans table (CAF)
	 * 
	 * @param loanName
	 * @param applicantName
	 * @param statusChange
	 * @param termSheetIssued
	 * @param termSheetSigned
	 * @param properties
	 * @param estimatedValue
	 * @param reqLoanAmount
	 * @param requiredDocs
	 * @param noLoaded
	 * @param noReviewed
	 * @param noRevised
	 * @param dateCreated
	 * @return
	 */
	public boolean isAllValueDisplayOnSearchLoansTableCAF(String loanName, String applicantName, String statusChange, String termSheetIssued, String termSheetSigned,
			String properties, String estimatedValue, String reqLoanAmount, String requiredDocs, String noLoaded, String noReviewed, String noRevised, String dateCreated) {
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_ALL_VALUE_DISPLAY_IN_SEARCH_LOAN_TABLE_CAF, loanName, applicantName, statusChange, termSheetIssued, termSheetSigned,
				properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded, noReviewed, noRevised, dateCreated, timeWait);
		return isControlDisplayed(driver, Interfaces.HomePage.DYNAMIC_ALL_VALUE_DISPLAY_IN_SEARCH_LOAN_TABLE_CAF, loanName, applicantName, statusChange, termSheetIssued,
				termSheetSigned, properties, estimatedValue, reqLoanAmount, requiredDocs, noLoaded, noReviewed, noRevised, dateCreated);
	}

	/**
	 * check Bucket status selected
	 */
	public boolean isBucketStatusSelected(String statusName) {
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_BUCKET_STATUS_SELECTED, statusName, timeWait);
		return isControlDisplayed(driver, Interfaces.HomePage.DYNAMIC_BUCKET_STATUS_SELECTED, statusName);
	}

	public int getTotalNumberAtBucketStatus(String statusName) {
		String text = getText(driver, Interfaces.HomePage.DYNAMIC_TOTAL_LOAN_NUMBER_ON_STATUS_BUCKET, statusName);
		String[] subString = text.split(" ");
		int result = Integer.parseInt(subString[subString.length - 1]);
		return result;
	}

	/**
	 * check Loan number detail equal total loan number in Dead bucket
	 */
	public boolean isLoanDetailEqualTotalLoanNumberInDeadBucket(int numberTotalLoan) {
		int numberLoanDetail;
		if (isControlDisplayed(driver, Interfaces.HomePage.LOAN_NUMBER_DETAILS_ON_STATUS_BUCKET_EQUAL_ZERO)) {
			numberLoanDetail = 0;
		} else {
			waitForControl(driver, Interfaces.HomePage.LOAN_NUMBER_DETAILS_ON_STATUS_BUCKET, timeWait);
			numberLoanDetail = countElement(driver, Interfaces.HomePage.LOAN_NUMBER_DETAILS_ON_STATUS_BUCKET);
		}
		if (numberTotalLoan == numberLoanDetail) {
			return true;
		}
		return false;
	}
	
	/**
	 * check Loan number detail equal total loan number in Dead bucket
	 */
	public boolean isLoanDetailEqualTotalLoanNumberInDeadBucketB2R(int numberTotalLoan) {
		int numberLoanDetail;
		if (isControlDisplayed(driver, Interfaces.HomePage.LOAN_NUMBER_DETAILS_ON_STATUS_BUCKET_EQUAL_ZERO)) {
			numberLoanDetail = 0;
		} else {
			waitForControl(driver, Interfaces.HomePage.LOAN_NUMBER_DETAILS_ON_STATUS_BUCKET, timeWait);
			numberLoanDetail = countElement(driver, Interfaces.HomePage.LOAN_NUMBER_DETAILS_ON_STATUS_BUCKET) - 1;
		}
		if (numberTotalLoan == numberLoanDetail) {
			return true;
		}
		return false;
	}
	
	public long getBucketAmountNumber(String statusName) {
		waitForControl(driver, Interfaces.HomePage.BUCKET_AMOUNT_NUMBER, statusName, timeWait);
		String text = getText(driver, Interfaces.HomePage.BUCKET_AMOUNT_NUMBER, statusName).trim();
		long result = Long.parseLong(text.replace(",", ""));
		return result;
	}
	
	public String getRandomLoansName(){
		waitForControl(driver, Interfaces.HomePage.ALL_LOAN_NAMES, timeWait);
		int numberOfLoans = countElement(driver, Interfaces.HomePage.ALL_LOAN_NAMES);
		Random rand = new Random();
		int randomLoan = rand.nextInt(numberOfLoans -500) +500;
		String index = Integer.toString(randomLoan);
		return getText(driver, Interfaces.HomePage.DYNAMIC_LOAN_NAME, index);
	}
	
	public String getRequiredDocs(String loanName){
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_REQUIRED_DOCS_COUNT, loanName, timeWait);
		return getText(driver, Interfaces.HomePage.DYNAMIC_REQUIRED_DOCS_COUNT, loanName);
	}
	
	public String getLoadedDocs(String loanName){
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_LOADED_DOCS_COUNT, loanName, timeWait);
		return getText(driver, Interfaces.HomePage.DYNAMIC_LOADED_DOCS_COUNT, loanName);
	}
	
	public String getReviewedDocs(String loanName){
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_REVIEWED_DOCS_COUNT, loanName, timeWait);
		return getText(driver, Interfaces.HomePage.DYNAMIC_REVIEWED_DOCS_COUNT, loanName);
	}
	
	public String getRevisedDocs(String loanName){
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_REVISED_DOCS_COUNT, loanName, timeWait);
		return getText(driver, Interfaces.HomePage.DYNAMIC_REVISED_DOCS_COUNT, loanName);
	}
	
	public void clickOnDocumentsSummaryLink(String loanName){
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_DOCUMENT_SUMMARY_LINK, loanName, timeWait);
		executeClick(driver, Interfaces.HomePage.DYNAMIC_DOCUMENT_SUMMARY_LINK, loanName);
	}
	
	/**
	 * check Totals Required docs in Documents Summary is correct
	 */
	public boolean isTotalRequiredDocsDisplayedCorrectly(String totalDocs, String id) {
		int actualTotalDocs;
		int expectedTotalDocs = Integer.parseInt(totalDocs);
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_TOTAL_REQUIRED_DOCS_COUNT, id, "1", timeWait);
		actualTotalDocs = Integer.parseInt(getText(driver, Interfaces.HomePage.DYNAMIC_TOTAL_REQUIRED_DOCS_COUNT, id, "1")) 
							+ Integer.parseInt(getText(driver, Interfaces.HomePage.DYNAMIC_TOTAL_REQUIRED_DOCS_COUNT, id, "2")); 
		if(isControlDisplayed(driver, Interfaces.HomePage.DYNAMIC_NUMBER_OF_DOCS_FOR_LEGAL_DOCUMENTS,"1")){
			actualTotalDocs -= Integer.parseInt(getText(driver, Interfaces.HomePage.DYNAMIC_NUMBER_OF_DOCS_FOR_LEGAL_DOCUMENTS,"2"));
		}
		return actualTotalDocs==expectedTotalDocs;
	}
	
	/**
	 * check Totals Loaded docs in Documents Summary is correct
	 */
	public boolean isTotalLoadedDocsDisplayedCorrectly(String totalDocs, String id) {
		int actualTotalDocs;
		int expectedTotalDocs = Integer.parseInt(totalDocs); 
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_TOTAL_LOADED_DOCS_COUNT, id, "1", timeWait);
		actualTotalDocs = Integer.parseInt(getText(driver, Interfaces.HomePage.DYNAMIC_TOTAL_LOADED_DOCS_COUNT, id, "1")) 
							+ Integer.parseInt(getText(driver, Interfaces.HomePage.DYNAMIC_TOTAL_LOADED_DOCS_COUNT, id, "2")); 
		if(isControlDisplayed(driver, Interfaces.HomePage.DYNAMIC_NUMBER_OF_DOCS_FOR_LEGAL_DOCUMENTS,"1")){
			actualTotalDocs -= Integer.parseInt(getText(driver, Interfaces.HomePage.DYNAMIC_NUMBER_OF_DOCS_FOR_LEGAL_DOCUMENTS,"3"));
		}
		return actualTotalDocs==expectedTotalDocs;
	}
	
	/**
	 * check Totals Reviewed docs in Documents Summary is correct
	 */
	public boolean isTotalReviewedDocsDisplayedCorrectly(String totalDocs, String id) {
		int actualTotalDocs;
		int expectedTotalDocs = Integer.parseInt(totalDocs); 
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_TOTAL_REVIEWED_DOCS_COUNT, id, "1", timeWait);
		actualTotalDocs = Integer.parseInt(getText(driver, Interfaces.HomePage.DYNAMIC_TOTAL_REVIEWED_DOCS_COUNT, id, "1")) 
							+ Integer.parseInt(getText(driver, Interfaces.HomePage.DYNAMIC_TOTAL_REVIEWED_DOCS_COUNT, id, "2")); 
		if(isControlDisplayed(driver, Interfaces.HomePage.DYNAMIC_NUMBER_OF_DOCS_FOR_LEGAL_DOCUMENTS,"1")){
			actualTotalDocs -= Integer.parseInt(getText(driver, Interfaces.HomePage.DYNAMIC_NUMBER_OF_DOCS_FOR_LEGAL_DOCUMENTS,"4"));
		}
		return actualTotalDocs==expectedTotalDocs;
	}
	
	/**
	 * check Totals Revised docs in Documents Summary is correct
	 */
	public boolean isTotalRevisedDocsDisplayedCorrectly(String totalDocs, String id) {
		int actualTotalDocs;
		int expectedTotalDocs = Integer.parseInt(totalDocs); 
		waitForControl(driver, Interfaces.HomePage.DYNAMIC_TOTAL_REVISED_DOCS_COUNT, id, "1", timeWait);
		actualTotalDocs = Integer.parseInt(getText(driver, Interfaces.HomePage.DYNAMIC_TOTAL_REVISED_DOCS_COUNT, id, "1")) 
							+ Integer.parseInt(getText(driver, Interfaces.HomePage.DYNAMIC_TOTAL_REVISED_DOCS_COUNT, id, "2")); 
		if(isControlDisplayed(driver, Interfaces.HomePage.DYNAMIC_NUMBER_OF_DOCS_FOR_LEGAL_DOCUMENTS,"1")){
			actualTotalDocs -= Integer.parseInt(getText(driver, Interfaces.HomePage.DYNAMIC_NUMBER_OF_DOCS_FOR_LEGAL_DOCUMENTS,"5"));
		}
		return actualTotalDocs==expectedTotalDocs;
	}

	private WebDriver driver;
	private String ipClient;
}
