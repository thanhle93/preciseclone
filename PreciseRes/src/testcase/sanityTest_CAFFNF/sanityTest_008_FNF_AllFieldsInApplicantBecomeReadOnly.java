package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFLoansPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Common;
import common.Constant;

public class sanityTest_008_FNF_AllFieldsInApplicantBecomeReadOnly extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		date = Common.getCommon().getCurrentDay();
		month = Common.getCommon().getCurrentMonth();
		year = Common.getCommon().getCurrentYear();
		companyName = "Edit_Borrower " + accountNumber;
		loanName = "Edit Loan Auto " + getUniqueNumber();
		editCloseDate = month + "/" + date + "/" + year;
		productTypeInstitiutional = "Institutional";
		editExistingLineAmount = "60.52";
		dealRecordType = "LOC Loan";
		statusApplicant = "Matured";
		underwriter = "CAF FNF Lender";
		locClosingDate = "04/12/2014";
		validDate = "02/12/2014";
		validNumber ="10";
		validText = ""+ getUniqueNumber();
		acquisitionType = "Acquisition";
		interestRateType = "Fixed";
		renovationFunding = "Yes";
		underwritingException = "No";
		guarantorsName ="Guarantor"+ getUniqueNumber();
		lender = "CAF Bridge Lending";
	}

	@Test(groups = { "regression" }, description = "Applicant 31 - Login as Applicant - Make sure fields in applicant screen becomes read-only after 'Invited' status")
	public void FieldReadOnly_01_Borrower_AllFieldsApplicantScreenBecomeReadOnlyAfterInvitedStatus() {
		log.info("FieldReadOnly_01 - Step 01. Login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);

		log.info("FieldReadOnly_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("FieldReadOnly_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("FieldReadOnly_01 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("FieldReadOnly_01 - Step 05: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("FieldReadOnly_01 - Step 06: Input loan name");
		loanPage.inputRequiredData(loanName, locClosingDate);
		
		log.info("FieldReadOnly_01 - Step 07: Click Save button");
		loanPage.clickOnSaveButton();

		log.info("FieldReadOnly_01 - Step 08: Login with Applicant");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);

		log.info("FieldReadOnly_01 - Step 09: Open 'Loan Name'");
		loanPage = applicantsPage.openLoanNameInOpportunityTable(loanName);
		
		log.info("FieldReadOnly_01 - Step 10: Input 'Loan Name, LOC Closing Date, Product Type, Approved Line Amount, Loan Record Type' in Loan page");
		loanPage.inputDataInLoanPage(loanName, editCloseDate, productTypeInstitiutional, editExistingLineAmount, dealRecordType);
		
		log.info("FieldReadOnly_01 - Step 11: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("VP: Verify that 'Loan Name, LOC Closing Date, Product Type, Approved Line Amount, Loan Record Type' is displayed correctly");
		verifyTrue(loanPage.isDataInLoanPageDisplay(loanName, editCloseDate, productTypeInstitiutional, editExistingLineAmount, dealRecordType));
		
		log.info("FieldReadOnly_01 - Step 12: Click on 'Send to Lender for Review' button");
		loanPage.clickOnSendToLenderForReviewButton();
		
		log.info("VP: Verify that 'Loan Name, LOC Closing Date, Product Type, Approved Line Amount, Loan Record Type' is set read-only successfully");
		verifyTrue(loanPage.isDataInLoanPageSetReadOnly(loanName, editCloseDate, productTypeInstitiutional, editExistingLineAmount, dealRecordType));
	}

	@Test(groups = { "regression" }, description = "Applicant 32 - Login as Lender - Make sure fields in applicant screen becomes read-only after 'Invited' status")
	public void FieldReadOnly_02_Lender_AllFieldsApplicantScreenBecomeReadOnlyAfterClosingStatus() {

		log.info("FieldReadOnly_02 - Step 01. Logout and login with Lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("FieldReadOnly_02 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("FieldReadOnly_02 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("FieldReadOnly_02 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("FieldReadOnly_02 - Step 05: Open 'Loan Name'");
		loanPage = applicantsPage.openLoanNameInOpportunityTable(loanName);
		
		log.info("VP: Verify that 'Loan Name, LOC Closing Date, Product Type, Approved Line Amount, Loan Record Type' is displayed correctly");
		verifyTrue(loanPage.isDataInLoanPageDisplay(loanName, editCloseDate, productTypeInstitiutional, editExistingLineAmount, dealRecordType));
		
		log.info("FieldReadOnly_02 - Step 06: Click on 'Send to Term Sheet Sent' button");
		loanPage.clickOnSendToTermSheetSentButton();
		
		log.info("FieldReadOnly_02 - Step 07: Click on 'Send to Underwriting' button");
		loanPage.clickOnSendToUnderwritingButton();
		
		log.info("FieldReadOnly_02 - Step 08: Click on 'Send to Committee Review' button");
		loanPage.createNewGuarantor(guarantorsName);
		loanPage.inputDataRequiredForCommitteeReview(validDate, dealRecordType, validNumber, acquisitionType, interestRateType, renovationFunding, underwritingException, underwriter, lender);
		loanPage.clickOnSendToCommitteeReviewButton();
		
		log.info("FieldReadOnly_02 - Step 09: Click on 'Send to Documentation' button");
		loanPage.inputDataRequiredForDocumentation(validDate, validText, validNumber);
		loanPage.clickOnSendToDocumentationButton();
		
		log.info("FieldReadOnly_02 - Step 10: Click on 'Send to Investment Period' button");
		loanPage.inputDataRequiredForInvestmentPeriod(editCloseDate, validText, editExistingLineAmount);
		loanPage.clickOnSendToInvestmentPeriodButton();
		
		log.info("VP: Record Saved message displays");
		verifyTrue(loanPage.isRecordSavedMessageDisplays());
		
		log.info("VP: Verify that 'Investment Period' status is changed successfully");
		verifyTrue(loanPage.isStatusChangedSuccessfull(statusApplicant));
		
		log.info("VP: Verify that 'Loan Name, LOC Closing Date, Product Type, Approved Line Amount, Loan Record Type' is set read-only successfully");
		verifyTrue(loanPage.isDataInLoanPageSetReadOnly(loanName, editCloseDate, productTypeInstitiutional, editExistingLineAmount, dealRecordType));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private FNFLoansPage loanPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername,FNFBorrowerPassword;
	private String statusApplicant, accountNumber, loanName, underwriter, locClosingDate;
	private String companyName, editCloseDate, productTypeInstitiutional, editExistingLineAmount, dealRecordType;
	private int date, month, year;
	private String validDate, validNumber, validText, acquisitionType, interestRateType, renovationFunding, underwritingException;
	private String guarantorsName, lender;
	
	}