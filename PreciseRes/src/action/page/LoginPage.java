package page;

import org.openqa.selenium.WebDriver;

import PreciseRes.Interfaces;

public class LoginPage extends AbstractPage {

	public LoginPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}
	
	/**
	 * type user name
	 * @param username
	 */
	public void typeUsername(String username) {
		waitForControl(driver, Interfaces.LoginPage.USERNAME_TEXTBOX, timeWait);
		if(driver.toString().toLowerCase().contains("internetexplorer")){
			sleep(2);
		}
		type(driver,Interfaces.LoginPage.USERNAME_TEXTBOX, username);
	}
	
	/**
	 * type password
	 * @param password
	 */
	public void typePassword(String password) {
		waitForControl(driver, Interfaces.LoginPage.PASSWORD_TEXTBOX, timeWait);
		type(driver,Interfaces.LoginPage.PASSWORD_TEXTBOX, password);
	}

	/**
	 * Click on Submit button
	 */
	public void clickSubmitButton() {
		waitForControl(driver, Interfaces.LoginPage.SUBMIT_BUTTON, timeWait);
		click(driver,Interfaces.LoginPage.SUBMIT_BUTTON);
		sleep(2);
	}
	
	/**
	 * login web site with user name and password
	 * @return Homepage
	 */
	public HomePage loginAsLender(String username, String password, boolean isCheck)
	{
		typeUsername(username);
		typePassword(password);
		if(isCheck){
			checkTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}else{
			uncheckTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}
		clickSubmitButton();
		sleep(5);
		return PageFactory.getHomePage(driver, ipClient);
	}
	
	/**
	 * login web site with user name and password
	 * @return Homepage
	 */
	public HomePage loginAsAdmin(String username, String password, boolean isCheck)
	{
		typeUsername(username);
		typePassword(password);
		if(isCheck){
			checkTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}else{
			uncheckTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}
		clickSubmitButton();
//		if(driver.toString().toLowerCase().contains("internetexplorer")){
//            sleep(15);
//          }
		sleep();
		return PageFactory.getHomePage(driver, ipClient);
	}
	
	/**
	 * login web site with user name and password
	 * @return TermsOfUserPage
	 */
	public TermsOfUsePage loginAsNewUser(String username, String password, boolean isCheck)
	{
		typeUsername(username);
		typePassword(password);
		if(isCheck){
			checkTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}else{
			uncheckTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}
		clickSubmitButton();
//		if(driver.toString().toLowerCase().contains("internetexplorer")){
//            sleep(30);
//          }
		sleep();
		return PageFactory.getTermsOfUsePage(driver, ipClient);
	}
	
	/**
	 * login web site with user name and password
	 * @return LoansPage
	 */
	public LoansPage loginAsBorrower(String username, String password, boolean isCheck)
	{
		typeUsername(username);
		typePassword(password);
		if(isCheck){
			checkTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}else{
			uncheckTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}
		clickSubmitButton();
//		if(driver.toString().toLowerCase().contains("internetexplorer")){
//            sleep(60);
//          }
		sleep();
		return PageFactory.getLoansPage(driver, ipClient);
	}
	
	/**
	 * login web site with user name and password
	 * @return MyInformationPage
	 */
	public FNFBorrowersPage loginFNFAsBorrower(String username, String password, boolean isCheck)
	{
		typeUsername(username);
		typePassword(password);
		if(isCheck){
			checkTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}else{
			uncheckTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}
		clickSubmitButton();
//		if(driver.toString().toLowerCase().contains("internetexplorer")){
//            sleep(60);
//          }
		sleep();
		return PageFactory.getFNFBorrowersPage(driver, ipClient);
	}

	/**
	 * login web site with user name and password
	 * @return Loan Dashboad Page
	 */
	public FNFLoanDashboardPage loginAsCAFFNFLender(String username, String password, boolean isCheck)
	{
		typeUsername(username);
		typePassword(password);
		if(isCheck){
			checkTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}else{
			uncheckTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}
		clickSubmitButton();
//		if(driver.toString().toLowerCase().contains("internetexplorer")){
//            sleep(60);
//          }
		sleep();
		return PageFactory.getLoanDashboardPage(driver, ipClient);
	}
	
	/**
	 * check error message display with text on login page
	 */
	public boolean isInvalidUsernamePasswordMessageDisplayWithText(String textContent)
	{
		waitForControl(driver, Interfaces.LoginPage.INVALID_USERNAME_PASSWORD_MESSAGE, textContent, timeWait);
		return isControlDisplayed(driver, Interfaces.LoginPage.INVALID_USERNAME_PASSWORD_MESSAGE, textContent);
	}
	
	/**
	 * check Remember Username
	 */
	public void checkRememberUsername(boolean isCheck)
	{
		waitForControl(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX, timeWait);
		if(isCheck){
			checkTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}
		else{
			uncheckTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}
	}
	
	/**
	 * get User name Checkbox Content
	 */
	public String getUsernameCheckboxContent()
	{
		waitForControl(driver, Interfaces.LoginPage.USERNAME_TEXTBOX, timeWait);
		return getAttributeValue(driver, Interfaces.LoginPage.USERNAME_TEXTBOX, "value");
	}
	
	/**
	 * check Remember User name Checked
	 */
	public boolean isRememberUsernameChecked()
	{
		waitForControl(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX, timeWait);
		return isCheckboxChecked(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
	}
	
	/**
	 * login web site with user name and password
	 * @return Transaction Page
	 */
	public TransactionPage loginStoreAsLender(String username, String password, boolean isCheck)
	{
		typeUsername(username);
		typePassword(password);
		if(isCheck){
			checkTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}else{
			uncheckTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}
		clickSubmitButton();
		sleep(5);
		return PageFactory.getTransactionPage(driver, ipClient);
	}
	
	/**
	 * login web site with user name and password
	 * @return Homepage
	 */
	public AdminPage loginWithAdmin(String username, String password, boolean isCheck)
	{
		typeUsername(username);
		typePassword(password);
		if(isCheck){
			checkTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}else{
			uncheckTheCheckbox(driver, Interfaces.LoginPage.REMEMBER_USERNAME_CHECKBOX);
		}
		clickSubmitButton();
//		if(driver.toString().toLowerCase().contains("internetexplorer")){
//            sleep(15);
//          }
		sleep();
		return PageFactory.getAdminPage(driver, ipClient);
	}
	
	private WebDriver driver;
	private String ipClient;
}
