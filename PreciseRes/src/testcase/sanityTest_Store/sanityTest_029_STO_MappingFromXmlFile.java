package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_029_STO_MappingFromXmlFile extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileNameSingleGuarantor = "SingleGuarantorMapping.xml";
		xmlFileNameMultiGuarantorNotMatchNumber = "MultiGuarantorNotMatchNumber.xml";
		xmlFileNameMultiGuarantorMatchNumber = "MultiGuarantorMatchNumber.xml";
		xmlFileNameNoGuarantor = "NoGuarantor.xml";
		xmlFileNameCPIMultiplierTrue = "CPIMultiplierTrue.xml";
		xmlFileNameCPIMultiplierFalse = "CPIMultiplierFalse.xml";
		xmlFileNameLeasePriorFalse = "LeasePriorFalse.xml";
		xmlFileNameLeasePriorNo = "LeasePriorNo.xml";
		xmlFileNameLeasePriorTrue = "LeasePriorTrue.xml";
		xmlFileNameLeasePriorYes = "LeasePriorYes.xml";
	}

	@Test(groups = { "regression" }, description = "Mapping From Xml File 01 - Import Single Guarantor")
	public void MappingFromXmlFile_01_ImportSingleGuarantor() {
		
		log.info("MappingFromXmlFile_01 - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("MappingFromXmlFile_01 - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("MappingFromXmlFile_01 - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("MappingFromXmlFile_01 - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileNameSingleGuarantor);

		log.info("VP: 'Number of guarantors found in the file doesn�t match' message isn't displayed");
		verifyFalse(transactionPage.isWarningDisplayedCorrectly(driver, "Number of guarantors found in the file doesn�t match"));
		
		log.info("VP: Guarantors are imported correctly");
		verifyTrue(transactionPage.isGuarantorMappingFromXmlFileCorrectly(xmlFileNameSingleGuarantor));
	}
	
	@Test(groups = { "regression" }, description = "Mapping From Xml File 03 - Import Multi Guarantors with Number isn't match")
	public void MappingFromXmlFile_02_ImportMultiGuarantorsNumberNotMatch() {
		
		log.info("MappingFromXmlFile_02 - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("MappingFromXmlFile_02 - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("MappingFromXmlFile_02 - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("MappingFromXmlFile_02 - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileNameMultiGuarantorNotMatchNumber);

		log.info("VP: 'Number of guarantors found in the file doesn�t match' message isn't displayed");
		verifyTrue(transactionPage.isWarningDisplayedCorrectly(driver, "Number of guarantors found in the file doesn�t match"));
		
		log.info("VP: Guarantors are imported correctly");
		verifyTrue(transactionPage.isGuarantorMappingFromXmlFileCorrectly(xmlFileNameMultiGuarantorNotMatchNumber));
	}
	
	@Test(groups = { "regression" }, description = "Mapping From Xml File 03 - Import Multi Guarantors with Number matches")
	public void MappingFromXmlFile_03_ImportMultiGuarantorsNumberMatches() {
		
		log.info("MappingFromXmlFile_03 - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("MappingFromXmlFile_03 - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("MappingFromXmlFile_03 - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("MappingFromXmlFile_03 - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileNameMultiGuarantorMatchNumber);

		log.info("VP: 'Number of guarantors found in the file doesn�t match' message isn't displayed");
		verifyFalse(transactionPage.isWarningDisplayedCorrectly(driver, "Number of guarantors found in the file doesn�t match"));
		
		log.info("VP: Guarantors are imported correctly");
		verifyTrue(transactionPage.isGuarantorMappingFromXmlFileCorrectly(xmlFileNameMultiGuarantorMatchNumber));
	}
	
	@Test(groups = { "regression" }, description = "Mapping From Xml File 04 - Import xml without guarantor")
	public void MappingFromXmlFile_04_ImportNoGuarantor() {
		
		log.info("MappingFromXmlFile_04 - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("MappingFromXmlFile_04 - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("MappingFromXmlFile_04 - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("MappingFromXmlFile_04 - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileNameNoGuarantor);

		log.info("VP: 'Number of guarantors found in the file doesn�t match' message isn't displayed");
		verifyFalse(transactionPage.isWarningDisplayedCorrectly(driver, "Number of guarantors found in the file doesn�t match"));
	}
	
	@Test(groups = { "regression" }, description = "Mapping From Xml File 03 - Import xml for checking Increase Type")
	public void MappingFromXmlFile_05_ImportIncreaseType() {
		
		log.info("MappingFromXmlFile_05 - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("MappingFromXmlFile_05 - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("MappingFromXmlFile_05 - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("MappingFromXmlFile_05 - Step 04: Upload XML filename with CPI Multiplier is true");
		transactionPage.uploadTransactionXML(xmlFileNameCPIMultiplierTrue);
		
		log.info("MappingFromXmlFile_05 - Step 05: Open Lease Details tab");
		transactionPage.openLeaseDetailsTab();

		log.info("VP: Increase Type default value is CPIL");
		verifyTrue(transactionPage.isIncreaseTypeMappingFromXmlFileCorrectly(xmlFileNameCPIMultiplierTrue));
		
		log.info("VP: Increase Type 'Blank' item is non-selected");
		verifyTrue(transactionPage.isItemInComboboxDisabled(driver, "LeaseDetailsLeaseDetails_R1_IcreaseType", ""));
		
		log.info("MappingFromXmlFile_05 - Step 06: Open Lease Details tab");
		transactionPage.openBasicDetailsTab();
		
		log.info("MappingFromXmlFile_05 - Step 07: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("MappingFromXmlFile_05 - Step 08: Upload XML filename with CPI Multiplier is false");
		transactionPage.uploadTransactionXML(xmlFileNameCPIMultiplierFalse);

		log.info("MappingFromXmlFile_05 - Step 09: Open Lease Details tab");
		transactionPage.openLeaseDetailsTab();
		
		log.info("VP: Increase Type default value is blank");
		verifyTrue(transactionPage.isIncreaseTypeMappingFromXmlFileCorrectly(xmlFileNameCPIMultiplierFalse));
		
		log.info("MappingFromXmlFile_05 - Step 10: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Address is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsLeaseDetails_R1_IcreaseType", "Increase Type is required"));
		
		log.info("VP: Increase Type 'Blank' item is non-selected");
		verifyTrue(transactionPage.isItemInComboboxDisabled(driver, "LeaseDetailsLeaseDetails_R1_IcreaseType", ""));
	}
	
	@Test(groups = { "regression" }, description = "Mapping From Xml File 03 - Import xml for checking Original Commencement Date")
	public void MappingFromXmlFile_06_ImportOriginalCommencementDate() {
		
		log.info("MappingFromXmlFile_06 - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);

		log.info("MappingFromXmlFile_06 - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();
		
		log.info("MappingFromXmlFile_06 - Step 03: Get Original commencement Date");
		originalCommencementDate = transactionPage.getOriginalCommencementDate();
		
		log.info("MappingFromXmlFile_06 - Step 04: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("MappingFromXmlFile_06 - Step 05: Upload XML filename with Exists Lease Prior is 'True'");
		transactionPage.uploadTransactionXML(xmlFileNameLeasePriorTrue);
		
		log.info("VP: Original Commencement Date mapping correctly");
		verifyTrue(transactionPage.isOriginalCommencementDateMappingFromXmlFileCorrectly(xmlFileNameLeasePriorTrue, originalCommencementDate));
		
		log.info("MappingFromXmlFile_06 - Step 06: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("MappingFromXmlFile_06 - Step 07: Upload XML filename with Exists Lease Prior is 'Yes'");
		transactionPage.uploadTransactionXML(xmlFileNameLeasePriorYes);
		
		log.info("VP: Original Commencement Date mapping correctly");
		verifyTrue(transactionPage.isOriginalCommencementDateMappingFromXmlFileCorrectly(xmlFileNameLeasePriorYes, originalCommencementDate));
		
		log.info("MappingFromXmlFile_06 - Step 08: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("MappingFromXmlFile_06 - Step 09: Upload XML filename with Exists Lease Prior is 'False'");
		transactionPage.uploadTransactionXML(xmlFileNameLeasePriorFalse);
		
		log.info("VP: Original Commencement Date mapping correctly");
		verifyTrue(transactionPage.isOriginalCommencementDateMappingFromXmlFileCorrectly(xmlFileNameLeasePriorFalse, originalCommencementDate));
		
		log.info("MappingFromXmlFile_06 - Step 10: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("MappingFromXmlFile_06 - Step 11: Upload XML filename with Exists Lease Prior is 'No'");
		transactionPage.uploadTransactionXML(xmlFileNameLeasePriorNo);
		
		log.info("VP: Original Commencement Date mapping correctly");
		verifyTrue(transactionPage.isOriginalCommencementDateMappingFromXmlFileCorrectly(xmlFileNameLeasePriorNo, originalCommencementDate));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileNameSingleGuarantor, xmlFileNameMultiGuarantorNotMatchNumber, xmlFileNameMultiGuarantorMatchNumber, xmlFileNameNoGuarantor, xmlFileNameCPIMultiplierTrue, xmlFileNameCPIMultiplierFalse;
	private String xmlFileNameLeasePriorFalse, xmlFileNameLeasePriorNo, xmlFileNameLeasePriorTrue, xmlFileNameLeasePriorYes;
	private String originalCommencementDate;
}