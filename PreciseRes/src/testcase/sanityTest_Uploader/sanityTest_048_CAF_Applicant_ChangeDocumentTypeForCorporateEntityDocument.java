package sanityTest_Uploader;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Constant;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.UploaderLoginPage;
import page.UploaderPage;

public class sanityTest_048_CAF_Applicant_ChangeDocumentTypeForCorporateEntityDocument extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameMember = "auto-CAF-member2014";
		passwordMember = "change";
		usernameApplicant = Constant.USERNAME_CAF_BORROWER;
		passwordApplicant = Constant.PASSWORD_CAF_BORROWER;
		loanName = "UploaderUdiTesting"+getUniqueNumber();
		applicantName = "Udi Dorner";
		borrowerName = "UdiTeamBorrower";
		loanStatus = Constant.LOAN_STATUS_CAF;
		fileNameDocument1 = "datatest1.pdf";
		fileNameDocument2 = "datatest2.pdf";
		editFileNameDocument1 = "editdatatest1.pdf";
		sectionName = "Borrower - UdiTeamBorrower";
		documentType1 = "Operating Agreement";
		documentType2 = "Certificate of Good Standing";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}
	
	@Test(groups = { "regression" },description = "UploaderLender36 - Change document file name")
	public void Uploader_33_ChangeDocumentFileName()
	{
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameMember, passwordMember, false);
		
		log.info("Precondition 02. Create new loans and get loans' id");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.createNewLoan(loanName, applicantName, borrowerName, loanStatus);
		
		log.info("Precondition 03. Click on logged user name on the top right corner");
//        loansPage.clickOnLoggedUserName( driver );
//        uploaderPageUrl = loansPage .getUploaderPageUrl( driver);
		
		log.info("Precondition 04. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("Precondition 05. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameApplicant, passwordApplicant);
		
		log.info("Uploader_33 - Step 01. Select loans item");
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("Uploader_33 - Step 02. Select folder Corporate Entity Documents");
		uploaderPage.selectSubFolderLevel2("Corporate Entity Documents");
		
		log.info("VP: There is a folder in Corporate Entity Documents folder with 'Borrower - UdiTeamBorrower' name");
		verifyTrue(uploaderPage.isSubFolderLevel3Display(sectionName));
		
		log.info("Uploader_33 - Step 03. Select sub-folder 'Borrower - UdiTeamBorrower'");
		uploaderPage.selectSubFolderLevel3(sectionName);
		
		log.info("Uploader_33 - Step 04. Upload document for a document type");
		uploaderPage.clickOnSelectNewFileIconByType(documentType1);
		uploaderPage.uploadFileOnUploader(fileNameDocument1);
		
		log.info("Uploader_33 - Step 05. Wait for upload complete");
		uploaderPage.waitForUploadCompleteOnUploader(fileNameDocument1);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(fileNameDocument1));
		
		log.info("VP: The document type display correctly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, documentType1));
		
		log.info("Uploader_33 - Step 06. Update document type for document");
		uploaderPage.clickOnEditDocumentIcon(fileNameDocument1);
		
		log.info("Uploader_33 - Step 07. Select new document type");
		uploaderPage.typeDocumentName(editFileNameDocument1);
		
		log.info("Uploader_33 - Step 08. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.clickYesButton();
		
		//Bug ->Change file to 'Unknown'
		log.info("VP: The document type display correctly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(editFileNameDocument1, documentType1));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender37 - Upload new document")
	public void Uploader_34_UploadNewDocument()
	{
		log.info("Uploader_34 - Step 01. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherDocuments();
		
		log.info("Uploader_34 - Step 02. Select file to upload");
		uploaderPage.uploadFileOnUploader(fileNameDocument2);
//		uploaderPage.waitForAddOtherDocument(fileNameDocument2);
			
		log.info("Uploader_34 - Step 03. Click 'OK' Button");
//		uploaderPage.clickOkButtonOnKeywords();
		uploaderPage.waitForUploadCompleteOnUploader(fileNameDocument2);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(fileNameDocument2));
		
		log.info("VP: The document type display correctly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument2, "Unknown"));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender38 - View document file")
	public void Uploader_35_ViewDocumentFile()
	{
		if(!driver.toString().toLowerCase().contains("internetexplorer")){
		log.info("Uploader_35 - Step 01. Click on 'Eye' icon to view document");
		uploaderPage.clickOnViewDocumentIcon(fileNameDocument2);
		
		log.info("VP: The document is openned to view");
		verifyTrue(uploaderPage.isDocumentViewable(fileNameDocument2));
		}
	}
	
	@Test(groups = { "regression" },description = "UploaderLender39 - Change document type using 'pencil' option")
	public void Uploader_36_ChangeDocumentTypeUsingPencilOption()
	{
		log.info("Uploader_36 - Step 01. Click on 'pencil' icon to edit document");
		uploaderPage.clickOnEditDocumentIcon(editFileNameDocument1);
		
		log.info("Uploader_36 - Step 02. Select new document type");
		uploaderPage.selectDocumentType(documentType2);
		
		log.info("Uploader_36 - Step 03. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.clickYesButton();
		
		log.info("VP: The document type display correctly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(editFileNameDocument1, documentType2));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private LoansPage loansPage;
	private String usernameMember, passwordMember, applicantName, borrowerName, loanStatus;
	private String usernameApplicant, passwordApplicant;
	private String loanName, uploaderPageUrl, fileNameDocument2;
	private String fileNameDocument1, documentType1, documentType2, editFileNameDocument1;
	private String sectionName;
}