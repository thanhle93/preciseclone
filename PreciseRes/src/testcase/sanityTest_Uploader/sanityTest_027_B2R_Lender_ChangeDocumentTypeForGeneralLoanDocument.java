package sanityTest_Uploader;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.UploaderLoginPage;
import page.UploaderPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_027_B2R_Lender_ChangeDocumentTypeForGeneralLoanDocument extends AbstractTest{

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port){

		driver  = openBrowser(browser, port, ipClient);		
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		loanName = "UploaderUdiTesting"+getUniqueNumber();
		applicantName = "Consolidation Demo Borrower New";
		borrowerName = "UdiTeamBorrower";
		loanStatus = Constant.LOAN_STATUS_B2R;
		fileNameDocument1 = "datatest1.pdf";
		fileNameDocument2 = "datatest2.pdf";
		editFileNameDocument1 = "editdatatest1.pdf";
		sectionName = "Property Management";
		documentType1 = "L32 - Management Agreement";
		documentType2 = "L33 - Management Company Certificate of Formation";
		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
	}
	
	@Test(groups = { "regression" },description = "UploaderLender32 - Change document file name")
	public void Uploader_28_ChangeDocumentFileName()
	{
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);
		
		log.info("Precondition 02. Create new loans and get loans' id");
		loansPage = homePage.openLoansPage(driver, ipClient);
		loansPage.createNewLoan(loanName, applicantName, borrowerName, loanStatus, loanName);
		
		log.info("Precondition 03. Click on logged user name on the top right corner");
//		loansPage.clickOnLoggedUserName(driver);
//		uploaderPageUrl = loansPage.getUploaderPageUrl(driver);
//		uploaderPageUrl = Constant.UPLOADER_PAGE_URL;
		
		log.info("Precondition 04. Go to Uploader");
		loginPage = logout(driver, ipClient);
		uploaderLoginPage = loginPage.gotoUploaderPage(driver, ipClient, uploaderPageUrl);
		
		log.info("Precondition 05. Login with correct user name, password on Uploader page");
		uploaderPage = uploaderLoginPage.loginUploader(usernameLender, passwordLender);
		
		log.info("Uploader_28 - Step 01. Select loans item");
		uploaderPage.clickChangeGroupedView();
		uploaderPage.searchLoanItem(loanName);
		uploaderPage.selectLoanItem(loanName);
		
		log.info("Uploader_28 - Step 02. Select folder General Loan Documents");
		uploaderPage.selectSubFolderLevel2("General Loan Documents");
		
		log.info("VP: There is a folder in General Loan Documents folder with 'Property Management' name");
		verifyTrue(uploaderPage.isSubFolderLevel3Display(sectionName));
		
		log.info("Uploader_28 - Step 03. Select sub-folder Property Management");
		uploaderPage.selectSubFolderLevel3(sectionName);
		
		log.info("Uploader_28 - Step 04. Upload document for a document type");
		uploaderPage.clickOnSelectNewFileIconByType(documentType1);
		uploaderPage.uploadFileOnUploader(fileNameDocument1);
		
		log.info("Uploader_28 - Step 05. Wait for upload complete");
		uploaderPage.waitForUploadCompleteOnUploader(fileNameDocument1);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(fileNameDocument1));
		
		log.info("VP: The document type display correctly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument1, documentType1));
		
		log.info("Uploader_28 - Step 06. Update document type for document");
		uploaderPage.clickOnEditDocumentIcon(fileNameDocument1);
		
		log.info("Uploader_28 - Step 07. Type new document type");
		uploaderPage.typeDocumentName(editFileNameDocument1);
		
		log.info("Uploader_28 - Step 08. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.clickYesButton();
		
		log.info("VP: The document type display correctly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(editFileNameDocument1, documentType1));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender33- Upload new document")
	public void Uploader_29_UploadNewDocument()
	{
		log.info("Uploader_29 - Step 01. Click 'Add Other Documents' button");
		uploaderPage.clickAddAnotherDocuments();
		
		log.info("Uploader_29 - Step 02. Select file to upload");
		uploaderPage.uploadFileOnUploader(fileNameDocument2);
			
		log.info("Uploader_29 - Step 03. Wait for Upload completed");
		uploaderPage.waitForUploadCompleteOnUploader(fileNameDocument2);
		
		log.info("VP: The document is uploaded successfully");
		verifyTrue(uploaderPage.isDocumentDisplay(fileNameDocument2));
		
		log.info("VP: The document type display correctly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(fileNameDocument2, "Unknown"));
	}
	
	@Test(groups = { "regression" },description = "UploaderLender34 - View document file")
	public void Uploader_30_ViewDocumentFile()
	{
		if(!driver.toString().toLowerCase().contains("internetexplorer")){
		log.info("Uploader_30 - Step 01. Click on 'Eye' icon to view document");
		uploaderPage.clickOnViewDocumentIcon(fileNameDocument2);
		
		log.info("VP: The document is openned to view");
		verifyTrue(uploaderPage.isDocumentViewable(fileNameDocument2));
		}
	}
	
	@Test(groups = { "regression" },description = "UploaderLender35 - Change document type using 'pencil' option")
	public void Uploader_31_ChangeDocumentTypeUsingPencilOption()
	{
		log.info("Uploader_31 - Step 01. Click on 'pencil' icon to edit document");
		uploaderPage.clickOnEditDocumentIcon(editFileNameDocument1);
		
		log.info("Uploader_31 - Step 02. Select new document type");
		uploaderPage.selectDocumentType(documentType2);
		
		log.info("Uploader_31 - Step 03. Click Ok button");
		uploaderPage.clickOkButtonOnEditDocument();
		uploaderPage.clickYesButton();
		
		log.info("VP: The document type display corretly");
		verifyTrue(uploaderPage.isDocumentTypeDisplay(editFileNameDocument1, documentType2));
	}

	@Test(groups = { "regression" },description = "UploaderLender36 - Delete document type")
	public void Uploader_32_DeleteDocumentType()
	{
		log.info("Uploader_32 - Step 01. Delete document");
		uploaderPage.clickOnDeleteDocumentIcon(editFileNameDocument1);
		
		log.info("Uploader_32 - Step 02. Click Yes button");
		uploaderPage.clickYesButton();
		
		log.info("VP: The document is deleted successfully");
		verifyFalse(uploaderPage.isDocumentDisplay(editFileNameDocument1));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {	
		logout(driver, ipClient);
		closeBrowser(driver);
	}
	
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private UploaderPage uploaderPage;
	private UploaderLoginPage uploaderLoginPage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender, applicantName, borrowerName, loanStatus;
	private String loanName, uploaderPageUrl, fileNameDocument2;
	private String fileNameDocument1, documentType1, documentType2, editFileNameDocument1;
	private String sectionName;
}