package page;

import org.openqa.selenium.WebDriver;

public class PageFactory {
	
	/**
	 * Get login page
	 * @param driver, ipClient
	 * @return Login page
	 */
	public static LoginPage getLoginPage(WebDriver driver, String ipClient)
	{
		return new LoginPage(driver, ipClient);
	}
	
	/**
	 * Get Home page
	 * @param driver, ipClient
	 * @return Home page
	 */
	public static HomePage getHomePage(WebDriver driver, String ipClient)
	{
		return new HomePage(driver, ipClient);
	}
	
	/**
	 * Get Mail Home page
	 * @param driver, ipClient
	 * @return Mail Homepage
	 */
	public static MailHomePage getMailHomePage(WebDriver driver, String ipClient)
	{
		return new MailHomePage(driver, ipClient);
	}
	
	/**
	 * Get SalesForce Home page
	 * @param driver, ipClient
	 * @return SalesForce Homepage
	 */
	public static SalesForceHomePage getSalesForceHomePage(WebDriver driver, String ipClient)
	{
		return new SalesForceHomePage(driver, ipClient);
	}
	
	/**
	 * Get Mail Login page
	 * @param driver, ipClient
	 * @return Mail Home page
	 */
	public static MailLoginPage getMailLoginPage(WebDriver driver, String ipClient)
	{
		return new MailLoginPage(driver, ipClient);
	}
	
	/**
	 * Get Salesforce Login page
	 * @param driver, ipClient
	 * @return Mail Home page
	 */
	public static SalesForceLoginPage getSalesforceLoginPage(WebDriver driver, String ipClient)
	{
		return new SalesForceLoginPage(driver, ipClient);
	}
	
	/**
	 * Get Salesforce Account page
	 * @param driver, ipClient
	 * @return Salesforce Account page
	 */
	public static SalesForceAccountPage getSalesforceAccountPage(WebDriver driver, String ipClient)
	{
		return new SalesForceAccountPage(driver, ipClient);
	}
	
	/**
	 * Get Salesforce Contacts page
	 * @param driver, ipClient
	 * @return Salesforce Contacts page
	 */
	public static SalesForceContactsPage getSalesforceContactsPage(WebDriver driver, String ipClient)
	{
		return new SalesForceContactsPage(driver, ipClient);
	}
	
	/**
	 * Get Salesforce Deals page
	 * @param driver, ipClient
	 * @return Salesforce Deals page
	 */
	public static SalesForceDealsPage getSalesforceDealsPage(WebDriver driver, String ipClient)
	{
		return new SalesForceDealsPage(driver, ipClient);
	}
	
	/**
	 * Get Loans page
	 * @param driver, ipClient
	 * @return Loans page
	 */
	public static LoansPage getLoansPage(WebDriver driver, String ipClient)
	{
		return new LoansPage(driver, ipClient);
	}
	
	/**
	 * Get Uploader page
	 * @param driver, ipClient
	 * @return Uploader page
	 */
	public static UploaderPage getUploaderPage(WebDriver driver, String ipClient)
	{
		return new UploaderPage(driver, ipClient);
	}
	
	/**
	 * Get UploaderLogin page
	 * @param driver, ipClient
	 * @return UploaderLogin page
	 */
	public static UploaderLoginPage getUploaderLoginPage(WebDriver driver, String ipClient)
	{
		return new UploaderLoginPage(driver, ipClient);
	}
	
	/**
	 * Get Applicants page
	 * @param driver, ipClient
	 * @return Applicants page
	 */
	public static ApplicantsPage getApplicantsPage(WebDriver driver, String ipClient)
	{
		return new ApplicantsPage(driver, ipClient);
	}
	
	
	/**
	 * Get Loan Dashboard [CAF-FNF] page
	 * @param driver, ipClient
	 * @return Applicant page
	 */
	public static FNFLoanDashboardPage getLoanDashboardPage(WebDriver driver, String ipClient)
	{
		return new FNFLoanDashboardPage(driver, ipClient);
	}
	
	/**
	 * Get Rehabs [CAF-FNF] page
	 * @param driver, ipClient
	 * @return Rehabs page
	 */
	public static FNFRehabsPage getRehabsPageFNF(WebDriver driver, String ipClient)
	{
		return new FNFRehabsPage(driver, ipClient);
	}
	
	/**
	 * Get Documents [CAF-FNF] page
	 * @param driver, ipClient
	 * @return Document page
	 */
	public static FNFDocumentPage getDocumentsPageFNF(WebDriver driver, String ipClient)
	{
		return new FNFDocumentPage(driver, ipClient);
	}
	
	/**
	 * Get Notification Templates page
	 * @param driver, ipClient
	 * @return Notification Template page
	 */
	public static NotificationTemplatesPage getNotificationTemplatesPage(WebDriver driver, String ipClient)
	{
		return new NotificationTemplatesPage(driver, ipClient);
	}
	
	/**
	 * Get Borrowers page [CAF-FNF]
	 * @param driver, ipClient
	 * @return Borrowers page
	 */
	public static FNFBorrowersPage getFNFBorrowersPage(WebDriver driver, String ipClient)
	{
		return new FNFBorrowersPage(driver, ipClient);
	}
	
	/**
	 * Get User Logins page
	 * @param driver, ipClient
	 * @return UserLogins page
	 */
	public static UserLoginsPage getUserLoginsPage(WebDriver driver, String ipClient)
	{
		return new UserLoginsPage(driver, ipClient);
	}
	
	/**
	 * Get Terms Of Use page
	 * @param driver, ipClient
	 * @return Terms Of Use page
	 */
	public static TermsOfUsePage getTermsOfUsePage(WebDriver driver, String ipClient)
	{
		return new TermsOfUsePage(driver, ipClient);
	}
	
	/**
	 * Get Document Security page
	 * @param driver, ipClient
	 * @return Document Security page
	 */
	public static DocumentSecurityPage getDocumentSecurityPage(WebDriver driver, String ipClient)
	{
		return new DocumentSecurityPage(driver, ipClient);
	}
	
	/**
	 * Get Documents page
	 * @param driver, ipClient
	 * @return Documents page
	 */
	public static DocumentsPage getDocumentsPage(WebDriver driver, String ipClient)
	{
		return new DocumentsPage(driver, ipClient);
	}
	
	/**
	 * Get Properties page
	 * @param driver, ipClient
	 * @return Properties page
	 */
	public static PropertiesPage getPropertiesPage(WebDriver driver, String ipClient)
	{
		return new PropertiesPage(driver, ipClient);
	}
	
	/**
	 * Get Lenders page
	 * @param driver, ipClient
	 * @return Lenders page
	 */
	public static LendersPage getLendersPage(WebDriver driver, String ipClient)
	{
		return new LendersPage(driver, ipClient);
	}
	
	/**
	 * Get My Information page
	 * @param driver, ipClient
	 * @return My Information page
	 */
	public static MyInformationPage getMyInformationPage(WebDriver driver, String ipClient)
	{
		return new MyInformationPage(driver, ipClient);
	}
	
	/**
	 * Get Check Lists page
	 * @param driver, ipClient
	 * @return Check Lists page
	 */
	public static CheckListsPage getCheckListsPage(WebDriver driver, String ipClient)
	{
		return new CheckListsPage(driver, ipClient);
	}
	
	/**
	 * Get My Information page
	 * @param driver, ipClient
	 * @return My Information page
	 */
	public static FNFBorrowersPage getFNFMyInformationPage(WebDriver driver, String ipClient)
	{
		return new FNFBorrowersPage(driver, ipClient);
	}
	
	/**
	 * Get CAFFNF Properties page
	 * @param driver, ipClient
	 * @return CAFFNF Properties page
	 */
	public static FNFPropertiesPage getFNFPropertiesPage(WebDriver driver, String ipClient)
	{
		return new FNFPropertiesPage(driver, ipClient);
	}
	
	/**
	 * Get Salesforce Lookup Page
	 * @param driver, ipclient
	 * @return Salesforce Lookup Page
	 */
	public static SalesForceLookupPage getSalesforceLookupPage(WebDriver driver, String ipClient)
	{
		return new SalesForceLookupPage(driver, ipClient);
	}
	
	/**
	 * Get FNF Loans Page
	 * @param driver, ipclient
	 * @return FNF Loans Page
	 */
	public static FNFLoansPage getFNFLoansPage(WebDriver driver, String ipClient)
	{
		return new FNFLoansPage(driver, ipClient);
	}
	
	/**
	 * Get Transaction page
	 * @param driver, ipClient
	 * @return Transaction page
	 */
	public static TransactionPage getTransactionPage(WebDriver driver, String ipClient)
	{
		return new TransactionPage(driver, ipClient);
	}
	
	/**
	 * Get Report page
	 * @param driver, ipClient
	 * @return Report page
	 */
	public static ReportsPage getReportsPage(WebDriver driver, String ipClient)
	{
		return new ReportsPage(driver, ipClient);
	}
	
	/**
	 * Get FNF Documents Page
	 * @param driver, ipclient
	 * @return FNF Documents Page
	 */
	public static FNFDocumentPage getFNFDocumentsPage(WebDriver driver, String ipClient)
	{
		return new FNFDocumentPage(driver, ipClient);
	}
	
	/**
	 * Get Users page
	 * @param driver, ipClient
	 * @return Users page
	 */
	public static UsersPage getUsersPage(WebDriver driver, String ipClient)
	{
		return new UsersPage(driver, ipClient);
	}
	
	/**
	 * Get Admin page
	 * @param driver, ipClient
	 * @return Admin page
	 */
	public static AdminPage getAdminPage(WebDriver driver, String ipClient)
	{
		return new AdminPage(driver, ipClient);
	}
}