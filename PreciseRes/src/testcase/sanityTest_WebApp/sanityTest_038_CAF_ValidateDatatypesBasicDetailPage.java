package sanityTest_WebApp;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;

import common.AbstractTest;
import common.Constant;

public class sanityTest_038_CAF_ValidateDatatypesBasicDetailPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		applicantName = "CAF Demo Applicant";
		loanStatus = "Term Sheet Issued";
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan"+accountNumber;
		invalidCurrency = "$40,0.00*%#$fail";
		invalidDate = "4/21/2015*%#$fail";
		invalidContactNumber = "1234";
		invalidEmail = "abc@gmail,com";
		state = "AA - Federal Services";
		companyType = "Non Profit Corporation";
		validCurrency = "$2,000,000.00";
		validDate = "12/31/2000";
		validPhoneNumber = "12345678901";
		validZipCode = "12345";
		validText = "Common value !@#";
		validEmail = "abc@gmail.com";
		invalidNumber = "123.12abc@";
		validWholeNumber = "123";
		validNumber = "12.31";
		loanTerm = "5 year";
	}

	@Test(groups = { "regression" }, description = "Validate Datatypes 01 - No input the data")
	public void ValidataDatatypes_01_NoInputTheData() {
		
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Precondition 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 02. Create new loans and get loans' id");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus);
		
		log.info("ValidataDatatypes_01 - Step 01. Input Loan amount requested empty");
		loansPage.inputLoanAmountRequested("");
		
		log.info("ValidataDatatypes_01 - Step 02. Input Floor empty");
		loansPage.inputFloor("");
		
		log.info("ValidataDatatypes_01 - Step 03. Select Bridge Borrower empty");
		loansPage.selectBridgeBorrower("");
		
		log.info("ValidataDatatypes_02 - Step 04. Click save button");
		loansPage.clickSaveButtonValidDataType();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(loansPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Loan Amount Requested is required' message is displayed");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_LoanAmount", "Loan Amount Requested is required"));
		
		log.info("VP: 'Bridge Borrower is required' message is displayed");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_BridgeBorrower", "Bridge Borrower is required"));
		
		log.info("VP: 'Floor % is required' message is displayed");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_Floor", "Floor % is required"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 02 - Input the invalid/ incorrect at Currency fields")
	public void ValidataDatatypes_02_InputInvalidOrIncorrectAtCurrencyFields() {
		
		log.info("ValidataDatatypes_02 - Step 01. Search for loan");
		loansPage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		
		log.info("ValidataDatatypes_02 - Step 02. Open loan basic detail tab");
		loansPage.openLoansDetailPage(loanName);
		
		log.info("ValidataDatatypes_02 - Step 03. Input Loan Amount Requested");
		loansPage.inputLoanAmountRequested(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 04. Click save button");
		loansPage.clickSaveButtonValidDataType();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(loansPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Currency value must be a number' message is displayed");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_LoanAmount", "Currency value must be a number"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 03 - Input the invalid/ incorrect at Date fields")
	public void ValidataDatatypes_03_InputInvalidOrIncorrectAtDateFields() {
		
		log.info("Precondition - Step 01. Search for loan");
		loansPage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		
		log.info("Precondition - Step 02. Open loan basic detail tab");
		loansPage.openLoansDetailPage(loanName);
		
		log.info("ValidataDatatypes_03 - Step 01: Input the invalid data to 'Term Sheet Issued Date ' field");
		loansPage.inputTermSheetIssuedDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 02: Input the invalid data to 'Term Sheet Signed Date ");
		loansPage.inputTermSheetSignedDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 02: Input the invalid data to 'Anticipated IC Approval ");
		loansPage.inputAnticipatedICApprovalDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 02: Input the invalid data to 'Date Established ");
		loansPage.inputDateEstablished(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 02: Input the invalid data to 'Expected Close Date ");
		loansPage.inputExpectedCloseDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 02: Input the invalid data to 'Kick Off Date ");
		loansPage.inputKickOffDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 05: Click 'Save' button");
		loansPage.clickSaveButtonValidDataType();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(loansPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Term Sheet Issued Date ' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_TermsheetDate", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Term Sheet Signed Date' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_TermSheetSignedDate", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Anticipated IC Approval' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_AnticipatedICApproval", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Date Established' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_DateEstablish", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Expected Close Date' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_ProjectedCloseDate", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Kick Off Date' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_KickOffDate", "Value must be a date"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 04 - Input the invalid/ incorrect at Contact fields")
	public void ValidataDatatypes_04_InputInvalidOrIncorrectAtContactFields() {
		
		log.info("Precondition - Step 01. Search for loan");
		loansPage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		
		log.info("Precondition - Step 02. Open loan basic detail tab");
		loansPage.openLoansDetailPage(loanName);
		
		log.info("ValidataDatatypes_04 - Step 01: Input the invalid data to 'Phone' field");
		loansPage.inputPhoneTextbox(invalidContactNumber);
		
		log.info("ValidataDatatypes_04 - Step 02: Input the invalid data to 'Zip' field");
		loansPage.inputZipcode(invalidContactNumber);
		
		log.info("ValidataDatatypes_04 - Step 03: Input the invalid data to 'Email' field");
		loansPage.inputEmailTextbox(invalidEmail);
		
		log.info("ValidataDatatypes_04 - Step 04: Click 'Save' button");
		loansPage.clickSaveButton();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(loansPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Value must be a whole number' message is displayed at 'Phone' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_Phone", "Not a valid Phone Number."));
		
		log.info("VP: 'Value must be a whole number' message is displayed at 'Zip' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_Zip", "Enter a valid ZIP or ZIP+4 code"));
		
		log.info("VP: 'Value left of % symbol must be a number' message is displayed at 'Email' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_Email", "Enter a valid email address"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 04 - Input the invalid/ incorrect at Number fields")
	public void ValidataDatatypes_05_InputInvalidOrIncorrectAtNumberFields() {
		
		log.info("Precondition - Step 01. Search for loan");
		loansPage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		
		log.info("Precondition - Step 02. Open loan basic detail tab");
		loansPage.openLoansDetailPage(loanName);
		
		log.info("ValidataDatatypes_05 - Step 01: Input the invalid data to 'Floor' field");
		loansPage.inputFloor(invalidNumber);
		
		log.info("ValidataDatatypes_05 - Step 02: Input the invalid data to 'Amortization Term' field");
		loansPage.inputAmortizationTerm(invalidNumber);
		
		log.info("ValidataDatatypes_05 - Step 04: Input the invalid data to 'IO Term' field");
		loansPage.inputIOTerm(invalidNumber);
		
		log.info("ValidataDatatypes_05 - Step 05: Input the invalid data to 'Margin' field");
		loansPage.inputMargin(invalidNumber);
		
		log.info("ValidataDatatypes_05 - Step 06: Input the invalid data to 'LTV' field");
		loansPage.inputLTV(invalidNumber);
		
		log.info("ValidataDatatypes_05 - Step 07: Click 'Save' button");
		loansPage.clickSaveButton();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(loansPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Value left of % symbol must be a number' message is displayed at 'Floor' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_Floor", "Value left of % symbol must be a number"));
		
		log.info("VP: 'Value must be a whole number' message is displayed at 'Amortization Term' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_AmortTerm", "Value must be a whole number"));
		
		log.info("VP: 'Value must be a whole number' message is displayed at 'IO Term' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_IOTerm", "Value must be a whole number"));
		
		log.info("VP: 'Value must be a number' message is displayed at 'Margin' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_Margin", "Value must be a number"));
		
		log.info("VP: 'Value must be a number' message is displayed at 'LTV' field");
		verifyTrue(loansPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationDetails_R1_LTV", "Value must be a number"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 06 - Input the valid into all fields")
	public void ValidataDatatypes_06_InputValidIntoAllFields() {
		
		log.info("Precondition - Step 01. Search for loan");
		loansPage.openLoansPage(driver, ipClient);
		loansPage.searchLoanByName(loanName);
		
		log.info("Precondition - Step 02. Open loan basic detail tab");
		loansPage.openLoansDetailPage(loanName);
		
		log.info("ValidataDatatypes_02 - Step 01. Input Loan Amount Requested");
		loansPage.inputLoanAmountRequested(validCurrency);
		
		log.info("ValidataDatatypes_03 - Step 02: Input the invalid data to 'Term Sheet Issued Date ' field");
		loansPage.inputTermSheetIssuedDate(validDate);
		
		log.info("ValidataDatatypes_03 - Step 03: Input the invalid data to 'Term Sheet Signed Date ");
		loansPage.inputTermSheetSignedDate(validDate);
		
		log.info("ValidataDatatypes_03 - Step 04: Input the invalid data to 'Anticipated IC Approval ");
		loansPage.inputAnticipatedICApprovalDate(validDate);
		
		log.info("ValidataDatatypes_03 - Step 05: Input the invalid data to 'Date Established ");
		loansPage.inputDateEstablished(validDate);
		
		log.info("ValidataDatatypes_03 - Step 06: Input the invalid data to 'Expected Close Date ");
		loansPage.inputExpectedCloseDate(validDate);
		
		log.info("ValidataDatatypes_03 - Step 07: Input the invalid data to 'Kick Off Date ");
		loansPage.inputKickOffDate(validDate);
		
		log.info("ValidataDatatypes_04 - Step 08: Input the invalid data to 'Phone' field");
		loansPage.inputPhoneTextbox(validPhoneNumber);
		
		log.info("ValidataDatatypes_04 - Step 09: Input the invalid data to 'Zip' field");
		loansPage.inputZipcode(validZipCode);
		
		log.info("ValidataDatatypes_04 - Step 10: Input the invalid data to 'Email' field");
		loansPage.inputEmailTextbox(validEmail);
		
		log.info("ValidataDatatypes_05 - Step 11: Input the invalid data to 'Floor' field");
		loansPage.inputFloor(validNumber);
		
		log.info("ValidataDatatypes_05 - Step 12: Input the invalid data to 'Amortization Term' field");
		loansPage.inputAmortizationTerm(validWholeNumber);
		
		log.info("ValidataDatatypes_05 - Step 13: Input the invalid data to 'Loan Term' field");
		loansPage.selectLoanTerm(loanTerm);
		
		log.info("ValidataDatatypes_05 - Step 14: Input the invalid data to 'IO Term' field");
		loansPage.inputIOTerm(validWholeNumber);
		
		log.info("ValidataDatatypes_05 - Step 15: Input the invalid data to 'Margin' field");
		loansPage.inputMargin(validNumber);
		
		log.info("ValidataDatatypes_05 - Step 16: Input the invalid data to 'LTV' field");
		loansPage.inputLTV(validNumber);
		
		log.info("ValidataDatatypes_05 - Step 16: Input the invalid data to 'Loan ID' field");
		loansPage.inputLoanID(validText+accountNumber);
		
		log.info("ValidataDatatypes_05 - Step 17: Input the invalid data to 'Company website' field");
		loansPage.inputCompanyWebsite(validText);
		
		log.info("ValidataDatatypes_05 - Step 18: Input the invalid data to 'County' field");
		loansPage.inputCounty(validText);
		
		log.info("ValidataDatatypes_05 - Step 19: Input the invalid data to 'Comment' field");
		loansPage.inputCommentTextbox(validText);
		
		log.info("ValidataDatatypes_05 - Step 20: Input the invalid data to 'Call Protection' field");
		loansPage.inputCallProtection(validText);
		
		log.info("ValidataDatatypes_05 - Step 20: Input the invalid data to 'Place Of Formation' field");
		loansPage.inputPlaceOfFormation(validText);
		
		log.info("ValidataDatatypes_05 - Step 20: Input the invalid data to 'Primary Contact' field");
		loansPage.inputPrimaryContact(validText);
		
		log.info("ValidataDatatypes_05 - Step 20: Input the invalid data to 'Address' field");
		loansPage.inputAddress(validText);
		
		log.info("ValidataDatatypes_05 - Step 20: Input the invalid data to 'City' field");
		loansPage.inputCity(validText);
		
		log.info("ValidataDatatypes_05 - Step 21: Input the invalid data to 'Cancellation Reason' field");
		loansPage.inputCancellationReason(validText);
		
		log.info("ValidataDatatypes_05 - Step 22: Input the valid data to 'Foreign National' field");
		loansPage.selectForeignNational("Yes");
		
		log.info("ValidataDatatypes_05 - Step 23: Input the valid data to 'Loan Purpose' field");
		loansPage.selectLoanPurpose("Refi");
		
		log.info("ValidataDatatypes_05 - Step 24: Input the valid data to 'Refinance Type' field");
		loansPage.selectRefinanceType("Refi Debt");
		
		log.info("ValidataDatatypes_05 - Step 25: Input the valid data to 'Property Management' field");
		loansPage.selectPropertyManagement("Self-Managed");
		
		log.info("ValidataDatatypes_05 - Step 26: Input the valid data to 'Originator' field");
		loansPage.selectOriginator("Dennis Spivey");
		
		log.info("ValidataDatatypes_05 - Step 27: Input the valid data to 'Underwriter ' field");
		underwriter = loansPage.getFirstOptionUnderwriter();
		loansPage.selectUnderwriter(underwriter);
		
		log.info("ValidataDatatypes_05 - Step 28: Input the valid data to 'Loan Coordinator ' field");
		loanCoord = loansPage.getFirstOptionLoanCoord();
		loansPage.selectLoanCoordinator(loanCoord);
		
		log.info("ValidataDatatypes_05 - Step 29: Input the valid data to 'Closer' field");
		closer = loansPage.getFirstOptionCloser();
		loansPage.selectCloser(closer);
		
		log.info("ValidataDatatypes_05 - Step 30: Input the valid data to 'Company Type' field");
		loansPage.selectRecourse("Yes");
		
		log.info("ValidataDatatypes_05 - Step 31: Input the valid data to 'Bridge Borrower' field");
		loansPage.selectBridgeBorrower("Yes");
		
		log.info("ValidataDatatypes_05 - Step 32: Input the valid data to 'Company Type' field");
		loansPage.selectCompanyType(companyType);
		
		log.info("ValidataDatatypes_05 - Step 33: Input the valid data to 'State' field");
		loansPage.selectState(state);
		
		log.info("ValidataDatatypes_05 - Step 34: Input the valid data to 'Owner' field");
		loansPage.selectOwner("Dennis Spivey");
		
		log.info("ValidataDatatypes_05 - Step 35: Input the valid data to 'Source' field");
		loansPage.selectSource("Bridge Borrower");
		
		log.info("ValidataDatatypes_05 - Step 03: Input the invalid data to 'Loan Term' field");
		loansPage.selectLoanTerm(loanTerm);
		
		log.info("ValidataDatatypes_05 - Step 36: Click 'Save' button");
		loansPage.clickSaveButton();
		
		//Check data saved
		
		log.info("VP: Verify that Loan Amount Requested is saved successfully");
		verifyTrue(loansPage.isLoanAmountRequestedDisplayedCorrectly(validCurrency));
		
		log.info("VP: Verify that 'Term Sheet Issued Date ' field is saved successfully");
		verifyTrue(loansPage.isTermSheetIssuedDateDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that'Term Sheet Signed Date is saved successfully ");
		verifyTrue(loansPage.isTermSheetSignedDateDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that 'Anticipated IC Approval is saved successfully");
		verifyTrue(loansPage.isAnticipatedICApprovalDateDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that 'Date Established is saved successfully ");
		verifyTrue(loansPage.isDateEstablishedDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that 'Expected Close Date is saved successfully ");
		verifyTrue(loansPage.isExpectedCloseDateDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that 'Kick Off Date is saved successfully ");
		verifyTrue(loansPage.isKickOffDateDisplayedCorrectly(validDate));
		
		log.info("VP: Verify that 'Phone' field is saved successfully");
		verifyTrue(loansPage.isPhoneTextboxDisplayedCorrectly(validPhoneNumber));
		
		log.info("VP: Verify that 'Zip' field is saved successfully");
		verifyTrue(loansPage.isZipcodeDisplayedCorrectly(validZipCode));
		
		log.info("VP: Verify that 'Email' field is saved successfully");
		verifyTrue(loansPage.isEmailTextboxDisplayedCorrectly(validEmail));
		
		log.info("VP: Verify that 'Floor' field is saved successfully");
		verifyTrue(loansPage.isFloorDisplayedCorrectly(validNumber));
		
		log.info("VP: Verify that 'Amortization Term' field is saved successfully");
		verifyTrue(loansPage.isAmortizationTermDisplayedCorrectly(validWholeNumber));
		
		log.info("VP: Verify that 'Loan Term' field is saved successfully");
		verifyTrue(loansPage.isLoanTermDisplayedCorrectly(loanTerm));
		
		log.info("VP: Verify that 'IO Term' field is saved successfully");
		verifyTrue(loansPage.isIOTermDisplayedCorrectly(validWholeNumber));
		
		log.info("VP: Verify that 'Margin' field is saved successfully");
		verifyTrue(loansPage.isMarginDisplayedCorrectly(validNumber));
		
		log.info("VP: Verify that 'LTV' field is saved successfully");
		verifyTrue(loansPage.isLTVDisplayedCorrectly(validNumber));
		
		log.info("VP: Verify that 'Loan ID' field is saved successfully");
		verifyTrue(loansPage.isLoanIDDisplayed(validText+accountNumber));
		
		log.info("VP: Verify that 'Company website' field is saved successfully");
		verifyTrue(loansPage.isCompanyWebsiteDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'County' field is saved successfully");
		verifyTrue(loansPage.isCountyDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Comment' field is saved successfully");
		verifyTrue(loansPage.isCommentTextboxDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Call Protection' field is saved successfully");
		verifyTrue(loansPage.isCallProtectionDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Place of Formation ' field is saved successfully");
		verifyTrue(loansPage.isPlaceOfFormationDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Primary Contact ' field is saved successfully");
		verifyTrue(loansPage.isPrimaryContactDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Address' field is saved successfully");
		verifyTrue(loansPage.isAddressDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'City ' field is saved successfully");
		verifyTrue(loansPage.isCityDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Cancellation Reason' field is saved successfully");
		verifyTrue(loansPage.isCancellationReasonDisplayedCorrectly(validText));
		
		log.info("VP: Verify that 'Foreign National' field is saved successfully");
		verifyTrue(loansPage.isForeignNationalDisplayedCorrectly("Yes"));
		
		log.info("VP: Verify that 'Loan Purpose' field is saved successfully");
		verifyTrue(loansPage.isLoanPurposeDisplayedCorrectly("Refi"));
		
		log.info("VP: Verify that 'Refinance Type' field is saved successfully");
		verifyTrue(loansPage.isRefinanceTypeDisplayedCorrectly("Refi Debt"));
		
		log.info("VP: Verify that 'Property Management' field is saved successfully");
		verifyTrue(loansPage.isPropertyManagementDisplayedCorrectly("Self-Managed"));
		
		log.info("VP: Verify that 'Originator' field is saved successfully");
		verifyTrue(loansPage.isOriginatorDisplayedCorrectly("Dennis Spivey"));
		
		log.info("VP: Verify that 'Underwriter ' field is saved successfully");
		verifyTrue(loansPage.isUnderwriterDisplayedCorrectly(underwriter));
		
		log.info("VP: Verify that 'Loan Coordinator ' field is saved successfully");
		verifyTrue(loansPage.isLoanCoordinatorDisplayedCorrectly(loanCoord));
		
		log.info("VP: Verify that 'Closer' field is saved successfully");
		verifyTrue(loansPage.isCloserDisplayedCorrectly(closer));
		
		log.info("VP: Verify that 'Company Type' field is saved successfully");
		verifyTrue(loansPage.isRecourseDisplayedCorrectly("Yes"));
		
		log.info("VP: Verify that 'Bridge Borrower' field is saved successfully");
		verifyTrue(loansPage.isBridgeBorrowerDisplayedCorrectly("Yes"));
		
		log.info("VP: Verify that 'Company Type' field is saved successfully");
		verifyTrue(loansPage.isCompanyTypeDisplayedCorrectly(companyType));
		
		log.info("VP: Verify that 'State' field is saved successfully");
		verifyTrue(loansPage.isStateDisplayedCorrectly(state));
		
		log.info("VP: Verify that 'Owner' field is saved successfully");
		verifyTrue(loansPage.isOwnerDisplayedCorrectly("Dennis Spivey"));
		
		log.info("VP: Verify that 'Source' field is saved successfully");
		verifyTrue(loansPage.isSourceDisplayedCorrectly("Bridge Borrower"));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private String usernameLender, passwordLender;
	private String loanName, loanStatus, accountNumber, applicantName;
	private String invalidEmail, invalidCurrency, invalidDate,invalidContactNumber, invalidNumber;
	private String state, companyType;
	private String validEmail, validCurrency, validDate, validPhoneNumber, validText, validZipCode, validWholeNumber, validNumber;
	private String underwriter, loanCoord, closer, loanTerm;
	}