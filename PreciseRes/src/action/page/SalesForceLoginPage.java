package page;

import org.openqa.selenium.WebDriver;

import PreciseRes.Interfaces;

public class SalesForceLoginPage extends AbstractPage {

	public SalesForceLoginPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
	}
	
	/**
	 * type user name
	 * @param username
	 */
	public void typeUsername(String username) {
		waitForControl(driver, Interfaces.SalesForceLoginPage.EMAIL_TEXTBOX, timeWait);
		type(driver,Interfaces.SalesForceLoginPage.EMAIL_TEXTBOX, username);
	}
	
	/**
	 * type password
	 * @param password
	 */
	public void typePassword(String password) {
		waitForControl(driver, Interfaces.SalesForceLoginPage.PASSWORD_TEXTBOX, timeWait);
		type(driver,Interfaces.SalesForceLoginPage.PASSWORD_TEXTBOX, password);
	}

	/**
	 * Click Submit button
	 * @return MailHomePage
	 */
	public SalesForceHomePage clickSubmitButton(WebDriver driver, String ipClient)
	{
		waitForControl(driver, Interfaces.SalesForceLoginPage.LOGIN_BUTTON, timeWait);
		click(driver,Interfaces.SalesForceLoginPage.LOGIN_BUTTON);
		sleep();
		return PageFactory.getSalesForceHomePage(driver, ipClient);
	}

	private WebDriver driver;
}