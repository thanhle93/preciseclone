package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFDocumentPage;
import page.FNFLoansPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Common;
import common.Constant;

public class sanityTest_011_FNF_DocumentHistorical extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		loanName = "Edit Loan Auto " + getUniqueNumber();
		year = Common.getCommon().getCurrentYearOfWeek();
		loggedLender = "CAF FNF Lender";
		loggedApplicant = "CAF FNF Applicant";
		documentFolder1 = "Entity Documents";
		documentFolder2 = "Financials";
		documentFileName1 = "datatest1.pdf";
		documentFileName2 = "datatest2.pdf";
		statusChange1 = "Under Review";
		statusChange2 = "Revised";
		locClosingDate = "04/12/2014";
	}

	@Test(groups = { "regression" }, description = "Applicant 29 - Login as Lender - Make sure you can open both the old document and the new document")
	public void DocumentHistorycal_01_Lender_MakeSureOpenBothTheOldDocumentAndNewDocument() {

		log.info("DocumentHistorycal_01 - Step 01. Login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);

		log.info("DocumentHistorycal_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("DocumentHistorycal_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("DocumentHistorycal_01 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("DocumentHistorycal_01 - Step 05: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("DocumentHistorycal_01 - Step 06: Input loan name");
		loanPage.inputRequiredData(loanName, locClosingDate);
		
		log.info("DocumentHistorycal_01 - Step 07: Click Save button");
		loanPage.clickOnSaveButton();

		log.info("DocumentHistorycal_01 - Step 09. Open document folder");
		documentsPage = loanPage.openDocumentsFolder(documentFolder1);
		
		log.info("DocumentHistorycal_01 - Step 10. Upload document file name 01");
		documentsPage.uploadDocumentFileForFolderByPlaceHolder(documentFileName1);
		
		log.info("DocumentHistorycal_01 - Step 11. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("DocumentHistorycal_01 - Step 13. Upload document file name 02");
		documentsPage.changeDocumentFile(documentFileName1, documentFileName2);

		log.info("DocumentHistorycal_01 - Step 14. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("DocumentHistorycal_01 - Step 15. Click History link");
		documentsPage.openHistoryPopupBox(documentFileName2);
		
		log.info("DocumentHistorycal_01 - Step 16. Switch to History popup frame");
		documentsPage.switchToHistoryPopupFrame(driver);
		
		log.info("VP: Document is loaded in 'Document Detail' table with 'Under Review' status");
		verifyTrue(documentsPage.isDocumentHistoryLoadedToDocumentType(documentFileName1, loggedLender, "/" + year, " ", statusChange1));
		
		log.info("DocumentHistorycal_01 - Step 17. Click 'Document File Name 1' detail");
		documentsPage.clickDocumentFileNameDetail( loggedLender, documentFileName1);
		
		log.info("VP: The document 1 is openned to view");
		verifyTrue(documentsPage.isDocumentViewable(documentFileName1));
		
		log.info("DocumentHistorycal_01 - Step 18. Switch to History popup frame");
		documentsPage.switchToHistoryPopupFrame(driver);
		
		log.info("VP: Document is loaded in 'Document Detail' table with 'Under Review' previous status and 'Revised' status");
		verifyTrue(documentsPage.isDocumentHistoryLoadedToDocumentType(documentFileName2, loggedLender, "/" + year, statusChange1, statusChange2));
		
		log.info("DocumentHistorycal_01 - Step 19. Click 'Document File Name 2' detail");
		documentsPage.clickDocumentFileNameDetail( loggedLender, documentFileName2);
		
		log.info("VP: The document 2 is openned to view");
		verifyTrue(documentsPage.isDocumentViewable(documentFileName2));
		documentsPage.switchToTopWindowFrame(driver);
		
		log.info("DocumentHistorycal_01 - Step 20. Close History pop-up box");
		documentsPage.closeHistoryPopupBox();
	}

	@Test(groups = { "regression" }, description = "Applicant 30 - Login as Applicant - Make sure you can open both the old document and the new document")
	public void DocumentHistorycal_02_Applicant_MakeSureOpenBothTheOldDocumentAndNewDocument() {

		log.info("DocumentHistorycal_02 - Step 01: Login with Applicant");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);
		applicantsPage.setApplicantNameToDefault("CAF FNF", "Applicant");

		log.info("DocumentHistorycal_02 - Step 02: Open 'Loan Name'");
		loanPage = applicantsPage.openLoanNameInOpportunityTable(loanName);
		
		log.info("DocumentHistorycal_02 - Step 04. Open document folder");
		documentsPage = loanPage.openDocumentsFolder(documentFolder2);
		
		log.info("DocumentHistorycal_02 - Step 05. Upload document file name 01");
		documentsPage.uploadDocumentFileForFolderByPlaceHolder(documentFileName1);
		
		log.info("DocumentHistorycal_02 - Step 06. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("DocumentHistorycal_02 - Step 08. Upload document file name 02");
		documentsPage.changeDocumentFile(documentFileName1, documentFileName2);

		log.info("DocumentHistorycal_02 - Step 09. Click Save button");
		documentsPage.clickSaveButton();
		
		log.info("DocumentHistorycal_02 - Step 10. Click History link");
		documentsPage.openHistoryPopupBox(documentFileName2);
		
		log.info("DocumentHistorycal_02 - Step 11. Switch to History popup frame");
		documentsPage.switchToHistoryPopupFrame(driver);
		
		log.info("VP: Document is loaded in 'Document Detail' table with 'Under Review' status");
		verifyTrue(documentsPage.isDocumentHistoryLoadedToDocumentType(documentFileName1, loggedApplicant, "/" + year, " ", statusChange1));
		
		log.info("DocumentHistorycal_02 - Step 12. Click 'Document File Name 1' detail");
		documentsPage.clickDocumentFileNameDetail( loggedApplicant, documentFileName1);
		
		log.info("VP: The document 1 is openned to view");
		verifyTrue(documentsPage.isDocumentViewable(documentFileName1));
		
		log.info("DocumentHistorycal_02 - Step 13. Switch to History popup frame");
		documentsPage.switchToHistoryPopupFrame(driver);
		
		log.info("VP: Document is loaded in 'Document Detail' table with 'Under Review' previous status and 'Revised' status");
		verifyTrue(documentsPage.isDocumentHistoryLoadedToDocumentType(documentFileName2, loggedApplicant, "/" + year, statusChange1, statusChange2));
		
		log.info("DocumentHistorycal_02 - Step 14. Click 'Document File Name 2' detail");
		documentsPage.clickDocumentFileNameDetail( loggedApplicant, documentFileName2);
		
		log.info("VP: The document 2 is openned to view");
		verifyTrue(documentsPage.isDocumentViewable(documentFileName2));
		documentsPage.switchToTopWindowFrame(driver);
		
		log.info("DocumentHistorycal_02 - Step 15. Close History pop-up box");
		documentsPage.closeHistoryPopupBox();
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private FNFLoansPage loanPage;
	private FNFDocumentPage documentsPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername,	FNFBorrowerPassword;
	private String documentFolder1, documentFolder2;
	private String documentFileName1, documentFileName2;
	private String companyName, accountNumber, loanName, locClosingDate;
	private String loggedLender, loggedApplicant;
	private int year;
	private String statusChange1, statusChange2;
	}