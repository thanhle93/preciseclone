package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_022_B2R_UpdateZipWithZeroBeginInLoanAndProperties extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_B2R_LENDER;
		passwordLender = Constant.PASSWORD_B2R_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_B2R;
		applicantName = "Consolidation Demo Borrower New";
		borrowerName = "UdiTeamBorrower";
		address1 = "37412 OAKHILL ST";
		propertiesFileNameRequired = "B2R Required.xlsx";
		zipCode = "06388";
	}
	
	@Test(groups = { "regression" }, description = "Loan 72 - Update Zip with zero in the beginning in both loan and property screen")
	public void ZipZeroBegin_01_UpdateZipWithZeroInTheBeginning() {

		log.info("ZipZeroBegin_01 - Step 01. Login with lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(usernameLender, passwordLender,	false);

		log.info("ZipZeroBegin_01 - Step 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("ZipZeroBegin_01 - Step 03. Create new loan");
		loansPage.createNewLoan(loanName, applicantName, borrowerName, loanStatus, loanName);
		
		log.info("ZipZeroBegin_01 - Step 04: Update Zip with zero in the beginning in Loan");
		loansPage.inputZipCode(zipCode);
		
		log.info("ZipZeroBegin_01 - Step 05: Click on 'Save' button");
		loansPage.clickSaveButton();

		log.info("VP: Record Saved message displays");
		verifyTrue(loansPage.isRecordSavedMessageDisplays());
		
		log.info("VP: Verify that 'Zip code' is saved successfully");
		verifyTrue(loansPage.isZipCodeSaved(zipCode));
		
		log.info("ZipZeroBegin_01 - Step 06. Open 'Properties' tab");
		loansPage.openPropertiesTab();

		log.info("ZipZeroBegin_01 - Step 07. Load test file");
		loansPage.uploadFileProperties(propertiesFileNameRequired);
		
		log.info("ZipZeroBegin_01 - Step 08. Open a property detail");
		propertiesPage = loansPage.openPropertyDetail(address1);
		
		log.info("ZipZeroBegin_01 - Step 09: Update Zip with zero in the beginning in Properties");
		propertiesPage.inputZipCode(zipCode);
		
		log.info("ZipZeroBegin_01 - Step 10: Click on 'Save' button");
		propertiesPage.clickSaveButton();

		log.info("VP: Record Saved message displays");
		verifyTrue(propertiesPage.isRecordSavedMessageDisplays());
		
		log.info("VP: Verify that 'Zip code' is saved successfully");
		verifyTrue(propertiesPage.isZipCodeSaved(zipCode));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private PropertiesPage propertiesPage;
	private String usernameLender, passwordLender, address1;
	private String loanName, accountNumber, applicantName, loanStatus;
	private String borrowerName, propertiesFileNameRequired;
	private String zipCode;
}