package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFPropertiesPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;

import common.AbstractTest;
import common.Constant;

public class sanityTest_017_FNF_ChangePropertyStatus extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		loanName = "Edit Loan Auto " + accountNumber;
		companyName = "Edit_Borrower " + accountNumber;
		addressLender = "CAF-FNF Lender Address " + getUniqueNumber();
		city = "Fresno";
		state = "CA - California";
		zip = "95542";
		date = "04/12/2016";
		number = "30";
		price = "$40.00";
		propertyType ="SFR";
		transactionType ="Refinance";
		titleCompany= "testing company global";
		escrowAgent = "testing escrow global";
		insuranceCompany = "testing insurance global";
	}


	@Test(groups = { "regression" }, description = "Lender - Login as Lender - Change status from Due Diligence")
	public void ChangeStatusProperties_01_Lender_ChangeStatusFromDueDiligence() {

		log.info("Precondition - Step 01. Logout and login with Lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("Precondition - Step 02: Go to 'Properties' page");
		propertiesPage = homePage.openPropertiesPageFNF(driver, ipClient);
		
		log.info("Precondition - Step 03: Click on 'New' button");
		propertiesPage.clickNewPropertyButton();

		log.info("Precondition - Step 04: Input valid  all required information");
		propertiesPage.inputRequiredInformationProperties(companyName, loanName, addressLender, city, state, zip, date, number, price, propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany);
		
		log.info("Precondition - Step 05: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();

		log.info("VP: Record Saved message displays");
		verifyTrue(propertiesPage.isRecordSavedMessageDisplays());
		
		log.info("VP: Verify that 'Due Diligence' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Due Diligence"));

		log.info("VP: Verify that 'Put on Hold' button is displayed");
		verifyTrue(propertiesPage.isPutOnHoldButtonDisplayed());
		
		log.info("VP: Verify that 'Rescind' button is displayed");
		verifyTrue(propertiesPage.isRescindButtonDisplayed());
		
		log.info("VP: Verify that 'Cancel' button is displayed");
		verifyTrue(propertiesPage.isCancelButtonDisplayed());
		
		//Send to on hold
		
		log.info("ChangeStatusProperties_01 - Step 01: Click On hold button");
		propertiesPage.clickOnPutOnHoldButton();
		
		log.info("VP: Verify that 'On Hold' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("On Hold"));
		
		log.info("ChangeStatusProperties_01 - Step 02: Click on Re-open button");
		propertiesPage.clickOnReopenButton();
		
		log.info("VP: Verify that 'Due Diligence' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Due Diligence"));
		
		//Send to Rescind
		
		log.info("ChangeStatusProperties_01 - Step 03: Click On Rescind button");
		propertiesPage.clickOnRescindButton();
		
		log.info("VP: Verify that 'Rescinded' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Rescinded"));
		
		log.info("ChangeStatusProperties_01 - Step 04: Click on Re-open button");
		propertiesPage.clickOnReopenButton();
		
		log.info("VP: Verify that 'Due Diligence' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Due Diligence"));
		
		//Send to on hold
		
		log.info("ChangeStatusProperties_01 - Step 05: Click On Cancel button");
		propertiesPage.clickOnCancelButton();
		
		log.info("VP: Verify that 'Cancelled' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Cancelled"));
		
		log.info("ChangeStatusProperties_01 - Step 06: Click on Re-open button");
		propertiesPage.clickOnReopenButton();
		
		log.info("VP: Verify that 'Due Diligence' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Due Diligence"));
		
		log.info("ChangeStatusProperties_01 - Step 07: Click on Send to Pending button");
		propertiesPage.clickOnSendToPendingButton();
		
		log.info("VP: Verify that 'Please correct errors marked below' error message is not displayed");
		verifyFalse(propertiesPage.isErrorMessageDisplayed("Please correct errors marked below"));
		
		log.info("VP: Verify that 'Pending' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Pending"));
		
	}
	
	@Test(groups = { "regression" }, description = "Lender - Login as Lender - Change status from Pending")
	public void ChangeStatusProperties_02_Lender_ChangeStatusFromPending() {

		log.info("ChangeStatusProperties_02 - Step 01: Click on Back to Due Diligence button");
		propertiesPage.clickOnBackToDueDiligenceButton();
		
		log.info("VP: Verify that 'Due Diligence' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Due Diligence"));
		
		log.info("ChangeStatusProperties_02 - Step 02: Click on Send to Pending button");
		propertiesPage.clickOnSendToPendingButton();
		
		log.info("VP: Verify that 'Pending' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Pending"));

		log.info("VP: Verify that 'Put on Hold' button is displayed");
		verifyTrue(propertiesPage.isPutOnHoldButtonDisplayed());
		
		log.info("VP: Verify that 'Rescind' button is displayed");
		verifyTrue(propertiesPage.isRescindButtonDisplayed());
		
		log.info("VP: Verify that 'Cancel' button is displayed");
		verifyTrue(propertiesPage.isCancelButtonDisplayed());
		
		//Send to on hold
		
		log.info("ChangeStatusProperties_02 - Step 03: Click On hold button");
		propertiesPage.clickOnPutOnHoldButton();
		
		log.info("VP: Verify that 'On Hold' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("On Hold"));
		
		log.info("ChangeStatusProperties_02 - Step 04: Click on Re-open button");
		propertiesPage.clickOnReopenButton();
		
		log.info("VP: Verify that 'Pending' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Pending"));
		
		//Send to Rescind
		
		log.info("ChangeStatusProperties_02 - Step 05: Click On Rescind button");
		propertiesPage.clickOnRescindButton();
		
		log.info("VP: Verify that 'Rescinded' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Rescinded"));
		
		log.info("ChangeStatusProperties_02 - Step 06: Click on Re-open button");
		propertiesPage.clickOnReopenButton();
		
		log.info("VP: Verify that 'Pending' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Pending"));
		
		//Send to on hold
		
		log.info("ChangeStatusProperties_02 - Step 07: Click On Cancel button");
		propertiesPage.clickOnCancelButton();
		
		log.info("VP: Verify that 'Cancelled' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Cancelled"));
		
		log.info("ChangeStatusProperties_02 - Step 08: Click on Re-open button");
		propertiesPage.clickOnReopenButton();
		
		log.info("VP: Verify that 'Pending' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Pending"));
		
		log.info("ChangeStatusProperties_01 - Step 09: Click on Send to Active button");
		propertiesPage.clickOnSendToActiveButton();
		
		log.info("VP: Verify that 'Insurance Approved must equal �Yes�' error message is displayed");
		verifyTrue(propertiesPage.isErrorMessageDisplayed("Insurance Approved must equal �Yes�"));
		
		log.info("ChangeStatusProperties_01 - Step 10: Select Insurance Approved to 'Yes'");
		propertiesPage.selectInsuranceApproved("Yes");
		
		log.info("ChangeStatusProperties_01 - Step 10: Click on Send to Active button");
		propertiesPage.clickOnSendToActiveButton();
		
		log.info("VP: Verify that 'Active' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Active"));
	}
	
	@Test(groups = { "regression" }, description = "Lender - Login as Lender - Change status from Active")
	public void ChangeStatusProperties_03_Lender_ChangeStatusFromActive() {

		log.info("ChangeStatusProperties_02 - Step 01: Click on Back to Due Diligence button");
		propertiesPage.clickOnBackToPendingButton();
		
		log.info("VP: Verify that 'Pending' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Pending"));
		
		log.info("ChangeStatusProperties_01 - Step 10: Click on Send to Active button");
		propertiesPage.clickOnSendToActiveButton();
		
		log.info("VP: Verify that 'Active' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Active"));

		log.info("VP: Verify that 'Put on Hold' button is displayed");
		verifyTrue(propertiesPage.isPutOnHoldButtonDisplayed());
		
		log.info("VP: Verify that 'Rescind' button is displayed");
		verifyTrue(propertiesPage.isRescindButtonDisplayed());
		
		log.info("VP: Verify that 'Cancel' button is displayed");
		verifyTrue(propertiesPage.isCancelButtonDisplayed());
		
		//Send to on hold
		
		log.info("ChangeStatusProperties_02 - Step 03: Click On hold button");
		propertiesPage.clickOnPutOnHoldButton();
		
		log.info("VP: Verify that 'On Hold' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("On Hold"));
		
		log.info("ChangeStatusProperties_02 - Step 04: Click on Re-open button");
		propertiesPage.clickOnReopenButton();
		
		log.info("VP: Verify that 'Active' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Active"));
		
		//Send to Rescind
		
		log.info("ChangeStatusProperties_02 - Step 05: Click On Rescind button");
		propertiesPage.clickOnRescindButton();
		
		log.info("VP: Verify that 'Rescinded' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Rescinded"));
		
		log.info("ChangeStatusProperties_02 - Step 06: Click on Re-open button");
		propertiesPage.clickOnReopenButton();
		
		log.info("VP: Verify that 'Active' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Active"));
		
		//Send to on hold
		
		log.info("ChangeStatusProperties_02 - Step 07: Click On Cancel button");
		propertiesPage.clickOnCancelButton();
		
		log.info("VP: Verify that 'Cancelled' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Cancelled"));
		
		log.info("ChangeStatusProperties_02 - Step 08: Click on Re-open button");
		propertiesPage.clickOnReopenButton();
		
		log.info("VP: Verify that 'Active' status is changed successfully");
		verifyTrue(propertiesPage.isStatusChangedSuccessfull("Active"));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFPropertiesPage propertiesPage;
	private String FNFLenderUsername, FNFLenderPassword;
	private String addressLender, city, state, zip, companyName, date, number, price;
	private String propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany;
	private String accountNumber, loanName;
	}