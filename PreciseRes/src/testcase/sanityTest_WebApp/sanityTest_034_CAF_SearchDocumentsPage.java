package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Constant;
import page.DocumentsPage;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;
import page.PropertiesPage;

public class sanityTest_034_CAF_SearchDocumentsPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		loanStatus = Constant.LOAN_STATUS_CAF;
		applicantName = "CAF Demo Applicant";
		propertiesFileName = "CAF Required.xlsx";
		propertyAddress = "504 W Euclid";
		propertySearchAddress = "504 W Euclid " +accountNumber;
		documentType = "Evidence of Insurance";
		documentFileName = "datatest.pdf";
	}

	@Test(groups = { "regression" }, description = "SearchDocumentsPage 01 - Search by Added on time")
	public void SearchDocumentsPage_01_SearchByAddedOnTime() {
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Precondition 02. Open Loans tab");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 03. Create first Loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatus);

		log.info("Precondition 04. Input Loan Amount Requested");
		loansPage.inputLoanAmountRequested(Integer.toString(1000));
		loansPage.inputFloor("10");
		loansPage.selectBridgeBorrower("Yes");

		log.info("Precondition 06. Click Save button");
		loansPage.clickSaveButton();

		log.info("Precondition 07. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Precondition 08. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("Precondition 09. Open Property Details page");
		propertiesPage = loansPage.openPropertyDetail(propertyAddress);
		
		log.info("Precondition 10. Input Address for searching");
		propertiesPage.inputAddress(propertySearchAddress);

		log.info("Precondition 11. Delete all empty placeholders");
		propertiesPage.deleteAllPlaceholders();
		
		log.info("Precondition 12. Click New document button");
		propertiesPage.clickNewDocumentButton();

		log.info("Precondition 13. Select document type");
		propertiesPage.selectDocumentType(documentType);

		log.info("Precondition 14. Upload document file");
		propertiesPage.uploadDocumentFile(documentFileName);

		log.info("Precondition 15. Click 'Save' button");
		propertiesPage.clickSaveButton();
		
		log.info("Precondition 16. Click on Mark as Reviewed button");
		propertiesPage.clickOnMarkAsReviewedButton();
		
		log.info("SearchDocumentsPage_01 - Step 01. Get all credentials for searching Documents");
		addedOnTime = propertiesPage.getAddedOnTime();
		username = propertiesPage.getReviewedByUser();
		submittedMethod = propertiesPage.getUploadMethod();
		reviewedTime = propertiesPage.getReviewedTime();
		active = "Yes";
		
		log.info("SearchDocumentsPage_01 - Step 02. Open Properties Page");
		documentsPage = propertiesPage.openDocumentsPage(driver, ipClient);

		log.info("SearchDocumentsPage_01 - Step 03. Search by Loan name");
		documentsPage.searchByAddedOnTime(addedOnTime);
		documentIndex = documentsPage.getTextDocumentsSearch("DataCell FieldTypeSQLServerIdentity datacell_Document");

		log.info("VP: Documents name display in Documents search table");
		verifyTrue(documentsPage.isAllValueDisplayOnSearchDocumentsTable(documentIndex, applicantName, propertySearchAddress, documentType, submittedMethod, username, addedOnTime, reviewedTime, active));
	}

	@Test(groups = { "regression" }, description = "SearchDocumentsPage 02 - Search by Documents Index")
	public void SearchDocumentsPage_02_SearchByDocumentsIndex() {
		
		log.info("SearchDocumentsPage_02 - Step 01. Open Properties Page");
		documentsPage.openDocumentsPage(driver, ipClient);
		
		log.info("SearchDocumentsPage_02 - Step 02. Search by Document index");
		documentsPage.searchByDocumentIndex(addedOnTime, documentIndex);

		log.info("VP: Documents name display in Documents search table");
		verifyTrue(documentsPage.isAllValueDisplayOnSearchDocumentsTable(documentIndex, applicantName, propertySearchAddress, documentType, submittedMethod, username, addedOnTime, reviewedTime, active));
	}
	
	@Test(groups = { "regression" }, description = "SearchDocumentsPage 03 - Search by Applicant")
	public void SearchDocumentsPage_03_SearchByApplicant() {
		
		log.info("SearchDocumentsPage_03 - Step 01. Open Documents Page");
		documentsPage.openDocumentsPage(driver, ipClient);
		
		log.info("SearchDocumentsPage_03 - Step 02. Search by Applicant");
		documentsPage.searchByApplicant(addedOnTime, applicantName);

		log.info("VP: Documents name display in Documents search table");
		verifyTrue(documentsPage.isAllValueDisplayOnSearchDocumentsTable(documentIndex, applicantName, propertySearchAddress, documentType, submittedMethod, username, addedOnTime, reviewedTime, active));
	}
	
	@Test(groups = { "regression" }, description = "SearchDocumentsPage 04 - Search by Property")
	public void SearchDocumentsPage_04_SearchByProperty() {
		
		log.info("SearchDocumentsPage_04 - Step 01. Open Documents Page");
		documentsPage.openDocumentsPage(driver, ipClient);
		
		log.info("SearchDocumentsPage_04 - Step 02. Search by Property");
		documentsPage.searchByProperty(addedOnTime, propertySearchAddress);

		log.info("VP: Documents name display in Documents search table");
		verifyTrue(documentsPage.isAllValueDisplayOnSearchDocumentsTable(documentIndex, applicantName, propertySearchAddress, documentType, submittedMethod, username, addedOnTime, reviewedTime, active));
	}
	
	@Test(groups = { "regression" }, description = "SearchDocumentsPage 05 - Search by Document Type")
	public void SearchDocumentsPage_05_SearchByDocumentType() {
		
		log.info("SearchDocumentsPage_05 - Step 01. Open Documents Page");
		documentsPage.openDocumentsPage(driver, ipClient);
		
		log.info("SearchDocumentsPage_05 - Step 02. Search by Document Type");
		documentsPage.searchByDocumentType(addedOnTime, documentType);

		log.info("VP: Documents name display in Documents search table");
		verifyTrue(documentsPage.isAllValueDisplayOnSearchDocumentsTable(documentIndex, applicantName, propertySearchAddress, documentType, submittedMethod, username, addedOnTime, reviewedTime, active));
	}
	
	@Test(groups = { "regression" }, description = "SearchDocumentsPage 06 - Search by Submitted Via")
	public void SearchDocumentsPage_06_SearchBySubmittedVia() {
		
		log.info("SearchDocumentsPage_06 - Step 01. Open Documents Page");
		documentsPage.openDocumentsPage(driver, ipClient);
		
		log.info("SearchDocumentsPage_06 - Step 02. Search by Submitted Via");
		documentsPage.searchBySubmittedVia(addedOnTime, submittedMethod);

		log.info("VP: Documents name display in Documents search table");
		verifyTrue(documentsPage.isAllValueDisplayOnSearchDocumentsTable(documentIndex, applicantName, propertySearchAddress, documentType, submittedMethod, username, addedOnTime, reviewedTime, active));
	}
	
	@Test(groups = { "regression" }, description = "SearchDocumentsPage 07 - Search by Last Changed By")
	public void SearchDocumentsPage_07_SearchByLastChangedBy() {
		
		log.info("SearchDocumentsPage_07 - Step 01. Open Documents Page");
		documentsPage.openDocumentsPage(driver, ipClient);
		
		log.info("SearchDocumentsPage_07 - Step 02. Search by Last Changed By");
		documentsPage.searchByLastChangedBy(addedOnTime, username);

		log.info("VP: Documents name display in Documents search table");
		verifyTrue(documentsPage.isAllValueDisplayOnSearchDocumentsTable(documentIndex, applicantName, propertySearchAddress, documentType, submittedMethod, username, addedOnTime, reviewedTime, active));
	}
	
	@Test(groups = { "regression" }, description = "SearchDocumentsPage 08 - Search by Reviewed On Time")
	public void SearchDocumentsPage_08_SearchByReviewedOnTime() {
		
		log.info("SearchDocumentsPage_08 - Step 01. Open Documents Page");
		documentsPage.openDocumentsPage(driver, ipClient);
		
		log.info("SearchDocumentsPage_08 - Step 02. Search by Reviewed on time");
		documentsPage.searchByReviewedOnTime(reviewedTime);

		log.info("VP: Documents name display in Documents search table");
		verifyTrue(documentsPage.isAllValueDisplayOnSearchDocumentsTable(documentIndex, applicantName, propertySearchAddress, documentType, submittedMethod, username, addedOnTime, reviewedTime, active));
	}
	
	@Test(groups = { "regression" }, description = "SearchDocumentsPage 09 - Search by Reviewed By")
	public void SearchDocumentsPage_09_SearchByReviewedBy() {
		
		log.info("SearchDocumentsPage_09 - Step 01. Open Documents Page");
		documentsPage.openDocumentsPage(driver, ipClient);
		
		log.info("SearchDocumentsPage_09 - Step 02. Search by Applicant");
		documentsPage.searchByReviewUsername(addedOnTime, username);

		log.info("VP: Documents name display in Documents search table");
		verifyTrue(documentsPage.isAllValueDisplayOnSearchDocumentsTable(documentIndex, applicantName, propertySearchAddress, documentType, submittedMethod, username, addedOnTime, reviewedTime, active));
	}
	
	@Test(groups = { "regression" }, description = "SearchDocumentsPage 09 - Search by Active")
	public void SearchDocumentsPage_10_SearchByActive() {
		
		log.info("SearchDocumentsPage_10 - Step 01. Open Properties Page");
		documentsPage.openDocumentsPage(driver, ipClient);
		
		log.info("SearchDocumentsPage_10 - Step 02. Search by Active status");
		documentsPage.selectActiveRadioButton("Yes");
		documentsPage.searchByAddedOnTime(addedOnTime);

		log.info("VP: Documents name display in Documents search table");
		verifyTrue(documentsPage.isAllValueDisplayOnSearchDocumentsTable(documentIndex, applicantName, propertySearchAddress, documentType, submittedMethod, username, addedOnTime, reviewedTime, active));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private DocumentsPage documentsPage;
	private PropertiesPage propertiesPage;
	private String usernameLender, passwordLender;
	private String loanName, accountNumber, applicantName, propertiesFileName, loanStatus;
	private String documentType, documentFileName, propertyAddress, propertySearchAddress;
	private String addedOnTime, username, submittedMethod, reviewedTime, documentIndex, active;
	
	
}