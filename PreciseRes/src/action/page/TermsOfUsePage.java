package page;

import org.openqa.selenium.WebDriver;
import PreciseRes.Interfaces;

public class TermsOfUsePage extends AbstractPage{
	public TermsOfUsePage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}
	
	/**
	 * check Terms Of Use Title Display
	 */
	public boolean isTermsOfUseTitleDisplay() {
		waitForControl(driver, Interfaces.TermsOfUsePage.TERMS_OF_USE_TITLE, timeWait);
		return isControlDisplayed(driver, Interfaces.TermsOfUsePage.TERMS_OF_USE_TITLE);
	}
	
	/**
	 * check Username Display Top Left Corner
	 */
	public boolean isUsernameDisplayTopLeftCorner(String loginId) {
		waitForControl(driver, Interfaces.TermsOfUsePage.DYNAMIC_USER_FULL_NAME_TITLE, loginId, timeWait);
		return isControlDisplayed(driver, Interfaces.TermsOfUsePage.DYNAMIC_USER_FULL_NAME_TITLE, loginId);
	}
	
	/**
	 * check Logged As Username Display Right Left Corner
	 */
	public boolean isLoggedAsUsernameDisplayRightLeftCorner(String userName) {
		waitForControl(driver, Interfaces.TermsOfUsePage.DYNAMIC_USER_FULL_NAME_LOGGED, userName, timeWait);
		return isControlDisplayed(driver, Interfaces.TermsOfUsePage.DYNAMIC_USER_FULL_NAME_LOGGED, userName);
	}
	
	/**
	 * check Read Terms Use Checkbox
	 */
	public void checkReadTermsUseCheckbox() {
		waitForControl(driver, Interfaces.TermsOfUsePage.READ_TERMS_USE_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.TermsOfUsePage.READ_TERMS_USE_CHECKBOX);
	}
	
	/**
	 * click Continue Button
	 */
	public void clickContinueButton() {
		waitForControl(driver, Interfaces.TermsOfUsePage.CONTINUE_BUTTON, timeWait);
		click(driver, Interfaces.TermsOfUsePage.CONTINUE_BUTTON);
		sleep();
	}
	
	/**
	 * input Security Answer
	 */
	public void inputSecurityAnswer(String answer) {
		waitForControl(driver, Interfaces.TermsOfUsePage.SECURITY_ANSWER_TEXTBOX, timeWait);
		type(driver, Interfaces.TermsOfUsePage.SECURITY_ANSWER_TEXTBOX, answer);
		sleep(1);
	}
	
	/**
	 * finish Password Retrieval Info
	 */
	public LoansPage finishPasswordRetrievalInfo() {
		clickContinueButton();
		return PageFactory.getLoansPage(driver, ipClient);
	}
	
	/**
	 * finish Password Retrieval Info
	 */
	public HomePage finishPasswordRetrievalInfoForMember() {
		clickContinueButton();
		return PageFactory.getHomePage(driver, ipClient);
	}
	
	private WebDriver driver;
	private String ipClient; 
}