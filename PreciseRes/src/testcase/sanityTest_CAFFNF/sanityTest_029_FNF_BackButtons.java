package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Constant;
import page.FNFBorrowersPage;
import page.FNFDocumentPage;
import page.FNFLoansPage;
import page.FNFPropertiesPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;

public class sanityTest_029_FNF_BackButtons extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
	}

	@Test(groups = { "regression" }, description = "Back Buttons 01 - Back Buttons at Borrower page")
	public void BackButtons_01_Lender_BorrowersPage() {

		log.info("BackButtons_01 - Step 01. Logout and login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("BackButtons_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
		
		log.info("BackButtons_01 - Step 03: Open first record");
		applicantsPage.openFirstRecord(driver);
		
		log.info("VP: Check Back to List button is displayed");
		verifyTrue(applicantsPage.isMenuButtonDisplayed(driver, "Back to List"));
		
		log.info("BackButtons_01 - Step 04: Click on Back button");
		applicantsPage.clickOnMenuButton(driver, "Back to List");
		
		log.info("VP: Check Back to List button is displayed");
		verifyTrue(applicantsPage.isPageNameDisplayedCorrectly(driver, "List of Borrowers"));
	}
	
	@Test(groups = { "regression" }, description = "Back Buttons 02 - Back Buttons at Loans page")
	public void BackButtons_02_Lender_LoansPage() {
	
		log.info("BackButtons_02 - Step 01: Go to 'Loans' page");
		loanPage = applicantsPage.openFNFLoansPage(driver, ipClient);
		
		log.info("BackButtons_02 - Step 02: Open first record");
		loanPage.openFirstRecord(driver);
		
		log.info("BackButtons_02 - Step 03: Open Borrower record");
		loanPage.openBorrowerRecord();
		
		log.info("VP: Check Loans List button is displayed");
		verifyTrue(loanPage.isMenuButtonDisplayed(driver, "Back to Loan"));
		
		log.info("BackButtons_02 - Step 04: Click on Back button");
		loanPage.clickOnMenuButton(driver, "Back to Loan");
		
		log.info("VP: Loan Details page is displayed correctly");
		verifyTrue(loanPage.isPageNameDisplayedCorrectly(driver, "Loan Details"));
		
		log.info("VP: Check Loans List button is displayed");
		verifyTrue(loanPage.isMenuButtonDisplayed(driver, "Loans List"));
		
		log.info("BackButtons_02 - Step 05: Click on Back button");
		loanPage.clickOnMenuButton(driver, "Loans List");
		
		log.info("VP: Check Back to List button is displayed");
		verifyTrue(loanPage.isPageNameDisplayedCorrectly(driver, "List of Loans"));
	}
	
	@Test(groups = { "regression" }, description = "Back Buttons 02 - Back Buttons at Properties page")
	public void BackButtons_03_Lender_PropertiesPage() {
	
		log.info("BackButtons_03 - Step 01: Go to 'Loans' page");
		propertiesPage = loanPage.openPropertiesPageFNF(driver, ipClient);
		
		log.info("BackButtons_03 - Step 02: Open first record");
		propertiesPage.openFirstRecord(driver);
		
		log.info("VP: Check Back to Loan button is displayed");
		verifyTrue(propertiesPage.isMenuButtonDisplayed(driver, "Back to Loan "));
		
		log.info("VP: Check Back to Properties button is displayed");
		verifyTrue(propertiesPage.isMenuButtonDisplayed(driver, "Back to Properties"));
		
		log.info("VP: Check Loans List button is displayed");
		verifyTrue(propertiesPage.isMenuButtonDisplayed(driver, "Back to Loan Properties"));
		
		log.info("BackButtons_03 - Step 03: Click on Back to Loan ");
		propertiesPage.clickOnMenuButton(driver, "Back to Loan ");
		
		log.info("VP: Check Loan page is displayed");
		verifyTrue(propertiesPage.isPageNameDisplayedCorrectly(driver, "Loan Details"));
		
		log.info("BackButtons_03 - Step 04: Open Properties Tab");
		loanPage.openPropertiesTab();
		
		log.info("BackButtons_03 - Step 05: Open first record");
		propertiesPage.openFirstRecord(driver);
		
		log.info("BackButtons_03 - Step 06: Click on Back to Properties");
		propertiesPage.clickOnMenuButton(driver, "Back to Properties");
		
		log.info("VP: Check Properties list is displayed");
		verifyTrue(propertiesPage.isPageNameDisplayedCorrectly(driver, "List of Properties"));
		
		log.info("BackButtons_03 - Step 07: Open first record");
		propertiesPage.openFirstRecord(driver);
		
		log.info("BackButtons_03 - Step 08: Click on Back to Loan Properties");
		propertiesPage.clickOnMenuButton(driver, "Back to Loan Properties");
		
		log.info("VP: Check Loan Properties Tab is displayed");
		verifyTrue(propertiesPage.isPageNameDisplayedCorrectly(driver, "List of Properties for"));
	}
	
//
	@Test(groups = { "regression" }, description = "Back Buttons 03 - Back Buttons at Loans Properties Documents page")
	public void BackButtons_04_Lender_LoansDocumentsTab() {
		
		log.info("BackButtons_04 - Step 01. Open Loan Document Tab");
		documentsPage = loanPage.openDocumentsPageFNF(driver, ipClient);
		
		log.info("BackButtons_04 - Step 02: Open first record");
		documentsPage.openFirstRecord(driver);
		
		log.info("VP: Check Back to Loan button is displayed");
		verifyTrue(documentsPage.isMenuButtonDisplayed(driver, "Back"));
		
		log.info("BackButtons_04 - Step 03: Click on Back button");
		documentsPage.clickOnMenuButton(driver, "Back");
		
		log.info("VP: Check Loan Documents is displayed");
		verifyTrue(documentsPage.isPageNameDisplayedCorrectly(driver, "Loan Documents"));
	}
	
	@Test(groups = { "regression" }, description = "Back Buttons 03 - Back Buttons at Loans Property Document Tab")
	public void BackButtons_05_Lender_PropertyDocumentsTab() {
		
		log.info("BackButtons_05 - Step 01. Click Property Document Tab");
		documentsPage.openPropertyDocumentsTab();
		
		log.info("BackButtons_05 - Step 02: Open first record");
		documentsPage.openFirstRecord(driver);
		
		log.info("VP: Check Back to Loan button is displayed");
		verifyTrue(documentsPage.isMenuButtonDisplayed(driver, "Back to Loan"));
		
		log.info("VP: Check Back to Property button is displayed");
		verifyTrue(documentsPage.isMenuButtonDisplayed(driver, "Back to Property"));
		
		log.info("BackButtons_05 - Step 03: Click on Back to Loan button");
		documentsPage.clickOnMenuButton(driver, "Back to Loan");
		
		log.info("VP: Check Loan page is displayed");
		verifyTrue(loanPage.isPageNameDisplayedCorrectly(driver, "Loan Details"));
		
		log.info("BackButtons_05 - Step 04. Click Property Document Tab");
		documentsPage = loanPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
		
		log.info("BackButtons_05 - Step 05: Open first record");
		propertiesPage.openFirstRecord(driver);
		
		log.info("BackButtons_05 - Step 06: Click on Back to Properties");
		propertiesPage.clickOnMenuButton(driver, "Back to Property");
		
		log.info("VP: Check Property Details is displayed");
		verifyTrue(propertiesPage.isPageNameDisplayedCorrectly(driver, "Property Details"));
	}
	
	@Test(groups = { "regression" }, description = "Back Buttons 02 - Back Buttons at Loans page")
	public void BackButtons_06_Borrower_LoansPage() {
		
		log.info("BackButtons_06 - Step 01. Logout and login with Borrower");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);
	
		log.info("BackButtons_06 - Step 02: Go to 'Loans' page");
		loanPage = applicantsPage.openFNFLoansPage(driver, ipClient);

		log.info("BackButtons_06 - Step 03: Open first record");
		loanPage.openFirstRecord(driver);
		
		log.info("BackButtons_02 - Step 03: Open Borrower record");
		loanPage.openBorrowerRecord();
		
		log.info("VP: Check Loans List button is displayed");
		verifyTrue(loanPage.isMenuButtonDisplayed(driver, "Back to Loan"));
		
		log.info("BackButtons_02 - Step 04: Click on Back button");
		loanPage.clickOnMenuButton(driver, "Back to Loan");
		
		log.info("VP: Loan Details page is displayed correctly");
		verifyTrue(loanPage.isPageNameDisplayedCorrectly(driver, "Loan Details"));
		
		log.info("VP: Check Loans List button is displayed");
		verifyTrue(loanPage.isMenuButtonDisplayed(driver, "Loans List"));
		
		log.info("BackButtons_06 - Step 04: Click on Back button");
		loanPage.clickOnMenuButton(driver, "Loans List");
		
		log.info("VP: Check Back to List button is displayed");
		verifyTrue(loanPage.isPageNameDisplayedCorrectly(driver, "List of Loans"));
		
	}
	
	@Test(groups = { "regression" }, description = "Back Buttons 02 - Back Buttons at Properties page")
	public void BackButtons_07_Borrower_PropertiesPage() {
	
		log.info("BackButtons_07 - Step 01: Go to 'Loans' page");
		propertiesPage = loanPage.openPropertiesPageFNF(driver, ipClient);
		
		log.info("BackButtons_07 - Step 02: Open first record");
		propertiesPage.openFirstRecord(driver);
		
		log.info("VP: Check Back to Loan button is displayed");
		verifyTrue(propertiesPage.isMenuButtonDisplayed(driver, "Back to Loan "));
		
		log.info("VP: Check Back to Properties button is displayed");
		verifyTrue(propertiesPage.isMenuButtonDisplayed(driver, "Back to Properties"));
		
		log.info("VP: Check Loans List button is displayed");
		verifyTrue(propertiesPage.isMenuButtonDisplayed(driver, "Back to Loan Properties"));
		
		log.info("BackButtons_07 - Step 03: Click on Back to Loan ");
		propertiesPage.clickOnMenuButton(driver, "Back to Loan ");
		
		log.info("VP: Check Loan page is displayed");
		verifyTrue(propertiesPage.isPageNameDisplayedCorrectly(driver, "Loan Details"));
		
		log.info("BackButtons_07 - Step 04: Open Properties Tab");
		loanPage.openPropertiesTab();
		
		log.info("BackButtons_07 - Step 05: Open first record");
		propertiesPage.openFirstRecord(driver);
		
		log.info("BackButtons_07 - Step 06: Click on Back to Properties");
		propertiesPage.clickOnMenuButton(driver, "Back to Properties");
		
		log.info("VP: Check Properties list is displayed");
		verifyTrue(propertiesPage.isPageNameDisplayedCorrectly(driver, "List of Properties"));
		
		log.info("BackButtons_07 - Step 07: Open first record");
		propertiesPage.openFirstRecord(driver);
		
		log.info("BackButtons_07 - Step 08: Click on Back to Loan Properties");
		propertiesPage.clickOnMenuButton(driver, "Back to Loan Properties");
		
		log.info("VP: Check Loan Properties Tab is displayed");
		verifyTrue(propertiesPage.isPageNameDisplayedCorrectly(driver, "List of Properties for"));
	}
	
//
	@Test(groups = { "regression" }, description = "Back Buttons 03 - Back Buttons at Loans Properties Documents page")
	public void BackButtons_08_Borrower_LoansDocumentsTab() {
		
		log.info("BackButtons_08 - Step 01. Open Loan Document Tab");
		documentsPage = loanPage.openDocumentsPageFNF(driver, ipClient);
		
		log.info("BackButtons_08 - Step 02: Open first record");
		documentsPage.openFirstRecord(driver);
		
		log.info("VP: Check Back to Loan button is displayed");
		verifyTrue(documentsPage.isMenuButtonDisplayed(driver, "Back"));
		
		log.info("BackButtons_08 - Step 03: Click on Back button");
		documentsPage.clickOnMenuButton(driver, "Back");
		
		log.info("VP: Check Loan Documents is displayed");
		verifyTrue(documentsPage.isPageNameDisplayedCorrectly(driver, "Loan Documents"));
	}
	
	@Test(groups = { "regression" }, description = "Back Buttons 03 - Back Buttons at Loans Property Document Tab")
	public void BackButtons_09_Borrower_PropertyDocumentsTab() {
		
		log.info("BackButtons_09 - Step 01. Click Property Document Tab");
		documentsPage.openPropertyDocumentsTab();
		
		log.info("BackButtons_09 - Step 02: Open first record");
		documentsPage.openFirstRecord(driver);
		
		log.info("VP: Check Back to Loan button is displayed");
		verifyTrue(documentsPage.isMenuButtonDisplayed(driver, "Back to Loan"));
		
		log.info("VP: Check Back to Property button is displayed");
		verifyTrue(documentsPage.isMenuButtonDisplayed(driver, "Back to Property"));
		
		log.info("BackButtons_09 - Step 03: Click on Back to Loan button");
		documentsPage.clickOnMenuButton(driver, "Back to Loan");
		
		log.info("VP: Check Loan page is displayed");
		verifyTrue(loanPage.isPageNameDisplayedCorrectly(driver, "Loan Details"));
		
		log.info("BackButtons_09 - Step 04. Click Property Document Tab");
		documentsPage = loanPage.openDocumentsPageFNF(driver, ipClient);
		documentsPage.openPropertyDocumentsTab();
		
		log.info("BackButtons_09 - Step 05: Open first record");
		propertiesPage.openFirstRecord(driver);
		
		log.info("BackButtons_09 - Step 06: Click on Back to Properties");
		propertiesPage.clickOnMenuButton(driver, "Back to Property");
		
		log.info("VP: Check Property Details is displayed");
		verifyTrue(propertiesPage.isPageNameDisplayedCorrectly(driver, "Property Details"));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFDocumentPage documentsPage;
	private FNFPropertiesPage propertiesPage;
	private FNFLoansPage loanPage;
	private FNFBorrowersPage applicantsPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername, FNFBorrowerPassword;
}