package page;

import org.openqa.selenium.WebDriver;
import PreciseRes.Interfaces;

public class CheckListsPage extends AbstractPage {
	public CheckListsPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
	}

	/**
	 * check My Information page display
	 * @param N/A
	 */
	public boolean isCheckListsPageDisplay() {
		waitForControl(driver, Interfaces.CheckListsPage.CHECK_LISTS_PAGE_SECTION, timeWait);
		return isControlDisplayed(driver, Interfaces.CheckListsPage.CHECK_LISTS_PAGE_SECTION);
	}
	
	
	private WebDriver driver;
}