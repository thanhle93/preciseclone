package sanityTest_WebApp;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.AbstractTest;
import common.Common;
import common.Constant;
import page.HomePage;
import page.LoansPage;
import page.LoginPage;
import page.PageFactory;

public class sanityTest_031_CAF_SearchLoansPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		usernameLender = Constant.USERNAME_CAF_LENDER;
		passwordLender = Constant.PASSWORD_CAF_LENDER;
		accountNumber = getUniqueNumber();
		loanName = "UdiTeam-Loan" + accountNumber;
		applicantName = "CAF Demo Applicant";
		loanStatusOld = "Deals Under Review";
		loanStatus = Constant.LOAN_STATUS_CAF;
		propertiesFileName = "CAF Required.xlsx";
		active = "Yes";
		inActive = "No";
		day = Common.getCommon().getCurrentDayOfWeek();
		month = Common.getCommon().getCurrentMonthOfWeek();
		year = Common.getCommon().getCurrentYearOfWeek();
		inputDay = month + "/" + day + "/" + year;
		comments = "Search Loan Colony";
		originator = "Dennis Spivey";
	}

	@Test(groups = { "regression" }, description = "SearchLoansPage 01 - Search by Legal Name")
	public void SearchLoansPage_01_SearchByLegalName() {
		log.info("Precondition 01. Login with lender");
		homePage = loginPage.loginAsLender(usernameLender, passwordLender, false);

		log.info("Precondition 02. Open Loans page");
		loansPage = homePage.openLoansPage(driver, ipClient);

		log.info("Precondition 03. Create New loan");
		loansPage.createNewLoan(loanName, applicantName, "", loanStatusOld);

		log.info("Precondition 04. Input Loan Amount Requested");
		loansPage.inputLoanAmountRequested("1000");

		log.info("Precondition 05. Input Loan Status Change Date");
		loansPage.selectLoanStatusBasicDetail(loanStatus);

		log.info("Precondition 06. Update Comments field");
		loansPage.inputComments(comments);

		log.info("Precondition 07. Update Originator field");
		loansPage.selectOriginator(originator);

		log.info("Precondition 08. Update Expected Close Date field");
		loansPage.inputExpectedCloseDate(inputDay);
		loansPage.inputFloor("10");
		loansPage.selectBridgeBorrower("Yes");

		log.info("Precondition 09. Click Save button");
		loansPage.clickSaveButton();
		loansPage.isRecordSavedMessageDisplays();

		log.info("Precondition 10. Open Properties tab");
		loansPage.openPropertiesTab();

		log.info("Precondition 11. Load test file");
		loansPage.uploadFileProperties(propertiesFileName);

		log.info("Precondition 12. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_01 - Step 01. Search by Legal Name");
		loansPage.searchLoanByName(loanName);

		expectedCloseDate = loansPage.getTextLoanSearch("DataCell FieldTypeDate datacell_ProjectedCloseDate");
		numberProperties = loansPage.getTextLoanSearch("DataCell FieldTypeInteger datacell_TotProp");
		totalEstimatedValue = loansPage.getTextLoanSearch("DataCell FieldTypeCurrency datacell_TotVal");
		loanStatusChangeDate = loansPage.getTextLoanSearch("DataCell FieldTypeDate datacell_StatusChangeDate");
		dateCreated = loansPage.getTextLoanSearch("DataCell FieldTypeDateTime datacell_CreateDate");

		log.info("VP: Legal Name display in Loans search table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, expectedCloseDate, numberProperties, totalEstimatedValue, loanStatus, dateCreated,
				originator, loanStatusChangeDate, comments, active));
	}

	@Test(groups = { "regression" }, description = "SearchLoansPage 02 - Search by Applicant")
	public void SearchLoansPage_02_SearchByApplicant() {

		log.info("SearchLoansPage_02 - Step 01. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_02 - Step 02. Search by Applicant");
		loansPage.searchLoanByApplicant(loanName, applicantName);

		log.info("VP: Applicant display in Loans search table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, expectedCloseDate, numberProperties, totalEstimatedValue, loanStatus, dateCreated,
				originator, loanStatusChangeDate, comments, active));
	}

	@Test(groups = { "regression" }, description = "SearchLoansPage 03 - Search by Expected Close Date")
	public void SearchLoansPage_03_SearchByExpectedCloseDate() {

		log.info("SearchLoansPage_03 - Step 01. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_03 - Step 02. Search by Expected Close Date");
		loansPage.searchLoanByExpectedCloseDate(loanName, expectedCloseDate);

		log.info("VP: Expected Close Date display in Loans search table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, expectedCloseDate, numberProperties, totalEstimatedValue, loanStatus, dateCreated,
				originator, loanStatusChangeDate, comments, active));
	}

	@Test(groups = { "regression" }, description = "SearchLoansPage 04 - Search by Loan Application Status")
	public void SearchLoansPage_04_SearchByLoanApplicationStatus() {

		log.info("SearchLoansPage_04 - Step 01. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_04 - Step 02. Search by Loan Application Status");
		loansPage.searchLoanByLoanApplicationStatusCAF(loanName, loanStatus);

		log.info("VP: Loan Application Status display in Loans search table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, expectedCloseDate, numberProperties, totalEstimatedValue, loanStatus, dateCreated,
				originator, loanStatusChangeDate, comments, active));
	}

	@Test(groups = { "regression" }, description = "SearchLoansPage 05 - Search by Date Created")
	public void SearchLoansPage_05_SearchByDateCreated() {

		log.info("SearchLoansPage_05 - Step 01. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_05 - Step 02. Search by Date Created");
		loansPage.searchLoanByDateCreated(loanName, dateCreated);

		log.info("VP: Date Created display in Loans search table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, expectedCloseDate, numberProperties, totalEstimatedValue, loanStatus, dateCreated,
				originator, loanStatusChangeDate, comments, active));
	}

	@Test(groups = { "regression" }, description = "SearchLoansPage 06 - Search by Loan Active status")
	public void SearchLoansPage_06_SearchByLoanActiveStatus() {

		log.info("SearchLoansPage_06 - Step 01. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_06 - Step 02. Search by Legal Name");
		loansPage.searchLoanByName(loanName);

		log.info("SearchLoansPage_06 - Step 03: Select Loan checkbox in List Loan Applicants table");
		loansPage.isSelectedPropertyLoanCheckbox(loanName);

		log.info("SearchLoansPage_06 - Step 04: Click 'Make Loan Inactive' button");
		loansPage.clickMakeInactiveButtonAtLoanTab();

		log.info("VP: The Loan not displayed in table");
		verifyFalse(loansPage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, expectedCloseDate, numberProperties, totalEstimatedValue, loanStatus, dateCreated,
				originator, loanStatusChangeDate, comments, active));

		log.info("SearchLoansPage_06 - Step 05. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_06 - Step 06. Search Loan by Inactive status");
		loansPage.searchByActiveOrInactive(loanName, "No");

		log.info("VP: The Loan inactive displayed in table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, expectedCloseDate, "0", "", loanStatus, dateCreated, originator, loanStatusChangeDate,
				comments, inActive));

		log.info("SearchLoansPage_06 - Step 07. Open Loans page");
		loansPage.openLoansPage(driver, ipClient);

		log.info("SearchLoansPage_06 - Step 08. Search Loan by Either status");
		loansPage.searchByActiveOrInactive(loanName, "Either");

		log.info("VP: The Loan inactive displayed in table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, expectedCloseDate, "0", "", loanStatus, dateCreated, originator, loanStatusChangeDate,
				comments, inActive));

		log.info("SearchLoansPage_06 - Step 09: Select Loan checkbox in List Loan Applicants table");
		loansPage.isSelectedPropertyLoanCheckbox(loanName);

		log.info("SearchLoansPage_06 - Step 10: Click 'Make Loan Active' button");
		loansPage.clickMakeActiveButtonAtLoanTab();

		log.info("SearchLoansPage_06 - Step 11. Search Loan by Active status");
		loansPage.searchByActiveOrInactive(loanName, "Yes");

		log.info("VP: The loan displayed in table");
		verifyTrue(loansPage.isAllValueDisplayOnSearchLoansTableCAF(loanName, applicantName, expectedCloseDate, numberProperties, totalEstimatedValue, loanStatus, dateCreated,
				originator, loanStatusChangeDate, comments, active));
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private LoansPage loansPage;
	private int day, month, year;
	private String usernameLender, passwordLender, loanName, loanStatus, propertiesFileName, inputDay, comments, originator, expectedCloseDate, loanStatusOld;
	private String accountNumber, applicantName, numberProperties, totalEstimatedValue, loanStatusChangeDate, dateCreated, active, inActive;
}