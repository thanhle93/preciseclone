package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_003_FNF_UpdateApplicantInvalidEmailZipNetIncome extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		invalidEmail = "damdm*%#*123%#ye@gma@dfd@df//il.com";
		invalidZipCode = "35695*%$fa";
		invalidNetIncome = "$40,000.00*%#$fail";
	}

	@Test(groups = { "regression" }, description = "Applicant 03 - Login as Lender and input invalid 'Zip code' field")
	public void Applicant_07_InputInvalidZipcode() {
		log.info("Applicant_07 - Step 01: Login with valid username password");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);

		log.info("Applicant_07 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
		
		log.info("Applicant_07 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("Applicant_07 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();

		log.info("Applicant_07 - Step 05: Input invalid 'Zip code'");
		applicantsPage.inputEmailZipCode(invalidZipCode, invalidEmail, "");

		log.info("Applicant_07 - Step 06: Click on 'Save' button");
		applicantsPage.clickOnSaveButton();

		log.info("VP: The error message should be displayed");
		verifyTrue(applicantsPage.isErrorMessageDisplayed("Enter a valid ZIP or ZIP+4 code"));
	}
	
	@Test(groups = { "regression" }, description = "Applicant 03 - Login as Lender and input invalid 'Borrower Email' field")
	public void Applicant_08_InputInvalidBorrowerEmail() {
		
		log.info("Applicant_08 - Step 01: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
		
		log.info("Applicant_08 - Step 02: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("Applicant_08 - Step 03: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();

		log.info("Applicant_08 - Step 04: Input invalid 'Borrower Email'");
		applicantsPage.inputEmailZipCode("", invalidEmail, "");

		log.info("Applicant_08 - Step 05: Click on 'Save' button");
		applicantsPage.clickOnSaveButton();

		log.info("VP: The error message should be displayed");
		verifyTrue(applicantsPage.isErrorMessageDisplayed("Enter a valid email address"));
	}
	
	@Test(groups = { "regression" }, description = "Applicant 03 - Login as Lender and input invalid 'Borrower Zip Code' field")
	public void Applicant_09_InputInvalidBorrowerZipCode() {
		log.info("Applicant_09 - Step 01: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
		
		log.info("Applicant_09 - Step 02: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("Applicant_09 - Step 03: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();

		log.info("Applicant_09 - Step 04: Input invalid 'Borrower Zip Code'");
		applicantsPage.inputEmailZipCode("", invalidEmail, invalidZipCode);

		log.info("Applicant_09 - Step 05: Click on 'Save' button");
		applicantsPage.clickOnSaveButton();

		log.info("VP: The error message should be displayed");
		verifyTrue(applicantsPage.isErrorMessageDisplayed("Enter a valid ZIP or ZIP+4 code"));
	}

	@Test(groups = { "regression" }, description = "Applicant 04 - Login as Lender and input invalid 'Net Income' field")
	public void Applicant_10_InputInvalidNetIncome() {
		log.info("Applicant_10 - Step 01: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
		
		log.info("Applicant_10 - Step 02: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
		
		log.info("Applicant_10 - Step 03: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
	
		log.info("Applicant_10 - Step 04: Input invalid 'Net Income'");
		applicantsPage.inputCurrency(invalidNetIncome);
	
		log.info("Applicant_10 - Step 05: Click on 'Save' button");
		applicantsPage.clickOnSaveButton();
	
		log.info("VP: The error message should be displayed");
		verifyTrue(applicantsPage.isErrorMessageDisplayed("Currency value must be a number"));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private String FNFLenderUsername, FNFLenderPassword;
	private String companyName, accountNumber;
	private String invalidZipCode, invalidEmail, invalidNetIncome;
	}