package sanityTest_Store;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;

import common.AbstractTest;
import common.Constant;

public class sanityTest_002_STO_ValidateDatatypesBasicDetailPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + getUniqueNumber();
		invalidCurrency = "$40,0.00*%#$fail";
		invalidDate = "4/21/2015*%#$fail";
		invalidNumber = "20%*%#$fail";
		workflowStatus = "In Underwriting";
		productType = "Lease";
		levelFrequency = "Q";
		breakover = "Monthly";
		validCurrency = "$2,000,000.00";
		validDate = "12/31/2000";
		validNumber = getUniqueNumber();
		validText = "STORE Capital Corporation";
		validRate = "2.36%";
	}

	@Test(groups = { "regression" }, description = "Validate Datatypes 01 - No input the data")
	public void ValidataDatatypes_01_NoInputTheData() {
		
		log.info("ValidataDatatypes_01 - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("ValidataDatatypes_01 - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();
		
		log.info("ValidataDatatypes_01 - Step 03: Check to 'Contingent Rent' checkbox");
		transactionPage.selectContingentRent();
		
		log.info("ValidataDatatypes_01 - Step 04: Check to 'Escrow Rights' checkbox");
		transactionPage.selectEscrowRights();
		
		log.info("ValidataDatatypes_01 - Step 05: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Company is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationBasicDetails_R1_CompanyName", "Company is required"));
		
		log.info("VP: 'Lease Expiration Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsBasicDetails_R1_Maturity", "Lease Expiration Date is required"));
		
		log.info("VP: '1st Payment Date is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationBasicDetails_R1_DateEstablish", "1st Payment Date is required"));
		
		log.info("VP: 'Cap Rate is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsBasicDetails_R1_InterestRate", "Cap Rate is required"));
		
		log.info("VP: 'Initial Rent is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsBasicDetails_R1_InitialPayment", "Initial Rent is required"));
		
		log.info("VP: 'Contingent Rent Description is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsBasicDetails_R1_ContingentRentDesc", "Contingent Rent Description is required"));
		
		log.info("VP: 'Period End is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsBasicDetails_R1_PeriodEnd", "Period End is required"));
		
		log.info("VP: 'Breakover is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsBasicDetails_R1_Breakover", "Breakover is required"));
		
		log.info("VP: 'Days to Report is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsBasicDetails_R1_DaysReport", "Days to Report is required"));
		
		log.info("VP: 'Escrow Description is required' message is displayed");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsBasicDetails_R1_EscrowDescription", "Escrow Description is required"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 02 - Input the invalid/ incorrect at Currency fields")
	public void ValidataDatatypes_02_InputInvalidOrIncorrectAtCurrencyFields() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();
		
		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();
		
		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);
		
		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);

		log.info("Precondition - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("ValidataDatatypes_02 - Step 01: Input the invalid data to 'Price' field");
		transactionPage.inputPrice(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 02: Input the invalid data to 'Security Deposit Amount' field");
		transactionPage.inputSercurityDepositAmount(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 03: Input the invalid data to 'Initial Rent' field");
		transactionPage.inputInitialRent(invalidCurrency);
		
		log.info("ValidataDatatypes_02 - Step 04: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Currency value must be a number' message is displayed at 'Price' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsBasicDetails_R1_LeaseAmount", "Currency value must be a number"));
		
		log.info("VP: 'Currency value must be a number' message is displayed at 'Security Deposit Amount' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationBasicDetails_R1_SecurityDepositAmount", "Currency value must be a number"));
	
		log.info("VP: 'Currency value must be a number' message is displayed at 'Initial Rent' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsBasicDetails_R1_InitialPayment", "Currency value must be a number"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 03 - Input the invalid/ incorrect at Date fields")
	public void ValidataDatatypes_03_InputInvalidOrIncorrectAtDateFields() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("ValidataDatatypes_03 - Step 01: Input the invalid data to 'Lease Expiration Date' field");
		transactionPage.inputLeaseExpirationDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 02: Input the invalid data to '1st Payment Date' field");
		transactionPage.inputFirstPaymentDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 03: Input the invalid data to 'Original Commencement Date' field");
		transactionPage.inputOriginalCommencementDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 04: Input the invalid data to 'Contract Commencement Date' field");
		transactionPage.inputContractCommencementDate(invalidDate);
		
		log.info("ValidataDatatypes_03 - Step 05: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Lease Expiration Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsBasicDetails_R1_Maturity", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at '1st Payment Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationBasicDetails_R1_DateEstablish", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Original Commencement Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsBasicDetails_R1_OrigCommencement", "Value must be a date"));
		
		log.info("VP: 'Value must be a date' message is displayed at 'Contract Commencement Date' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsBasicDetails_R1_ContractCommencementDate", "Value must be a date"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 04 - Input the invalid/ incorrect at Number fields")
	public void ValidataDatatypes_04_InputInvalidOrIncorrectAtNumberFields() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("ValidataDatatypes_04 - Step 01: Input the invalid data to 'Prorated Days' field");
		transactionPage.inputProratedDays(invalidNumber);
		
		log.info("ValidataDatatypes_04 - Step 02: Input the invalid data to 'Term' field");
		transactionPage.inputTerm(invalidNumber);
		
		log.info("ValidataDatatypes_04 - Step 03: Input the invalid data to 'Cap Rate' field");
		transactionPage.inputCapRate(invalidNumber);
		
		log.info("ValidataDatatypes_04 - Step 04: Check to 'Contingent Rent' checkbox");
		transactionPage.selectContingentRent();
		
		log.info("ValidataDatatypes_04 - Step 05: Input the invalid data to 'Period End' field");
		transactionPage.inputPeriodEnd(invalidNumber);
		
		log.info("ValidataDatatypes_04 - Step 06: Input the invalid data to 'Days to Report' field");
		transactionPage.inputDaysToReport(invalidNumber);
		
		log.info("ValidataDatatypes_04 - Step 07: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Please correct errors marked below' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Please correct errors marked below"));
		
		log.info("VP: 'Value must be a whole number' message is displayed at 'Prorated Days' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsBasicDetails_R1_ProratedDays", "Value must be a whole number"));
		
		log.info("VP: 'Value must be a whole number' message is displayed at 'Term' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LoanApplicationBasicDetails_R1_Term", "Value must be a whole number"));
		
		log.info("VP: 'Value left of % symbol must be a number' message is displayed at 'Cap Rate' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsBasicDetails_R1_InterestRate", "Please enter correct value ###.####%."));
		
		log.info("VP: 'Value must be a whole number' message is displayed at 'Period End' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsBasicDetails_R1_PeriodEnd", "Value must be a whole number"));
		
		log.info("VP: 'Value must be a whole number' message is displayed at 'Days to Report' field");
		verifyTrue(transactionPage.isDynamicAlertMessageTitleDisplay("field_LeaseDetailsBasicDetails_R1_DaysReport", "Value must be a whole number"));
	}
	
	@Test(groups = { "regression" }, description = "Validate Datatypes 05 - Input the valid into all fields")
	public void ValidataDatatypes_05_InputValidIntoAllFields() {
		
		log.info("Precondition - Step 01: Open Transaction page");
		transactionPage.openTransactionPage(driver, ipClient);
		
		log.info("Precondition - Step 02: Search Transaction by Company name");
		transactionPage.searchTransactionByCompanyName(companyName);
		
		log.info("Precondition - Step 03: Open Transaction detail page");
		transactionPage.openTransactionDetailPage(companyName);
		
		log.info("ValidataDatatypes_05 - Step 01: Input the valid data to 'Workflow Status' field");
		transactionPage.selectWorkflowStatus(workflowStatus);
		
		log.info("ValidataDatatypes_05 - Step 02: Input the valid data to 'Salesforce Opportunity ID' field");
		transactionPage.inputSalesforceOpportunityID(validNumber);
		
		log.info("ValidataDatatypes_05 - Step 03: Input the valid data to 'Client ID' field");
		transactionPage.inputClientID(validNumber);
		
		log.info("ValidataDatatypes_05 - Step 04: Input the valid data to 'Deal ID' field");
		transactionPage.inputDealID(validNumber);
		
		log.info("ValidataDatatypes_05 - Step 05: Input the valid data to 'Product Type' field");
		transactionPage.selectProductType(productType);
		
		log.info("ValidataDatatypes_05 - Step 06: Input the valid data to 'Existing Lease Prior to Purchase' field");
		transactionPage.selectExistingLeasePriorToPurchase();
		
		log.info("ValidataDatatypes_05 - Step 07: Input the valid data to 'Industry Type' field");
		transactionPage.selectIndustryType("Auto Dealers (4411)");
		
		log.info("ValidataDatatypes_05 - Step 08: Input the valid data to 'Portfolio Company' field");
		transactionPage.inputPortfolioCompany(validText);
		
		log.info("ValidataDatatypes_05 - Step 09: Input the valid data to 'Store Entity' field");
		transactionPage.inputStoreEntity(validText);
		
		log.info("ValidataDatatypes_05 - Step 10: Input the valid data to 'Price' field");
		transactionPage.inputPrice(validCurrency);
		
		log.info("ValidataDatatypes_05 - Step 11: Input the valid data to 'Security Deposit Amount' field");
		transactionPage.inputSercurityDepositAmount(validCurrency);
		
		log.info("ValidataDatatypes_05 - Step 12: Input the valid data to 'Term' field");
		transactionPage.inputTerm(validNumber);
		
//		log.info("ValidataDatatypes_05 - Step 13: Input the valid data to 'ACH Payment' field");
//		transactionPage.selectACHPayment();
		
		log.info("ValidataDatatypes_05 - Step 14: Input the valid data to 'Cap Rate' field");
		transactionPage.inputCapRate(validRate);
		
		log.info("ValidataDatatypes_05 - Step 15: Input the valid data to 'Original Commencement Date' field");
		transactionPage.inputOriginalCommencementDate(validDate);
		
		log.info("ValidataDatatypes_05 - Step 16: Input the valid data to 'Initial Rent' field");
		transactionPage.inputInitialRent(validCurrency);
		
		log.info("ValidataDatatypes_05 - Step 17: Input the valid data to 'Contract Commencement Date' field");
		transactionPage.inputContractCommencementDate(validDate);
		
		log.info("ValidataDatatypes_05 - Step 18: Input the valid data to '1st Payment Date' field");
		transactionPage.inputFirstPaymentDate(validDate);
		
		log.info("ValidataDatatypes_05 - Step 19: Input the valid data to 'Lease Expiration Date' field");
		transactionPage.inputLeaseExpirationDate(validDate);
		
		log.info("ValidataDatatypes_05 - Step 20: Input the valid data to 'Prorated Days' field");
		transactionPage.inputProratedDays(validNumber);
		
		log.info("ValidataDatatypes_05 - Step 21: Input the valid data to 'Corporate Financial Statements Required' field");
		transactionPage.selectCorporateFinancialStatementsRequired();
		
		log.info("ValidataDatatypes_05 - Step 22: Input the valid data to 'Unit Financial Statements Required' field");
		transactionPage.selectUnitFinancialStatementsRequired();
		
		log.info("ValidataDatatypes_05 - Step 23: Input the valid data to 'Corporate is Unit Proxy' field");
		transactionPage.selectCorporateUnitProxy();
		
		log.info("ValidataDatatypes_05 - Step 24: Input the valid data to 'Unit Sales Only Required' field");
		transactionPage.selectUnitSalesOnlyRequired();
		
		log.info("ValidataDatatypes_05 - Step 25: Input the valid data to 'Corporate Level Frequency' field");
		transactionPage.selectCorporateLevelFrequency(levelFrequency);
		
		log.info("ValidataDatatypes_05 - Step 26: Input the valid data to 'Unit Level Financials Frequency' field");
		transactionPage.selectUnitLevelFinancialsFrequency(levelFrequency);
		
		log.info("ValidataDatatypes_05 - Step 27: Input the valid data to 'Contingent Rent' field");
		transactionPage.selectContingentRent();
				
		log.info("ValidataDatatypes_05 - Step 28: Input the valid data to 'Contingent Rent Description' field");
		transactionPage.inputContingentRentDescription(validText);
		
		log.info("ValidataDatatypes_05 - Step 29: Input the valid data to 'Period End' field");
		transactionPage.inputPeriodEnd(validNumber);
		
		log.info("ValidataDatatypes_05 - Step 30: Input the valid data to 'Breakover' field");
		transactionPage.selectBreakover(breakover);
		
		log.info("ValidataDatatypes_05 - Step 31: Input the valid data to 'Days to Report' field");
		transactionPage.inputDaysToReport(validNumber);
				
//		log.info("ValidataDatatypes_05 - Step 32: Input the valid data to 'Renewal Option' field");
//		transactionPage.selectRenewalOption();
		
		log.info("ValidataDatatypes_05 - Step 33: Input the valid data to 'Cross-Defaulted' field");
		transactionPage.selectCrossDefaulted();
		
		log.info("ValidataDatatypes_05 - Step 34: Input the valid data to 'Escrow Rights' field");
		transactionPage.selectEscrowRights();
		
		log.info("ValidataDatatypes_05 - Step 35: Input the valid data to 'STORE Preferred Contract Terms' field");
		transactionPage.selectStorePreferredContracTerms();
		
		log.info("ValidataDatatypes_05 - Step 36: Input the valid data to 'Franchise Agreement Required' field");
		transactionPage.selectFranchiseAgreementRequired();
		
		log.info("ValidataDatatypes_05 - Step 37: Input the valid data to 'Guarantor' field");
		transactionPage.selectGuarantor();
		
		log.info("ValidataDatatypes_05 - Step 38: Input the valid data to 'Escrow Description' field");
		transactionPage.inputEscrowDescription(validText);
		
		log.info("ValidataDatatypes_05 - Step 39: Click 'Save' button");
		transactionPage.clickSaveButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: Verify that 'Workflow Status' is saved successfully");
		verifyTrue(transactionPage.isWorkflowStatusSavedSuccessfully(workflowStatus));
		
		log.info("VP: Verify that 'Salesforce Opportunity ID' is saved successfully");
		verifyTrue(transactionPage.isSalesforceOpportunityIDSavedSuccessfully(validNumber));
		
		log.info("VP: Verify that 'Client ID' is saved successfully");
		verifyTrue(transactionPage.isClientIDSavedSuccessfully(validNumber));
		
		log.info("VP: Verify that 'Deal ID' is saved successfully");
		verifyTrue(transactionPage.isDealIDSavedSuccessfully(validNumber));
		
		log.info("VP: Verify that 'Product Type' is saved successfully");
		verifyTrue(transactionPage.isProductTypeSavedSuccessfully(productType));
		
		log.info("VP: Verify that 'Industry Type' is saved successfully");
		verifyTrue(transactionPage.isIndustryTypeSavedSuccessfully("Auto Dealers (4411)"));
		
		log.info("VP: Verify that 'Portfolio Company' is saved successfully");
		verifyTrue(transactionPage.isPortfolioCompanySavedSuccessfully(validText));
		
		log.info("VP: Verify that 'Store Entity' is saved successfully");
		verifyTrue(transactionPage.isStoreEntitySavedSuccessfully(validText));
		
		log.info("VP: Verify that 'Prorated Days' is saved successfully");
		verifyTrue(transactionPage.isProratedDaysSavedSuccessfully(validNumber));
		
		log.info("VP: Verify that 'Price' is saved successfully");
		verifyTrue(transactionPage.isPriceSavedSuccessfully(validCurrency));
		
		log.info("VP: Verify that 'Lease Expiration Date' is saved successfully");
		verifyTrue(transactionPage.isLeaseExpirationDateSavedSuccessfully(validDate));
		
		log.info("VP: Verify that '1st Payment Date' is saved successfully");
		verifyTrue(transactionPage.isFirstPaymentDateSavedSuccessfully(validDate));
		
		log.info("VP: Verify that 'Security Deposit Amount' is saved successfully");
		verifyTrue(transactionPage.isSercurityDepositAmountSavedSuccessfully(validCurrency));
		
		log.info("VP: Verify that 'Term' is saved successfully");
		verifyTrue(transactionPage.isTermSavedSuccessfully(validNumber));
		
		log.info("VP: Verify that 'Cap Rate' is saved successfully");
		verifyTrue(transactionPage.isCapRateSavedSuccessfully(validRate));
		
		log.info("VP: Verify that 'Original Commencement Date' is saved successfully");
		verifyTrue(transactionPage.isOriginalCommencementDateSavedSuccessfully(validDate));
		
		log.info("VP: Verify that 'Initial Rent' is saved successfully");
		verifyTrue(transactionPage.isInitialRentSavedSuccessfully(validCurrency));
		
		log.info("VP: Verify that 'Contract Commencement Date' is saved successfully");
		verifyTrue(transactionPage.isContractCommencementDateSavedSuccessfully(validDate));
		
		log.info("VP: Verify that 'Corporate Level Frequency' is saved successfully");
		verifyTrue(transactionPage.isCorporateLevelFrequencySavedSuccessfully(levelFrequency));
		
		log.info("VP: Verify that 'Unit Level Financials Frequency' is saved successfully");
		verifyTrue(transactionPage.isUnitLevelFinancialsFrequencySavedSuccessfully(levelFrequency));
		
		log.info("VP: Verify that 'Contingent Rent Description' is saved successfully");
		verifyTrue(transactionPage.isContingentRentDescriptionSavedSuccessfully(validText));
		
		log.info("VP: Verify that 'Period End' is saved successfully");
		verifyTrue(transactionPage.isPeriodEndSavedSuccessfully(validNumber));
		
		log.info("VP: Verify that 'Breakover' is saved successfully");
		verifyTrue(transactionPage.isBreakoverSavedSuccessfully(breakover));
		
		log.info("VP: Verify that 'Days to Report' is saved successfully");
		verifyTrue(transactionPage.isDaysToReportSavedSuccessfully(validNumber));
		
		log.info("VP: Verify that 'Escrow Description' is saved successfully");
		verifyTrue(transactionPage.isEscrowDescriptionSavedSuccessfully(validText));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName, invalidCurrency, invalidDate,invalidNumber;
	private String workflowStatus, productType, levelFrequency, breakover;
	private String validCurrency, validDate, validNumber, validText, validRate;
	}