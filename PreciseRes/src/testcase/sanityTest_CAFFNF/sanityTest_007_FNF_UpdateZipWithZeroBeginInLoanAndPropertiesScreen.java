package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.FNFBorrowersPage;
import page.FNFPropertiesPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_007_FNF_UpdateZipWithZeroBeginInLoanAndPropertiesScreen extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		loanName = "Edit Loan Auto " + accountNumber;
		companyName = "Edit_Borrower " + accountNumber;
		email = "minhdam06@gmail.com";
		zipCode = "06387";
		addressApplicant = "CAF-FNF Applicant Address " + getUniqueNumber();
		addressLender = "CAF-FNF Lender Address " + getUniqueNumber();
		city = "Fresno";
		state = "CA - California";
		date = "04/12/2016";
		number = "30";
		price = "$40.00";
		propertyType ="SFR";
		transactionType ="Refinance";
		titleCompany= "testing company global";
		escrowAgent = "testing escrow global";
		insuranceCompany = "testing insurance global";
	}

	@Test(groups = { "regression" }, description = "Applicant 40 - Login as Applicant - Update Zip with zero in the beginning in both Loan and Property screen")
	public void ZipZeroBegin_01_Borrower_UpdateZipWithZeroInTheBeginning() {
		
		log.info("ZipZeroBegin_01 - Step 01: Login with Applicant");
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);

		log.info("ZipZeroBegin_01 - Step 02: Update Zip with zero in the beginning in Loan");
		applicantsPage.inputEmailZipCode(zipCode, email, zipCode);
		applicantsPage.inputFirstNameAndPhoneNumberTextBoxApplicant("John", "(650) 383-8381");

		log.info("ZipZeroBegin_01 - Step 03: Click on 'Save' button");
		applicantsPage.clickOnSaveButton();

		log.info("VP: Record Saved message displays");
		verifyTrue(applicantsPage.isRecordSavedMessageDisplays());
		
		log.info("VP: Verify that 'Zip code' is saved successfully");
		verifyTrue(applicantsPage.isEmailZipCodeSaved(zipCode, email, zipCode));
		
		log.info("ZipZeroBegin_01 - Step 04: Go to 'Properties' page");
		propertiesPage = applicantsPage.openPropertiesPageFNF(driver, ipClient);
		
		log.info("ZipZeroBegin_01 - Step 05: Click on 'New' button");
		propertiesPage.clickNewPropertyButton();

		log.info("ZipZeroBegin_01 - Step 06: Update Zip with zero in the beginning in Properties'");
		propertiesPage.inputRequiredInformationProperties("", loanName, addressApplicant, city, state, zipCode, date, number, price, propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany);
		
		log.info("ZipZeroBegin_01 - Step 07: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();

		log.info("VP: Record Saved message displays");
		verifyTrue(propertiesPage.isRecordSavedMessageDisplays());
		
		log.info("VP: Verify that 'Zip code' is saved successfully");
		verifyTrue(propertiesPage.isApplicantPropertiesRequiredInformationSaved(loanName, companyName, addressApplicant, city, state, zipCode, date, number, price, propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany));
	}
	
	@Test(groups = { "regression" }, description = "Applicant 41 - Login as Lender - Update Zip with zero in the beginning in both Loan and Property screen")
	public void ZipZeroBegin_02_Lender_UpdateZipWithZeroInTheBeginning() {
		
		log.info("ZipZeroBegin_02 - Step 01. Logout and login with Lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("ZipZeroBegin_02 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("ZipZeroBegin_02 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
			
		log.info("ZipZeroBegin_02 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("ZipZeroBegin_02 - Step 05: Update Zip with zero in the beginning in Loan");
		applicantsPage.inputEmailZipCode(zipCode, email, zipCode);
		applicantsPage.inputFirstNameAndPhoneNumberTextBoxApplicant("John","(650) 383-8381");

		log.info("ZipZeroBegin_02 - Step 06: Click on 'Save' button");
		applicantsPage.clickOnSaveButton();

		log.info("VP: Record Saved message displays");
		verifyTrue(applicantsPage.isRecordSavedMessageDisplays());
		
		log.info("VP: Verify that 'Email, Zip code, Borrower Email/ Zip code' is saved successfully");
		verifyTrue(applicantsPage.isEmailZipCodeSaved(zipCode, email, zipCode));
		
		log.info("ZipZeroBegin_02 - Step 07: Go to 'Properties' page");
		propertiesPage = applicantsPage.openPropertiesPageFNF(driver, ipClient);
		
		log.info("ZipZeroBegin_02 - Step 08: Click on 'New' button");
		propertiesPage.clickNewPropertyButton();

		log.info("ZipZeroBegin_02 - Step 09: Update Zip with zero in the beginning in Properties");
		propertiesPage.inputRequiredInformationProperties(companyName, loanName, addressLender, city, state, zipCode, date, number, price, propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany);
		
		log.info("ZipZeroBegin_02 - Step 10: Click on 'Save' button");
		propertiesPage.clickOnSaveButton();

		log.info("VP: Record Saved message displays");
		verifyTrue(propertiesPage.isRecordSavedMessageDisplays());
		
		log.info("VP: Verify that 'Zip code' is saved successfully");
		verifyTrue(propertiesPage.isApplicantPropertiesRequiredInformationSaved(loanName, companyName, addressLender, city, state, zipCode, date, number, price, propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFBorrowersPage applicantsPage;
	private FNFPropertiesPage propertiesPage;
	private String FNFBorrowerUsername,FNFBorrowerPassword;
	private String FNFLenderUsername, FNFLenderPassword; 
	private String email, companyName, accountNumber, loanName;
	private String zipCode, addressApplicant, addressLender, city, state, date, number, price;
	private String propertyType, transactionType, titleCompany, escrowAgent, insuranceCompany;
	
	}