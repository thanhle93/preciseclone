package page;

import java.util.Set;
import java.util.Stack;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import PreciseRes.Interfaces;
import common.Common;

/**
 * @author Administrator
 */
public class FNFLoansPage extends AbstractPage {
	public FNFLoansPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
	}
	
	/**
	 * input Loan Name, LOC Closing Date, Product Type, Approved Line Amount, Loan Record Type
	 */
	public void inputDataInLoanPage(String loanName, String closeDate, String productType, String approvedLineAmount, String loanRecordType) {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_LOAN_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.FNFLoansPage.FNF_LOAN_NAME_TEXTBOX, loanName);
		sleep();
		type(driver, Interfaces.FNFLoansPage.FNF_LOC_CLOSING_DATE_TEXTBOX, closeDate);
		sleep();
		selectItemCombobox(driver, Interfaces.FNFLoansPage.FNF_PRODUCT_TYPE_COMBOBOX, productType);
		sleep();
		type(driver, Interfaces.FNFLoansPage.FNF_APPROVED_LINE_AMOUNT_TEXTBOX, approvedLineAmount);
		sleep();
		type(driver, Interfaces.FNFLoansPage.FNF_LOAN_RECORD_TYPE_TEXTBOX, loanRecordType);
		sleep();
	}
	
	/**
	 * Select Loans Product Type
	 */
	public void selectProductType(String productType) {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_PRODUCT_TYPE_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.FNFLoansPage.FNF_PRODUCT_TYPE_COMBOBOX, productType);
		sleep();
	}
	
	/**
	 * check Loan Name, LOC Closing Date, Product Type, Approved Line Amount, Loan Record Type display
	 */
	public boolean isDataInLoanPageDisplay(String loanName, String closeDate, String productType, String approvedLineAmount, String loanRecordType) {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_LOAN_NAME_TEXTBOX, timeWait);
		String realLoanName = getAttributeValue(driver, Interfaces.FNFLoansPage.FNF_LOAN_NAME_TEXTBOX, "value");
		String realLocClosingDate = getAttributeValue(driver, Interfaces.FNFLoansPage.FNF_LOC_CLOSING_DATE_TEXTBOX, "value");
		String realProductType = getItemSelectedCombobox(driver, Interfaces.FNFLoansPage.FNF_PRODUCT_TYPE_COMBOBOX);
		String realApprovedLineAmount = getAttributeValue(driver, Interfaces.FNFLoansPage.FNF_APPROVED_LINE_AMOUNT_TEXTBOX, "value");
		String realLoanRecordType = getAttributeValue(driver, Interfaces.FNFLoansPage.FNF_LOAN_RECORD_TYPE_TEXTBOX, "value");
		return realLoanName.contains(loanName) && realLocClosingDate.contains(closeDate) && realProductType.contains(productType) 
				&& realApprovedLineAmount.contains(approvedLineAmount) && realLoanRecordType.contains(loanRecordType);
	}
	
	/**
	 * check 'Loan Name, LOC Closing Date, Product Type, Approved Line Amount, Loan Record Type' Fields Set Read-Only
	 */
	public boolean isDataInLoanPageSetReadOnly(String loanName, String closeDate, String productType, String approvedLineAmount, String loanRecordType) {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_LOAN_NAME_READ_ONLY_TEXTBOX, timeWait);
		String realLoanName = getText(driver, Interfaces.FNFLoansPage.FNF_LOAN_NAME_READ_ONLY_TEXTBOX);
		String realClosingDate = getText(driver, Interfaces.FNFLoansPage.FNF_LOC_CLOSING_DATE_READ_ONLY_TEXTBOX);
		String realProductType = getText(driver, Interfaces.FNFLoansPage.FNF_PRODUCT_TYPE_READ_ONLY_COMBOBOX);
		String realApprovedLineAmount = getText(driver, Interfaces.FNFLoansPage.FNF_APPROVED_LINE_AMOUNT_READ_ONLY_TEXTBOX);
		String realLoanRecordType = getText(driver, Interfaces.FNFLoansPage.FNF_LOAN_RECORD_TYPE_READ_ONLY_TEXTBOX);
//		return realLoanName.contains(loanName) && realClosingDate.contains(closeDate) && realProductType.contains(productType) && realApprovedLineAmount.contains(approvedLineAmount) 
//		&& realLoanRecordType.contains(loanRecordType);
		
		if(!realLoanName.contains(loanName)){
			System.out.println("Failed at: "+realLoanName +"!=" + loanName);
			return false;
		}
		if(!realClosingDate.contains(closeDate)){
			System.out.println("Failed at: "+realClosingDate +" != " + closeDate);
			return false;
		}
		if(!realProductType.contains(productType)){
			System.out.println("Failed at: "+realProductType +"!=" + productType);
			return false;
		}
		if(!realApprovedLineAmount.contains(approvedLineAmount)){
			System.out.println("Failed at: "+realApprovedLineAmount +"!=" + approvedLineAmount);
			return false;
		}
		if(!realLoanRecordType.contains(loanRecordType)){
			System.out.println("Failed at: "+realLoanRecordType +"!=" + loanRecordType);
			return false;
		}
		return true;
	}
	
	/**
	 * input Loan name
	 */
	public void enterLoanName(String loanName){
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_LOAN_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.FNFLoansPage.FNF_LOAN_NAME_TEXTBOX, loanName);
		sleep(2);
	}
	
	/**
	 * click On 'Save' button
	 */
	public void clickOnSaveButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.SAVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		sleep(2);
		isRecordSavedMessageDisplays();
	}
	
	/**
	 * check Record Saved Message Displays
	 * @return recordMessage
	 */
	public boolean isRecordSavedMessageDisplays() {
		waitForControl(driver, Interfaces.FNFLoansPage.RECORD_SAVED_MESSAGE,	timeWait);
		return isControlDisplayed(driver, Interfaces.FNFLoansPage.RECORD_SAVED_MESSAGE);
	}
	
	/**
	 * click On 'Send to Lender for Review' button
	 */
	public void clickOnSendToLenderForReviewButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_SEND_TO_LENDER_REVIEW_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('SendToInitialReview').click();");
		sleep(1);
	}
	
	/**
	 * click On 'Send to Initial Review' button
	 */
	public void clickOnSendToInitialReviewButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_SEND_TO_INITIAL_REVIEW_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('MovetoNextStatus').click();");
		sleep(1);
	}
	
	/**
	 * click On 'Send to Term Sheet Sent' button
	 */
	public void clickOnSendToTermSheetSentButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_SEND_TO_TERM_SHEET_SENT_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('MovetoNextStatus').click();");
		sleep(1);
	}
	
	/**
	 * click On 'Back to Term Sheet Sent' button
	 */
	public void clickOnBackToTermSheetSentButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_BACK_TO_TERM_SHEET_SENT_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('MovetoPreviousStatus').click();");
		sleep(1);
	}
	
	/**
	 * click On 'Back to Documentation' button
	 */
	public void clickOnBackToDocumentationButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_BACK_TO_DOCUMENT_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('MovetoStatusPrevious').click();");
		sleep(1);
	}
	
	/**
	 * click On 'Send to Underwriting' button
	 */
	public void clickOnSendToUnderwritingButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_SEND_TO_UNDERWRITING_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('MovetoNextStatus').click();");
		sleep(1);
	}
	
	/**
	 * click On 'Send to Committee Review' button
	 */
	public void clickOnSendToCommitteeReviewButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_SEND_TO_COMMITTEE_REVIEW_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('MovetoNextStatus').click();");
		sleep(1);
	}
	
	/**
	 * click On 'Send to Documentation' button
	 */
	public void clickOnSendToDocumentationButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_SEND_TO_DOCUMENTATION_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('MovetoNextStatus').click();");
		sleep(1);
	}
	
	/**
	 * click On 'Send to Investment Period' button
	 */
	public void clickOnSendToInvestmentPeriodButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_SEND_TO_INVESTMENT_PERIOD_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('MovetoNextStatus').click();");
		sleep(2);
	}
	
	/**
	 * check 'Status' is changed successful
	 */
	public boolean isStatusChangedSuccessfull(String statusApplicant) {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_CHECK_STATUS_CHANGED_SUCCESSFULL, timeWait);
		String realStatus = getText(driver, Interfaces.FNFLoansPage.FNF_CHECK_STATUS_CHANGED_SUCCESSFULL);
		return realStatus.contains(statusApplicant);
	}
	
	/**
	 * click New Loan button
	 */
	public void clickNewLoanButton(){
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_LOAN_NAME_TEXTBOX, timeWait);
		click(driver, Interfaces.FNFLoansPage.FNF_LOAN_NAME_TEXTBOX);
		sleep(2);
	}
	
	/**
	 * check Detail Message Display With Text
	 */
	public boolean isSavedNameMessageDisplayWithText(String messageContent) {
		waitForControl(driver,	Interfaces.FNFLoansPage.SAVE_NAME_MESSAGE_DISPLAY_WITH_TEXT,messageContent, timeWait);
		return isControlDisplayed(driver,Interfaces.FNFLoansPage.SAVE_NAME_MESSAGE_DISPLAY_WITH_TEXT,messageContent);
	}
	
	/**
	 * click On 'New' Guarantors button
	 */
	public void clickOnNewGuarantorButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.GUARANTOR_NEW_BUTTON,timeWait);
		click(driver, Interfaces.FNFLoansPage.GUARANTOR_NEW_BUTTON);
		sleep();
	}
	
	/**
	 * click On 'New' Documents button
	 */
	public FNFDocumentPage clickOnNewDocumentsButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.DOCUMENTS_NEW_BUTTON,timeWait);
		click(driver, Interfaces.FNFLoansPage.DOCUMENTS_NEW_BUTTON);
		sleep();
		return PageFactory.getFNFDocumentsPage(driver, ipClient);
	}

	/**
	 * input 'Guarantors' info
	 * @param fullName
	 * @param percentageOwned
	 */
	public void inputGuarantorInfo(String fullName,  String percentageOwned) {
		waitForControl(driver, Interfaces.FNFLoansPage.FULLNAME_TEXTBOX,timeWait);
		type(driver, Interfaces.FNFLoansPage.FULLNAME_TEXTBOX, fullName);
		sleep(1);
		type(driver, Interfaces.FNFLoansPage.PERCENTAGE_OWNED_TEXTBOX, percentageOwned);
		sleep(1);
		click(driver, Interfaces.FNFLoansPage.FULLNAME_TEXTBOX);
	}
	
	/**
	 * check 'Guarantor' Saved
	 * @param fullName
	 * @param percentageOwned
	 * @return
	 */
	public boolean isGuarantorSaved(String fullName,String percentageOwned) {
		waitForControl(	driver,Interfaces.FNFLoansPage.SAVE_GUARANTOR_MESSAGE_DISPLAY_WITH_TEXT,fullName, percentageOwned, timeWait);
		return isControlDisplayed(driver,Interfaces.FNFLoansPage.SAVE_GUARANTOR_MESSAGE_DISPLAY_WITH_TEXT, fullName, percentageOwned);
	}
	
	/**
	 * click On 'Back' button
	 */
	public void clickOnBackButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.BACK_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Back').click();");
		sleep(1);
	}
	
	/**
	 * Delete guarantor
	 * @param guarantorName
	 */
	public void deleteGuarator(String guarantorName) {
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_DELETE_GUARANTOR_CHECKBOX, guarantorName, timeWait);
		click(driver, Interfaces.FNFLoansPage.DYNAMIC_DELETE_GUARANTOR_CHECKBOX, guarantorName);
		sleep();
	}

	/**
	 * click On Guarantor 'First Name' Field
	 */
	public void clickOnGuarantorFirstNameFields() {
		waitForControl(driver, Interfaces.FNFLoansPage.FIRSTNAME_FIELD,	timeWait);
		click(driver, Interfaces.FNFLoansPage.FIRSTNAME_FIELD);
		sleep();
	}

	/**
	 * check 'FirstName' Message Display With Text
	 */
	public boolean isFirstNameGuarantorsMessageDisplayWithText(String messageContent) {
		waitForControl(driver, Interfaces.FNFLoansPage.FIRSTNAME_FIELD,messageContent, timeWait);
		return isControlDisplayed(driver,Interfaces.FNFLoansPage.FIRSTNAME_FIELD, messageContent);
	}

	/**
	 * check 'LastName' Message Display With Text
	 * @param messageContent
	 * @return
	 */
	public boolean isLastNameGuarantorsMessageDisplayWithText(String messageContent) {
		waitForControl(driver, Interfaces.FNFLoansPage.LASTNAME_FIELD,messageContent, timeWait);
		return isControlDisplayed(driver,Interfaces.FNFLoansPage.LASTNAME_FIELD, messageContent);
	}

	/**
	 * check 'Guarantor' Message Display With Text
	 * @param messageContent
	 * @return
	 */
	public boolean isGuarantorsMessageDisplayWithText(String messageContent) {
		waitForControl(driver, Interfaces.FNFLoansPage.FIRSTNAME_FIELD,messageContent, timeWait);
		return isControlDisplayed(driver,Interfaces.FNFLoansPage.FIRSTNAME_FIELD, messageContent);
	}
	
	/**
	 * Select underwriter
	 * @param underwriter
	 */
	public void selectUnderwriterCombobox(String underwriter){
		waitForControl(driver, Interfaces.FNFLoansPage.UNDERWRITER_DROPDOWN_LIST,underwriter, timeWait);
		selectItemCombobox(driver, Interfaces.FNFLoansPage.UNDERWRITER_DROPDOWN_LIST, underwriter);
		sleep();
	}
	
	/**
	 * upload Dynamic Document File by Place Holder
	 */
	public void uploadDynamicDocumentFileByPlaceHolder(String documentFolder, String fileName) {
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_CHOOSE_FILE_BUTTON_PLACE_HOLDER, documentFolder, timeWait);
		doubleClick(driver, Interfaces.FNFLoansPage.DYNAMIC_CHOOSE_FILE_BUTTON_PLACE_HOLDER, documentFolder);
		sleep(1);
		Common.getCommon().openFileForUpload(driver, fileName);
		sleep();
	}
	
	/**
	 * upload Dynamic Document File by Place Holder
	 */
	public void uploadDynamicDocumentFileByPlaceHolder(String documentFolder, String fileName1, String fileName2, String fileName3) {
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_CHOOSE_FILE_BUTTON_PLACE_HOLDER, documentFolder, timeWait);
		observeControl(driver, Interfaces.FNFLoansPage.DYNAMIC_CHOOSE_FILE_BUTTON_PLACE_HOLDER, documentFolder);
		doubleClick(driver, Interfaces.FNFLoansPage.DYNAMIC_CHOOSE_FILE_BUTTON_PLACE_HOLDER, documentFolder);
		sleep(1);
		Common.getCommon().openFileForUpload(driver, fileName1, fileName2, fileName3);
		sleep();
	}
	
	/**
	 * check Document Loaded To Document Type
	 */
	public boolean isDocumentLoadedToDocumentType(String documentType, String fileName) {
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL, documentType, fileName,  timeWait);
		return isControlDisplayed(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL, documentType, fileName);
	}
	
	/**
	 * check loaded Document files displayed correctly
	 */
	public boolean isDocumentLoadedToDocumentType(String fileName) {
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENT_FILE_IN_DOCUMENT_FOLDER, fileName,  30);
		return isControlDisplayed(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENT_FILE_IN_DOCUMENT_FOLDER, fileName);
	}
	
	
	/**
	 * check Document History Loaded To Document File, Submit Via, Submitted On, Previous Status, Status
	 */
	public boolean isDocumentHistoryLoadedToDocumentType(String documentFile, String submitVia, String submitOn, String previousStatus, String status) {
		waitForControl(driver, Interfaces.FNFLoansPage.HISTORY_DYNAMIC_DOCUMENT_LOADED_FOR_DOCTYPE, documentFile, submitVia, submitOn, previousStatus, status, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFLoansPage.HISTORY_DYNAMIC_DOCUMENT_LOADED_FOR_DOCTYPE, documentFile, submitVia, submitOn, previousStatus, status);
	}
	
	/**
	 * check Document History Loaded To No Document File, Submit Via, Submitted On, Previous Status, Status
	 */
	public boolean isNoDocumentHistoryLoadedToDocumentType(String submitVia, String submitOn, String previousStatus, String status) {
		waitForControl(driver, Interfaces.FNFLoansPage.HISTORY_DYNAMIC_NO_DOCUMENT_LOADED_FOR_DOCTYPE, submitVia, submitOn, previousStatus, status, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFLoansPage.HISTORY_DYNAMIC_NO_DOCUMENT_LOADED_FOR_DOCTYPE, submitVia, submitOn, previousStatus, status);
	}

	/**
	 * click 'Document Type' detail
	 */
	public void clickDocumentTypeDetail(String documentFile, String documentType) {
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENT_TYPE_DETAIL_PLACE_HOLDER, documentFile, documentType, timeWait);
		click(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENT_TYPE_DETAIL_PLACE_HOLDER, documentFile, documentType);
		sleep();
	}
	
	/**
	 * click 'Document File Name' detail
	 */
	public void clickDocumentFileNameDetail(String submitVia, String documentFile) {
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENT_FILE_NAME_DETAIL_PLACE_HOLDER, submitVia, documentFile, timeWait);
		click(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENT_FILE_NAME_DETAIL_PLACE_HOLDER, submitVia, documentFile);
		sleep();
	}
	
	/**
	 * check Document Viewable
	 */
	public boolean isDocumentViewable(String fileName) {
		sleep(3);
		String currentHandle = driver.getWindowHandle();
		openWindowHandles.push(driver.getWindowHandle());
		Set<String> newHandles = driver.getWindowHandles();
		newHandles.removeAll(openWindowHandles);
		String handle = newHandles.iterator().next();
		driver.switchTo().window(handle);
		String url = getCurrentUrl(driver);
		boolean exists = url.contains(fileName);
		driver.close();
		driver.switchTo().window(currentHandle);
		return exists;
	}
	
	/**
	 * click 'Back to Applicant' button
	 */
	public void clickBackToApplicant() {
		waitForControl(driver, Interfaces.FNFLoansPage.BACK_TO_APPLICANT_BUTTON,	timeWait);
		executeJavaScript(driver, "document.getElementById('BackToApplicant').click();");
		sleep(1);
	}
	
	/**
	 * click 'Reject' Document button
	 */
	public void changeDocumentsStatus(String documentFileName, String status) {
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENTS_STATUS_COMBO_BOX, documentFileName, timeWait);
		selectItemCombobox(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENTS_STATUS_COMBO_BOX, documentFileName, status);
		sleep(1);
	}
	
	/**
	 * click 'Approve' button
	 */
	public void clickApproveButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.APPROVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('ApproveDocument').click();");
		sleep(1);
	}
	
	/**
	 * click On Dynamic History title
	 */
	public void clickOnDynamicHistoryTitle(String documentType, String documentFilename) {
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_HISTORY_POPUP_TITLE,	documentType, documentFilename, timeWait);
		click(driver, Interfaces.FNFLoansPage.DYNAMIC_HISTORY_POPUP_TITLE, documentType, documentFilename);
		sleep();
	}
	
	/**
	 * switch to 'History Popup' frame
	 */
	public WebDriver switchToHistoryPopupFrame(WebDriver driver) {
		waitForControl(driver, Interfaces.FNFLoansPage.HISTORY_POPUP_IFRAME, timeWait);
		sleep();
		driver = driver.switchTo().frame(driver.findElement(By.xpath(Interfaces.FNFLoansPage.HISTORY_POPUP_IFRAME)));
		return driver;
	}
	
		/**
	 * click 'History close' button
	 */
	public void clickHistoryCloseButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.HISTORY_CLOSE_BUTTON, timeWait);
		click(driver, Interfaces.FNFLoansPage.HISTORY_CLOSE_BUTTON);
		sleep(1);
	}
	
	/**
	 * Input valid  'Product Type, Underwriter, Good faith deposits Amount, Good faith deposits Date received'
	 */
	public void inputValidProductTypeOriginatorDepositAmountDepositDate(String productType, String originator) {
		waitForControl(driver, Interfaces.ApplicantsPage.FNF_PRODUCT_TYPE_COMBOBOX, productType, timeWait);
		selectItemCombobox(driver,	Interfaces.ApplicantsPage.FNF_PRODUCT_TYPE_COMBOBOX, productType);
		sleep(1);
		selectItemCombobox(driver, Interfaces.ApplicantsPage.FNF_UNDERWRITER_COMBOBOX, originator);
		sleep(1);
	}
	
	/**
	 * input Loan Name, LOC Closing Date
	 */
	public void inputRequiredData(String loanName, String closeDate) {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_LOAN_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.FNFLoansPage.FNF_LOAN_NAME_TEXTBOX, loanName);
		sleep();
		type(driver, Interfaces.FNFLoansPage.FNF_LOC_CLOSING_DATE_TEXTBOX, closeDate);
		click(driver, Interfaces.FNFLoansPage.FNF_LOAN_NAME_TEXTBOX);
		sleep();
	}
	
	/**
	 * input required date for change Loan to Commitee review
	 */
	public void inputDataRequiredForCommitteeReview(String validDate, String validText, String validNumber, String acquisitionType, String interestRateType, String renovationFunding, String underwritingException, String underwriter, String lender) {
		waitForControl(driver, Interfaces.FNFLoansPage.CAF_LOAN_ID_TEXTBOX, timeWait);
		type(driver, Interfaces.FNFLoansPage.CAF_LOAN_ID_TEXTBOX, validText);
		sleep();
		selectItemCombobox(driver, Interfaces.FNFLoansPage.LENDER_TEXTBOX, lender);
		sleep();
		type(driver, Interfaces.FNFLoansPage.ADVANCE_PERIOD_TEXTBOX, validNumber);
		sleep();
		type(driver, Interfaces.FNFLoansPage.EXIT_FEE_TEXTBOX, validNumber);
		sleep();
		type(driver, Interfaces.FNFLoansPage.CAF_DRAW_FEE_TEXTBOX, validNumber);
		sleep();
		type(driver, Interfaces.FNFLoansPage.DEPOSIT_DATE_TEXTBOX, validDate);
		sleep();
		type(driver, Interfaces.FNFLoansPage.ORIGINATION_FEE_TEXTBOX, validNumber);
		sleep();
		type(driver, Interfaces.FNFLoansPage.DATE_INDIVIDUAL_SEARCH_TEXTBOX, validDate);
		sleep();
		type(driver, Interfaces.FNFLoansPage.UNDERWRITING_FILE_COMPLETE_DATE_TEXTBOX, validDate);
		sleep();
		type(driver, Interfaces.FNFLoansPage.DSCR_TEXTBOX, validNumber);
		sleep();
		type(driver, Interfaces.FNFLoansPage.FNF_LOAN_RECORD_TYPE_TEXTBOX, validText);
		sleep();
		
		selectUnderwriterCombobox(underwriter);
		
		selectItemCombobox(driver, Interfaces.FNFLoansPage.ACQUISITION_DROPDOWN, acquisitionType);
		sleep();
		selectItemCombobox(driver, Interfaces.FNFLoansPage.INTEREST_RATE_TYPE_DROPDOWN, interestRateType);
		sleep();
		selectItemCombobox(driver, Interfaces.FNFLoansPage.RENOVATION_FUNDING_DROPDOWN, renovationFunding);
		sleep();
		selectItemCombobox(driver, Interfaces.FNFLoansPage.UNDERWRITING_EXCEPTION_DROPDOWN, underwritingException);
		sleep();
		selectItemCombobox(driver, Interfaces.FNFLoansPage.REVOLVING_DROPDOWN, renovationFunding);
		sleep();
	}
	
	/**
	 * input required date for change Loan to Documentation review
	 */
	public void inputDataRequiredForDocumentation(String validDate, String validText, String validNumber) {
		waitForControl(driver, Interfaces.FNFLoansPage.NEW_LOAN_CROSSED_TO_PRIOR_TEXTBOX, timeWait);
		type(driver, Interfaces.FNFLoansPage.NEW_LOAN_CROSSED_TO_PRIOR_TEXTBOX, validNumber);
		sleep();
		type(driver, Interfaces.FNFLoansPage.DATE_OF_CERT_OF_GOOD_STANDING_TEXTBOX, validDate);
		sleep();
		type(driver, Interfaces.FNFLoansPage.DATE_ENTITY_SEARCH_TEXTBOX, validDate);
		sleep();
		type(driver, Interfaces.FNFLoansPage.ENTITY_SEARCH_FEE_TEXTBOX, validNumber);
		sleep();
		type(driver, Interfaces.FNFLoansPage.APPROVAL_DATE_TEXTBOX, validDate);
		sleep();
		type(driver, Interfaces.FNFLoansPage.ACTIVE_COUNTIES_TEXTBOX, validNumber);
		sleep();
	}
	
	/**
	 * input required data for moving Loan to Investment Period
	 */
	public void inputDataRequiredForInvestmentPeriod(String validDate, String validText, String validNumber) {
		waitForControl(driver, Interfaces.FNFLoansPage.DEPOSIT_REMAINING_TEXTBOX, timeWait);
//		type(driver, Interfaces.FNFLoansPage.DEPOSIT_REMAINING_TEXTBOX, validNumber);
//		sleep();
		
		type(driver, Interfaces.FNFLoansPage.FEES_DATE_TEXTBOX, validDate);
		sleep();
		type(driver, Interfaces.FNFLoansPage.DATE_CLOSING_DOCS_RECEIVED_TEXTBOX, validDate);
		sleep();
		type(driver, Interfaces.FNFLoansPage.LEGAL_FEE_TEXTBOX, validNumber);
		sleep();
		type(driver, Interfaces.FNFLoansPage.FNF_LOC_CLOSING_DATE_TEXTBOX, validDate);
		keyPressing("enter");
		sleep();
		type(driver, Interfaces.FNFLoansPage.EXPIRATION_DATE_TEXTBOX, validDate);
		sleep();
		type(driver, Interfaces.FNFLoansPage.FNF_APPROVED_LINE_AMOUNT_TEXTBOX, validNumber);
		sleep();		
	}
	
	/**
	 * input Loan Name, LOC Closing Date
	 */
	public void createNewGuarantor(String guarantorName) {
		waitForControl(driver, Interfaces.FNFLoansPage.NEW_GUARANTORS_BUTTON, timeWait);
		click(driver, Interfaces.FNFLoansPage.NEW_GUARANTORS_BUTTON);
		waitForControl(driver, Interfaces.FNFLoansPage.GUARANTOR_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.FNFLoansPage.GUARANTOR_NAME_TEXTBOX, guarantorName);
		sleep();
		clickOnSaveButton();
		clickOnBackButton();
	}
	
	/**
	 * open Document Folder
	 */
	public FNFDocumentPage openDocumentsFolder(String documentFolder) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.DYNAMIC_DOCUMENTS_FOLDER, documentFolder, timeWait);
		click(driver, Interfaces.FNFDocumentsPage.DYNAMIC_DOCUMENTS_FOLDER, documentFolder);
		sleep(1);
		return PageFactory.getFNFDocumentsPage(driver, ipClient);
	}
	
	/**
	 * check 'On Hold' button displays
	 */
	public Boolean isOnHoldButtonDisplayed() {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_ON_HOLD_BUTTON, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFLoansPage.FNF_ON_HOLD_BUTTON);
	}
	
	/**
	 * click On 'On Hold' button
	 */
	public void clickOnOnHoldButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_ON_HOLD_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('SendToOnHold').click();");
		sleep(1);
	}
	
	/**
	 * click On 'Re-open' button
	 */
	public void clickOnReopenButton() {
		waitForControl(driver, Interfaces.FNFLoansPage.FNF_REOPEN_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('SendToReopened').click();");
		sleep(3);
	}
	
	/**
	 * check Error Message Displayed
	 * 
	 * @return
	 */
	public boolean isErrorMessageDisplayed(String messageContent) {
		waitForControl(driver,	Interfaces.ApplicantsPage.ERROR_MESSAGE_DISPLAYED,messageContent, timeWait);
		return isControlDisplayed(driver,Interfaces.ApplicantsPage.ERROR_MESSAGE_DISPLAYED,messageContent);
	}
	
	/**
	 * check Dynamic Alert message title display
	 * 
	 * @param id
	 * @param messageContent
	 * @return
	 */
	public boolean isDynamicAlertMessageTitleDisplay(String id, String messageContent) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_FIELD_ALERT_MESSAGE_TITLE, id, messageContent, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_FIELD_ALERT_MESSAGE_TITLE, id, messageContent);
	}
	
	/**
	 * input required data for moving Loan to Investment Period
	 */
	public void inputExpirationDate(String validDate) {
		waitForControl(driver, Interfaces.FNFLoansPage.EXPIRATION_DATE_TEXTBOX, timeWait);
		type(driver, Interfaces.FNFLoansPage.EXPIRATION_DATE_TEXTBOX, validDate);
		sleep();
	}
	
	/**
	 * Click Back To List Button
	 */
	public void clickBackToListButton() {
		waitForControl(driver, Interfaces.ReportsPage.BACK_TO_LIST_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('BackToList').click();");
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		sleep();
	}
	
	/**
	 * input required date for Loan Searching
	 */
	public void inputDataForLoanSearching(String originator, String underwriter, String validNumber, String validDate) {
		waitForControl(driver, Interfaces.FNFLoansPage.UNDERWRITER_DROPDOWN_LIST, timeWait);
		selectUnderwriterCombobox(underwriter);
		sleep();
		selectItemCombobox(driver, Interfaces.FNFLoansPage.ORIGINATOR_DROPDOWN, originator);
		sleep();
		type(driver, Interfaces.FNFLoansPage.FNF_APPROVED_LINE_AMOUNT_TEXTBOX, validNumber);
		sleep();	
		type(driver, Interfaces.FNFLoansPage.CAF_DRAW_FEE_TEXTBOX, validNumber);
		keyPressing("enter");
		sleep();
		type(driver, Interfaces.FNFLoansPage.EXPIRATION_DATE_TEXTBOX, validDate);
		sleep();
	}
	
	/**
	 * get Field index
	 */
	public String getIndexOfField(String fieldName){
		waitForControl(driver,	Interfaces.FNFLoansPage.FNF_LOANS_SEARCH_HEADER, timeWait);
		int numbersOfField = countElement(driver, Interfaces.FNFLoansPage.FNF_LOANS_SEARCH_HEADER);
		for(int i=2;i<=numbersOfField; i++){
			String index = Integer.toString(i);
			if(getText(driver,	Interfaces.FNFLoansPage.DYNAMIC_FNF_LOANS_SEARCH_HEADER, index).contains(fieldName)) return index;
		}
		return null;
	}
	
	/**
	 * get Field text
	 */
	public String getRecordValue(String fieldName){
		String index = getIndexOfField(fieldName);
		waitForControl(driver,	Interfaces.FNFLoansPage.DYNAMIC_FNF_LOANS_SEARCH_RESULT, index, timeWait);
		return getText(driver,	Interfaces.FNFLoansPage.DYNAMIC_FNF_LOANS_SEARCH_RESULT, index);
	}
	
	/**
	 * check record displayed
	 */
	public Boolean isRecordValueDisplayedCorrectly(String fieldName, String value){
		String index = getIndexOfField(fieldName);
		waitForControl(driver,	Interfaces.FNFLoansPage.DYNAMIC_FNF_LOANS_SEARCH_RESULT, index, timeWait);
		System.out.println(getText(driver,	Interfaces.FNFLoansPage.DYNAMIC_FNF_LOANS_SEARCH_RESULT, index));
		return getText(driver,	Interfaces.FNFLoansPage.DYNAMIC_FNF_LOANS_SEARCH_RESULT, index).contains(value);
	}
	
	/**
	 * check record displayed
	 */
	public Boolean isAllRecordValuesDisplayedCorrectly(String companyName, String status, String productType, String originator, String underwriter, String validNumber, String validDate){
		sleep(2);
		return (
				isRecordValueDisplayedCorrectly("Borrower",companyName) 
				&& isRecordValueDisplayedCorrectly("Status",status) 
				&& isRecordValueDisplayedCorrectly("Product Type",productType) 
				&& isRecordValueDisplayedCorrectly("Originator", originator) 
				&& isRecordValueDisplayedCorrectly("Underwriter", underwriter)
				&& isRecordValueDisplayedCorrectly("LOC Commitment", validNumber) 
				&& isRecordValueDisplayedCorrectly("CAF Draw Fee%", validNumber+"%") 
				&& isRecordValueDisplayedCorrectly("Current Close Date", validDate) 
				&& isRecordValueDisplayedCorrectly("Expiration Date",validDate)
				&& isRecordValueDisplayedCorrectly("Line Maturity Date",validDate));
	}
	
	/**
	 * check record displayed
	 */
	public Boolean isAllRecordValuesDisplayedCorrectly(String status, String productType, String originator, String underwriter, String validNumber, String validDate){
		sleep(2);
		return (
				isRecordValueDisplayedCorrectly("Status",status) 
				&& isRecordValueDisplayedCorrectly("Product Type",productType) 
				&& isRecordValueDisplayedCorrectly("Originator", originator) 
				&& isRecordValueDisplayedCorrectly("Underwriter", underwriter)
				&& isRecordValueDisplayedCorrectly("LOC Commitment", validNumber) 
				&& isRecordValueDisplayedCorrectly("CAF Draw Fee%", validNumber+"%") 
				&& isRecordValueDisplayedCorrectly("Current Close Date", validDate) 
				&& isRecordValueDisplayedCorrectly("Expiration Date",validDate)
				&& isRecordValueDisplayedCorrectly("Line Maturity Date",validDate));
	}
	
	/**
	 * click On 'Search' button
	 */
	public void clickOnSearchButton(String applicantName) {
		waitForControl(driver, Interfaces.FNFLoansPage.SEARCH_LOANS_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.FNFLoansPage.SEARCH_LOANS_NAME_TEXTBOX, applicantName);
		sleep(1);
		waitForControl(driver, Interfaces.ApplicantsPage.SEARCH_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		sleep(2);
	}
	
	/**
	 * Select Borrower for search
	 */
	public void selectBorrowerForSearch(String value) {
		waitForControl(driver, Interfaces.FNFLoansPage.SEARCH_BORROWER_NAME_DROPDOWN, value, timeWait);
		selectItemCombobox(driver,	Interfaces.FNFLoansPage.SEARCH_BORROWER_NAME_DROPDOWN, value);
		sleep(1);
	}
	
	/**
	 * Select Status for search
	 */
	public void selectStatusForSearch(String value) {
		waitForControl(driver, Interfaces.FNFLoansPage.SEARCH_STATUS_NAME_DROPDOWN, value, timeWait);
		selectItemCombobox(driver,	Interfaces.FNFLoansPage.SEARCH_STATUS_NAME_DROPDOWN, value);
		sleep(1);
	}
	
	/**
	 * Select Product Type for search
	 */
	public void selectProductTypeForSearch(String value) {
		waitForControl(driver, Interfaces.FNFLoansPage.SEARCH_PRODUCT_TYPE_NAME_DROPDOWN, value, timeWait);
		selectItemCombobox(driver,	Interfaces.FNFLoansPage.SEARCH_PRODUCT_TYPE_NAME_DROPDOWN, value);
		sleep(1);
	}
	
	/**
	 * Select Originator for search
	 */
	public void selectOriginatorForSearch(String value) {
		waitForControl(driver, Interfaces.FNFLoansPage.SEARCH_ORIGINATOR_NAME_DROPDOWN, value, timeWait);
		selectItemCombobox(driver,	Interfaces.FNFLoansPage.SEARCH_ORIGINATOR_NAME_DROPDOWN, value);
		sleep(1);
	}
	
	/**
	 * Select Underwriter for search
	 */
	public void selectUnderwriterForSearch(String value) {
		waitForControl(driver, Interfaces.FNFLoansPage.SEARCH_UNDERWRITER_NAME_DROPDOWN, value, timeWait);
		selectItemCombobox(driver,	Interfaces.FNFLoansPage.SEARCH_UNDERWRITER_NAME_DROPDOWN, value);
		sleep(1);
	}
	
	/**
	 * input CAF Draw Fee For Search
	 */
	public void inputCAFDrawFeeForSearch(String value) {
		waitForControl(driver, Interfaces.FNFLoansPage.SEARCH_CAF_DRAW_FEE_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.FNFLoansPage.SEARCH_CAF_DRAW_FEE_NAME_TEXTBOX, value);
		keyPressing("enter");
		sleep();
	}
	
	/**
	 * input Current Close Date For Search
	 */
	public void selectCurrentCloseDateForSearch(String value) {
		waitForControl(driver, Interfaces.FNFLoansPage.SEARCH_CURRENT_CLOSE_DATE_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.FNFLoansPage.SEARCH_CURRENT_CLOSE_DATE_NAME_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input Expiration Date For Search
	 */
	public void selectExpirationDateForSearch(String value) {
		waitForControl(driver, Interfaces.FNFLoansPage.SEARCH_EXPIRATION_DATE_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.FNFLoansPage.SEARCH_EXPIRATION_DATE_NAME_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * input Line Maturity Date For Search
	 */
	public void selectLineMaturityDateForSearch(String value) {
		waitForControl(driver, Interfaces.FNFLoansPage.SEARCH_LINE_MATURITY_DATE_NAME_TEXTBOX, timeWait);
		type(driver, Interfaces.FNFLoansPage.SEARCH_LINE_MATURITY_DATE_NAME_TEXTBOX, value);
		sleep();
	}
	
	/**
	 * click on check all link
	 */
	public void clickOnCheckAllLink() {
		waitForControl(driver,	Interfaces.ApplicantsPage.CHECK_ALL_LINK, timeWait);
		click(driver, Interfaces.ApplicantsPage.CHECK_ALL_LINK);
		sleep(1);
	}
	
	/**
	 * click Make Inactive Button
	 */
	public void clickMakeInactiveButton() {
		waitForControl(driver, Interfaces.ApplicantsPage.MAKE_INACTIVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('SetInactive').click();");
		sleep();
		acceptJavascriptAlert(driver);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}

	/**
	 * click Make Active Button 
	 */
	public void clickMakeActiveButton() {
		waitForControl(driver, Interfaces.ApplicantsPage.MAKE_ACTIVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('SetActive').click();");
		sleep();
		acceptJavascriptAlert(driver);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}
	
	/**
	 * select Active radio button
	 */
	public void selectActiveRadioButton(String status) {
		waitForControl(driver,	Interfaces.ApplicantsPage.BORROWER_NAME_TEXTBOX, timeWait);
		if(status.contains("Yes")) 
		click(driver, Interfaces.ApplicantsPage.DYNAMIC_FNF_BORROWER_SEARCH_ACTIVE_RADIO_BUTTON, "1");
		else if(status.contains("No")) click(driver, Interfaces.ApplicantsPage.DYNAMIC_FNF_BORROWER_SEARCH_ACTIVE_RADIO_BUTTON, "0");
		else click(driver, Interfaces.ApplicantsPage.DYNAMIC_FNF_BORROWER_SEARCH_ACTIVE_RADIO_BUTTON, "");
	}
	
	/**
	 * open Loans Detail Page
	 */
	public void openLoansDetailPage(String loanName) {
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_LOAN_NAME_LINK, loanName, timeWait);
		click(driver, Interfaces.FNFLoansPage.DYNAMIC_LOAN_NAME_LINK, loanName);
		sleep(1);
	}
	
	/**
	 * check Info Message Displayed
	 * 
	 * @return
	 */
	public boolean isInfoMessageDisplayed(String messageContent) {
		waitForControl(driver,	Interfaces.ApplicantsPage.INFO_MESSAGE_DISPLAYED,messageContent, 10);
		return isControlDisplayed(driver,Interfaces.ApplicantsPage.INFO_MESSAGE_DISPLAYED,messageContent);
	}
	
	/**
	 * open Document Folder
	 */
	public Boolean isDocumentsFolderDisplayed(String documentFolder) {
		waitForControl(driver, Interfaces.FNFDocumentsPage.DYNAMIC_DOCUMENTS_FOLDER, documentFolder, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFDocumentsPage.DYNAMIC_DOCUMENTS_FOLDER, documentFolder);
	}
	
	/**
	 * open Properties Tab
	 */
	public FNFPropertiesPage openPropertiesTab() {
		waitForControl(driver, Interfaces.LoansPage.PROPERTIES_TAB, timeWait);
		executeClick(driver, Interfaces.LoansPage.PROPERTIES_TAB);
//		executeJavaScript(driver, "document.getElementById('PropertyTB').click();");
		sleep();
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
		return PageFactory.getFNFPropertiesPage(driver, ipClient);
	}
	
	/**
	 * open Properties Tab
	 */
	public FNFBorrowersPage openBorrowerRecord() {
		waitForControl(driver, Interfaces.FNFLoansPage.BORROWER_LINK, timeWait);
		click(driver, Interfaces.FNFLoansPage.BORROWER_LINK);
		return PageFactory.getFNFBorrowersPage(driver, ipClient);
	}
	
	/**
	 * check textfield is read only
	 */
	public boolean isTextfieldReadOnly(String textfieldName){
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_READ_ONLY_TEXTFIELD, textfieldName, timeWait);
		return isControlDisplayed(driver, Interfaces.FNFLoansPage.DYNAMIC_READ_ONLY_TEXTFIELD, textfieldName);
	}
	
	/**
	 * check textfield is read only
	 */
	public boolean isReadOnlyTextfieldDisplayCorrectly(String textfieldName, String value){
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_READ_ONLY_TEXTFIELD, textfieldName, timeWait);
		return getAttributeValue(driver, Interfaces.FNFLoansPage.DYNAMIC_READ_ONLY_TEXTFIELD, textfieldName, "value").contains(value);
	}
	
	/**
	 * check textfield displays correctly
	 */
	public boolean isTextfieldDisplayedCorrectly(String textfieldName, String value){
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_TEXTFIELD_BY_NAME, textfieldName, timeWait);
		return getAttributeValue(driver, Interfaces.FNFPropertiesPage.DYNAMIC_TEXTFIELD_BY_NAME, textfieldName, "value").contains(value);
	}
	
	/**
	 * input value to textfield
	 */
	public void inputTextfieldByName(String textfieldName, String value){
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_TEXTFIELD_BY_NAME, textfieldName, timeWait);
		type(driver, Interfaces.FNFPropertiesPage.DYNAMIC_TEXTFIELD_BY_NAME, textfieldName, value);
	}
	
	/**
	 * select item from dropdown
	 */
	public void selectItemFromDropdown(String dropdownName, String value){
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_DROPDOWN_BY_NAME, dropdownName, timeWait);
		selectItemCombobox(driver, Interfaces.FNFPropertiesPage.DYNAMIC_DROPDOWN_BY_NAME, dropdownName, value);
	}
	
	/**
	 * check dropdown displays correctly
	 */
	public boolean isDropdownDisplayedCorrectly(String textfieldName, String value){
		waitForControl(driver, Interfaces.FNFPropertiesPage.DYNAMIC_DROPDOWN_BY_NAME, textfieldName, timeWait);
		String realValue = getItemSelectedCombobox(driver, Interfaces.FNFPropertiesPage.DYNAMIC_DROPDOWN_BY_NAME, textfieldName);
		return realValue.contains(value);
	}
	
	/**
	 * check Document Loaded To Document Type
	 */
	public boolean isDocumentFolderNameDisplayedCorrectly(String documentType, String numberOfDocumentsLoaded) {
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENTS_FOLDER_NAME, documentType, numberOfDocumentsLoaded,  timeWait);
		return isControlDisplayed(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENTS_FOLDER_NAME, documentType, numberOfDocumentsLoaded);
	}
	
	/**
	 * click on Placeholder Link
	 */
	public FNFDocumentPage clickOnPlaceholderLink(String documentType, String linkName) {
		waitForControl(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL, documentType, linkName,  timeWait);
		click(driver, Interfaces.FNFLoansPage.DYNAMIC_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL, documentType, linkName);
		sleep(2);
		return PageFactory.getFNFDocumentsPage(driver, ipClient);
	}
	
	public void inputDataForCheckingAddPropertyBorrower(String maxLTC, String maxLTV, String cafDrawFee){
		inputTextfieldByName("Max LTC%", maxLTC);
		inputTextfieldByName("Max LTV%", maxLTV);
		inputTextfieldByName("CAF Draw Fee (%)", cafDrawFee);
	}
	
	private WebDriver driver;
	private String ipClient;
	private final Stack<String> openWindowHandles = new Stack<String>();
}