package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;
import page.UsersPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_026_STO_TransactionsChildTable extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		dateCreated = Constant.CURRENT_YEAR;
		accountNumber = getUniqueNumber();
		xmlFileName = "Transaction New.xml";
		companyName = "Testing QA " + accountNumber;
		firstName = "Testing ";
		email = "qatesting" + accountNumber + "@gmail.com";
		address = "Street " + accountNumber ;
		city = "Los Angeles";
		state = "CA - California";
		zipCode = "90001";
		lesseeName = "New Lessee " + accountNumber;
	}

	@Test(groups = { "regression" }, description = "TransactionsChildTable_01 - Check Transactions table display")
	public void TransactionsChildTable_01_CheckTransactionsTableDisplay() {
		
		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("Precondition - Step 02: Click 'New' Transaction button");
		transactionPage.clickNewTransactionButton();

		log.info("Precondition - Step 03: Click 'Load' Transaction button");
		transactionPage.clickLoadTransactionButton();

		log.info("Precondition - Step 04: Upload XML filename");
		transactionPage.uploadTransactionXML(xmlFileName);

		log.info("Precondition - Step 05: Update Company name");
		transactionPage.inputCompanyName(companyName);
		transactionPage.inputClientID(companyName);

		log.info("Precondition - Step 06: Click 'Save' button");
		transactionPage.clickSaveButton();

		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("Precondition - Step 07: Click 'New External Entities' button");
		transactionPage.clickNewExternalEntitiesButton();
		
		log.info("Precondition - Step 08: Select 'Lessee' in 'Entity Type' dropdown");
		transactionPage.selectEntityType("Lessee");
		
		log.info("Precondition - Step 09: Select 'Individual' in 'Entity Classification' dropdown");
		transactionPage.selectEntityClassification("Individual");
		
		log.info("Precondition - Step 10. Input Lessee information");
		transactionPage.inputExternalEntitiesInfoToCreate(accountNumber, firstName, lesseeName, lesseeName, email, address, city, state, zipCode);
		
		log.info("Precondition - Step 11: Click 'Create New Entity' button");
		transactionPage.clickNewEntityButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("VP: New Lessee is saved");
		verifyTrue(transactionPage.isNewExternalEntitiesSaved(lesseeName));
		
		log.info("Precondition - Step 12. Input Lessee Provider information");
		transactionPage.inputExternalEntitiesInfoToCreateNewLogin(firstName, lesseeName, email);
		
		log.info("Precondition - Step 13: Click 'Create New Entity' button");
		transactionPage.clickNewLoginButton();
		
		log.info("VP: 'Record saved' message is displayed");
		verifyTrue(transactionPage.isBasicDetailMessageTitleDisplay("Record saved"));
		
		log.info("Precondition - Step 14. Click Back button on New External Entites");
		transactionPage.clickBackExternalEntitiesButton();	
		
		log.info("VP: The Lessee display on External Entities table");
		verifyTrue(transactionPage.isNewEntitiesDisplayOnExternalEntitiesTable(lesseeName, "Lessee"));

		log.info("TransactionsChildTable_01 - Step 01: Open Users page");
		usersPage = transactionPage.openUsersPage();
	
		log.info("TransactionsChildTable_01 - Step 02: Search Users by User name");
		usersPage.searchName(lesseeName);
		
		log.info("TransactionsChildTable_01 - Step 03: Open User details page");
		usersPage.openUserDetailsOfExistingLogin();
		
		log.info("VP: Make sure Transaction child table displayed correctly");
		verifyTrue(usersPage.isTransactionsChildTableDisplayedCorrectly("Lessee QA", companyName, companyName, dateCreated, "Yes"));
		
		log.info("TransactionsChildTable_01 - Step 04: Click on Tracsaction link");
		transactionPage = usersPage.clickOnTransactionLink();
		
		log.info("VP: Transaction basic details page displayed correctly");
		verifyTrue(transactionPage.isCompanyNameSavedSuccessfully(companyName));
		
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private UsersPage usersPage;
	private String storeLenderUsername, storeLenderPassword;
	private String xmlFileName, companyName, dateCreated;
	private String accountNumber, firstName, lesseeName;
	private String address, city, state, zipCode, email;
}