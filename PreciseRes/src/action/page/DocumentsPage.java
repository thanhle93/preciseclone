package page;

import org.openqa.selenium.WebDriver;

import PreciseRes.Interfaces;
import common.Common;

public class DocumentsPage extends AbstractPage {
	public DocumentsPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}

	/**
	 * check Documents page display
	 * @param N/A
	 */
	public boolean isDocumentsPageDisplay() {
		waitForControl(driver, Interfaces.DocumentsPage.DOCUMENTS_PAGE_SECTION, timeWait);
		return isControlDisplayed(driver, Interfaces.DocumentsPage.DOCUMENTS_PAGE_SECTION);
	}
	
	/**
	 * search Document Type
	 */
	public void searchDocumentType(String documentType) {
		waitForControl(driver, Interfaces.DocumentsPage.SEARCH_DOCUMENT_TYPE_DROPDOWN, timeWait);
		click(driver, Interfaces.DocumentsPage.SEARCH_DOCUMENT_TYPE_DROPDOWN);
		waitForControl(driver, Interfaces.DocumentsPage.UNCHECK_ALL_LINK, timeWait);
		sleep(1);
		click(driver, Interfaces.DocumentsPage.UNCHECK_ALL_LINK);
		sleep(1);
		waitForControl(driver, Interfaces.DocumentsPage.DYNAMIC_DOCUMENT_TYPE_CHECKBOX, documentType, timeWait);
		click(driver, Interfaces.DocumentsPage.DYNAMIC_DOCUMENT_TYPE_CHECKBOX, documentType);
		sleep(1);
		click(driver, Interfaces.DocumentsPage.SEARCH_DOCUMENT_TYPE_DROPDOWN);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(7);
     }
		sleep(1);
	}

	/**
	 * search Active
	 */
	public void searchActive() {
		waitForControl(driver, Interfaces.DocumentsPage.ACTIVE_RADIO_BUTTON, timeWait);
		click(driver, Interfaces.DocumentsPage.ACTIVE_RADIO_BUTTON);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(7);
     }
		sleep();
	}

	/**
	 * check 'Review Selected' Document Loaded To Document Type
	 */
	public boolean isReviewSelectedDocumentLoadedToDocumentType(String docType, String reviewedBy) {
		waitForControl(driver,
				Interfaces.PropertiesPage.DYNAMIC_REVIEW_SELECTED_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL, docType,	reviewedBy, timeWait);
		return isControlDisplayed(driver,
				Interfaces.PropertiesPage.DYNAMIC_REVIEW_SELECTED_DOCUMENT_FOR_DOCTYPE_ON_PROPERTY_DETAIL, docType,reviewedBy);
	}

	/**
	 * check 'Activated' Document Loaded
	 */
	public boolean isActivatedDocumentLoadedToDocumentType(String docType, String active) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_ACTIVATED_DOCUMENT_FOR_DOCTYPE, docType, active,timeWait);
		return isControlDisplayed(driver, Interfaces.PropertiesPage.DYNAMIC_ACTIVATED_DOCUMENT_FOR_DOCTYPE, docType,
				active);
	}

	/**
	 * check 'Document Type' checkbox
	 */
	public void isSelectedDocumentTypeCheckbox(String documentType, String activeStatus) {
		waitForControl(driver, Interfaces.PropertiesPage.SELECTED_DOCUMENT_TYPE_CHECKBOX, documentType, activeStatus,timeWait);
		checkTheCheckbox(driver, Interfaces.PropertiesPage.SELECTED_DOCUMENT_TYPE_CHECKBOX, documentType, activeStatus);
		sleep(2);
	}

	/**
	 * click On Make Document Active Button
	 */
	public void clickMakeDocumentActiveButton() {
		waitForControl(driver, Interfaces.PropertiesPage.MAKE_DOCUMENT_ACTIVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('MakeDocumentActive').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(7);
     }
		sleep(2);
	}

	/**
	 * search Section and Document Type
	 */
	public void searchSectionAndDocumentType(String documentSection, String documentType) {
		waitForControl(driver, Interfaces.DocumentsPage.SEARCH_SECTION_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.DocumentsPage.SEARCH_SECTION_COMBOBOX, documentSection);
		sleep();
		if (!documentType.equals("")) {
			waitForControl(driver, Interfaces.DocumentsPage.SEARCH_DOCUMENT_TYPE_DROPDOWN, timeWait);
			click(driver, Interfaces.DocumentsPage.SEARCH_DOCUMENT_TYPE_DROPDOWN);
			sleep(1);
			waitForControl(driver, Interfaces.DocumentsPage.UNCHECK_ALL_LINK, timeWait);
			click(driver, Interfaces.DocumentsPage.UNCHECK_ALL_LINK);
			sleep(1);
			waitForControl(driver, Interfaces.DocumentsPage.DYNAMIC_DOCUMENT_TYPE_CHECKBOX, documentType, timeWait);
			click(driver, Interfaces.DocumentsPage.DYNAMIC_DOCUMENT_TYPE_CHECKBOX, documentType);
			sleep(1);
			click(driver, Interfaces.DocumentsPage.SEARCH_DOCUMENT_TYPE_DROPDOWN);
			sleep(1);
		}
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(7);
     }
		sleep();
	}

	/**
	 * search Section and Uncheck document type
	 */
	public void searchSectionAndUncheckDocumentType(String documentSection) {
		waitForControl(driver, Interfaces.DocumentsPage.SEARCH_SECTION_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.DocumentsPage.SEARCH_SECTION_COMBOBOX, documentSection);
		sleep(1);
		waitForControl(driver, Interfaces.DocumentsPage.SEARCH_DOCUMENT_TYPE_DROPDOWN, timeWait);
		click(driver, Interfaces.DocumentsPage.SEARCH_DOCUMENT_TYPE_DROPDOWN);
		sleep(1);
		waitForControl(driver, Interfaces.DocumentsPage.UNCHECK_ALL_LINK, timeWait);
		click(driver, Interfaces.DocumentsPage.UNCHECK_ALL_LINK);
		sleep(1);
		click(driver, Interfaces.DocumentsPage.SEARCH_DOCUMENT_TYPE_DROPDOWN);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(7);
     }
		sleep();
	}

	/**
	 * open Document Detail
	 */
	public void openDocumentDetail(String documentType) {
		waitForControl(driver, Interfaces.DocumentsPage.DYNAMIC_DOCUMENT_ID, documentType, timeWait);
		click(driver, Interfaces.DocumentsPage.DYNAMIC_DOCUMENT_ID, documentType);
		sleep();
	}

	/**
	 * upload Document File
	 */
	public void uploadDocumentFile(String documentFileName) {
		waitForControl(driver, Interfaces.DocumentsPage.CHOOSE_FILE_BUTTON, timeWait);
		doubleClick(driver, Interfaces.DocumentsPage.CHOOSE_FILE_BUTTON);
		sleep(1);
		Common.getCommon().openFileForUpload(driver, documentFileName);
		sleep();
	}

	/**
	 * uncheck Active Checkbox
	 */
	public void uncheckActiveCheckbox() {
		waitForControl(driver, Interfaces.PropertiesPage.ACTIVE_CHECKBOX, timeWait);
		uncheckTheCheckbox(driver, Interfaces.PropertiesPage.ACTIVE_CHECKBOX);
		sleep(1);
	}

	/**
	 * click Save Button [General - Corporate]
	 */
	public void clickSaveButton() {
		waitForControl(driver, Interfaces.DocumentsPage.SAVE_BUTTON, timeWait);
		executeJavaScript(driver, "document.getElementById('Save').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(7);
     }
		sleep(5);
		isRecordSavedMessageDisplays();
	}
	
	/**
	 * check Record Saved Message Displays
	 * 
	 * @return
	 */
	public boolean isRecordSavedMessageDisplays() {
		waitForControl(driver, Interfaces.LoansPage.RECORD_SAVED_MESSAGE, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.RECORD_SAVED_MESSAGE);
	}
	
	/**
	 * Check Document History loaded to Document File, Submit Via, Submitted On
	 */
	public boolean isDocumentHistoryLoadedToDocumentType(String documentFile, String submitVia, String submitOn) {
		waitForControl(driver, Interfaces.LoansPage.DYNAMIC_HISTORY_DOCUMENT_LOADED_FOR_DOCTYPE, documentFile, submitVia, submitOn, timeWait);
		return isControlDisplayed(driver, Interfaces.LoansPage.DYNAMIC_HISTORY_DOCUMENT_LOADED_FOR_DOCTYPE, documentFile, submitVia, submitOn);
	}

	/**
	 * click Document List Button
	 */
	public void clickDocumentListButton() {
		waitForControl(driver, Interfaces.DocumentsPage.DOCUMENT_LIST_BUTTON, timeWait);
		click(driver, Interfaces.DocumentsPage.DOCUMENT_LIST_BUTTON);
		if(driver.toString().toLowerCase().contains("internetexplorer")){
			executeJavaScript(driver, "document.getElementById('GotoLAList').click();");
            sleep(5);
     }
		sleep(3);
	}

	/**
	 * check Document Uploaded To Document Type
	 */
	public boolean isDocumentUploadedToDocumentType(String documentType) {
		waitForControl(driver, Interfaces.DocumentsPage.DYNAMIC_DOCUMENT_LOADED, documentType, timeWait);
		return isControlDisplayed(driver, Interfaces.DocumentsPage.DYNAMIC_DOCUMENT_LOADED, documentType);
	}
	
	/**
	 * check Section and Document Type display correctly
	 */
	public boolean isSectionAndDocumentTypeDisplay(String section, String documentType) {
		waitForControl(driver, Interfaces.DocumentsPage.DYNAMIC_SECTION_DOCUMENT_TYPE_LOADED, section, documentType, timeWait);
		return isControlDisplayed(driver, Interfaces.DocumentsPage.DYNAMIC_SECTION_DOCUMENT_TYPE_LOADED, section, documentType);
	}

	/**
	 * check No Document Uploaded To Document Type
	 */
	public boolean isNoDocumentUploadedToDocumentType(String documentType) {
		waitForControl(driver, Interfaces.DocumentsPage.DYNAMIC_NO_DOCUMENT_LOADED, documentType, timeWait);
		return isControlDisplayed(driver, Interfaces.DocumentsPage.DYNAMIC_NO_DOCUMENT_LOADED, documentType);
	}

	/**
	 * check Document Loaded In Documents Tab NAV
	 */
	public boolean isDocumentLoadedInDocumentsTabNav(String documentID, String documentType) {
		waitForControl(driver, Interfaces.DocumentsPage.DYNAMIC_DOCUMENT_LOADED_IN_DOCUMENTS_NAV, documentID,documentType, timeWait);
		return isControlDisplayed(driver, Interfaces.DocumentsPage.DYNAMIC_DOCUMENT_LOADED_IN_DOCUMENTS_NAV, documentID,documentType);
	}

	/**
	 * open Corporate Entity Documents Tab
	 */
	public void openCorporateEntityDocumentsTab() {
		waitForControl(driver, Interfaces.DocumentsPage.CORPORATE_ENTITY_DOCUMENT_TAB, timeWait);
		executeJavaScript(driver, "document.getElementById('CorpEntityLoanDocsTB').click();");
		executeJavaScript(driver, "document.getElementById('CBRCorpEntityLoanDocsTB').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(5);
     }
		sleep();
	}

	/**
	 * open Task Tab
	 */
	public LoansPage openTaskTab() {
		waitForControl(driver, Interfaces.DocumentsPage.TASK_TAB, timeWait);
		executeJavaScript(driver, "document.getElementById('TaskSearchTB').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(7);
     }
		sleep();
		return PageFactory.getLoansPage(driver, ipClient);
	}

	/**
	 * create New Task
	 */
	public void createNewTask(String taskTemplate, String taskName) {
		clickNewTaskButton();
		waitForControl(driver, Interfaces.DocumentsPage.TASK_TEMPLATE_COMBOBOX, timeWait);
		selectItemCombobox(driver, Interfaces.DocumentsPage.TASK_TEMPLATE_COMBOBOX, taskTemplate);
		type(driver, Interfaces.DocumentsPage.TASK_NAME_TEXTBOX, taskName);
		checkTheCheckbox(driver, Interfaces.DocumentsPage.ALL_PROPERTIES_CHECKBOX);
		clickSaveButton();
	}

	/**
	 * click New Task Button
	 */
	public void clickNewTaskButton() {
		waitForControl(driver, Interfaces.DocumentsPage.NEW_TASK_BUTTON, timeWait);
		click(driver, Interfaces.DocumentsPage.NEW_TASK_BUTTON);
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(7);
     }
		sleep();
	}

	/**
	 * click Task List Button
	 */
	public void clickTaskListButton() {
		waitForControl(driver, Interfaces.DocumentsPage.TASK_LIST_BUTTON, timeWait);
		click(driver, Interfaces.DocumentsPage.TASK_LIST_BUTTON);
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(7);
     }
		sleep();
	}

	/**
	 * search by Task Name
	 */
	public void searchbyTaskName(String taskName) {
		waitForControl(driver, Interfaces.DocumentsPage.SEARCH_TASKNAME_TEXTBOX, timeWait);
		type(driver, Interfaces.DocumentsPage.SEARCH_TASKNAME_TEXTBOX, taskName);
		clickSearchTaskButton();
	}

	/**
	 * click Search Task Button
	 */
	public void clickSearchTaskButton() {
		waitForControl(driver, Interfaces.DocumentsPage.SEARCH_TASK_BUTTON, timeWait);
		click(driver, Interfaces.DocumentsPage.SEARCH_TASK_BUTTON);
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(5);
     }
		sleep();
	}

	/**
	 * check Task Display
	 */
	public boolean isTaskDisplay(String taskName) {
		waitForControl(driver, Interfaces.DocumentsPage.DYNAMIC_TASK_ITEM, taskName, timeWait);
		return isControlDisplayed(driver, Interfaces.DocumentsPage.DYNAMIC_TASK_ITEM, taskName);
	}

	/**
	 * open Reporting Schedule Tab
	 */
	public LoansPage openReportingScheduleTab() {
		waitForControl(driver, Interfaces.DocumentsPage.REPORTING_SCHEDULE_TAB, timeWait);
		executeJavaScript(driver, "document.getElementById('DocScheduleTB').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(7);
     }
		return PageFactory.getLoansPage(driver, ipClient);
	}

	/**
	 * check User Name Display On CE Doc List
	 */
	public boolean isUserNameDisplayOnCEDocList(String userName) {
		waitForControl(driver, Interfaces.DocumentsPage.DYNAMIC_SECTION_NAME_ON_CEDOC, userName, timeWait);
		return isControlDisplayed(driver, Interfaces.DocumentsPage.DYNAMIC_SECTION_NAME_ON_CEDOC, userName);
	}

	/**
	 * check General Loan Documents Page Display
	 */
	public boolean isGeneralLoanDocumentsPageDisplay() {
		waitForControl(driver, Interfaces.DocumentsPage.GENERAL_LOAN_DOCUMENT_TITLE, timeWait);
		return isControlDisplayed(driver, Interfaces.DocumentsPage.GENERAL_LOAN_DOCUMENT_TITLE);
	}

	/**
	 * uncheck Private Checkbox
	 */
	public void uncheckPrivateCheckbox() {
		waitForControl(driver, Interfaces.DocumentsPage.PRIVATE_CHECKBOX, timeWait);
		uncheckTheCheckbox(driver, Interfaces.DocumentsPage.PRIVATE_CHECKBOX);
		sleep(1);
	}

	/**
	 * check Private Checkbox
	 */
	public void checkPrivateCheckbox() {
		waitForControl(driver, Interfaces.DocumentsPage.PRIVATE_CHECKBOX, timeWait);
		checkTheCheckbox(driver, Interfaces.DocumentsPage.PRIVATE_CHECKBOX);
		sleep(1);
		waitForControl(driver, Interfaces.DocumentsPage.YES_CONFIRM_BUTTON, 5);
		if (isControlDisplayed(driver, Interfaces.DocumentsPage.YES_CONFIRM_BUTTON)) {
			click(driver, Interfaces.DocumentsPage.YES_CONFIRM_BUTTON);
			sleep();
		}
	}

	/**
	 * click New Button
	 */
	public void clickNewButton() {
		waitForControl(driver, Interfaces.DocumentsPage.NEW_BUTTON, timeWait);
		click(driver, Interfaces.DocumentsPage.NEW_BUTTON);
		executeJavaScript(driver, "document.getElementById('NewLoanDoc').click();");
		executeJavaScript(driver, "document.getElementById('NewCBRLoanDoc').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(5);
     }
		sleep(1);
	}

	/**
	 * select General Document Section
	 */
	public void selectGeneralDocumentSection(String documentSection) {
		waitForControl(driver, Interfaces.DocumentsPage.GENERAL_DOCUMENT_SECTION_CLICK, timeWait);
		click(driver, Interfaces.DocumentsPage.GENERAL_DOCUMENT_SECTION_CLICK);
		sleep(2);
		waitForControl(driver, Interfaces.DocumentsPage.GENERAL_DOCUMENT_SECTION_SELECT, documentSection, timeWait);
		click(driver, Interfaces.DocumentsPage.GENERAL_DOCUMENT_SECTION_SELECT, documentSection);
		sleep(1);
	}

	/**
	 * select General Document Type
	 */
	public void selectGeneralDocumentType(String documentType) {
		waitForControl(driver, Interfaces.DocumentsPage.GENERAL_DOCUMENT_TYPE, timeWait);
		sleep(1);
		selectItemCombobox(driver, Interfaces.DocumentsPage.GENERAL_DOCUMENT_TYPE, documentType);
		sleep();
	}

	/**
	 * get Number Of Property In Properties Tab
	 */
	public int getNumberOfPropertyInPropertiesTab() {
		waitForControl(driver, Interfaces.LoansPage.TOTAL_PROPERTY_LABEL, timeWait);
		String text = getText(driver, Interfaces.LoansPage.TOTAL_PROPERTY_LABEL);
		String[] subString = text.split(" ");
		int result = Integer.parseInt(subString[subString.length - 1]);
		return result;
	}

	/**
	 * get Document ID In [General Loan Document - Corporate Entity Documents]
	 * Tab
	 */
	public String getDocumentID() {
		waitForControl(driver, Interfaces.DocumentsPage.GET_DOCUMENT_ID, timeWait);
		String documentID = getText(driver, Interfaces.DocumentsPage.GET_DOCUMENT_ID);
		return documentID;
	}

	public void searchDocumentID(String documentID) {
		waitForControl(driver, Interfaces.DocumentsPage.DOCUMENT_ID_TEXTBOX, documentID, timeWait);
		type(driver, Interfaces.DocumentsPage.DOCUMENT_ID_TEXTBOX, documentID);
		sleep(1);
	}

	public void searchID(String documentID) {
		waitForControl(driver, Interfaces.DocumentsPage.DOCUMENT_ID_TEXTBOX, documentID, timeWait);
		type(driver, Interfaces.DocumentsPage.DOCUMENT_ID_TEXTBOX, documentID);
		sleep(1);
		click(driver, Interfaces.DocumentsPage.ACTIVE_RADIO_YES_BUTTON);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(5);
     }
		sleep(1);
	}

	/**
	 * open General Document Detail
	 */
	public void openGeneralDocumentDetail(String documentType) {
		waitForControl(driver, Interfaces.DocumentsPage.DYNAMIC_GENERAL_DOCUMENT_DETAILS, documentType, timeWait);
		click(driver, Interfaces.DocumentsPage.DYNAMIC_GENERAL_DOCUMENT_DETAILS, documentType);
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(7);
     }
		sleep();
	}

	/**
	 * open Basic Detail Tab
	 * 
	 * @return
	 */
	public LoansPage openBasicDetailTab() {
		waitForControl(driver, Interfaces.LoansPage.BASIC_DETAIL_TAB, timeWait);
		executeJavaScript(driver, "document.getElementById('CBRLoanApplicationTB').click();");
		executeJavaScript(driver, "document.getElementById('LoanApplicationTB').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(7);
     }
		sleep(1);
		return PageFactory.getLoansPage(driver, ipClient);
	}
	
	/**
	 * click Send this information to email
	 */
	public void clickSendInformationToEmail() {
		waitForControl(driver, Interfaces.DocumentsPage.SEND_INFORMATION_TO_EMAIL, timeWait);
		click(driver, Interfaces.DocumentsPage.SEND_INFORMATION_TO_EMAIL);
		sleep(15);
	}
	
	/**
	 * Get user information
	 */
	public String getDocumentNumber() {
		waitForControl(driver, Interfaces.DocumentsPage.FIRST_DOCUMENT_NUMBER, timeWait);
		return getText(driver, Interfaces.DocumentsPage.FIRST_DOCUMENT_NUMBER).trim().toLowerCase();
	}
	
	/**
	 * click On Property List Button
	 */
	public LoansPage clickOnPropertyTab() {
		waitForControl(driver, Interfaces.DocumentsPage.PROPERTIES_TAB, timeWait);
		executeJavaScript(driver, "document.getElementById('PropertyTB').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")){
            sleep(10);
       }
		sleep();
		return PageFactory.getLoansPage(driver, ipClient);
	}
	
	/**
	 * Get user information
	 */
	public String getDataTapeName() {
		waitForControl(driver, Interfaces.DocumentsPage.DATA_TAPE_NAME, timeWait);
		return getText(driver, Interfaces.DocumentsPage.DATA_TAPE_NAME).trim().toLowerCase();
	}
	
	/**
	 * Check Loan changed date displays
	 */
	
	public Boolean isLastChangedDateDisplayedCorrectly(){
		waitForControl(driver, Interfaces.DocumentsPage.DOCUMENT_TYPES, timeWait);
		int numberOfDocumentTypes = countElement(driver, Interfaces.DocumentsPage.DOCUMENT_TYPES);
		for(int i =1; i<= numberOfDocumentTypes; i++){
			if (!getText(driver, Interfaces.DocumentsPage.DYNAMIC_DOCUMENT_LAST_CHANGED_BY_STATUS, Integer.toString(i)).equals(""))
				if(getText(driver, Interfaces.DocumentsPage.DYNAMIC_DOCUMENT_LOADED_FILE_STATUS, Integer.toString(i)).contains("No"))
					return false;
		}
		return true;
	}
	
	/**
	 * search Document Index
	 */
	public void searchByDocumentIndex(String addedOnTime, String value) {
		waitForControl(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_FROM_DATE_TEXTFIED, timeWait);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_FROM_DATE_TEXTFIED, addedOnTime);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_TO_DATE_TEXTFIED, addedOnTime);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_DOCUMENT_INDEX_TEXTFIED, value);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search4').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		sleep();
	}
	
	/**
	 * search Added on time
	 */
	public void searchByAddedOnTime(String addedOnTime) {
		waitForControl(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_FROM_DATE_TEXTFIED, timeWait);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_FROM_DATE_TEXTFIED, addedOnTime);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_TO_DATE_TEXTFIED, addedOnTime);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search4').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		sleep();
	}
	
	/**
	 * search Reviewed on time
	 */
	public void searchByReviewedOnTime(String reviewedOnTime) {
		waitForControl(driver, Interfaces.DocumentsPage.SEARCH_BY_REVIEWED_ON_FROM_DATE_TEXTFIED, timeWait);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_REVIEWED_ON_FROM_DATE_TEXTFIED, reviewedOnTime);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_REVIEWED_ON_TO_DATE_TEXTFIED, reviewedOnTime);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search4').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		sleep();
	}
	
	/**
	 * search Property
	 */
	public void searchByProperty(String addedOnTime, String value) {
		waitForControl(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_FROM_DATE_TEXTFIED, timeWait);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_FROM_DATE_TEXTFIED, addedOnTime);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_TO_DATE_TEXTFIED, addedOnTime);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_PROPERTY_TEXTFIED, value);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search4').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		sleep();
	}
	
	/**
	 * search Document Index
	 */
	public void searchByLastChangedBy(String addedOnTime, String value) {
		waitForControl(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_FROM_DATE_TEXTFIED, timeWait);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_FROM_DATE_TEXTFIED, addedOnTime);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_TO_DATE_TEXTFIED, addedOnTime);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_LAST_CHANGED_BY_TEXTFIED, value);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search4').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		sleep();
	}
	
	/**
	 * search property Applicant
	 */
	public void searchByApplicant(String addedOnTime, String value) {
		waitForControl(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_FROM_DATE_TEXTFIED, timeWait);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_FROM_DATE_TEXTFIED, addedOnTime);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_TO_DATE_TEXTFIED, addedOnTime);
		selectItemCombobox(driver,	Interfaces.DocumentsPage.SEARCH_BY_APPLICANT_DROPDOWN, value);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search4').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		sleep();
	}
	
	/**
	 * search Reviewed by username
	 */
	public void searchByReviewUsername(String addedOnTime, String value) {
		waitForControl(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_FROM_DATE_TEXTFIED, timeWait);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_FROM_DATE_TEXTFIED, addedOnTime);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_TO_DATE_TEXTFIED, addedOnTime);
		selectItemCombobox(driver,	Interfaces.DocumentsPage.SEARCH_BY_REVIEWED_BY_TEXTFIED, value);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search4').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		sleep();
	}
	
	/**
	 * search Submitted Via
	 */
	public void searchBySubmittedVia(String addedOnTime, String value) {
		waitForControl(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_FROM_DATE_TEXTFIED, timeWait);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_FROM_DATE_TEXTFIED, addedOnTime);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_TO_DATE_TEXTFIED, addedOnTime);
		selectItemCombobox(driver,	Interfaces.DocumentsPage.SEARCH_BY_SUBMITTED_VIA_DROPDOWN, value);
		sleep(1);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search4').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		sleep();
	}
	
	/**
	 * search documents type
	 */
	public void searchByDocumentType(String addedOnTime, String value) {
		waitForControl(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_FROM_DATE_TEXTFIED, timeWait);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_FROM_DATE_TEXTFIED, addedOnTime);
		type(driver, Interfaces.DocumentsPage.SEARCH_BY_ADDED_ON_TO_DATE_TEXTFIED, addedOnTime);
		click(driver, Interfaces.DocumentsPage.SEARCH_BY_DOCUMENT_TYPE_DROPDOWN);
		sleep(1);
		click(driver, Interfaces.DocumentsPage.DYNAMIC_SEARCH_BY_DOCUMENT_TYPE_DROPDOWN_OPTION, value);
		executeJavaScript(driver, "document.getElementById('Search').click();");
		executeJavaScript(driver, "document.getElementById('Search4').click();");
		if(driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(60);
		}
		sleep();
	}
	
	/**
	 * select Active radio button
	 */
	public void selectActiveRadioButton(String status) {
		waitForControl(driver,	Interfaces.PropertiesPage.SEARCH_BY_CITY_TEXTFIELD, timeWait);
		if(status.contains("Yes")) 
		click(driver, Interfaces.PropertiesPage.DYNAMIC_SEARCH_BY_ACTIVE_RADIO_BUTTON, "1");
		else if(status.contains("No")) click(driver, Interfaces.PropertiesPage.DYNAMIC_SEARCH_BY_ACTIVE_RADIO_BUTTON, "0");
		else click(driver, Interfaces.PropertiesPage.DYNAMIC_SEARCH_BY_ACTIVE_RADIO_BUTTON, "");
	}

	/**
	 * Get text in Documents search
	 * 
	 * @param text
	 * @return
	 */
	public String getTextDocumentsSearch(String text) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_TEXT_PROPERTY_INFORMATION_SEARCH, text, timeWait);
		String itemName = getText(driver, Interfaces.PropertiesPage.DYNAMIC_TEXT_PROPERTY_INFORMATION_SEARCH, text);
		return itemName;
	}
	
	/**
	 * Get text in Documents search
	 * 
	 * @param text
	 * @return
	 */
	public String getTextDocumentsSearch(String text, int i) {
		waitForControl(driver, Interfaces.PropertiesPage.DYNAMIC_TEXT_PROPERTY_INFORMATION_SEARCH, text, timeWait);
		String itemName = getText(driver, Interfaces.PropertiesPage.DYNAMIC_TEXT_PROPERTY_INFORMATION_SEARCH, text);
		if(i==1)return convertDate(itemName);
		else return itemName.replace("$", "").replace(".00", "");
	}
	
	public Boolean isAllValueDisplayOnSearchDocumentsTable(String documentIndex, String applicantName, String propertySearchAddress, String documentType1, String submittedMethod, String username, String addedOnTime, String reviewedTime, String active){
		waitForControl(driver, Interfaces.DocumentsPage.NO_DOCUMENTS_FOUND_ALERT, 5);
		if(isControlDisplayed(driver, Interfaces.DocumentsPage.NO_DOCUMENTS_FOUND_ALERT)) return false;
		if(!documentIndex.contains(getTextDocumentsSearch("DataCell FieldTypeSQLServerIdentity datacell_Document"))) return false;
		if(!applicantName.contains(getTextDocumentsSearch("DataCell FieldTypeVarchar datacell_Partner"))) return false;
		if(!propertySearchAddress.contains(getTextDocumentsSearch("DataCell FieldTypeVarchar datacell_PropDisp"))) return false;
		if(!documentType1.contains(getTextDocumentsSearch("DataCell FieldTypeVarchar datacell_FormDisp"))) return false;
		if(!getTextDocumentsSearch("DataCell FieldTypeVarchar datacell_CaptureDisplay").contains(submittedMethod)) return false;
		if(!username.contains(getTextDocumentsSearch("DataCell FieldTypeForeignKey datacell_LastModifiedBy"))) return false;
		if(!addedOnTime.contains(getTextDocumentsSearch("DataCell FieldTypeDateTime datacell_Attached"))) return false;
		if(!reviewedTime.contains(getTextDocumentsSearch("DataCell FieldTypeDateTime datacell_LastReviewed"))) return false;
		if(!username.contains(getTextDocumentsSearch("DataCell FieldTypeForeignKey datacell_ReviewBy"))) return false;
		if(!active.contains(getTextDocumentsSearch("DataCell FieldTypeBoolean datacell_IsActive"))) return false;
		return true;
	}
	
	/**
	 * open Property Detail
	 */
	public void clickOnMarkAsReviewedButton() {
		waitForControl(driver, Interfaces.PropertiesPage.MARK_AS_REVIEWED_BUTTON, timeWait);
		click(driver, Interfaces.PropertiesPage.MARK_AS_REVIEWED_BUTTON);
		sleep(3);
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			sleep(5);
		}
	}
	
	public String getAddedOnTime(){
		waitForControl(driver, Interfaces.DocumentsPage.ADDED_ON_TIME, timeWait);
		return getText(driver, Interfaces.DocumentsPage.ADDED_ON_TIME);
	}
	
	private WebDriver driver;
	private String ipClient;
}