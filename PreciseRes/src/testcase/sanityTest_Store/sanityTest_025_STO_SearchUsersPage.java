package sanityTest_Store;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import page.LoginPage;
import page.PageFactory;
import page.TransactionPage;
import page.UsersPage;
import common.AbstractTest;
import common.Constant;

public class sanityTest_025_STO_SearchUsersPage extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		storeLenderUsername = Constant.USERNAME_STORE_LENDER;
		storeLenderPassword = Constant.PASSWORD_STORE_LENDER;
		accountNumber = getUniqueNumber();
		companyName = "Testing QA " + accountNumber;
		email = "qatesting" + accountNumber + "@gmail.com";
		userName = "New user for Search" + accountNumber;
		userType = "Survey Provider";
	}

	@Test(groups = { "regression" }, description = "Search User page 01 - Search by User name")
	public void SearchUsersPage_01_SearchByUserName() {
		
		log.info("Precondition - Step 01: Login with valid username password");
		transactionPage = loginPage.loginStoreAsLender(storeLenderUsername, storeLenderPassword, false);

		log.info("Precondition - Step 02: Open Users page");
		usersPage = transactionPage.openUsersPage();
		
		log.info("Precondition - Step 03: Create three Survey Provider");
		usersPage.createNewUser(userName, userType, email, companyName);
		
		log.info("SearchActiveInactive_01 - Step 02: Search Users by User name");
		usersPage.searchName(userName);
		
		log.info("VP: The Transaction with Company name display in table");
		verifyTrue(usersPage.isAllValueDisplayOnSearchUsersEntityTable(userName, userType, companyName, email, "Store."+userName));
	}
	
	@Test(groups = { "regression" }, description = "Search User page 03 - Search by user type")
	public void SearchUsersPage_02_SearchByUserType() {
		
		log.info("Precondition - Step 02: Open Users page");
		usersPage = transactionPage.openUsersPage();
		
		log.info("SearchActiveInactive_01 - Step 02: Search Users by User type");
		usersPage.searchEntityType(userName, userType);
		
		log.info("VP: The Transaction with Company name display in table");
		verifyTrue(usersPage.isAllValueDisplayOnSearchUsersEntityTable(userName, userType, companyName, email, "Store."+userName));
	}
	
	@Test(groups = { "regression" }, description = "Search User page 03 - Search by company name")
	public void SearchUsersPage_03_SearchByCompanyName() {
		
		log.info("Precondition - Step 02: Open Users page");
		usersPage = transactionPage.openUsersPage();
		
		log.info("SearchActiveInactive_01 - Step 02: Search Users by User type");
		usersPage.searchCompanyName(companyName);
		
		log.info("VP: The Transaction with Company name display in table");
		verifyTrue(usersPage.isAllValueDisplayOnSearchUsersEntityTable(userName, userType, companyName, email, "Store."+userName));
	}
	
	@Test(groups = { "regression" }, description = "Search User page 04 - Search by Email")
	public void SearchUsersPage_04_SearchByEmail() {
		
		log.info("Precondition - Step 02: Open Users page");
		usersPage = transactionPage.openUsersPage();
		
		log.info("SearchActiveInactive_01 - Step 02: Search Users by User type");
		usersPage.searchEmail(email);
		
		log.info("VP: The Transaction with Company name display in table");
		verifyTrue(usersPage.isAllValueDisplayOnSearchUsersEntityTable(userName, userType, companyName, email, "Store."+userName));
	}
	
	@Test(groups = { "regression" }, description = "Search User page 05 - Search by Login id")
	public void SearchUsersPage_05_SearchByLoginID() {
		
		log.info("Precondition - Step 02: Open Users page");
		usersPage = transactionPage.openUsersPage();
		
		log.info("SearchActiveInactive_01 - Step 02: Search Users by User type");
		usersPage.searchLogin("Store."+userName);
		
		log.info("VP: The Transaction with Company name display in table");
		verifyTrue(usersPage.isAllValueDisplayOnSearchUsersEntityTable(userName, userType, companyName, email, "Store."+userName));
	}
	
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private TransactionPage transactionPage;
	private UsersPage usersPage;
	private String storeLenderUsername, storeLenderPassword;
	private String companyName, accountNumber;
	private String  email;
	private String userName, userType;
}