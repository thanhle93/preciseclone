package sanityTest_CAFFNF;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.FNFBorrowersPage;
import page.FNFLoansPage;
import page.HomePage;
import page.LoginPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;

public class sanityTest_022_FNF_MakeLoanActiveInactive extends AbstractTest {

	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		driver = openBrowser(browser, port, ipClient);
		loginPage = PageFactory.getLoginPage(driver, ipClient);
		FNFLenderUsername = Constant.USERNAME_FNF_LENDER;
		FNFLenderPassword = Constant.PASSWORD_FNF_LENDER;
		FNFBorrowerUsername = Constant.USERNAME_FNF_BORROWER;
		FNFBorrowerPassword = Constant.PASSWORD_FNF_BORROWER;
		accountNumber = Constant.ACCOUNT_NUMBER;
		companyName = "Edit_Borrower " + accountNumber;
		locClosingDate = "04/12/2014";
		loanName = "Edit Loan Auto " + getUniqueNumber();
	}

	@Test(groups = { "regression" }, description = "Applicant - Login as Lender - Make Loan inactive")
	public void MakeLoanActiveInactive_01_Lender_MakeLoanInactive() {

		log.info("MakeLoanActiveInactive_01 - Step 01. Logout and login with Lender");
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("MakeLoanActiveInactive_01 - Step 02: Go to 'Applicant' page");
		applicantsPage = homePage.openBorrowersPage(driver, ipClient);
	
		log.info("MakeLoanActiveInactive_01 - Step 03: Input 'Applicant Name' and click on 'Search' button");
		applicantsPage.clickOnSearchButton(companyName);
	
		log.info("MakeLoanActiveInactive_01 - Step 04: Click 'Applicant Name'");
		applicantsPage.clickOnApplicantName();
		
		log.info("MakeLoanActiveInactive_01 - Step 05: Click 'New Loan' button");
		loanPage = applicantsPage.clickNewLoanButton();
		
		log.info("MakeLoanActiveInactive_01 - Step 06: Input loan name");
		loanPage.inputRequiredData(loanName, locClosingDate);
		
		log.info("MakeLoanActiveInactive_01 - Step 07: Click Save button");
		loanPage.clickOnSaveButton();
		
		log.info("MakeLoanActiveInactive_01 - Step 08: Click Back to loan list button");
		loanPage.clickBackToListButton();
		
		log.info("MakeLoanActiveInactive_01 - Step 09: Click on Search button");
		loanPage.clickOnSearchButton(loanName);
		
		log.info("MakeLoanActiveInactive_01 - Step 10: Click on 'Check all' link");
		loanPage.clickOnCheckAllLink();
		
		log.info("MakeLoanActiveInactive_01 - Step 11: Click on 'Make Borrower Inactive' button");
		loanPage.clickMakeInactiveButton();
		
		log.info("MakeLoanActiveInactive_01 - Step 12: Search for Inactive loan");
		loanPage.selectActiveRadioButton("No");
		loanPage.clickOnSearchButton(loanName);
		
		log.info("MakeLoanActiveInactive_01 - Step 13: Open loan page");
		loanPage.openLoansDetailPage(loanName);
		
		log.info("VP: Make sure that 'This record is inactive!' is displayed");
		verifyTrue(loanPage.isInfoMessageDisplayed("This record is inactive!"));
		
		log.info("MakeLoanActiveInactive_01 - Step 14: Login with Applicant");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);
		
		log.info("VP: Make sure that loan isn't displayed in Loan Opportunity table");
		verifyFalse(applicantsPage.isLoanDisplayedInOpportunityTable(loanName));
		
		log.info("MakeLoanActiveInactive_01 - Step 15: Open Loans page");
		applicantsPage.openLoansPage(driver, ipClient);
		
		log.info("MakeLoanActiveInactive_01 - Step 16: Search for Inactive loan");
		loanPage.selectActiveRadioButton("No");
		loanPage.clickOnSearchButton(loanName);
		
		log.info("MakeLoanActiveInactive_01 - Step 17: Open loan page");
		loanPage.openLoansDetailPage(loanName);
		
		log.info("VP: Make sure that 'Warehoused' fields aren't displayed");
		verifyFalse(loanPage.isFieldNameDisplayed(driver, "Warehoused"));
		
		log.info("VP: Make sure that 'This record is inactive!' is displayed");
		verifyTrue(loanPage.isInfoMessageDisplayed("This record is inactive!"));
	}
	
	@Test(groups = { "regression" }, description = "Applicant - Login as Lender - Make Borrower Active")
	public void MakeLoanActiveInactive_02_Lender_MakeBorrowerActiveAgain() {

		log.info("MakeLoanActiveInactive_02 - Step 01. Logout and login with Lender");
		loginPage = logout(driver, ipClient);
		homePage = loginPage.loginAsLender(FNFLenderUsername, FNFLenderPassword, false);
	
		log.info("MakeLoanActiveInactive_02 - Step 02: Open Loans page");
		loanPage = homePage.openFNFLoansPage(driver, ipClient);
		
		log.info("MakeLoanActiveInactive_02 - Step 03: Search for Inactive loan");
		loanPage.selectActiveRadioButton("No");
		loanPage.clickOnSearchButton(loanName);
		
		log.info("MakeLoanActiveInactive_02 - Step 04: Click on 'Check all' link");
		loanPage.clickOnCheckAllLink();
		
		log.info("MakeLoanActiveInactive_02 - Step 05: Click on 'Make Borrower Inactive' button");
		loanPage.clickMakeActiveButton();
		
		log.info("MakeLoanActiveInactive_02 - Step 06: Search for Inactive loan");
		loanPage.selectActiveRadioButton("Yes");
		loanPage.clickOnSearchButton(loanName);
		
		log.info("MakeLoanActiveInactive_02 - Step 07: Open loan page");
		loanPage.openLoansDetailPage(loanName);
		
		log.info("VP: Make sure that 'This record is inactive!' is displayed");
		verifyFalse(applicantsPage.isInfoMessageDisplayed("This record is inactive!"));
		
		log.info("MakeLoanActiveInactive_02 - Step 08: Login with Applicant");
		loginPage = logout(driver, ipClient);
		applicantsPage = loginPage.loginFNFAsBorrower(FNFBorrowerUsername, FNFBorrowerPassword, false);
		
		log.info("VP: Make sure that loan isn't displayed in Loan Opportunity table");
		verifyTrue(applicantsPage.isLoanDisplayedInOpportunityTable(loanName));
		
		log.info("MakeLoanActiveInactive_02 - Step 09: Open Loans page");
		applicantsPage.openLoansPage(driver, ipClient);
		
		log.info("MakeLoanActiveInactive_02 - Step 10: Search for Inactive loan");
		loanPage.selectActiveRadioButton("Yes");
		loanPage.clickOnSearchButton(loanName);
		
		log.info("MakeLoanActiveInactive_02 - Step 11: Open loan page");
		loanPage.openLoansDetailPage(loanName);
		
		log.info("VP: Make sure that 'Warehoused' fields aren't displayed");
		verifyFalse(loanPage.isFieldNameDisplayed(driver, "Warehoused"));
		
		log.info("VP: Make sure that 'This record is inactive!' is displayed");
		verifyFalse(loanPage.isInfoMessageDisplayed("This record is inactive!"));
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		logout(driver, ipClient);
		closeBrowser(driver);
	}

	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private FNFLoansPage loanPage;
	private FNFBorrowersPage applicantsPage;
	private String FNFLenderUsername, FNFLenderPassword, FNFBorrowerUsername,	FNFBorrowerPassword;
	private String companyName, accountNumber, loanName;
	private String locClosingDate;
	}